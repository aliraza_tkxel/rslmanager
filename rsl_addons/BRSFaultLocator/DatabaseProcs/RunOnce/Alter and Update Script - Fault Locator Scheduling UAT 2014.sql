--=========================================================================
--====== Add Duration Column in FL_TEMP_FAULT
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_TEMP_FAULT'
		AND COLUMN_NAME = 'Duration'
) ALTER TABLE FL_TEMP_FAULT Add Duration DECIMAL(9,2) NULL
GO

--===============================================================
--=== Populate Available values from FL_FAULT ===================
--===============================================================

UPDATE TF SET
	TF.Duration = COALESCE(TF.duration,F.duration)
FROM FL_TEMP_FAULT TF
INNER JOIN FL_FAULT F ON F.FaultID = TF.FaultID
WHERE TF.Duration IS NULL
GO

--=========================================================================
--====== Add FollowOnFaultLogId Column in FL_FAULT_LOG
--=========================================================================
IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_LOG'
		AND COLUMN_NAME = 'FollowOnFaultLogId'
) ALTER TABLE FL_FAULT_LOG Add FollowOnFaultLogId INT NULL
GO
--===============================================================
--=== Populate Available values from FL_TEMP_FAULT ==============
-- This will partially fill the values in FollowOnFaultLogId
-- as values from FL_TEMP_FAULT are clean on fault basket complete.
--===============================================================

UPDATE FL SET
	FL.FollowOnFaultLogId = TF.FollowOnFaultLogID
FROM FL_FAULT_LOG FL
INNER JOIN FL_TEMP_FAULT TF ON TF.FaultLogId = FL.FaultLogID
WHERE FL.FollowOnFaultLogId IS NULL
GO

--=========================================================================
--====== Add Duration Column in FL_FAULT_LOG
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_LOG'
		AND COLUMN_NAME = 'Duration'
) ALTER TABLE FL_FAULT_LOG Add Duration DECIMAL(9,2) NULL
GO

--===============================================================
--=== Populate Available values from FL_FAULT ===================
--===============================================================

-- Step 1: Populate Values Available in FL_FAULT
UPDATE FL SET
	FL.Duration = COALESCE(FL.duration,F.duration)
FROM FL_FAULT_LOG FL
INNER JOIN FL_FAULT F ON F.FaultID = FL.FaultID
WHERE FL.Duration IS NULL
GO

-- Step 2: Populate Values from the difference of appointment start and end time

UPDATE
	FL
SET
	FL.Duration = COALESCE(FL.duration, DATEDIFF(MINUTE, DATEADD(MINUTE, DATEDIFF(MINUTE, DATEDIFF(DAY, 0, A.Time), A.Time), A.AppointmentDate), DATEADD(MINUTE, DATEDIFF(MINUTE, DATEDIFF(DAY, 0, A.EndTime), A.EndTime), A.AppointmentDate)) / 60)
FROM
	FL_FAULT_LOG FL	
	INNER JOIN FL_FAULT_APPOINTMENT FA
		ON FA.FaultLogId = FL.FaultLogID
	INNER JOIN FL_CO_APPOINTMENT A
		ON A.AppointmentID = FA.AppointmentId
		AND A.Time IS NOT NULL
		AND A.Time NOT IN ('00:00:00 PM')
		AND A.EndTime IS NOT NULL
		AND A..EndTime NOT IN ('00:00:00 PM')
WHERE
	FL.Duration IS NULL
	
-- Step 3: Populate Values from the difference of appointment start and end time
UPDATE
	FL
SET
	FL.Duration = COALESCE(FL.duration, DATEDIFF(MINUTE, DATEADD(MINUTE, DATEDIFF(MINUTE, DATEDIFF(DAY, 0, A.Time), A.Time), A.AppointmentDate), DATEADD(MINUTE, DATEDIFF(MINUTE, DATEDIFF(DAY, 0, A.EndTime), A.EndTime), A.AppointmentDate)) / 60)
FROM
	FL_FAULT_LOG FL		
	INNER JOIN FL_CO_APPOINTMENT A
		ON  A.JobSheetNumber IS NOT NULL
				AND FL.FaultLogID = SUBSTRING(A.JobSheetNumber, 3, LEN(A.JobSheetNumber))		
		AND A.Time IS NOT NULL
		AND A.Time NOT IN ('00:00:00 PM')
		AND A.EndTime IS NOT NULL
		AND A.EndTime NOT IN ('00:00:00 PM')		
WHERE
	FL.Duration IS NULL

--=========================================================================
--====== Add Duration Column in FL_FAULT_LOG_HISTORY
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_LOG_HISTORY'
		AND COLUMN_NAME = 'Duration'
) ALTER TABLE FL_FAULT_LOG_HISTORY Add Duration DECIMAL(9,2) NULL
GO

--===============================================================
--=== Populate Available values from FL_FAULT ===================
--===============================================================

UPDATE FLH SET
	FLH.Duration = COALESCE(FLH.duration,FL.duration)
FROM FL_FAULT_LOG_HISTORY FLH
INNER JOIN FL_FAULT_LOG FL ON FL.FaultLogID = FLH.FaultLogID
WHERE FLH.Duration IS NULL
GO

--=========================================================================
--====== Add AppointmentEndDate Column in FL_CO_APPOINTMENT
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_CO_APPOINTMENT'
		AND COLUMN_NAME = 'AppointmentEndDate'
) ALTER TABLE FL_CO_APPOINTMENT Add AppointmentEndDate DATETIME NULL
GO

--===============================================================
--=== Populate Available values from FL_CO_APPOINTMENT ==========
-- Update AppointmentDate as AppointmentEndDate for existing
--  recorders
--===============================================================

UPDATE FL_CO_APPOINTMENT SET
	AppointmentEndDate = ISNULL(AppointmentEndDate,AppointmentDate)
WHERE AppointmentEndDate IS NULL
GO

--=========================================================================
--====== Add AppointmentEndDate Column in FL_CO_APPOINTMENT
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_CO_APPOINTMENT_HISTORY'
		AND COLUMN_NAME = 'AppointmentEndDate'
) ALTER TABLE FL_CO_APPOINTMENT_HISTORY Add AppointmentEndDate DATETIME NULL
GO

--===============================================================
--=== Populate Available values from FL_CO_APPOINTMENT_HISTORY
-- Update AppointmentDate as AppointmentEndDate for existing
--  recorders
--===============================================================

UPDATE FL_CO_APPOINTMENT_HISTORY SET
	AppointmentEndDate = ISNULL(AppointmentEndDate,AppointmentDate)
WHERE AppointmentEndDate IS NULL	
GO