/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	2nd October 2013
--  Description:	Describes whether the appointment is created from calendar or not
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE FL_CO_APPOINTMENT
ADD isCalendarAppointment bit

ALTER TABLE FL_CO_APPOINTMENT_HISTORY
ADD isCalendarAppointment bit
