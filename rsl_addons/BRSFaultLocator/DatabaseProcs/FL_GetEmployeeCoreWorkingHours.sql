USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--Exec FL_GetEmployeeCoreWorkingHours @employeeIds ='100,1000,108,113,127,1005'
-- Author:		<Ahmed Mehmood>
-- Create date: <23/10/2014>
-- Description:	<This store procedure shall provide the engineer's core working information>
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetEmployeeCoreWorkingHours] 
@employeeIds nvarchar(250)
AS
BEGIN

SELECT
	ID
	,COLUMN1	AS EmployeeId
	INTO
		#employees
FROM
	dbo.SPLIT_STRING(@employeeIds, ',')

SELECT DISTINCT
	NAME				AS WeekDayName
	,E_WEEKDAYS.DayId	AS DayId
	,ECWH.EMPLOYEEID	AS EmployeeId
	,STARTTIME			AS StartTime
	,ENDTIME			AS EndTime
FROM
	E_CORE_WORKING_HOURS ECWH
	INNER JOIN E_WEEKDAYS
		ON ECWH.DAYID = E_WEEKDAYS.DAYID
	INNER JOIN #employees employees
		ON employees.EmployeeId = ECWH.EMPLOYEEID

WHERE
	LEN(STARTTIME) > 0
	AND LEN(ENDTIME) > 0
	
UNION ALL SELECT
	NAME				AS WeekDayName
	,E_WEEKDAYS.DayId	AS DayId
	,EOOH.EMPLOYEEID	AS EmployeeId
	,STARTTIME			AS StartTime
	,ENDTIME			AS EndTime
FROM
	E_OUT_OF_HOURS EOOH
	INNER JOIN E_WEEKDAYS
		ON DATEPART(WEEKDAY, EOOH.STARTDATE) = E_WEEKDAYS.DAYID
	INNER JOIN #employees employees
		ON employees.EmployeeId = EOOH.EMPLOYEEID

DROP TABLE #employees

END