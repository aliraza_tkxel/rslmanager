SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*-- =============================================
-- Author:           Ahmed Mehmood
-- Create date:      20/01/2016
-- Description:      PropertyCalendar.aspx

-- EXEC FL_GetAppointmentNotes 12, 'Reactive'

Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[FL_GetAppointmentNotes]
		@appointmentId = 1,
		@appointmentType = N'Pdr'

SELECT	'Return Value' = @return_value

*================================================================================= */

CREATE PROCEDURE [dbo].[FL_GetAppointmentNotes]
@appointmentId int = -1
, @appointmentType nvarchar(200) = ''

AS
BEGIN

		
		IF @appointmentType = 'Reactive'
		BEGIN
			SELECT	FL_CO_APPOINTMENT.Notes AS notes
			FROM	FL_CO_APPOINTMENT
			WHERE	AppointmentID = @appointmentId
		END
		ELSE IF @appointmentType = 'Planned'
		BEGIN
			SELECT	PLANNED_APPOINTMENTS.APPOINTMENTNOTES  AS notes
			FROM	PLANNED_APPOINTMENTS
			WHERE	APPOINTMENTID = @appointmentId
		END
		ELSE IF @appointmentType = 'Pdr'
		BEGIN
			SELECT	PDR_APPOINTMENTS.APPOINTMENTNOTES AS notes
			FROM	PDR_APPOINTMENTS
			WHERE	APPOINTMENTID = @appointmentId
		END
		ELSE IF @appointmentType = 'Stock'
		BEGIN
			SELECT	PS_Appointment.AppointNotes 
			FROM	PS_Appointment
			WHERE	AppointId = @appointmentId
		END
		ELSE IF @appointmentType = 'Appliance'
		BEGIN
			SELECT	AS_APPOINTMENTS.NOTES AS notes
			FROM	AS_APPOINTMENTS
			WHERE	APPOINTMENTID = @appointmentId
		END


END