USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetSbJobSheetSummaryByJSN]    Script Date: 01/05/2016 11:47:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[FL_GetSbJobSheetSummaryByJSN]
/* ===========================================================================
--	EXEC FL_GetJobSheetSummaryByJSN
		@JSN = 'JS4173'
--  Author:			Aamir Waheed
--  DATE CREATED:	14 March 2013
--  Description:	To Get Job Sheet Summary Detail By Job Sheet Number
--  Webpage:		View/Reports/ReportArea.aspx 
 '==============================================================================*/
	@JSN nvarchar(20) = NULL  
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT	TOP 1			
			FL_FAULT_LOG.FaultLogID AS FaultLogID,
			FL_FAULT_LOG.JobSheetNumber AS JSN,
			FL_FAULT_STATUS.Description AS FaultStatus,
			S_ORGANISATION.NAME AS Contractor,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1
				THEN CONVERT(varchar(5), FL_FAULT_PRIORITY.ResponseTime) + ' days' 
				ELSE CONVERT(varchar(5), FL_FAULT_PRIORITY.ResponseTime) + ' hours' END AS priority,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1
				THEN CONVERT(VARCHAR, DATEADD(DAY, FL_FAULT_PRIORITY.ResponseTime, FL_FAULT_LOG.SubmitDate), 106)
					 + ' ' + CONVERT(VARCHAR(5), DATEADD(DAY, FL_FAULT_PRIORITY.ResponseTime, GETDATE()), 108) 
				ELSE CONVERT(VARCHAR, DATEADD(Hour, FL_FAULT_PRIORITY.ResponseTime, FL_FAULT_LOG.SubmitDate), 106)
					 + ' ' + CONVERT(VARCHAR(5), DATEADD(Hour, FL_FAULT_PRIORITY.ResponseTime, GETDATE()), 108) END AS completionDue, 
			CONVERT(VARCHAR, FL_FAULT_LOG.SubmitDate, 106)
				 + ' ' + CONVERT(VARCHAR(5), FL_FAULT_LOG.SubmitDate, 108) AS orderDate,
			FL_AREA.AreaName AS Location,
			FL_FAULT.Description AS Description,
			ISNULL(STUFF((
							SELECT
								', ' + [G_TRADE].[Description] 
							FROM
								FL_FAULT_TRADE
								JOIN G_TRADE
								ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
							WHERE
								FL_FAULT_TRADE.FaultId = FL_FAULT.FaultID
							FOR XML PATH('')
						)
						,1,1,''), 'No Trade Specified') As Trade,
			FL_FAULT_LOG.Notes AS Notes,
			ISNULL(P_SCHEME.SCHEMENAME,' ') as SchemeName, 
				Case 
					When FL_FAULT_LOG.BlockId IS NOT NULL THEN P_BLOCK.BLOCKNAME
					ELSE 'Non Selected' END as [BlockName],
					
				Case 
					When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.ADDRESS1,' ')+' '+ISNULL(P_BLOCK.ADDRESS2,' ')+' '+ISNULL(P_BLOCK.ADDRESS3,' ') 
					ELSE ISNULL(PDR_DEVELOPMENT.ADDRESS1,' ')+' '+ISNULL(PDR_DEVELOPMENT.ADDRESS2,' ') END as [Address],
				Case	
					When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.TOWNCITY,'N/A')
					ELSE ISNULL(PDR_DEVELOPMENT.TOWN,'N/A ') END as Town,
					
				Case	
					When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.COUNTY,'N/A')
					ELSE ISNULL(PDR_DEVELOPMENT.COUNTY,'N/A ') END as County,
				Case	
					When FL_FAULT_LOG.BlockId IS NOT NULL THEN ISNULL(P_BLOCK.POSTCODE,'N/A')
					ELSE ISNULL(PDR_DEVELOPMENT.PostCode,'N/A ') END as PostCode	,	
					

			ISNULL(TblRepairNotes.Notes,' ') as RepairNotes       
	FROM    FL_FAULT_LOG
			INNER JOIN S_ORGANISATION ON FL_FAULT_LOG.ORGID = S_ORGANISATION.ORGID
			INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
			INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
			INNER JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID			
			INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
			INNER JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
			Left JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID  
			LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
			LEFT JOIN (Select * from FL_FAULT_LOG_HISTORY Where FaultStatusID = 17 ) as TblRepairNotes on TblRepairNotes.FaultLogID = FL_FAULT_LOG.FaultLogID

	WHERE	(
				FL_FAULT_LOG.JobSheetNumber = @JSN
				--AND C_ADDRESS.ISDEFAULT = 1
				 )
	ORDER BY TblRepairNotes.LastActionDate DESC

	Select	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID,
			P_Asbestos.RISKDESCRIPTION Description
				
	From	FL_FAULT_LOG 
			INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL ON P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID = FL_FAULT_LOG.PROPERTYID
			INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
			INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID	  
		
	Where	FL_FAULT_LOG.JobSheetNumber = @JSN

END
