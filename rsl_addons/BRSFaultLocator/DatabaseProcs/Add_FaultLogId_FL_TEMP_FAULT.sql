
-- =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <30/01/2013>
-- Description:	<Add FaultLogId column in FL_TEMP_FAULT to get JSN when Appointment is confirmed>
-- =============================================

ALTER TABLE FL_TEMP_FAULT
ADD FaultLogId int,
IsAppointmentConfirmed bit