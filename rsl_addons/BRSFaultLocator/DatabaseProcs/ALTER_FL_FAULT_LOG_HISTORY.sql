/*
   Tuesday, January 20, 201512:17:08 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_LOG_HISTORY ADD
	SchemeID int NULL,
	BlockID int NULL
GO
ALTER TABLE dbo.FL_FAULT_LOG_HISTORY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_LOG_HISTORY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_LOG_HISTORY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_LOG_HISTORY', 'Object', 'CONTROL') as Contr_Per 