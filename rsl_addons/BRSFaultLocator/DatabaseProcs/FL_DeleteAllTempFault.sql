SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Behroz Sikander>
-- Create date: <14/03/2013>
-- Description:	<This stored procedrue deletes all the temp fault records provided in the parameters>
-- Webpage : ReArrangingCurrentFault.aspx
-- EXEC [FL_DeleteAllTempFault]
-- @TempFaultIDs = 1,2,3,
-- @DeletedRowCount = 0
-- =============================================
CREATE PROCEDURE [dbo].[FL_DeleteAllTempFault]
	-- Add the parameters for the stored procedure here
	@TempFaultIDs varchar(max),
	@DeletedRowCount INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from	
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @MainQuery As varchar(max)
	
	SET @MainQuery = 'DELETE FROM FL_TEMP_FAULT WHERE TempFaultID in (' + @TempFaultIDs + ')'
	Print(@MainQuery)
	EXEC(@MainQuery)
	
	SET @DeletedRowCount = @@ROWCOUNT
	Print(@DeletedRowCount)
END
GO
