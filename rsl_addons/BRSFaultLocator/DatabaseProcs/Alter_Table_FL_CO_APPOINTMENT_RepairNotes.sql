-- Author:		<Zeeshan Malik>
-- Create date: <22/04/2013>
-- Description:	<This script will create the new field in FL_CO_APPOINTMENT>
ALTER TABLE [dbo].[FL_CO_APPOINTMENT]	add [RepairNotes] [varchar](500) NULL
