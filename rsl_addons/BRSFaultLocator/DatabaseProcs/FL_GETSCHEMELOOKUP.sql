USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GETSCHEMELOOKUP]    Script Date: 03/04/2015 12:45:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO













ALTER PROCEDURE [dbo].[FL_GETSCHEMELOOKUP]
/* ===========================================================================
 '   NAME:          FL_GETSCHEMELOOKUP
 '   DATE CREATED:  23 Dec. 2008
 '   CREATED BY:    Waseem Hassan
 '   CREATED FOR:    Broadland Housing
 '   PURPOSE:        To get scheme G_SCHEME table which will be shown as lookup value in presentation pages
 '   IN:             No
 '   OUT:            No
 '   RETURN:         Nothing    
 '   VERSION:        1.0           
 '   COMMENTS:       
 '   MODIFIED ON:    
 '   MODIFIED BY:    
 '   REASON MODIFICATION: 
 '==============================================================================*/
AS
	SELECT SCHEMEID AS id,SCHEMENAME AS val
	FROM	P_SCHEME
	ORDER BY SCHEMENAME ASC










