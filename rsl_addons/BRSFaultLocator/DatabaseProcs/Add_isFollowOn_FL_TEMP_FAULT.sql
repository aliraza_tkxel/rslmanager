IF NOT EXISTS (SELECT * 
FROM information_schema.COLUMNS 
WHERE
TABLE_NAME = 'FL_TEMP_FAULT' 
AND COLUMN_NAME = 'isFollowon')
BEGIN
	ALTER TABLE FL_TEMP_FAULT
	ADD isFollowon BIT NOT NULL DEFAULT(0)
	PRINT 'Column isFollowon Added Sucessfully'
END
ELSE
BEGIN
	PRINT 'Column "isFollowon" Alreaady Added, no Changes required.'
END