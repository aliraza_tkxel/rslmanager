-- =============================================
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <20/02/2013>
-- Description:	<Add Fault Recall table to get recall data>
-- =============================================

USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[FL_FAULT_RECALL]    Script Date: 02/22/2013 12:34:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FL_FAULT_RECALL](
	[RecallID] [int] IDENTITY(1,1) NOT NULL,
	[FaultLogId] [int] NULL,
	[RecordedOn] [smalldatetime] NULL,
	[Notes] [nvarchar](100) NULL,
	[NoOfRecalls] [int] NULL,
 CONSTRAINT [PK_FL_FAULT_RECALL] PRIMARY KEY CLUSTERED 
(
	[RecallID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FL_FAULT_RECALL] ON
INSERT [dbo].[FL_FAULT_RECALL] ([RecallID], [FaultLogId], [RecordedOn], [Notes], [NoOfRecalls]) VALUES (1, 179, CAST(0xA1690000 AS SmallDateTime), N'This is the recall 1', 2)
INSERT [dbo].[FL_FAULT_RECALL] ([RecallID], [FaultLogId], [RecordedOn], [Notes], [NoOfRecalls]) VALUES (2, 194, CAST(0xA1680000 AS SmallDateTime), N'This is the recall 2', 3)
INSERT [dbo].[FL_FAULT_RECALL] ([RecallID], [FaultLogId], [RecordedOn], [Notes], [NoOfRecalls]) VALUES (3, 277, CAST(0xA1670000 AS SmallDateTime), N'This is the recall 3', 1)
INSERT [dbo].[FL_FAULT_RECALL] ([RecallID], [FaultLogId], [RecordedOn], [Notes], [NoOfRecalls]) VALUES (4, 308, CAST(0xA1660000 AS SmallDateTime), N'This is the recall 4', 2)
INSERT [dbo].[FL_FAULT_RECALL] ([RecallID], [FaultLogId], [RecordedOn], [Notes], [NoOfRecalls]) VALUES (5, 227, CAST(0xA1650000 AS SmallDateTime), N'This is the recall 5', 4)
SET IDENTITY_INSERT [dbo].[FL_FAULT_RECALL] OFF
