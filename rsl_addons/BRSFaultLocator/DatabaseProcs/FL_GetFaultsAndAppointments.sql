USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetFaultsAndAppointments]    Script Date: 09/27/2013 12:57:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
--EXEC FL_GetFaultsAndAppointments 
--  @customerId = 5,
--	@propertyId ="A010000018"
-- Author:		Bilal Ahmed lilla
-- Create date: 17/1/2013
-- Description:	<This store procedure shall provide the inform of other appointments that are already been arranged>
-- Web page: OtherAppointments.aspx

-- =============================================
ALTER PROCEDURE [dbo].[FL_GetFaultsAndAppointments]
	-- Add the parameters for the stored procedure here
	@customerId int ,
	@propertyId nvarchar(20)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	----------------------------------------------------------------------------------------
	----------------------------------Current Faults ---------------------------------------	
	Select FL_CO_APPOINTMENT.AppointmentID as AppointmentId
	,FL_FAULT_LOG.FaultLogID As FaultLogId 
	,FL_AREA.AreaName as AreaName
	,FL_FAULT.Description as Description
	,FL_FAULT_STATUS.Description as FaultStatus
	,convert(date, FL_FAULT_LOG.SubmitDate,103)as SubmitDate
	,convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108)+' '+convert(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103)  AppointmentDate
	,FL_FAULT.FaultId as FaultId
	,FL_FAULT_LOG.JobSheetNumber as JobSheetNumber		
	,FL_FAULT.Duration
 ,Case  
		WHEN FL_FAULT.Duration is null THEN 'N/A' 
		WHEN FL_FAULT.Duration <=1   THEN convert(varchar(10), FL_FAULT.Duration) + ' hr'	
		ELSE convert(varchar(10), FL_FAULT.Duration) + ' hrs' 
		END AS DurationString  
 ,G_Trade.Description as Trade  
 ,G_Trade.Description as TradeName   
 ,FL_FAULT_LOG.ProblemDays  
 ,FL_FAULT_LOG.RecuringProblem  
 ,FL_FAULT_LOG.CommunalProblem  
 ,Case days WHEN 1 then convert(char(6), dateadd(day,FL_Fault_Priority.ResponseTime,getdate()),0) else convert(char(6), dateadd(hour,FL_Fault_Priority.ResponseTime,getdate()),0) end AS DueDate  
 ,FL_FAULT_LOG.Notes  
 ,FL_FAULT.isGasSafe  
 ,FL_FAULT.isOftec  
 ,FL_FAULT_LOG.Quantity  
 ,FL_FAULT_LOG.Recharge,  
 '' As OrgId,  
 '' As IsAppointmentConfirmed,  
 0 As tempFaultId,  
 FL_FAULT_TRADE.FaultTradeId,  
 FL_FAULT_TRADE.TradeId ,
 FL_Fault_Priority.PriorityName ,
 (Case days   
  WHEN 1 then convert(char(12), dateadd(day,FL_Fault_Priority.ResponseTime,getdate()),103)   
  else convert(char(12), dateadd(hour,FL_Fault_Priority.ResponseTime,getdate()),103)   
  end ) AS CompleteDueDate  ,  
 (CASE WHEN FL_Fault_Priority.Days = 1 THEN  
 CONVERT(nvarchar(50), FL_Fault_Priority.ResponseTime) + ' days' ELSE  
 CONVERT(nvarchar(50), FL_Fault_Priority.ResponseTime) + ' hours' END) as Response      
   
 FROM FL_FAULT   
 INNER JOIN FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID  
 INNER JOIN FL_AREA ON FL_FAULT_LOG.AreaID = FL_AREA.AreaID
 INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID =FL_FAULT_LOG.StatusID  
 LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId  
 LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID              
 INNER JOIN FL_Fault_Priority ON FL_Fault.PriorityId  = FL_Fault_Priority.PriorityId  
 INNER JOIN FL_FAULT_TRADE ON FL_FAULT_TRADE.FaultTradeId = FL_FAULT_LOG.FaultTradeId  
 INNER JOIN G_Trade ON FL_FAULT_TRADE.TradeId = G_Trade.TradeId                
        
 where FL_FAULT_LOG.CustomerId = @customerId   
 AND FL_FAULT_LOG.PropertyId = @propertyId   
 AND FL_FAULT_STATUS.FaultStatusID != 17   
 AND FL_FAULT_STATUS.FaultStatusID != 13   
 Order by SubmitDate DESC   
        
 ----------------------------------------------------------------------------------------  
 ----------------------------------Completed Faults -------------------------------------   
 Select FL_CO_APPOINTMENT.AppointmentID, FL_FAULT_LOG.FaultLogID As FaultLogID,FL_Area.AreaName as ElementName,FL_FAULT.Description as FaultDetail, FL_FAULT_STATUS.Description as FaultStatus,  
  convert(varchar, FL_FAULT_LOG_HISTORY.LastActionDate,103)as SubmitDate,convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108)+' '+convert(varchar, FL_CO_APPOINTMENT.AppointmentDate, 103) as AppointmentDate, FL_FAULT_LOG_HISTORY.LastActionDate  
 from FL_FAULT   
   inner join FL_FAULT_LOG  on FL_FAULT.FaultID = FL_FAULT_LOG.FaultID 
   INNER JOIN FL_AREA ON FL_FAULT_LOG.AreaID = FL_AREA.AreaID 
   inner join FL_FAULT_STATUS on FL_FAULT_STATUS.FaultStatusID =FL_FAULT_LOG.StatusID  
   LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId  
   LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID  
   INNER JOIN (select * from FL_FAULT_LOG_HISTORY where PROPERTYID =@propertyId and FaultStatusID=17 ) as FL_FAULT_LOG_HISTORY ON FL_FAULT_LOG_HISTORY.FaultLogID = FL_FAULT_LOG.FaultLogID  
     
 where FL_FAULT_LOG.CustomerId = @customerId  
   AND FL_FAULT_LOG.PropertyId = @propertyId   
   AND FL_FAULT_STATUS.FaultStatusID = 17  
   AND FL_FAULT_LOG_HISTORY.LastActionDate >= DATEADD(mm, -3, GETDATE())    
   ORDER BY FL_FAULT_LOG_HISTORY.LastActionDate DESC  
        
 ----------------------------------------------------------------------------------------      
 -----------------------------------Gas servicing appointments.--------------------------  
 select AS_APPOINTMENTS.APPOINTMENTID     
   ,convert(char(5),cast(AS_APPOINTMENTS.APPOINTMENTSTARTTIME as datetime),108)+' '+convert(varchar, AS_APPOINTMENTS.APPOINTMENTDATE, 103) as AppointmentDate  
   , AS_Status.Title as STATUS,e.FIRSTNAME + ' ' + e.LASTNAME AS Operative   
  from AS_APPOINTMENTS  
   inner join AS_JOURNAL ON  
   AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID  
   INNER join AS_Status ON  
   AS_JOURNAL.STATUSID=   AS_Status.StatusId  
   INNER JOIN dbo.E__EMPLOYEE e ON e.EMPLOYEEID =   AS_APPOINTMENTS.ASSIGNEDTO
   where AS_JOURNAL.PROPERTYID = @propertyId  
   AND ISCURRENT = 1  
   
   
 ----------------------------------------------------------------------------------------      
 ----------------------------------- Planned Appointments -------------------------------  
     SELECT	JOURNAL_PMO.PMO as PMO
		,APPOINTMENT_AID.JSN as JSN
		,ISNULL(PLANNED_COMPONENT.COMPONENTNAME,'N/A') as Component
		,ISNULL(CASE 
			WHEN PLANNED_APPOINTMENTS.isMiscAppointment = 1  THEN
				MISCTRADE.Description 
			ELSE 
				COMPTRADE.Description 
		END,'N/A') as Trade		
		, ISNULL(CONVERT(VARCHAR(3),datename(weekday,PLANNED_APPOINTMENTS.APPOINTMENTDATE)) +' '+
		CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 106),'N/A') as AppointmentDate
		,PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME as AppointmentTime	
					
FROM	PLANNED_JOURNAL 
		INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID 
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
		
		LEFT JOIN PLANNED_COMPONENT_TRADE ON  PLANNED_APPOINTMENTS.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID 
		LEFT JOIN PLANNED_MISC_TRADE ON PLANNED_APPOINTMENTS.APPOINTMENTID = PLANNED_MISC_TRADE.AppointmentId 
		LEFT JOIN G_TRADE AS MISCTRADE ON  PLANNED_MISC_TRADE.TradeId = MISCTRADE.TradeId
		LEFT JOIN G_TRADE AS COMPTRADE ON  PLANNED_COMPONENT_TRADE.TradeId = COMPTRADE.TradeId
		 
		INNER JOIN (SELECT	PLANNED_JOURNAL.JOURNALID as JID , 'PMO'+CONVERT(VARCHAR,PLANNED_JOURNAL.JOURNALID) as PMO
					FROM	PLANNED_JOURNAL ) JOURNAL_PMO ON PLANNED_JOURNAL.JOURNALID = JOURNAL_PMO.JID 
		INNER JOIN (SELECT	PLANNED_APPOINTMENTS.APPOINTMENTID as AID , 'JSN' +RIGHT('00000'+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),5) as JSN
					FROM	PLANNED_APPOINTMENTS ) as APPOINTMENT_AID ON PLANNED_APPOINTMENTS.APPOINTMENTID = APPOINTMENT_AID.AID 
		
		INNER JOIN C_TENANCY ON PLANNED_APPOINTMENTS.TENANCYID = C_TENANCY.TENANCYID 
		INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID 
					
WHERE	PLANNED_STATUS.TITLE= 'Arranged' 
		AND YEAR(PLANNED_APPOINTMENTS.APPOINTMENTDATE)=YEAR(GETDATE())
		AND C_TENANCY.PROPERTYID = @propertyId
		AND C_CUSTOMERTENANCY.CUSTOMERID = @customerId
     
     
END  
  
  
