/*
   Monday, January 26, 201511:32:49 AM
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_FL_FAULT_LOG
	(
	FaultLogID int NOT NULL IDENTITY (1, 1),
	CustomerId int NULL,
	FaultID int NULL,
	FaultBasketID int NULL,
	SubmitDate datetime NULL,
	ORGID int NULL,
	StatusID int NOT NULL,
	IsReported bit NOT NULL,
	Quantity int NULL,
	ProblemDays int NULL,
	RecuringProblem bit NULL,
	CommunalProblem bit NULL,
	Notes varchar(4000) NULL,
	JobSheetNumber varchar(50) NULL,
	DueDate datetime NULL,
	IsSelected bit NULL,
	UserId int NULL,
	Recharge bit NULL,
	PROPERTYID nvarchar(20) NULL,
	ContractorID smallint NULL,
	FaultTradeID int NULL,
	CompletedDate datetime NULL,
	FollowOnFaultLogId int NULL,
	Duration decimal(9, 2) NULL,
	SchemeId int NULL,
	BlockId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_FL_FAULT_LOG SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_FL_FAULT_LOG ON
GO
IF EXISTS(SELECT * FROM dbo.FL_FAULT_LOG)
	 EXEC('INSERT INTO dbo.Tmp_FL_FAULT_LOG (FaultLogID, CustomerId, FaultID, FaultBasketID, SubmitDate, ORGID, StatusID, IsReported, Quantity, ProblemDays, RecuringProblem, CommunalProblem, Notes, JobSheetNumber, DueDate, IsSelected, UserId, Recharge, PROPERTYID, ContractorID, FaultTradeID, CompletedDate, FollowOnFaultLogId, Duration, SchemeId, BlockId)
		SELECT FaultLogID, CustomerId, FaultID, FaultBasketID, SubmitDate, ORGID, StatusID, IsReported, Quantity, ProblemDays, RecuringProblem, CommunalProblem, Notes, JobSheetNumber, DueDate, IsSelected, UserId, Recharge, PROPERTYID, ContractorID, FaultTradeID, CompletedDate, FollowOnFaultLogId, Duration, SchemeId, BlockId FROM dbo.FL_FAULT_LOG WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_FL_FAULT_LOG OFF
GO
ALTER TABLE dbo.FL_FAULT_CANCELLED
	DROP CONSTRAINT FL_FAULT_CANCELLED_FL_FAULT_LOG
GO
ALTER TABLE dbo.FL_FAULT_FOLLOWON
	DROP CONSTRAINT FL_FAULT_FOLLOWON_FL_FAULT_LOG
GO
ALTER TABLE dbo.FL_FAULT_JOBTIMESHEET
	DROP CONSTRAINT FL_FAULT_JOBTIMESHEET_FL_FAULT_LOG
GO
ALTER TABLE dbo.FL_FAULT_NOENTRY
	DROP CONSTRAINT FL_FAULT_NOENTRY_FL_FAULT_LOG
GO
ALTER TABLE dbo.FL_FAULT_PAUSED
	DROP CONSTRAINT FL_FAULT_PAUSED_FL_FAULT_LOG
GO
ALTER TABLE dbo.FL_FAULT_APPOINTMENT
	DROP CONSTRAINT FK_FL_FAULT_APPOINTMENT_FL_FAULT_LOG
GO
DROP TABLE dbo.FL_FAULT_LOG
GO
EXECUTE sp_rename N'dbo.Tmp_FL_FAULT_LOG', N'FL_FAULT_LOG', 'OBJECT' 
GO
ALTER TABLE dbo.FL_FAULT_LOG ADD CONSTRAINT
	PK_FL_FAULT_LOG PRIMARY KEY CLUSTERED 
	(
	FaultLogID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_LOG', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_LOG', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_LOG', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_APPOINTMENT ADD CONSTRAINT
	FK_FL_FAULT_APPOINTMENT_FL_FAULT_LOG FOREIGN KEY
	(
	FaultLogId
	) REFERENCES dbo.FL_FAULT_LOG
	(
	FaultLogID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FL_FAULT_APPOINTMENT SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_APPOINTMENT', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_APPOINTMENT', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_APPOINTMENT', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_PAUSED ADD CONSTRAINT
	FL_FAULT_PAUSED_FL_FAULT_LOG FOREIGN KEY
	(
	FaultLogId
	) REFERENCES dbo.FL_FAULT_LOG
	(
	FaultLogID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FL_FAULT_PAUSED SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_NOENTRY ADD CONSTRAINT
	FL_FAULT_NOENTRY_FL_FAULT_LOG FOREIGN KEY
	(
	FaultLogId
	) REFERENCES dbo.FL_FAULT_LOG
	(
	FaultLogID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FL_FAULT_NOENTRY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_NOENTRY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_NOENTRY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_NOENTRY', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_JOBTIMESHEET ADD CONSTRAINT
	FL_FAULT_JOBTIMESHEET_FL_FAULT_LOG FOREIGN KEY
	(
	FaultLogId
	) REFERENCES dbo.FL_FAULT_LOG
	(
	FaultLogID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FL_FAULT_JOBTIMESHEET SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_JOBTIMESHEET', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_JOBTIMESHEET', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_JOBTIMESHEET', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_FOLLOWON ADD CONSTRAINT
	FL_FAULT_FOLLOWON_FL_FAULT_LOG FOREIGN KEY
	(
	FaultLogId
	) REFERENCES dbo.FL_FAULT_LOG
	(
	FaultLogID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FL_FAULT_FOLLOWON SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_FOLLOWON', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_FOLLOWON', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_FOLLOWON', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.FL_FAULT_CANCELLED ADD CONSTRAINT
	FL_FAULT_CANCELLED_FL_FAULT_LOG FOREIGN KEY
	(
	FaultLogId
	) REFERENCES dbo.FL_FAULT_LOG
	(
	FaultLogID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.FL_FAULT_CANCELLED SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_CANCELLED', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_CANCELLED', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_CANCELLED', 'Object', 'CONTROL') as Contr_Per 