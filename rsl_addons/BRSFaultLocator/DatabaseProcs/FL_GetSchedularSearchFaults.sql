USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetSchedularSearchFaults]    Script Date: 10/04/2013 11:16:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









ALTER PROCEDURE [dbo].[FL_GetSchedularSearchFaults] 

/* ===========================================================================
 '   NAME:           FL_GetSchedularSearchFaults
--EXEC	[dbo].[FL_GetSchedularSearchFaults]
--		
-- Author:		<Ahmed Mehmood>
-- Create date: <12/02/2013>
-- Description:	<Get Schedular Search Results>
-- Web Page: SchedulersCalanderSearch.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
	    @patchId	int	=  NULL,
		@schemeId	int	=  NULL,	
		@tradeId	int = NULL,
		
		@jsn	varchar(50) = NULL,
		@operative	varchar(50) = NULL,
		@tenant		varchar(50) = NULL,    
		@address varchar(100) = NULL,
		
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JobSheetNumber',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int=0 output

	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),	        
	        @OrderClause  varchar(2000),
	        @mainSelectQuery varchar(5500),        
			@rowNumberQuery varchar(6000),
			@finalQuery varchar(6500),
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @SearchCriteria varchar(8000),

			--variables for paging
			@offset int,
			@limit int
			
			--Paging Formula
			SET @offset = 1+(@pageNumber-1) * @pageSize
			SET @limit = (@offset + @pageSize)-1

    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided
         
    SET @SearchCriteria = '' 
        
    IF NOT @patchId = 0
       SET @SearchCriteria = @SearchCriteria + CHAR(10) +
                             'PDR_DEVELOPMENT.PATCHID= '+ LTRIM(@patchId) + ' AND'  
    
    
     IF NOT @schemeId=0
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) +
                             'P__PROPERTY.SCHEMEID = '+ LTRIM(@schemeId) + ' AND'  
    
    
    IF NOT @tradeId=0
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'FL_FAULT_TRADE.TradeId = '+ LTRIM(@tradeId) + ' AND'  
    
    IF  NOT @jsn=''
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             ' FL_FAULT_LOG.JobSheetNumber LIKE ''%'+LTRIM(@jsn) +'%'' AND'  
                             
    IF NOT @operative=''
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'Operatives.Name LIKE ''%'+ LTRIM(@operative) +'%'' AND'
                                             
                             
    IF NOT @tenant=''
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             'C__CUSTOMER.FIRSTNAME LIKE ''%'+ LTRIM(@tenant) +'%'' OR'

                  
    IF NOT @tenant=''
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'C__CUSTOMER.LASTNAME LIKE ''%'+ LTRIM(@tenant) +'%'' AND'
                             
                             
    IF NOT @address=''
       SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                             '(FREETEXT(P__PROPERTY.HouseNumber ,'''+@address+''')  OR FREETEXT(P__PROPERTY.ADDRESS1, '''+@address+''')  OR FREETEXT(P__PROPERTY.ADDRESS2, '''+@address+''')  OR FREETEXT(P__PROPERTY.ADDRESS3, '''+@address+'''))AND'    
                             
               
           
    -- End building SearchCriteria clause   
    --========================================================================================

	        
	        
    --========================================================================================	        
    -- Begin building SELECT clause
      SET @SelectClause = 'SELECT top ('+convert(varchar(10),@limit)+')' +                      
                        CHAR(10) + 'FL_FAULT_LOG.JobSheetNumber JobSheetNumber,ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address,' +
                        CHAR(10) + 'FL_AREA.AreaName + '' > ''+ FL_FAULT.Description as Location' +
                        CHAR(10) + ',G_TRADE.Description Trade 
									,CONVERT(NVARCHAR(5), CAST([Time] AS DATETIME2), 108) + '' '' + DATENAME(WEEKDAY,AppointmentDate) + '' '' + DATENAME(DAY,AppointmentDate) + '' '' + DATENAME(MONTH,AppointmentDate) + '' '' + DATENAME(YEAR,AppointmentDate) AppointmentStartDateTime' +
                        CHAR(10) + ',FL_FAULT_STATUS.Description Status' +
                        CHAR(10) + ',FL_FAULT_LOG.FaultLogID FaultLogID' +
                        CHAR(10) + ',P__PROPERTY.HOUSENUMBER HouseNumber' +
                        CHAR(10) + ',P__PROPERTY.ADDRESS1 PrimaryAddress' +
                        CHAR(10) + ',FL_AREA.AreaName FaultArea' +
                        CHAR(10) + ',FL_FAULT.Description AreaDescription' +
                       	CHAR(10) + ',FL_CO_APPOINTMENT.AppointmentDate +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.Time as datetime),108) AppointmentSort' +
                        CHAR(10) + ',CONVERT(NVARCHAR(5), CAST([EndTime] AS DATETIME2), 108) + '' '' + DATENAME(WEEKDAY,COALESCE(AppointmentEndDate, AppointmentDate)) + '' '' + DATENAME(DAY,COALESCE(AppointmentEndDate, AppointmentDate)) + '' '' + DATENAME(MONTH,COALESCE(AppointmentEndDate, AppointmentDate)) + '' '' + DATENAME(YEAR,COALESCE(AppointmentEndDate, AppointmentDate)) AppointmentEndDateTime' +
                        CHAR(10) + ',COALESCE(AppointmentEndDate, AppointmentDate) +'' ''+convert(char(5),cast(FL_CO_APPOINTMENT.EndTime as datetime),108) AppointmentEndSort'
    -- End building SELECT clause
    --========================================================================================    
       
    
    --========================================================================================    
    -- Begin building FROM clause
                    
                    
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + 'FL_FAULT_LOG ' +
                      CHAR(10) + 'INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogId = FL_FAULT_APPOINTMENT.FaultLogId' +
                      CHAR(10) + 'INNER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID' +
                      CHAR(10) + 'INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID' +
                      CHAR(10) + 'INNER JOIN FL_FAULT ON  FL_FAULT_LOG.FaultID= FL_FAULT.FaultID' +
                      CHAR(10) + 'INNER JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID' +
                      CHAR(10) + 'INNER JOIN FL_FAULT_TRADE ON FL_FAULT_LOG.FaultTradeId = FL_FAULT_TRADE.FaultTradeId' +
                      CHAR(10) + 'INNER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId' +
                      CHAR(10) + 'INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID' +
                      CHAR(10) + 'INNER JOIN PDR_DEVELOPMENT ON P__PROPERTY.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID' + 
                      CHAR(10) + 'INNER JOIN E_PATCH ON PDR_DEVELOPMENT.PATCHID = E_PATCH.PATCHID' + 
                      CHAR(10) + 'LEFT JOIN P_SCHEME ON P__PROPERTY.SCHEMEID = P_SCHEME.SCHEMEID' +
                      CHAR(10) + 'LEFT OUTER JOIN (Select DISTINCT E__EMPLOYEE.EMPLOYEEID EmpId, E__EMPLOYEE.FIRSTNAME+'' ''+E__EMPLOYEE.LASTNAME as Name 
						FROM dbo.E__EMPLOYEE
						INNER JOIN dbo.AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
						INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
						INNER JOIN dbo.E_JOBDETAILS  ON E__EMPLOYEE.EMPLOYEEID =E_JOBDETAILS.EMPLOYEEID  
						WHERE P_INSPECTIONTYPE.[Description]  =''Reactive'' AND E_JOBDETAILS.ACTIVE =1)as Operatives on Operatives.EmpId=FL_CO_APPOINTMENT.OperativeID INNER JOIN' +
					CHAR(10) + 'C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID'
						
						
                             
    
    -- End building FROM clause
 	
	--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' cast(SUBSTRING(HouseNumber, 1,case when patindex(''%[^0-9]%'',HouseNumber) > 0 then patindex(''%[^0-9]%'',HouseNumber) - 1 else LEN(HouseNumber) end) as int)' 	
			
		END
		
		IF(@sortColumn = 'Location')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultArea' 		
		END
		
		IF(@sortColumn = 'AppointmentStartDateTime')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'AppointmentSort' 		
		END
		
		IF(@sortColumn = 'AppointmentEndDateTime')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'AppointmentEndSort' 		
		END
		
		IF(@sortColumn = 'JobSheetNumber')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FaultLogID' 		
		END
		
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		
		
		-- End building OrderBy clause
		--========================================================================================	
                             
    --========================================================================================
    -- Begin building WHERE clause
    
    -- This Where clause contains subquery to exclude already displayed records
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' + 
                     -- Search Based Criteria added if supplied by user
                        CHAR(10) + CHAR(10) + @SearchCriteria +                
                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'
                        
    -- End building WHERE clause
   
 	--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		--Set @rowNumberQuery ='  SELECT *, row_number() over ( ORDER BY (SELECT NULL)) as row	
		--						FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		-- End building the row number query
		--========================================================================================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									

		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================















