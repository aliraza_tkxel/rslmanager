-- =============================================
-- Author: <Ahmed Mehmood>
-- Create date: <29/01/2013>
-- Description:	< Add Duration Field in FL_FAULT Table >
-- Web Page: FaultBasket.aspx
-- =============================================

ALTER TABLE FL_FAULT
ADD duration float