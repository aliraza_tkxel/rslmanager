-- Stored Procedure

-- =============================================  
-- EXEC FL_GetAppointmentsCalendarInfo @startDate='31/10/2013',@endDate='30/11/2013' 
-- Author:  Aqib Javed  
-- Create date: <29/01/2013>  
-- Description: <Provided information of Scheduled Appointments of Engineers on Weekly basis>  
-- Webpage : SchedulingCalendar.aspx  

-- =============================================  
ALTER PROCEDURE [dbo].[FL_GetAppointmentsCalendarInfo]
    @startDate VARCHAR(25) ,
    @endDate VARCHAR(25) ,
    @scheme INT = -1
AS 
    DECLARE @InsertClause VARCHAR(1000)

    DECLARE @SelectClause VARCHAR(1000)

    DECLARE @FromClause VARCHAR(800)

    DECLARE @WhereClause VARCHAR(800)

    DECLARE @MainQuery VARCHAR(MAX)

    BEGIN


        WITH    CTE_Appointments ( AppointmentDate, OperativeID, [Time], NAME, MOBILE, CustAddress, POSTCODE, FaultStatus, AppointmentID, EndTime, StartTimeInSec, EndTimeInSec, TimeToOrderAppointments, TenancyId, IsCalendarAppointment, AppointmentType ,AppointmentEndDate)
                  AS ( 

	-- ==========================================================================================    
	--	FAULT APPOINTMENTS
	-- ========================================================================================== 

    SELECT DISTINCT
	CONVERT(VARCHAR(20), APPOINTMENTDATE, 103)									AS AppointmentDate
	,FL_CO_APPOINTMENT.OperativeID												AS OperativeID
	,FL_CO_APPOINTMENT.[Time]													AS [Time]
	,'Test Name'																AS Name
	,'Test Mobile'																AS MOBILE
	,Case 
	When FL_FAULT_LOG.PROPERTYID <> '' THEN
	ISNULL(P__PROPERTY.HouseNumber, '') + ' '
	+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
	+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
	+ ISNULL(P__PROPERTY.POSTCODE, '')
	WHEN FL_FAULT_LOG.BLOCKID > 0 THEN 
	ISNULL(P_BLOCK.ADDRESS1,'')
	+ ISNULL(', '+P_BLOCK.TOWNCITY, '') 
	+ ISNULL(', '+P_BLOCK.POSTCODE, '') 
	When FL_FAULT_LOG.SCHEMEID > 0 THEN
	ISNULL(P_SCHEME.SCHEMENAME,'')
	+ ISNULL(', '+PDR_DEVELOPMENT.TOWN, '') 
	+ ISNULL(', '+PDR_DEVELOPMENT.COUNTY, '')
	+ ISNULL(', '+PDR_DEVELOPMENT.PostCode, '')
	END																			AS CustAddress
	,'Test PostCode'															AS POSTCODE
	,FL_FAULT_STATUS.[Description]												AS FaultStatus
	,FL_CO_APPOINTMENT.AppointmentID											AS AppointmentID
	,FL_CO_APPOINTMENT.EndTime													AS EndTime
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103)
	+ ' ' + [Time], 103))														AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103)
	+ ' ' + EndTime, 103))														AS EndTimeInSec
	,CONVERT(TIME, [TIME])														AS TimeToOrderAppointments
	,C_TENANCY.TENANCYID														AS TenancyId
	,FL_CO_APPOINTMENT.isCalendarAppointment									AS IsCalendarAppointment
	,Case 
	When FL_FAULT_LOG.PROPERTYID IS NULL THEN	'SbFault'	
	Else 'Fault'
	End																AS AppointmentType
	,CONVERT(VARCHAR(20), COALESCE(AppointmentEndDate, AppointmentDate), 103)	AS AppointmentEndDate
FROM
	FL_CO_APPOINTMENT
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId
		INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
		Left JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		Left JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
		LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
		LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID  
		LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
WHERE
	(C_TENANCY.ENDDATE IS NULL
		OR C_TENANCY.ENDDATE > GETDATE()
	)
	AND FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103)
	AND
	CONVERT(DATE, @endDate, 103)
	AND FL_FAULT_STATUS.[Description] != 'Cancelled'
	AND (P__PROPERTY.SCHEMEID = @scheme
		OR @scheme = -1
		OR @scheme = 0
	) 
	AND(FL_FAULT_LOG.PROPERTYID <> '' OR FL_FAULT_LOG.SCHEMEID  IS NOT NULL OR FL_FAULT_LOG.BlockId > 0)

	UNION ALL 

	-- ==========================================================================================    
	--	PLANNED APPOINTMENTS
	-- ========================================================================================== 

	SELECT DISTINCT
	CONVERT(VARCHAR(20), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)		AS AppointmentDate
	,PLANNED_APPOINTMENTS.ASSIGNEDTO									AS OperativeID
	,PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME							AS Time
	,'Test Name'														AS Name
	,'Test Mobile'														AS MOBILE
	,ISNULL(P__PROPERTY.HouseNumber, '') + ' '
	+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
	+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
	+ ISNULL(P__PROPERTY.POSTCODE, '')									AS CustAddress
	,'Test PostCode'													AS POSTCODE
	,PLANNED_STATUS.TITLE												AS FaultStatus
	,PLANNED_APPOINTMENTS.AppointmentID									AS AppointmentID
	,PLANNED_APPOINTMENTS.APPOINTMENTENDTIME							AS EndTime
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)
	+ ' '
	+ PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))					AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)
	+ ' '
	+ PLANNED_APPOINTMENTS.APPOINTMENTENDTIME, 103))					AS EndTimeInSec
	,CONVERT(TIME, PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME)			AS TimeToOrderAppointments
	,C_TENANCY.TENANCYID												AS TenancyId
	,0																	AS IsCalendarAppointment
	,'Planned'															AS AppointmentType
	,CONVERT(VARCHAR(20), PLANNED_APPOINTMENTS.APPOINTMENTENDDATE, 103)	AS AppointmentEndDate
FROM
	PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
WHERE
	(C_TENANCY.ENDDATE IS NULL
		OR C_TENANCY.ENDDATE > GETDATE())
	AND (PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
		OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
		OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= CONVERT(DATE, @startDate, 103)
			AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103)))
	AND (P__PROPERTY.SCHEMEID = @scheme
		OR @scheme = -1
		OR @scheme = 0
	)
AND APPOINTMENTSTATUS <> 'Cancelled' AND ISPENDING = 0
	UNION ALL 

	-- ==========================================================================================    
	--	PDR APPOINTMENTS
	-- ==========================================================================================   

	---Get PDR appointment e.g M&E Servicing, Maintenance Servicing
	SELECT DISTINCT 
CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)		AS AppointmentDate
	,PDR_APPOINTMENTS.ASSIGNEDTO										AS OperativeID
	,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME								AS [Time]
	,'Test Name'														AS Name
	,'Test Mobile'														AS MOBILE
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
		+ ISNULL(P__PROPERTY.ADDRESS1, '') 
		+ ISNULL(', '+P__PROPERTY.TOWNCITY, '') 
		+ ISNULL( ' '+P__PROPERTY.POSTCODE, '')
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		ISNULL(P_SCHEME.SCHEMENAME , '') 
		+ ISNULL( ' '+P_SCHEME.SCHEMECODE, '')
	WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN
		ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
		+ ISNULL(P_BLOCK.ADDRESS1 , '') 
		+ ISNULL(', '+P_BLOCK.TOWNCITY, '') 
		+ ISNULL( ' '+P_BLOCK.POSTCODE, '')
	END 							AS CustAddress
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		ISNULL(P__PROPERTY.POSTCODE, '')
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		ISNULL(P_SCHEME.SCHEMECODE,'') 
	WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN
		ISNULL(P_BLOCK.POSTCODE, '')
	END 		AS POSTCODE
	, CASE
    WHEN PDR_STATUS.TITLE='Arranged' THEN 'Appointment Arranged'
    WHEN PDR_STATUS.TITLE='Completed' THEN 'Complete'
     ELSE  PDR_STATUS.TITLE	END											AS FaultStatus
	,PDR_APPOINTMENTS.APPOINTMENTID										AS AppointmentID
	,PDR_APPOINTMENTS.APPOINTMENTENDTIME								AS EndTime
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
	+ ' '
	+ PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
	+ ' '
	+ PDR_APPOINTMENTS.APPOINTMENTENDTIME, 103))						AS EndTimeInSec
	,CONVERT(TIME, PDR_APPOINTMENTS.APPOINTMENTSTARTTIME)			    AS TimeToOrderAppointments
	,C_TENANCY.TENANCYID												AS TenancyId
	,0																	AS IsCalendarAppointment
	,PDR_MSATType.MSATTypeName											AS AppointmentType
	,CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103)	AS AppointmentEndDate

   FROM PDR_APPOINTMENTS
   INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
   INNER JOIN pdr_status ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID AND PDR_STATUS.TITLE <>'Cancelled'
   INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
   LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
   LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
   LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
   INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
   LEFT JOIN p_status ON p__property.[status] = p_status.statusid


   LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid
   AND (dbo.c_tenancy.enddate IS NULL
        OR dbo.c_tenancy.enddate > Getdate())
   LEFT JOIN c_customer_names_grouped CG ON CG.i = dbo.c_tenancy.tenancyid
   AND CG.id IN
     (SELECT Max(id) ID
      FROM c_customer_names_grouped
      GROUP BY i)
   WHERE PDR_STATUS.TITLE != 'Cancelled' AND pdr_appointments.appointmentstartdate BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR pdr_appointments.appointmentenddate BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR (pdr_appointments.appointmentstartdate <= CONVERT(DATE, @startDate, 103)
         AND pdr_appointments.appointmentenddate >= CONVERT(DATE, @endDate, 103))

)
SELECT
	AppointmentDate
	,OperativeID
	,[Time]
	,NAME
	,MOBILE
	,CustAddress
	,POSTCODE
	,FaultStatus
	,AppointmentID
	,EndTime
	,StartTimeInSec
	,EndTimeInSec
	,TimeToOrderAppointments	TenancyId
	,IsCalendarAppointment
	,AppointmentType
	,AppointmentEndDate
FROM
	CTE_Appointments
ORDER BY	APPOINTMENTDATE
			,TimeToOrderAppointments	ASC

--------------------------------------------------------------------------------------------------------------------------  

	-- ==========================================================================================    
	--	FAULT APPOINTMENTS TEMP TABLE
	-- ========================================================================================== 


--Creating Temp Table for JSN against Appointment  
CREATE TABLE #tblAppointmentFaults(AppointmentId INT,
FaultLogId INT,
JobSheetNumber NVARCHAR(50),
FaultDetail NVARCHAR(1000),
AppointmentType NVARCHAR(50))
SET @InsertClause = 'INSERT INTO #tblAppointmentFaults(JobSheetNumber,AppointmentId,FaultLogId,FaultDetail,AppointmentType)'

SET @SelectClause = 'Select distinct FL_FAULT_LOG.JobSheetNumber,  
						FL_FAULT_APPOINTMENT.AppointmentID,  
						FL_FAULT_APPOINTMENT.FaultLogId
						,FL_FAULT.[Description]
						,Case 
						When FL_FAULT_LOG.PROPERTYID IS NULL And FL_FAULT_LOG.SchemeId IS NOT NULL THEN	''SbFault''	
						Else ''Fault'' END as AppointmentType '

SET @FromClause = 'FROM  FL_CO_APPOINTMENT   
						INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId   
						INNER JOIN FL_FAULT_LOG  ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID  
						INNER JOIN FL_FAULT  ON FL_FAULT_LOG.FaultID  = FL_FAULT.FaultID

						'

SET @WhereClause = ' Where FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date,'''
+ @startDate + ''',103) and Convert(date,''' + @endDate+ ''',103)'
SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10)
+ @FromClause + CHAR(10) + @WhereClause

--print @InsertClause+Char(10)  
--print @SelectClause+Char(10)  
--print @FromClause+Char(10)  
--print @WhereClause         
PRINT @MainQuery
EXEC (@MainQuery)

PRINT 'done fault'

	-- ==========================================================================================    
	--	PLANNED APPOINTMENTS TEMP TABLE
	-- ========================================================================================== 


SET @SelectClause = 'Select distinct ''JSN'' +RIGHT(''00000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),5) as JobSheetNumber  
				,PLANNED_APPOINTMENTS.AppointmentID
				,PLANNED_APPOINTMENTS.AppointmentID as FaultLogId
				,PLANNED_APPOINTMENTS.APPOINTMENTNOTES as Description
				,''Planned'' as AppointmentType '

SET @FromClause = 'FROM	PLANNED_APPOINTMENTS '

SET @WhereClause = ' Where 
        PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(date,''' + @startDate + ''',103) and Convert(date,''' + @endDate + ''',103)
        OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE,''' + @startDate + ''', 103) AND CONVERT(DATE,''' + @endDate + ''', 103)
		OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= CONVERT(DATE,''' + @startDate + ''', 103) AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE,''' + @endDate + ''', 103))'
SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10)
+ @FromClause + CHAR(10) + @WhereClause

--print @InsertClause+Char(10)  
--print @SelectClause+Char(10)  
--print @FromClause+Char(10)  
--print @WhereClause         
PRINT @MainQuery
EXEC (@MainQuery)

	-- ==========================================================================================    
	--	PDR APPOINTMENTS TEMP TABLE
	-- ========================================================================================== 

SET @SelectClause = 'Select distinct case when PDR_MSATType.MSATTypeName Like ''%Void%'' then ''JSV'' +RIGHT(''00000''+ CONVERT(VARCHAR,PDR_JOURNAL.JOURNALID),5) 
 ELSE ''JSN'' +RIGHT(''00000''+ CONVERT(VARCHAR,PDR_APPOINTMENTS.APPOINTMENTID),5) END as JobSheetNumber  
				,PDR_APPOINTMENTS.APPOINTMENTID
				,PDR_APPOINTMENTS.APPOINTMENTID as FaultLogId
				,PDR_APPOINTMENTS.APPOINTMENTNOTES as Description
				,PDR_MSATType.MSATTypeName as AppointmentType '

SET @FromClause = 'FROM	PDR_APPOINTMENTS 
 INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
   INNER JOIN pdr_status ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID AND PDR_STATUS.TITLE <>''Cancelled''
   INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
   LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
   LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
   LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
   INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId

'

SET @WhereClause = ' Where 
        PDR_APPOINTMENTS.appointmentstartdate BETWEEN CONVERT(date,''' + @startDate + ''',103) and Convert(date,''' + @endDate + ''',103)
        OR PDR_APPOINTMENTS.appointmentenddate BETWEEN CONVERT(DATE,''' + @startDate + ''', 103) AND CONVERT(DATE,''' + @endDate + ''', 103)
		OR (PDR_APPOINTMENTS.appointmentstartdate <= CONVERT(DATE,''' + @startDate + ''', 103) AND PDR_APPOINTMENTS.appointmentenddate >= CONVERT(DATE,''' + @endDate + ''', 103))'
SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10)
+ @FromClause + CHAR(10) + @WhereClause

PRINT @MainQuery
EXEC (@MainQuery)








SELECT
	*
FROM
	#tblAppointmentFaults

DROP TABLE #tblAppointmentFaults

END
GO