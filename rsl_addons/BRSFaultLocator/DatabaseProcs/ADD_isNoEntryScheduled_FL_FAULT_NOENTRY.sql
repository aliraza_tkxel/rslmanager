IF NOT EXISTS (SELECT * 
FROM information_schema.COLUMNS 
WHERE
TABLE_NAME = 'FL_FAULT_NOENTRY' 
AND COLUMN_NAME = 'isNoEntryScheduled')
BEGIN
	ALTER TABLE FL_FAULT_NOENTRY
	ADD isNoEntryScheduled BIT NOT NULL DEFAULT(0)
	PRINT 'Column "isNoEntryScheduled" Added Sucessfully.'
END
ELSE
BEGIN
	PRINT 'Column "isNoEntryScheduled" Alreaady Added, no Changes required.'
END