
USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- EXEC FL_GetFaultInfo
--	@tempFaultId =1212
-- Author:		<Ahmed Mehmood>
-- Create date: <2/04/2015>
-- Description:	<Get Fault information>
-- Web Page: FaultBasket.aspx
-- =============================================

CREATE PROCEDURE [dbo].[FL_GetFaultInfo]

	@tempFaultId	INT	

AS
BEGIN


SELECT	
		FF.FAULTID AS FaultId
		,FF.Description AS Description
		,FTF.Notes AS WorksRequired
		,ISNULL(FF.NetCost,0) NetCost
		,ISNULL(FF.Vat,0) Vat
		,ISNULL(FF.Gross,0) Gross
		,ISNULL(FF.VatRateId ,-1 ) VatRateId
		,FTF.PropertyId
FROM	FL_TEMP_FAULT AS FTF
		INNER JOIN FL_FAULT AS FF ON FTF.FAULTID = FF.FAULTID
WHERE	FTF.tempfaultId = @tempFaultId


END
