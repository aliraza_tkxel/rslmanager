IF NOT EXISTS (SELECT * 
FROM information_schema.COLUMNS 
WHERE
TABLE_NAME = 'FL_TEMP_FAULT' 
AND COLUMN_NAME = 'FollowOnFaultLogID')
BEGIN
	ALTER TABLE FL_TEMP_FAULT
	ADD FollowOnFaultLogID INT NULL
	PRINT 'Column FollowOnFaultLogID Added Sucessfully'
END
ELSE
BEGIN
	PRINT 'Column "FollowOnFaultLogID" Alreaady Added, no Changes required.'
END