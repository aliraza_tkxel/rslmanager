
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,22 Jan,2013>
-- Description:	<Description,,This script will create the full text search index for fault description>
-- Web Page: SearchFault.aspx

CREATE FULLTEXT CATALOG FL_FAULTFTS
WITH ACCENT_SENSITIVITY = OFF


CREATE FULLTEXT INDEX ON FL_FAULT
(Description )
KEY INDEX PK_FL_FAULT
ON FL_FAULTFTS

ALTER FULLTEXT INDEX ON FL_FAULT SET CHANGE_TRACKING AUTO;