-- Author:		<Aqib Javed>
-- Create date: <20/02/2013>
-- Description:	<This script will create the new field in FL_CO_APPOINTMENT>
ALTER TABLE [dbo].[FL_CO_APPOINTMENT]	add [EndTime] [varchar](50) NULL
