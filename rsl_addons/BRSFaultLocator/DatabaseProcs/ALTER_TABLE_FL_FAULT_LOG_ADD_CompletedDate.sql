-- =============================================
-- Author: <Aamir Waheed>
-- Create date: <24/09/2014>
-- Description:	< Add CompletedDate Field in FL_FAULT_LOG Table >
-- Web Page: JobSheetSummary.aspx
-- =============================================


BEGIN TRANSACTION
BEGIN TRY

DECLARE @TableName NVARCHAR(128)
DECLARE @ColumnName NVARCHAR(128)

SET @TableName = 'FL_FAULT_LOG'
SET @ColumnName = 'CompletedDate'
	
IF NOT EXISTS (SELECT * FROM sys.columns 
        WHERE [name] = @ColumnName AND [object_id] = OBJECT_ID(@TableName))
BEGIN	
DECLARE @AlterTableSQL NVARCHAR(4000) = ' ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' datetime'
EXEC sys.sp_executesql @AlterTableSQL

PRINT 'This column ''CompletedDate'' has been added successfully in table FL_FAULT_LOG.' + CHAR(10) + CHAR(13)

END
ELSE
	PRINT 'This column ''CompletedDate'' already exists in table FL_FAULT_LOG. (No need to add again)' + CHAR(10) + CHAR(13)

END TRY BEGIN CATCH
IF @@TRANCOUNT > 0 BEGIN

ROLLBACK TRANSACTION;

END

DECLARE @ErrorMessage nvarchar(4000);
DECLARE @ErrorSeverity int;
DECLARE @ErrorState int;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
@ErrorSeverity, -- Severity.
@ErrorState -- State.
);
END CATCH
  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 