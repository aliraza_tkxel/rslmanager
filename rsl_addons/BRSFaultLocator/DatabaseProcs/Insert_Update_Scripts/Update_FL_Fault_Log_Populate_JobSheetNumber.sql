/* ==============================================================================    
    
    Author: Aamir Waheed
    Creation Date:  29/06/2015
    Description: 1- Update FL_FAULT_LOG to populate missing  JobSheetNumber for
					few old records.

================================================================================= */

BEGIN TRANSACTION
BEGIN TRY

	UPDATE FL_FAULT_LOG
		SET JobSheetNumber = 'JS' + CONVERT(NVARCHAR,FaultLogID)
	WHERE JobSheetNumber IS NULL
	
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
      BEGIN 
          PRINT 'Some thing went wrong with this transaction' 

          ROLLBACK TRANSACTION
      END 

    -- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage
END CATCH