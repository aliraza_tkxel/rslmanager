/*

	Author: Aamir Waheed
	Date: 28/07/2015
	Discription: Update fault job sheet status
	This script is generated for a special case, to update job sheets status having complete appointment status.

*/


BEGIN TRANSACTION

DECLARE @JobCompleteStatusId INT

SELECT @JobCompleteStatusId = FS.FaultStatusID
FROM FL_FAULT_STATUS FS
WHERE FS.Description = 'Complete'

DECLARE @FaultLog AS TABLE(FaultLogId INT, OperativeId INT)

UPDATE FL
	SET FL.StatusID = @JobCompleteStatusId
		,FL.CompletedDate = COALESCE(FL.CompletedDate, A.RepairCompletionDateTime)
	OUTPUT INSERTED.FaultLogId, A.OperativeID INTO @FaultLog
FROM FL_CO_APPOINTMENT A
INNER JOIN FL_FAULT_APPOINTMENT FA ON A.AppointmentID = FA.AppointmentId AND A.AppointmentStatus = 'Complete'
INNER JOIN FL_FAULT_LOG FL ON FA.FaultLogId = FL.FaultLogID
INNER JOIN FL_FAULT_STATUS FS ON FL.StatusID = FS.FaultStatusID AND FS.Description NOT IN ('Complete', 'Cancelled')

INSERT INTO FL_FAULT_LOG_HISTORY
           (JournalID
           ,FaultStatusID           
           ,LastActionDate
           ,LastActionUserID
           ,FaultLogID
           ,ORGID
           ,Notes
           ,PROPERTYID
           ,ContractorID
           ,Duration
           ,SchemeID
           ,BlockID)
SELECT FJ.JournalID
      ,FL.StatusID
      ,FL.CompletedDate
      ,F.OperativeId
      ,FL.FaultLogID
      ,FL.ORGID
      ,Notes
      ,FL.PROPERTYID
      ,ContractorID
      ,Duration
      ,FL.SchemeId
      ,FL.BlockId
FROM FL_FAULT_LOG FL
INNER JOIN @FaultLog F ON FL.FaultLogID = F.FaultLogId
INNER JOIN FL_FAULT_JOURNAL FJ ON FL.FaultLogID = FJ.FaultLogID

COMMIT TRANSACTION