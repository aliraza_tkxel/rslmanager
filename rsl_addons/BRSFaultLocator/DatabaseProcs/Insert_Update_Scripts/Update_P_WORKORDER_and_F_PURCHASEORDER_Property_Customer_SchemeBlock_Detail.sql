/* ==============================================================================    
    
    Author: Aamir Waheed
    Creation Date:  19/06/2015
    Description: 1- Update P_WORKORDER to populate missing (PropertyId and CustomerId) or (BlockId and SchemeId and development)
					for the works assigned to contractor from reactire repair module.
				 2- Update F_PURCHASEORDERto pupulate missing BlockId and DevelopmentId

================================================================================= */

BEGIN TRANSACTION
BEGIN TRY
	
	DECLARE @Today DATE = GETDATE()	
	
	UPDATE WO
		SET 
			  WO.PROPERTYID = COALESCE(NULLIF(WO.PropertyID,'0'), FL.PROPERTYID)
			, WO.CUSTOMERID = COALESCE(NULLIF(WO.CUSTOMERID, 0),FL.CustomerId)
			, WO.TENANCYID = COALESCE(NULLIF(WO.TENANCYID, 0), Tenancy.TenancyID)
			, WO.DEVELOPMENTID = COALESCE(NULLIF(WO.DEVELOPMENTID, 0), S.DEVELOPMENTID)
			, WO.BLOCKID = COALESCE(NULLIF(WO.BLOCKID, 0), FL.BlockId)
			, WO.SchemeId = COALESCE(NULLIF(WO.SchemeId, 0), FL.SchemeId)
	FROM P_WORKORDER WO		
		INNER JOIN FL_CONTRACTOR_WORK FCW ON WO.ORDERID = FCW.PurchaseORDERID
		INNER JOIN FL_FAULT_JOURNAL FJ ON FCW.JournalId = FJ.JournalID
		INNER JOIN FL_FAULT_LOG FL ON FJ.FaultLogID = FL.FaultLogID
		LEFT JOIN P_SCHEME S ON FL.SchemeId	= S.SCHEMEID
		OUTER APPLY ( SELECT MAX(T.TenancyID) TenancyID FROM C_TENANCY T
						WHERE T.PROPERTYID = FL.PROPERTYID AND (FL.SubmitDate BETWEEN T.STARTDATE AND ISNULL(T.ENDDATE, @Today)) ) Tenancy
	
	UPDATE PO					
		SET 
			PO.BLOCKID = COALESCE(NULLIF(PO.BLOCKID, 0), WO.BLOCKID)
			, PO.DEVELOPMENTID = COALESCE(NULLIF(PO.DEVELOPMENTID, 0), WO.DEVELOPMENTID)	
	FROM F_PURCHASEORDER PO
		INNER JOIN P_WORKORDER WO ON PO.ORDERID = WO.ORDERID
		/* Just a filter to get only Fault PO(s). */
		INNER JOIN FL_CONTRACTOR_WORK FCW ON PO.ORDERID = FCW.PurchaseORDERID
	
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
      BEGIN 
          PRINT 'Some thing went wrong with this transaction' 

          ROLLBACK TRANSACTION
      END 

    -- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage
END CATCH