/* ==============================================================================    
    
    Author: Aamir Waheed
    Creation Date:  29/06/2015
    Description: 1- Update C_JOURNAL to populate missing (PropertyId and CustomerId)
					for the works assigned to contractor from reactire repair module.

================================================================================= */

BEGIN TRANSACTION
BEGIN TRY
	
	DECLARE @Today DATE = GETDATE()	
	
	UPDATE CJ
		SET
			CJ.CUSTOMERID = COALESCE(NULLIF(CJ.CUSTOMERID, 0),FL.CustomerId)
			,CJ.PROPERTYID = COALESCE(NULLIF(CJ.PROPERTYID,'0'), FL.PROPERTYID)
			,CJ.TENANCYID = COALESCE(NULLIF(CJ.TENANCYID, 0), Tenancy.TenancyID)
	
	FROM C_JOURNAL CJ
	INNER JOIN P_WOTOREPAIR WOTR ON CJ.JOURNALID = WOTR.JOURNALID
	INNER JOIN P_WORKORDER WO ON WOTR.WOID = WO.WOID
	INNER JOIN FL_CONTRACTOR_WORK FCW ON WO.ORDERID = FCW.PurchaseORDERID
	INNER JOIN FL_FAULT_JOURNAL FJ ON FCW.JournalId = FJ.JournalID
	INNER JOIN FL_FAULT_LOG FL ON FJ.FaultLogID = FL.FaultLogID
	OUTER APPLY ( SELECT MAX(T.TenancyID) TenancyID FROM C_TENANCY T
						WHERE T.PROPERTYID = FL.PROPERTYID AND (FL.SubmitDate BETWEEN T.STARTDATE AND ISNULL(T.ENDDATE, @Today)) ) Tenancy	
		
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
      BEGIN 
          PRINT 'Some thing went wrong with this transaction' 

          ROLLBACK TRANSACTION
      END 

    -- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage
END CATCH