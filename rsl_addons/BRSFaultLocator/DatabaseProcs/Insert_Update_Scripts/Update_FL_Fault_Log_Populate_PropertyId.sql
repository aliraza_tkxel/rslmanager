/* ==============================================================================    
    
    Author: Aamir Waheed
    Creation Date:  29/06/2015
    Description: 1- Update FL_FAULT_LOG to populate missing PropertyId
					from C_TENANCY based on customerId and active tenancy
					at the time of fault log

================================================================================= */

BEGIN TRANSACTION
BEGIN TRY
	
	DECLARE @Today DATE = GETDATE()	

/* After applying this script, there may be few records with missing properyId in FL_FAULT_LOG
 * due to no active tenancy on the date fault was logged.
 */

/* Note: Select and order by caluse are available in script for data evaluation. */
/*
SELECT FL.FaultLogID, FL.PROPERTYID, FL.SubmitDate
		, CT.STARTDATE, CT.ENDDATE
		, T.STARTDATE, T.ENDDATE, T.PROPERTYID
		--, FS.Description
*/

UPDATE FL
	SET FL.PROPERTYID = COALESCE(FL.PROPERTYID, T.PROPERTYID)
FROM   FL_FAULT_LOG FL
/* This is to filter records only with 'Assigned To Contractor'
, to update all record with missing PropertyId and any status comment next line.
*/
INNER JOIN FL_FAULT_STATUS FS ON FL.StatusID = FS.FaultStatusID AND FS.Description = 'Assigned To Contractor'
INNER JOIN C_CUSTOMERTENANCY CT ON FL.CustomerId = CT.CUSTOMERID AND (FL.SubmitDate BETWEEN CT.STARTDATE AND COALESCE(CT.ENDDATE,@Today))
INNER JOIN C_TENANCY T ON CT.TENANCYID = T.TENANCYID AND (FL.SubmitDate BETWEEN T .STARTDATE AND COALESCE(T.ENDDATE,@Today))

Where
	FL.PROPERTYID IS NULL	
/*
--ORDER BY FL.FaultLogID ASC
*/


	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0 
      BEGIN
          PRINT 'Some thing went wrong with this transaction'

          ROLLBACK TRANSACTION
      END

    -- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage
END CATCH