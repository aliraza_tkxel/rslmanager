-- =============================================
-- Author:		<Aamir Waheed>
-- Create date: <08/05/2013>
-- Description:	<Add Email Column to Send Email to subcontractor.>
-- =============================================

ALTER TABLE S_ORGANISATION
ADD EMAIL NVARCHAR(150) NULL