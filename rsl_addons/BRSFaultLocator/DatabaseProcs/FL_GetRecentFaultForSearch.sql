USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetRecentFaultForSearch]    Script Date: 08/30/2013 16:52:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================

--EXEC	[dbo].[FL_GetRecentFaultForSearch]
--		@searchedText = N'the tap'	
	
-- Author:		Ali Raza
-- Create date: <8/26/2013>
-- Description:	<Get list of InProgress Faults>
-- Web Page: ReportsArea.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetRecentFaultForSearch] 
(
@searchedText NVARCHAR(MAX)
)
AS
BEGIN

DECLARE @mainQuery NVARCHAR(MAX)
DECLARE @selectQuery NVARCHAR(MAX)
DECLARE @whereQuery NVARCHAR(MAX)
DECLARE @count int
DECLARE @searchCriteria NVARCHAR(MAX)='' 
DECLARE @RESULT NVARCHAR(100) 
DECLARE @intFlag INT

 CREATE TABLE #tempTableSplit(  
     ID int,  
     item  NVARCHAR(MAX)    
    ) 
   --Split string and insert in temp table 
INSERT #tempTableSplit SELECT ID,COLUMN1 FROM dbo.SPLIT_STRING(@searchedText, N' '); 


SET @count =(select COUNT(ID) from #tempTableSplit)
--record count in temp table
-- Add Search criteria from temp table
--======================================================================
SET @intFlag = 1
WHILE (@intFlag <=@count)
BEGIN
	SELECT @RESULT=item from #tempTableSplit where ID=@intFlag
	SET @searchCriteria += ' AND FL_FAULT.Description like ''%'+@RESULT+'%'''

	SET @intFlag = @intFlag + 1
END

-- Begin Select Statement
--======================================================================
SET @selectQuery='SELECT        
		FL_FAULT.FaultId as FaultId   
		,FL_FAULT.Description as Description    
		FROM FL_FAULT '
--======================================================================
-- end Select Statement		
-- Begin Where clause
--======================================================================
SET @whereQuery=' Where FL_FAULT.FaultId in (      
		SELECT  
		FL_FAULT.FaultId as FaultId              
		FROM FL_FAULT                 
		WHERE 
		ISNULL(FL_FAULT.FAULTACTIVE,0) = 1  '+ @searchCriteria+'
		Group By FaultId  ) Order by Description ASC  ' 
-- End Where clause
--========================================================================================
-- Begin building the main select Query	     
SET @mainQuery  = @selectQuery +@whereQuery

--========================================================================================
-- END building the main select Query	 
--========================================================================================
-- Begin print and execute main query
print(@mainQuery)
EXEC (@mainQuery)
-- end print and execute main query
--========================================================================================

-- Drop temp tble 
--========================================================================================
DROP TABLE #tempTableSplit;
end	