
/* =================================================================================    
    Page Description: Get Area for Dop down list over scheme/Block Assign to contractor PopUp 
 
    Author: Ali Raza
    Creation Date: Feb-1-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Feb-1-2015     Ali Raza           Get Area for Dop down list over scheme/Block Assign to contractor PopUp 
    
    Execution Command:
    
    Exec PDR_GetLocalAuthority
  =================================================================================*/
CREATE PROCEDURE dbo.FL_GetArea
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT  AreaID, PA_LOCATION.LocationName+' > '+PA_AREA.AreaName As AreaName 

 FROM PA_AREA 
INNER JOIN PA_LOCATION ON PA_AREA.LocationId = PA_LOCATION.LocationID
WHERE PA_AREA.IsActive = 1 AND PA_AREA.ShowInApp = 1
	ORDER BY PA_LOCATION.LocationID  ASC
END
GO
