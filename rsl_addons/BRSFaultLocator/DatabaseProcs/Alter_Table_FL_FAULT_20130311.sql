/* ===========================================================================
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To ALTER COLUMN ElementID in FL_FAULT column, so that ElementID Column 
--					can accept null values
 '==============================================================================*/

USE [RSLBHALive]
GO

ALTER TABLE FL_FAULT
ALTER COLUMN ElementID INT NULL