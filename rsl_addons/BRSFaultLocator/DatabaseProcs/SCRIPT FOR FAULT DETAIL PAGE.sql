
/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	16 January 2015
--  Description:	Insertion scripts for Fault Management List
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY     

DECLARE @REPAIRS_MENUID INT, @FAULT_DATABASE_PAGEID INT

--======================================================================================================
--												PAGES	(LEVEL 4)
--======================================================================================================
SELECT	@FAULT_DATABASE_PAGEID = PAGEID, @REPAIRS_MENUID = MENUID
FROM	AC_PAGES 
WHERE	DESCRIPTION = 'Fault Database' 		

--=========================================================
-- Repairs > Reports > Fault Management Report > FAULT INFO
--==========================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Fault Information',@REPAIRS_MENUID,1,0,'~/../FaultScheduling/Views/Faults/FaultDetail.aspx','1',4,@FAULT_DATABASE_PAGEID,'',0)


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 
	