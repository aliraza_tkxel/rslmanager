--Author: Behroz Sikander
--Date: 19/03/2013
--This script adds a new column in FL_TEMP_FAULT table

--1. First select the below statement press F5
ALTER TABLE FL_TEMP_FAULT ADD ISREARRANGE BIT NULL