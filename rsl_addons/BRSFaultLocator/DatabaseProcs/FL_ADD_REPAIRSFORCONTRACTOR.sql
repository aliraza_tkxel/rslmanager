USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_ADD_REPAIRSFORCONTRACTOR]    Script Date: 12/13/2013 16:05:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC	[dbo].[FL_ADD_REPAIRSFORCONTRACTOR]
--		@FaultLogID = 45,
--		@FaultRepairIDList = N'1,2,3,4,5,6,7,8,9,10',
--		@Time = N'09:15 AM',
--		@Date = N'2012-02-08 00:00:00.000',
--		@UserID = 615
-- =============================================
-- Author:		<Author,Ali Raza>
-- Create date: <Create Date,12/Dec/2013>
-- Description:	<Description,Add repair for subcontrator>
-- =============================================
CREATE PROCEDURE [dbo].[FL_ADD_REPAIRSFORCONTRACTOR]
	-- Add the parameters for the stored procedure here
@FaultLogID int,
@FaultRepairIDList varchar(8000),
@Time VARCHAR(50),
@Date datetime,
@UserID Int,
@RESULT  INT = 1  OUTPUT 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON
	--BEGIN TRAN
DECLARE @FaultRepairId INT,@Success int	
CREATE TABLE #tempFaultRepairId(FaultRepairId INT )
INSERT INTO #tempFaultRepairId (FaultRepairId)
	SELECT
		COLUMN1
	FROM dbo.SPLIT_STRING(@FaultRepairIDList, ',')

 
 DECLARE @ItemToInsertCursor CURSOR
--Initialize cursor
SET @ItemToInsertCursor = CURSOR FAST_FORWARD FOR SELECT
	FaultRepairId
FROM #tempFaultRepairId;
   --Open cursor
   OPEN @ItemToInsertCursor
---fetch row from cursor
   FETCH NEXT FROM @ItemToInsertCursor INTO @FaultRepairId
 
   
    ---Iterate cursor to get record row by row
		WHILE @@FETCH_STATUS = 0
		BEGIN
		Set @Success = 0	
INSERT INTO FL_CO_FAULTLOG_TO_REPAIR (FaultLogID,FaultRepairListID, InspectionDate, InspectionTime, UserId )
VALUES (@FaultLogID, @FaultRepairId, @Date,@Time,@UserID)
	
		 FETCH NEXT FROM @ItemToInsertCursor INTO @FaultRepairId
  
		END
--close & deallocate cursor		
CLOSE @ItemToInsertCursor
DEALLOCATE @ItemToInsertCursor
	
	IF OBJECT_ID('tempdb..#tempFaultRepairId') IS NOT NULL
DROP TABLE #tempFaultRepairId
 Set @Success = 1
	
	Select @RESULT= @Success
	
END
