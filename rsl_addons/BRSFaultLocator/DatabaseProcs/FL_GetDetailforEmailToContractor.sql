USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[Planned_GetDetailforEmailToContractor]    Script Date: 02/02/2015 14:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Aamir Waheed
-- Create date: 30/04/2014
-- Description:	To get detail to send email to contractor, details are: contractor details
-- Web Page:	AssignToContractor.ascx(User Control)(Details for email)
-- =============================================
Create PROCEDURE [dbo].[FL_GetDetailforEmailToContractor]

	@contractorId INT
	


AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--=================================================
--Get Contractor Detail(s)
--=================================================
SELECT
	O.ORGID AS [ORGID],
	O.NAME AS [NAME],
	ISNULL(O.Email,'') AS [Email],
	ISNULL(E.FIRSTNAME,'') + ISNULL(' ' + E.LASTNAME,'') AS [ContractorContactName]
	
FROM S_ORGANISATION O
LEFT JOIN E__EMPLOYEE E ON E.ORGID = O.ORGID
WHERE O.ORGID = @contractorId

END
