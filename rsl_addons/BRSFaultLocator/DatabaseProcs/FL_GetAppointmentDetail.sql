/****** Object:  StoredProcedure [dbo].[FL_GetAppointmentDetail]    Script Date: 01/31/2013 12:10:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC FL_GetAppointmentDetail
--@faultLogID =20171
-- Author:		<Ahmed Mehmood>
-- Create date: <31/1/2013>
-- Description:	<Returns Appointment details>
-- Webpage:FaultBasket.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetAppointmentDetail] 
	-- Add the parameters for the stored procedure here
	(
	@faultLogID int
	)
AS
BEGIN


SELECT	FL_FAULT_LOG.JobSheetNumber ,
			CASE WHEN FL_FAULT_LOG.StatusID = 2 THEN
			S_ORGANISATION.NAME  ELSE
			convert(VARCHAR(20), AppointmentDate,103)+' '+FL_CO_APPOINTMENT.Time END as Info
			
FROM	FL_FAULT_LOG 
		LEFT OUTER JOIN FL_FAULT_APPOINTMENT on FL_FAULT_LOG.FaultLogID =FL_FAULT_APPOINTMENT.FaultLogId
		LEFT OUTER JOIN FL_CO_APPOINTMENT on  FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
		LEFT OUTER JOIN S_ORGANISATION on FL_FAULT_LOG.ORGID=S_ORGANISATION.ORGID 
		
WHERE FL_FAULT_LOG.FaultLogID = @faultLogID

END

