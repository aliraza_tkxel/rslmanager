﻿Imports FLS_BusinessObject
Imports FLS_Utilities

Namespace FLS_DataAccess


    Public Class SearchFaultDAL : Inherits BaseDAL
        Public Sub getRecentFaults(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef customerId As Integer, ByRef searchText As String, ByRef jsn As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim searchFaultDt As DataTable = New DataTable(ApplicationConstants.RecentFaultsDtName)
            Dim customerDt As DataTable = New DataTable(ApplicationConstants.CustomerDtName)
            Dim propertyDt As DataTable = New DataTable(ApplicationConstants.PropertyDtName)

            Dim proeprtyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(proeprtyIdParam)

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", customerId, DbType.Int32)
            parametersList.Add(customerIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            'Added by Behroz - 20/02/2013 - Start
            Dim jsnParam As ParameterBO = New ParameterBO("jsn", jsn, DbType.String)
            parametersList.Add(jsnParam)
            'Added by Behroz - 20/02/2013 - End

            resultDataSet.Tables.Add(searchFaultDt)
            resultDataSet.Tables.Add(customerDt)
            resultDataSet.Tables.Add(propertyDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRecentFaults)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, searchFaultDt, customerDt, propertyDt)
        End Sub

        Public Sub getSbRecentFaults(ByRef resultDataSet As DataSet, ByRef schemeId As Integer, ByRef blockId As Integer, ByRef searchText As String, ByRef jsn As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim searchFaultDt As DataTable = New DataTable(ApplicationConstants.RecentFaultsDtName)            
            Dim schemeBlockDt As DataTable = New DataTable(ApplicationConstants.SchemeBlockDtName)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim jsnParam As ParameterBO = New ParameterBO("jsn", jsn, DbType.String)
            parametersList.Add(jsnParam)

            resultDataSet.Tables.Add(searchFaultDt)            
            resultDataSet.Tables.Add(schemeBlockDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSbRecentFaults)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, searchFaultDt, schemeBlockDt)
        End Sub
    End Class
End Namespace