﻿Imports FLS_BusinessObject
Imports FLS_Utilities





Namespace FLS_DataAccess

    Public Class AssignToContractorDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Cost Centre Drop Down Values."

        Public Sub GetCostCentreDropDownVales(ByRef dropDownList As List(Of DropDownBO))
            getDropDownValuesBySpName(dropDownList, SpNameConstants.GetCostCentreDropDownValues)
        End Sub

#End Region

#Region "Get contact email details"
        ''' <summary>
        ''' Get contact email details
        ''' </summary>
        ''' <param name="contactId"></param>
        ''' <param name="detailsForEmailDS"></param>
        ''' <remarks></remarks>

        Sub getContactEmailDetail(ByRef contactId As Integer, ByRef detailsForEmailDS As DataSet)
            Dim inParamList As New ParameterList

            Dim contactIdParam As New ParameterBO("employeeId", contactId, DbType.Int32)
            inParamList.Add(contactIdParam)

            Dim datareader = MyBase.SelectRecord(inParamList, SpNameConstants.GetContactEmailDetail)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, ApplicationConstants.ContactEmailDetailsDt)
        End Sub

#End Region

#Region "Get Budget Head Drop Down Values by Cost Centre Id"

        Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef dropDownList As List(Of DropDownBO), ByRef CostCentreId As Integer)
            Dim inParamList As New ParameterList

            Dim costCentreIdparam As ParameterBO = New ParameterBO("costCentreId", CostCentreId, DbType.Int32)
            inParamList.Add(costCentreIdparam)

            getDropDownValuesBySpName(dropDownList, SpNameConstants.GetBudgetHeadDropDownValuesByCostCentreId, inParamList)
        End Sub

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id and EmployeeId"

        Sub GetExpenditureDropDownValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
            Dim inParamList As New ParameterList

            Dim budgetHeadIdparam As ParameterBO = New ParameterBO("HeadId", BudgetHeadId, DbType.Int32)
            inParamList.Add(budgetHeadIdparam)

            Dim employeeIdparam As ParameterBO = New ParameterBO("userId", EmployeeId, DbType.Int32)
            inParamList.Add(employeeIdparam)

            Dim spName As String = SpNameConstants.GetExpenditureDropDownValuesByBudgetHeadId

            Dim myDataReader As IDataReader = MyBase.SelectRecord(inParamList, spName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim description As String = String.Empty
                Dim limit As Decimal = 0.0
                Dim remaining As Decimal = 0.0

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("LIMIT")) Then
                    limit = myDataReader.GetDecimal(myDataReader.GetOrdinal("LIMIT"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("REMAINING")) Then
                    remaining = myDataReader.GetDecimal(myDataReader.GetOrdinal("REMAINING"))
                End If

                Dim objExpenditureBo As New ExpenditureBO(id, description, limit, remaining)
                expenditureBOList.Add(objExpenditureBo)
            End While
            'getDropDownValuesBySpName(dropDownList, SpNameConstants.GetExpenditureDropDownValuesByBudgetHeadId, inParamList)
        End Sub

#End Region

#Region "Get Contractor Having Repairs Contract"

        Sub GetRepairsContractors(ByRef dropDownList As List(Of DropDownBO))
            getDropDownValuesBySpName(dropDownList, SpNameConstants.GetRepairsContractorDropDownValues)
        End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

        Sub GetContactDropDownValuesbyContractorId(ByRef dropDownBoList As List(Of DropDownBO), ByVal ContractorId As Integer)
            Dim inParamList As New ParameterList

            Dim propertyIdparam As ParameterBO = New ParameterBO("contractorId", ContractorId, DbType.Int32)
            inParamList.Add(propertyIdparam)

            getDropDownValuesBySpName(dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList)
        End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

        Sub GetVatDropDownValues(ByRef vatBoList As List(Of ContractorVatBO))

            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetVatDropDownValues)

            If vatBoList Is Nothing Then
                vatBoList = New List(Of ContractorVatBO)
            End If

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim description As String = String.Empty
                Dim vatRate As Decimal

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("vatRate")) Then
                    vatRate = myDataReader.GetDecimal(myDataReader.GetOrdinal("vatRate"))
                End If

                Dim objVatBo As New ContractorVatBO(id, description, vatRate)
                vatBoList.Add(objVatBo)
            End While
        End Sub

        Sub GetVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
            getDropDownValuesBySpName(dropDownList, SpNameConstants.GetVatDropDownValues)
        End Sub

#End Region

#Region "Assign Work to Contractor - Save Process"

        Public Function assignToContractor(ByRef assignToContractorBo As AssignToContractorBO) As Boolean

            Dim inParamList As New ParameterList
            Dim outParamList As New ParameterList

            Dim contactIdParam As New ParameterBO("contactId", assignToContractorBo.ContactId, DbType.Int32)
            inParamList.Add(contactIdParam)

            Dim contractroIdParam As New ParameterBO("contractorId", assignToContractorBo.ContractorId, DbType.Int32)
            inParamList.Add(contractroIdParam)

            Dim userIdParam As New ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32)
            inParamList.Add(userIdParam)

            Dim poStatusParam As New ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32)
            inParamList.Add(poStatusParam)

            Dim blockIdParam As New ParameterBO("PropertyId", assignToContractorBo.PropertyId, DbType.String)
            inParamList.Add(blockIdParam)

            Dim journalIdParam As New ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32)
            inParamList.Add(journalIdParam)

            Dim contractorWorkDetalParam As New ParameterBO("ContractorWorksDetail", assignToContractorBo.WorksRequired, SqlDbType.Structured)
            inParamList.Add(contractorWorkDetalParam)

            Dim isSavedParam As New ParameterBO("isSaved", 0, DbType.Boolean)
            outParamList.Add(isSavedParam)

            Dim journalIdOutParam As New ParameterBO("journalIdOut", -1, DbType.Int32)
            outParamList.Add(journalIdOutParam)

            Dim faultContractorIdOutParam As New ParameterBO("faultContractorId", -1, DbType.Int32)
            outParamList.Add(faultContractorIdOutParam)

            Dim POIdOutParam As New ParameterBO("POId", -1, DbType.Int32)
            outParamList.Add(POIdOutParam)

            Dim POStatusIdOutParam As New ParameterBO("POStatusId", -1, DbType.Int32)
            outParamList.Add(POStatusIdOutParam)

            outParamList = MyBase.SelectRecord(inParamList, outParamList, SpNameConstants.AssignPropertyWorkToContractor)
            assignToContractorBo.JournalId = outParamList.Item(1).Value
            assignToContractorBo.FaultContractorId = Convert.ToInt32(outParamList.Item(2).Value)
            assignToContractorBo.OrderId = Convert.ToInt32(outParamList.Item(3).Value)
            assignToContractorBo.POCurrentStatus = Convert.ToInt32(outParamList.Item(4).Value)

            Dim isSaved As Boolean = False
            isSaved = outParamList.Item(0).Value
            Return isSaved

        End Function

#End Region

#Region "Get Details for email"

        ''' <summary>
        ''' This function is to get details for email, these details include contractor name, email and other details.
        ''' Contact details of customer/tenant.
        ''' Risk/Vulnerability details of the tenant
        ''' Property Address
        ''' </summary>
        ''' <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        ''' <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        ''' <remarks></remarks>
        Sub getdetailsForEmail(ByRef assignToContractorBo As AssignToContractorBO, ByRef detailsForEmailDS As DataSet)
            Dim inParamList As New ParameterList

            Dim contractorIdParam As New ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32)
            inParamList.Add(contractorIdParam)

            Dim propertyIdParam As New ParameterBO("propertyId", assignToContractorBo.PropertyId, DbType.String)
            inParamList.Add(propertyIdParam)

            Dim empolyeeIdParam As New ParameterBO("employeeId", assignToContractorBo.ContactId, DbType.Int32)
            inParamList.Add(empolyeeIdParam)

            Dim faultContractorIdParam As New ParameterBO("ContractorId", assignToContractorBo.FaultContractorId, DbType.Int32)
            inParamList.Add(faultContractorIdParam)

            Dim datareader = MyBase.SelectRecord(inParamList, SpNameConstants.GetContractorDetailforEmail)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, _
                                   ApplicationConstants.ContractorDetailsDt _
                                   , ApplicationConstants.PropertyDetailsDT _
                                   , ApplicationConstants.TenantDetailsDt _
                                   , ApplicationConstants.TenantRiskDetailsDt _
                                   , ApplicationConstants.TenantVulnerabilityDetailsDt _
                                   , ApplicationConstants.OrderedByDetailsDt _
                                   , ApplicationConstants.AsbestosDetailsDt _
                                   , ApplicationConstants.FaultInfoDt)
        End Sub

#End Region

#Region "Supporting Functions"

#Region "Get Drop Down Values By Stored Procedure name and optional input Parameters(filters)"

        ''' <summary>
        ''' Requirements to Use This function: the stored procedure must return two columns
        ''' 1- id of type integer
        ''' 2- description of type string
        ''' To use this function provide a stored procedure name that fulfill the above conditions and provide
        ''' optional input parameters(filters).
        ''' </summary>
        ''' <param name="dropDownList">A list of DropDownBo(s) List</param>
        ''' <param name="spName">Stored Procedure name as string</param>
        ''' <param name="paramList">List of input parameter(s) as ParameterList business object.</param>
        ''' <remarks></remarks>
        Private Sub getDropDownValuesBySpName(ByRef dropDownList As List(Of DropDownBO), ByRef spName As String, Optional ByRef paramList As ParameterList = Nothing)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(paramList, spName)

            If dropDownList Is Nothing Then
                dropDownList = New List(Of DropDownBO)
            End If

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim description As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("description")) Then
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"))
                End If
                Dim objDropDownBo As New DropDownBO(id, description)
                dropDownList.Add(objDropDownBo)
            End While
        End Sub

#End Region

#End Region


#Region "get Budget Holder By OrderId"

        Sub getBudgetHolderByOrderId(ByRef resultDs As DataSet, ByRef orderId As Integer, ByVal expenditureId As Integer)

            Dim parametersList As New ParameterList()

            Dim orderIdParam As New ParameterBO("orderId", orderId, DbType.Int32)
            parametersList.Add(orderIdParam)

            Dim expenditureIdParam As New ParameterBO("expenditureId", expenditureId, DbType.Int32)
            parametersList.Add(expenditureIdParam)

            MyBase.LoadDataSet(resultDs, parametersList, SpNameConstants.GetBudgetHolderByOrderId)

        End Sub

#End Region



#Region "get HEADID and EXPENDITUREID for assign to contractor"

        Sub GetHeadAndExpenditureId(ByRef resultDs As DataSet, ByVal ExpenditureType As String)

            Dim parametersList As New ParameterList()
            Dim ExpenditureTypeParam As New ParameterBO("ExpenditureType", ExpenditureType, DbType.String)
            parametersList.Add(ExpenditureTypeParam)
            MyBase.LoadDataSet(resultDs, parametersList, SpNameConstants.GetHeadAndExpenditureId)

        End Sub

#End Region


#End Region

    End Class

End Namespace
