﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports FLS_Utilities
Imports FLS_BusinessObject

Namespace FLS_DataAccess

    Public Class MasterPageDAL : Inherits BaseDAL

#Region "Get Property Menus List"
        ''' <summary>
        ''' Get Property Menus List
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Sub getPropertyMenuList(ByRef resultDataset As DataSet, ByVal employeeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim menusDt As DataTable = New DataTable()
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt
            resultDataset.Tables.Add(menusDt)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuList)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, menusDt)

        End Sub

#End Region

#Region "Get Property Page List "
        ''' <summary>
        ''' Get Property Page List 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getPropertyPageList(ByRef resultDataSet As DataSet, ByVal employeeId As Integer, ByVal selectedMenu As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim modulesDt As DataTable = New DataTable()
            modulesDt.TableName = ApplicationConstants.AccessGrantedModulesDt
            resultDataSet.Tables.Add(modulesDt)

            Dim menusDt As DataTable = New DataTable()
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt
            resultDataSet.Tables.Add(menusDt)

            Dim pageDt As DataTable = New DataTable()
            pageDt.TableName = ApplicationConstants.AccessGrantedPagesDt
            resultDataSet.Tables.Add(pageDt)

            Dim randomPageDt As DataTable = New DataTable()
            randomPageDt.TableName = ApplicationConstants.RandomPageDt
            resultDataSet.Tables.Add(randomPageDt)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim menuTextParam As ParameterBO = New ParameterBO("menuText", selectedMenu, DbType.String)
            parametersList.Add(menuTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuPages)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, modulesDt, menusDt, pageDt, randomPageDt)
        End Sub
#End Region

    End Class

End Namespace


