﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports FLS_Utilities
Imports FLS_BusinessObject

Namespace FLS_DataAccess
    Public Class SchedularSearchDAL : Inherits BaseDAL

#Region "Functions"



#Region "Get Patch and Trade Info"

        Public Sub GetPatchAndTradeInfo(ByRef patchDataSet As DataSet, ByRef tradeDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPatch As DataTable = New DataTable()
            Dim dtTrade As DataTable = New DataTable()

            dtPatch.TableName = "Patches"
            dtTrade.TableName = "Trades"

            patchDataSet.Tables.Add(dtPatch)
            tradeDataSet.Tables.Add(dtTrade)

            Dim lPatchResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyPatches)
            patchDataSet.Load(lPatchResultDataReader, LoadOption.OverwriteChanges, dtPatch)

            Dim lTradeResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFaultTrades)
            tradeDataSet.Load(lTradeResultDataReader, LoadOption.OverwriteChanges, dtTrade)


        End Sub

#End Region


#Region "Get Scheme Info"

        Public Sub GetSchemeInfo(ByRef resultDataSet As DataSet, ByVal patchid As Int32)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtScheme As DataTable = New DataTable()
            dtScheme.TableName = "Patches"
            resultDataSet.Tables.Add(dtScheme)

            Dim propertyIdParam As ParameterBO = New ParameterBO("patchid", patchid, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSchemeNames)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtScheme)


        End Sub

#End Region


#Region "Get Schedular Search Result"

        Public Function GetSchedularSearchResult(ByRef resultDataSet As DataSet, ByVal objSchedularSearchBO As SchedularSearchBO, ByVal objPageSortBo As PageSortBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", objSchedularSearchBO.Patch, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objSchedularSearchBO.Scheme, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objSchedularSearchBO.Trade, DbType.Int32)
            parametersList.Add(tradeIdParam)

            Dim jsnParam As ParameterBO = New ParameterBO("jsn", objSchedularSearchBO.JobSheetNumber, DbType.String)
            parametersList.Add(jsnParam)

            Dim operativeParam As ParameterBO = New ParameterBO("operative", objSchedularSearchBO.Operative, DbType.String)
            parametersList.Add(operativeParam)

            Dim tenantParam As ParameterBO = New ParameterBO("tenant", objSchedularSearchBO.Tenant, DbType.String)
            parametersList.Add(tenantParam)

            Dim addressParam As ParameterBO = New ParameterBO("address", objSchedularSearchBO.Address, DbType.String)
            parametersList.Add(addressParam)



            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetSchedularSearchFaults)
            Return outParametersList.Item(0).Value.ToString()


        End Function

#End Region

#End Region
    End Class
End Namespace

