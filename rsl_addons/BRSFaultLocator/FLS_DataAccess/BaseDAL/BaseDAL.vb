﻿' Class Name -- BaseDAL
' Base Class -- System
' Summary -- Abstract class, all DAL classes will inherit from it
' Methods -- SaveRecord, SelectRecord
Imports System
Imports System.Data.Common
Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports FLS_BusinessObject

Namespace FLS_DataAccess
    Public MustInherit Class BaseDAL
#Region "Atrributes"

        ' Establish connection with Database by reading configuration from web.config file
        Private varDatabase As Database

        ' Execute commands e-g StoredProcedures
        Private varDbCommand As DbCommand

#End Region

#Region "Properties"

        ' No Properties are defined as Attributes are supposed to be accessed within this class only

#End Region

#Region "Constructor"

        Public Sub BaseDAL()

            varDatabase = Nothing
            varDbCommand = Nothing

        End Sub


#End Region

#Region "Functions"

#Region "SaveRecord Function"

        ' This generic Function will be used to save(new record), delete and update records

        Protected Function SaveRecord(ByRef inParamList As ParameterList, ByRef outParamList As ParameterList, ByRef sprocName As String) As Integer

            ' Used to return No of rows affected
            Dim rowCount As Integer = -1


            varDatabase = DatabaseFactory.CreateDatabase()
            varDbCommand = varDatabase.GetStoredProcCommand(sprocName)


            ' For Each Loop to iterate over inParamList
            ' IN-Parameters will be extracted and added to StoredProcedure
            For Each paramBO As ParameterBO In inParamList
                ' Adding parameter Name and Type
                '' SqlDbType.Structured is used to send table valued parameter
                If paramBO.Type = SqlDbType.Structured Then
                    varDbCommand.Parameters.Add(New SqlParameter(paramBO.Name, paramBO.Value) With {.SqlDbType = paramBO.Type})
                Else
                    ' Adding parameter Name and Type
                    varDatabase.AddInParameter(varDbCommand, paramBO.Name, paramBO.Type)

                    ' Setting value of parameter
                    varDatabase.SetParameterValue(varDbCommand, paramBO.Name, paramBO.Value)
                End If
            Next


            ' For Each Loop to iterate over outParamList
            ' OUT-Parameters will be extracted and added to StoredProcedure
            For Each outParamBO As ParameterBO In outParamList

                ' Adding output parameters Name and Type
                varDatabase.AddOutParameter(varDbCommand, outParamBO.Name, outParamBO.Type, 100)
            Next


            ' Executing Non query and recieving results (No. of rows affected)
            rowCount = varDatabase.ExecuteNonQuery(varDbCommand)



            ' Getting out parameter values after executing query
            For Each paramBO As ParameterBO In outParamList
                paramBO.Value = varDatabase.GetParameterValue(varDbCommand, paramBO.Name)

            Next
            Return rowCount

        End Function

#End Region

#Region "Select Function"

        ' This Function will be used to Select Single/Multiple records

        Protected Function SelectRecord(ByRef paramList As ParameterList, ByRef sprocName As String) As IDataReader

            ' Holds the results/records found after executing stored procedure
            Dim myReader As IDataReader = Nothing



            varDatabase = DatabaseFactory.CreateDatabase()
            varDbCommand = varDatabase.GetStoredProcCommand(sprocName)

            ' For Each Loop to iterate over paramList
            ' ParameterBO's will be extracted and added to StoredProcedure
            If Not paramList Is Nothing Then
                For Each paramBO As ParameterBO In paramList
                    ' Adding parameter Name and Type
                    '' SqlDbType.Structured is used to send table valued parameter
                    If paramBO.Type = SqlDbType.Structured Then
                        varDbCommand.Parameters.Add(New SqlParameter(paramBO.Name, paramBO.Value) With {.SqlDbType = paramBO.Type})
                    Else
                        ' Adding parameter Name and Type
                        varDatabase.AddInParameter(varDbCommand, paramBO.Name, paramBO.Type)

                        ' Setting value of parameter
                        varDatabase.SetParameterValue(varDbCommand, paramBO.Name, paramBO.Value)
                    End If
                Next
            End If


            ' Executing ExecuteReader() and returning records found
            myReader = varDatabase.ExecuteReader(varDbCommand)


            Return myReader

        End Function



        ' This Function will be used to Select Single/Multiple records

        Protected Function SelectRecord(ByRef inParamList As ParameterList, ByRef outParamList As ParameterList, ByRef sprocName As String) As ParameterList



            varDatabase = DatabaseFactory.CreateDatabase()
            varDbCommand = varDatabase.GetStoredProcCommand(sprocName)

            ' For Each Loop to iterate over paramList
            ' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In inParamList
                ' Adding parameter Name and Type
                '' SqlDbType.Structured is used to send table valued parameter
                If paramBO.Type = SqlDbType.Structured Then
                    varDbCommand.Parameters.Add(New SqlParameter(paramBO.Name, paramBO.Value) With {.SqlDbType = paramBO.Type})
                Else
                    ' Adding parameter Name and Type
                    varDatabase.AddInParameter(varDbCommand, paramBO.Name, paramBO.Type)

                    ' Setting value of parameter
                    varDatabase.SetParameterValue(varDbCommand, paramBO.Name, paramBO.Value)
                End If
            Next

            For Each outParamBO As ParameterBO In outParamList
                ' Adding parameter Name and Type
                varDatabase.AddOutParameter(varDbCommand, outParamBO.Name, outParamBO.Type, 8000)
            Next

            varDatabase.ExecuteScalar(varDbCommand)

            For Each paramBO As ParameterBO In outParamList
                paramBO.Value = varDatabase.GetParameterValue(varDbCommand, paramBO.Name)
            Next

            Return outParamList

        End Function

#End Region

#Region "Load DataSet"
        ''' <summary>
        ''' This function load the data set but it cannot return outuput variable
        ''' </summary>
        ''' <param name="myDataSet">Data set which would be filled by reader</param>
        ''' <param name="paramList">input paramters of stroed procuedure</param>
        ''' <param name="sprocName">output paramters of stroed procuedure</param>
        ''' <remarks></remarks>
        Protected Sub LoadDataSet(ByRef myDataSet As DataSet, ByRef paramList As ParameterList, ByRef sprocName As String)


            varDatabase = DatabaseFactory.CreateDatabase()
            varDbCommand = varDatabase.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In paramList
                ' Adding parameter Name and Type
                '' SqlDbType.Structured is used to send table valued parameter
                If paramBO.Type = SqlDbType.Structured Then
                    varDbCommand.Parameters.Add(New SqlParameter(paramBO.Name, paramBO.Value) With {.SqlDbType = paramBO.Type})
                Else
                    ' Adding parameter Name and Type
                    varDatabase.AddInParameter(varDbCommand, paramBO.Name, paramBO.Type)

                    ' Setting value of parameter
                    varDatabase.SetParameterValue(varDbCommand, paramBO.Name, paramBO.Value)
                End If
            Next

            Dim reader As IDataReader = varDatabase.ExecuteReader(varDbCommand)

            Me.ConvertDataReaderToDataSet(myDataSet, reader)
            varDbCommand.Connection.Close()
        End Sub
#End Region

#Region "Load DataSet"
        ''' <summary>
        ''' This function load the data set but it also returns outuput variable
        ''' </summary>
        ''' <param name="myDataSet">Data set which would be filled by reader</param>
        ''' <param name="paramList">input paramters of stroed procuedure</param>
        ''' <param name="sprocName">output paramters of stroed procuedure</param>
        ''' <remarks></remarks>
        Protected Sub LoadDataSet(ByRef myDataSet As DataSet, ByRef paramList As ParameterList, ByRef outParamList As ParameterList, ByRef sprocName As String)


            varDatabase = DatabaseFactory.CreateDatabase()
            varDbCommand = varDatabase.GetStoredProcCommand(sprocName)

            '' For Each Loop to iterate over paramList
            '' ParameterBO's will be extracted and added to StoredProcedure

            For Each paramBO As ParameterBO In paramList
                ' Adding parameter Name and Type
                '' SqlDbType.Structured is used to send table valued parameter
                If paramBO.Type = SqlDbType.Structured Then
                    varDbCommand.Parameters.Add(New SqlParameter(paramBO.Name, paramBO.Value) With {.SqlDbType = paramBO.Type})
                Else
                    ' Adding parameter Name and Type
                    varDatabase.AddInParameter(varDbCommand, paramBO.Name, paramBO.Type)

                    ' Setting value of parameter
                    varDatabase.SetParameterValue(varDbCommand, paramBO.Name, paramBO.Value)
                End If
            Next

            For Each outParamBO As ParameterBO In outParamList
                ' Adding parameter Name and Type
                varDatabase.AddOutParameter(varDbCommand, outParamBO.Name, outParamBO.Type, 8000)
            Next

            Dim reader As IDataReader = varDatabase.ExecuteReader(varDbCommand)

            Me.ConvertDataReaderToDataSet(myDataSet, reader)

            varDbCommand.Connection.Close()

            For Each paramBO As ParameterBO In outParamList
                paramBO.Value = varDatabase.GetParameterValue(varDbCommand, paramBO.Name)
            Next


        End Sub
#End Region

#Region "Conversion from dataReader to dataSet"

        Private Sub ConvertDataReaderToDataSet(ByRef dataSet As DataSet, ByVal reader As IDataReader)

            Dim schemaTable As DataTable = reader.GetSchemaTable()

            Dim dataTable As DataTable = New DataTable()

            Dim intCounter As Integer

            For intCounter = 0 To schemaTable.Rows.Count - 1

                Dim dataRow As DataRow = schemaTable.Rows(intCounter)

                Dim columnName As String = CType(dataRow("ColumnName"), String)

                Dim column As DataColumn = New DataColumn(columnName, CType(dataRow("DataType"), Type))

                dataTable.Columns.Add(column)

            Next
            dataSet.Tables.Add(dataTable)
            While reader.Read()

                Dim dataRow As DataRow = dataTable.NewRow()

                For intCounter = 0 To reader.FieldCount - 1

                    dataRow(intCounter) = reader.GetValue(intCounter)

                Next

                dataTable.Rows.Add(dataRow)

            End While

        End Sub

#End Region

#End Region

    End Class

End Namespace

