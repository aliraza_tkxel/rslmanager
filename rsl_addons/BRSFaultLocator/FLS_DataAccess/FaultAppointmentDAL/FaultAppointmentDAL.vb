﻿Imports FLS_BusinessObject
Imports FLS_Utilities
Imports System.Text

Namespace FLS_DataAccess


    Public Class FaultAppointmentDAL : Inherits BaseDAL
#Region "get Available Operatives"
        ''' <summary>
        ''' 'This function 'll send you the get available operatives 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tempFaultIds"></param>
        ''' <remarks></remarks>
        Public Sub getAvailableOperatives(ByRef resultDataSet As DataSet, ByRef tempFaultIds As StringBuilder, ByRef isRecall As Boolean)
            Dim parametersList As ParameterList = New ParameterList()
            Dim employeeDt As DataTable = New DataTable()
            Dim leavesDt As DataTable = New DataTable()
            Dim appointmentsDt As DataTable = New DataTable()

            Dim proeprtyIdParam As ParameterBO = New ParameterBO("tempFaultIds", tempFaultIds.ToString(), DbType.String)
            parametersList.Add(proeprtyIdParam)

          

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", isRecall, DbType.Boolean)
            parametersList.Add(isRecallParam)

            resultDataSet.Tables.Add(employeeDt)
            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAvailableOperatives)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt)
        End Sub

        ''' <summary>
        ''' 'This function 'll send you the get available operatives 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="faultIds"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getAvailableOperativesForRearrange(ByRef resultDataSet As DataSet, ByRef faultIds As String, ByRef propertyId As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim employeeDt As DataTable = New DataTable()
            Dim leavesDt As DataTable = New DataTable()
            Dim appointmentsDt As DataTable = New DataTable()

            Dim faultIdParam As ParameterBO = New ParameterBO("faultIds", faultIds, DbType.String)
            parametersList.Add(faultIdParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            resultDataSet.Tables.Add(employeeDt)
            resultDataSet.Tables.Add(leavesDt)
            resultDataSet.Tables.Add(appointmentsDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAvailableOperativesForRearrange)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt)
        End Sub
#End Region

#Region "get Temporary Fault Basket"
        ''' <summary>
        ''' Get Temporary Fault Basket
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        ''' 
        Public Sub getTemporaryFaultBasket(ByRef resultDataSet As DataSet, ByRef tempFaultIds As StringBuilder, ByRef tempFaultTradeIds As StringBuilder, ByRef propertyId As String, ByVal customerId As Integer, ByVal isRecall As Boolean)
            Dim parametersList As ParameterList = New ParameterList()
            Dim tempFaultDt As DataTable = New DataTable(ApplicationConstants.TempFaultDataTable)
            Dim confirmFaultDt As DataTable = New DataTable(ApplicationConstants.ConfirmFaultDataTable)

            Dim tempFaultIdsParam As ParameterBO = New ParameterBO("tempFaultIds", tempFaultIds.ToString(), DbType.String)
            parametersList.Add(tempFaultIdsParam)

            Dim tempFaultTradeIdsParam As ParameterBO = New ParameterBO("tempFaultTradeIds", tempFaultTradeIds.ToString(), DbType.String)
            parametersList.Add(tempFaultTradeIdsParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId.ToString(), DbType.String)
            parametersList.Add(propertyIdParam)

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", customerId, DbType.Int32)
            parametersList.Add(customerIdParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", isRecall, DbType.Boolean)
            parametersList.Add(isRecallParam)

            resultDataSet.Tables.Add(tempFaultDt)
            resultDataSet.Tables.Add(confirmFaultDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTemporaryFaultBasket)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, tempFaultDt, confirmFaultDt)
        End Sub
#End Region
#Region "get Sb Temporary Fault Basket"
        ''' <summary>
        ''' Get Temporary Fault Basket
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        ''' 
        Public Sub getTemporaryFaultBasket(ByRef resultDataSet As DataSet, ByRef tempFaultIds As StringBuilder, ByRef tempFaultTradeIds As StringBuilder, ByRef schemeId As Integer, ByVal blockId As Integer, ByVal isRecall As Boolean)
            Dim parametersList As ParameterList = New ParameterList()
            Dim tempFaultDt As DataTable = New DataTable(ApplicationConstants.TempFaultDataTable)
            Dim confirmFaultDt As DataTable = New DataTable(ApplicationConstants.ConfirmFaultDataTable)

            Dim tempFaultIdsParam As ParameterBO = New ParameterBO("tempFaultIds", tempFaultIds.ToString(), DbType.String)
            parametersList.Add(tempFaultIdsParam)

            Dim tempFaultTradeIdsParam As ParameterBO = New ParameterBO("tempFaultTradeIds", tempFaultTradeIds.ToString(), DbType.String)
            parametersList.Add(tempFaultTradeIdsParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim customerIdParam As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(customerIdParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", isRecall, DbType.Boolean)
            parametersList.Add(isRecallParam)

            resultDataSet.Tables.Add(tempFaultDt)
            resultDataSet.Tables.Add(confirmFaultDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSbTemporaryFaultBasket)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, tempFaultDt, confirmFaultDt)
        End Sub
#End Region

#Region "get Newly created Appointment"
        ''' <summary>
        ''' Get newly created appointment
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub getNewlyCreatedAppointment(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal customerId As Integer, ByVal appointmentId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim confirmFaultDt As DataTable = New DataTable(ApplicationConstants.ConfirmFaultDataTable)

            Dim tempFaultIdsParam As ParameterBO = New ParameterBO("customerId", customerId, DbType.Int32)
            parametersList.Add(tempFaultIdsParam)

            Dim tempFaultTradeIdsParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(tempFaultTradeIdsParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            resultDataSet.Tables.Add(confirmFaultDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetNewCreatedAppointmentData)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, confirmFaultDt)
        End Sub
#End Region

#Region "get operative working hours"
        ''' <summary>
        ''' get operative working hours
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub getOperativeWorkingHours(ByRef resultDataSet As DataSet, ByVal operativeId As Integer, ByVal ofcCoreStartTime As String, ByVal ofcCoreEndTime As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim workingHourDataTable As DataTable = New DataTable(ApplicationConstants.WorkingHourDataTable)

            Dim tempFaultIdsParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.Int32)
            parametersList.Add(tempFaultIdsParam)

            Dim ofcCoreStartTimeParam As ParameterBO = New ParameterBO("defaultStartTime", ofcCoreStartTime, DbType.String)
            parametersList.Add(ofcCoreStartTimeParam)

            Dim ofcCoreEndTimeParam As ParameterBO = New ParameterBO("defaultEndTime", ofcCoreEndTime, DbType.String)
            parametersList.Add(ofcCoreEndTimeParam)

            resultDataSet.Tables.Add(workingHourDataTable)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetOperativeWorkingHours)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, workingHourDataTable)
        End Sub
#End Region

#Region "Update temp fault Duration by tempFaultId"

        Sub updateTempFaultDuration(tempFaultId As Integer, duration As Decimal)
            Dim parametersList As ParameterList = New ParameterList()
            
            Dim tempFaultIdsParam As New ParameterBO("tempFaultId", tempFaultId, DbType.Int32)
            parametersList.Add(tempFaultIdsParam)

            Dim durationParam As New ParameterBO("duration", duration, DbType.Decimal)
            parametersList.Add(durationParam)

            MyBase.SaveRecord(parametersList, New ParameterList(), SpNameConstants.UpdateTempFaultDuration)

        End Sub

#End Region

#Region "is Time Slot Available"

        Function isTimeSlotAvailable(ByVal operativeId As Integer, ByVal startDateTime As Date, ByVal endDateTime As Date) As Boolean
            Dim status As Boolean = False

            Dim inParametersList As New ParameterList()

            Dim operativeIdParam As New ParameterBO("operativeId", operativeId, DbType.Int32)
            inParametersList.Add(operativeIdParam)

            Dim startDateTimeParam As New ParameterBO("startDateTime", startDateTime, DbType.DateTime2)
            inParametersList.Add(startDateTimeParam)

            Dim endDateTimeParam As New ParameterBO("endDateTime", endDateTime, DbType.DateTime2)
            inParametersList.Add(endDateTimeParam)


            Dim outParametersList As New ParameterList()
            Dim isTimeSlotAvailableParam As New ParameterBO("isTimeSlotAvailable", False, DbType.Boolean)
            outParametersList.Add(isTimeSlotAvailableParam)

            MyBase.SelectRecord(inParametersList, outParametersList, SpNameConstants.IsTimeSlotAvailable)
            status = isTimeSlotAvailableParam.Value

            isTimeSlotAvailable = status
        End Function

#End Region

    End Class
End Namespace
