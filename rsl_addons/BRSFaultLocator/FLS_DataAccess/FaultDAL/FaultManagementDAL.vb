﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports FLS_Utilities
Imports FLS_BusinessObject
Namespace FLS_DataAccess

    Public Class FaultManagementDAL : Inherits BaseDAL

#Region "Fault Management Search Results"

#Region "Get Search Total Count"

        Public Function getFaultManagementSearchRowCount(ByRef search As String) As Integer
            Dim sprocName As String
            Dim rowCount As Integer = 0

            sprocName = SpNameConstants.getFaultManagementSearchRowCount
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO

            If search <> Nothing Then
                faultParams = New ParameterBO("searchTerm", search, DbType.String)
                paramList.Add(faultParams)
            Else
                faultParams = New ParameterBO("searchTerm", Nothing, DbType.String)
                paramList.Add(faultParams)
            End If

            '' Passing Array of ParameterBO and sproc-name to base class method: SelectRecord
            Dim myReader As IDataReader = MyBase.SelectRecord(paramList, sprocName)

            '' If record found i.e. No. of rows returned
            If myReader.Read Then

                rowCount = Me.getRowCount(myReader)

            End If

            Return rowCount

        End Function

        '' Private Function used to return no. of results
        Private Function getRowCount(ByRef myDataRecord As IDataRecord) As Integer

            Return myDataRecord.GetInt32(myDataRecord.GetOrdinal("numOfRows"))

        End Function

#End Region

#Region "Get Fault Management Search Results (Single Page)"

        Public Sub GetFaultSearchData(ByRef faultDS As DataSet, ByVal search As String, ByRef pageSortBO As PageSortBO)
            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String
            '' This List will hold instances of type EnquiryLogSearchBO
            Dim paramList As ParameterList = New ParameterList()
            'checking if this are the query is from search button or not
            Dim faultParam As ParameterBO

            '' rowindex, pageindex and sortby will be common to both type of searches
            faultParam = New ParameterBO("noOfRows", pageSortBO.PageSize, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("offSet", pageSortBO.PageNumber, DbType.Int32)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortColumn", pageSortBO.SortExpression, DbType.String)
            paramList.Add(faultParam)
            faultParam = New ParameterBO("sortOrder", pageSortBO.SmallSortDirection, DbType.String)
            paramList.Add(faultParam)

            '' if search is based on username tenancyid date nature etc then we will get
            '' these values from  EnquiryLogSearchBO object

            If search <> Nothing Then
                faultParam = New ParameterBO("searchTerm", search, DbType.String)
                paramList.Add(faultParam)
            Else
                faultParam = New ParameterBO("searchTerm", Nothing, DbType.String)
                paramList.Add(faultParam)
            End If

            sprocName = SpNameConstants.getFaultManagementSerachRecords

            MyBase.LoadDataSet(faultDS, paramList, sprocName)

        End Sub

#End Region

#End Region

#Region "Get Vat Rate"

        Public Sub getVatValues(ByRef vatBO As VatBO)

            Dim sprocName As String
            Dim myDS As New DataSet()
            sprocName = SpNameConstants.getVatValuesByVatID
            Dim paramList As ParameterList = New ParameterList()

            Dim faultParams As ParameterBO
            faultParams = New ParameterBO("VATID", vatBO.VatID, DbType.Int32)
            paramList.Add(faultParams)
            MyBase.LoadDataSet(myDS, paramList, sprocName)
            If (myDS.Tables(0).Rows.Count > 0 And Not (myDS Is Nothing)) Then
                vatBO.VatID = CType(myDS.Tables(0).Rows(0)("VATID"), Int32)
                vatBO.VatName = myDS.Tables(0).Rows(0)("VATNAME").ToString()
                vatBO.VatRate = CType(myDS.Tables(0).Rows(0)("VATRATE"), Double)
                vatBO.VatCode = CType(myDS.Tables(0).Rows(0)("VATCODE"), Char)
            End If

        End Sub

#End Region

#Region "Add New Fault"

        Public Sub addNewFault(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SpNameConstants.addFault

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters AreaID
            Dim AreaID As ParameterBO = New ParameterBO("AreaID", faultBO.AreaID, DbType.Int32)
            inParamList.Add(AreaID)

            '' Creating input ParameterBO objects and passing prameters description
            Dim description As ParameterBO = New ParameterBO("DESCRIPTION", faultBO.Description, DbType.AnsiString)
            inParamList.Add(description)

            '' Creating input ParameterBO objects and passing prameters priorityID
            Dim priorityID As ParameterBO = New ParameterBO("PRIORITYID", faultBO.PriorityID, DbType.Int32)
            inParamList.Add(priorityID)

            '' Creating input ParameterBO objects and passing prameters Sub Contractor
            Dim duration As ParameterBO = New ParameterBO("DURATION", faultBO.Duration, DbType.Double)
            inParamList.Add(duration)

            '' Creating input ParameterBO objects and passing prameters recharge
            Dim recharge As ParameterBO = New ParameterBO("RECHARGE", faultBO.Recharge, DbType.Boolean)
            inParamList.Add(recharge)

            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO
            If IsNothing(faultBO.NetCost) Then
                netCost = New ParameterBO("NETCOST", Nothing, DbType.Double)
            Else
                netCost = New ParameterBO("NETCOST", faultBO.NetCost, DbType.Double)
            End If
            inParamList.Add(netCost)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO
            If IsNothing(faultBO.VatRateID) Then
                vatRateID = New ParameterBO("VATRATEID", Nothing, DbType.Double)
            Else
                vatRateID = New ParameterBO("VATRATEID", faultBO.VatRateID, DbType.Double)
            End If
            inParamList.Add(vatRateID)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO
            If IsNothing(faultBO.Vat) Then
                vat = New ParameterBO("VAT", Nothing, DbType.Double)
            Else
                vat = New ParameterBO("VAT", faultBO.Vat, DbType.Double)
            End If
            inParamList.Add(vat)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO
            If IsNothing(faultBO.Gross) Then
                gross = New ParameterBO("GROSS", Nothing, DbType.Double)
            Else
                gross = New ParameterBO("GROSS", faultBO.Gross, DbType.Double)
            End If
            inParamList.Add(gross)

            '' Creating input ParameterBO objects and passing prameters Sub Contractor
            Dim isContractor As ParameterBO = New ParameterBO("ISCONTRACTOR", faultBO.IsContractor, DbType.Int32)
            inParamList.Add(isContractor)

            '' Creating input ParameterBO objects and passing prameters faultActive
            Dim faultActive As ParameterBO = New ParameterBO("FAULTACTIVE", faultBO.FaultActive, DbType.Boolean)
            inParamList.Add(faultActive)

            '' Creating input ParameterBO objects and passing prameters TennatsOnline
            Dim tenantsOnline As ParameterBO = New ParameterBO("TENANTSONLINE", faultBO.TenantsOnline, DbType.Boolean)
            inParamList.Add(tenantsOnline)

            '' Creating input ParameterBO objects and passing prameters Gas Safe
            Dim gasSafe As ParameterBO = New ParameterBO("ISGASSAFE", faultBO.isGasSafe, DbType.Int32)
            inParamList.Add(gasSafe)

            '' Creating input ParameterBO objects and passing prameters OFTEC
            Dim OFTEC As ParameterBO = New ParameterBO("ISOFTEC", faultBO.isOFTEC, DbType.Int32)
            inParamList.Add(OFTEC)

            '' Creating input ParameterBO objects and passing prameters submitDate
            Dim submitDate As ParameterBO = New ParameterBO("SUBMITDATE", faultBO.SubmitDate, DbType.String)
            inParamList.Add(submitDate)

            '' Creating input ParameterBO objects and passing parameters userId
            Dim userID As ParameterBO = New ParameterBO("USERID", faultBO.UserID, DbType.Int32)
            inParamList.Add(userID)



            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            '' Creating output ParameterBO objects and passing prameters FaultID
            Dim FaultID As ParameterBO = New ParameterBO("FaultID", Nothing, DbType.Int32)
            outParamList.Add(FaultID)


            '' Passing Array of FaultBO and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value
            Dim returnFaultID As Integer = outParamList.Item(1).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                faultBO.IsFlagStatus = False
            Else
                'If qry Results Fault Addition is sucessfull add trades for that Fault.
                If (faultBO.Trades IsNot Nothing AndAlso faultBO.Trades.Rows.Count > 0) Then
                    For I As Integer = 0 To faultBO.Trades.Rows.Count - 1
                        Dim ReturnedFaultID As ParameterBO = New ParameterBO("FaultID", returnFaultID, DbType.Int32)
                        Dim TradeID As ParameterBO = New ParameterBO("TradeID", faultBO.Trades.Rows(I).Item(ApplicationConstants.tradeIdColumn), DbType.Int32)

                        Dim resultTrade As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)

                        inParamList.Clear()
                        outParamList.Clear()

                        inParamList.Add(ReturnedFaultID)
                        inParamList.Add(TradeID)

                        outParamList.Add(resultTrade)

                        sprocName = SpNameConstants.addFaultTradeRelation
                        MyBase.SaveRecord(inParamList, outParamList, sprocName)

                        Dim qryResultTrade As Integer = outParamList.Item(0).Value

                        If qryResultTrade = -1 Then
                            faultBO.IsFlagStatus = False
                            faultBO.ExceptionMsg = "Error Adding FaultTradeRelation to database"
                            Exit For
                        End If
                    Next
                End If

                faultBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "Amend A Fault"

        Public Sub amendFault(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SpNameConstants.amendFault


            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters FaultID
            Dim FaultID As ParameterBO = New ParameterBO("FaultID", faultBO.FaultID, DbType.Int32)
            inParamList.Add(FaultID)

            '' Creating input ParameterBO objects and passing prameters AreaID
            Dim AreaID As ParameterBO = New ParameterBO("AreaID", faultBO.AreaID, DbType.Int32)
            inParamList.Add(AreaID)

            '' Creating input ParameterBO objects and passing prameters description
            Dim description As ParameterBO = New ParameterBO("DESCRIPTION", faultBO.Description, DbType.AnsiString)
            inParamList.Add(description)

            '' Creating input ParameterBO objects and passing prameters priorityID
            Dim priorityID As ParameterBO = New ParameterBO("PRIORITYID", faultBO.PriorityID, DbType.Int32)
            inParamList.Add(priorityID)

            '' Creating input ParameterBO objects and passing prameters Sub Contractor
            Dim duration As ParameterBO = New ParameterBO("DURATION", faultBO.Duration, DbType.Double)
            inParamList.Add(duration)

            '' Creating input ParameterBO objects and passing prameters recharge
            Dim recharge As ParameterBO = New ParameterBO("RECHARGE", faultBO.Recharge, DbType.Int32)
            inParamList.Add(recharge)

            '' Creating input ParameterBO objects and passing prameters netCost
            Dim netCost As ParameterBO
            If IsNothing(faultBO.NetCost) Then
                netCost = New ParameterBO("NETCOST", Nothing, DbType.Double)
            Else
                netCost = New ParameterBO("NETCOST", faultBO.NetCost, DbType.Double)
            End If
            inParamList.Add(netCost)

            '' Creating input ParameterBO objects and passing prameters vatRateID
            Dim vatRateID As ParameterBO
            If IsNothing(faultBO.VatRateID) Then
                vatRateID = New ParameterBO("VATRATEID", Nothing, DbType.Double)
            Else
                vatRateID = New ParameterBO("VATRATEID", faultBO.VatRateID, DbType.Double)
            End If
            inParamList.Add(vatRateID)

            '' Creating input ParameterBO objects and passing prameters vat
            Dim vat As ParameterBO
            If IsNothing(faultBO.Vat) Then
                vat = New ParameterBO("VAT", Nothing, DbType.Double)
            Else
                vat = New ParameterBO("VAT", faultBO.Vat, DbType.Double)
            End If
            inParamList.Add(vat)

            '' Creating input ParameterBO objects and passing prameters gross
            Dim gross As ParameterBO
            If IsNothing(faultBO.Gross) Then
                gross = New ParameterBO("GROSS", Nothing, DbType.Double)
            Else
                gross = New ParameterBO("GROSS", faultBO.Gross, DbType.Double)
            End If
            inParamList.Add(gross)

            '' Creating input ParameterBO objects and passing prameters Sub Contractor
            Dim isContractor As ParameterBO = New ParameterBO("ISCONTRACTOR", faultBO.IsContractor, DbType.Boolean)
            inParamList.Add(isContractor)

            '' Creating input ParameterBO objects and passing prameters faultActive
            Dim faultActive As ParameterBO = New ParameterBO("FAULTACTIVE", faultBO.FaultActive, DbType.Boolean)
            inParamList.Add(faultActive)

            '' Creating input ParameterBO objects and passing prameters TennatsOnline
            Dim tenantsOnline As ParameterBO = New ParameterBO("TENANTSONLINE", faultBO.TenantsOnline, DbType.Boolean)
            inParamList.Add(tenantsOnline)

            '' Creating input ParameterBO objects and passing prameters Gas Safe
            Dim gasSafe As ParameterBO = New ParameterBO("ISGASSAFE", IIf(faultBO.isGasSafe, 1, 0), DbType.Int32)
            inParamList.Add(gasSafe)

            '' Creating input ParameterBO objects and passing prameters OFTEC
            Dim OFTEC As ParameterBO = New ParameterBO("ISOFTEC", IIf(faultBO.isOFTEC, 1, 0), DbType.Int32)
            inParamList.Add(OFTEC)

            '' Creating input ParameterBO objects and passing parameters userId
            Dim userID As ParameterBO = New ParameterBO("USERID", faultBO.UserID, DbType.Int32)
            inParamList.Add(userID)

            '' Creating input ParameterBO objects and passing prameters submitDate
            Dim submitDate As ParameterBO = New ParameterBO("SUBMITDATE", CType(faultBO.SubmitDate, DateTime), DbType.DateTime)
            inParamList.Add(submitDate)

            '' Creating input ParameterBO objects and passing prameters TradesToDelete
            Dim tradesToDelete As ParameterBO = New ParameterBO("TradesToDelete", Nothing, DbType.String)
            If faultBO.TradesToDelete IsNot Nothing AndAlso faultBO.TradesToDelete.Count > 0 Then
                Dim trades As String = String.Join(",", faultBO.TradesToDelete)
                tradesToDelete.Value = trades
            End If
            inParamList.Add(tradesToDelete)

            '' Creating input ParameterBO objects and passing prameters TradesToInsert
            Dim tradesToInsert As ParameterBO = New ParameterBO("TradesToInsert", Nothing, DbType.String)
            If faultBO.TradesToInsert IsNot Nothing AndAlso faultBO.TradesToInsert.Count > 0 Then
                Dim trades As String = String.Join(",", faultBO.TradesToInsert)
                tradesToInsert.Value = trades
            End If
            inParamList.Add(tradesToInsert)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            Dim messageout As ParameterBO = New ParameterBO("MESSAGE", Nothing, DbType.String)
            outParamList.Add(messageout)

            '' Passing Array of FaultBO Parameters INPUT and OUTPUT and sproc-name to base class method: SelectRecord
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value
            Dim message As String = IIf(IsDBNull(outParamList.Item(1).Value), String.Empty, outParamList.Item(1).Value)

            If qryResult = -1 Then
                faultBO.IsFlagStatus = False
                If Not (IsDBNull(message) OrElse message = String.Empty) Then
                    faultBO.ExceptionMsg = message
                End If
            ElseIf qryResult = 1 Then
                faultBO.IsFlagStatus = True
                faultBO.ExceptionMsg = String.Empty
            End If

            'If qryResult = -1 Then
            '    faultBO.IsFlagStatus = False
            'Else
            '    'Delete Existing FaultTrades Relation From Database for FaultID
            '    sprocName = SpNameConstants.deleteAllFaultsByFaultID
            '    inParamList.Clear()
            '    outParamList.Clear()

            '    inParamList.Add(FaultID)

            '    Dim resultTradeDelete As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            '    outParamList.Add(resultTradeDelete)

            '    MyBase.SaveRecord(inParamList, outParamList, sprocName)

            '    Dim qryResultTradeDelete As Integer = outParamList.Item(0).Value

            '    If qryResultTradeDelete = -1 Then
            '        faultBO.IsFlagStatus = False
            '        faultBO.ExceptionMsg = "Error Deleting FaultTrade Relation from database"
            '    End If

            '    'Add FaultTrades Relation to Database for FaultID
            '    If (faultBO.Trades IsNot Nothing AndAlso faultBO.Trades.Rows.Count > 0) Then
            '        For I As Integer = 0 To faultBO.Trades.Rows.Count - 1

            '            Dim TradeID As ParameterBO = New ParameterBO("TradeID", faultBO.Trades.Rows(I).Item(ApplicationConstants.tradeIdColumn), DbType.Int32)
            '            Dim resultTrade As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)

            '            inParamList.Clear()
            '            outParamList.Clear()

            '            inParamList.Add(FaultID)
            '            inParamList.Add(TradeID)

            '            outParamList.Add(resultTrade)

            '            sprocName = SpNameConstants.addFaultTradeRelation
            '            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            '            Dim qryResultTrade As Integer = outParamList.Item(0).Value

            '            If qryResultTrade = -1 Then
            '                faultBO.IsFlagStatus = False
            '                faultBO.ExceptionMsg = "Error Adding FaultTradeRelation to database"
            '                Exit For
            '            End If
            '        Next
            '    End If

            '    faultBO.IsFlagStatus = True
            'End If

        End Sub

#End Region

#Region "Save fault info"

        Public Sub saveFaultInfo(ByRef objFaultBO As FaultBO)

            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim descriptionParam As ParameterBO = New ParameterBO("description", objFaultBO.Description, DbType.AnsiString)
            inParamList.Add(descriptionParam)

            Dim priorityIdParam As ParameterBO = New ParameterBO("priorityId", objFaultBO.PriorityID, DbType.Int32)
            inParamList.Add(priorityIdParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", objFaultBO.Duration, DbType.Double)
            inParamList.Add(durationParam)

            Dim rechargeParam As ParameterBO = New ParameterBO("recharge", objFaultBO.Recharge, DbType.Boolean)
            inParamList.Add(rechargeParam)

            Dim netCostParam As ParameterBO = New ParameterBO("netcost", objFaultBO.NetCost, DbType.Double)
            inParamList.Add(netCostParam)

            Dim vatRateIdParam As ParameterBO = New ParameterBO("vatRateId", objFaultBO.VatRateID, DbType.Double)
            inParamList.Add(vatRateIdParam)

            Dim vatParam As ParameterBO = New ParameterBO("vat", objFaultBO.Vat, DbType.Double)
            inParamList.Add(vatParam)

            Dim grossParam As ParameterBO = New ParameterBO("gross", objFaultBO.Gross, DbType.Double)
            inParamList.Add(grossParam)

            Dim isContractorParam As ParameterBO = New ParameterBO("isContractor", objFaultBO.IsContractor, DbType.Int32)
            inParamList.Add(isContractorParam)

            Dim faultActiveParam As ParameterBO = New ParameterBO("isFaultActive", objFaultBO.FaultActive, DbType.Boolean)
            inParamList.Add(faultActiveParam)

            '' Creating input ParameterBO objects and passing prameters TennatsOnline
            Dim tenantsOnline As ParameterBO = New ParameterBO("TENANTSONLINE", objFaultBO.TenantsOnline, DbType.Boolean)
            inParamList.Add(tenantsOnline)

            Dim gasSafeParam As ParameterBO = New ParameterBO("isGasSafe", objFaultBO.isGasSafe, DbType.Int32)
            inParamList.Add(gasSafeParam)

            Dim oftecParam As ParameterBO = New ParameterBO("isOftec", objFaultBO.isOFTEC, DbType.Int32)
            inParamList.Add(oftecParam)

            Dim submitDateParam As ParameterBO = New ParameterBO("submitDate", CType(objFaultBO.SubmitDate, DateTime), DbType.DateTime)
            inParamList.Add(submitDateParam)

            Dim faultTradeParam As New ParameterBO("faultTrades", objFaultBO.Trades, SqlDbType.Structured)
            inParamList.Add(faultTradeParam)

            Dim faultRepairsParam As New ParameterBO("faultRepairs", objFaultBO.FaultRepairs, SqlDbType.Structured)
            inParamList.Add(faultRepairsParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", objFaultBO.UserID, DbType.Int32)
            inParamList.Add(userIdParam)

            Dim faultIdParam As ParameterBO = New ParameterBO("faultId", objFaultBO.FaultID, DbType.Int32)
            inParamList.Add(faultIdParam)

            Dim resultFaultIdParam As ParameterBO = New ParameterBO("resultFaultId", Nothing, DbType.Int32)
            outParamList.Add(resultFaultIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("isSaved", Nothing, DbType.Int32)
            outParamList.Add(resultParam)

            Dim sprocName As String = SpNameConstants.saveFault
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim returnFaultID As Integer = outParamList.Item(0).Value
            objFaultBO.FaultID = returnFaultID
            Dim qryResult As Integer = outParamList.Item(1).Value

            ''In the case of invalid information provided
            If qryResult = -1 Then
                objFaultBO.IsFlagStatus = False
            Else
                objFaultBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "Get Fault Values"

        Sub getFaultValues(ByRef objFaultBO As FaultBO)

            Dim faultGeneralDt As DataTable = New DataTable(ApplicationConstants.FaultGeneralInfoDataTable)
            Dim faultTradesDt As DataTable = New DataTable(ApplicationConstants.FaultTradesDataTable)
            Dim faultRepairsDt As DataTable = New DataTable(ApplicationConstants.FaultRepairsDataTable)

            Dim parametersList As ParameterList = New ParameterList()
            Dim faultIdParams As ParameterBO = New ParameterBO("faultID", objFaultBO.FaultID, DbType.Int32)
            parametersList.Add(faultIdParams)

            Dim resultDataSet As New DataSet()
            resultDataSet.Tables.Add(faultGeneralDt)
            resultDataSet.Tables.Add(faultTradesDt)
            resultDataSet.Tables.Add(faultRepairsDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.getFaultValuesByID)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, faultGeneralDt, faultTradesDt, faultRepairsDt)

            If resultDataSet.Tables.Count > 0 Then

                '' Fault general information
                If (faultGeneralDt.Rows.Count > 0) Then

                    objFaultBO.Description = faultGeneralDt.Rows(0).Item("Description").ToString()
                    objFaultBO.PriorityID = CType(faultGeneralDt.Rows(0).Item("PriorityID"), Integer)

                    If (IsDBNull(faultGeneralDt.Rows(0).Item("Duration"))) Then
                        objFaultBO.Duration = Nothing
                    Else
                        objFaultBO.Duration = CType(faultGeneralDt.Rows(0).Item("Duration"), Double)
                    End If

                    objFaultBO.Recharge = CType(faultGeneralDt.Rows(0).Item("Recharge"), Boolean)

                    If (IsDBNull(faultGeneralDt.Rows(0).Item("NetCost"))) Then
                        objFaultBO.NetCost = Nothing
                    Else
                        objFaultBO.NetCost = CType(faultGeneralDt.Rows(0).Item("NetCost"), Double)
                    End If

                    If (IsDBNull(faultGeneralDt.Rows(0).Item("VatRateID"))) Then
                        objFaultBO.VatRateID = Nothing
                    Else
                        objFaultBO.VatRateID = CType(faultGeneralDt.Rows(0).Item("VatRateID"), Integer)
                    End If

                    If (IsDBNull(faultGeneralDt.Rows(0).Item("Vat"))) Then
                        objFaultBO.Vat = Nothing
                    Else
                        objFaultBO.Vat = CType(faultGeneralDt.Rows(0).Item("Vat"), Double)
                    End If

                    If (IsDBNull(faultGeneralDt.Rows(0).Item("Gross"))) Then
                        objFaultBO.Gross = Nothing
                    Else
                        objFaultBO.Gross = CType(faultGeneralDt.Rows(0).Item("Gross"), Double)
                    End If

                    If IsDBNull(faultGeneralDt.Rows(0).Item("isContractor")) Then
                        objFaultBO.IsContractor = False
                    Else
                        objFaultBO.IsContractor = CType(faultGeneralDt.Rows(0).Item("isContractor"), Boolean)
                    End If

                    If IsDBNull(faultGeneralDt.Rows(0).Item("FaultActive")) Then
                        objFaultBO.FaultActive = False
                    Else
                        objFaultBO.FaultActive = CType(faultGeneralDt.Rows(0).Item("FaultActive"), Boolean)
                    End If


                    If IsDBNull(faultGeneralDt.Rows(0).Item("TenantsOnline")) Then
                        objFaultBO.TenantsOnline = False
                    Else
                        objFaultBO.TenantsOnline = CType(faultGeneralDt.Rows(0).Item("TenantsOnline"), Boolean)
                    End If


                    If IsDBNull(faultGeneralDt.Rows(0).Item("IsGasSafe")) Then
                        objFaultBO.isGasSafe = False
                    Else
                        objFaultBO.isGasSafe = CType(faultGeneralDt.Rows(0).Item("IsGasSafe"), Boolean)
                    End If

                    If IsDBNull(faultGeneralDt.Rows(0).Item("IsOftec")) Then
                        objFaultBO.isOFTEC = False
                    Else
                        objFaultBO.isOFTEC = CType(faultGeneralDt.Rows(0).Item("IsOftec"), Boolean)
                    End If

                End If

                '' Fault Trades information
                objFaultBO.Trades = faultTradesDt

                '' Fault Repairs information
                objFaultBO.FaultRepairs = faultRepairsDt

            End If

        End Sub

#End Region

#Region "Check fault trade in use"

        Function checkFaultTradeInUse(ByVal faultTradeId As Integer) As Boolean

            Dim inParamList As ParameterList = New ParameterList()
            Dim faultTradeIdParam As ParameterBO = New ParameterBO("faultTradeId", faultTradeId, DbType.Int32)
            inParamList.Add(faultTradeIdParam)

            Dim outParamList As ParameterList = New ParameterList()
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Boolean)
            outParamList.Add(result)

            Dim sprocName As String = SpNameConstants.checkFaultTradeInUse
            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            Return outParamList.Item(0).Value

        End Function

#End Region

#Region "Get Lookup Values"

        Private Sub getLookupValuesBySpNameNoFilter(ByRef lskLookUp As List(Of LookUpBO), ByVal spName As String, Optional ByVal parameter As ParameterList = Nothing)
            Dim myDataReader As IDataReader = MyBase.SelectRecord(parameter, spName)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookUpBO(id, name)
                lskLookUp.Add(objLookUp)
            End While
        End Sub

        Sub getAreaLookUpValuesAll(ByRef lstLookUp As List(Of LookUpBO))
            Dim sprocName As String = SpNameConstants.getAreaLookupAll
            getLookupValuesBySpNameNoFilter(lstLookUp, sprocName)
        End Sub

        Sub getRepairsLookUpValues(ByRef lstLookUp As List(Of LookUpBO))
            Dim sprocName As String = SpNameConstants.getRepairsLookup
            getLookupValuesBySpNameNoFilter(lstLookUp, sprocName)
        End Sub

        Sub getPriorityLookUpValuesAll(ByVal lstLookUp As List(Of LookUpBO))
            Dim sprocName As String = SpNameConstants.getPriorityLookup
            getLookupValuesBySpNameNoFilter(lstLookUp, sprocName)
        End Sub

        Sub getTradeLookUpValuesAll(ByRef lstLookUp As List(Of LookUpBO))
            Dim sprocName As String = SpNameConstants.getTradeLookup
            getLookupValuesBySpNameNoFilter(lstLookUp, sprocName)
        End Sub

        Sub getVatRateLookUpValuesAll(ByVal lstLookUp As List(Of LookUpBO))
            Dim sprocName As String = SpNameConstants.getVatRateLookUp
            getLookupValuesBySpNameNoFilter(lstLookUp, sprocName)
        End Sub

#End Region

#Region "Get Repair Search Result"
        ''' <summary>
        ''' Get Repair Search Result
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="searchText"></param>
        ''' <remarks></remarks>
        Public Sub getRepairSearchResult(ByRef resultDataSet As DataSet, ByRef searchText As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim dtRepairResult As DataTable = New DataTable()
            dtRepairResult.TableName = ApplicationConstants.RepairSearchResultDataTable
            resultDataSet.Tables.Add(dtRepairResult)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.SearchFaultRepair)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRepairResult)

        End Sub

#End Region

    End Class

End Namespace
