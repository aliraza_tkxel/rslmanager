﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports FLS_Utilities
Imports FLS_BusinessObject
Namespace FLS_DataAccess
    Public Class FaultDAL : Inherits BaseDAL

#Region "Functions"

#Region "Save Fault Information"

        Public Function saveFaultInfo(ByVal objFaultBO As FaultBO) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultIdParam As ParameterBO = New ParameterBO("faultId", objFaultBO.FaultID, DbType.Int32)
            parametersList.Add(faultIdParam)

            If (objFaultBO.SchemeId <> 0) Then
                Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", objFaultBO.SchemeId, DbType.Int32)
                parametersList.Add(schemeIdParam)

                Dim blockIdParam As ParameterBO = New ParameterBO("blockId", objFaultBO.BlockId, DbType.Int32)
                parametersList.Add(blockIdParam)

                Dim customerIdParam As ParameterBO = New ParameterBO("customerId", Nothing, DbType.Int32)
                parametersList.Add(customerIdParam)

                Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", Nothing, DbType.String)
                parametersList.Add(propertyIdParam)
            Else
                Dim schemeIdParam As ParameterBO = New ParameterBO("schemeId", Nothing, DbType.Int32)
                parametersList.Add(schemeIdParam)

                Dim blockIdParam As ParameterBO = New ParameterBO("blockId", Nothing, DbType.Int32)
                parametersList.Add(blockIdParam)

                Dim customerIdParam As ParameterBO = New ParameterBO("customerId", objFaultBO.CustomerId, DbType.Int32)
                parametersList.Add(customerIdParam)

                Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objFaultBO.PropertyId, DbType.String)
                parametersList.Add(propertyIdParam)

            End If

            Dim quantityParam As ParameterBO = New ParameterBO("quantity", objFaultBO.Quantity, DbType.Int32)
            parametersList.Add(quantityParam)

            Dim locationIdParam As ParameterBO = New ParameterBO("locationId", objFaultBO.LocationID, DbType.Int32)
            parametersList.Add(locationIdParam)

            Dim problemDaysParam As ParameterBO = New ParameterBO("problemDays", objFaultBO.ProblemDays, DbType.Int32)
            parametersList.Add(problemDaysParam)

            Dim recuringProblemParam As ParameterBO = New ParameterBO("recuringProblem", objFaultBO.RecuringProblem, DbType.Int32)
            parametersList.Add(recuringProblemParam)

            'Dim communalProblemParam As ParameterBO = New ParameterBO("communalProblem", objFaultBO.CommunalProblem, DbType.Int32)
            'parametersList.Add(communalProblemParam)

            Dim rechargeParam As ParameterBO = New ParameterBO("recharge", objFaultBO.Recharge, DbType.Int32)
            parametersList.Add(rechargeParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", objFaultBO.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim recallParam As ParameterBO = New ParameterBO("isRecall", objFaultBO.IsRecall, DbType.Boolean)
            parametersList.Add(recallParam)


            Dim originalFaultLogIdParam As ParameterBO = New ParameterBO("originalFaultLogId", objFaultBO.OriginalFaultLogId, DbType.Int32)
            parametersList.Add(originalFaultLogIdParam)

            Dim isFollowOnParam As ParameterBO = New ParameterBO("isFollowOn", objFaultBO.IsFollowOn, DbType.Boolean)
            parametersList.Add(isFollowOnParam)

            Dim followOnFaultLogId As ParameterBO = New ParameterBO("followOnFaultLogId", objFaultBO.FollowOnFaultLogId, DbType.Int32)
            parametersList.Add(followOnFaultLogId)

            Dim tempFaultIdParam As ParameterBO = New ParameterBO("tempFaultId", 0, DbType.Int32)
            outParametersList.Add(tempFaultIdParam)

            'This procedure will save the record in Fault Temporary table
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SaveFaultTemporary)
            Return CType(outParametersList.Item(0).Value, Int32).ToString()

        End Function

#End Region

#Region "Get Fault Basket Information"
        Sub getFaultBasket(ByVal objFaultBO As FaultBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", objFaultBO.CustomerId, DbType.String)
            parametersList.Add(customerIdParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objFaultBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", objFaultBO.IsRecall, DbType.Boolean)
            parametersList.Add(isRecallParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFaultBasketInfo)
        End Sub
#End Region

#Region "Get scheme/Block Fault Basket Information"
        Sub getSbFaultBasket(ByVal objFaultBO As FaultBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim customerIdParam As ParameterBO = New ParameterBO("schemeId", objFaultBO.SchemeId, DbType.Int32)
            parametersList.Add(customerIdParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("blockId", objFaultBO.BlockId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", objFaultBO.IsRecall, DbType.Boolean)
            parametersList.Add(isRecallParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetSbFaultBasketInfo)
        End Sub
#End Region

#Region "Get Faults and Appointments"

        Public Sub GetFaultsAndAppointments(ByRef resultDataSet As DataSet, ByVal customerId As Integer, ByVal propertyId As String)
            Dim parametersList As ParameterList = New ParameterList()
            Dim dtCompleted As DataTable = New DataTable()
            Dim dtNotCompleted As DataTable = New DataTable()
            Dim dtGasServiceAppointments As DataTable = New DataTable()
            Dim dtPlannedAppointments As DataTable = New DataTable()

            dtPlannedAppointments.TableName = ApplicationConstants.PlannedDataTable
            dtCompleted.TableName = ApplicationConstants.RecallFaultsDataTable
            dtNotCompleted.TableName = ApplicationConstants.CurrentFaultsDataTable
            dtGasServiceAppointments.TableName = ApplicationConstants.GasAppointmentssDataTable

            resultDataSet.Tables.Add(dtNotCompleted)
            resultDataSet.Tables.Add(dtCompleted)
            resultDataSet.Tables.Add(dtGasServiceAppointments)
            resultDataSet.Tables.Add(dtPlannedAppointments)

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", customerId, DbType.Int32)
            parametersList.Add(customerIdParam)
            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFaultsAndAppointments)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtNotCompleted, dtCompleted, dtGasServiceAppointments, dtPlannedAppointments)
        End Sub

#End Region

#Region "Get Warranties List"

        Public Function GetWarratiesList(ByRef resultDataSet As DataSet, ByVal propertyId As String) As DataSet
            Dim paramList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()
            Dim paramBlockId As ParameterBO = New ParameterBO("Id", propertyId, DbType.String)
            paramList.Add(paramBlockId)

            LoadDataSet(resultDataSet, paramList, outParametersList, SpNameConstants.GetWarrantyList)

        End Function
#End Region

#Region "Get Faults and Appointments"

        Public Sub GetFaultsAndAppointments(ByRef resultDataSet As DataSet, ByVal schemeId As Integer, ByVal blockId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim dtCompleted As DataTable = New DataTable()
            Dim dtNotCompleted As DataTable = New DataTable()
            Dim dtGasServiceAppointments As DataTable = New DataTable()
            Dim dtPlannedAppointments As DataTable = New DataTable()

            dtPlannedAppointments.TableName = ApplicationConstants.PlannedDataTable
            dtCompleted.TableName = ApplicationConstants.RecallFaultsDataTable
            dtNotCompleted.TableName = ApplicationConstants.CurrentFaultsDataTable
            dtGasServiceAppointments.TableName = ApplicationConstants.GasAppointmentssDataTable

            resultDataSet.Tables.Add(dtNotCompleted)
            resultDataSet.Tables.Add(dtCompleted)
            resultDataSet.Tables.Add(dtGasServiceAppointments)
            resultDataSet.Tables.Add(dtPlannedAppointments)

            Dim customerIdParam As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(customerIdParam)
            Dim propertyIdParam As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSbFaultsAndAppointments)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtNotCompleted, dtCompleted, dtGasServiceAppointments, dtPlannedAppointments)
        End Sub

#End Region

#Region "Get Recall Detail"

        Public Sub GetRecallDetails(ByRef resultDataSet As DataSet, ByVal faultLogId As Integer)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultAndAppointment As DataTable = New DataTable()
                Dim dtAsbestos As DataTable = New DataTable()
                Dim dtRepairDetail As DataTable = New DataTable()

                dtFaultAndAppointment.TableName = "FaultAppointment"
                dtAsbestos.TableName = "Asbestos"
                dtRepairDetail.TableName = "RepairDetail"

                resultDataSet.Tables.Add(dtFaultAndAppointment)
                resultDataSet.Tables.Add(dtAsbestos)
                resultDataSet.Tables.Add(dtRepairDetail)

                Dim faultLog As ParameterBO = New ParameterBO("faultLogID", faultLogId, DbType.Int32)
                parametersList.Add(faultLog)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRecallDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultAndAppointment, dtAsbestos, dtRepairDetail)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get Contractors By Trade"

        Public Sub GetContractorsByTrade(ByRef resultDataSet As DataSet, ByVal tradeType As String)
            Try
                Dim parametersList As ParameterList = New ParameterList()
                Dim dtContractors As DataTable = New DataTable()

                dtContractors.TableName = "Contractors"
                resultDataSet.Tables.Add(dtContractors)

                Dim faultLog As ParameterBO = New ParameterBO("tradeType", tradeType, DbType.String)
                parametersList.Add(faultLog)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.FL_GetContractorsByTrade)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtContractors)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Delete Fault from Basket"

        Public Function deleteFaultFromBasket(ByVal tempFaultId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultLog As ParameterBO = New ParameterBO("TempFaultId", tempFaultId, DbType.Int32)
            parametersList.Add(faultLog)

            Dim resultParam As ParameterBO = New ParameterBO("Result", 0, DbType.Int32)
            outParametersList.Add(resultParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.FL_DeleteFaultFromBasket)
            Return CType(outParametersList.Item(0).Value, Int32)


        End Function

#End Region

#Region "Update Contractor in Basket"

        Public Sub updateContractorFromBasket(ByVal tempFaultId As Integer, ByVal contractorID As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim faultLog As ParameterBO = New ParameterBO("TempFaultId", tempFaultId, DbType.Int32)
            parametersList.Add(faultLog)

            Dim OrgID As ParameterBO = New ParameterBO("Orgid", contractorID, DbType.Int32)
            parametersList.Add(OrgID)

            MyBase.SelectRecord(parametersList, SpNameConstants.FL_UpdateOrgId)

        End Sub

#End Region

#Region "Get Appointment Detail"

        Public Sub getAppointmentDetail(ByRef resultDataSet As DataSet, ByVal faultLogId As Integer)
            Try
                Dim parametersList As ParameterList = New ParameterList()
                Dim dtAppointment As DataTable = New DataTable()

                dtAppointment.TableName = "AppointmentDetail"
                resultDataSet.Tables.Add(dtAppointment)

                Dim faultLog As ParameterBO = New ParameterBO("faultLogID", faultLogId, DbType.Int32)
                parametersList.Add(faultLog)

                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.FL_GetAppointmentDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointment)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get Fault List"
        Public Sub getFaultsList(ByRef resultDataSet As DataSet, ByVal faultsListString As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim faultListParam As ParameterBO = New ParameterBO("faultsList", faultsListString, DbType.String)
            parametersList.Add(faultListParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFaultsList)

        End Sub

#End Region

#Region "Get Appointment Data"
        Public Sub getAppointmentData(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim appointmentParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentData)

        End Sub
#End Region

#Region "Get Gas Appointment Data"
        Public Sub getGasAppointmentData(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim appointmentParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetGasAppointmentData)

        End Sub
#End Region

#Region "Update Fault Notes"
        Public Function updateFaultNotes(ByVal text As String, ByVal accessNotes As String, ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim appointmentParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", text, DbType.String)
            parametersList.Add(notesParam)

            Dim accessNotesParam As ParameterBO = New ParameterBO("accessnotes", accessNotes, DbType.String)
            parametersList.Add(accessNotesParam)

            Dim result As ParameterBO = New ParameterBO("@isUpdated", Nothing, DbType.Int32)
            outParamList.Add(result)

            MyBase.SaveRecord(parametersList, outParamList, SpNameConstants.UpdateFaultNote)

            'Out parameter will return 1 in case of success or 0 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            If qryResult = -1 Then
                Return -1
            Else
                Return 1
            End If

            'MyBase.SelectRecord(parametersList, SpNameConstants.UpdateFaultNote)

        End Function
#End Region

#Region "Update Gas Notes"
        Public Function updateGasNotes(ByVal text As String, ByVal appointmentId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim appointmentParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", text, DbType.String)
            parametersList.Add(notesParam)

            Dim result As ParameterBO = New ParameterBO("@isUpdated", Nothing, DbType.Int32)
            outParamList.Add(result)

            MyBase.SaveRecord(parametersList, outParamList, SpNameConstants.UpdateGasNote)

            'Out parameter will return 1 in case of success or 0 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            If qryResult = -1 Then
                Return -1
            Else
                Return 1
            End If

            'MyBase.SelectRecord(parametersList, SpNameConstants.UpdateFaultNote)

        End Function
#End Region

#Region "Save Appointment Details"
        Public Sub saveAppointmentDetails(ByVal appointmentSummary As AppointmentSummaryBO, ByRef appointmentId As Integer, ByRef tempCount As Integer)

            Dim inparametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim faultsListParam As New ParameterBO("faultsList", appointmentSummary.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim operativeIdParam As New ParameterBO("operativeId", appointmentSummary.OperativeId, DbType.Int32)
            inparametersList.Add(operativeIdParam)

            Dim appointmentDateParam As New ParameterBO("appointmentDate", appointmentSummary.AppointmentStartDate.Date, DbType.DateTime)
            inparametersList.Add(appointmentDateParam)

            Dim durationParam As New ParameterBO("duration", appointmentSummary.Duration, DbType.Double)
            inparametersList.Add(durationParam)

            Dim faultNotesParam As New ParameterBO("faultNotes", appointmentSummary.FaultNotes, DbType.String)
            inparametersList.Add(faultNotesParam)

            Dim timeParam As New ParameterBO("time", appointmentSummary.Time, DbType.String)
            inparametersList.Add(timeParam)

            Dim endTimeParam As New ParameterBO("endTime", appointmentSummary.EndTime, DbType.String)
            inparametersList.Add(endTimeParam)

            Dim customerIdParam As New ParameterBO("CustomerID", appointmentSummary.CustomerId, DbType.Int32)
            inparametersList.Add(customerIdParam)

            Dim propertyIdParam As New ParameterBO("PropertyId", appointmentSummary.PropertyId, DbType.String)
            inparametersList.Add(propertyIdParam)

            Dim isRecallParam As New ParameterBO("isRecall", appointmentSummary.IsRecall, DbType.Boolean)
            inparametersList.Add(isRecallParam)

            Dim isCalendarAppointmentParam As New ParameterBO("isCalendarAppointment", appointmentSummary.IsCalendarAppointment, DbType.Boolean)
            inparametersList.Add(isCalendarAppointmentParam)

            Dim appointmentNotesParam As New ParameterBO("appointmentNotes", appointmentSummary.AppointmentNotes, DbType.String)
            inparametersList.Add(appointmentNotesParam)

            Dim userIdParam As New ParameterBO("userId", appointmentSummary.UserId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim appointmentEndDateParam As New ParameterBO("appointmentEndDate", appointmentSummary.AppointmentEndDate.Date, DbType.DateTime)
            inparametersList.Add(appointmentEndDateParam)

            Dim appointmentIdParam As New ParameterBO("appointmentID", Nothing, DbType.Int32)
            outParametersList.Add(appointmentIdParam)

            Dim tempCountParam As New ParameterBO("tempCount", Nothing, DbType.Int32)
            outParametersList.Add(tempCountParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveAppointmentDetails)

            appointmentId = outParametersList.ElementAt(0).Value
            tempCount = outParametersList.ElementAt(1).Value

        End Sub
#End Region

#Region "Get Appointment Summary"
        Public Sub getAppointmentSummary(ByRef resultDataSet As DataSet, ByVal faultsList As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", faultsList, DbType.String)
            parametersList.Add(faultsListParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetAppointmentSummary)

        End Sub

#End Region

#Region "Save Fault Cancellation"
        Public Function saveFaultCancellation(ByVal appointmentCancellation As AppointmentCancellationBO)

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", appointmentCancellation.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", appointmentCancellation.Notes, DbType.String)
            inparametersList.Add(notesParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", appointmentCancellation.userId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveFaultCancellation)

            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Save Gas Servicing Cancellation"
        Public Function saveGasServicingCancellation(ByVal appointmentCancellation As AppointmentCancellationBO)

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", appointmentCancellation.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", appointmentCancellation.Notes, DbType.String)
            inparametersList.Add(notesParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", appointmentCancellation.userId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveGasServicingCancellation)

            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Get Job Sheet Detail"

        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultAndAppointment As DataTable = New DataTable()
                Dim dtAsbestos As DataTable = New DataTable()
                Dim dtCustomer As DataTable = New DataTable()

                dtFaultAndAppointment.TableName = "FaultAppointment"
                dtAsbestos.TableName = "Asbestos"
                dtCustomer.TableName = "Customer"


                resultDataSet.Tables.Add(dtFaultAndAppointment)
                resultDataSet.Tables.Add(dtAsbestos)
                resultDataSet.Tables.Add(dtCustomer)


                Dim jsn As ParameterBO = New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
                parametersList.Add(jsn)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJobSheetDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultAndAppointment, dtAsbestos, dtCustomer)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get Subcontractor Job Sheet Detail"

        Public Sub getSubcontractorJobSheetDetails(ByRef resultDataSet As DataSet, ByVal objTempFaultBO As TempFaultBO, ByVal propertyId As String)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultDetail As DataTable = New DataTable()
                Dim dtAsbestos As DataTable = New DataTable()

                dtFaultDetail.TableName = "FaultDetail"
                dtAsbestos.TableName = "Asbestos"

                resultDataSet.Tables.Add(dtFaultDetail)
                resultDataSet.Tables.Add(dtAsbestos)

                Dim tempFaultIdParam As ParameterBO = New ParameterBO("tempfaultId", objTempFaultBO.TempFaultId, DbType.Int32)
                parametersList.Add(tempFaultIdParam)
                Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objTempFaultBO.Trade, DbType.Int32)
                parametersList.Add(tradeIdParam)
                Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
                parametersList.Add(propertyIdParam)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSubcontractorJobSheetDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultDetail, dtAsbestos)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Save Subcontractor Fault"

        Public Function saveSubcontractorFault(ByVal objFaultBO As FaultBO, ByVal userId As Integer, ByVal tempfaultId As Integer) As String

            Try

                Dim parametersList As ParameterList = New ParameterList()
                Dim outParametersList As ParameterList = New ParameterList()

                Dim customerIdParam As ParameterBO = New ParameterBO("CustomerID", objFaultBO.CustomerId, DbType.Int32)
                parametersList.Add(customerIdParam)

                Dim faultIdParam As ParameterBO = New ParameterBO("FaultId", objFaultBO.FaultID, DbType.Int32)
                parametersList.Add(faultIdParam)

                Dim orgIdParam As ParameterBO = New ParameterBO("ORGID", objFaultBO.OrgID, DbType.Int32)
                parametersList.Add(orgIdParam)

                Dim quantityParam As ParameterBO = New ParameterBO("Quantity", objFaultBO.Quantity, DbType.Int32)
                parametersList.Add(quantityParam)

                Dim problemDaysParam As ParameterBO = New ParameterBO("ProblemDays", objFaultBO.ProblemDays, DbType.Int32)
                parametersList.Add(problemDaysParam)

                Dim recuringProblemParam As ParameterBO = New ParameterBO("RecuringProblem", objFaultBO.RecuringProblem, DbType.Boolean)
                parametersList.Add(recuringProblemParam)

                Dim communalProblemParam As ParameterBO = New ParameterBO("CommunalProblem", objFaultBO.CommunalProblem, DbType.Boolean)
                parametersList.Add(communalProblemParam)

                Dim notesParam As ParameterBO = New ParameterBO("Notes", objFaultBO.Notes, DbType.String)
                parametersList.Add(notesParam)

                Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", objFaultBO.PropertyId, DbType.String)
                parametersList.Add(propertyIdParam)

                Dim faultTradeIdParam As ParameterBO = New ParameterBO("FaultTradeId", objFaultBO.FaultTradeId, DbType.Int32)
                parametersList.Add(faultTradeIdParam)

                Dim originalFaultlogIdParam As ParameterBO = New ParameterBO("OriginalFaultLogId", objFaultBO.OriginalFaultLogId, DbType.Int32)
                parametersList.Add(originalFaultlogIdParam)

                Dim rechargeParam As ParameterBO = New ParameterBO("Recharge", objFaultBO.Recharge, DbType.Boolean)
                parametersList.Add(rechargeParam)

                Dim tempFaultIdParam As ParameterBO = New ParameterBO("TempFaultID", tempfaultId, DbType.Int32)
                parametersList.Add(tempFaultIdParam)

                Dim userIdParam As ParameterBO = New ParameterBO("UserId", userId, DbType.Int32)
                parametersList.Add(userIdParam)

                Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", objFaultBO.IsRecall, DbType.Boolean)
                parametersList.Add(isRecallParam)

                Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.String)
                outParametersList.Add(resultParam)

                outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SaveSubcontractorFault)
                Return outParametersList.Item(0).Value.ToString()

            Catch ex As Exception
                Return String.Empty
            End Try

        End Function

#End Region

#Region "Get More Details"

        Public Sub getMoreDetails(ByRef resultDataSet As DataSet, ByVal faultId As Integer)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtGetMoreDetials As DataTable = New DataTable()
                dtGetMoreDetials.TableName = "MoreDetails"
                resultDataSet.Tables.Add(dtGetMoreDetials)

                Dim dtLocation As DataTable = New DataTable()
                dtLocation.TableName = "Locations"
                resultDataSet.Tables.Add(dtLocation)

                Dim faultIdParam As ParameterBO = New ParameterBO("faultId", faultId, DbType.Int32)
                parametersList.Add(faultIdParam)

                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetMoreDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtGetMoreDetials, dtLocation)
            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Delete All Temp fault"

        Public Function deleteAllFault(ByVal tempFaultId As String) As Integer
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim tempFaultIdParam As ParameterBO = New ParameterBO("TempFaultIDs", tempFaultId, DbType.String)
            parametersList.Add(tempFaultIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("DeletedRowCount", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeleteAllTemporaryFault)

            Return outParametersList.ElementAt(0).Value
        End Function

#End Region

#Region "Get Job Sheed Summary Detail By JSN(Job Sheet Number)"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)

            Dim inParametersList As New ParameterList

            Dim jsnparameter As ParameterBO = New ParameterBO("JSN", jsn, DbType.String)
            inParametersList.Add(jsnparameter)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.getJobSheetSummaryByJSN)

            ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable)

        End Sub

#End Region

#Region "Get Status Look Up For Job Sheet Summary Sub Contractor Update"

        Sub getFaultStatusLookUpSubContractor(ByRef LookUpList As List(Of LookUpBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.getFaultStatusLookUpSubContractor)

            ''Iterate the dataReader
            While (myDataReader.Read)

                Dim id As Integer
                Dim name As String = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("id")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("val")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("val"))
                End If
                Dim objLookUp As New LookUpBO(id, name)
                LookUpList.Add(objLookUp)
            End While
        End Sub

#End Region

#Region "UpdateFaultNotes"
        Sub updateFaultNotes(ByVal tempFaultId As Integer, ByVal notes As String)

            Dim sprocName As String = SpNameConstants.updateFaultNotes

            'Parameter List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim tempFaultIdParam As ParameterBO = New ParameterBO("tempFaultId", tempFaultId, DbType.Int32)
            inParamList.Add(tempFaultIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", notes, DbType.String)
            inParamList.Add(notesParam)

            Dim result As ParameterBO = New ParameterBO("@isUpdated", Nothing, DbType.Int32)
            outParamList.Add(result)

            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            'Out parameter will return 1 in case of success or 0 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            If qryResult = -1 Then
                'Not Able to Update
            Else
                'Updated
            End If

        End Sub
#End Region
#Region "Save/Set Fault Stauts in Database with Notes"

        Sub setFaultStatusSubContractorUpdate(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SpNameConstants.setFaultStatusSubContractorUpdate

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters FaultLogID
            Dim FaultLogID As ParameterBO = New ParameterBO("FaultLogID", faultBO.FaultID, DbType.Int32)
            inParamList.Add(FaultLogID)

            '' Creating input ParameterBO objects and passing prameters FaultStatusID
            Dim FaultStatusID As ParameterBO = New ParameterBO("FaultStatusID ", faultBO.StatusID, DbType.Int32)
            inParamList.Add(FaultStatusID)

            '' Creating input ParameterBO objects and passing prameters Notes
            Dim Notes As ParameterBO = New ParameterBO("Notes ", faultBO.Notes, DbType.String)
            inParamList.Add(Notes)

            '' Creating input ParameterBO objects and passing prameters LastActionDate
            Dim LastActionDate As ParameterBO = New ParameterBO("LastActionDate", faultBO.SubmitDate, DbType.DateTime)
            inParamList.Add(LastActionDate)

            '' Creating input ParameterBO objects and passing prameters NoEntryDate
            Dim NoEntryDate As ParameterBO = New ParameterBO("NoEntryDate", faultBO.NoEntryDate, DbType.DateTime)
            inParamList.Add(NoEntryDate)

            '' Creating input ParameterBO objects and passing prameters LastActionUserID
            Dim LastActionUserID As ParameterBO = New ParameterBO("LastActionUserID", faultBO.UserID, DbType.Int32)
            inParamList.Add(LastActionUserID)

            '' Creating input ParameterBO objects and passing prameters FollowOnNotes
            Dim FollowOnNotes As ParameterBO = New ParameterBO("FollowOnNotes", faultBO.FollowOnNotes, DbType.String)
            inParamList.Add(FollowOnNotes)

            '' Creating input ParameterBO objects and passing prameters isFollowon
            Dim isFollowon As ParameterBO = New ParameterBO("isFollowon", faultBO.IsFollowOn, DbType.Int32)
            inParamList.Add(isFollowon)

            Dim completedDate As ParameterBO = New ParameterBO("CompletedDate", faultBO.CompletedDate, DbType.DateTime)
            inParamList.Add(completedDate)

            '' Creating output ParameterBO objects and passing prameters result
            Dim result As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParamList.Add(result)

            MyBase.SaveRecord(inParamList, outParamList, sprocName)

            ''Out parameter will return the TerminationId in case of success or -1 otherwise
            Dim qryResult As Integer = outParamList.Item(0).Value

            If qryResult = -1 Then
                faultBO.IsFlagStatus = False
                faultBO.UserMsg = "Unable to save Fault status in database"
            Else
                faultBO.IsFlagStatus = True
            End If

        End Sub

#End Region

#Region "Cancel Fault Purchase Order"

        Public Function cancelFaultPurchaseOrder(ByRef faultBO As FaultBO)

            '' SprocNameConstants contains stored-procedures names, inside project Utilities\constants
            Dim sprocName As String = SpNameConstants.cancelPuchaseOrder

            '' Creating parameterBO type List
            Dim inParamList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            '' Creating input ParameterBO objects and passing prameters FaultLogID
            Dim FaultLogID As ParameterBO = New ParameterBO("FaultLogId", faultBO.FaultID, DbType.Int32)
            inParamList.Add(FaultLogID)

            '' Creating input ParameterBO objects and passing prameters UserId
            Dim UserId As ParameterBO = New ParameterBO("UserId", faultBO.UserID, DbType.Int32)
            inParamList.Add(UserId)

            Dim sendEmail As ParameterBO = New ParameterBO("sendEmail", faultBO.UserID, DbType.Int32)
            outParamList.Add(sendEmail)

            outParamList = MyBase.SelectRecord(inParamList, outParamList, sprocName)
            Return CType(outParamList.Item(0).Value, Int32)
        End Function

#End Region

#Region "Get Email Detail For Purchase Order"

        Sub getEmailDetail(ByRef faultBO As FaultBO, ByRef detailsForEmailDS As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim FaultLogId As ParameterBO = New ParameterBO("FaultLogId", faultBO.FaultID, DbType.String)
            parametersList.Add(FaultLogId)

            Dim PropertyId As ParameterBO = New ParameterBO("propertyId", faultBO.PropertyId, DbType.String)
            parametersList.Add(PropertyId)

            Dim BlockId As ParameterBO = New ParameterBO("BlockId", faultBO.BlockId, DbType.String)
            parametersList.Add(BlockId)


            Dim datareader = MyBase.SelectRecord(parametersList, SpNameConstants.FL_GetContractorEmailDetail)

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, _
                                   ApplicationConstants.ContractorDetailsDt _
                                   , ApplicationConstants.PropertyDetailsDT _
                                   , ApplicationConstants.BlockDetailsdt _
                                   , ApplicationConstants.FaultInfoDt)

        End Sub

#End Region

#Region "Save Fault Info ReArrange"

        Public Function saveFaultInfoRearrange(ByVal objFaultBO As FaultBO) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultIdParam As ParameterBO = New ParameterBO("faultId", objFaultBO.FaultID, DbType.Int32)
            parametersList.Add(faultIdParam)

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", objFaultBO.CustomerId, DbType.String)
            parametersList.Add(customerIdParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", objFaultBO.PropertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim quantityParam As ParameterBO = New ParameterBO("quantity", objFaultBO.Quantity, DbType.Int32)
            parametersList.Add(quantityParam)

            Dim problemDaysParam As ParameterBO = New ParameterBO("problemDays", objFaultBO.ProblemDays, DbType.Int32)
            parametersList.Add(problemDaysParam)

            Dim recuringProblemParam As ParameterBO = New ParameterBO("recuringProblem", objFaultBO.RecuringProblem, DbType.Int32)
            parametersList.Add(recuringProblemParam)

            'Dim communalProblemParam As ParameterBO = New ParameterBO("communalProblem", objFaultBO.CommunalProblem, DbType.Int32)
            'parametersList.Add(communalProblemParam)

            Dim rechargeParam As ParameterBO = New ParameterBO("recharge", objFaultBO.Recharge, DbType.Int32)
            parametersList.Add(rechargeParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", objFaultBO.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim recallParam As ParameterBO = New ParameterBO("isRecall", objFaultBO.IsRecall, DbType.Boolean)
            parametersList.Add(recallParam)

            Dim faultTradeIdParam As ParameterBO = New ParameterBO("faultTradeId", objFaultBO.FaultTradeId, DbType.Int32)
            parametersList.Add(faultTradeIdParam)

            Dim originalFaultLogIdParam As ParameterBO = New ParameterBO("originalFaultLogId", objFaultBO.OriginalFaultLogId, DbType.Int32)
            parametersList.Add(originalFaultLogIdParam)

            Dim isRearrangeParam As ParameterBO = New ParameterBO("isRearrange", objFaultBO.IsRearrange, DbType.Boolean)
            parametersList.Add(isRearrangeParam)

            Dim tempFaultIdParam As ParameterBO = New ParameterBO("tempFaultId", 0, DbType.Int32)
            outParametersList.Add(tempFaultIdParam)

            'This procedure will save the record in Fault Temporary table
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SaveFaultTemporaryRearrange)
            Return CType(outParametersList.Item(0).Value, Int32).ToString()

        End Function

#End Region


#Region "Save Fault Info ReArrange"

        Public Function updateFaultInfoRearrange(ByVal AppointmentBO As AppointmentBO, faultLogidList As String) As String

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim faultLogIdParam As New ParameterBO("faultLogIdList", faultLogidList, DbType.String)
            parametersList.Add(faultLogIdParam)

            Dim operativeIdParam As New ParameterBO("operativeId", AppointmentBO.OperativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim appointmentDateParam As New ParameterBO("appointmentDate", AppointmentBO.AppointmentStartDate.Date, DbType.DateTime)
            parametersList.Add(appointmentDateParam)

            Dim startTimeParam As New ParameterBO("appointmentStartTime", AppointmentBO.StartTime, DbType.String)
            parametersList.Add(startTimeParam)

            Dim endTimeParam As New ParameterBO("appointmentEndTime", AppointmentBO.EndTime, DbType.String)
            parametersList.Add(endTimeParam)

            Dim faultNotes As New ParameterBO("faultNotes", AppointmentBO.FaultNotes, DbType.String)
            parametersList.Add(faultNotes)

            Dim isCalendarAppointmentParam As New ParameterBO("isCalendarAppointment", AppointmentBO.IsCalendarAppointment, DbType.Boolean)
            parametersList.Add(isCalendarAppointmentParam)

            Dim faultLogListParam As New ParameterBO("faultLogList", AppointmentBO.FaultLogdt, SqlDbType.Structured)
            parametersList.Add(faultLogListParam)

            Dim appointmentEndDateParam As New ParameterBO("appointmentEndDate", AppointmentBO.AppointmentEndDate.Date, DbType.DateTime)
            parametersList.Add(appointmentEndDateParam)

            Dim resultParam As New ParameterBO("result", 0, DbType.Int32)
            outParametersList.Add(resultParam)

            'This procedure will save the record in Fault Temporary table
            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.RearrangeAppointment)
            Return CType(outParametersList.Item(0).Value, Int32).ToString()

        End Function

#End Region

#Region "Get confirmed faults based on JSN"

        Public Sub getConfirmedFaultsByJSN(ByRef resultDataSet As DataSet, ByVal jsn As String)
            Try
                Dim parametersList As ParameterList = New ParameterList()
                Dim dtConfirmedFaults As DataTable = New DataTable()

                'dtConfirmedFaults.TableName = "ConfirmedFaults"

                'resultDataSet.Tables.Add(dtConfirmedFaults)

                Dim jsnParam As ParameterBO = New ParameterBO("jsn", jsn, DbType.String)
                parametersList.Add(jsnParam)


                'Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.ConfirmedAppointmentByJsn)
                'resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtConfirmedFaults)


                'Dim parametersList As ParameterList = New ParameterList()

                'Dim faultIdParam As ParameterBO = New ParameterBO("appointmentId", objCalendarAppointmentsBO.AppointmentId, DbType.Int32)
                'parametersList.Add(faultIdParam)

                MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.ConfirmedAppointmentByJsn)
            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get JobsheetNumber list by Appointment Id"

        Public Sub getJobsheetlistByAppointment(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentId", appointmentId, DbType.Int32)
                parametersList.Add(appointmentIdParam)

                MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetJobSheetByAppointment)
            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get RSLModules"
        ''' <summary>
        ''' Get RSLModules
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByRef userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtRSLModules As DataTable = New DataTable()
            dtRSLModules.TableName = "RSLModules"
            resultDataSet.Tables.Add(dtRSLModules)

            Dim userIdParam As ParameterBO = New ParameterBO("USERID", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim orderParam As ParameterBO = New ParameterBO("OrderASC", 1, DbType.Int32)
            parametersList.Add(orderParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetRSLModulesList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtRSLModules)

        End Sub
#End Region

#End Region
#Region "save SBAppointment Details"
        Sub saveSBAppointmentDetails(ByVal appointmentSummary As AppointmentSummaryBO, ByRef appointmentId As Integer, ByVal tempCount As Integer)
            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", appointmentSummary.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", appointmentSummary.OperativeId, DbType.Int32)
            inparametersList.Add(operativeIdParam)

            Dim appointmentDateParam As ParameterBO = New ParameterBO("appointmentDate", appointmentSummary.AppointmentStartDate, DbType.DateTime)
            inparametersList.Add(appointmentDateParam)

            Dim durationParam As ParameterBO = New ParameterBO("duration", appointmentSummary.Duration, DbType.Double)
            inparametersList.Add(durationParam)

            Dim faultNotesParam As ParameterBO = New ParameterBO("faultNotes", appointmentSummary.FaultNotes, DbType.String)
            inparametersList.Add(faultNotesParam)

            Dim timeParam As ParameterBO = New ParameterBO("time", appointmentSummary.Time, DbType.String)
            inparametersList.Add(timeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", appointmentSummary.EndTime, DbType.String)
            inparametersList.Add(endTimeParam)

            Dim blockidParam As ParameterBO = New ParameterBO("Blockid", appointmentSummary.Blockid, DbType.Int32)
            inparametersList.Add(blockidParam)

            Dim schemeidParam As ParameterBO = New ParameterBO("Schemeid", appointmentSummary.Schemeid, DbType.Int32)
            inparametersList.Add(schemeidParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", appointmentSummary.IsRecall, DbType.Boolean)
            inparametersList.Add(isRecallParam)

            Dim isCalendarAppointmentParam As ParameterBO = New ParameterBO("isCalendarAppointment", appointmentSummary.IsCalendarAppointment, DbType.Boolean)
            inparametersList.Add(isCalendarAppointmentParam)

            Dim appointmentNotesParam As ParameterBO = New ParameterBO("appointmentNotes", appointmentSummary.AppointmentNotes, DbType.String)
            inparametersList.Add(appointmentNotesParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", appointmentSummary.UserId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim appointmentIdParam As ParameterBO = New ParameterBO("appointmentID", Nothing, DbType.Int32)
            outParametersList.Add(appointmentIdParam)

            Dim tempCountParam As ParameterBO = New ParameterBO("tempCount", Nothing, DbType.Int32)
            outParametersList.Add(tempCountParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveSBAppointmentDetails)

            appointmentId = outParametersList.ElementAt(0).Value
            tempCount = outParametersList.ElementAt(1).Value
        End Sub
#End Region

#Region "get SchemeBloc kAddress"
        Sub getSchemeBlockAddress(ByVal resultDataSet As DataSet, ByVal schemeId As Integer, ByVal blockId As Integer)
            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim paramSchemeId As ParameterBO = New ParameterBO("schemeId", schemeId, DbType.Int32)
            inparametersList.Add(paramSchemeId)

            Dim paramBlockId As ParameterBO = New ParameterBO("blockId", blockId, DbType.Int32)
            inparametersList.Add(paramBlockId)

            MyBase.LoadDataSet(resultDataSet, inparametersList, SpNameConstants.GetSchemeBlockAddress)

        End Sub
#End Region
        
#Region "Get Job Sheet Detail"

        Public Sub getSbJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultAndAppointment As DataTable = New DataTable()
                Dim dtAsbestos As DataTable = New DataTable()
                Dim dtCustomer As DataTable = New DataTable()

                dtFaultAndAppointment.TableName = "FaultAppointment"
                dtAsbestos.TableName = "Asbestos"
                dtCustomer.TableName = "Customer"


                resultDataSet.Tables.Add(dtFaultAndAppointment)
                resultDataSet.Tables.Add(dtCustomer)


                Dim jsn As ParameterBO = New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
                parametersList.Add(jsn)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSbJobSheetDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultAndAppointment, dtCustomer)

            Catch ex As Exception

            End Try
        End Sub

#End Region
#Region "Get SB Job Sheed Summary Detail By JSN(Job Sheet Number)"

        Sub getSbJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)

            Dim inParametersList As New ParameterList

            Dim jsnparameter As ParameterBO = New ParameterBO("JSN", jsn, DbType.String)
            inParametersList.Add(jsnparameter)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.GetSbJobSheetSummaryByJSN)

            ds.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.jobSheetSummaryDetailTable, ApplicationConstants.jobSheetSummaryAsbestosTable)

        End Sub

#End Region

#Region "Save Sb Subcontractor Fault"

        Public Function saveSbSubcontractorFault(ByVal objFaultBO As FaultBO, ByVal userId As Integer, ByVal tempfaultId As Integer) As Integer

            Try

                Dim parametersList As ParameterList = New ParameterList()
                Dim outParametersList As ParameterList = New ParameterList()



                Dim faultIdParam As ParameterBO = New ParameterBO("FaultId", objFaultBO.FaultID, DbType.Int32)
                parametersList.Add(faultIdParam)

                Dim orgIdParam As ParameterBO = New ParameterBO("ORGID", objFaultBO.OrgID, DbType.Int32)
                parametersList.Add(orgIdParam)

                Dim quantityParam As ParameterBO = New ParameterBO("Quantity", objFaultBO.Quantity, DbType.Int32)
                parametersList.Add(quantityParam)

                Dim problemDaysParam As ParameterBO = New ParameterBO("ProblemDays", objFaultBO.ProblemDays, DbType.Int32)
                parametersList.Add(problemDaysParam)

                Dim recuringProblemParam As ParameterBO = New ParameterBO("RecuringProblem", objFaultBO.RecuringProblem, DbType.Boolean)
                parametersList.Add(recuringProblemParam)

                Dim communalProblemParam As ParameterBO = New ParameterBO("CommunalProblem", objFaultBO.CommunalProblem, DbType.Boolean)
                parametersList.Add(communalProblemParam)

                Dim notesParam As ParameterBO = New ParameterBO("Notes", objFaultBO.Notes, DbType.String)
                parametersList.Add(notesParam)

                Dim propertyIdParam As ParameterBO = New ParameterBO("schemeId", objFaultBO.SchemeId, DbType.Int32)
                parametersList.Add(propertyIdParam)

                Dim blockIdParam As ParameterBO = New ParameterBO("blockId", objFaultBO.BlockId, DbType.Int32)
                parametersList.Add(blockIdParam)

                Dim faultTradeIdParam As ParameterBO = New ParameterBO("FaultTradeId", objFaultBO.FaultTradeId, DbType.Int32)
                parametersList.Add(faultTradeIdParam)

                Dim originalFaultlogIdParam As ParameterBO = New ParameterBO("OriginalFaultLogId", objFaultBO.OriginalFaultLogId, DbType.Int32)
                parametersList.Add(originalFaultlogIdParam)

                Dim rechargeParam As ParameterBO = New ParameterBO("Recharge", objFaultBO.Recharge, DbType.Boolean)
                parametersList.Add(rechargeParam)

                Dim tempFaultIdParam As ParameterBO = New ParameterBO("TempFaultID", tempfaultId, DbType.Int32)
                parametersList.Add(tempFaultIdParam)

                Dim userIdParam As ParameterBO = New ParameterBO("UserId", userId, DbType.Int32)
                parametersList.Add(userIdParam)

                Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", objFaultBO.IsRecall, DbType.Boolean)
                parametersList.Add(isRecallParam)

                Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.String)
                outParametersList.Add(resultParam)

                outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SaveSbSubcontractorFault)
                Return outParametersList.Item(0).Value

            Catch ex As Exception
                Return String.Empty
            End Try

        End Function

#End Region

#Region "Save Subcontractor Fault"

        Public Function saveSubcontractorFault(ByVal contractorId As Integer, ByVal userId As Integer, ByVal tempfaultId As Integer, ByRef jsn As String) As Integer
            Try
                Dim parametersList As ParameterList = New ParameterList()
                Dim outParametersList As ParameterList = New ParameterList()

                Dim orgIdParam As ParameterBO = New ParameterBO("ORGID", contractorId, DbType.Int32)
                parametersList.Add(orgIdParam)

                Dim tempFaultIdParam As ParameterBO = New ParameterBO("TempFaultID", tempfaultId, DbType.Int32)
                parametersList.Add(tempFaultIdParam)

                Dim userIdParam As ParameterBO = New ParameterBO("UserId", userId, DbType.Int32)
                parametersList.Add(userIdParam)

                Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.String)
                outParametersList.Add(resultParam)

                Dim jsnParam As ParameterBO = New ParameterBO("jsn", Nothing, DbType.String)
                outParametersList.Add(jsnParam)

                outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.SaveContractorAppointmentDetails)
                jsn = outParametersList.Item(1).Value
                Return outParametersList.Item(0).Value

            Catch ex As Exception
                Return String.Empty
            End Try

        End Function

#End Region

#Region "Get Subcontractor Job Sheet Detail"

        Public Sub getSubcontractorJobSheetDetails(ByRef resultDataSet As DataSet, ByVal objTempFaultBO As TempFaultBO)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultDetail As DataTable = New DataTable()
                Dim dtAsbestos As DataTable = New DataTable()

                dtFaultDetail.TableName = "FaultDetail"
                dtAsbestos.TableName = "Asbestos"

                resultDataSet.Tables.Add(dtFaultDetail)
                resultDataSet.Tables.Add(dtAsbestos)

                Dim tempFaultIdParam As ParameterBO = New ParameterBO("tempfaultId", objTempFaultBO.TempFaultId, DbType.Int32)
                parametersList.Add(tempFaultIdParam)
                Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objTempFaultBO.Trade, DbType.Int32)
                parametersList.Add(tradeIdParam)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSbSubcontractorJobSheetDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultDetail, dtAsbestos)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get Operative Info"
        ''' <summary>
        ''' Get Operative Info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="operativeId"></param>
        ''' <remarks></remarks>
        Sub getOperativeInformation(ByRef resultDataSet As DataSet, ByVal operativeId As String)

            Dim parameterList As ParameterList = New ParameterList()

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", operativeId, DbType.String)
            parameterList.Add(operativeIdParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetOperativeInformation)

        End Sub
#End Region

#Region "Get Fault Assign To Contractor"

        Public Sub getFaultAssignToContractor(ByRef resultDataSet As DataSet, ByVal templFaultId As Int32)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultInfo As DataTable = New DataTable()
                dtFaultInfo.TableName = ApplicationConstants.FaultInfoDt
                resultDataSet.Tables.Add(dtFaultInfo)

                Dim faultLogIdParam As ParameterBO = New ParameterBO("tempFaultId", templFaultId, DbType.Int32)
                parametersList.Add(faultLogIdParam)

                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFaultInfo)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultInfo)

            Catch ex As Exception

            End Try
        End Sub

#End Region

    End Class
End Namespace

