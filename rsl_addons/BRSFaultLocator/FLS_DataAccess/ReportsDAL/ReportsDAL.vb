﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports FLS_BusinessObject
Imports FLS_Utilities

Namespace FLS_DataAccess

    Public Class ReportsDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Follow On List"

        Public Function getFollowOnList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer, ByVal followOnStatusId As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim followOnStatusIdParam As New ParameterBO("followOnStatusId", followOnStatusId, DbType.Int32)
            parametersList.Add(followOnStatusIdParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetFollowOnList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region



#Region "Delete FollowOn Work"
        Public Sub DeleteFollowOnWork(ByVal followOnId As Int32, ByVal faultLogId As Int32)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()
            Dim followOnIdParam As New ParameterBO("followOnWorkId", followOnId, DbType.Int32)
            parametersList.Add(followOnIdParam)
            Dim faultLogIdParam As New ParameterBO("faultLogId", faultLogId, DbType.Int32)
            parametersList.Add(faultLogIdParam)
            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeleteFollowOnWork)

        End Sub

#End Region



#Region "Get Subcontractor List"

        Public Function getSubcontractorList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal statusId As Integer, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.Int32)
            parametersList.Add(blockIdParam)

            Dim statusIdParam As New ParameterBO("statusId", statusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetSubcontractorList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get FollowOn Work Detail"

        Public Sub getFollowOnWorkDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As New ParameterList()

            Dim dtJobSheetDetail As DataTable = New DataTable()
            Dim dtCustomerDetail As DataTable = New DataTable()
            Dim dtRepairDetail As DataTable = New DataTable()
            Dim dtFollowOnWorkDetail As DataTable = New DataTable()

            dtJobSheetDetail.TableName = "JobSheetDetail"
            dtCustomerDetail.TableName = "CustomerDetail"
            dtRepairDetail.TableName = "RepairDetail"
            dtFollowOnWorkDetail.TableName = "FollowOnWorkDetail"

            resultDataSet.Tables.Add(dtJobSheetDetail)
            resultDataSet.Tables.Add(dtCustomerDetail)
            resultDataSet.Tables.Add(dtRepairDetail)
            resultDataSet.Tables.Add(dtFollowOnWorkDetail)

            Dim followOnParam As New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(followOnParam)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFollowOnWorksDetails)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtJobSheetDetail, dtCustomerDetail, dtRepairDetail, dtFollowOnWorkDetail)


        End Sub

#End Region

#Region "Get No Entry List"

        Public Function getNoEntryList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetNoEntryList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get NoEntry Detail"

        Public Sub getNoEntryDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As New ParameterList()

            Dim dtJobSheetDetail As DataTable = New DataTable()
            Dim dtCustomerDetail As DataTable = New DataTable()
            Dim dtNoEntryDetail As DataTable = New DataTable()
            Dim dtPropertyDetail As DataTable = New DataTable()

            dtJobSheetDetail.TableName = "JobSheetDetail"
            dtCustomerDetail.TableName = "CustomerDetail"
            dtNoEntryDetail.TableName = "NoEntryDetail"
            dtPropertyDetail.TableName = "PropertyDetail"

            resultDataSet.Tables.Add(dtJobSheetDetail)
            resultDataSet.Tables.Add(dtCustomerDetail)
            resultDataSet.Tables.Add(dtNoEntryDetail)
            resultDataSet.Tables.Add(dtPropertyDetail)

            Dim noEntryParam As New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(noEntryParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetNoEntryDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtJobSheetDetail, dtCustomerDetail, dtNoEntryDetail, dtPropertyDetail)

        End Sub

#End Region

#Region "Get Operative List"

        Public Sub getOperativeList(ByRef resultDataSet As DataSet)

            Dim parametersList As New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetOperativeList)

        End Sub

#End Region

#Region "Get Paused fault Operative List"

        Public Sub getPausedOperativeList(ByRef resultDataSet As DataSet)

            Dim parametersList As New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetPausedFaultOperativeList)

        End Sub

#End Region

#Region "Get Recall List"

        Public Function getRecallList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal operativeId As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim operativeIdParam As New ParameterBO("operativeId", operativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetRecallList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Recall Detail By Original FaultLogID"

        Sub getRecallDetailByFaultLogID(ByVal resultDataset As DataSet, ByVal faultLogID As Integer)

            Dim inParametersList As New ParameterList()
            'Tables Added in Code Behind file and commmented out here.
            'Dim propertyDT As New DataTable(ApplicationConstants.PropertyDetailsDT)
            'Dim OriginalFaultDetailsDT As New DataTable(ApplicationConstants.OriginalFaultDetailsDT)
            'Dim RecallFaultsDetailsDT As New DataTable(ApplicationConstants.RecallFaultsDetailsDT)


            Dim faultLogIDParam As New ParameterBO("faultLogID", faultLogID, DbType.String)
            inParametersList.Add(faultLogIDParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(inParametersList, SpNameConstants.GetRecallDetailByFaultLogID)

            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, ApplicationConstants.PropertyDetailsDT, ApplicationConstants.OriginalFaultDetailsDT, ApplicationConstants.RecallFaultsDetailsDT)

        End Sub

#End Region

#Region "Get Paused Fault List"

        Public Function getPausedFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal operativeId As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim operativeIdParam As New ParameterBO("operativeId", operativeId, DbType.Int32)
            parametersList.Add(operativeIdParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim schemeIdParam As New ParameterBO("schemeId", schemeId, DbType.String)
            parametersList.Add(schemeIdParam)

            Dim blockIdParam As New ParameterBO("blockId", blockId, DbType.String)
            parametersList.Add(blockIdParam)

            Dim financialYearParam As New ParameterBO("financialYear", financialYear, DbType.Int32)
            parametersList.Add(financialYearParam)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetPausedFaultList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Get Property Search Result"
        ''' <summary>
        ''' Get Property Search Result
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="searchText"></param>
        ''' <remarks></remarks>
        Public Sub getRepairSearchResult(ByRef resultDataSet As DataSet, ByRef searchText As String)

            Dim parametersList As New ParameterList()

            Dim dtPropertyResult As DataTable = New DataTable()

            dtPropertyResult.TableName = "RepairSearchResult"

            resultDataSet.Tables.Add(dtPropertyResult)

            Dim searchTextParam As New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFaultRepairList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult)

        End Sub

#End Region

#Region "Insert Repair List Data"
        ''' <summary>
        ''' Insert Repair List Data
        ''' </summary>
        ''' <param name="objRepairListBo">RepairListBO as object</param>
        ''' <remarks></remarks>

        Public Function InsertRepairListData(ByRef objRepairListBo As RepairListBO) As Integer

            Dim outParametersList As New ParameterList()
            Dim parameterList As New ParameterList()

            Dim faultLogId As New ParameterBO("FaultLogID", objRepairListBo.FaultLogId, DbType.Int32)
            parameterList.Add(faultLogId)

            Dim faultRepairIdList As New ParameterBO("FaultRepairIDList", objRepairListBo.FaultRepairIdList, DbType.String)
            parameterList.Add(faultRepairIdList)

            Dim time As New ParameterBO("Time", objRepairListBo.Time, DbType.String)
            parameterList.Add(time)

            Dim inspectionDate As New ParameterBO("Date", objRepairListBo.InspectionDate, DbType.DateTime)
            parameterList.Add(inspectionDate)

            Dim userId As New ParameterBO("UserId", objRepairListBo.UserId, DbType.Int32)
            parameterList.Add(userId)

            Dim totalCountParam As New ParameterBO("Result", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.SaveRecord(parameterList, outParametersList, SpNameConstants.AddFaultRepairList)
            Return outParametersList.Item(0).Value.ToString()
            'MyBase.LoadDataSet(resultDataSet, parameterList, outParametersList, SpNameConstants.GetComponentsForLifeCycle)
            'Return outParametersList.Item(0).Value.ToString()

        End Function
#End Region

#Region "Load Scheme DropDown"
        ''' <summary>
        ''' Load Scheme dropdown for all reports
        ''' </summary>
        ''' <param name="schemeDataSet"></param>
        ''' <remarks></remarks>
        Sub loadSchemeDropDown(ByRef schemeDataSet As DataSet)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim developmentId As New ParameterBO("developmentId", -1, DbType.Int32)
            parametersList.Add(developmentId)

            MyBase.LoadDataSet(schemeDataSet, parametersList, outParametersList, SpNameConstants.GetSchemeDropDown)

        End Sub
#End Region

#Region "Load Block Dropdown by SchemeId"
        ''' <summary>
        ''' Load Block dropdown by scheme id
        ''' </summary>
        ''' <param name="blockDataSet"></param>
        ''' <param name="schemeId"></param>
        ''' <remarks></remarks>
        Sub loadBlockDropdownbySchemeId(ByRef blockDataSet As DataSet, ByVal schemeId As Integer)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim ParamschemeId As New ParameterBO("schemeId", schemeId, DbType.Int32)
            parametersList.Add(ParamschemeId)

            MyBase.LoadDataSet(blockDataSet, parametersList, outParametersList, SpNameConstants.GetBlockDropDownBySchemeId)
        End Sub

#End Region

#Region "get Reactive Repair List"

        Public Function getReactiveRepairList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal isFullList As Boolean)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim isFullListParam As New ParameterBO("isFullList", isFullList, DbType.Boolean)
            parametersList.Add(isFullListParam)


            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetRepairList)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#Region "Amend a repair"
        Public Sub AmendRepair(ByRef objRepaisBo As RepairsBO)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim faultRepairListId As New ParameterBO("FAULTREPAIRLISTID", objRepaisBo.FaultRepairListId, DbType.Int32)
            parametersList.Add(faultRepairListId)
            Dim repairDesc As New ParameterBO("DESCRIPTION", objRepaisBo.RepairDescription, DbType.String)
            parametersList.Add(repairDesc)
            Dim repairNetCost As New ParameterBO("NETCOST", objRepaisBo.NetCost, DbType.Double)
            parametersList.Add(repairNetCost)
            Dim repairVat As New ParameterBO("VAT", objRepaisBo.Vat, DbType.Double)
            parametersList.Add(repairVat)
            Dim repairVatRateId As New ParameterBO("VATRATEID", objRepaisBo.VatRateId, DbType.Int32)
            parametersList.Add(repairVatRateId)
            Dim repairGross As New ParameterBO("GROSS", objRepaisBo.GrossCost, DbType.Double)
            parametersList.Add(repairGross)
            Dim repairPostInspection As New ParameterBO("POSTINSPECTION", objRepaisBo.PostInspection, DbType.Boolean)
            parametersList.Add(repairPostInspection)
            Dim repairStockConditionItem As New ParameterBO("STOCKCONDITIONITEM", objRepaisBo.StockConditionItem, DbType.Boolean)
            parametersList.Add(repairStockConditionItem)
            Dim repairActive As New ParameterBO("REPAIRACTIVE ", objRepaisBo.IsActive, DbType.Boolean)
            parametersList.Add(repairActive)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.AmendRepair)



        End Sub
#End Region



#Region "Add a repair"
        Public Sub AddNewRepair(ByRef objRepaisBo As RepairsBO)
            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim repairDesc As New ParameterBO("DESCRIPTION", objRepaisBo.RepairDescription, DbType.String)
            parametersList.Add(repairDesc)
            Dim repairNetCost As New ParameterBO("NETCOST", objRepaisBo.NetCost, DbType.Double)
            parametersList.Add(repairNetCost)
            Dim repairVat As New ParameterBO("VAT", objRepaisBo.Vat, DbType.Double)
            parametersList.Add(repairVat)
            Dim repairVatRateId As New ParameterBO("VATRATEID", objRepaisBo.VatRateId, DbType.Int32)
            parametersList.Add(repairVatRateId)
            Dim repairGross As New ParameterBO("GROSS", objRepaisBo.GrossCost, DbType.Double)
            parametersList.Add(repairGross)
            Dim repairPostInspection As New ParameterBO("POSTINSPECTION", objRepaisBo.PostInspection, DbType.Boolean)
            parametersList.Add(repairPostInspection)
            Dim repairStockConditionItem As New ParameterBO("STOCKCONDITIONITEM", objRepaisBo.StockConditionItem, DbType.Boolean)
            parametersList.Add(repairStockConditionItem)
            Dim repairActive As New ParameterBO("REPAIRACTIVE ", objRepaisBo.IsActive, DbType.Boolean)
            parametersList.Add(repairActive)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.AddNewRepair)



        End Sub
#End Region


#Region "Associated faults list"

        Public Function getAssociatedFaultList(ByRef resultDataSet As DataSet, ByVal repairId As Integer, ByRef objPageSortBo As PageSortBO)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim faultRepairID As New ParameterBO("repairId", repairId, DbType.Int32)
            parametersList.Add(faultRepairID)

            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.AssociatedFaults)
            Return 0

        End Function

#End Region

#Region "get Reactive Repair List for Export to excel"

        Public Sub getReactiveRepairListExportToExcel(ByRef resultDataSet As DataSet, ByVal search As String)

            Dim parametersList As New ParameterList()
            Dim outParametersList As New ParameterList()
            Dim searchedValueParam As New ParameterBO("searchedText", search, DbType.String)
            parametersList.Add(searchedValueParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, outParametersList, SpNameConstants.GetRepairListExportToExcel)

        End Sub

#End Region

#End Region

    End Class

End Namespace
