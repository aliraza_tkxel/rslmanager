﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports FLS_Utilities
Imports FLS_BusinessObject
Namespace FLS_DataAccess
    Public Class CalendarDAL : Inherits BaseDAL

#Region "Functions"

#Region "get Fault Engineers Info for Appointment"
        Sub getFaultEngineersForAppointment(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", objCalendarAppointmentsBO.PatchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objCalendarAppointmentsBO.TradeId, DbType.Int32)
            parametersList.Add(tradeIdParam)


            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetFaultAppointmentEngineerInfo)
        End Sub
#End Region

#Region "get Searched Fault Engineers Info for Appointment"
        Sub getSearchedFaultEngineersForAppointment(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim patchIdParam As ParameterBO = New ParameterBO("patchId", objCalendarAppointmentsBO.PatchId, DbType.Int32)
            parametersList.Add(patchIdParam)

            Dim schemeIdParam As ParameterBO = New ParameterBO("SchemeId", objCalendarAppointmentsBO.SchemeId, DbType.Int32)
            parametersList.Add(schemeIdParam)

            Dim tradeIdParam As ParameterBO = New ParameterBO("tradeId", objCalendarAppointmentsBO.TradeId, DbType.Int32)
            parametersList.Add(tradeIdParam)


            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.SearchedFaultAppointmentEngineerInfo)
        End Sub
#End Region

#Region "get Engineer Leaves Details"
        Sub getEngineerLeavesDetail(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objCalendarAppointmentsBO.StartDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim returnDateParam As ParameterBO = New ParameterBO("returnDate", objCalendarAppointmentsBO.ReturnDate, DbType.Date)
            parametersList.Add(returnDateParam)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeIds", objCalendarAppointmentsBO.EmpolyeeIds, DbType.String)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetEngineersLeaveInformation)
        End Sub
#End Region

#Region "get Employee Core Working Hours"
        Sub getEmployeeCoreWorkingHours(ByRef resultDataSet As DataSet, ByVal employeeIds As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeIds", employeeIds, DbType.String)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetEmployeeCoreWorkingHours)
        End Sub
#End Region

#Region "get Fault Appointments Details To Display On Calendar"
        Sub getFaultAppointmentsCalendarDetail(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            Dim calendarAppointmentDt As DataTable = New DataTable(ApplicationConstants.calendarAppointment)
            Dim faultJSN As DataTable = New DataTable(ApplicationConstants.faultJSN)

            Dim startDateParam As ParameterBO = New ParameterBO("startDate", objCalendarAppointmentsBO.StartDate, DbType.Date)
            parametersList.Add(startDateParam)

            Dim endDateParam As ParameterBO = New ParameterBO("endDate", objCalendarAppointmentsBO.EndDate, DbType.Date)
            parametersList.Add(endDateParam)

            Dim scheme As ParameterBO = New ParameterBO("scheme", objCalendarAppointmentsBO.SchemeId, DbType.Int32)
            parametersList.Add(scheme)

            resultDataSet.Tables.Add(calendarAppointmentDt)
            resultDataSet.Tables.Add(faultJSN)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAppointmentsCalendarInfo)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, calendarAppointmentDt, faultJSN)

        End Sub
#End Region

#Region "get Newly Created Appointment Details"
        Sub getNewlyCreatedAppointmentDetail(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim faultIdParam As ParameterBO = New ParameterBO("appointmentId", objCalendarAppointmentsBO.AppointmentId, DbType.Int32)
            parametersList.Add(faultIdParam)

            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetNewlyCreatedAppointmentInfo)
        End Sub
#End Region

#Region "get Property Patches"
        Public Sub getPatchLocations(ByVal index As Int32, ByVal userTypeList As List(Of CalendarAppointmentsBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetPropertyPatches)
            Dim id As Integer = 0
            Dim name As String = String.Empty
            ' id = 0
            ' name = "Please Select Patch"
            'Dim objDefault As New CalendarAppointmentsBO(id, name)
            'userTypeList.Add(objDefault)
            While (myDataReader.Read)
                id = 0
                name = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("PATCHID")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("PATCHID"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Location")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("Location"))
                End If
                Dim objList As New CalendarAppointmentsBO(id, name)
                userTypeList.Add(objList)
            End While
        End Sub
#End Region

#Region "get Trades Information"
        Public Sub getTrades(ByVal index As Int32, ByVal userTypeList As List(Of CalendarAppointmentsBO))
            Dim myDataReader As IDataReader = MyBase.SelectRecord(Nothing, SpNameConstants.GetFaultTrades)
            Dim id As Integer = 0
            Dim name As String = String.Empty
            'id = 0
            ' name = "Please Select Patch"
            'Dim objDefault As New CalendarAppointmentsBO(id, name)
            'userTypeList.Add(objDefault)
            While (myDataReader.Read)
                id = 0
                name = String.Empty

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("TradeId")) Then
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("TradeId"))
                End If

                If Not myDataReader.IsDBNull(myDataReader.GetOrdinal("Description")) Then
                    name = myDataReader.GetString(myDataReader.GetOrdinal("Description"))
                End If
                Dim objList As New CalendarAppointmentsBO(id, name)
                userTypeList.Add(objList)
            End While
        End Sub
#End Region

#Region "Save New Appointment Information"
        Public Function saveAppointmentInformation(ByVal appointmentSummary As AppointmentSummaryBO) As Integer

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", appointmentSummary.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", appointmentSummary.OperativeId, DbType.Int32)
            inparametersList.Add(operativeIdParam)

            Dim appointmentDateParam As ParameterBO = New ParameterBO("appointmentDate", appointmentSummary.AppointmentStartDate, DbType.DateTime)
            inparametersList.Add(appointmentDateParam)

            Dim faultNotesParam As ParameterBO = New ParameterBO("appointmentNotes", appointmentSummary.FaultNotes, DbType.String)
            inparametersList.Add(faultNotesParam)

            Dim timeParam As ParameterBO = New ParameterBO("time", appointmentSummary.Time, DbType.String)
            inparametersList.Add(timeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", appointmentSummary.EndTime, DbType.String)
            inparametersList.Add(endTimeParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", appointmentSummary.PropertyId, DbType.String)
            inparametersList.Add(propertyIdParam)

            Dim customerIdParam As ParameterBO = New ParameterBO("customerId", appointmentSummary.CustomerId, DbType.Int32)
            inparametersList.Add(customerIdParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", appointmentSummary.IsRecall, DbType.Boolean)
            inparametersList.Add(isRecallParam)

            Dim isCalendarAppointmentParam As ParameterBO = New ParameterBO("isCalendarAppointment", appointmentSummary.IsCalendarAppointment, DbType.Boolean)
            inparametersList.Add(isCalendarAppointmentParam)

            Dim isNoEntryParam As ParameterBO = New ParameterBO("isNoEntry", appointmentSummary.IsNoEntry, DbType.Boolean)
            inparametersList.Add(isNoEntryParam)

            Dim noEntryFaultLogIdparam As ParameterBO = New ParameterBO("NoEntryFaultLogId", appointmentSummary.NoEntryFaultLogId, DbType.Int32)
            inparametersList.Add(noEntryFaultLogIdparam)

            Dim userIdParam As ParameterBO = New ParameterBO("UserId", appointmentSummary.UserId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim appointmentEndDateParam As ParameterBO = New ParameterBO("appointmentEndDate", appointmentSummary.AppointmentEndDate, DbType.DateTime)
            inparametersList.Add(appointmentEndDateParam)

            Dim areaNameParam As ParameterBO = New ParameterBO("areaName", appointmentSummary.AreaName, DbType.String)
            inparametersList.Add(areaNameParam)

            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveAppointments)

            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Get Tenant(s) Info by TenancyId - Joint Tenancy"

        Sub GetJointTenantsInfoByTenancyID(ByRef dstenantsInfo As DataSet, ByRef tenancyID As Integer)

            Dim parametersList As New ParameterList()

            Dim tenancyIDParam As New ParameterBO("tenancyID", tenancyID, DbType.Int32)
            parametersList.Add(tenancyIDParam)

            MyBase.LoadDataSet(dstenantsInfo, parametersList, SpNameConstants.GetJointTenantsInfoByTenancyID)

        End Sub

#End Region

#Region "Get PropertyID and TradeID of an Appointmet by giving AppointmetID"

        Sub getPidTidbyAppointmentID(ByVal currentAppointmentID As Integer, ByRef calendarAppointmentsBO As CalendarAppointmentsBO)

            Dim parametersList As New ParameterList()

            Dim appointmentIDParam As New ParameterBO("AppointmentID", currentAppointmentID, DbType.Int32)
            parametersList.Add(appointmentIDParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPidTidbyAppointmentID)

            If lResultDataReader.Read Then
                calendarAppointmentsBO.PatchId = lResultDataReader.GetInt32(lResultDataReader.GetOrdinal("PatchID"))
                calendarAppointmentsBO.TradeId = lResultDataReader.GetInt32(lResultDataReader.GetOrdinal("TradeID"))
            End If

        End Sub

#End Region

#Region "Save New Appointment Information"
        Public Function saveSbAppointmentInformation(ByVal appointmentSummary As AppointmentSummaryBO)

            Dim inparametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim faultsListParam As ParameterBO = New ParameterBO("faultsList", appointmentSummary.FaultsList, DbType.String)
            inparametersList.Add(faultsListParam)

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", appointmentSummary.OperativeId, DbType.Int32)
            inparametersList.Add(operativeIdParam)

            Dim appointmentDateParam As ParameterBO = New ParameterBO("appointmentDate", appointmentSummary.AppointmentStartDate, DbType.DateTime)
            inparametersList.Add(appointmentDateParam)

            'Dim durationParam As ParameterBO = New ParameterBO("duration", appointmentSummary.Duration, DbType.Double)
            'inparametersList.Add(durationParam)

            Dim faultNotesParam As ParameterBO = New ParameterBO("appointmentNotes", appointmentSummary.FaultNotes, DbType.String)
            inparametersList.Add(faultNotesParam)

            Dim timeParam As ParameterBO = New ParameterBO("time", appointmentSummary.Time, DbType.String)
            inparametersList.Add(timeParam)

            Dim endTimeParam As ParameterBO = New ParameterBO("endTime", appointmentSummary.EndTime, DbType.String)
            inparametersList.Add(endTimeParam)

            Dim propertyIdParam As ParameterBO = New ParameterBO("schemeId", appointmentSummary.Schemeid, DbType.Int32)
            inparametersList.Add(propertyIdParam)

            Dim customerIdParam As ParameterBO = New ParameterBO("blockId", appointmentSummary.Blockid, DbType.Int32)
            inparametersList.Add(customerIdParam)

            Dim isRecallParam As ParameterBO = New ParameterBO("isRecall", appointmentSummary.IsRecall, DbType.Boolean)
            inparametersList.Add(isRecallParam)

            Dim isCalendarAppointmentParam As ParameterBO = New ParameterBO("isCalendarAppointment", appointmentSummary.IsCalendarAppointment, DbType.Boolean)
            inparametersList.Add(isCalendarAppointmentParam)

            Dim isNoEntryParam As ParameterBO = New ParameterBO("isNoEntry", appointmentSummary.IsNoEntry, DbType.Boolean)
            inparametersList.Add(isNoEntryParam)

            Dim noEntryFaultLogIdparam As ParameterBO = New ParameterBO("NoEntryFaultLogId", appointmentSummary.NoEntryFaultLogId, DbType.Int32)
            inparametersList.Add(noEntryFaultLogIdparam)

            Dim userIdParam As ParameterBO = New ParameterBO("UserId", appointmentSummary.UserId, DbType.Int32)
            inparametersList.Add(userIdParam)

            Dim appointmentEndDateParam As ParameterBO = New ParameterBO("appointmentEndDate", appointmentSummary.AppointmentEndDate, DbType.DateTime)
            inparametersList.Add(appointmentEndDateParam)
            Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
            outParametersList.Add(resultParam)

            MyBase.SaveRecord(inparametersList, outParametersList, SpNameConstants.SaveSbAppointments)

            Return outParametersList.ElementAt(0).Value

        End Function
#End Region

#Region "Get Operatives filtered by trade Id"

        Public Sub getOperativeFilteredByTradeId(ByRef operatives As List(Of DropDownBO), Optional ByVal tradeId As Integer = -1)
            Dim inparametersList As New ParameterList()

            inparametersList.Add(New ParameterBO("tradeId", tradeId, DbType.Int32))

            Dim lDataReader = SelectRecord(inparametersList, SpNameConstants.GetOperativesFilterByTrandeId)

            Dim id As Integer
            Dim val As String

            While (lDataReader.Read)
                id = 0
                val = String.Empty

                id = lDataReader.GetInt32(lDataReader.GetOrdinal("operativeId"))
                val = lDataReader.GetString(lDataReader.GetOrdinal("operativeName"))

                operatives.Add(New DropDownBO(id, val))

            End While
            lDataReader.Close()
        End Sub

#End Region


#Region "Get Appointment type"

        Public Sub GetAppointmentType(ByRef appointmentType As List(Of DropDownBO))
            Dim inparametersList As New ParameterList()

            Dim lDataReader = SelectRecord(inparametersList, SpNameConstants.GetAppointmentTypes)

            Dim id As Integer
            Dim val As String

            While (lDataReader.Read)
                id = 0
                val = String.Empty

                id = lDataReader.GetInt32(lDataReader.GetOrdinal("AppointmentTypeId"))
                val = lDataReader.GetString(lDataReader.GetOrdinal("Description"))

                appointmentType.Add(New DropDownBO(id, val))

            End While
            lDataReader.Close()
        End Sub

#End Region




#Region "Get Property Calendar Details"

        Public Sub getPropertyCalendarDetails(ByRef objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef calendarDetailsDataSet As DataSet)

            Dim inParameterList As New ParameterList

            '@schemeId INT = -1            
            inParameterList.Add(New ParameterBO("schemeId", objCalendarAppointmentsBO.SchemeId, DbType.Int32))

            '@appointmentType NVARCHAR(50) = ''
            inParameterList.Add(New ParameterBO("appointmentType", objCalendarAppointmentsBO.AppointmentType, DbType.String))

            ',@patchId INT = -1            
            inParameterList.Add(New ParameterBO("patchId", objCalendarAppointmentsBO.PatchId, DbType.Int32))

            ',@tradeId INT = -1
            inParameterList.Add(New ParameterBO("tradeId", objCalendarAppointmentsBO.TradeId, DbType.Int32))

            ',@operativeId INT = -1
            inParameterList.Add(New ParameterBO("operativeId", objCalendarAppointmentsBO.OperativeId, DbType.Int32))

            ',@jsn NVARCHAR(50) = ''
            inParameterList.Add(New ParameterBO("jsn", objCalendarAppointmentsBO.JSN, DbType.String))

            ',@tenant NVARCHAR(200) = ''
            inParameterList.Add(New ParameterBO("tenant", objCalendarAppointmentsBO.Tenant, DbType.String))

            ',@address NVARCHAR(200) = ''            
            inParameterList.Add(New ParameterBO("address", objCalendarAppointmentsBO.Addresss, DbType.String))

            ',@startDate DATETIME
            inParameterList.Add(New ParameterBO("startDate", objCalendarAppointmentsBO.StartDate, DbType.DateTime))

            ',@endDate DATETIME
            inParameterList.Add(New ParameterBO("endDate", objCalendarAppointmentsBO.EndDate, DbType.DateTime))

            Dim lDataReader = SelectRecord(inParameterList, SpNameConstants.GetPropertyCalendarDetails)



            calendarDetailsDataSet.Load(lDataReader, LoadOption.OverwriteChanges, ApplicationConstants.PropertyCalendarOperativesDt _
                                        , ApplicationConstants.PropertyCalendarLeavesDt _
                                        , ApplicationConstants.PropertyCalendarAppointmentsDt _
                                        , ApplicationConstants.PropertyCalendarCoreWorkingHoursDt)
        End Sub

#End Region

#Region "Get Appointment Notes"

        Public Sub getAppointmentNotes(ByRef dsNotes As DataSet, ByVal appointmentId As Int32, ByVal aptType As String)
            Dim parametersList As New ParameterList()

            Dim appointmentIdParam As New ParameterBO("appointmentId", appointmentId, DbType.Int32)
            parametersList.Add(appointmentIdParam)

            Dim appointmentTypeParam As New ParameterBO("appointmentType", aptType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            MyBase.LoadDataSet(dsNotes, parametersList, SpNameConstants.GetAppointmentNotes)
        End Sub

#End Region

#End Region



    End Class
End Namespace

