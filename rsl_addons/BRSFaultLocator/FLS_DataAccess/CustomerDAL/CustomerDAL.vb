﻿Imports FLS_DataAccess
Imports FLS_BusinessObject
Imports FLS_Utilities

Public Class CustomerDAL : Inherits BaseDAL
#Region "Functions"
#Region "Update customer address"
    Public Function updateAddress(ByVal objCustomerBO As CustomerBO)

        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()

        Dim customerIdParam As ParameterBO = New ParameterBO("customerId", objCustomerBO.CustomerId, DbType.Int32)
        parametersList.Add(customerIdParam)

        Dim telephoneParam As ParameterBO = New ParameterBO("telephone", objCustomerBO.Telephone, DbType.String)
        parametersList.Add(telephoneParam)

        Dim mobileParam As ParameterBO = New ParameterBO("mobile", objCustomerBO.Mobile, DbType.String)
        parametersList.Add(mobileParam)

        Dim emailParam As ParameterBO = New ParameterBO("email", objCustomerBO.Email, DbType.String)
        parametersList.Add(emailParam)

        Dim resultParam As ParameterBO = New ParameterBO("result", Nothing, DbType.Int32)
        outParametersList.Add(resultParam)

        MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.AmendCustomerAddress)

        Return outParametersList.ElementAt(0).Value

    End Function

#End Region
#End Region

End Class
