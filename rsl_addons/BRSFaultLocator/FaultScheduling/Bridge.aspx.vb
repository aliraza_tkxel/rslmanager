﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports FLS_Utilities


Public Class Bridge
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    Dim customerId As Integer = 0
    Dim propertyId As String = String.Empty
    Dim schemeId As Integer = 0
    Dim blockId As Integer = 0
    Dim classicUserId As Integer = 0
    Dim isCalendarView As Boolean = False
    Dim isReportView As Boolean = False
    Dim isRejectedByContractor As Boolean = False
    Dim isAccessDenied As Boolean = False
    Dim isRepairsDashboard As Boolean = False
    Dim isSchemeBlockArea As Boolean = False
    Dim isPropertyArea As Boolean = False
    Dim reportPrifix As String = String.Empty
    Dim isSchedule As Boolean = False
    Dim isPropertyCalendar As Boolean = False
    Dim isFaultBasket As Boolean = False
    Dim isSbFaultBasket As Boolean = False
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "load User"
    Private Sub loadUser()

        Dim objUserBo As UserBO = New UserBO()
        Dim objUserBl As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()

        Me.getQueryStringValues()
        Me.checkClassicAspSession()
        'classicUserId = 943
        resultDataSet = objUserBl.getEmployeeById(classicUserId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
        Else
            Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

            If isActive = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
            Else
                setUserSession(resultDataSet)
                If isPropertyCalendar Then
                    Dim menu As String = Request.QueryString(PathConstants.Menu)
                    Dim selectedMenu As String = String.Empty
                    If String.IsNullOrEmpty(menu) Then
                        selectedMenu = ApplicationConstants.RepairsMenu
                    Else
                        selectedMenu = menu
                    End If

                    Response.Redirect(PathConstants.PropertyCalendarPath + "?mn=" + selectedMenu, True)
                ElseIf isCalendarView = True Then
                    Response.Redirect(PathConstants.AccessDeniedPath, True)

                ElseIf isReportView = True And isRepairsDashboard = True And isRejectedByContractor = True Then
                    Response.Redirect(PathConstants.ReportAreaPath + "?" + PathConstants.RepairsDashboard + "=" + Me.reportPrifix + "&" + PathConstants.RejectedByContractor + "=true", True)

                ElseIf isReportView = True And isRepairsDashboard = True Then

                    Dim menu As String = Request.QueryString(PathConstants.Menu)
                    If String.IsNullOrEmpty(menu) Then
                        Response.Redirect(PathConstants.ReportAreaPath + "?" + PathConstants.RepairsDashboard + "=" + Me.reportPrifix, True)
                    Else
                        Response.Redirect(PathConstants.ReportAreaPath + "?" + PathConstants.RepairsDashboard + "=" + Me.reportPrifix + "&mn=" + menu, True)
                    End If

                ElseIf isReportView = True Then
                    Response.Redirect(PathConstants.AccessDeniedPath, True)
                ElseIf isSchedule = True Then
                    Response.Redirect(PathConstants.RearrangCurrentFault + "?" + PathConstants.Src + "=" + PathConstants.ReportAreaSrcVal + "&" + PathConstants.Create + "=yes&" + PathConstants.Jsn + "=" + Request.QueryString(PathConstants.Jsn))
                ElseIf isAccessDenied = True Then
                    Response.Redirect(PathConstants.AccessDeniedPath, True)
                ElseIf isSchemeBlockArea = True Then
                    Response.Redirect(PathConstants.SearchSbFault + "?" + PathConstants.Bid + "=" + Me.blockId.ToString() + "&" + PathConstants.Sid + "=" + Me.schemeId.ToString())
                ElseIf isFaultBasket = True Then
                    Response.Redirect(PathConstants.FaultBasketPath)
                ElseIf isSbFaultBasket = True Then
                    Response.Redirect(PathConstants.SbFaultBasketPath)
                ElseIf isPropertyArea = True Then
                    Response.Redirect(PathConstants.SearchFault + "?" + PathConstants.Cid + "=" + Me.customerId.ToString() + "&" + PathConstants.Pid + "=" + Me.propertyId)
                Else
                    Response.Redirect(PathConstants.AccessDeniedPath, True)
                End If

            End If

        End If
    End Sub
#End Region

#Region "set User Session"
    Private Sub setUserSession(ByRef resultDataSet)

        SessionManager.setFaultSchedulingUserId(classicUserId)
        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())
        SessionManager.setLoggedInUserType(resultDataSet.Tables(0).Rows(0).Item("UserType").ToString())

    End Sub
#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        Dim pg As String = Request.QueryString(PathConstants.Page)
        If Request.QueryString(PathConstants.Page) IsNot Nothing AndAlso Request.QueryString(PathConstants.Page) = PathConstants.PropertyCalendar Then
            isPropertyCalendar = True
        ElseIf Request.QueryString(PathConstants.Page) IsNot Nothing Then

            isFaultBasket = If(pg = PathConstants.fBasket, True, False)
            isSbFaultBasket = If(pg = PathConstants.sbfBasket, True, False)

            If (pg = PathConstants.fBasket) Then
                SessionManager.setPropertyId(Request.QueryString(PathConstants.Pid).ToString)
                SessionManager.setCustomerId(Convert.ToInt32(Request.QueryString(PathConstants.Cid)))
            ElseIf (pg = PathConstants.sbfBasket) Then
                SessionManager.setSchemeId(Convert.ToInt32(Request.QueryString(PathConstants.Sid).ToString))
                SessionManager.setBlockId(Convert.ToInt32(Request.QueryString(PathConstants.Bid)))
            End If

        ElseIf Request.QueryString(PathConstants.Cid) IsNot Nothing And Request.QueryString(PathConstants.Pid) IsNot Nothing Then
            Me.customerId = CType(Request.QueryString(PathConstants.Cid), Integer)
            Me.propertyId = CType(Request.QueryString(PathConstants.Pid), String)
            isPropertyArea = True
        ElseIf Request.QueryString(PathConstants.Bid) IsNot Nothing And Request.QueryString(PathConstants.Sid) IsNot Nothing Then

            Me.blockId = CType(Request.QueryString(PathConstants.Bid), Integer)
            Me.schemeId = CType(Request.QueryString(PathConstants.Sid), Integer)
            isSchemeBlockArea = True

        ElseIf Request.QueryString(PathConstants.CalView) IsNot Nothing Then
            'the user want to see screen 16, 17
            isCalendarView = IIf(Request.QueryString(PathConstants.CalView) = PathConstants.Yes, True, False)
        ElseIf Request.QueryString(PathConstants.ReportView) IsNot Nothing And Request.QueryString(PathConstants.RepairsDashboard) IsNot Nothing And Request.QueryString(PathConstants.RejectedByContractor) IsNot Nothing Then
            Me.reportPrifix = CType(Request.QueryString(PathConstants.RepairsDashboard), String)
            isRepairsDashboard = True
            isRejectedByContractor = True
            isReportView = IIf(Request.QueryString(PathConstants.ReportView) = PathConstants.Yes, True, False)

        ElseIf Request.QueryString(PathConstants.ReportView) IsNot Nothing And Request.QueryString(PathConstants.RepairsDashboard) IsNot Nothing Then
            'the user want to go to report area via repairdashboard
            Me.reportPrifix = CType(Request.QueryString(PathConstants.RepairsDashboard), String)
            isRepairsDashboard = True
            isReportView = IIf(Request.QueryString(PathConstants.ReportView) = PathConstants.Yes, True, False)
        ElseIf Request.QueryString(PathConstants.ReportView) IsNot Nothing Then
            'the user want to go to report area
            isReportView = IIf(Request.QueryString(PathConstants.ReportView) = PathConstants.Yes, True, False)
        ElseIf Request.QueryString(PathConstants.schedule) IsNot Nothing Then
            'the user want to go to schedule Appointment
            isSchedule = IIf(Request.QueryString(PathConstants.schedule) = PathConstants.Yes, True, False)
        ElseIf Request.QueryString(PathConstants.AccessDenied) IsNot Nothing Then
            isAccessDenied = IIf(Request.QueryString(PathConstants.AccessDenied) = PathConstants.Yes, True, False)

        Else
            Me.redirectToLoginPage()
        End If
    End Sub
#End Region

#Region "check Classic Asp Session"
    Private Sub checkClassicAspSession()
        '''''''''comment this line below for production
        ' classicUserId = 201
        '''''''''comment this line ábove for production

        ''''''''''Un comment these below lines for production
        'ASPSession("USERID") = 760

        If ASPSession("USERID") IsNot Nothing Then
            classicUserId = Integer.Parse(ASPSession("USERID").ToString())
        Else
            Me.redirectToLoginPage()
        End If
        ''''''''''Un comment these above lines for production
    End Sub
#End Region

#Region "redirect To Login Page"
    Private Sub redirectToLoginPage()
        Response.Redirect(PathConstants.LoginPath, True)
    End Sub
#End Region

End Class
