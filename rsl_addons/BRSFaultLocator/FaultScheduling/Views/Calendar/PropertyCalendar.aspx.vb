﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports FLS_BusinessObject

Public Class PropertyCalendar
    Inherits PageBase

#Region "Properties / Attributes"
    Public varWeeklyDT As DataTable = New DataTable()
#End Region

#Region "Events Handling"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMessage.Text = String.Empty

        If (Not IsPostBack) Then
            Me.populateDropDowns()

            'Calendar code start here
            If (Not IsNothing(Request.QueryString("mov"))) Then
                If (Request.QueryString("mov") = "n") Then
                    ApplicationConstants.startDate = DateAdd("d", 7, ApplicationConstants.startDate)
                ElseIf (Request.QueryString("mov") = "p") Then
                    ApplicationConstants.startDate = DateAdd("d", -7, ApplicationConstants.startDate)
                ElseIf (Request.QueryString("mov") = "t") Then
                    ApplicationConstants.startDate = Date.Today
                End If

                pnlCalendar.Visible = True

            ElseIf (Not IsNothing(Request.QueryString("jsn")) AndAlso Not IsPostBack) Then
                Me.ViewFault(Request.QueryString("jsn"))
            ElseIf (Not IsNothing(Request.QueryString("jsv")) AndAlso Not IsPostBack) Then
                Me.ViewVoidsJobSheet(Request.QueryString("jsv"), Request.QueryString("appointmentType"))
            ElseIf (Not IsNothing(Request.QueryString("jsp")) AndAlso Not IsPostBack) Then
                Me.ViewPlannedJobSheet(Request.QueryString("jsp"))
            ElseIf (Not IsNothing(Request.QueryString("jsg")) AndAlso Not IsPostBack) Then
                Me.ViewGasJobSheet(Request.QueryString("jsg"))
            ElseIf (Not IsNothing(Request.QueryString("jsgsb")) AndAlso Not IsPostBack) Then
                Me.ViewSchemeBlockGasJobSheet(Request.QueryString("jsgsb"))
            ElseIf (Not IsNothing(Request.QueryString("jsd")) AndAlso Not IsPostBack) Then
                Me.ViewDefectsJobSheet(Request.QueryString("jsd"))
            ElseIf (Not IsNothing(Request.QueryString("jsm")) AndAlso Not IsPostBack) Then
                Me.ViewMaintenanceJobSheet(Request.QueryString("jsm"))
            Else
                ApplicationConstants.startDate = Date.Today
                Me.saveFiltersState()
            End If
            Me.restoreDropdownsState()
            Me.displayCalendarScheduleAppointment()
            'To correct start and end date of scheduling calendar
            ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
            AddHandler ucAppointmentNotes.CloseButtonClicked, AddressOf btnClose_Click

        End If
    End Sub

#End Region

#Region "Patch Drop down Selected Index Event"

    Protected Sub ddlPatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPatch.SelectedIndexChanged
        Me.populateSchemeDropdown()
        Me.saveFiltersState()
        validateCalenderView()
    End Sub

#End Region

#Region "Scheme Drop down Selected Index Event"

    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        Me.saveFiltersState()
        validateCalenderView()
    End Sub

#End Region

#Region "Trade Drop down Selected Index Event"

    Protected Sub ddlTrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTrade.SelectedIndexChanged
        populateOperativeDropDown()
        Me.saveFiltersState()
        validateCalenderView()

    End Sub
#End Region

#Region "Find Button Click"

    Protected Sub btnFind_Click(ByVal seder As Object, ByVal e As EventArgs) Handles btnFind.Click
        saveFiltersState()
        Me.displayCalendarScheduleAppointment()
    End Sub

#End Region

#Region "Btn notes detail"

    Protected Sub txtDummyNotes_TextChanged(ByVal seder As Object, ByVal e As EventArgs) Handles txtDummyNotes.TextChanged
        Dim appointmentInfo As String() = txtDummyNotes.Text.Split(",")
        Dim appointmentId As Integer = Convert.ToInt32(appointmentInfo(0))
        Dim appointmentType As String = appointmentInfo(1)
        Me.restoreDropdownsState()
        ucAppointmentNotes.loadAppointmentNotes(appointmentId, appointmentType)
        displayCalendarScheduleAppointment()
        txtDummyNotes.Text = String.Empty
        mdlAppointmentNotesPopup.Show()

    End Sub

#End Region



#End Region

#Region "Functions"

#Region "btn Close Click"
    Protected Sub btnClose_Click()
        Try
            mdlAppointmentNotesPopup.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Save Drop down state"

    Protected Sub saveFiltersState()
        Try
            SessionManager.setPropertyCalendarFilters(ddlPatch.SelectedValue + "," + ddlScheme.SelectedValue + "," + ddlTrade.SelectedValue + "," + ddlOperative.SelectedValue + "," + ddlAppointmentType.SelectedValue + "," + ddlJs.SelectedValue + "," + txtTenant.Text + "," + txtAddress.Text)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Restore Drop down state"

    Sub restoreDropdownsState()
        Try

            Dim ddlArry = SessionManager.getPropertyCalendarFilters().Split(",")
            Dim patchId = Convert.ToInt32(ddlArry(0))
            Dim schemeId = Convert.ToInt32(ddlArry(1))
            Dim tradeId = Convert.ToInt32(ddlArry(2))
            Dim operativeId = Convert.ToInt32(ddlArry(3))
            Dim appTypeId = Convert.ToInt32(ddlArry(4))

            ddlJs.SelectedValue = ddlArry(5)
            txtTenant.Text = ddlArry(6)
            txtAddress.Text = ddlArry(7)

            Me.populateDropDowns()

            ddlPatch.SelectedValue = patchId
            Me.populateSchemeDropdown()
            ddlScheme.SelectedValue = schemeId
            ddlTrade.SelectedValue = tradeId
            ddlOperative.SelectedValue = operativeId
            ddlAppointmentType.SelectedValue = appTypeId
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Populate scheme Drop down"

    Sub populateSchemeDropdown()

        Try
            If (ddlPatch.SelectedIndex > 0) Then
                Dim objSchedularSearchBL As SchedularSearchBL = New SchedularSearchBL()
                Dim patchId As Int32 = ddlPatch.SelectedValue
                Dim schemeDataSet As DataSet = New DataSet()

                objSchedularSearchBL.getSchemes(schemeDataSet, patchId)

                ddlScheme.DataSource = schemeDataSet.Tables(0).DefaultView
                ddlScheme.DataValueField = "DEVELOPMENTID"
                ddlScheme.DataTextField = "SCHEMENAME"
                ddlScheme.DataBind()
                Dim schemeItem As ListItem = New ListItem("Please Select", "-1")
                ddlScheme.Items.Insert(0, schemeItem)
            Else
                ddlScheme.Items.Clear()
                Dim schemeItem As ListItem = New ListItem("Please Select", "-1")
                ddlScheme.Items.Insert(0, schemeItem)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Display Search Results On Calendar"

    Protected Sub displayCalendarScheduleAppointment()
        Try

            Dim divCounter As Integer
            Dim objCalendarBL As CalendarBL = New CalendarBL()

            Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
            Dim objCalendarAppointmentsBO As CalendarAppointmentsBO = New CalendarAppointmentsBO()

            Dim dtOperatives As DataTable
            Dim dtAppointments As DataTable
            Dim dtEmployeeLeaves As DataTable
            Dim dtEmployeesCoreWorkingHours As DataTable

            Dim drLeaves() As DataRow
            'Dim drAppointmentJSN() As DataRow

            Dim strAppointment As StringBuilder = New StringBuilder()
            Dim rowCount As Integer

            'Seek for Monday as starting and Sunday ending day of the Week
            If (ApplicationConstants.startDate.ToString("dddd") <> "Monday") Then
                objCalendarUtilities.calculateMonday()
            End If
            If (Request.QueryString("e") <> "p") Then
                'To correct start and end date of scheduling calendar
                ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
            End If

            objCalendarAppointmentsBO.PatchId = ddlPatch.SelectedItem.Value
            objCalendarAppointmentsBO.SchemeId = ddlScheme.SelectedItem.Value
            objCalendarAppointmentsBO.AppointmentType = ddlAppointmentType.SelectedItem.Text
            objCalendarAppointmentsBO.TradeId = ddlTrade.SelectedItem.Value
            objCalendarAppointmentsBO.JSN = ddlJs.SelectedValue
            objCalendarAppointmentsBO.Tenant = txtTenant.Text.Trim
            objCalendarAppointmentsBO.Addresss = txtAddress.Text.Trim
            objCalendarAppointmentsBO.OperativeId = ddlOperative.SelectedValue
            objCalendarAppointmentsBO.StartDate = ApplicationConstants.startDate
            objCalendarAppointmentsBO.EndDate = ApplicationConstants.endDate

            Dim calendarDetailsDataSet As New DataSet

            objCalendarBL.getPropertyCalendarDetails(objCalendarAppointmentsBO, calendarDetailsDataSet)

            dtOperatives = calendarDetailsDataSet.Tables(ApplicationConstants.PropertyCalendarOperativesDt)
            'Getting Employees Leaves Information
            dtEmployeeLeaves = calendarDetailsDataSet.Tables(ApplicationConstants.PropertyCalendarLeavesDt)
            dtAppointments = calendarDetailsDataSet.Tables(ApplicationConstants.PropertyCalendarAppointmentsDt)
            dtEmployeesCoreWorkingHours = calendarDetailsDataSet.Tables(ApplicationConstants.PropertyCalendarCoreWorkingHoursDt)


            'Getting Appointment Row Schema
            Dim drAppointment = dtAppointments.NewRow

            If (dtOperatives.Rows.Count > 0) Then 'If Gas Engineer exist
                'Creating Columns based on Engineers names
                varWeeklyDT.Columns.Add(" ")
                For dsColCount = 0 To dtOperatives.Rows.Count - 1
                    objCalendarUtilities.createColumns(dtOperatives.Rows(dsColCount).Item(0).ToString() + ":" + dtOperatives.Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
                Next

                'Getting Appointments Information in Datarow for Data table {varWeeklyDT}
                Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
                'Drawing Weekly Calendar

                Dim appointmentIcon As String
                Dim startLeaveTime, endLeaveTime, startLeaveDate As String                

                For dayCount = 0 To 6
                    scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                    rowCount = 1
                    For dsColCount = 0 To dtOperatives.Rows.Count - 1

                        Dim runningDate = DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString()
                        Dim operative = dtOperatives.Rows(dsColCount).Item(0).ToString()

                        drLeaves = dtEmployeeLeaves.Select("(EMPLOYEEID =" + operative + " AND ((StartDate <= '" + runningDate + "' AND EndDate >= '" + runningDate + "' ) OR (StartDate <= '" + runningDate + "' AND EndDate IS NULL )))")

                        If (drLeaves.Count > 0) Then
                            For leaveCounter As Integer = 0 To drLeaves.Count - 1
                                startLeaveTime = drLeaves(leaveCounter).Item(7).ToString()
                                endLeaveTime = drLeaves(leaveCounter).Item(8).ToString()
                                startLeaveDate = String.Format("{0:dd MMM yyyy}", drLeaves(leaveCounter).Item(2))
                                If (drLeaves(leaveCounter).Item(1).ToString().Contains("Training:")) Then
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><table> <tr><td><img src='../../images/arrow_right.png' border=0/></td><td><span style='margin-right:10px;'><b>" + drLeaves(leaveCounter).Item(1).ToString() + "</b></span></td></tr></table> </div>")
                                Else
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><table> <tr><td><img src='../../images/arrow_right.png' border=0/></td><td><span style='margin-right:10px;'><b>" + drLeaves(leaveCounter).Item(1).ToString() + "</b></span></td></tr></table> <br/> Start Date : " + startLeaveDate + " <br/>" + startLeaveTime + "-" + endLeaveTime + "</div><br/>")
                                End If
                            Next
                        End If

                        For rdCount = 0 To dtAppointments.Rows.Count - 1

                            drAppointment = dtAppointments.Rows(rdCount)
                            'Check the appointments of the engineers in Datarow
                            Dim aptStartDate As DateTime = System.DateTime.Parse(drAppointment.Item("AppointmentDate").ToString())
                            Dim aptEndDate As DateTime = System.DateTime.Parse(drAppointment.Item("AppointmentEndDate").ToString())
                            Dim currentDate As DateTime = DateAdd("d", dayCount, ApplicationConstants.startDate)
                            Dim operativeId As Integer = dtOperatives.Rows(dsColCount).Item(0)
                            Dim drEmployeesCoreWorkingHours() As DataRow = dtEmployeesCoreWorkingHours.Select("EmployeeId=" + operativeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")

                            If (currentDate >= aptStartDate And currentDate <= aptEndDate _
                                And operativeId = drAppointment.Item("OperativeID") _
                                And drEmployeesCoreWorkingHours.Count > 0) Then

                                ' Get appointment icon for appointment type or use default.
                                appointmentIcon = ApplicationConstants.appointmentIcons("Default")
                                If ApplicationConstants.appointmentIcons.Keys.Contains(drAppointment("AppointmentType").ToString()) Then
                                    If (drAppointment("IsVoid").ToString().Equals("1")) Then 'Specifically added for Void Servicing only
                                        appointmentIcon = ApplicationConstants.appointmentIcons("VoidGAS")
                                    Else
                                        appointmentIcon = ApplicationConstants.appointmentIcons(drAppointment("AppointmentType").ToString())
                                    End If

                                End If

                                If drAppointment("AppointmentType").ToString().Contains("Void") Then
                                    appointmentIcon = ApplicationConstants.appointmentIcons("Void")
                                End If
                                strAppointment.Append("<div class='main-box'><div class='box-cover'><div class='box-column1'><img src='../../images/AppointmentTypeIcons/" + appointmentIcon + "' alt='' border=0 width='20' /><a href='javascript:void(0)'  id='arrow-left" + divCounter.ToString() + "' onclick='displayMore(" + divCounter.ToString() + ");return false;'> <img src='../../images/arrow_right.png' alt='' border=0 /></a>")
                                'strAppointment.Append("<div class='main-box-current'><div class='box-cover'><div class='box-column1'><a href='#' id='arrow-left" + divCounter.ToString() + "' onclick='displayMore(" + divCounter.ToString() + "," + tenancy.ToString() + ");return false;'> <img src='../../images/arrow_right.png' alt='' border=0 /></a>")
                                strAppointment.Append("<a href='javascript:void(0)' id='arrow-down" + divCounter.ToString() + "' style='display:none;' onclick='displayMoreHide(" + divCounter.ToString() + ");'> <img src='../../images/down_arrow.gif' alt='' border=0 /></a></div><div class='box-column2'>" + getAppointmentStartEndTime(drAppointment, dtEmployeesCoreWorkingHours, currentDate, rdCount, operativeId) + "</div>")

                                'Adding Status Icons
                                Dim appointmentStatus As String = drAppointment.Item("FaultStatus").ToString()
                                If appointmentStatus = "In Progress" OrElse appointmentStatus = "InProgress" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/inprogress.png'></div>")
                                ElseIf appointmentStatus = "Paused" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/paused.png'></div>")
                                ElseIf appointmentStatus = "Appointment Arranged" OrElse appointmentStatus = "Arranged" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/arranged.png'></div>")
                                ElseIf appointmentStatus = "Complete" OrElse appointmentStatus = "Completed" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/completed.png'></div>")
                                End If

                                ' Checking the appointment was scheduled from Calendar or not 
                                ' If appointment was scheduled from calendar then show icon.
                                Dim isCalendarApt As Boolean = False
                                If (Not IsDBNull(drAppointment("IsCalendarAppointment"))) Then
                                    isCalendarApt = drAppointment("IsCalendarAppointment")
                                End If

                                If (isCalendarApt) Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../Images/calendarFlag.png'></div>")
                                End If
                                'End If

                                Dim notes As String = drAppointment.Item("AppointmentNotes").ToString()
                                If notes.Trim.Length > 0 Then
                                    Dim inspectionType As String = drAppointment.Item("InspectionType").ToString()
                                    Dim appointmentId As String = drAppointment.Item("AppointmentID").ToString()
                                    Dim argument As String = String.Format("{0},{1}", appointmentId, inspectionType)
                                    strAppointment.Append("<div class='box-column3'> <a href='javascript:void(0)' onclick='showAppointmentNotes(""" & argument & """);'> <img src='../../images/editCustomerNotes.png'  style='width:18px;'  alt='' border=0 /></a></div>")
                                End If
                                'Adding Address for collapsed view
                                If (drAppointment.Item("CustAddress").ToString() <> "") Then
                                    strAppointment.Append("<div id='addressDiv" + divCounter.ToString() + "' style='display:block;'><div class='box2-column2'>" + drAppointment.Item("CustAddress").ToString() + "</div></div>")
                                End If
                                'strAppointment.Append("</div>")
                                'If (dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString() <> "") Then
                                '    strAppointment.Append("<div id='display" + divCounter.ToString() + "' style='display:none;'><div class='box-cover2'></div>")
                                'Else
                                strAppointment.Append("<div id='display" + divCounter.ToString() + "' style='display:none;'>")
                                ' End If



                                'Closing Main box and adding a line break to seprate it from next appointment of day.

                                'Adding Job Sheet Numbers
                                ' drAppointmentJSN = dsAppointments.Tables(1).Select("AppointmentId=" + dsAppointments.Tables(0).Rows(rdCount).Item(8).ToString())
                                ' If (drAppointmentJSN.Count > 0) Then
                                strAppointment.Append("<div class='inner-box'><div class='box-cover-des-inner-box'>")

                                Dim jsnLink As String = String.Empty
                                If dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.SbFaultAppointment Then
                                    jsnLink = "PropertyCalendar.aspx?isSb=yes&jsn=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()
                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.FaultAppointment Then
                                    jsnLink = "PropertyCalendar.aspx?jsn=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()
                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).Contains("Void") Then
                                    jsnLink = "PropertyCalendar.aspx?jsv=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber") + "&appointmentType=" + dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn)
                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.PlannedAppointment _
                                Or dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.MiscAppointment _
                                Or dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.AdaptationAppointment _
                                Or dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.ConditionAppointment Then
                                    jsnLink = "PropertyCalendar.aspx?jsp=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()
                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.GasAppointment Then
                                    jsnLink = "PropertyCalendar.aspx?jsg=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()
                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.DefectAppointment Then
                                    jsnLink = "PropertyCalendar.aspx?jsd=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()

                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.GasAppointmentForSchemeBlock Then
                                    jsnLink = "PropertyCalendar.aspx?jsgsb=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()

                                ElseIf dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.CyclicMaintenanceAppointment _
                                                                Or dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.MEServicingAppointment Then
                                    jsnLink = "PropertyCalendar.aspx?jsm=" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString()
                                Else
                                    jsnLink = ApplicationConstants.NumberSymbol
                                End If
                                'jsnLink = "SchedulingCalendar.aspx?jsn=" + dtAppointments.Rows(rdCount).Item(2).ToString()
                                Dim type As String = dtAppointments.Rows(rdCount).Item(ApplicationConstants.AppointmentTypeColumn).ToString()

                                If (type = "GASSB") Then
                                    type = "GAS"
                                End If

                                Dim appointmentType As String = type + "Appointment"

                                strAppointment.Append("<div class='box2-column2' style='padding-left:5px;'>" + appointmentType + "</div><div class='box2-column2' style='padding-left:5px;'><a title='" + appointmentType + "' href='" + jsnLink + "'><u>" + dtAppointments.Rows(rdCount).Item("JobsheetNumber").ToString() + "</u></a></div>")
                                'strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>" + drAppointmentJSN(i).Item(3).ToString() + "</div>")
                                strAppointment.Append("</div><br/>")
                                strAppointment.Append("</div></div></div></div>")
                                strAppointment.Append("<div style='clear:both'></div>")
                                divCounter = divCounter + 1


                            End If
                        Next

                        If (strAppointment.ToString() <> "") Then
                            scheduledAppt(rowCount) = strAppointment.ToString()
                            strAppointment.Clear()
                        Else
                            scheduledAppt(rowCount) = ""
                        End If
                        rowCount = rowCount + 1
                    Next

                    varWeeklyDT.Rows.Add(scheduledAppt)

                Next
                pnlCalendar.Visible = True
                lblMonth.Text = ApplicationConstants.startDate.Day.ToString() + "-" + DateAdd("d", 6, ApplicationConstants.startDate).Day.ToString() + " " + MonthName(ApplicationConstants.startDate.Month) + " " + Year(ApplicationConstants.startDate).ToString()
            Else
                pnlCalendar.Visible = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EngNotFound, True)
            End If
            'ApplicationConstants.isMoved = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'lbNextFault.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Get Appointment Start and End Time"

    Function getAppointmentStartEndTime(ByVal drAppointment As DataRow, ByVal dtEmployeesCoreWorkingHours As DataTable, ByVal currentDate As DateTime, ByVal rdCount As Integer, ByVal employeeId As Integer)

        Dim aptStartDate As DateTime = System.DateTime.Parse(drAppointment.Item("AppointmentDate").ToString())
        Dim aptEndDate As DateTime = System.DateTime.Parse(drAppointment.Item("AppointmentEndDate").ToString())
        Dim aptStartTime As String = String.Empty
        Dim aptEndTime As String = String.Empty
        Dim empCoreStartTime As String = String.Empty
        Dim empCoreEndTime As String = String.Empty
        Dim dayStartHour As Double = GeneralHelper.getDayStartHour()
        Dim dayEndHour As Double = GeneralHelper.getDayEndHour()
        Dim ofcCoreStartTime As String = DateTime.Parse(Convert.ToInt32(dayStartHour - dayStartHour Mod 1).ToString() + ":" + IIf(dayStartHour Mod 1 = 0, "00", Convert.ToInt32((dayStartHour Mod 1) * 60).ToString())).ToString("HH:mm")
        Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + IIf(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString())).ToString("HH:mm")


        'Get employee core working hour
        Dim dr() As DataRow = dtEmployeesCoreWorkingHours.Select("EmployeeId=" + employeeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")
        If (dr.Count > 0) Then
            empCoreStartTime = DateTime.Parse(dr(0).Item(3).ToString()).ToString("HH:mm")
            empCoreEndTime = DateTime.Parse(dr(0).Item(4).ToString()).ToString("HH:mm")
        Else
            empCoreStartTime = ofcCoreStartTime
            empCoreEndTime = ofcCoreEndTime
        End If

        'Setting start and end time depending on cases
        'Case 1 : Single day appointment start time and end time exist in same day
        'Case 2 : Multiple day appointment where start time exist in current date
        'Case 3 : Multiple day appointment where end time exist in current date
        'Case 4 : Multiple day appointment where start and end time do not exist in current date

        If (aptStartDate = aptEndDate) Then
            aptStartTime = DateTime.Parse(drAppointment.Item("Time")).ToString("HH:mm")
            aptEndTime = DateTime.Parse(drAppointment.Item("EndTime")).ToString("HH:mm")
        ElseIf (currentDate = aptStartDate And currentDate <> aptEndDate) Then
            aptStartTime = DateTime.Parse(drAppointment.Item("Time")).ToString("HH:mm")
            aptEndTime = empCoreEndTime
        ElseIf (currentDate <> aptStartDate And currentDate = aptEndDate) Then
            aptStartTime = empCoreStartTime
            aptEndTime = DateTime.Parse(drAppointment.Item("EndTime")).ToString("HH:mm")
        Else
            aptStartTime = empCoreStartTime
            aptEndTime = empCoreEndTime
        End If

        Return aptStartTime + " - " + aptEndTime

    End Function

#End Region



#Region "Populate DropDowns"

    Sub populateDropDowns()
        Try
            Dim objSchedularSearchBL As SchedularSearchBL = New SchedularSearchBL()
            Dim patchDataSet As DataSet = New DataSet()
            Dim tradeDataSet As DataSet = New DataSet()

            'Patch Dropdown List
            If (ddlPatch.Items.Count <= 0) Then
                objSchedularSearchBL.getPatchesAndTrades(patchDataSet, tradeDataSet)
                ddlPatch.DataSource = patchDataSet.Tables(0).DefaultView
                ddlPatch.DataValueField = "PATCHID"
                ddlPatch.DataTextField = "LOCATION"
                ddlPatch.DataBind()

                ddlPatch.Items.Insert(0, New ListItem("Please Select", "-1"))
            End If
            'Scheme Dropdown List
            If (ddlScheme.Items.Count <= 0) Then
                ddlScheme.Items.Insert(0, New ListItem("Please Select", "-1"))
            End If
            'Trade Dropdown List
            If (ddlTrade.Items.Count <= 0) Then
                ddlTrade.DataSource = tradeDataSet.Tables(0).DefaultView
                ddlTrade.DataValueField = "TradeId"
                ddlTrade.DataTextField = "Description"
                ddlTrade.DataBind()
                ddlTrade.Items.Insert(0, New ListItem("Please Select", "-1"))
            End If
            If (ddlOperative.Items.Count = 0) Then
                populateOperativeDropDown()
            End If

            If (ddlAppointmentType.Items.Count = 0) Then
                populateAppointmentTypeDropDown()
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region



#Region "Populate Operatives Drop down filter by Trade Id"

    Protected Sub populateOperativeDropDown()
        Dim lstDropDownBo As New List(Of DropDownBO)
        Dim calendarBL As New CalendarBL

        calendarBL.getOperativeFilteredByTradeId(lstDropDownBo, ddlTrade.SelectedValue)

        ddlOperative.DataValueField = "ID"
        ddlOperative.DataTextField = "Description"
        ddlOperative.DataSource = lstDropDownBo
        ddlOperative.DataBind()
        ddlOperative.Items.Insert(0, New ListItem("Please Select", "-1"))
    End Sub

#End Region


#Region "Populate Appointment type"

    Protected Sub populateAppointmentTypeDropDown()
        Dim lstDropDownBo As New List(Of DropDownBO)
        Dim calendarBL As New CalendarBL

        calendarBL.GetAppointmentType(lstDropDownBo)

        ddlAppointmentType.DataValueField = "ID"
        ddlAppointmentType.DataTextField = "Description"
        ddlAppointmentType.DataSource = lstDropDownBo
        ddlAppointmentType.DataBind()
    End Sub

#End Region


#Region "Reset controls"
    Sub validateCalenderView()
        If (ddlPatch.SelectedItem.Value = -1 AndAlso ddlScheme.SelectedItem.Value = -1 AndAlso ddlTrade.SelectedItem.Value = -1 AndAlso ddlAppointmentType.SelectedItem.Value = -1) Then
            pnlCalendar.Visible = False
        End If
    End Sub
#End Region

#Region "View Job Sheet detail move to screen 13"

    Protected Sub ViewFault(ByVal jobSheetNumber As String)
        'lblFaultIdHidden.Text = jobSheetNumber
        If Not IsNothing(Request.QueryString(PathConstants.isSb)) AndAlso Request.QueryString(PathConstants.isSb) = PathConstants.Yes Then
            ifrmJobSheet.Attributes("src") = GeneralHelper.getSbJobSheetSummaryAddress() + jobSheetNumber
        Else
            ifrmJobSheet.Attributes("src") = GeneralHelper.getJobSheetSummaryAddress() + jobSheetNumber
        End If

        popupJobSheet.Show()
    End Sub

#End Region

#Region "View Job Sheet detail for voids "

    Protected Sub ViewVoidsJobSheet(ByVal jobSheetNumber As String, ByVal appointmentType As String)
        ifrmJobSheet.Attributes("src") = GeneralHelper.getVoidJobSheetSummaryAddress() + jobSheetNumber + "&appointmentType=" + appointmentType
        popupJobSheet.Show()
    End Sub

#End Region

#Region "View Job Sheet detail for Planned Appointment "

    Protected Sub ViewPlannedJobSheet(ByVal jobSheetNumber As String)
        ifrmJobSheet.Attributes("src") = GeneralHelper.getPlannedJobSheetSummaryAddress() + jobSheetNumber
        popupJobSheet.Show()
    End Sub

#End Region

#Region "View Job Sheet detail for Gas Appointment "

    Protected Sub ViewGasJobSheet(ByVal jobSheetNumber As String)
        ifrmJobSheet.Attributes("src") = GeneralHelper.getGasJobSheetSummaryAddress() + jobSheetNumber
        popupJobSheet.Show()
    End Sub

#End Region

#Region "View Job Sheet detail for Scheme/Block Gas Appointment "

    Protected Sub ViewSchemeBlockGasJobSheet(ByVal jobSheetNumber As String)
        ifrmJobSheet.Attributes("src") = GeneralHelper.getSchemeBlockGasJobSheetSummaryAddress() + jobSheetNumber
        popupJobSheet.Show()
    End Sub

#End Region

#Region "View Job Sheet detail for Defects Appointment "

    Protected Sub ViewDefectsJobSheet(ByVal jobSheetNumber As String)
        ifrmJobSheet.Attributes("src") = GeneralHelper.getDefectsJobSheetSummaryAddress() + jobSheetNumber
        popupJobSheet.Show()
    End Sub

#End Region

#Region "View Job Sheet detail for Maintenance Appointment "

    Protected Sub ViewMaintenanceJobSheet(ByVal jobSheetNumber As String)
        ifrmJobSheet.Attributes("src") = GeneralHelper.getMEJobSheetSummary() + jobSheetNumber
        popupJobSheet.Show()
    End Sub

#End Region
#End Region

End Class