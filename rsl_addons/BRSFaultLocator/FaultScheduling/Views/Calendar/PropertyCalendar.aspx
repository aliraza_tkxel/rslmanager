﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/RDMasterPage.Master"
    CodeBehind="PropertyCalendar.aspx.vb" Inherits="FaultScheduling.PropertyCalendar"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="appointmentNotesTag" TagName="AppointmentNotes" Src="~/Controls/Fault/AppointmentNotes.ascx" %>
<asp:Content ID="Content4" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/aptCalendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
        /* Applied his rule set to hide wrapper for fixed right column, as we are not using fixed right column
         * In case right column fix is required rule set must be removed/changed.
         */
        .DTFC_RightBodyLiner
        {
            display: none;
        }
        .select_div select
        {
            width: 175px !important;
        }
        .detailBoxSmall
        {
            border: 1px solid #E8E9EA;
        }
        .color-black
        {
            color:#000 !important;
        }
    </style>
    <script src="../../Scripts/jquery.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.fixedColumns.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function displayMore(value) {
            document.getElementById("display" + value).style.display = 'block';
            document.getElementById("arrow-left" + value).style.display = 'none';
            document.getElementById("arrow-down" + value).style.display = 'block';
            //document.getElementById("addressDiv" + value).style.display = 'none';
        }

        function displayMoreHide(value) {
            document.getElementById("display" + value).style.display = 'none';
            document.getElementById("arrow-left" + value).style.display = 'block';
            document.getElementById("arrow-down" + value).style.display = 'none';
            // document.getElementById("addressDiv" + value).style.display = 'block';
        }

        function showAppointmentNotes(value) {
            document.getElementById('<%= txtDummyNotes.ClientID %>').value = value;
            __doPostBack('<%= txtDummyNotes.ClientID %>', '');
        }

        function PrintJobSheet() {
            javascript: window.open('PrintJobSheetDetail.aspx?JobSheetNumber=' + document.getElementById("lblFaultId").innerHTML);
            return false;
        }

        $(document).ready(function () {

            //applyFixHeader();

            var offset = $('#tblCalendar').offset();
            var myCalendartableheight = $('#tblCalendar').height();

            if (myCalendartableheight > $(window).height() - offset.top) {
                myCalendartableheight = $(window).height() - offset.top;
            }

            myCalendartableheight -= 10;

            var calendarTable = $('#tblCalendar').DataTable({
                "searching": false,
                "paging": false,
                "ordering": false,
                "info": false,
                "scrollX": true,
                "scrollCollapse": true,
                "scrollY": myCalendartableheight
            });

            new $.fn.dataTable.FixedColumns(calendarTable, {
                heightMatch: 'none'
            });

            setTimeout(function () {
                var tblCalendarRows = $("#tblCalendar > tbody > tr");
                for (index = 0; index < tblCalendarRows.length; index++) {
                    $("div.DTFC_LeftBodyWrapper div.DTFC_LeftBodyLiner > table > tbody > tr").eq(index).height($("#tblCalendar > tbody > tr").eq(index).height());
                }
            }, 50);
        });    
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="portlet">
        <div class="header">
            <span style="padding-left:10px; font-weight: bold;">
                Calendar
            </span>
        </div>
        <div class="portlet-body" style="padding-bottom:0px;">
            <div class="searchInputTable" style="overflow:auto;">
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Patch:
                        </div>
                        <div class="field" style="margin-left: 22px;">
                            <asp:DropDownList ID="ddlPatch" class="styleselect" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Operative:
                        </div>
                        <div class="field">
                            <asp:DropDownList ID="ddlOperative" class="styleselect" runat="server" ToolTip="Select an operative to filter calendar.">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Type:
                        </div>
                        <div class="field" style="    margin-left: 26px !important;">
                            <asp:DropDownList ID="ddlAppointmentType" class="styleselect" runat="server" ToolTip="Please Select">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            JS:
                        </div>
                        <div class="field" style="    margin-left: 36px !important;">
                            <asp:DropDownList ID="ddlJs" class="styleselect" runat="server">
                            <asp:ListItem Text="All" Value="All" />
                            <asp:ListItem Text="JS" Value="JS" />
                            <asp:ListItem Text="JSV" Value="JSV" />
                            <asp:ListItem Text="JSN" Value="JSN" />
                            <asp:ListItem Text="JSD" Value="JSD" />
                            <asp:ListItem Text="JSG" Value="JSG" />
                        </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Scheme:
                        </div>
                        <div class="field">
                            <asp:DropDownList ID="ddlScheme" class="styleselect" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Trade:
                        </div>
                        <div class="field" style="margin-left: 30px !important;">
                            <asp:DropDownList ID="ddlTrade" class="styleselect" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Address:
                        </div>
                        <div class="field">
                            <asp:TextBox ID="txtAddress" runat="server" Style="height: 20px; width: 170px;"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-control">
                    <div class="select_div">
                        <div class="label">
                            Tenant:
                        </div>
                        <div class="field">
                            <asp:TextBox ID="txtTenant" runat="server" Style="height: 20px; width: 170px;"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-control right" style="padding:10px 12px 10px 10px;">
                    <div class="field">
                        <asp:Button ID="btnFind" CssClass="btn btn-xs btn-blue right" style="padding: 3px 15px !important;" runat="server" Text="Find" />
                    </div>
                </div>
            </div>
            <hr />
            <!--Calendar Code Start Here-->
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <asp:Panel ID="pnlCalendar" runat="server" Visible="false">
                <div style="width: 98%; padding:10px">
                    <div style="background-color: #34AEE1; padding: 0px 10px;">
                        <asp:Label ID="lblMonth" runat="server" Font-Bold="True" ForeColor="Black"
                            Font-Size="Large" Height="100%"></asp:Label>
                        <span style="padding: 3px 0 0 0 !important;" class="right">
                            <asp:HyperLink ID="hrPrevious" runat="server" NavigateUrl="PropertyCalendar.aspx?mov=p">
                                <img src="../../images/arrow_left.png" border="0" style="padding: 1px 1px !important; border-radius: 4px 0 0 4px;
                                    vertical-align: text-top;float:left" alt="" class="btn btn-xs btn-blue" />
                            </asp:HyperLink>
                            <span style="border-left: solid 1px gray; border-right: solid 1px gray; float: left;
                                font-size: 12px; height: 17px;">
                                <asp:HyperLink ID="hlToday" class="btn btn-xs btn-blue color-black" Font-Bold="True" ForeColor="Black" style="padding:0 5px !important; border-radius: 0; line-height:normal" runat="server" NavigateUrl="PropertyCalendar.aspx?mov=t">Today</asp:HyperLink>
                            </span>
                            <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="PropertyCalendar.aspx?mov=n">
                                <img src="../../images/arrow_right_.png" border="0" style="padding: 1px 1px !important; border-radius: 0 4px 4px 0;
                                    vertical-align: text-top;float:left" alt="" class="btn btn-xs btn-blue" />
                            </asp:HyperLink>
                        </span>
                    </div>
                    <% 
                        If varWeeklyDT.Rows.Count > 0 Then%>
                    <table id="tblCalendar" style="border: 0px; border-left: solid 1px gray; border-top: solid 1px gray;
                        border-collapse: collapse; width: 98%">
                        <thead>
                            <tr>
                                <%  Dim empArray() As String
                                    Dim bgColor As String
                                    For dtCount = 0 To varWeeklyDT.Columns.Count - 1%>
                                <td style="font-weight: bold; border-left: solid 1px gray; border-right: solid 1px gray;
                                    border-bottom: solid 1px gray; text-align: center; min-width: 95px; white-space: nowrap;
                                    background-color: White;">
                                    <%  If varWeeklyDT.Columns(dtCount).ToString() <> " " Then
                                            empArray = varWeeklyDT.Columns(dtCount).ToString().Split(":")
                                            Response.Write(empArray(1).Trim())
                                        End If%>
                                </td>
                                <%Next%>
                            </tr>
                        </thead>
                        <tbody>
                            <%  For rdCount = 0 To varWeeklyDT.Rows.Count - 1
                                    If (rdCount Mod 2 = 0) Then
                                        bgColor = "#E8E9EA"
                                    Else
                                        bgColor = "#FFFFFF"
                                    End If
                            %>
                            <tr style='<%response.write("background-color:"+ bgColor+";")%>'>
                                <%  For rdItemCount = 0 To varWeeklyDT.Columns.Count - 1
                                        If Not String.IsNullOrEmpty(varWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then

                                            If rdItemCount = 0 Then
                             
                                %>
                                <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; min-width: 95px;
                                    white-space: nowrap;">
                                    <%--<div class="subHeading">--%>
                                    <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%>
                                    <%--</div>--%>
                                </td>
                                <%Else%>
                                <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; min-width: 95px;">
                                    <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%>
                                </td>
                                <%End If%><%Else%>
                                <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; min-width: 95px;">
                                    &nbsp;
                                </td>
                                <%End If%><%
                                          Next%>
                            </tr>
                            <%  Next%>
                        </tbody>
                    </table>
                    <%  End If
                    %>
                    <%--<div style="float: right;">
                        </div>--%>
                    <%--</div>--%>
                </div>
            </asp:Panel>
            <!--Calendar Code End Here -->
            <%--Appointment Notes Popup Start--%>
            <asp:Panel ID="pnlAppointmentNotesPopup" runat="server">
                <div style="float: right;">
                    <img src="../../Images/cross2.png" style="position: absolute; top: -12px; right: -12px;
                        border: 0px;" id="imgBtnClose" />
                </div>
                <asp:UpdatePanel ID="updPanelAppointmentNotes" runat="server">
                    <ContentTemplate>
                        <appointmentNotesTag:AppointmentNotes ID="ucAppointmentNotes" runat="server" MaxValue="10"
                            MinValue="1" />
                        <asp:TextBox ID="txtDummyNotes" AutoPostBack="true" runat="server" OnTextChanged="txtDummyNotes_TextChanged"
                            BorderStyle="None" ForeColor="White" Style="display: none;"></asp:TextBox>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtDummyNotes" EventName="TextChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Label ID="lblDisplayAppointmentNotesPopup" runat="server" Text=""></asp:Label>
            <ajaxtoolkit:modalpopupextender id="mdlAppointmentNotesPopup" runat="server" dynamicservicepath=""
                enabled="True" targetcontrolid="lblDisplayAppointmentNotesPopup" popupcontrolid="pnlAppointmentNotesPopup"
                dropshadow="true" backgroundcssclass="modalBackground" cancelcontrolid="imgBtnClose">
            </ajaxtoolkit:modalpopupextender>
            <%--Appointment Notes Popup End--%>
            <!-- ModalPopupExtender -->
            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
            <ajaxtoolkit:modalpopupextender id="popupJobSheet" runat="server" popupcontrolid="pnlJobSheet"
                targetcontrolid="btnHidden" cancelcontrolid="btnClose" backgroundcssclass="modalBackground">
            </ajaxtoolkit:modalpopupextender>
            <!-- ModalPopupExtender -->
            <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="min-height: 600px;
                height: 90%; min-width: 900px; overflow: auto; display: none;">
                <iframe runat="server" id="ifrmJobSheet" class="ifrmJobSheet" style="min-height: 575px;
                    overflow: auto; width: 100%; border: 0px none transparent; height: 96%;" />
                <div style="width: 100%; text-align: left; clear: both;">
                    <div style="float: right;">
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="margin_right20" />
                    </div>
                </div>
                <br />
            </asp:Panel>
        </div>
    </div>
    
</asp:Content>
