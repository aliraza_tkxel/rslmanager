﻿Imports System
Imports FLS_BusinessObject
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_Utilities
Imports System.Drawing

Public Class SbFaultBasket
    Inherits PageBase

    Dim isRecall As Boolean = False
    Dim source As String = String.Empty
    Dim schemeBlockBo As SchemeBlockBO = New SchemeBlockBO()
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()


#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'SessionManager.setSchemeId(1)
        If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
            isRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
        End If

        If (Not IsPostBack) Then
            prePopulatedValues()
            populateSchemeBlockObject()
            populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
        End If
    End Sub

#End Region

#Region "Sort basket"
    Protected Sub gdFaultBasket_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gdFaultBasket.Sorting
        ViewState(ViewStateConstants.SortExpression) = e.SortExpression
        ViewState(ViewStateConstants.SortDirection) = "asc"
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
    End Sub
#End Region

#Region "Page Index Changing"
    Protected Sub gdFaultBasket_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdFaultBasket.PageIndexChanging
        gdFaultBasket.PageIndex = e.NewPageIndex
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
    End Sub
#End Region

#Region "Bind Row Data"
    Protected Sub gdFaultBasket_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdFaultBasket.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim lbIsConfirmed As LinkButton = DirectCast(e.Row.FindControl("isAppointmentConfirmed"), LinkButton)
                Dim checkBox As CheckBox = DirectCast(e.Row.FindControl("chkGroup"), CheckBox)
                checkBox.Attributes.Add("onClick", "highlightDescription(" + e.Row.RowIndex.ToString + ",this.checked);")

                validateDurationFormat(e.Row)

                ' Check whether appointment is confirmed against this fault
                Dim checkConfirmed As Boolean
                Boolean.TryParse(lbIsConfirmed.CommandArgument.ToString.ToLower, checkConfirmed)

                If (checkConfirmed) Then
                    Dim lbFaultLogId As LinkButton = DirectCast(e.Row.FindControl("imgCompleted"), LinkButton)

                    If (lbFaultLogId.CommandArgument = Nothing) Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.JSNNotAvailable, True)
                    Else
                        Dim faultLogId As Integer = Convert.ToInt32(lbFaultLogId.CommandArgument.ToString)

                        'Populate Appointment Confirmed Controls
                        manipulateAppointmentConfirmedControls(e.Row, faultLogId)
                    End If


                Else
                    'Populate Contractors Dropdown
                    populateContractorsDropdownList(e.Row)
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Check Box Event"
    Protected Sub chkGroup_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim chkStatus As CheckBox = DirectCast(sender, CheckBox)
            Dim row As GridViewRow = DirectCast(chkStatus.NamingContainer, GridViewRow)
            Dim lnkBtnDescription As Label = DirectCast(row.FindControl("lnkBtnDescription"), Label)

            If chkStatus.Checked = True Then
                lnkBtnDescription.Font.Bold = True
            Else
                lnkBtnDescription.Font.Bold = False

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Schedule Selected Faults"
    Protected Sub btnScheduleSelectedFaults_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleSelectedFaults.Click

        Dim checkedTempFaultList As New ArrayList
        Dim jobType As Integer = 0
        lblMessage.Text = String.Empty
        Try
            If (Not checkAnySelected(checkedTempFaultList)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectFault, True)
            ElseIf (Not checkJobSimilarity(checkedTempFaultList, jobType)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.JobsNotSame, True)
            Else
                If (jobType = 0) Then
                    SessionManager.setSbTemporaryFaultBo(checkedTempFaultList)

                    If (rbtnGroupTradeYes.Checked) Then
                        SessionManager.setGroupBy(True)
                    Else
                        SessionManager.setGroupBy(False)
                    End If

                    If isRecall Then
                        Response.Redirect(PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbFaultBasketSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
                    Else
                        Response.Redirect(PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbFaultBasketSrcVal))
                    End If


                Else

                    If checkedTempFaultList.Count > 1 Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectOneFault, True)
                    Else
                        SessionManager.setSbTemporaryFaultBo(checkedTempFaultList)
                        saveSubcontractors()
                        ' Get array from Viewstate
                        Dim arrTempFaultBO As ArrayList = DirectCast(SessionManager.getSbTemporaryFaultBo, ArrayList)
                        ' Get current temp fault BO 
                        Dim tempFaultBO As TempFaultBO = arrTempFaultBO.Item(0)

                        Dim resultDataSet As DataSet = New DataSet()
                        Dim objFaultsBl As FaultBL = New FaultBL()

                        objFaultsBl.getSubcontractorJobSheetDetails(resultDataSet, tempFaultBO)
                        populateCurrentFaultBO(resultDataSet)
                        pnlAssignToContractor.Visible = True
                        ucAssignToContractor.PopulateControl()
                        popupAssignToContractor.Show()
                        'If (Not IsNothing(Request.QueryString(PathConstants.IsRecall))) Then
                        '    Response.Redirect(PathConstants.SbJobSheetSummarySubcontractor + GeneralHelper.getSrc(PathConstants.SbFaultBasketSrcVal) + "&" + PathConstants.IsRecall + "=" + Request.QueryString(PathConstants.IsRecall))
                        'Else
                        '    Response.Redirect(PathConstants.SbJobSheetSummarySubcontractor + GeneralHelper.getSrc(PathConstants.SbFaultBasketSrcVal))
                        'End If
                    End If

                   
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub
#End Region

#Region "Add More Faults"
    Protected Sub btnAddMoreFaults_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddMoreFaults.Click
        redirectToSearch()
    End Sub
#End Region

#Region "Close basket window and return to search screen"
    Protected Sub btnCloseWindow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCloseWindow.Click
        'redirectToCustomer()
    End Sub
#End Region

#Region ""
    Protected Sub lnkBtnDescription_Clicked(ByVal sender As Object, ByVal e As EventArgs)
        'Get Row Number
        Dim lnkBtnDescription As LinkButton = DirectCast(sender, LinkButton)
        Dim row As GridViewRow = DirectCast(lnkBtnDescription.NamingContainer, GridViewRow)
        Dim rowNumber As Integer = row.RowIndex

        'Get Location,Notes
        Dim location, notes As String
        Dim tempFaultId As Integer

        location = DirectCast(row.FindControl("lblLocation"), Label).Text
        notes = DirectCast(row.FindControl("hfNotes"), HiddenField).Value
        tempFaultId = DirectCast(row.FindControl("hfTempFaultID"), HiddenField).Value

        pnlFaultNotes.Visible = True
        ucFaultNotes.PopulateControl(location, notes, tempFaultId) 'location & notes
        mdlPopupFaultNotes.Show()

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate Scheme Block Object"
    Private Sub populateSchemeBlockObject() 'ByRef resultDataSet As DataSet
        Dim resultDataSet As New DataSet()
        Dim objSerFaultBl As SearchFaultBL = New SearchFaultBL()

        Dim schemeId As Integer = SessionManager.getSchemeId()
        Dim blockId As Integer = SessionManager.getBlockId()

        objSerFaultBl.getSbRecentFaults(resultDataSet, schemeId, blockId, String.Empty, String.Empty)
        Dim customerObj As CustomerBO = New CustomerBO()

        schemeBlockBo.SchemeName = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("SchemeName").ToString()
        schemeBlockBo.SchemeId = Int32.Parse(resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("SchemeId"))
        schemeBlockBo.BlockName = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("BlockName").ToString()
        schemeBlockBo.BlockId = Int32.Parse(resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("BlockId"))

        'fill object for common item of property,scheme and Block
        commonAddressBo.Street = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Address = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Area = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("County").ToString()
        commonAddressBo.City = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("TownCity").ToString()
        commonAddressBo.PostCode = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("PostCode").ToString()
        commonAddressBo.PatchId = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("PatchId").ToString()
        commonAddressBo.PatchName = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("PatchName").ToString()

        'store dataset in session for further use in intelligent scheduling
        SessionManager.setCommonAddressBO(Me.commonAddressBo)
        SessionManager.setSbData(Me.schemeBlockBo)
    End Sub
#End Region

#Region "Populate Fault Basket"
    Sub populateFaultBasket(ByVal sortExp As String, ByVal sortDir As String)

        Dim confirmedJsn As ArrayList

        If (ViewState(ViewStateConstants.ConfirmedJsnList) Is Nothing) Then
            confirmedJsn = New ArrayList
        Else
            confirmedJsn = DirectCast(ViewState(ViewStateConstants.ConfirmedJsnList), ArrayList)
        End If

        confirmedJsn.Clear()
        ViewState.Add(ViewStateConstants.ConfirmedJsnList, confirmedJsn)

        Dim objFaultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim dsFaultBasket As DataSet = New DataSet()
        Dim basketDataView As New DataView()

        objFaultBO.IsRecall = isRecall

        objFaultBO.SchemeId = SessionManager.getSchemeId()
        If SessionManager.getBlockId() > 0 Then
            objFaultBO.BlockId = SessionManager.getBlockId()
        End If

        objFaultBL.getSbFaultBasket(objFaultBO, dsFaultBasket)
        basketDataView = dsFaultBasket.Tables(0).DefaultView

        If Not String.IsNullOrEmpty(sortExp) Then
            basketDataView.Sort = String.Format("{0} {1}", sortExp, sortDir)
        End If

        If (dsFaultBasket.Tables(0).Rows.Count = 0) Then
            btnScheduleSelectedFaults.Enabled = False
        Else
            btnScheduleSelectedFaults.Enabled = True
        End If

        gdFaultBasket.DataSource = basketDataView
        gdFaultBasket.DataBind()

        checkAllFaultsConfirmed(dsFaultBasket)

    End Sub
#End Region

#Region "Check all faults confirmed"
    Sub checkAllFaultsConfirmed(ByVal dsFaultBasket As DataSet)

        Dim confirmedJsn As ArrayList
        confirmedJsn = DirectCast(ViewState(ViewStateConstants.ConfirmedJsnList), ArrayList)


        ' Check for all appointments confirmed
        If (confirmedJsn.Count = dsFaultBasket.Tables(0).Rows.Count And Not confirmedJsn.Count = 0) Then

            ' Hide AddMoreFault , ScheduleSelectedFaults Buttons & RadioBox
            btnAddMoreFaults.Visible = False
            btnScheduleSelectedFaults.Visible = False
            tradeGroupPanel.Visible = False

            ' Show Success Panel Section with JSN list
            dlSubmittedJsn.DataSource = confirmedJsn
            dlSubmittedJsn.DataBind()
            tblSubmitted.Visible = True
            btnCloseWindow.Visible = True

            ' Hide Header Text
            gdFaultBasket.HeaderRow.Cells(6).Visible = False
            deleteAllConfirmedFaults()

        End If


    End Sub
#End Region

#Region "Populate Customer Info From Session"
    Sub prePopulatedValues()
        source = SessionManager.getPageSource()
        ViewState(ViewStateConstants.SortExpression) = String.Empty
        ViewState(ViewStateConstants.SortDirection) = String.Empty
    End Sub
#End Region

#Region "Redirect to Search screen"
    Private Sub redirectToSearch()
        If isRecall Then
            Response.Redirect(PathConstants.SearchSbFault + GeneralHelper.getSrc(PathConstants.SbFaultBasketSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.SearchSbFault + GeneralHelper.getSrc(PathConstants.SbFaultBasketSrcVal))
        End If
    End Sub
#End Region



#Region "Delete All Confirmed Faults"
    Sub deleteAllConfirmedFaults()
        Dim linkBtn As LinkButton
        Dim tempFaultId As Integer

        ' Delete Faults From Basket
        For Each basketGridRows As GridViewRow In gdFaultBasket.Rows
            linkBtn = basketGridRows.FindControl("imgDelete")
            tempFaultId = Convert.ToInt32(linkBtn.CommandArgument.ToString)
            deleteFaultFromBasket(tempFaultId, 0)
        Next
    End Sub
#End Region

#Region "Delete Fault From Basket"

    Protected Sub deleteFaultFromBasket(ByVal tempFaultId As Integer, ByVal type As Integer)

        Dim objFaultBL As FaultBL = New FaultBL()

        Try
            Dim result As Integer = objFaultBL.deleteFaultFromBasket(tempFaultId)

            If (type = 1) Then

                If result = -1 Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultDoesNotExist, True)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultDeletedSuccessfuly, False)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Delete Fault From Basket"

    Protected Sub deleteFault(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim tempFaultId As Integer = CType(CType(sender, LinkButton).CommandArgument, Int32)
        deleteFaultFromBasket(tempFaultId, 1)
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))


    End Sub
#End Region

#Region "Check if there is any fault slected"
    Function checkAnySelected(ByRef checkedTempFaultList As ArrayList) As Boolean

        Dim chkStatus As CheckBox
        Dim linkBtn As LinkButton
        Dim tempFaultId As Integer
        Dim btnTrade As Button
        Dim tradeId As Integer
        Dim contractorIndex As Integer
        Dim ddlContractor As DropDownList

        For Each basketGridRows As GridViewRow In gdFaultBasket.Rows

            chkStatus = basketGridRows.FindControl("chkGroup")
            If (chkStatus.Checked = True) Then

                linkBtn = basketGridRows.FindControl("imgDelete")
                tempFaultId = Convert.ToInt32(linkBtn.CommandArgument.ToString)

                btnTrade = DirectCast(basketGridRows.FindControl("btnTrade"), Button)
                tradeId = Convert.ToInt32(btnTrade.CommandArgument)

                ddlContractor = DirectCast(basketGridRows.FindControl("ddlContractors"), DropDownList)
                contractorIndex = ddlContractor.SelectedIndex

                Dim checkedTempFaultBO As New TempFaultBO
                checkedTempFaultBO.TempFaultId = tempFaultId
                checkedTempFaultBO.Trade = tradeId
                checkedTempFaultBO.Contractor = contractorIndex
                checkedTempFaultList.Add(checkedTempFaultBO)

            End If
        Next

        If (checkedTempFaultList.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function
#End Region

#Region "Check all selected faults are of the same job type"
    Function checkJobSimilarity(ByVal checkedTempFaultList As ArrayList, ByRef jobType As Integer) As Boolean

        Dim tempFaultBO As TempFaultBO
        Dim currentIndexType As Integer

        'Get first index value as flag
        Dim firstIndexType As Integer = DirectCast(checkedTempFaultList.Item(0), TempFaultBO).Contractor
        If (firstIndexType > 0) Then
            firstIndexType = -1
        Else
            firstIndexType = 0
        End If

        For i As Integer = 0 To checkedTempFaultList.Count - 1
            tempFaultBO = checkedTempFaultList.Item(i)

            If (tempFaultBO.Contractor = 0) Then
                currentIndexType = 0
            Else
                currentIndexType = -1
            End If

            If (Not currentIndexType = firstIndexType) Then
                Return False
            End If
        Next

        If (firstIndexType = 0) Then
            jobType = 0
        Else
            jobType = 1
        End If

        Return True
    End Function
#End Region

#Region "Save Subcontractors in Basket"
    Sub saveSubcontractors()

        Dim chkStatus As CheckBox
        Dim linkBtn As LinkButton
        Dim ddl As DropDownList
        Dim tempFaultId As Integer
        Dim orgId As Integer
        Dim objFaultBL As FaultBL = New FaultBL()

        For Each basketGridRows As GridViewRow In gdFaultBasket.Rows
            chkStatus = basketGridRows.FindControl("chkGroup")
            If (chkStatus.Checked = True) Then

                linkBtn = basketGridRows.FindControl("imgDelete")
                tempFaultId = Convert.ToInt32(linkBtn.CommandArgument.ToString)

                ddl = basketGridRows.FindControl("ddlContractors")
                If (ddl.SelectedIndex > 0) Then
                    ' Get Trade Type of this row
                    SessionManager.removeTradeType()
                    Dim tradeType As String = DirectCast(basketGridRows.FindControl("lblTrade"), Label).Text
                    SessionManager.setTradeType(tradeType)
                    orgId = Convert.ToInt32(ddl.SelectedValue)
                    objFaultBL.updateContractor(tempFaultId, orgId)
                End If
            End If
        Next
    End Sub
#End Region

#Region "Validate duration format"
    Sub validateDurationFormat(ByVal row As GridViewRow)

        Dim btnDuration As Button = DirectCast(row.FindControl("btnDuration"), Button)
        Dim lblDuration As Label = DirectCast(row.FindControl("lblDuration"), Label)

        'Adding 'hours' at end of duration label
        If (btnDuration.CommandArgument = "") Then
            lblDuration.Text = "N/A"
        ElseIf (Convert.ToDouble(btnDuration.CommandArgument.ToString) > 1) Then
            lblDuration.Text = btnDuration.CommandArgument.ToString + " hours"
        Else
            lblDuration.Text = btnDuration.CommandArgument.ToString + " hour"
        End If

    End Sub
#End Region

#Region "Populate Contractors DropdownList"
    Sub populateContractorsDropdownList(ByVal row As GridViewRow)

        ' Get Trade Type of this row
        Dim tradeType As String = DirectCast(row.FindControl("lblTrade"), Label).Text

        ' Get Contractor's list
        Dim resultDataSet As DataSet = New DataSet()
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim ddl As DropDownList = DirectCast(row.FindControl("ddlContractors"), DropDownList)

        objFaultBL.getContractorsByTrade(tradeType, resultDataSet)

        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            ddl.DataSource = resultDataSet.Tables(0).DefaultView
            ddl.DataValueField = "orgid"
            ddl.DataTextField = "name"
            ddl.DataBind()
        End If

        Dim item As ListItem = New ListItem("Please Select", "-1")
        ddl.Items.Insert(0, item)

    End Sub
#End Region

#Region "Manipulate Appointment Confirmed Controls"
    Sub manipulateAppointmentConfirmedControls(ByVal row As GridViewRow, ByVal faultLogId As Integer)

        'Fetch Job Sheet Number & Appointment time from DB
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim dsAppointmentDetail As DataSet = New DataSet()
        Dim jsn As String = String.Empty
        Dim time As String = String.Empty
        Dim confirmedJsn As ArrayList = DirectCast(ViewState(ViewStateConstants.ConfirmedJsnList), ArrayList)

        dsAppointmentDetail = objFaultBL.GetAppointmentDetail(faultLogId)

        If (dsAppointmentDetail.Tables(0).Rows.Count > 0) Then
            jsn = dsAppointmentDetail.Tables("AppointmentDetail").Rows(0)(0).ToString()
            time = dsAppointmentDetail.Tables("AppointmentDetail").Rows(0)(1).ToString()

            'Add to Confirmed List
            confirmedJsn.Add(jsn)

        End If

        ViewState.Add(ViewStateConstants.ConfirmedJsnList, confirmedJsn)


        'Hide check box , DropDownList , Delete Button
        Dim cb_Fault As CheckBox = DirectCast(row.FindControl("chkGroup"), CheckBox)
        Dim ddlContractor As DropDownList = DirectCast(row.FindControl("ddlContractors"), DropDownList)
        Dim lbDelete As LinkButton = DirectCast(row.FindControl("imgDelete"), LinkButton)

        cb_Fault.Visible = False
        ddlContractor.Visible = False
        lbDelete.Visible = False

        'Show Confirmed Button , JSN , Appointment Time
        Dim lblJsn As Label = DirectCast(row.FindControl("lblJsnCompleted"), Label)
        Dim lblTime As Label = DirectCast(row.FindControl("lblAptTime"), Label)

        lblJsn.Text = jsn
        lblTime.Text = time

        Dim tblConfirmed As Table = DirectCast(row.FindControl("tblSuccess"), Table)
        tblConfirmed.Visible = True

        'Highlight Description
        Dim lnkBtnDescription As LinkButton = DirectCast(row.FindControl("lnkBtnDescription"), LinkButton)
        lnkBtnDescription.Font.Bold = True

    End Sub
#End Region


#Region "Populate current Fault BO"
    Private Sub populateCurrentFaultBO(ByVal resultDataSet As DataSet)

        Dim currentFaultBOObj As FaultBO = New FaultBO()

        currentFaultBOObj.SchemeId = SessionManager.getSchemeId()
        currentFaultBOObj.BlockId = SessionManager.getBlockId()
        currentFaultBOObj.FaultID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(12).ToString)
        currentFaultBOObj.Quantity = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(14).ToString)
        currentFaultBOObj.ProblemDays = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(15).ToString)
        currentFaultBOObj.Notes = resultDataSet.Tables("FaultDetail").Rows(0)(11).ToString
        currentFaultBOObj.DueDate = resultDataSet.Tables("FaultDetail").Rows(0)(4).ToString
        currentFaultBOObj.PriorityID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(2).ToString)


        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(16).ToString)) Then
            currentFaultBOObj.RecuringProblem = False
        Else
            currentFaultBOObj.RecuringProblem = Convert.ToBoolean(resultDataSet.Tables("FaultDetail").Rows(0)(16).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(17).ToString)) Then
            currentFaultBOObj.CommunalProblem = False
        Else
            currentFaultBOObj.CommunalProblem = Convert.ToBoolean(resultDataSet.Tables("FaultDetail").Rows(0)(17).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(13).ToString)) Then
            currentFaultBOObj.OrgID = -1
        Else
            currentFaultBOObj.OrgID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(13).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(18).ToString)) Then
            currentFaultBOObj.ItemActionID = -1
        Else
            currentFaultBOObj.ItemActionID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(18).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(19).ToString)) Then
            currentFaultBOObj.Recharge = False
        Else
            currentFaultBOObj.Recharge = Convert.ToBoolean(resultDataSet.Tables("FaultDetail").Rows(0)(19).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(20).ToString)) Then
            currentFaultBOObj.FaultTradeId = -1
        Else
            currentFaultBOObj.FaultTradeId = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(20).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(21).ToString)) Then
            currentFaultBOObj.OriginalFaultLogId = -1
        Else
            currentFaultBOObj.OriginalFaultLogId = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(21).ToString)
        End If

        If (Not IsNothing(Request.QueryString(PathConstants.IsRecall))) Then
            currentFaultBOObj.IsRecall = IIf(Request.QueryString(PathConstants.IsRecall) = PathConstants.Yes, 1, 0)
        Else
            currentFaultBOObj.IsRecall = 0
        End If

        SessionManager.removeSbSubcontractorCurrentFaultBO()
        SessionManager.setSbSubcontractorCurrentFaultBO(currentFaultBOObj)

    End Sub
#End Region

    Public Sub loadParent()
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
        popupAssignToContractor.Hide()
    End Sub

#End Region


End Class
