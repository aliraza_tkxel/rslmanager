﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject
Imports System.IO
Imports System.Net.Mail

Public Class JobSheetSummarySubcontractor
    Inherits PageBase


#Region "Data Members"
    Dim customerObj As CustomerBO = New CustomerBO()
    Dim source As String = String.Empty
#End Region

#Region "Events"

#Region "Page Pre Render"
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.setBackButtonPostBackUrl(btnBack, PathConstants.FaultBasketPath + GeneralHelper.getSrc(PathConstants.JobSheetSummarySubcontractorSrcVal))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            source = SessionManager.getPageSource()

            If Not IsPostBack Then
                If (populateSessionValues()) Then

                    ' Populate customer Information
                    populateCustomerInformation()

                    ' Setting previous next buttons states
                    setPreviousNextButtonStates(ViewState(ViewStateConstants.CurrentIndex))

                    ' Populate Contractor JobSheet 
                    populateContractorJobsheet(ViewState(ViewStateConstants.CurrentIndex))

                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Previous button clicked"
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            'Changes By Aamir Waheed - May 08,2013 - Start
            removeIsEmailSentViewState()
            'Changes By Aamir Waheed - May 08,2013 - End

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)
            currentIndex = currentIndex - 1
            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            SessionManager.setSubcontractorJobSheetIndex(currentIndex)
            populateContractorJobsheet(currentIndex)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Next button clicked"
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim currentIndex As Integer = DirectCast(ViewState(ViewStateConstants.CurrentIndex), Integer)

            'Changes By Aamir Waheed - May 08,2013 - Start
            'currentIndex = currentIndex + 1
            If getIsEmailSentState() Then
                currentIndex = currentIndex + 1
            Else
                removeIsEmailSentViewState()
            End If
            'Changes By Aamir Waheed - May 08,2013 - End

            ViewState(ViewStateConstants.CurrentIndex) = currentIndex
            SessionManager.setSubcontractorJobSheetIndex(currentIndex)
            populateContractorJobsheet(currentIndex)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Confirm selected fault and send email to subcontractor"
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirm.Click

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            Dim objFaultsBl As FaultBL = New FaultBL()
            Dim arrTempFaultBO As ArrayList = DirectCast(SessionManager.getTemporaryFaultBo, ArrayList)
            Dim tempFaultBO As TempFaultBO = arrTempFaultBO.Item(ViewState(ViewStateConstants.CurrentIndex))

            Dim tempFaultId As Integer = tempFaultBO.TempFaultId
            Dim currentFaultBOObj As FaultBO = DirectCast(SessionManager.getSubcontractorCurrentFaultBO, FaultBO)
            Dim currentUserId As Integer = SessionManager.getUserEmployeeId()
            Dim jobSheetNumber As String


            'Save Fault Info in database
            jobSheetNumber = objFaultsBl.saveSubcontractorFaultInfo(currentFaultBOObj, currentUserId, tempFaultId)

            'Email !
            Me.sendEmail(jobSheetNumber)

            ' Remove fault from session move to next one OR redirect to Basket screen
            arrTempFaultBO.RemoveAt(ViewState(ViewStateConstants.CurrentIndex))

            If Not uiMessageHelper.IsError Then

                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AssignedToSubcontractor, False)


                If (arrTempFaultBO.Count = 0) Then
                    'Response.Redirect(PathConstants.FaultBasket)
                    redirectToFaultBasket()
                Else

                    SessionManager.setTemporaryFaultBo(arrTempFaultBO)
                    ViewState(ViewStateConstants.CurrentIndex) = 0
                    SessionManager.setSubcontractorJobSheetIndex(0)
                    populateContractorJobsheet(0)

                    If (Not IsNothing(Request.QueryString(PathConstants.IsRecall))) Then
                        Response.Redirect(PathConstants.JobSheetSummarySubcontractor & "?" & PathConstants.IsRecall & "=" & Request.QueryString(PathConstants.IsRecall))
                    Else
                        Response.Redirect(PathConstants.JobSheetSummarySubcontractor)
                    End If

                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions "

#Region "Populate Email body"
    Private Function populateBody(ByVal jobSheetNumber As String) As String
        Dim body As String = String.Empty
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/EmailTemplate.htm"))
        body = reader.ReadToEnd.ToString
        body = body.Replace("{JSN}", jobSheetNumber)
        body = body.Replace("{ContractorName}", lblContractor.Text)
        body = body.Replace("{Priority}", lblFaultPriority.Text)
        body = body.Replace("{Completion}", lblFaultCompletionDateTime.Text)
        body = body.Replace("{Location}", lblFaultLocation.Text)
        body = body.Replace("{Description}", lblFaultDescription.Text)
        body = body.Replace("{Notes}", txtBoxFaultOperatorNote.Text)
        body = body.Replace("{TenantName}", lblClientName.Text)
        body = body.Replace("{Street}", lblClientStreetAddress.Text)
        body = body.Replace("{City}", lblClientCity.Text)
        body = body.Replace("{Postalcode}", lblClientPostCode.Text)
        body = body.Replace("{Tel}", lblClientTelPhoneNumber.Text)
        body = body.Replace("{Mobile}", lblClientMobileNumber.Text)
        body = body.Replace("{Email}", lblClientEmailId.Text)
        Return body
    End Function
#End Region

#Region "Send Html Formatted Email"
    'This function has been transfered to Email Helper.
    'Private Sub sendHtmlFormattedEmail(ByVal recepientEmail As String, ByVal subject As String, ByVal body As String)

    '    Dim mailMessage As MailMessage = New MailMessage
    '    mailMessage.Subject = subject
    '    mailMessage.Body = body
    '    mailMessage.IsBodyHtml = True
    '    mailMessage.To.Add(New MailAddress(recepientEmail))

    '    Dim smtp As SmtpClient = New SmtpClient

    '    ' These setting has been placed in Web.Config - Start
    '    'mailMessage.From = New MailAddress("")
    '    'smtp.Host = "smtp.gmail.com" 'ConfigurationManager.AppSettings("Host")
    '    'smtp.EnableSsl = True 'Convert.ToBoolean(ConfigurationManager.AppSettings("EnableSsl"))
    '    'Dim NetworkCred As System.Net.NetworkCredential = New System.Net.NetworkCredential
    '    'NetworkCred.UserName = "" 'ConfigurationManager.AppSettings("UserName")
    '    'NetworkCred.Password = "" 'ConfigurationManager.AppSettings("Password")
    '    'smtp.UseDefaultCredentials = True
    '    'smtp.Credentials = NetworkCred
    '    'smtp.Port = 587 'Integer.Parse(ConfigurationManager.AppSettings("Port"))
    '    ' These setting has been placed in Web.Config - End

    '    smtp.Send(mailMessage)

    'End Sub
#End Region

#Region "Send Email"
    Private Sub sendEmail(ByVal jobSheetNumber As String)
        Dim body As String = Me.populateBody(jobSheetNumber)
        Dim recepientName As String = lblContractor.Text
        Dim subject As String = "URGENT: BHG Fault Reported - Ref:" + jobSheetNumber

        ' Get Email from View state to send email to sub contractor
        Dim recepientEmail As String = getEmailViewState()

        Try
            If Validation.isEmail(recepientEmail) Then
                EmailHelper.sendHtmlFormattedEmail(recepientName, recepientEmail, subject, body)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultSavedinvalidEmail, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                setIsEmailSentViewState(False)
                btnConfirm.Enabled = False
            End If
        End Try


        ' TO DO (WHEN EMAIL FUNCTIONALITY IS WORKING) 
        'Me.sendHtmlFormattedEmail("adsbdsfasdfh!@#$23@to.com", "URGENT: BHG Fault Reported - Ref:" + jobSheetNumber, body)
    End Sub
#End Region

#Region "Populate contractor jobsheet"
    Private Sub populateContractorJobsheet(ByVal currentIndex As Integer)

        ' Get array from Viewstate
        Dim arrTempFaultBO As ArrayList = DirectCast(SessionManager.getTemporaryFaultBo, ArrayList)
        ' Get current temp fault BO 
        Dim tempFaultBO As TempFaultBO = arrTempFaultBO.Item(currentIndex)
        Dim propertyId As String = ViewState(ViewStateConstants.PropertyId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objFaultsBl As FaultBL = New FaultBL()

        objFaultsBl.getSubcontractorJobSheetDetails(resultDataSet, tempFaultBO, propertyId)


        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAsbestos.Visible = False
        Else

            ' Populating visible fields

            grdAsbestos.Visible = True
            grdAsbestos.DataSource = resultDataSet.Tables("Asbestos")
            grdAsbestos.DataBind()

            lblContractor.Text = resultDataSet.Tables("FaultDetail").Rows(0)(1).ToString
            lblOperativeName.Text = "-"
            lblFaultPriority.Text = resultDataSet.Tables("FaultDetail").Rows(0)(3).ToString
            lblFaultCompletionDateTime.Text = resultDataSet.Tables("FaultDetail").Rows(0)(4).ToString
            lblOrderDate.Text = resultDataSet.Tables("FaultDetail").Rows(0)(5).ToString
            lblFaultLocation.Text = resultDataSet.Tables("FaultDetail").Rows(0)(6).ToString
            lblFaultDescription.Text = resultDataSet.Tables("FaultDetail").Rows(0)(7).ToString
            lblTrade.Text = resultDataSet.Tables("FaultDetail").Rows(0)(8).ToString
            txtBoxFaultOperatorNote.Text = resultDataSet.Tables("FaultDetail").Rows(0)(11).ToString

            ' Populate Current FaultBO
            populateCurrentFaultBO(resultDataSet)

            ' Setting previous next buttons states
            setPreviousNextButtonStates(currentIndex)

            ' Set Email in View state to send email to sub contractor
            setEmailViewState(resultDataSet.Tables("FaultDetail").Rows(0)("Email").ToString)

            btnConfirm.Enabled = True

        End If

    End Sub
#End Region

#Region "Populate current Fault BO"
    Private Sub populateCurrentFaultBO(ByVal resultDataSet As DataSet)

        Dim currentFaultBOObj As FaultBO = New FaultBO()

        currentFaultBOObj.CustomerId = SessionManager.getCustomerId()
        currentFaultBOObj.PropertyId = SessionManager.getPropertyId()
        currentFaultBOObj.FaultID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(12).ToString)
        currentFaultBOObj.Quantity = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(14).ToString)
        currentFaultBOObj.ProblemDays = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(15).ToString)
        currentFaultBOObj.Notes = resultDataSet.Tables("FaultDetail").Rows(0)(11).ToString
        currentFaultBOObj.DueDate = resultDataSet.Tables("FaultDetail").Rows(0)(4).ToString
        currentFaultBOObj.PriorityID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(2).ToString)


        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(16).ToString)) Then
            currentFaultBOObj.RecuringProblem = False
        Else
            currentFaultBOObj.RecuringProblem = Convert.ToBoolean(resultDataSet.Tables("FaultDetail").Rows(0)(16).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(17).ToString)) Then
            currentFaultBOObj.CommunalProblem = False
        Else
            currentFaultBOObj.CommunalProblem = Convert.ToBoolean(resultDataSet.Tables("FaultDetail").Rows(0)(17).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(13).ToString)) Then
            currentFaultBOObj.OrgID = -1
        Else
            currentFaultBOObj.OrgID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(13).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(18).ToString)) Then
            currentFaultBOObj.ItemActionID = -1
        Else
            currentFaultBOObj.ItemActionID = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(18).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(19).ToString)) Then
            currentFaultBOObj.Recharge = False
        Else
            currentFaultBOObj.Recharge = Convert.ToBoolean(resultDataSet.Tables("FaultDetail").Rows(0)(19).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(20).ToString)) Then
            currentFaultBOObj.FaultTradeId = -1
        Else
            currentFaultBOObj.FaultTradeId = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(20).ToString)
        End If

        If (String.IsNullOrEmpty(resultDataSet.Tables("FaultDetail").Rows(0)(21).ToString)) Then
            currentFaultBOObj.OriginalFaultLogId = -1
        Else
            currentFaultBOObj.OriginalFaultLogId = Convert.ToInt32(resultDataSet.Tables("FaultDetail").Rows(0)(21).ToString)
        End If

        If (Not IsNothing(Request.QueryString(PathConstants.IsRecall))) Then
            currentFaultBOObj.IsRecall = IIf(Request.QueryString(PathConstants.IsRecall) = PathConstants.Yes, 1, 0)
        Else
            currentFaultBOObj.IsRecall = 0
        End If

        SessionManager.removeSubcontractorCurrentFaultBO()
        SessionManager.setSubcontractorCurrentFaultBO(currentFaultBOObj)

    End Sub
#End Region

#Region "Populate Session Value"
    Private Function populateSessionValues() As Boolean
        If (validateSessionValues()) Then
            ViewState.Add(ViewStateConstants.CustomerId, SessionManager.getCustomerId())
            ViewState.Add(ViewStateConstants.PropertyId, SessionManager.getPropertyId())
            customerObj = SessionManager.getCustomerData()
            ViewState.Add(ViewStateConstants.CurrentIndex, 0)
            SessionManager.setSubcontractorJobSheetIndex(0)
            Return True
        End If

        Return False
    End Function
#End Region

#Region "Validate sessions values"
    Private Function validateSessionValues() As Boolean

        If (SessionManager.getCustomerId = Nothing) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidCustomerId
            Return False
        End If

        If (SessionManager.getPropertyId = Nothing) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidPropertyId
            Return False
        End If

        If (DirectCast(SessionManager.getTemporaryFaultBo(), ArrayList).Count = 0) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.NoTempFaultData
            Return False
        End If

        Return True

    End Function
#End Region

#Region "Populate customer information"
    Private Sub populateCustomerInformation()

        ' Header Customer Labels
        lblClientNameHeader.Text = customerObj.Name
        lblClientStreetAddressHeader.Text = customerObj.Street
        lblClientCityHeader.Text = customerObj.City
        lblClientTelPhoneNumberHeader.Text = customerObj.Telephone

        ' Body customer Labels

        lblClientName.Text = customerObj.Name
        lblClientStreetAddress.Text = customerObj.Street
        lblClientCity.Text = customerObj.City
        lblClientPostCode.Text = customerObj.PostCode
        lblClientRegion.Text = customerObj.Area
        lblClientTelPhoneNumber.Text = customerObj.Telephone
        lblClientMobileNumber.Text = customerObj.Mobile
        lblClientEmailId.Text = customerObj.Email

    End Sub
#End Region

#Region "Set previous next button states"
    Private Sub setPreviousNextButtonStates(ByVal updatedIndex As Integer)

        Dim arrTempFaultBO As ArrayList = DirectCast(SessionManager.getTemporaryFaultBo, ArrayList)
        'Previous Button state

        If (updatedIndex - 1 < 0) Then
            btnPrevious.Enabled = False
        Else
            btnPrevious.Enabled = True
        End If

        'Next Button state
        If (updatedIndex + 1 > arrTempFaultBO.Count - 1) Then
            btnNext.Enabled = False
        Else
            btnNext.Enabled = True
        End If

    End Sub
#End Region

#Region "Redirect to Fault Basket"

    Private Sub redirectToFaultBasket()
        If (Not IsNothing(Request.QueryString(PathConstants.IsRecall))) Then
            Response.Redirect(PathConstants.FaultBasket + GeneralHelper.getSrc(PathConstants.JobSheetSummarySubcontractorSrcVal) + "&" + PathConstants.IsRecall & "=" & Request.QueryString(PathConstants.IsRecall))
        Else
            Response.Redirect(PathConstants.FaultBasket + GeneralHelper.getSrc(PathConstants.JobSheetSummarySubcontractorSrcVal))
        End If
    End Sub
#End Region

#End Region

#Region "View State Get and Set Method/Function(s)" 'Changes By Aamir Waheed - May 08,2013 - Start

#Region "Email View State Get, Set and Remove Method/Function(s)"

    Protected Sub setEmailViewState(ByVal Email As String)
        ViewState(ViewStateConstants.Email) = Email
    End Sub

    Protected Function getEmailViewState() As String
        If ViewState(ViewStateConstants.Email) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.Email), String)
        Else
            Return String.Empty
        End If
    End Function

    Protected Sub removeEmailViewState()
        ViewState.Remove(ViewStateConstants.Email)
    End Sub

#End Region

#Region "Is Email Sent View State Get, Set and Remove Method/Function(s)"

    Protected Sub setIsEmailSentViewState(ByVal EmailSend As Boolean)
        ViewState(ViewStateConstants.IsEmailSent) = EmailSend
    End Sub

    Protected Function getIsEmailSentState() As Boolean
        If ViewState(ViewStateConstants.IsEmailSent) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.IsEmailSent), Boolean)
        Else
            Return True
        End If
    End Function

    Protected Sub removeIsEmailSentViewState()
        ViewState.Remove(ViewStateConstants.IsEmailSent)
    End Sub

#End Region

    'Changes By Aamir Waheed - May 08,2013 - End
#End Region

End Class