﻿Imports System
Imports FLS_BusinessObject
Imports FLS_BusinessLogic
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class MoreDetails
    Inherits PageBase

    Dim faultLogId As Integer
    Dim faultId As Integer
    Dim source As String = String.Empty
    Dim jobSheetNumber As String = String.Empty
    Dim isRecall As Boolean = False


#Region "Events"

#Region "Page Pre Render"
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            SetValuesFromSession()

            If Me.isRecall = True Then
                Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.RecallDetails + GeneralHelper.getSrc(PathConstants.MoreDetailsSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes + "&jsn=" + jobSheetNumber)
            Else
                Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.MoreDetailsSrcVal))
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)            

            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)                
            End If

            GetValuesFromSession()

            If Not IsPostBack Then
                PopulateValuesInControls()
            Else
                ' GetValuesFromSession()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Add To Fault Basket Click"
    ''' <summary>
    ''' This function handles the add to fault basket event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddToFaultBasket_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddToFaultBasket.Click
        Try
            If validateFaultInfo() Then
                saveFaultInfo()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "rbtn Fault Rechargable Checked Changed"
    ''' <summary>
    ''' This function handles the check change event of fault recharge radio button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rbtnFaultRechargable_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Validate Fault Info"
    Function validateFaultInfo()
        Dim isValid As Boolean = True
        If ddlLocation.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue) Then
            isValid = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectLocation, True)
        Else
            If txtFaultOperatorNote.Text.Length >= 500 Then
                isValid = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoteLength, True)
            End If
        End If
        Return isValid
    End Function
#End Region

#Region "Save Fault More Details"
    Sub saveFaultInfo()

        Dim objFaultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultBL = New FaultBL()
        objFaultBO.FaultID = faultId
        objFaultBO.CustomerId = SessionManager.getCustomerId()
        objFaultBO.PropertyId = SessionManager.getPropertyId()
        objFaultBO.Quantity = 1
        objFaultBO.ProblemDays = CInt(ddlFaultDays.SelectedItem.Value)
        objFaultBO.IsRecall = isRecall
        objFaultBO.IsAppointmentConfirmed = False
        objFaultBO.OriginalFaultLogId = SessionManager.getOriginalFaultLogId()
        objFaultBO.IsFollowOn = SessionManager.getIsFollowOn()
        objFaultBO.FollowOnFaultLogId = SessionManager.getFollowOnFaultLogId()
        objFaultBO.LocationID = Convert.ToInt32(ddlLocation.SelectedValue)

        If (rbtnRecurreingProblemYes.Checked) Then
            objFaultBO.RecuringProblem = 1
        Else
            objFaultBO.RecuringProblem = 0
        End If

        If (lblRechargeStatus.Equals("Yes")) Then
            objFaultBO.Recharge = 1
        Else
            objFaultBO.Recharge = 0
        End If

        objFaultBO.Notes = txtFaultOperatorNote.Text

        Dim tmpFaultId As Integer = CInt(objFaultBL.saveFaultInfo(objFaultBO))

        If (tmpFaultId > 0) Then
            If isRecall Then
                Response.Redirect(PathConstants.FaultBasketPath + GeneralHelper.getSrc(PathConstants.MoreDetailsSrcVal) + "&" & PathConstants.IsRecall + "=" + PathConstants.Yes, True)
            Else
                Response.Redirect(PathConstants.FaultBasketPath + GeneralHelper.getSrc(PathConstants.MoreDetailsSrcVal), True)
            End If
        Else
            If tmpFaultId = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorNoTradeExist, True)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorUserInsert, True)
            End If
        End If
    End Sub

    ''' <summary>
    ''' This functions gets the values from session and sets the local variables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub GetValuesFromSession()
        source = SessionManager.getPageSource()
        faultId = SessionManager.getFaultId()
        jobSheetNumber = SessionManager.getJobSheetNumber()
    End Sub

    ''' <summary>
    ''' This method sets the values in session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SetValuesFromSession()
        SessionManager.setPageSource(source)
        SessionManager.setJobSheetNumber(jobSheetNumber)
    End Sub
#End Region

#Region "Pre Populate Values"
    Sub PopulateValuesInControls()
        faultId = SessionManager.getFaultId()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objRecallDetailsBl As FaultBL = New FaultBL()
        objRecallDetailsBl.getMoreDetails(resultDataSet, faultId)
        Dim dtMoreDetailInfo As DataTable = resultDataSet.Tables("MoreDetails")

        If (dtMoreDetailInfo.Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            populateLocationDropdown(resultDataSet)

            lblFaultDescription.Text = dtMoreDetailInfo.Rows(0)("Description").ToString
            lblRecommendedTime.Text = IIf(IsDBNull(dtMoreDetailInfo.Rows(0)("ResponseTime")), "NA", dtMoreDetailInfo.Rows(0)("ResponseTime").ToString)
            lblRechargableAmount.Text = IIf(IsDBNull(dtMoreDetailInfo.Rows(0)("Gross")), "NA", "&#163;" & dtMoreDetailInfo.Rows(0)("Gross").ToString)
            lblRechargeStatus.Text = IIf(CType(dtMoreDetailInfo.Rows(0)("Recharge"), Boolean), "Yes", "No")
            lblPriorityName.Text = IIf(IsDBNull(dtMoreDetailInfo.Rows(0)("PriorityName")), "NA", dtMoreDetailInfo.Rows(0)("PriorityName").ToString)
        End If

    End Sub

#End Region

#Region "Populate Location Dropdown"
    Sub populateLocationDropdown(ByVal resultDataSet As DataSet)

        Dim dtLocation As DataTable = resultDataSet.Tables("Locations")
        ddlLocation.Items.Clear()
        ddlLocation.DataSource = dtLocation
        ddlLocation.DataTextField = "area"
        ddlLocation.DataValueField = "areaId"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))

        If dtLocation.Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorNoLocationExist, True)
            btnAddToFaultBasket.Enabled = False
        Else
            btnAddToFaultBasket.Enabled = True
        End If

    End Sub

#End Region

#End Region







End Class