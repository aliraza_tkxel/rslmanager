﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="CancellationAppointmentSummaryForGas.aspx.vb" Inherits="FaultScheduling.CancellationAppointmentSummaryForGas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../Controls/Common/CustomerDetail.ascx" TagName="CustomerDetails"
    TagPrefix="custom" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 311px;
        }
    </style>
    <script type="text/javascript">
        function closeWindow() {
            var browserName = navigator.appName;
            var browserVer = parseInt(navigator.appVersion);

            if (browserName == "Microsoft Internet Explorer") {
                var ie7 = (document.all && !window.opera && window.XMLHttpRequest) ? true : false;
                if (ie7) {
                    //This method is required to close a window without any prompt for IE7 & greater versions.
                    window.open('', '_parent', '');
                    window.close();
                }
                else {
                    //This method is required to close a window without any prompt for IE6
                    this.focus();
                    self.opener = this;
                    self.close();
                }
            } else {
                //For NON-IE Browsers except Firefox which doesnt support Auto Close
                try {
                    this.focus();
                    self.opener = this;
                    self.close();
                }
                catch (e) {

                }

                try {
                    window.open('', '_self', '');
                    window.close();
                }
                catch (e) {

                }
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="fl_width_100_text_a_l">
        <asp:UpdatePanel ID="updPanelFaults" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <div class="fl_width_100_text_a_l">
                    <div style="margin: 0 0 0 -8px">
                        <asp:Panel ID="pnlAddress" runat="server">
                            <!--<asp:Label ID="lblAppointmentText" runat="server" Font-Bold="True" 
                            Font-Size="Smaller" Text="Appointment summary for : "></asp:Label>-->
                            <custom:CustomerDetails ID="custDetails" runat="server" StartString="Appointment summary for : " />
                        </asp:Panel>
                    </div>
                    <div style="height: 10px;">
                    </div>
                    <hr />
                    <div style="height: 10px;">
                    </div>
                    <div style="height: 50px; overflow-y: auto; border: 2px solid #ADADAD;">
                        <asp:GridView ID="grdAppointments" runat="server" AutoGenerateColumns="False" Width="100%"
                            CssClass="gridfaults" GridLines="None" DataKeyNames="AppointmentId" ShowHeader="False">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JobSheetNumber") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="50px" />
                                </asp:TemplateField>
<%--                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                        >
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>' Font-Bold="True"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDurationText" runat="server" Text="Duration:"></asp:Label>
                                        <asp:Label ID="lblDuration" runat="server" Text="1"></asp:Label>
                                        <asp:Label ID="lblHoursText" runat="server" Text="Hrs"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("TradeName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDue" runat="server" Text="Due:"></asp:Label>
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%# Bind("DueDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="fl_width_100_text_a_l bottom_line">
        <div class="fl_heading">
            Appointment:
        </div>
        <div class="fl_width_100_text_a_l">
            <asp:UpdatePanel ID="updPanelFaultViewer" runat="server">
                <ContentTemplate>
                    <table id="operativeSummary">
                        <tr>
                            <td>
                                Operative
                            </td>
                            <td>
                                <asp:Label ID="lblOperative" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Duration
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date
                            </td>
                            <td>
                                <asp:Label ID="lblStartTime" runat="server"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                End Date
                            </td>
                            <td>
                                <asp:Label ID="lblEndTime" runat="server"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Notes
                            </td>
                            <td>
                                <asp:Label ID="lblFaultNotes" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <%--<div id="faultviewer" class="roundcorner" style="padding-left: 20px; padding-top: 10px;">
                        <div>
                            <asp:UpdatePanel ID="updPanelFaultNumber" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="lblFaultLabel" runat="server">Fault </asp:Label>
                                    <asp:Label ID="lblJSN" runat="server" Font-Bold="False"></asp:Label>
                                    :
                                    <asp:LinkButton ID="lnkBtnNextFault" runat="server" ForeColor="Black">Next Fault &gt;</asp:LinkButton>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div style="padding-top: 5px;">
                            <asp:UpdatePanel ID="updPanelFaultInfo" runat="server">
                                <ContentTemplate>
                                    <asp:Label ID="lblProblemDays" runat="server"></asp:Label><br />
                                    <asp:Label ID="lblReccuringProblem" runat="server"></asp:Label><br />
                                    <asp:Label ID="lblCommunalProblem" runat="server"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="fl_width_100_text_a_l bottom_line">
        <div id="customerheading">
            <b>Customer :</b>
        </div>
        <table id="customerSummary">
            <tr>
                <td>
                    <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                </td>
                <td>
                    <div class="fl_width_100_text_a_l">
                        <div class="fl_width_20pc_text_a_l">
                            Telephone:</div>
                        <div class="fl_width_80pc_text_a_l">
                            <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label></div>
                        <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCustomerStreet" runat="server"></asp:Label>
                </td>
                <td>
                    <div class="fl_width_100_text_a_l">
                        <div class="fl_width_20pc_text_a_l">
                            Mobile:</div>
                        <div class="fl_width_80pc_text_a_l">
                        </div>
                        <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                        <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCustomerAddress" runat="server"></asp:Label>
                </td>
                <td>
                    <div class="fl_width_100_text_a_l">
                        <div class="fl_width_20pc_text_a_l">
                            Email:</div>
                        <div class="fl_width_80pc_text_a_l">
                            <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label></div>
                        <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblCustomerArea" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div class="text_a_r_padd__r_20px">
            <asp:Button ID="btnUpdateCustomerDetail" runat="server" Text="Update Customer Detail" />
        </div>
    </div>
    <div class="fl_width_100_text_a_l">
        <table id="AccessNote">
            <tr>
                <td style="width: 20%">
                    Access Note:
                </td>
                <td style="width: 80%">
                    <asp:Label ID="lblAccessNote" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
        <div style="height: 20px;">
        </div>
        <div>
            <div style="float: left;">
                <asp:Button ID="btnBack" runat="server" Text="&lt; Back" />
            </div>
            <div style="float: left; width:100%; text-align: right;">
                <asp:Button ID="btnCancelReportedFaults" runat="server" Text="Cancel Reported Fault" />
                <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdateClick" />
            </div>
            <div style="clear: left;">
            </div>
        </div>
    </div>
    <!-----------------------------------------------------Cancel Fault Popup-------------------------------------------------->
    <asp:Panel ID="pnlCancelFaults" CssClass="left" runat="server" BackColor="White"
        Width="390px" Height="270px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;
        ">
        <div style="width: 100%; font-weight: bold; padding-left: 10px;">
            Cancel</div>
        <div style="clear: both; height: 1px;">
        </div>
        <hr />
        <asp:ImageButton ID="imgBtnCloseCancelFaults" runat="server" Style="position: absolute;
            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        <div style="width: 100%; height: 490px; overflow: hidden;">
            <div style="height: 370px; padding-top: 20px !important; padding-left: 20px;">
                <table>
                    <tr>
                        <td align="left" class="style1">
                            <asp:Panel ID="pnlErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblErrorMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Panel ID="pnlDescription" runat="server">
                                <asp:Label ID="lblDescription" runat="server" Text="Enter the reason for cancellation below:"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:TextBox ID="txtBoxDescription" runat="server" Height="87px" Width="325px" TextMode="MultiLine"></asp:TextBox>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Panel ID="pnlCancelMessage" runat="server" Visible="False" HorizontalAlign="Center">
                                <asp:Label ID="lblCancelFaultsText" runat="server" Text="The appointment has been cancelled."
                                    Font-Bold="True"></asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="pnlSaveButton" runat="server" HorizontalAlign="Right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style1">
                            <asp:Panel ID="pnlCustomerMessage" runat="server" HorizontalAlign="Center" Visible="False">
                                <asp:LinkButton ID="lnkBtnClickHere" runat="server" Visible="True" OnClientClick="closeWindow()">Click here</asp:LinkButton>
                                &nbsp;<asp:Label ID="lblCustomerText" runat="server" Text="to return to the customers record"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdlPopUpCancelFaults" runat="server" DynamicServicePath=""
        Enabled="True" TargetControlID="lblDispCancelFaultsPopup" PopupControlID="pnlCancelFaults"
        DropShadow="true" CancelControlID="imgBtnCloseCancelFaults" BackgroundCssClass="modalBackground"
        BehaviorID="mdlPopUpCancelFaults">
    </asp:ModalPopupExtender>
    <asp:Label ID="lblDispCancelFaultsPopup" runat="server"></asp:Label>
    <!-----------------------------------------------------End Add Add Appointment for Fault------------------------------------------>
     <!----------------------------------------------------- Update Fault Notes Popup-------------------------------------------------->
               
                        <asp:Panel ID="pnlFaultNotes" CssClass="left" runat="server" BackColor="White"
                            Width="380px" Style=" padding: 15px 5px; top: 32px; border: 1px solid black;
         ">
                            <div style=" font-weight: bold; padding-left: 10px;">
                                <b>Update Notes</b>
                            </div>
                           
                            <asp:Panel ID="pnlFaultNotesErrorMessage" runat="server" Visible="false">
                                <asp:Label ID="lblFaultNotesErrorMessage" style=" margin:5px; color:Red; font-style:oblique;" runat="server">
                                </asp:Label>
                            </asp:Panel>
                            <div style="clear: both; height: 1px;">
                            </div>
                            <hr />
                            <asp:ImageButton ID="CancelledBack" runat="server" Style="position: absolute;
            top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" OnClick="btnFaultBack" />
                                <br />
                                <br />
                                <table style="width: 100%;">
                                    
                                    <tr>
                                        <td style=" margin-left:10px;">
                                            Notes:
                                        </td>
                                        <td>
                                             <asp:TextBox ID="txtFaultNotes" Height="70px" Width="93%" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                   
                                </table>
                                <br />
                                <br />
                                <asp:Panel ID="pnlRejectButtons" runat="server" HorizontalAlign="Right">
                                 
                                    <asp:Button ID="btnSaveFaultNotes"  BorderStyle="Solid" Style=" margin-right:10px; font-size:small; padding:3px; padding-left:15px; padding-right:15px;" 
                                    runat="server" Text="Save" OnClick="btnFaultSave" />
                                </asp:Panel>
                        </asp:Panel>
                        <asp:ModalPopupExtender ID="mdlPopupFaultNotes" runat="server" DynamicServicePath=""
                            Enabled="True" TargetControlID="lblFaultNotesPopup" PopupControlID="pnlFaultNotes"
                            DropShadow="true"  BackgroundCssClass="modalBackground">
                        </asp:ModalPopupExtender>
                        <asp:Label ID="lblFaultNotesPopup" runat="server"></asp:Label>
                    
                <!---------------------------------------------------------------------------------------------------------------------->
          

</asp:Content>
