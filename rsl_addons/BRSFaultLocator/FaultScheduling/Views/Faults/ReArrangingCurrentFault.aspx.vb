﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject
Imports System.Collections
Imports FLS_BusinessLogic
Imports System.Globalization
Imports System.Net
Imports System.IO
Imports System.Net.Mail

Public Class ReArrangingCurrentFault
    Inherits PageBase

#Region "Data Members"

    Dim dsOperativeData As New DataSet()
    Dim mOperativesDt As DataTable
    Dim mOperativeAbsentDt As DataTable
    Dim mOperativeAppointmentsDt As DataTable
    Dim hashTable As Hashtable = New Hashtable()
    Dim jsn As String = String.Empty
    Dim isReArrange As Boolean = False
    Dim firstTimeDisplay As Boolean = True
    Dim preCalculatedSlotsDt As New DataTable()
    Dim calculatesingleoperativecount As Integer = 0
    Dim isNoEntry As Boolean = False

    Dim operativeDs As DataSet = New DataSet()
    Dim propertyId As String = String.Empty
    Dim customerBo As CustomerBO = New CustomerBO()
    Dim appointmentBo As AppointmentBO = New AppointmentBO()
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' This event handles the page load event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Not (SessionManager.getLoggedInUserType().Equals(ApplicationConstants.LoggedInUserTypes.Manager.ToString())) Then
            '    btnViewCalender.Visible = False
            'End If
            getValuesFromQueryString()

            If Not IsPostBack Then
                'Remove the existing appointments from the session, to show new pins of map.
                removeMarkersJavaScript()
                SessionManager.removeExistingAppointmentsForRearranggeAppointmentsMap()

                If uiMessageHelper.IsError = False Then
                    If jsn <> String.Empty Then
                        fetchConfirmedFaultsBasedOnJsnAndPopulate(jsn)
                    End If
                    getValueFromSession()
                    'if operative list is going to display first time after page load
                    firstTimeDisplay = True
                    'if operative list is going to display first time after page load then clear the session
                    'if it has previous calculated slots
                    SessionManager.removePreCalculatedSlots()
                    'Get Data from Session and populates the fault grid
                    fetchDataAndPopulateFaultGrid()
                    'polulateOperativeGrid(True)
                    'Handle button events
                    Me.getAvailableOperatives()
                    Me.populateTempFaultsAndAppointments()
                Else
                    'pnlControls.Enabled = False
                    Me.disableRefreshButton()
                End If
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'pnlControls.Enabled = False
                Me.disableRefreshButton()
            End If
        End Try
    End Sub
#End Region

#Region "Page Pre Render"
    ''' <summary>
    ''' This event handles the render event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            setValueInSession()
            If (Not IsNothing(Request.QueryString(PathConstants.RepairsDashboard))) AndAlso (Request.QueryString(PathConstants.RepairsDashboard) = "ne") Then
                Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.RearrangeAppointmentSrcVal) + "&apt=" + getAppointmentId().ToString() + "&" + PathConstants.RepairsDashboard + "=ne")
            Else
                Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.RearrangeAppointmentSrcVal) + "&apt=" + getAppointmentId().ToString())
            End If

            saveExistingAppointmentForVisibleOperatives()
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'pnlControls.Enabled = False
                Me.disableRefreshButton()
                Me.disableSelectButton()
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                btnRefresh.Enabled = True
                
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn Refresh Click"
    ''' <summary>
    ''' This event handles the refresh button event.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefresh.Click
        Try
            'if operative list is going to display first time after page load then set it true
            'otherwise false
            'firstTimeDisplay = False
            'Refresh Logic
            Me.getValueFromSession()
            Me.refreshTheOpertivesList(1)
            'polulateOperativeGrid(False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'pnlControls.Enabled = False
                Me.disableRefreshButton()
            End If

        End Try

    End Sub
#End Region

#Region "btn View Calender Click"
    ''' <summary>
    ''' This event handles the click event of the View Calender button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnViewCalender_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewCalender.Click
        Try
            'Redirect to Calender Page
            Dim lRetDt As New DataTable
            If saveFaultInTempTable(CType(SessionManager.getConfirmedFaults(), DataTable), lRetDt) Then
                If SessionManager.getIsAppointmentRearrange() = True Then
                    SessionManager.setIsAppointmentRearrange(True)
                Else
                    'If appointment id 'll be set to zero the user 'll be able to create the appointment 
                    ' on calendar
                    SessionManager.setAppointmentId(0)
                    SessionManager.setIsAppointmentRearrange(False)
                End If

                'set temporary faults dv so calendar screen 'll use this.
                SessionManager.setSelectedTempFaultsDv(lRetDt.DefaultView)
                Me.redirectToCalendar()
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.InvalidFaultData
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'pnlControls.Enabled = False
                Me.disableRefreshButton()
            End If
        End Try
    End Sub
#End Region

#Region "btn Select Click"
    ''' <summary>
    ''' This event handles the click button of the grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSelect_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'In case of rearrange ---------
            '1.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Appointment Arranged’ 
            '2.) INSERT INTO FL_FAULT_LOG_HISTORY
            '3.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Appointment Arranged’
            '4.) UPDATE FL_CO_APPOINTMENT
            '5.) INSERT INTO FL_CO_APPOINTMENT_HISTORY
            '6.) UPDATE FL_FAULT_NOENTRY 'In case of no entry(rearrange).
            'In case of rearrange ---------

            setAppointmentBo(sender.CommandArgument.ToString)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                pnlControls.Enabled = False
            End If
        End Try
    End Sub
#End Region

#Region "Check Box Patch checked change event"

    ''' <summary>
    ''' This event handles the Patch checked change event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkPatch_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkPatch.CheckedChanged
        Try
            Me.getValueFromSession()
            operativeDs = SessionManager.getAvailableOperativesDs()

            Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()

            'If selectedFaultAptBlockId > 0 Then
            tempFaultAptList = SessionManager.getTempFaultAptBlockList()
            ' End If

            'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
            'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
            ' Dim index As Integer = selectedFaultAptBlockId - 1
            tempFaultAptBo = tempFaultAptList(0)

            tempFaultAptBo.IsPatchSelected = chkPatch.Checked

            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date

            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
            'Before calling the addMoreAppointment check for valid operatives dataset.
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)
            End If

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment            
            faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)

            'save in temporary fault operative list
            tempFaultAptList(0) = tempFaultAptBo

            Dim FaultDt As DataTable = tempFaultAptList(0).tempAppointmentDtBo.dt
            grdOperative.DataSource = FaultDt
            grdOperative.DataBind()

            'Call the update markers for any refresh in appointment slots.
            If Page.IsPostBack Then
                updateMarkersJavaScript()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub

#End Region

#Region "Check Box Due Date change event"

    ''' <summary>
    '''   This event handles the Due Date checked change event
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Protected Sub chkDueDate_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkDueDate.CheckedChanged
        Try


            Me.getValueFromSession()
            operativeDs = SessionManager.getAvailableOperativesDs()

            Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()


            tempFaultAptList = SessionManager.getTempFaultAptBlockList()
            If tempFaultAptList.Count > 0 Then
                tempFaultAptBo = tempFaultAptList(0)
            End If
            If chkDueDate.Checked Then
                'selectedFaultAptBlockId = 1
                tempFaultAptBo.IsDateSelected = True
            Else
                tempFaultAptBo.IsDateSelected = False
            End If
            'tempFaultAptBo.IsDateSelected = chkDueDate.Checked

            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()

            'Before calling the addMoreAppointment check for valid operatives dataset.
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)
            End If

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment             
            faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)
            'save in temporary fault operative list
            tempFaultAptList.Add(tempFaultAptBo)
            Dim FaultDt As DataTable = tempFaultAptList(0).tempAppointmentDtBo.dt
            grdOperative.DataSource = FaultDt
            grdOperative.DataBind()

            'Call the update markers for any refresh in appointment slots.
            If Page.IsPostBack Then
                updateMarkersJavaScript()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub

#End Region

#Region "Button Go Back Event"

    Protected Sub btnGoBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoBack.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            ViewState.Item(ViewStateConstants.PopupSlideIndex) = 0
            lnkBtnNextPopupFault.Enabled = True
            lnkBtnNextPopupFault.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Done Image Button (Green Mark) Click Event"

    Protected Sub DoneButton_Clicked(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim imgBtnDone = DirectCast(sender, ImageButton)
            Dim faultLogId As Integer = Integer.Parse(imgBtnDone.CommandArgument)

            Dim confirmedFaultDt As DataTable = SessionManager.getConfirmedFaults()

            If Not IsNothing(confirmedFaultDt) Then
                confirmedFaultDt.Columns("Duration").ReadOnly = False
                Dim datarows() As DataRow = confirmedFaultDt.Select("FaultLogId = " & faultLogId.ToString)
                Dim duration As Decimal = DirectCast(imgBtnDone.NamingContainer, FaultDuration).Duration


                If datarows(0)("Duration") <> duration Then
                    datarows(0)("Duration") = duration

                    ' save the changed values in session
                    SessionManager.setConfirmedFaults(confirmedFaultDt)

                    'bind modified faults datatable to grid.
                    grdFaults.DataSource = confirmedFaultDt
                    grdFaults.DataBind()

                    'if duration is changed let the page behave like first time
                    firstTimeDisplay = True
                    'and remove the previously calculated appoitment slots.
                    SessionManager.removePreCalculatedSlots()

                    'get existing values from session
                    getValueFromSession()

                    'get Operative Dataset from session.
                    operativeDs = SessionManager.getAvailableOperativesDs()

                    ' populate the appintment slots according to new data.
                    Me.populateTempFaultsAndAppointments()

                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region


#Region "Check Box Patch checked change event"

    ''' <summary>
    ''' This event handles the Patch checked change event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtfromDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try

            If Not isValidDate(txtFromDate.Text) Then
                'If patch is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForAvailableAppointmentsMap()
                'remove pins from map.
                removeMarkersJavaScript()
                Exit Sub
            End If
            Me.getValueFromSession()
            operativeDs = SessionManager.getAvailableOperativesDs()

            Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()

            'If selectedFaultAptBlockId > 0 Then
            tempFaultAptList = SessionManager.getTempFaultAptBlockList()
            ' End If

            'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
            'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
            ' Dim index As Integer = selectedFaultAptBlockId - 1
            tempFaultAptBo = tempFaultAptList(0)

            tempFaultAptBo.FromDate = txtFromDate.Text

            If tempFaultAptBo.FromDate > tempFaultAptBo.CompleteDueDate AndAlso tempFaultAptBo.IsDateSelected = True Then
                uiMessageHelper.IsError = True
                uiMessageHelper.message = String.Format(UserMessageConstants.UncheckDueDate)
                Exit Sub
            Else
                tempFaultAptBo.IsFromDateSelected = True
            End If
            tempFaultAptBo.tempAppointmentDtBo.dt.Clear()

            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date

            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
            'Before calling the addMoreAppointment check for valid operatives dataset.
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)
            End If

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment            
            faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)

            'save in temporary fault operative list
            tempFaultAptList(0) = tempFaultAptBo

            Dim FaultDt As DataTable = tempFaultAptList(0).tempAppointmentDtBo.dt
            grdOperative.DataSource = FaultDt
            grdOperative.DataBind()

            'Call the update markers for any refresh in appointment slots.
            If Page.IsPostBack Then
                updateMarkersJavaScript()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub

#End Region
#End Region

#Region "Private Methods"

#Region "Redirect To Calendar"
    Private Sub redirectToCalendar()
        'If isRecall Then
        '    Response.Redirect(PathConstants.ScheduleCalender + GeneralHelper.getSrc(PathConstants.RearrangeAppointmentSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes + "&" + PathConstants.openPopup + "=" + PathConstants.Yes)
        'Else
        '    Response.Redirect(PathConstants.ScheduleCalender + GeneralHelper.getSrc(PathConstants.RearrangeAppointmentSrcVal) + "&" + PathConstants.openPopup + "=" + PathConstants.Yes)
        'End If
        Response.Redirect(PathConstants.ScheduleCalender + GeneralHelper.getSrc(PathConstants.RearrangeAppointmentSrcVal), True)
    End Sub
#End Region

#Region "disable Refresh Button"
    Private Sub disableRefreshButton()
        Me.btnRefresh.Enabled = False
    End Sub
#End Region

#Region "disable Select Button"
    Private Sub disableSelectButton()
        For Each grow As GridViewRow In grdOperative.Rows
            Dim btn As Button = DirectCast(grow.FindControl("btnSelect"), Button)
            btn.Enabled = False
        Next


    End Sub
#End Region

#Region "fetch Confirmed Faults Based On Jsn And Populate"
    Private Sub fetchConfirmedFaultsBasedOnJsnAndPopulate(ByVal jsn As String)
        Dim lFaultObj As FaultBL = New FaultBL()
        Dim resultDataSet As DataSet = New DataSet()

        lFaultObj.getConfirmedFaultsByJSN(resultDataSet, jsn)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            SessionManager.setConfirmedFaults(resultDataSet.Tables(0))
        End If
    End Sub
#End Region

#Region "update Appointment "
    ''' <summary>
    ''' This method updates the appointment.
    ''' </summary>
    ''' <param name="appointmentBo"></param>
    ''' <param name="faultList"></param>    
    ''' <remarks></remarks>
    Private Sub updateAppointment(ByVal appointmentBo As AppointmentBO, ByVal faultList As String)
        Dim lFaultBL As New FaultBL
        Dim lresult As Integer = lFaultBL.updateFaultInfoRearrange(appointmentBo, faultList)

        If lresult = -1 Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidFaultData
        Else
            'Save the appointmentId in session
            SessionManager.setAppointmentId(lresult)
        End If

    End Sub
#End Region

#Region "insert In Fault Log"
    ''' <summary>
    ''' This function inserts the data in fault log table
    ''' </summary>
    ''' <param name="operativeId"></param>
    ''' <param name="faultList"></param>
    ''' <param name="appointmentDate"></param>
    ''' <param name="startTime"></param>
    ''' <param name="endTime"></param>
    ''' <remarks></remarks>
    Private Sub insertInFaultLog(ByVal operativeId As Integer, ByVal faultList As String, ByVal appointmentDate As Date, ByVal startTime As String, ByVal endTime As String, ByVal isCalendarAppointment As Boolean, ByVal appointmentEndDate As Date, ByVal areaName As String)
        Dim lCalenderBL As New CalendarBL
        Dim lAppointmentBO As New AppointmentSummaryBO
        Dim lPropertyId As String = SessionManager.getPropertyId()
        Dim lCustomerId As String = SessionManager.getCustomerId()
        Dim lResultDs As New DataSet
        Dim lAppointmentId As Integer = 0

        'Populate Object
        lAppointmentBO.OperativeId = operativeId
        lAppointmentBO.FaultsList = faultList
        lAppointmentBO.AppointmentStartDate = appointmentDate
        lAppointmentBO.FaultNotes = String.Empty
        lAppointmentBO.Time = startTime
        lAppointmentBO.EndTime = endTime
        lAppointmentBO.PropertyId = lPropertyId
        lAppointmentBO.CustomerId = lCustomerId
        lAppointmentBO.IsRecall = False
        lAppointmentBO.IsCalendarAppointment = isCalendarAppointment
        lAppointmentBO.IsNoEntry = SessionManager.getIsNoEntry()
        lAppointmentBO.NoEntryFaultLogId = SessionManager.getNoEntryFaultLogId()
        lAppointmentBO.UserId = SessionManager.getUserEmployeeId()
        lAppointmentBO.AppointmentEndDate = appointmentEndDate
        lAppointmentBO.AreaName = areaName

        lAppointmentId = lCalenderBL.saveAppointmentInformation(lAppointmentBO)

        'Fetch newly created appointment data and save in session
        Dim lFaultAppointmentBL As New FaultAppointmentBL
        lFaultAppointmentBL.getNewlyCreatedAppointment(lResultDs, lPropertyId, lCustomerId, lAppointmentId)

        SessionManager.setConfirmedFaults(lResultDs.Tables(ApplicationConstants.ConfirmFaultDataTable))
        SessionManager.setAppointmentId(lAppointmentId)
    End Sub
#End Region

#Region "save Fault In Temp Table"
    ''' <summary>
    ''' This function saves the value in temp fault table
    ''' </summary>
    ''' <param name="pFaultDt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Private Function saveFaultInTempTable(ByVal pFaultDt As DataTable, ByRef lRetDt As DataTable, Optional ByRef pTempFaultIdsList As String = "") As Boolean
        Dim objFaultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim lResult As Boolean = False
        Dim lTempFaultIdsToDelete As String = String.Empty

        'In below code , we are updating the values in datatable so for this 
        'we need to update its column properties for successful updation & 
        'to avoid any error
        pFaultDt.Columns("tempFaultId").ReadOnly = False
        ' pFaultDt.Columns("tempFaultId").MaxLength = 2147483647

        For Each dr As DataRow In pFaultDt.Rows
            objFaultBO.CustomerId = SessionManager.getCustomerId()
            objFaultBO.PropertyId = SessionManager.getPropertyId()
            objFaultBO.IsRecall = False
            objFaultBO.IsAppointmentConfirmed = False

            If IsDBNull(dr("FaultID")) Then
                objFaultBO.FaultID = 0
            Else
                objFaultBO.FaultID = CInt(dr("FaultID"))
            End If

            If IsDBNull(dr("ProblemDays")) Then
                objFaultBO.ProblemDays = 0
            Else
                objFaultBO.ProblemDays = CInt(dr("ProblemDays"))
            End If

            If IsDBNull(dr("RecuringProblem")) Then
                objFaultBO.RecuringProblem = False
            Else
                objFaultBO.RecuringProblem = CType(dr("RecuringProblem"), Boolean)
            End If

            If IsDBNull(dr("Notes")) Then
                objFaultBO.Notes = False
            Else
                objFaultBO.Notes = dr("Notes").ToString
            End If

            If IsDBNull(dr("CommunalProblem")) Then
                objFaultBO.CommunalProblem = False
            Else
                objFaultBO.CommunalProblem = CType(dr("CommunalProblem"), Boolean)
            End If

            If IsDBNull(dr("FaultLogID")) Then
                objFaultBO.FaultLogId = 0
            Else
                objFaultBO.FaultLogId = CInt(dr("FaultLogID"))
            End If

            If IsDBNull(dr("Quantity")) Then
                objFaultBO.Quantity = 0
            Else
                objFaultBO.Quantity = CInt(dr("Quantity"))
            End If

            If IsDBNull(dr("Recharge")) Then
                objFaultBO.Recharge = False
            Else
                objFaultBO.Recharge = CType(dr("Recharge"), Boolean)
            End If

            If IsDBNull(dr("FaultTradeId")) Then
                objFaultBO.FaultTradeId = 0
            Else
                objFaultBO.FaultTradeId = CType(dr("FaultTradeId"), Integer)
            End If

            '--ItemActionId,itemstatusid -- missing
            If SessionManager.getIsAppointmentRearrange() = True Then
                objFaultBO.IsRearrange = True
            End If

            Dim tmpFaultId As Integer = CInt(objFaultBL.saveFaultInfoRearrange(objFaultBO))
            If tmpFaultId > 0 Then
                lResult = True
                lTempFaultIdsToDelete = lTempFaultIdsToDelete & tmpFaultId.ToString() & ","

                dr("tempFaultId") = tmpFaultId
            Else
                lResult = False

                If lTempFaultIdsToDelete <> String.Empty Then
                    lTempFaultIdsToDelete = lTempFaultIdsToDelete.Remove(lTempFaultIdsToDelete.Length - 1, 1)
                    If (objFaultBL.deleteAllFault(lTempFaultIdsToDelete) > 0) Then
                        'do nothing
                    Else
                        uiMessageHelper.IsError = True
                        uiMessageHelper.message = UserMessageConstants.DeleteAllFault
                    End If
                End If

                Exit For
            End If
        Next

        If lResult Then
            lRetDt = pFaultDt.Copy()

            lTempFaultIdsToDelete = lTempFaultIdsToDelete.Remove(lTempFaultIdsToDelete.Length - 1, 1)
            pTempFaultIdsList = lTempFaultIdsToDelete
        End If

        Return lResult
    End Function
#End Region

#Region "get Values From Query String"
    ''' <summary>
    ''' This function gets the values from query string
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getValuesFromQueryString()
        If Not IsNothing(Request.QueryString(PathConstants.Jsn)) _
            AndAlso Request.QueryString(PathConstants.isNoEnty) = PathConstants.Yes _
            AndAlso Request.QueryString(PathConstants.Jsn).ToString() = "JS" + SessionManager.getNoEntryFaultLogId().ToString() Then

            jsn = Request.QueryString(PathConstants.Jsn)
            isNoEntry = IIf(Request.QueryString(PathConstants.RepairsDashboard) = "ne", True, False)
            SessionManager.setIsNoEntry(True)
            SessionManager.setIsAppointmentRearrange(False)

        ElseIf Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
            'SessionManager.setJsnIds(Request.QueryString("jsnList"))
            jsn = Request.QueryString(PathConstants.Jsn)
            isReArrange = True

            SessionManager.setIsAppointmentRearrange(True)
        End If
    End Sub
#End Region

#Region "get Value From Session"
    ''' <summary>
    ''' This function gets the values from session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getValueFromSession()
        jsn = SessionManager.getJsnId()
        hashTable = SessionManager.getHashTable()
        isReArrange = SessionManager.getIsAppointmentRearrange()
        Me.customerBo = SessionManager.getCustomerData()
        Me.propertyId = SessionManager.getPropertyId()

        Me.commonAddressBo = SessionManager.getCommonAddressBO()
        Dim faultsDataTable As DataTable = New DataTable()
        faultsDataTable = SessionManager.getConfirmedFaults()

        'validate the session values
        If IsNothing(faultsDataTable) And String.IsNullOrEmpty(Me.propertyId) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidGroupByPropFaults, True)
        End If

    End Sub
#End Region

#Region "set Value In Session"
    ''' <summary>
    ''' This function sets the values in session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setValueInSession()
        SessionManager.setJsnId(jsn)
        SessionManager.setHashTable(hashTable)
    End Sub
#End Region

#Region "fetch Data And Populate Fault Grid"
    ''' <summary>
    ''' This method fetches the fault grid data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub fetchDataAndPopulateFaultGrid()
        Try
            ' Dim TempFaultIdDv As DataView = CType(SessionManager.getSelectedTempFaultsDv(), DataView)
            Dim TempFaultIdDt As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)

            grdFaults.DataSource = TempFaultIdDt
            grdFaults.DataBind()

            lblDefaultPatch.Text = Me.customerBo.PatchName

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            End If

        End Try
    End Sub
#End Region

#Region "fetch Operative Data"
    ''' <summary>
    ''' This function fetches the operative Data
    ''' </summary>
    ''' <param name="pfaultLogId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function fetchOperativeData(ByVal pfaultLogId As String, ByVal pPropertyId As String) As DataSet
        Dim objFaultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
        Dim lDsOperative As New DataSet()

        objFaultAppointmentBl.getAvailableOperativesForRearrange(lDsOperative, pfaultLogId, pPropertyId)
        SessionManager.setAvailableOperativesDsForRearrange(lDsOperative)
        If (lDsOperative.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        Return lDsOperative
    End Function
#End Region

#Region "get Operatives"

    Private Function getOperatives(ByVal pDsOperativeData As DataSet, ByVal pFaultsDuration As Integer, ByVal pCurrentTime As Date) As DataTable
        Dim dtOperative As DataTable
        Dim operativeName As String = String.Empty
        Dim operativeId As String = String.Empty
        Dim completeOperativeInfo As String = String.Empty
        Dim appointmentInfoSeparater As String = ":::"
        Dim selectedTempFaultIds As String = String.Empty
        Dim timeString As String = String.Empty
        Dim dateString As String = String.Empty

        Dim currentDate As Date = Date.Now.AddDays(1)
        Dim operativeCount As Integer = 0

        dtOperative = getOperativeDt()

        For Each operativeDr In pDsOperativeData.Tables(0).Rows
            Dim faultDuration As Double
            'Dim currentTime As Date = Convert.ToDateTime("9:00")
            Dim nextTime As New Date
            Dim hour As Double = 9
            faultDuration = pFaultsDuration

            Dim lDrOperative As DataRow = dtOperative.NewRow()

            While (hour < 18) And (hour + faultDuration < 18)
                hour = hour + faultDuration
                nextTime = pCurrentTime.AddHours(faultDuration)

                operativeCount = operativeCount + 1

                operativeId = Convert.ToString(operativeDr.Item("EmployeeId"))
                operativeName = Convert.ToString(operativeDr.Item("FullName"))
                timeString = pCurrentTime.ToString("hh") + ":" + pCurrentTime.ToString("mm") + " " + pCurrentTime.ToString("tt") + " - " + nextTime.ToString("hh") _
                             + ":" + nextTime.ToString("mm") + " " + nextTime.ToString("tt")

                dateString = currentDate.ToShortDateString()
                completeOperativeInfo = operativeId + appointmentInfoSeparater + operativeName + appointmentInfoSeparater _
                                        + timeString + appointmentInfoSeparater + dateString + appointmentInfoSeparater _
                                        + appointmentInfoSeparater + selectedTempFaultIds

                'lDrOperative("Jsn") = "test"
                lDrOperative("operativeId") = operativeId
                lDrOperative("operativeName") = operativeName
                lDrOperative("timeString") = timeString
                lDrOperative("dateString") = dateString

                dtOperative.Rows.Add(lDrOperative.ItemArray)

                If operativeCount >= 5 Then
                    Exit For
                End If
                pCurrentTime = nextTime
            End While
        Next

        Return dtOperative
    End Function
#End Region

#End Region

#Region "Recursive Call"

#Region "calculate All Operative"
    ''' <summary>
    ''' This is the main function that fetches the operatives data from the database and calcuates the operative appointment slots
    ''' </summary>
    ''' <param name="pFirstTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculateAllOperative(ByVal pFirstTime As Boolean) As DataTable
        Dim i As Integer = 0
        Dim dtOperative As DataTable
        'the day start at 8:00
        Dim lStartTime As Date = Convert.ToDateTime("8:00").AddDays(1)
        Dim lOperativeIndex As Integer = 0
        Dim lOperativeCount As Integer = 0
        Dim lDuration As Double = 0
        Dim lFaultLogId As String = String.Empty
        'Convert.ToDateTime("8:00")

        If pFirstTime Then
            'Remove the hardcode value
            'dsOperativeData = FetchOperativeData("3,6", "A720040007") '--for testing

            lFaultLogId = calculateFaultLogList()
            'Delete the following
            'lFaultLogId = "a"
            '------------------
            If lFaultLogId <> String.Empty Then
                dsOperativeData = fetchOperativeData(lFaultLogId, SessionManager.getPropertyId())

                If dsOperativeData.Tables.Count > 0 Then
                    mOperativesDt = dsOperativeData.Tables(0)
                    mOperativeAbsentDt = dsOperativeData.Tables(1)
                    mOperativeAppointmentsDt = dsOperativeData.Tables(2)
                    mOperativeAbsentDt = Me.fillLeavesDt(mOperativeAbsentDt)

                    Dim TempFaultIdDt As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)
                    'Filter the operatives based on the fault data in session
                    mOperativesDt = filterOperatives(mOperativesDt, TempFaultIdDt)

                    lOperativeCount = mOperativesDt.Rows.Count
                    lDuration = calculateDuration()

                    If lOperativeCount = 0 Then
                        uiMessageHelper.IsError = True
                        uiMessageHelper.message = UserMessageConstants.OperativeNotFound
                    End If

                    'If the total fault time exceed from 8-5 then the algorithm will not stop. So, show error.
                    If lDuration > 9 Then
                        uiMessageHelper.IsError = True
                        uiMessageHelper.message = UserMessageConstants.DurationExceeded
                    End If
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message = UserMessageConstants.InvalidFaultData
                End If
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.InvalidFaultData
            End If
        Else
            'Fetch From hashtable
            mOperativesDt = hashTable("OperativeDt")
            mOperativeAbsentDt = hashTable("OperativeAbsentDt")
            mOperativeAppointmentsDt = hashTable("OperativeAppointmentDt")
            lStartTime = hashTable("StartTime")
            'lOperativeIndex = hashTable("OperativeIndex")            
            lOperativeIndex = 0
            lDuration = hashTable("Duration")
            lOperativeCount = hashTable("OperativeCount")
        End If


        If uiMessageHelper.IsError = False Then

            'Dim lOperativeCount As Integer = mOperativesDt.Rows.Count
            'Dim lDuration As Integer = CalculateDuration()

            'Create empty datatable
            dtOperative = getOperativeDt()

            'Run the loop 5 times to fetch 5 operative data
            While (i < 5)
                'Create a new Datarow so that the inner function can populate it
                Dim lDrOperative As DataRow = dtOperative.NewRow()
                calculatesingleoperativecount = 0
                'Add the operative datarow in the datatable
                dtOperative.Rows.Add(calculateSingleOperative(lDrOperative, lOperativeIndex, lStartTime, lDuration, lOperativeCount, False).ItemArray)

                i = i + 1
            End While

            'saves the values in hashtable
            saveValuesInHash(lStartTime, lOperativeIndex, lDuration, lOperativeCount)

            'save pre calculated slots in session

            Return dtOperative
        Else
            Throw New Exception(uiMessageHelper.message)
        End If

    End Function
#End Region

#Region "filter Operatives "
    ''' <summary>
    ''' This function filters the operatives based on the fault types
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function filterOperatives(ByVal pOperativeDt As DataTable, ByVal pFaultDt As DataTable) As DataTable

        If Not (pFaultDt Is Nothing) Then

            Dim lFaultDt As DataTable = pFaultDt
            Dim lIsOftec As Boolean = False
            Dim lIsGasSafe As Boolean = False
            Dim lDv As DataView = New DataView(pOperativeDt)

            For Each dr As DataRow In lFaultDt.Rows
                If Not IsDBNull(dr("IsOftec")) Then
                    If (CType(dr("IsOftec"), Integer) = 1) Then
                        lIsOftec = True
                    End If
                End If

                If Not IsDBNull(dr("IsGasSafe")) Then
                    If (CType(dr("IsGasSafe"), Integer) = 1) Then
                        lIsGasSafe = True
                    End If
                End If
            Next

            If lIsGasSafe And lIsOftec Then
                'Use operatives with both skills
                'pOperativeDt.Select("IsGasSafe = 1 And IsOftec = 1")
                lDv.RowFilter = "IsGasSafe = 1 And IsOftec = 1"
            ElseIf lIsGasSafe = True And lIsOftec = False Then
                'Use gas safe operatives
                lDv.RowFilter = "IsGasSafe = 1"
            ElseIf lIsGasSafe = False And lIsOftec = True Then
                'Use oftec Opeatives
                lDv.RowFilter = "IsOftec = 1"
            Else
                'Use both
                'no need to filter
            End If

            Return lDv.ToTable()
        Else
            'show error
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidFaultData, True)
            uiMessageHelper.IsError = True
            Return Nothing
        End If
    End Function
#End Region

#Region "save Values In Hash"
    ''' <summary>
    ''' This method saves the values in hashtable
    ''' </summary>
    ''' <param name="pStartTime"></param>
    ''' <param name="pOperativeIndex"></param>
    ''' <remarks></remarks>
    Private Sub saveValuesInHash(ByVal pStartTime As Date, ByVal pOperativeIndex As Integer, ByVal pDuration As Double, ByVal pOperativeCount As Integer)
        hashTable.Clear()

        hashTable.Add("OperativeDt", mOperativesDt)
        hashTable.Add("OperativeAbsentDt", mOperativeAbsentDt)
        hashTable.Add("OperativeAppointmentDt", mOperativeAppointmentsDt)
        hashTable.Add("StartTime", pStartTime)
        hashTable.Add("OperativeIndex", pOperativeIndex)
        hashTable.Add("OperativeCount", pOperativeCount)
        hashTable.Add("Duration", pDuration)
    End Sub
#End Region

#Region "calculate Single Operative"
    ''' <summary>
    ''' This method will recursively calculate the operative
    ''' </summary>
    ''' <param name="pOperativeDr"></param>
    ''' <param name="pOperativeIndex"></param>
    ''' <param name="pDuration"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculateSingleOperative(ByVal pOperativeDr As DataRow, ByRef pOperativeIndex As Integer, ByRef pStartTime As Date, ByVal pDuration As Double, ByVal pOperativeCount As Integer, ByVal goNextOperative As Boolean) As DataRow
        calculatesingleoperativecount = calculatesingleoperativecount + 1
        Dim lEndTime As New Date

        'Add the Duration to Start Time
        lEndTime = calculateDateTime(pStartTime, pDuration)

        If Me.firstTimeDisplay = False Then
            'This function returns a datatable that has previously displayed operatives and their timeslots
            preCalculatedSlotsDt = SessionManager.getPreCalculatedSlots()
            pStartTime = Me.getStartEndTime(pOperativeDr, pOperativeIndex, pOperativeCount, pStartTime, lEndTime, pDuration, goNextOperative)
        Else
            If IsNothing(SessionManager.getPreCalculatedSlots()) Then
                preCalculatedSlotsDt = Me.createPreCalculatedSlotsDt()
            Else
                'This function returns a datatable that has previously displayed operatives and their timeslots
                preCalculatedSlotsDt = SessionManager.getPreCalculatedSlots()
                pStartTime = Me.getStartEndTime(pOperativeDr, pOperativeIndex, pOperativeCount, pStartTime, lEndTime, pDuration, goNextOperative)
            End If
        End If

        'Verify the Time Slot Calculated 
        If verifyTimeSlot(lEndTime) Then
            'Verify slot availablity
            'Verify Absents
            'Verify Appointments 
            If (verifyAppointment(pStartTime, lEndTime, pOperativeIndex)) Then
                'Check the user absents
                If (verifyAbsents(pStartTime, lEndTime, pOperativeIndex)) Then
                    'We have reached a base case
                    'Save this appointment and return

                    'Start - Changes by Aamir Waheed
                    'Dim lAppointmentTime As String = pStartTime.ToString("hh") + ":" + pStartTime.ToString("mm") + " " + pStartTime.ToString("tt") + " - " + lEndTime.ToString("hh") _
                    '        + ":" + lEndTime.ToString("mm") + " " + lEndTime.ToString("tt")

                    Dim lAppointmentTime As String = pStartTime.ToString("HH:mm") + " - " + lEndTime.ToString("HH:mm")

                    Dim lAppointmentDate As String = pStartTime.ToString(ApplicationConstants.CustomLongDateFormat, CultureInfo.CreateSpecificCulture("en-US"))

                    pOperativeDr("operativeId") = mOperativesDt.Rows(pOperativeIndex)("EmployeeId")
                    pOperativeDr("operativeName") = mOperativesDt.Rows(pOperativeIndex)("FullName")
                    pOperativeDr("timeString") = lAppointmentTime
                    pOperativeDr("dateString") = lAppointmentDate
                    pOperativeDr("CompleteData") = mOperativesDt.Rows(pOperativeIndex)("EmployeeId").ToString + "|" _
                        + mOperativesDt.Rows(pOperativeIndex)("FullName").ToString + "|" + lAppointmentTime + "|" + lAppointmentDate

                    'add this slot in previous calculated slots data table
                    addRowInPreCalculatedSlots(mOperativesDt.Rows(pOperativeIndex)("EmployeeId"), pStartTime, lEndTime)

                    pStartTime = lEndTime

                    Return pOperativeDr

                Else
                    'Do recursive call
                    'Since the time slot is not available because the operative is absent in this slot.
                    ''''''''move on to next slot                                        
                    goNextOperative = False
                    calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, goNextOperative)
                    ''''''''move on to next slot

                    'If (pOperativeIndex < pOperativeCount - 1) Then
                    '    'Do recursive call
                    '    'Since the time slot is not available because the operative is absent.
                    '    'Try to check the next slot with different operative and with time slot that starts with 9:00 on the same day
                    '    pOperativeIndex = pOperativeIndex + 1
                    '    'added                        
                    '    'pStartTime = Convert.ToDateTime(pStartTime.ToShortDateString().ToString()).AddHours(8)                        
                    '    addHours = True
                    '    addDay = False
                    '    swapStartEndTime = False
                    '    calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, addHours, addDay, swapStartEndTime)
                    'Else
                    '    pOperativeIndex = 0
                    '    'pStartTime = Convert.ToDateTime(pStartTime.AddDays(1).ToShortDateString().ToString()).AddHours(8)
                    '    addHours = True
                    '    addDay = True
                    '    swapStartEndTime = False
                    '    calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, addHours, addDay, swapStartEndTime)
                    'End If
                End If
            Else
                'Do recursive call
                'Since the time slot is not available because the operative has another appointment.
                ''''''''move on to next slot                
                goNextOperative = False
                calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, goNextOperative)
                ''''''''move on to next slot

                'Do recursive call
                'Since the time slot is not available because the operative has another appointment.
                'Try to check the next slot in the same day
                'added
                'pStartTime = lEndTime
                'addHours = False
                'addDay = False
                'swapStartEndTime = True

            End If

        Else
            ''Recursively call the same function
            'Since the time slot has approached the last slot of the working hours so go to next day
            ''''''''move on to next day            
            goNextOperative = True
            calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, goNextOperative)
            ''''''''move on to next day

            ''Recursively call the same function
            'If (pOperativeIndex < pOperativeCount - 1) Then
            '    'if new operative index is less then the total count then do recursive call based on the new operative
            '    'This means that another operative exist and we can look for his appointments in the today's date

            '    'Make the Start time to start from 9:00
            '    'added                
            '    pOperativeIndex = pOperativeIndex + 1
            '    'pStartTime = Convert.ToDateTime(pStartTime.ToShortDateString().ToString()).AddHours(8)
            '    addHours = True
            '    addDay = False
            '    swapStartEndTime = False
            '    calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, addHours, addDay, swapStartEndTime)
            'Else
            '    'Do the calculations with the same operative but with next date

            '    'Add 1 day to the start time and make the time to start from 9:00
            '    'added
            '    'Dim lRefreshedDate As Date = Convert.ToDateTime(pStartTime.AddDays(1).ToShortDateString().ToString()).AddHours(9)
            '    pOperativeIndex = 0
            '    ' pStartTime = Convert.ToDateTime(pStartTime.AddDays(1).ToShortDateString().ToString()).AddHours(8)
            '    'add this slot in previous calculated slots data table
            '    'addRowInPreCalculatedSlots(mOperativesDt.Rows(pOperativeIndex)("EmployeeId"), pStartTime, lEndTime)
            '    addHours = True
            '    addDay = True
            '    swapStartEndTime = False
            '    calculateSingleOperative(pOperativeDr, pOperativeIndex, pStartTime, pDuration, pOperativeCount, addHours, addDay, swapStartEndTime)
            'End If

        End If

        Return pOperativeDr
    End Function
#End Region

#Region "calculate Date Time"
    ''' <summary>
    ''' This method adds the duration to the time
    ''' </summary>
    ''' <param name="pTime"></param>
    ''' <param name="pDuration"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculateDateTime(ByVal pTime As Date, ByVal pDuration As Double) As Date
        Return pTime.AddHours(pDuration)
    End Function
#End Region

#Region "verify Time Slot"
    ''' <summary>
    ''' This function verifies that the slot is avaiable with in 9-5 (12- Hour format) or 9-17 (24- Hour format)
    ''' </summary>
    ''' <param name="pEndTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function verifyTimeSlot(ByVal pEndTime As Date) As Boolean
        'Operative can only work between 8-5
        'First verify that time doesnot exceed the 9-5 slot

        If (pEndTime.Hour > 17 And pEndTime.Hour < 8) Then
            Return False
        Else
            If (pEndTime.Hour = 17 And pEndTime.Minute = 0) Then
                Return True
            ElseIf (pEndTime.Hour <= 17 And pEndTime.Hour >= 8) Then
                Return True
            Else
                Return False
            End If
        End If
    End Function
#End Region

#Region "verify Absents"
    ''' <summary>
    ''' This function verifies that the appointment time that has been selected does not coincides with the 
    ''' engineers absents and if yes then save a row in pre calculated slots. After verfication if appointment 
    ''' time matches with input time slot then save this slot in pre calculated data structure
    ''' </summary>
    ''' <param name="pStartTime"></param>
    ''' <param name="pEndTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function verifyAbsents(ByVal pStartTime As Date, ByVal pEndTime As Date, ByVal pOperativeIndex As Integer) As Boolean
        If mOperativeAbsentDt.Rows.Count = 0 Then
            'Since, no absent exist for the operative we donot need to do anything.
            Return True
        Else
            Dim lDateRange As ArrayList = New ArrayList()
            Dim lTimeSlot As Tuple(Of Date, Date) = New Tuple(Of Date, Date)(pStartTime, pEndTime)

            'Add the time slot that we calculated on the first place
            lDateRange.Add(lTimeSlot)

            'Get the operative against which we have to verify the appointment
            Dim lOperativeId As Integer = mOperativesDt.Rows(pOperativeIndex)("EmployeeId")

            'Add all the other appointments in the Tuple
            For Each lDr As DataRow In mOperativeAbsentDt.Rows
                If CType(lDr("employeeid"), Integer) = lOperativeId Then
                    lDateRange.Add(New Tuple(Of Date, Date)(lDr("StartDate"), lDr("ReturnDate")))
                End If
            Next

            Dim overLap As Boolean = False
            overLap = (Not checkOverlap(lDateRange))

            If (overLap) = False Then
                'After verfication if absent leaves(time) matches with input time slot then save this slot in pre calculated data structure
                addRowInPreCalculatedSlots(lOperativeId, lDateRange(0).Item1, lDateRange(0).Item2)
            End If

            Return overLap
        End If
    End Function
#End Region

#Region "verify Appointment"
    ''' <summary>
    ''' This method verifies that the time of appointment does not coincides with the user appointments
    ''' After verfication if appointment time matches with input time slot then save this slot in pre calculated data structure
    ''' </summary>
    ''' <param name="pStartTime"></param>
    ''' <param name="pEndTime"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function verifyAppointment(ByRef pStartTime As Date, ByVal pEndTime As Date, ByVal pOperativeIndex As Integer) As Boolean
        Dim lDateRange As ArrayList = New ArrayList()
        Dim lTimeSlot As Tuple(Of Date, Date) = New Tuple(Of Date, Date)(pStartTime, pEndTime)

        'Add the time slot that we calculated on the first place
        lDateRange.Add(lTimeSlot)

        'Get the operative against which we have to verify the appointment
        Dim lOperativeId As Integer = mOperativesDt.Rows(pOperativeIndex)("EmployeeId")

        'Add all the other appointments in the Tuple
        For Each lDr As DataRow In mOperativeAppointmentsDt.Rows
            If CType(lDr("OperativeId"), Integer) = lOperativeId Then
                lDateRange.Add(New Tuple(Of Date, Date)(lDr("StartTime"), lDr("EndTime")))
            End If
        Next

        Dim overLap As Boolean = False
        overLap = (Not checkOverlap(lDateRange))

        If mOperativeAppointmentsDt.Rows.Count = 0 Then
            'After verfication if appointment time matches with input time slot then save this slot in pre calculated data structure
            addRowInPreCalculatedSlots(lOperativeId, lDateRange(0).Item1, lDateRange(0).Item2)
            Return True
        Else
            If (overLap) = False Then
                'After verfication if appointment time matches with input time slot then save this slot in pre calculated data structure
                addRowInPreCalculatedSlots(lOperativeId, lDateRange(0).Item1, lDateRange(0).Item2)
            End If

            Return overLap
        End If
    End Function
#End Region

#Region "check Overlap"
    ''' <summary>
    ''' This method checks the overlapping of dates
    ''' </summary>
    ''' <param name="pRanges"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function checkOverlap(ByVal pRanges As ArrayList) As Boolean
        Dim lRetVal As Boolean = False

        'For j As Integer = 1 To j < pRanges.Count Step 1
        Dim j As Integer = 1
        While (j < pRanges.Count)

            'Checks for following rules: (appointment time slot to check (pRanges(0)) and existing appointment to compare(pRanges(j), where j>=1 and j<totalexisting appointments)
            '1- Check for appointment slot to check start time is between appointment to compare start time(>=, start time is included)
            '   and end time (<, end time is not included)
            '2- Check for appointment slot to check end time is between appointment to compare start time(>, start time is not included)
            '   and end time (<=, end time is included)
            '3- Check for appointment to compare is between appointment slot to check(or both are same).
            If (pRanges(0).Item1 >= pRanges(j).Item1 AndAlso pRanges(0).Item1 < pRanges(j).Item2) _
                OrElse (pRanges(0).Item2 > pRanges(j).Item1 AndAlso pRanges(0).Item2 <= pRanges(j).Item2) _
                OrElse (pRanges(0).Item1 <= pRanges(j).Item1 AndAlso pRanges(0).Item2 >= pRanges(j).Item2) Then


                lRetVal = True
                Exit While
            End If
            j = j + 1
        End While


        Return lRetVal
    End Function
#End Region

#Region "calculate Duration"
    ''' <summary>
    ''' This function calculates the duration from the faults data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculateDuration() As Double
        Dim lDtFaultLogId As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)

        Dim lDuration As Double = 0

        For Each dr As DataRow In lDtFaultLogId.Rows
            If Not IsDBNull(dr("Duration")) And Not IsNothing(dr("Duration")) Then
                lDuration += CType(dr("Duration"), Double)
            End If
        Next

        Return lDuration
        'Return 4 '-- for testing
    End Function
#End Region

#Region "calculate Fault Log List"
    ''' <summary>
    ''' This function calculates the faultLogIdList from the faults data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function calculateFaultLogList() As String
        Dim lFaultLogDt As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)

        Dim lFaultLogId As String = String.Empty

        For Each dr As DataRow In lFaultLogDt.Rows
            If dr("FaultLogID") <> Nothing Then
                lFaultLogId = lFaultLogId + dr("FaultLogID").ToString() + ","
            End If
        Next

        If lFaultLogDt.Rows.Count > 0 Then
            lFaultLogId = lFaultLogId.Remove(lFaultLogId.Length - 1, 1)
        End If

        Return lFaultLogId
    End Function
#End Region

#Region "get Operative Dt"
    ''' <summary>
    ''' This function returns a datatable
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getOperativeDt() As DataTable
        Dim lDtOperative As New DataTable

        'lDtOperative.Columns.Add("Jsn")
        lDtOperative.Columns.Add("operativeId")
        lDtOperative.Columns.Add("operativeName")
        lDtOperative.Columns.Add("timeString")
        lDtOperative.Columns.Add("dateString")
        lDtOperative.Columns.Add("CompleteData")

        Return lDtOperative
    End Function
#End Region

#Region "get Leaves Dt"
    Private Function getLeavesDt()
        Dim dt As New DataTable

        dt.Columns.Add("StartDate")
        dt.Columns.Add("ReturnDate")
        dt.Columns.Add("employeeid")
        Return dt
    End Function
#End Region

#Region "fill Leaves Dt"
    ''' <summary>
    ''' This function converts the leaves of operative in daily time slots and returns the new datatable of leaves
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function fillLeavesDt(ByVal dt As DataTable)
        Dim leavesDt As New DataTable()

        If dt.Rows.Count() > 0 Then

            Dim startDate As Date = Date.Today
            Dim endDate As Date = Date.Today
            Dim startTime As DateTime = DateTime.Now
            Dim endTime As DateTime = DateTime.Now
            Dim goToNext As Boolean = True
            leavesDt = Me.getLeavesDt()

            For Each dr As DataRow In dt.Rows

                startDate = CType(dr("STARTDATE"), Date)
                endDate = CType(dr("RETURNDATE"), Date)

                Dim counter As Integer = 0

                While startDate <= endDate

                    'morning =  08:00 AM - 12:00 PM
                    'evening =  01:00 PM - 05:00 PM
                    Dim lDr As DataRow = leavesDt.NewRow()
                    lDr("employeeid") = CType(dr("employeeid"), Integer)
                    If CType(dr("HolType"), String) = "M" Then
                        'A means morning is off
                        lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "8:00 AM")
                        lDr("ReturnDate") = Convert.ToDateTime(endDate.ToShortDateString() + " " + "12:00 PM")
                    ElseIf CType(dr("HolType"), String) = "A" Then
                        'A means evening is off
                        lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "12:00 PM")
                        lDr("ReturnDate") = Convert.ToDateTime(endDate.ToShortDateString() + " " + "05:00 PM")
                    ElseIf CType(dr("HolType"), String) = "F" Then
                        'F means full day is off
                        lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "08:00 AM")
                        lDr("ReturnDate") = Convert.ToDateTime(endDate.ToShortDateString() + " " + "05:00 PM")
                    ElseIf CType(dr("HolType"), String) = "F-F" Then
                        'F means full day is off
                        lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "08:00 AM")
                        lDr("ReturnDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "05:00 PM")
                    ElseIf CType(dr("HolType"), String) = "A-F" Then
                        'A-F means on first day half day (evenning) is off and rest of the days are full days off
                        If counter = 0 Then
                            lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "01:00 PM")
                            lDr("ReturnDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "05:00 PM")
                        Else
                            lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "08:00 AM")
                            lDr("ReturnDate") = Convert.ToDateTime(endDate.ToShortDateString() + " " + "05:00 PM")
                        End If
                    ElseIf CType(dr("HolType"), String) = "F-M" Then
                        'F-M means last day is off only in morning rest of the days are full off days 
                        If startDate = endDate Then
                            lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "08:00 AM")
                            lDr("ReturnDate") = Convert.ToDateTime(endDate.ToShortDateString() + " " + "12:00 PM")
                        Else
                            lDr("StartDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "08:00 AM")
                            lDr("ReturnDate") = Convert.ToDateTime(startDate.ToShortDateString() + " " + "05:00 PM")
                        End If

                    End If
                    leavesDt.Rows.Add(lDr.ItemArray)
                    startDate = startDate.AddDays(1)
                    counter = counter + 1
                End While

            Next
        End If
        Return leavesDt
    End Function
#End Region

#Region "create Pre Calculated Slots Dt"
    ''' <summary>
    ''' This function returns a datatable to save the already displayed operatives and their timeslots
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function createPreCalculatedSlotsDt() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("OperativeId")
        dt.Columns.Add("StartDate")
        dt.Columns.Add("EndDate")
        Return dt
    End Function
#End Region

#Region "add Row In Previous Calculated Slots"
    ''' <summary>
    ''' This function add new row  is previously claculated slots
    ''' </summary>
    ''' <param name="operativeId"></param>
    ''' <param name="startDate"></param>
    ''' <param name="endDate"></param>
    ''' <remarks></remarks>

    Private Sub addRowInPreCalculatedSlots(ByRef operativeId As Integer, ByRef startDate As Date, ByVal endDate As Date)
        Dim preCalculatedSlotsDr As DataRow = preCalculatedSlotsDt.NewRow()
        preCalculatedSlotsDr("OperativeId") = operativeId
        preCalculatedSlotsDr("StartDate") = startDate
        preCalculatedSlotsDr("EndDate") = endDate
        preCalculatedSlotsDt.Rows.Add(preCalculatedSlotsDr.ItemArray)
        SessionManager.setPreCalculatedSlots(preCalculatedSlotsDt)
    End Sub
#End Region

#Region "get Start End Time"
    ''' <summary>
    ''' this function calculates the start and end time of operative from the last calculated time
    ''' </summary>
    ''' <param name="pOperativeDr"></param>
    ''' <param name="pOperativeIndex"></param>
    ''' <param name="pOperativeCount"></param>
    ''' <param name="pStartTime"></param>
    ''' <param name="lEndTime"></param>
    ''' <param name="pDuration"></param>
    ''' <param name="goNextOperative"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getStartEndTime(ByVal pOperativeDr As DataRow, ByRef pOperativeIndex As Integer, ByVal pOperativeCount As Integer, ByRef pStartTime As Date, ByRef lEndTime As Date, ByVal pDuration As Double, ByVal goNextOperative As Boolean)

        Dim operativeId As Integer
        ' switch the operative
        If goNextOperative = True Then
            If (pOperativeIndex < pOperativeCount - 1) Then
                pOperativeIndex = pOperativeIndex + 1
            Else
                pOperativeIndex = 0
            End If
        End If
        'get the operative id
        operativeId = mOperativesDt.Rows(pOperativeIndex)("EmployeeId")
        'query the already calculated slots
        Dim slots = preCalculatedSlotsDt.AsEnumerable()
        'Start - Changes By Aamir Waheed on 21 June 2013
        'To remove an exception(Stack Overflow that lead to application crash)
        'The issue was becase of wrong ordered soltsResults created by app.Item("EndDate") Descending
        'Like in an example when order for app.Item("EndDate") Descending with dates i.e 30/06/2013 and 01/07/2013
        'It returned 30/06/2013 in place of 01/07/2013
        'Dim slotsResult = (From app In slots Where app("OperativeId") = operativeId Select app Order By app.Item("EndDate") Descending)
        Dim slotsResult = (From app In slots Where app("OperativeId") = operativeId Select app)
        'if already calcualated slot has been found then set last calculated end time as Start Time
        If slotsResult.Count() > 0 Then
            'Used Last Item insted of using First and Order Clause in Linq
            pStartTime = slotsResult.Last().Item("EndDate")
            'pStartTime = slotsResult.First().Item("EndDate")
            'End - Changes By Aamir Waheed on 21 June 2013
            lEndTime = calculateDateTime(pStartTime, pDuration)
        Else
            'if operative does not have any pre calculated entry then calculate start time and end time
            pStartTime = Convert.ToDateTime(Date.Today.AddDays(1).ToShortDateString().ToString()).AddHours(8)
            lEndTime = calculateDateTime(pStartTime, pDuration)
        End If

        'In one scenario when we switch the operative it is possible that start and end time does not lie with in working hour slot
        'so claculate the next day slot
        If goNextOperative = True Then
            If (Me.verifyTimeSlot(lEndTime)) = False Then
                pStartTime = Convert.ToDateTime(pStartTime.AddDays(1).ToShortDateString().ToString()).AddHours(8)
                lEndTime = calculateDateTime(pStartTime, pDuration)
            End If
        End If

        Return pStartTime
    End Function
#End Region

#End Region

#Region "Function"

#Region "get Available Operatives"

    Private Sub getAvailableOperatives()

        Dim lFaultLogId As String = String.Empty

        lFaultLogId = calculateFaultLogList()

        If lFaultLogId <> String.Empty Then
            operativeDs = fetchOperativeData(lFaultLogId, SessionManager.getPropertyId())
        End If

        If (operativeDs.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.OperativeNotFound, True)
        End If
        'save Available operative in session
        SessionManager.setAvailableOperativesDs(operativeDs)

    End Sub
#End Region

#Region "Populate temp Faults And Appointment"
    Private Sub populateTempFaultsAndAppointments()

        Dim tempFaultsExist As Boolean = False
        'check if the unconfirmed faults exists in table
        Dim TempFaultIdDt As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)
        If TempFaultIdDt.Rows.Count > 0 Then
            tempFaultsExist = True
        End If

        'if confirmed faults exists then create a list 
        If tempFaultsExist = True Then            '
            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
            Dim tempFaultAptBoList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            'this function 'll process the faults and 'll create the appointments
            ' Parameter group by must be true because group by is using for same trad operatives.
            ' if group by false calculated hours will be different from actual calculated hour.
            tempFaultAptBoList = faultAppointmentBl.getTempFaultsAppointments(TempFaultIdDt, Me.operativeDs.Tables(0), Me.operativeDs.Tables(1), Me.operativeDs.Tables(2), True, Me.commonAddressBo)
            'save list in session 
            SessionManager.setTempFaultAptBlockList(tempFaultAptBoList)
            Dim FaultDt As DataTable = tempFaultAptBoList(0).tempAppointmentDtBo.dt
            grdOperative.DataSource = FaultDt
            grdOperative.DataBind()

            'Call the update markers for any refresh in appointment slots.
            If Page.IsPostBack Then
                updateMarkersJavaScript()
            End If
        End If

    End Sub
#End Region

#Region "refresh The Opertives List"
    Private Sub refreshTheOpertivesList(ByVal selectedFaultAptBlockId As Integer)
        operativeDs = SessionManager.getAvailableOperativesDs()
        Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
        Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()

        If selectedFaultAptBlockId > 0 Then
            tempFaultAptList = SessionManager.getTempFaultAptBlockList()
        End If

        tempFaultAptBo.IsPatchSelected = chkPatch.Checked
        tempFaultAptBo.IsDateSelected = chkDueDate.Checked

        'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
        'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
        Dim index As Integer = selectedFaultAptBlockId - 1 ' Me.getBlockListIndex(selectedFaultAptBlockId)
        tempFaultAptBo = tempFaultAptList(index)

        'get the datatable from the object
        Dim tempAptDt As DataTable = tempFaultAptBo.tempAppointmentDtBo.dt

        'increase the count of operatives by 
        tempFaultAptBo.DisplayCount = tempFaultAptBo.DisplayCount + 5
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
        faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)

        'save in temporary fault operative list
        tempFaultAptList(index) = tempFaultAptBo

        'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment         
        faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)
        SessionManager.setTempFaultAptBlockList(tempFaultAptList)
        'SessionManager.removeCustomerData()

        Dim FaultDt As DataTable = tempFaultAptList(0).tempAppointmentDtBo.dt
        grdOperative.DataSource = FaultDt
        grdOperative.DataBind()
        'Call the update markers for any refresh in appointment slots.
        If Page.IsPostBack Then
            updateMarkersJavaScript()
        End If

    End Sub
#End Region

#Region "get Block List Index"
    ''' <summary>
    ''' 'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
    ''' 'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultApList 
    ''' </summary>
    ''' <param name="selectedFaultAptBlockId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getBlockListIndex(ByVal selectedFaultAptBlockId As Integer)
        Return selectedFaultAptBlockId - 1
    End Function
#End Region

#Region "set AppointmentBo And Temp Faults"

    Private Sub setAppointmentBo(ByRef parameters As String)
        Dim lOperativeDataArr As String() = parameters.Split("|")

        Dim lAppointmentStartDateTime As Date = Date.ParseExact(lOperativeDataArr(appointmentBo.AppointmentParameter.AppointmentStartDateTime).Trim(), ApplicationConstants.AppointmentDateTimeFormat, CultureInfo.CreateSpecificCulture("en-GB"))
        Dim lAppointmentEndDateTime As Date = Date.ParseExact(lOperativeDataArr(appointmentBo.AppointmentParameter.AppointmentEndDateTime).Trim(), ApplicationConstants.AppointmentDateTimeFormat, CultureInfo.CreateSpecificCulture("en-GB"))

        Me.appointmentBo.OperativeId = lOperativeDataArr(appointmentBo.AppointmentParameter.OperativeId).Trim()
        Me.appointmentBo.Operative = lOperativeDataArr(appointmentBo.AppointmentParameter.Operative).Trim()
        Me.appointmentBo.StartTime = lAppointmentStartDateTime.ToString("HH:mm")
        Me.appointmentBo.EndTime = lAppointmentEndDateTime.ToString("HH:mm")
        Me.appointmentBo.AppointmentStartDate = lAppointmentEndDateTime.Date
        Me.appointmentBo.AppointmentEndDate = lAppointmentEndDateTime.Date

        SessionManager.setAppointmentDataForAppointmentSummary(Me.appointmentBo)

        populateAppointmentDetailPopup()
        'Save the confirmed faults in temp fault
    End Sub

#End Region

#Region "Popups"

#Region "Popup Link Button Next Fault Click Event"

    Protected Sub lnkBtnNextPopupFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnNextPopupFault.Click

        Try

            Me.scrollPopupFaults()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Populate appointment detail popup"

    Sub populateAppointmentDetailPopup()

        Me.resetAppointmentDetailPopup()
        Dim confirmedFaultDT As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        If appointmentData.OperativeId = 0 Or IsNothing(confirmedFaultDT) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else

            lblPopOperative.Text = appointmentData.Operative
            lblPopAppointmentStartDateTime.Text = String.Format("{0} {1}", appointmentData.StartTime, appointmentData.AppointmentStartDate.ToString(ApplicationConstants.CustomLongDateFormat))
            lblPopAppointmentEndDateTime.Text = String.Format("{0} {1}", appointmentData.EndTime, appointmentData.AppointmentEndDate.ToString(ApplicationConstants.CustomLongDateFormat))

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.PopupSlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = confirmedFaultDT

            changePopupFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count = 1 Then
                lnkBtnNextPopupFault.Enabled = False
                lnkBtnNextPopupFault.Visible = False
            End If
            popupConfirmAppointmentDetail.Show()

        End If


    End Sub

#End Region

#Region "Reset appointment detail popup controls"

    Sub resetAppointmentDetailPopup()
        lblPopOperative.Text = String.Empty
        lblPopupAreaName.Text = String.Empty
        lblPopupFaultDescription.Text = String.Empty
        lblPopDuration.Text = String.Empty
        lblPopTrade.Text = String.Empty
        lblPopDue.Text = String.Empty
        lblPopAppointmentStartDateTime.Text = String.Empty
        lblPopAppointmentEndDateTime.Text = String.Empty
    End Sub

#End Region

#Region "Scroll Popup Faults"

    Sub scrollPopupFaults()

        Dim confirmedFaultDT As DataTable = CType(SessionManager.getConfirmedFaults(), DataTable)
        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = confirmedFaultDT

        If (IsNothing(slideDataSet)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.PopupSlideIndex)
            changePopupFaultValues(slideDataSet, slideIndex)
            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                lnkBtnNextPopupFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.PopupSlideIndex) = slideIndex
            End If

        End If
    End Sub

#End Region

#Region "Change Popup Fault Values"

    Sub changePopupFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("AreaName")) Then
            lblPopupAreaName.Text = "NA"
        Else
            lblPopupAreaName.Text = slideDataSet.Rows(slideIndex).Item("AreaName").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Description")) Then
            lblPopupFaultDescription.Text = "NA"
        Else
            lblPopupFaultDescription.Text = slideDataSet.Rows(slideIndex).Item("Description").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Duration")) Then
            lblPopDuration.Text = "NA"
        Else
            lblPopDuration.Text = slideDataSet.Rows(slideIndex).Item("Duration").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("TradeName")) Then
            lblPopTrade.Text = "NA"
        Else
            lblPopTrade.Text = slideDataSet.Rows(slideIndex).Item("TradeName").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("CompleteDueDate")) Then
            lblPopDue.Text = "NA"
        Else

            Dim aptDueDate As DateTime = Convert.ToDateTime(slideDataSet.Rows(slideIndex).Item("CompleteDueDate").ToString())
            Dim format As String = "d  MMMMMMMMMM yyyy"
            aptDueDate.ToString()
            lblPopDue.Text = aptDueDate.ToString(format)
        End If

    End Sub

#End Region

#Region "Popup Confirm Appointment Button Click Event"

    Protected Sub btnPopupConfirmAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupConfirmAppointment.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            Me.confirmAppointment()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Confirm Appointment"

    Sub confirmAppointment()

        Dim appointmentId As Integer = -1
        Dim appointmentType As Integer = -1
        Dim tempCount As Integer = 0
        Dim isAppointmentPushed As Boolean = False
        Dim objFaultBL As FaultBL = New FaultBL
        Dim resultDataset As DataSet = New DataSet
        Dim jobSheetList As String = ""
        Dim appointmentData As New AppointmentBO
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        Me.saveAppointment(appointmentId, appointmentType)

        If (Not appointmentId = -1) Then
            If (appointmentData.AppointmentStartDate.Date = Today.Date) Then
                isAppointmentPushed = Me.pushAppointmentConfirmed(appointmentId, appointmentType)
                If (Not isAppointmentPushed) Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message = UserMessageConstants.PushNotificationFailed
                End If
            End If

            objFaultBL.getJobsheetlistByAppointment(resultDataset, appointmentId)

            If (resultDataset.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.message += "<br />" + UserMessageConstants.NoJobSheetFound
            Else
                Try
                    jobSheetList = Me.getCommaSeparatedJobsheetList(resultDataset)
                    Me.sendEmail(jobSheetList)
                    Me.sendSMS(jobSheetList)
                    ViewState.Add(ViewStateConstants.RedirectTempCount, tempCount)
                    Me.populateAppointmentConfirmedPopup()
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message += "<br />" + ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        uiMessageHelper.IsExceptionLogged = True
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                End Try
            End If

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
        End If

    End Sub

#End Region

#Region "Get comma separated jobsheet list"

    Function getCommaSeparatedJobsheetList(ByVal resultDataset As DataSet) As String

        Dim length As Integer = resultDataset.Tables(0).Rows.Count
        Dim jobSheetList As StringBuilder = New StringBuilder()

        For i As Integer = 0 To length - 1
            jobSheetList.Append(resultDataset.Tables(0).Rows(i).Item("JobSheetNumber").ToString() + ",")
        Next

        jobSheetList.Remove(jobSheetList.ToString().Length - 1, 1)

        Return jobSheetList.ToString()

    End Function

#End Region

#Region "Save Appointment"

    Sub saveAppointment(ByRef appointmentId As Integer, ByRef type As Integer)

        Dim lRetDt As New DataTable
        Dim lfaultList As String = String.Empty
        Dim appointmentData As New AppointmentBO
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()
        appointmentData.IsCalendarAppointment = False
        appointmentData.FaultLogdt = SessionManager.getConfirmedFaults().AsDataView().ToTable("FaultLog", False, "FaultLogId", "Duration")

        If SessionManager.getIsAppointmentRearrange() = True Then
            lfaultList = calculateFaultLogList()
            updateAppointment(appointmentData, lfaultList)
            appointmentId = Me.getAppointmentId()
            type = ApplicationConstants.RearrangeAppointmentType

        Else
            ' if user is coming from screen 26 then user 'll have to create the appointment instead of arrange appointment
            If saveFaultInTempTable(CType(SessionManager.getConfirmedFaults(), DataTable), lRetDt, lfaultList) Then
                'call save method
                'save in getConfirmedFaults session
                'save it
                'redriect  
                Dim dt As New DataTable
                dt = lRetDt
                Dim areaName As String = dt.Rows.Item(0).Item("AreaName")

                insertInFaultLog(appointmentData.OperativeId, lfaultList, appointmentData.AppointmentStartDate, appointmentData.StartTime, appointmentData.EndTime, appointmentData.IsCalendarAppointment, appointmentData.AppointmentEndDate, areaName)
                SessionManager.setIsAppointmentRearrange(True)
                appointmentId = IIf(SessionManager.getAppointmentId() = 0, -1, SessionManager.getAppointmentId())
                type = ApplicationConstants.NewAppointmentType
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.InvalidFaultData
            End If

        End If

    End Sub

#End Region

#Region "Get appointment Id "

    Function getAppointmentId() As Integer
        Dim appointmentId As Integer = -1

        Dim faultsDataTable As DataTable = New DataTable()
        faultsDataTable = SessionManager.getConfirmedFaults()

        'validate the session values
        If Not IsNothing(faultsDataTable) Then

            'Make comma separated list of temporary fault ids and fault trade ids
            For Each row As DataRow In faultsDataTable.Rows
                appointmentId = IIf(IsDBNull(row.Item("AppointmentId")), 0, row.Item("AppointmentId"))
            Next

        End If

        Return appointmentId

    End Function

#End Region

#Region "Push appointment confirmed"

    Public Function pushAppointmentConfirmed(ByVal appointmentId As Integer, ByVal type As Integer) As Boolean
        Dim URL As String = String.Format("{0}?appointmentId={1}&type={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentId, type)
        Dim request As WebRequest = Net.WebRequest.Create(URL)
        request.Credentials = CredentialCache.DefaultCredentials
        Dim response As WebResponse = request.GetResponse()
        Dim dataStream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        response.Close()

        If (responseFromServer.ToString.Equals("true")) Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "Populate appointment confirmed popup"

    Sub populateAppointmentConfirmedPopup()

        lblAptArrangedOperative.Text = String.Empty

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        If appointmentData.OperativeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else
            lblAptArrangedOperative.Text = appointmentData.Operative
            popupAppointmentConfirmed.Show()
        End If

    End Sub

#End Region

#Region "Continue button after appointment confirmed."

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinue.Click

        Try

            popupAppointmentConfirmed.Hide()

            If isNoEntry Then
                'Remove the values from session used to schedule an appointment for no entry and set isNoEntryScheduled flag accordingly.
                SessionManager.removeIsNoEntry()
                SessionManager.removeNoEntryFaultLogId()
                Response.Redirect(PathConstants.ReportAreaPath + "?" + PathConstants.RepairsDashboard + "=ne", True)
            End If

            Me.redirectToSearch()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Redirect To Search"
    Private Sub redirectToSearch()
        Response.Redirect(PathConstants.SearchFault + GeneralHelper.getSrc(PathConstants.RearrangeAppointmentSrcVal), True)
    End Sub
#End Region

#Region "Button Close Event"

    Protected Sub imgBtnCloseAppointmentDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgBtnCloseAppointmentDetail.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            ViewState.Item(ViewStateConstants.PopupSlideIndex) = 0
            lnkBtnNextPopupFault.Enabled = True
            lnkBtnNextPopupFault.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Email Functionality"

#Region "Send Email"
    Private Sub sendEmail(ByVal jobSheetNumberList As String)

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        'Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheetNumberList, appointmentData.Time, appointmentData.AppointmentDate, customerBo.Name)

        Dim recepientEmail As String = SessionManager.getCustomerData().Email
        Dim recepientName As String = customerBo.Name

        Try
            If Validation.isEmail(recepientEmail) Then
                Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail
                Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheetNumberList, appointmentData.StartTime, appointmentData.AppointmentStartDate, recepientName)

                Dim mailMessage As New Mail.MailMessage

                mailMessage.Subject = subject
                mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                mailMessage.AlternateViews.Add(alternateView)

                EmailHelper.sendEmail(mailMessage)
                'EmailHelper.sendHtmlFormattedEmail(Me.customerBo.Name, recepientEmail, subject, body)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Send SMS"
    Private Sub sendSMS(ByVal jobSheetNumberList As String)

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()


        Dim recepientMobile As String = SessionManager.getCustomerData().Mobile

        Try
            If Validation.isMobile(recepientMobile) Then

                Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(jobSheetNumberList, appointmentData.StartTime, appointmentData.AppointmentStartDate)
                GeneralHelper.sendSmsUpdatedAPI(body, recepientMobile.Replace(" ", String.Empty))
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub
#End Region

#Region "Google Map Functions"

#Region "Save existing appointments in session to show on map"

    Private Sub saveExistingAppointmentForVisibleOperatives()
        Dim operativesDs As DataSet = SessionManager.getAvailableOperativesDsForRearrange()
        Dim existingAppointments As New DataTable()

        If Not IsNothing(operativesDs) AndAlso operativesDs.Tables.Count >= 3 Then
            existingAppointments = operativesDs.Tables(2)
        End If

        Dim tempFaultAptBoList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)

        tempFaultAptBoList = SessionManager.getTempFaultAptBlockList()

        Dim dt As DataTable
        Dim operativeId As Integer
        Dim operativesList As New List(Of Integer)
        For Each tempFaultAptBO As TempFaultAppointmentBO In tempFaultAptBoList
            dt = tempFaultAptBO.tempAppointmentDtBo.dt

            For Each row As DataRow In dt.Rows
                operativeId = row(TempAppointmentDtBO.operativeIdColName)
                If Not operativesList.Contains(operativeId) Then
                    operativesList.Add(operativeId)
                End If
            Next
        Next

        SessionManager.removeExistingAppointmentsForRearranggeAppointmentsMap()
        If existingAppointments.Columns.Count > 0 And existingAppointments.Columns.Contains("OperativeId") Then
            Dim appointmentsForMap = From extapt In existingAppointments.AsEnumerable()
                                     Where operativesList.Contains(extapt("OperativeId"))
                                     Select extapt

            If appointmentsForMap.Count() > 0 Then
                SessionManager.setExistingAppointmentsForRearranggeAppointmentsMap(appointmentsForMap.CopyToDataTable())
            End If

        End If

    End Sub

#End Region

#Region "Web Method - Get Existing Appointments Data (Property Address)"

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function getExistingAppointmentsData() As String
        Dim returnJSON As New StringBuilder()
        Dim serializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim appointmentsForMap = SessionManager.getExistingAppointmentsForRearranggeAppointmentsMap()

        Dim rows = New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        If Not IsNothing(appointmentsForMap) Then
            For Each dr As DataRow In appointmentsForMap.Rows
                row = New Dictionary(Of String, Object)()
                For Each col As DataColumn In appointmentsForMap.Columns
                    row.Add(col.ColumnName, dr(col))
                Next
                rows.Add(row)
            Next
        End If

        Return serializer.Serialize(rows)
    End Function

#End Region

#Region "Register client side script to update markers on Google Map. on appointments refresh."

    Private Sub updateMarkersJavaScript()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "updateMarkersAptA", "addOriginMarkers();", True)
    End Sub

#End Region

#Region "Register client side script to remove existing markers from Map"

    Private Sub removeMarkersJavaScript()
        'Register client side script to remove markers on Google Map.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "deleteMarkersAptA", "deleteMarkers();", True)
    End Sub

#End Region

#End Region

#Region "Validate Date"
    Private Function isValidDate(ByVal fromDate As String)
        Dim startDate As DateTime = Date.Now
        Dim result As Boolean = True
        If fromDate <> "" Then
            Dim dateTime As Date
            Dim valid As Boolean = Date.TryParseExact(Convert.ToDateTime(fromDate), "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTime)
            If valid = False Then

                uiMessageHelper.IsError = True
                uiMessageHelper.message = String.Format(UserMessageConstants.notValidDateFormat)
                ' Dim appointmentSlotsDtBo As AppointmentSlotsDtBO = New AppointmentSlotsDtBO()

                '  Me.displayMsgInEmptyGrid(appointmentSlotsDtBo, UserMessageConstants.NoOperativesExistWithInTime)

                result = False
            End If

            startDate = Convert.ToDateTime(fromDate)
        End If


        If startDate < Now.Date Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.DateNotValid
            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            result = False

        Else
            uiMessageHelper.IsError = False
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)


        End If
        Return result
    End Function

#End Region
#End Region

End Class