﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/Fault.Master"
    CodeBehind="MoreSbDetails.aspx.vb" Inherits="FaultScheduling.MoreSbDetails" %>

<%@ Register TagPrefix="custom" TagName="SchemeBlockDetails" Src="~/Controls/Common/SchemeBlockHeader.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelRecallDetails" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <table id="more_details_table_1">
                <tr>
                    <td colspan="2">
                        <%--<strong>More fault details for:</strong>
                <asp:Label ID="lblPropertyAddress" runat="server"></asp:Label>
                , <strong>Tel:</strong><asp:Label ID="lblTelephone" runat="server"></asp:Label>--%>
                        <custom:SchemeBlockDetails ID="schemeBlockDetails" runat="server" StartString="More fault details for:">
                        </custom:SchemeBlockDetails>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location :
                    </td>
                    <td align="left">
                        &nbsp;<asp:DropDownList ID="ddlLocation" runat="server" Width="360px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Description :</td>
                    <td>
                        <asp:Label ID="lblFaultDescription" runat="server" Text="lblFaultDescription " Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <table id="more_details_table_3" width="700px">
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recommended response time :
                    </td>
                    <td align="right">
                        <%--<asp:Label ID="lblFaultPriority" runat="server"></asp:Label>--%>
                        <asp:Label ID="lblRecommendedTime" runat="server"></asp:Label>
                        <asp:Label ID="Label1" runat="server" Text="( "></asp:Label>
                        <asp:Label ID="lblPriorityName" runat="server"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text=" )"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;How long have you had this fault?
                    </td>
                    <td align="right">
                        <asp:DropDownList ID="ddlFaultDays" runat="server">
                            <asp:ListItem Value="1">1 Day</asp:ListItem>
                            <asp:ListItem Value="2">2 Day</asp:ListItem>
                            <asp:ListItem Value="3">3 Day</asp:ListItem>
                            <asp:ListItem Value="4">4 Day</asp:ListItem>
                            <asp:ListItem Value="5">5 Day</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Is this a recurring problem?
                    </td>
                    <td align="right">
                        <div style="width: 20%; float: right;">
                            <asp:RadioButton ID="rbtnRecurreingProblemNo" runat="server" GroupName="RecurringProblem"
                                Text="No" Checked="True" />
                        </div>
                        <div style="width: 30%; float: right;">
                            <asp:RadioButton ID="rbtnRecurreingProblemYes" runat="server" GroupName="RecurringProblem"
                                CssClass="margin_right20" Text="Yes" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Is this fault rechargeable?</td>
                    <td align="right">
                        <div style="width: 70%; float: left;">
                          <asp:Label ID="lblRechargeStatus" Font-Bold="true" runat="server"></asp:Label> : &nbsp;&nbsp;
                        </div>
                        <div style="width: 30%; float: right;">
                           ( <asp:Label ID="lblRechargableAmount" runat="server"></asp:Label> )
                        </div>
                        <%--   <div style="width: 50%; float: right;">
                            <asp:RadioButton ID="rbtnFaultRechargable" runat="server" Text="Yes" CssClass="margin_right20"/>
                        </div>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Notes you may feel relevant to the fault:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="width: 100%; text-align: right;">
                            <asp:TextBox ID="txtFaultOperatorNote" CssClass="roundcornerby5" runat="server" TextMode="MultiLine"
                                Height="65px" Width="90%"></asp:TextBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This information will appear on the app for the operative to see.
                    </td>
                </tr>
            </table>
            <div class="text_a_r_padd__r_20px" style="width: 800px; margin-top: 70px;">
                <asp:Button ID="btnBack" runat="server" CssClass="margin_right20" Text="&lt; Back" />
                <asp:Button ID="btnAddToFaultBasket" runat="server" CssClass="margin_right20" Text="Add To Fault Basket" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
