﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintSubcontractorJobSheetSummary.aspx.vb" Inherits="FaultScheduling.PrintSubcontractorJobSheetSummary" %>
 <link href="~/Styles/layout.css" rel="stylesheet" type="text/css" />
  <link href="~/Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
<form id="form1" runat="server">
<asp:panel id="pnlMessage" runat="server" visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:panel>
<asp:scriptmanager runat="server"></asp:scriptmanager>
<table id="jobsheet_detail_table" style="width: 100%; ">
    <tr>
        <td colspan="3">
            <asp:label id="lblRecallDetailsFor" runat="server" text="Recall details for : " font-bold="true"></asp:label>
            <asp:label id="lblClientNameHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;<asp:label id="lblClientStreetAddressHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;,
            <asp:label id="lblClientCityHeader" runat="server" text="" font-bold="true"></asp:label>
            &nbsp;<asp:label id="lblClientTelHeader" runat="server" text=" Tel :" font-bold="true"> </asp:label><asp:label
                id="lblClientTelPhoneNumberHeader" runat="server" text="" font-bold="true"></asp:label>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <asp:label id="lblFaultId" runat="server" font-bold="true"></asp:label>
        </td>
        <td>
        </td>
        <td>
            <asp:label id="lblClientName" runat="server" font-bold="true"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Contractor :
        </td>
        <td>
            <asp:label id="lblContractor" runat="server"></asp:label>
        </td>
        <td>
            <asp:label id="lblClientStreetAddress" runat="server"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Operative :
        </td>
        <td>
            <asp:label id="lblOperativeName" runat="server"></asp:label>
        </td>
        <td>
            <asp:label id="lblClientCity" runat="server"></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Priority :
        </td>
        <td>
            <asp:label id="lblFaultPriority" runat="server"></asp:label>
        </td>
        <td>
            <asp:label id="lblClientPostCode" runat="server" text=""></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Completion due :
        </td>
        <td>
            <asp:label id="lblFaultCompletionDateTime" runat="server"></asp:label>
        </td>
        <td>
            <asp:label id="lblClientRegion" runat="server" text=""></asp:label>
        </td>
    </tr>
    <tr>
        <td>
            Order date :
        </td>
        <td>
            <asp:label id="lblOrderDate" runat="server"></asp:label>
        </td>
        <td>
            <div class="fault_detail_right_col_first_div">
                Tel:</div>
            <div class="fault_detail_right_col_second_div">
                <asp:label id="lblClientTelPhoneNumber" runat="server" font-bold="True"></asp:label>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            Location :
        </td>
        <td>
            <asp:label id="lblFaultLocation" runat="server"></asp:label>
        </td>
        <td>
            <div class="fault_detail_right_col_first_div">
                Mobile:</div>
            <div class="fault_detail_right_col_second_div">
                <asp:label id="lblClientMobileNumber" runat="server" text=""></asp:label>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            Description :
        </td>
        <td>
            <asp:label id="lblFaultDescription" runat="server" text="" font-bold="true"></asp:label>
        </td>
        <td>
            <div class="fault_detail_right_col_first_div">
                Email :</div>
            <div class="fault_detail_right_col_second_div">
                <asp:label id="lblClientEmailId" runat="server" text=""></asp:label>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            Trade :
        </td>
        <td>
            <asp:label id="lblTrade" runat="server" text=""></asp:label>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td>
            Note :
        </td>
        <td>
            <asp:textbox Enabled="false"  id="lblFaultOperatorNote" cssclass="roundcornerby5" runat="server" textmode="MultiLine"
                height="65px" width="248px"></asp:textbox>
        </td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <b>Appointment</b>
        </td>
        <td>
            <br />
            <asp:label id="lblFaultAppointmentDateTime" runat="server" text="N/A"></asp:label>
            <br />
        </td>
        <td rowspan="2">
            <b>Asbestos</b>
            <asp:gridview id="grdAsbestos" runat="server" autogeneratecolumns="False" showheader="false"
                borderstyle="None" CellSpacing="5" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID" />
                                <asp:BoundField DataField="Description" />
                            </Columns>
                        </asp:gridview>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="3">
            &nbsp;</td>
    </tr>
</table>
<div style="width: 100%; text-align: left; clear: both;">
</div>
</form>
