﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject

Public Class SbRecallDetails
    Inherits PageBase

#Region "Properties"

    Dim isRecall As Boolean = False

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' This event handles the page Load event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
            End If

            If Not IsPostBack Then
                'populateCustomerData()
                populateRecallDetails(SessionManager.getSbFaultLogId())
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Pre Render"
    ''' <summary>
    ''' This event handles Pre render function 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.btnScheduleNewFault.PostBackUrl = PathConstants.MoreSbDetailsFromStart + GeneralHelper.getSrc(PathConstants.RecallSbDetailsSrcVal)
            If Me.isRecall = True Then
                Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.OtherSbAppointmentPath + GeneralHelper.getSrc(PathConstants.RecallSbDetailsSrcVal))
            Else
                Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.RecallSbDetailsSrcVal))
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "btn Schedule Recal Click"
    ''' <summary>
    ''' This functino hanldes the schedule Recall button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnScheduleRecal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleRecal.Click
        SessionManager.setOriginalFaultLogId(SessionManager.getSbFaultLogId)

        Response.Redirect(PathConstants.SearchSbFault + GeneralHelper.getSrc(PathConstants.RecallSbDetailsSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes + "&jsn=" + lblFaultId.Text.Trim())
    End Sub
#End Region

#End Region

#Region "Functions "

#Region "Populate Values"


    ''' <summary>
    ''' This functino populates the recall detials in the form
    ''' </summary>
    ''' <param name="faultLogID"></param>
    ''' <remarks></remarks>
    Sub populateRecallDetails(ByVal faultLogID As Integer)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objRecallDetailsBl As RecallDetailsBL = New RecallDetailsBL()

        objRecallDetailsBl.getRecallDetails(resultDataSet, faultLogID)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAsbestos.Visible = False
        Else
            grdAsbestos.Visible = True
            grdAsbestos.DataSource = resultDataSet.Tables("Asbestos")
            grdAsbestos.DataBind()
            If Not IsNothing(resultDataSet.Tables("Asbestos")) AndAlso resultDataSet.Tables("Asbestos").Rows.Count > 0 Then
                lblAsbestos.Visible = True
            Else
                lblAsbestos.Visible = False
            End If
            grdRepairDetails.Visible = True
            grdRepairDetails.DataSource = resultDataSet.Tables("RepairDetail")
            grdRepairDetails.DataBind()

            lblFaultId.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(0).ToString
            lblContractor.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(1).ToString
            lblOperativeName.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(2).ToString
            lblFaultPriority.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(3).ToString
            lblFaultCompletionDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(4).ToString
            lblOrderDate.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(5).ToString
            lblFaultLocation.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(6).ToString
            lblFaultDescription.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(7).ToString
            lblFaultOperatorNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(8).ToString
            lblFaultAppointmentDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(9).ToString
            txtBoxAppointmentNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(10).ToString
            lblTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(11).ToString
            SessionManager.setSbFaultId(CType(resultDataSet.Tables("FaultAppointment").Rows(0)(12), Integer))

        End If

    End Sub

#End Region
#End Region
End Class