﻿Imports FLS_BusinessObject
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic

Public Class CancellationSbAppointmentSummary
    Inherits PageBase

#Region "Properties "

    Dim objCustomerBL As CustomerBL = New CustomerBL()
    Dim objFaultBl As FaultBL = New FaultBL()
    Dim rearrange As String

#End Region

#Region "Events"

#Region "Page Pre Render"
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            btnRescheduleAppointment.PostBackUrl = PathConstants.ReArrangingSbFault + GeneralHelper.getSrc(PathConstants.CancelSbAppointmentSummarySrcVal)
            Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.CancelSbAppointmentSummarySrcVal))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'createData()
                prePopulatedValues()
                populateAppointmentSummary()
                populateAppointmentDetails()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region



#Region "Link Button Next Fault Click Event"

    Protected Sub lnkBtnNextFault_Click(sender As Object, e As EventArgs) Handles lnkBtnNextFault.Click
        Try
            scrollFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Cancel Reported Faults Click Event"

    Protected Sub btnCancelReportedFaults_Click(sender As Object, e As EventArgs) Handles btnCancelReportedFaults.Click
        Try
            mdlPopUpCancelFaults.Show()
            If txtBoxDescription.Enabled = False Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.CancellationError, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Button Save Click Event"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            cancelFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Link Button Click Here Click Event"

    Protected Sub lnkBtnClickHere_Click(sender As Object, e As EventArgs) Handles lnkBtnClickHere.Click
        'Commented out the code as window will be closed by Java Script.
        'Dim customerId As Integer = SessionManager.getCustomerId()

        'If customerId <> 0 Then
        '    Response.Redirect(PathConstants.CustomerRecords + customerId.ToString())
        'Else
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        'End If

    End Sub

#End Region

#Region "Record No Entry Button Event Handler"

    Protected Sub btnNoEntry_Click(sender As Object, e As EventArgs) Handles btnNoEntry.Click
        Response.Redirect(PathConstants.OtherSbAppointmentPath + GeneralHelper.getSrc(PathConstants.CancelSbAppointmentSummarySrcVal))
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Values"

    Sub prePopulatedValues()

        resetSchemeBlockDetail()
        Dim commonAddressBo As CommonAddressBO = SessionManager.getCommonAddressBO()
        Dim sbData As SchemeBlockBO = SessionManager.getSbData()
        If sbData.SchemeId > 0 Then
            lblScheme.Text = sbData.SchemeName
            lblSchemeBlockAddress.Text = commonAddressBo.Address
            lblCOUNTY.Text = commonAddressBo.City
            lblPostCode.Text = commonAddressBo.PostCode
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If
    End Sub

#End Region



#Region "Populate Appointment Summary"

    Sub populateAppointmentSummary()
        Dim faultsDataTable As DataTable = New DataTable()
        faultsDataTable = SessionManager.getSbConfirmedFaults()

        'Dim faultsList As String = Request.QueryString.Get(PathConstants.FaultsList)
        '' The below line will be removed when query string is available **** Abdul Wahhab 14/02/2013 ****
        'faultsList = "112,179,194,225,398"

        If IsNothing(faultsDataTable) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else
            'Dim resultDataSet As DataSet = New DataSet()

            'objFaultBl.getAppointmentSummary(resultDataSet, faultsList)

            'If resultDataSet.Tables(0).Rows.Count = 0 Then
            '    ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            '    lnkBtnNextFault.Enabled = False
            '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            'Else

            '    ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)

            grdAppointments.DataSource = faultsDataTable
            grdAppointments.DataBind()

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.SlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = faultsDataTable

            changeFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count = 1 Then
                lnkBtnNextFault.Enabled = False
            End If

            'End If

        End If


    End Sub

#End Region

#Region "Populate Appointment Details"

    Sub populateAppointmentDetails()

        Dim appointmentId As Integer = Request.QueryString.Get(PathConstants.AppointmentId)

        If appointmentId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentIdNotProvided, True)
        Else
            Dim resultDataSet As DataSet = New DataSet()

            objFaultBl.getAppointmentData(resultDataSet, appointmentId)

            If resultDataSet.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoAppointmentRecord, True)
            Else

                Dim appointmentDataSet = resultDataSet.Tables(0)

                Dim durationTable As DataTable = New DataTable()
                durationTable = SessionManager.getSbConfirmedFaults()

                Dim length As Integer = 0

                If Not IsNothing(durationTable) Then
                    length = durationTable.Rows.Count
                End If

                Dim duration As Double = 0

                For i As Integer = 0 To length - 1
                    Dim dur As Double = 0
                    Double.TryParse(durationTable.Rows(i).Item("Duration").ToString, dur)
                    duration = duration + dur
                Next

                lblOperative.Text = appointmentDataSet.Rows(0).Item("Operative").ToString()
                If duration < 1 Or duration = 1 Then
                    lblDuration.Text = duration.ToString() + " Hour Approx"
                Else
                    lblDuration.Text = duration.ToString() + " Hours Approx"
                End If


                lblStartTime.Text = appointmentDataSet.Rows(0).Item("AppointmentStartTime").ToString()
                Dim appointmentStartDate As DateTime = appointmentDataSet.Rows(0).Item("AppointmentStartDate").ToString
                Dim appointmentEndDate As DateTime = appointmentDataSet.Rows(0).Item("AppointmentEndDate").ToString()
                lblStartDate.Text = Format(appointmentStartDate, ApplicationConstants.CustomLongDateFormat)
                lblEndDate.Text = Format(appointmentEndDate, ApplicationConstants.CustomLongDateFormat)
                lblAccessNote.Text = appointmentDataSet.Rows(0).Item("Notes").ToString()


                If Not IsDBNull(appointmentDataSet.Rows(0).Item("AppointmentEndTime")) Then
                    lblEndTime.Text = appointmentDataSet.Rows(0).Item("AppointmentEndTime")
                Else
                    'In case End time is not available for old appointments.
                    Dim EndTime As TimeSpan
                    Dim sTimeFrom As TimeSpan
                    TimeSpan.TryParse(lblStartTime.Text, sTimeFrom)
                    Dim result As TimeSpan = TimeSpan.FromHours(duration)

                    EndTime = sTimeFrom.Add(result)

                    lblEndTime.Text = EndTime.Hours.ToString() + ":" + EndTime.Minutes.ToString("00")
                End If
            End If
        End If
    End Sub

#End Region

#Region "Scroll Faults"

    Sub scrollFaults()

        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = SessionManager.getSbConfirmedFaults

        If IsNothing(slideDataSet) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.SlideIndex)

            changeFaultValues(slideDataSet, slideIndex)

            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                lnkBtnNextFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.SlideIndex) = slideIndex
            End If

        End If

    End Sub

#End Region

#Region "Cancel Faults"

    Sub cancelFaults()
        If txtBoxDescription.Text = "" Then
            mdlPopUpCancelFaults.Show()
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)
        Else
            Dim resultDataSet As DataTable = New DataTable()
            resultDataSet = SessionManager.getSbConfirmedFaults()

            'Dim faultsList As String = Request.QueryString.Get(PathConstants.FaultsList)
            '' The below line will be removed when query string is available **** Abdul Wahhab 14/02/2013 ****
            'faultsList = "112,179,194,225,398"

            If IsNothing(resultDataSet) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            Else
                mdlPopUpCancelFaults.Show()
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False

                Dim length As Integer = resultDataSet.Rows.Count

                Dim faultsList As StringBuilder = New StringBuilder()

                For i As Integer = 0 To length - 1
                    faultsList.Append(resultDataSet.Rows(i).Item("FaultLogID").ToString() + ",")
                Next

                faultsList.Remove(faultsList.ToString().Length - 1, 1)

                Dim objAppointmentCancellationBO As New AppointmentCancellationBO()

                objAppointmentCancellationBO.FaultsList = faultsList.ToString()
                objAppointmentCancellationBO.Notes = txtBoxDescription.Text

                If IsDBNull(objFaultBl.saveFaultCancellation(objAppointmentCancellationBO)) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultNotCancelled, True)
                    pnlErrorMessage.Visible = False
                    pnlCancelMessage.Visible = False
                    pnlCustomerMessage.Visible = False
                    pnlSaveButton.Visible = True
                    txtBoxDescription.Text = ""
                    txtBoxDescription.Enabled = True
                    mdlPopUpCancelFaults.Hide()
                Else
                    pnlMessage.Visible = False
                End If

            End If

        End If

    End Sub

#End Region

#Region "Change Fault Values"

    Sub changeFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        lblJSN.Text = slideDataSet.Rows(slideIndex).Item("JobSheetNumber").ToString()

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("ProblemDays")) Then
            lblProblemDays.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() = 1 Then
                lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " day"
            Else
                lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " days"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("RecuringProblem")) Then
            lblReccuringProblem.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("RecuringProblem").ToString() = True Then
                lblReccuringProblem.Text = "This is a recurring problem"
            Else
                lblReccuringProblem.Text = "This is not a recurring problem"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("CommunalProblem")) Then
            lblCommunalProblem.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("CommunalProblem").ToString() = True Then
                lblCommunalProblem.Text = "This is a communal problem"
            Else
                lblCommunalProblem.Text = "This is not a communal problem"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Notes")) Then
            lblFaultNotes.Text = "NA"
        Else
            lblFaultNotes.Text = slideDataSet.Rows(slideIndex).Item("Notes").ToString()
        End If

    End Sub

#End Region



#Region "Button Update Click"
    ''' <summary>
    ''' Button Update Click.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateClick(ByVal sender As Object, ByVal e As EventArgs)
        txtFaultNotes.Text = lblFaultNotes.Text
        txtAccessNotes.Text = lblAccessNote.Text
        mdlPopupFaultNotes.Show()

    End Sub

#End Region


#Region "Button Back Fault Click"
    ''' <summary>
    ''' Button Back Fault Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFaultBack(ByVal sender As Object, ByVal e As EventArgs)
        pnlFaultNotesErrorMessage.Visible = False
        mdlPopupFaultNotes.Hide()

    End Sub

#End Region


#Region "Button Save Fault Click"
    ''' <summary>
    ''' Button Save Fault Click.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFaultSave(ByVal sender As Object, ByVal e As EventArgs)
        If txtFaultNotes.Text.Length >= 500 Or txtAccessNotes.Text.Length >= 500 Then
            lblFaultNotesErrorMessage.Text = UserMessageConstants.NoteLength
            pnlFaultNotesErrorMessage.Visible = True
            mdlPopupFaultNotes.Show()
        Else
            Dim appointmentId As Integer = Request.QueryString.Get(PathConstants.AppointmentId)
            Dim updated As Integer
            updated = objFaultBl.updateFaultNotes(txtFaultNotes.Text, txtAccessNotes.Text, appointmentId)
            If updated = 1 Then
                lblFaultNotes.Text = txtFaultNotes.Text
                lblAccessNote.Text = txtAccessNotes.Text
                pnlFaultNotesErrorMessage.Visible = False
                mdlPopupFaultNotes.Hide()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UpdateFaultNotesSuccess, False)
            Else
                lblFaultNotesErrorMessage.Text = UserMessageConstants.AddFaultFailure
                pnlFaultNotesErrorMessage.Visible = True
                mdlPopupFaultNotes.Show()
            End If
        End If

    End Sub

#End Region


#Region "Create Data"

    Sub createData()

        Dim resultDataSet As DataSet = New DataSet()

        Dim resultTable As DataTable = New DataTable()
        resultTable.Columns.Add("FaultLogID")
        resultTable.Columns.Add("FaultId")
        resultTable.Columns.Add("JobSheetNumber")
        resultTable.Columns.Add("AreaName")
        resultTable.Columns.Add("Description")
        resultTable.Columns.Add("Duration")
        resultTable.Columns.Add("Trade")
        resultTable.Columns.Add("ProblemDays")
        resultTable.Columns.Add("RecuringProblem")
        resultTable.Columns.Add("CommunalProblem")
        resultTable.Columns.Add("DueDate")
        resultTable.Columns.Add("Notes")

        resultDataSet.Tables.Add(resultTable)

        Dim dataRow As DataRow = resultDataSet.Tables(0).NewRow()
        dataRow.ItemArray = {112, 870, "JS112", "Living Room", "The radiator is leaking", 3, "Carpenter", 1, 0, 1, "10 May", "major radiator leak in number 45. Due to a mobility scooter crashing into radiator. water gushing out. no electrics wet at the moment. please pass js"}
        resultDataSet.Tables(0).Rows.Add(dataRow)

        Dim dataRow1 As DataRow = resultDataSet.Tables(0).NewRow()
        dataRow1.ItemArray = {179, 870, "JS179", "Living Room", "The radiator is leaking", 3, "Carpenter", 2, 0, 0, "12 May", "concerns that the pressure on boiler dropping due to the leak on radiator."}
        resultDataSet.Tables(0).Rows.Add(dataRow1)

        Dim dataRow2 As DataRow = resultDataSet.Tables(0).NewRow()
        dataRow2.ItemArray = {194, 870, "JS194", "Living Room", "The radiator is leaking", 3, "Carpenter", 3, 0, 0, "13 May", "please note this radiator is brand new and was only fitted last week and it is now leaking. pls contact on 01493 664337. hk"}
        resultDataSet.Tables(0).Rows.Add(dataRow2)

        SessionManager.setSbConfirmedFaults(resultDataSet.Tables(0))

    End Sub

#End Region

#Region "reset SchemeBlock Detail"
    Sub resetSchemeBlockDetail()
        lblScheme.Text = String.Empty
        lblSchemeBlockAddress.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblCOUNTY.Text = String.Empty
    End Sub

#End Region
#End Region

End Class