﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports FLS_BusinessObject

Public Class SchedulersCalanderSearch
    Inherits PageBase
#Region "Attributes"
    Public varWeeklyDT As DataTable = New DataTable()
    Dim objcustomerData As CustomerBO = New CustomerBO()
    Dim ddlArry(3) As String
#End Region

#Region "Properties "
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobSheetNumber", 1, 30)
    Dim totalCount As Integer = 0
#End Region

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMessage.Text = String.Empty
        If (Not IsPostBack) Then
            'pnlCalendar.Visible = False
            pnlGridView.Visible = False
            Me.populateDropDowns()
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

            'Calendar code start here
            If (Not IsNothing(Request.QueryString("mov"))) Then
                If (Request.QueryString("mov") = "n") Then 'And ApplicationConstants.isMoved = False
                    'ApplicationConstants.startDate = ApplicationConstants.endDate
                    ApplicationConstants.startDate = DateAdd("d", 7, ApplicationConstants.startDate)

                    'Start - Changes 20 Jun 2013 - Aamir Waheed
                    'To correct start and end date of scheduling calendar
                    ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
                    'End - Changes 20 Jun 2013 - Aamir Waheed
                ElseIf (Request.QueryString("mov") = "p") Then
                    'ApplicationConstants.endDate = ApplicationConstants.startDate
                    ApplicationConstants.startDate = DateAdd("d", -7, ApplicationConstants.startDate)

                    'Start - Changes 20 Jun 2013 - Aamir Waheed
                    'To correct start and end date of scheduling calendar
                    ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
                    'End - Changes 20 Jun 2013 - Aamir Waheed
                ElseIf (Request.QueryString("mov") = "t") Then
                    ApplicationConstants.startDate = Date.Today

                    'Start - Changes 20 Jun 2013 - Aamir Waheed
                    'To correct start and end date of scheduling calendar
                    ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
                    'End - Changes 20 Jun 2013 - Aamir Waheed
                End If
                pnlCalendar.Visible = True
                Me.restoreDropdownsState()
                Me.displayCalendarScheduleAppointment()
            Else
                ApplicationConstants.startDate = Date.Today

                'Start - Changes 20 Jun 2013 - Aamir Waheed
                'To correct start and end date of scheduling calendar
                ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
                'End - Changes 20 Jun 2013 - Aamir Waheed

                Me.saveDropdownsState()
                'ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            End If

            'Else
            'Me.searchRecord()
        End If
    End Sub
#End Region

#Region "Search result Grid Sorting"

    Protected Sub grdSearchResult_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSearchResult.Sorting
        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdSearchResult.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            populateGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Page Index Changing"

    Protected Sub grdSearchResult_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSearchResult.PageIndexChanging
        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdSearchResult.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            populateGrid()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Patch Drop down Selected Index Event"
    Protected Sub ddlPatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPatch.SelectedIndexChanged
        ' reBindGridForLastPage()

        Me.populateSchemeDropdown()
        Me.saveDropdownsState()
        Me.changeFindButtonState(True)
        validateCalenderView()

    End Sub

#End Region

#Region "Scheme Drop down Selected Index Event"
    Protected Sub ddlScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlScheme.SelectedIndexChanged
        ' reBindGridForLastPage()

        Me.saveDropdownsState()
        validateCalenderView()
    End Sub
#End Region

#Region "Trade Drop down Selected Index Event"
    Protected Sub ddlTrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTrade.SelectedIndexChanged
        ' reBindGridForLastPage()

        Me.saveDropdownsState()
        Me.changeFindButtonState(True)
        validateCalenderView()
    End Sub
#End Region

#Region "Reset controls"
    Sub validateCalenderView()

        If (ddlPatch.SelectedItem.Value = -1 And ddlScheme.SelectedItem.Value = -1 And ddlTrade.SelectedItem.Value = -1) Then
            pnlCalendar.Visible = False
        End If

    End Sub
#End Region

#Region "View Job Sheet detail"

    Protected Sub ViewFault(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim jobSheetNumber As String = CType(CType(sender, Button).CommandArgument, String)
        Me.displayJobSheetInfo(CType(CType(sender, Button).CommandArgument, String))

    End Sub
#End Region

#Region "Find Button"
    Protected Sub btnFind_Click(ByVal seder As Object, ByVal e As EventArgs) Handles btnFind.Click

        ' reBindGridForLastPage()
        Me.searchRecord()

    End Sub
#End Region

#Region "Jsn text changed"
    Protected Sub txtJSN_TextChanged(sender As Object, e As EventArgs) Handles txtJSN.TextChanged
        changeFindButtonState(True)
    End Sub
#End Region

#Region "Jsn text changed"
    Protected Sub txtJsnumber_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtJsnumber.TextChanged
        Dim jobSheetNumber As String = txtJsnumber.Text
        Me.restoreDropdownsState()
        Me.displayJobSheetInfo(jobSheetNumber)
        Me.searchRecord()
        txtJsnumber.Text = String.Empty
    End Sub
#End Region

#Region "Address text changed"
    Protected Sub txtAddress_TextChanged(sender As Object, e As EventArgs) Handles txtAddress.TextChanged
        changeFindButtonState(True)
    End Sub
#End Region

#Region "Tenant text changed"
    Protected Sub txtTenant_TextChanged(sender As Object, e As EventArgs) Handles txtTenant.TextChanged
        changeFindButtonState(True)
    End Sub
#End Region

#Region "Operative text changed"
    Protected Sub txtOperative_TextChanged(sender As Object, e As EventArgs) Handles txtOperative.TextChanged
        changeFindButtonState(True)
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Save Drop down state"
    Sub saveDropdownsState()
        Try
            SessionManager.setDDLData(ddlPatch.SelectedValue + "," + ddlScheme.SelectedValue + "," + ddlTrade.SelectedValue)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Restore Drop down state"
    Sub restoreDropdownsState()
        Dim ddlArry(3) As String
        Dim patchId As Integer
        Dim schemeId As Integer
        Dim tradeId As Integer

        Try

            ddlArry = SessionManager.getDDLData().Split(",")
            patchId = Convert.ToInt32(ddlArry(0))
            schemeId = Convert.ToInt32(ddlArry(1))
            tradeId = Convert.ToInt32(ddlArry(2))

            Me.populateDropDowns()

            ddlPatch.SelectedValue = patchId
            Me.populateSchemeDropdown()
            ddlScheme.SelectedValue = schemeId
            ddlTrade.SelectedValue = tradeId


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate scheme Drop down"
    Sub populateSchemeDropdown()

        Try
            If (ddlPatch.SelectedIndex > 0) Then
                Dim objSchedularSearchBL As SchedularSearchBL = New SchedularSearchBL()
                Dim patchId As Int32 = ddlPatch.SelectedValue
                Dim schemeDataSet As DataSet = New DataSet()

                objSchedularSearchBL.getSchemes(schemeDataSet, patchId)

                ddlScheme.DataSource = schemeDataSet.Tables(0).DefaultView
                ddlScheme.DataValueField = "DEVELOPMENTID"
                ddlScheme.DataTextField = "SCHEMENAME"
                ddlScheme.DataBind()
                Dim schemeItem As ListItem = New ListItem("Please Select", "-1")
                ddlScheme.Items.Insert(0, schemeItem)
            Else
                ddlScheme.Items.Clear()
                Dim schemeItem As ListItem = New ListItem("Please Select", "-1")
                ddlScheme.Items.Insert(0, schemeItem)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Grid"
    Sub populateGrid()
        Try
            Dim objSchedularSearchBL As SchedularSearchBL = New SchedularSearchBL()
            Dim resultDataSet As DataSet = New DataSet()
            Dim objSchedularSearchBO As SchedularSearchBO = New SchedularSearchBO()

            If (Not ddlPatch.SelectedValue = -1) Then
                objSchedularSearchBO.Patch = ddlPatch.SelectedValue
            End If

            If (Not ddlScheme.SelectedValue = -1) Then
                objSchedularSearchBO.Scheme = ddlScheme.SelectedValue
            End If

            If (Not ddlTrade.SelectedValue = -1) Then
                objSchedularSearchBO.Trade = ddlTrade.SelectedValue
            End If

            If (Not txtJSN.Text.Trim.Length = 0) Then
                objSchedularSearchBO.JobSheetNumber = txtJSN.Text
            End If

            If (Not txtOperative.Text.Trim.Length = 0) Then
                objSchedularSearchBO.Operative = txtOperative.Text
            End If

            If (Not txtTenant.Text.Trim.Length = 0) Then
                objSchedularSearchBO.Tenant = txtTenant.Text
            End If

            If (Not txtAddress.Text.Trim.Length = 0) Then
                objSchedularSearchBO.Address = txtAddress.Text
            End If

            Dim totalCount As Integer = objSchedularSearchBL.getSchedularSearchResult(resultDataSet, objSchedularSearchBO, objPageSortBo)

            If (totalCount = 0) Then
                grdSearchResult.VirtualItemCount = totalCount
                grdSearchResult.DataSource = resultDataSet
                grdSearchResult.DataBind()
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            Else
                grdSearchResult.VirtualItemCount = totalCount
                grdSearchResult.DataSource = resultDataSet
                grdSearchResult.DataBind()
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Display Search Results On Calendar"

    Protected Sub displayCalendarScheduleAppointment()
        Try
            Dim divCounter As Integer
            Dim objCalendarBL As CalendarBL = New CalendarBL()
            Dim objFaultBL As FaultBL = New FaultBL()
            Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
            Dim objCalendarAppointmentsBO As CalendarAppointmentsBO = New CalendarAppointmentsBO()

            Dim dsEmployeeList As DataSet = New DataSet()
            Dim dsAppointments As DataSet = New DataSet()
            Dim dsEmployeeLeaves As DataSet = New DataSet()
            Dim dsEmployeeCoreWorkingHours As DataSet = New DataSet()

            Dim drLeaves() As DataRow
            Dim drAppointmentJSN() As DataRow

            Dim strAppointment As StringBuilder = New StringBuilder()
            Dim strEmployeeIds As StringBuilder = New StringBuilder()
            Dim rowCount As Integer


            'Seek for Monday as starting and Sunday ending day of the Month
            If (ApplicationConstants.startDate.ToString("dddd") <> "Monday") Then
                objCalendarUtilities.calculateMonday()
            End If
            If (Request.QueryString("e") <> "p") Then
                'Start - Changes 20 Jun 2013 - Aamir Waheed
                'To correct start and end date of scheduling calendar
                ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
                'End - Changes 20 Jun 2013 - Aamir Waheed
                'ApplicationConstants.endDate = DateAdd("d", 7, ApplicationConstants.startDate)
            End If

            objCalendarAppointmentsBO.PatchId = ddlPatch.SelectedItem.Value
            objCalendarAppointmentsBO.SchemeId = ddlScheme.SelectedItem.Value
            objCalendarAppointmentsBO.TradeId = ddlTrade.SelectedItem.Value

            objCalendarBL.getSearchedFaultEngineersForAppointment(objCalendarAppointmentsBO, dsEmployeeList)
            'Sending Start and End date
            objCalendarAppointmentsBO.StartDate = ApplicationConstants.startDate
            objCalendarAppointmentsBO.EndDate = ApplicationConstants.endDate
            objCalendarBL.getFaultAppointmentsCalendarDetail(objCalendarAppointmentsBO, dsAppointments)
            ''End If
            If (dsEmployeeList.Tables(0).Rows.Count > 0) Then 'If Gas Engineer exist
                'Creating Columns based on Engineers names
                varWeeklyDT.Columns.Add(" ")
                For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                    objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
                    strEmployeeIds.Append(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ",")
                Next

                'Getting Employees Leaves Information
                objCalendarAppointmentsBO.LeaveStartDate = ApplicationConstants.startDate
                objCalendarAppointmentsBO.ReturnDate = ApplicationConstants.endDate
                objCalendarAppointmentsBO.EmpolyeeIds = strEmployeeIds.ToString().TrimEnd(",")
                objCalendarBL.getEngineerLeavesDetail(objCalendarAppointmentsBO, dsEmployeeLeaves)
                objCalendarBL.getEmployeeCoreWorkingHours(dsEmployeeCoreWorkingHours, strEmployeeIds.ToString().TrimEnd(","))
                'End Getting Employees Leaves Information

                'Getting Appointments Information in Datarow for Data table {varWeeklyDT}
                Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
                'Drawing Weekly Calendar
                'If (dr.Count > 0) Then 'Check if record exist
                For dayCount = 0 To 6
                    scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                    rowCount = 1
                    For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                        'Adding Leaves Information
                        drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                        If (drLeaves.Count > 0) Then
                            For leaveCounter As Integer = 0 To drLeaves.Count - 1
                                strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><table><tr><td><img src='../../images/arrow_right.png' border=0></td><td><span style='margin-right:10px;'>" + drLeaves(leaveCounter).Item(1).ToString() + "</span></td></tr></table></div><br/>")
                            Next
                        End If
                        For rdCount = 0 To dsAppointments.Tables(0).Rows.Count - 1

                            'Check the appointments of the engineers in Datarow
                            Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString())
                            Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(15).ToString())
                            Dim currentDate As DateTime = DateAdd("d", dayCount, ApplicationConstants.startDate)
                            Dim operativeId As Integer = dsEmployeeList.Tables(0).Rows(dsColCount).Item(0)
                            Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + operativeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")

                            If (currentDate >= aptStartDate And currentDate <= aptEndDate _
                                And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0) = dsAppointments.Tables(0).Rows(rdCount).Item(1) _
                                And dr.Count > 0) Then

                                If (Not IsNothing(Request.QueryString("apt")) And Request.QueryString("apt") = dsAppointments.Tables(0).Rows(rdCount).Item(8)) Then
                                    strAppointment.Append("<div class='main-box-current'><div class='box-cover'><div class='box-column1'><a href='javascript:void(0)' id='arrow-left" + divCounter.ToString() + "' onclick='displayMore(" + divCounter.ToString() + ")'> <img src='../../images/arrow_right.png' alt='' border=0 /></a>")
                                Else
                                    strAppointment.Append("<div class='main-box'><div class='box-cover'><div class='box-column1'><a href='javascript:void(0)' id='arrow-left" + divCounter.ToString() + "' onclick='displayMore(" + divCounter.ToString() + ")'> <img src='../../images/arrow_right.png' alt='' border=0 /></a>")
                                End If
                                strAppointment.Append("<a href='javascript:void(0)' id='arrow-down" + divCounter.ToString() + "' style='display:none;' onclick='displayMoreHide(" + divCounter.ToString() + ");'> <img src='../../images/down_arrow.gif' alt='' border=0 /></a></div><div class='box-column2'>" + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + "</div>")
                                'Adding Status Icons
                                If dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "In Progress" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/inprogress.png'></div>")
                                ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Paused" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/paused.png'></div>")
                                ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Appointment Arranged" Or dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Arranged" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/arranged.png'></div>")
                                ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Complete" Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../images/completed.png'></div>")
                                End If

                                ' Checking the appointment was scheduled from Calendar or not 
                                ' If appointment was scheduled from calendar then show icon.
                                Dim isCalendarApt As Boolean = False
                                If (Not IsDBNull(dsAppointments.Tables(0).Rows(rdCount)("IsCalendarAppointment"))) Then
                                    isCalendarApt = dsAppointments.Tables(0).Rows(rdCount)("IsCalendarAppointment")
                                End If

                                If (isCalendarApt) Then
                                    strAppointment.Append("<div class='box-column3'><img src='../../Images/calendarFlag.png'></div></div>")
                                Else
                                    strAppointment.Append("</div>")
                                End If

                                'Adding Address for collapsed view
                                If (dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() <> "") Then
                                    'strAppointment.Append("<div id='addressDiv" + divCounter.ToString() + "' style='display:block;'><div class='box2-column2' style='padding:5px 5px 5px 35px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + "</div></div>")
                                    strAppointment.Append("<div id='addressDiv" + divCounter.ToString() + "' style='display:block;'><div class='box2-column2' style='padding:5px 5px 5px 35px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + "</div></div>")
                                End If

                                'Adding the div to to contain appointment detail like customer name, Mobile and Job Sheets number and description.
                                strAppointment.Append("<div id='display" + divCounter.ToString() + "' style='display:none;'>")

                                'Adding Address
                                If (dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() <> "") Then
                                    strAppointment.Append("<div class='box2-column2' style='padding:5px 5px 5px 35px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item("CustAddress").ToString() + "</div>")
                                End If
                                'Adding Job Sheet Numbers
                                drAppointmentJSN = dsAppointments.Tables(1).Select("AppointmentId=" + dsAppointments.Tables(0).Rows(rdCount).Item(8).ToString())
                                If (drAppointmentJSN.Count > 0) Then
                                    strAppointment.Append("<div class='inner-box'><div class='box-cover-des-inner-box'>")


                                    For i = 0 To drAppointmentJSN.Count - 1
                                        Dim jsnClick As String = String.Empty
                                        Dim apptType As String = drAppointmentJSN(i).Item(ApplicationConstants.AppointmentTypeColumn)
                                        Dim voidAppointments As String() = [Enum].GetNames(GetType(VoidAppointmentTypes))
                                        If voidAppointments.Contains(apptType.Replace(" ", "")) Then
                                            jsnClick = String.Empty
                                            strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'><a href='javascript:void(0)'" + jsnClick + "><u>" + drAppointmentJSN(i).Item(2).ToString() + "<br/>" + apptType + "</u></a></div>")

                                        Else
                                            Dim apttype As String = drAppointmentJSN(i).Item(ApplicationConstants.AppointmentTypeColumn).ToString()
                                            If (apttype.Equals(ApplicationConstants.PlannedAppointment) _
                                                Or apttype.Equals(ApplicationConstants.CyclicMaintenanceAppointment) _
                                                Or apttype.Equals(ApplicationConstants.MEServicingAppointment)) Then
                                                jsnClick = String.Empty
                                                strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'><a href='#'" + jsnClick + "><u>" + drAppointmentJSN(i).Item(2).ToString() + "</u></a></div>")
                                            Else
                                                jsnClick = " OnClick=setJsn('" + drAppointmentJSN(i).Item(2).ToString() + "') return false;"
                                                strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'><a href='javascript:void(0)'" + jsnClick + "><u>" + drAppointmentJSN(i).Item(2).ToString() + "</u></a></div>")
                                            End If


                                        End If

                                        strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>" + drAppointmentJSN(i).Item(3).ToString() + "</div>")
                                    Next
                                    strAppointment.Append("</div></div>")
                                End If

                                'Closing the div contain job sheet numbers.
                                strAppointment.Append("</div>")

                                'Closing Main box and adding a line break to seprate it from next appointment of day.
                                strAppointment.Append("</div><br/>")

                                strAppointment.Append("<div style='clear:both'></div>")
                                divCounter = divCounter + 1
                            End If

                        Next
                        'End If
                        If (strAppointment.ToString() <> "") Then
                            ' strAppointment.Append(DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy"))
                            scheduledAppt(rowCount) = strAppointment.ToString()
                            strAppointment.Clear()
                        Else
                            scheduledAppt(rowCount) = "" 'DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy") '""
                        End If
                        rowCount = rowCount + 1
                    Next

                    varWeeklyDT.Rows.Add(scheduledAppt)

                Next
                lblMonth.Text = ApplicationConstants.startDate.Day.ToString() + "-" + DateAdd("d", 6, ApplicationConstants.startDate).Day.ToString() + " " + MonthName(ApplicationConstants.startDate.Month) + " " + Year(ApplicationConstants.startDate).ToString()
            Else
                pnlCalendar.Visible = False
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EngNotFound, True)
            End If
            'ApplicationConstants.isMoved = True
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'lbNextFault.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Get Appointment Start and End Time"

    Function getAppointmentStartEndTime(ByVal dsAppointments As DataSet, ByVal dsEmployeeCoreWorkingHours As DataSet, ByVal currentDate As DateTime, ByVal rdCount As Integer, ByVal employeeId As Integer)

        Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString())
        Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(15).ToString())
        Dim aptStartTime As String = String.Empty
        Dim aptEndTime As String = String.Empty
        Dim empCoreStartTime As String = String.Empty
        Dim empCoreEndTime As String = String.Empty
        Dim dayStartHour As Double = GeneralHelper.getDayStartHour()
        Dim dayEndHour As Double = GeneralHelper.getDayEndHour()
        Dim ofcCoreStartTime As String = DateTime.Parse(Convert.ToInt32(dayStartHour - dayStartHour Mod 1).ToString() + ":" + IIf(dayStartHour Mod 1 = 0, "00", Convert.ToInt32((dayStartHour Mod 1) * 60).ToString())).ToString("HH:mm")
        Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + IIf(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString())).ToString("HH:mm")


        'Get employee core working hour
        Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + employeeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")
        If (dr.Count > 0) Then
            empCoreStartTime = DateTime.Parse(dr(0).Item(3).ToString()).ToString("HH:mm")
            empCoreEndTime = DateTime.Parse(dr(0).Item(4).ToString()).ToString("HH:mm")
        Else
            empCoreStartTime = ofcCoreStartTime
            empCoreEndTime = ofcCoreEndTime
        End If

        'Setting start and end time depending on cases
        'Case 1 : Single day appointment start time and end time exist in same day
        'Case 2 : Multiple day appointment where start time exist in current date
        'Case 3 : Multiple day appointment where end time exist in current date
        'Case 4 : Multiple day appointment where start and end time do not exist in current date

        If (aptStartDate = aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(2)).ToString("HH:mm")
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        ElseIf (currentDate = aptStartDate And currentDate <> aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(2)).ToString("HH:mm")
            aptEndTime = empCoreEndTime
        ElseIf (currentDate <> aptStartDate And currentDate = aptEndDate) Then
            aptStartTime = empCoreStartTime
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        Else
            aptStartTime = empCoreStartTime
            aptEndTime = empCoreEndTime
        End If

        Return aptStartTime + " - " + aptEndTime

    End Function

#End Region

#Region "Display Job Sheet Details"
    Private Sub displayJobSheetInfo(ByVal jobSheetNumber As String)


        Try
            Me.resetJobsheetControls()

            Dim resultDataSet As DataSet = New DataSet()
            Dim objFaultsBl As FaultBL = New FaultBL()

            objFaultsBl.getJobSheetDetails(resultDataSet, jobSheetNumber)


            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                grdAsbestos.Visible = False
            Else
                grdAsbestos.Visible = True
                grdAsbestos.DataSource = resultDataSet.Tables("Asbestos")
                grdAsbestos.DataBind()

                lblFaultId.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(0).ToString
                lblContractor.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(1).ToString
                lblOperativeName.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(2).ToString
                lblFaultPriority.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(3).ToString
                lblFaultCompletionDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(4).ToString
                lblOrderDate.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(5).ToString
                lblFaultLocation.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(6).ToString
                lblFaultDescription.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(7).ToString
                lblFaultOperatorNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(8).ToString
                lblFaultAppointmentDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(9).ToString
                txtAppointmentNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(10).ToString
                lblTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(11).ToString
                lblTrade.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(12).ToString
                lblFaultStatus.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(13).ToString

            End If


            If (resultDataSet.Tables("Customer").Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                grdAsbestos.Visible = False
            Else

                ' Header Customer Labels
                lblClientNameHeader.Text = resultDataSet.Tables("Customer").Rows(0)(0).ToString
                lblClientStreetAddressHeader.Text = resultDataSet.Tables("Customer").Rows(0)(1).ToString
                lblClientCityHeader.Text = resultDataSet.Tables("Customer").Rows(0)(2).ToString
                lblClientPostalcodeHeader.Text = resultDataSet.Tables("Customer").Rows(0)(3).ToString
                lblClientTelPhoneNumberHeader.Text = resultDataSet.Tables("Customer").Rows(0)(5).ToString

                ' Body customer Labels

                lblClientName.Text = resultDataSet.Tables("Customer").Rows(0)(0).ToString
                lblClientStreetAddress.Text = resultDataSet.Tables("Customer").Rows(0)(1).ToString
                lblClientCity.Text = resultDataSet.Tables("Customer").Rows(0)(2).ToString
                lblClientPostCode.Text = resultDataSet.Tables("Customer").Rows(0)(3).ToString
                lblClientRegion.Text = resultDataSet.Tables("Customer").Rows(0)(4).ToString
                lblClientTelPhoneNumber.Text = resultDataSet.Tables("Customer").Rows(0)(5).ToString
                lblClientMobileNumber.Text = resultDataSet.Tables("Customer").Rows(0)(6).ToString
                lblClientEmailId.Text = resultDataSet.Tables("Customer").Rows(0)(7).ToString

            End If
            ' reBindGridForLastPage()

            Me.popupJobSheet.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Populate DropDowns"
    Sub populateDropDowns()
        Try
            Dim objSchedularSearchBL As SchedularSearchBL = New SchedularSearchBL()
            Dim patchDataSet As DataSet = New DataSet()
            Dim tradeDataSet As DataSet = New DataSet()
            Dim defaultItem As ListItem
            Dim ddlSubArry(2) As String
            'Patch Dropdown List
            If (ddlPatch.Items.Count <= 0) Then

                objSchedularSearchBL.getPatchesAndTrades(patchDataSet, tradeDataSet)
                ddlPatch.DataSource = patchDataSet.Tables(0).DefaultView
                ddlPatch.DataValueField = "PATCHID"
                ddlPatch.DataTextField = "LOCATION"
                ddlPatch.DataBind()
                'If (ddlPatch.Items.Count <= 0) Then
                defaultItem = New ListItem("Please Select", "-1")
                'Else
                '    defaultItem = New ListItem(ddlArry(0))
                'End If

                ddlPatch.Items.Insert(0, defaultItem)
            End If
            'Scheme Dropdown List
            If (ddlScheme.Items.Count <= 0) Then
                'If (IsNothing(ViewState("ddlData"))) Then
                defaultItem = New ListItem("Please Select", "-1")
                'Else
                '    defaultItem = New ListItem(ddlArry(1))
                'End If
                ddlScheme.Items.Insert(0, defaultItem)
            End If
            'Trade Dropdown List
            If (ddlTrade.Items.Count <= 0) Then
                ddlTrade.DataSource = tradeDataSet.Tables(0).DefaultView
                ddlTrade.DataValueField = "TradeId"
                ddlTrade.DataTextField = "Description"
                ddlTrade.DataBind()
                'If (IsNothing(ViewState("ddlData"))) Then
                defaultItem = New ListItem("Please Select", "-1")
                'Else
                '    defaultItem = New ListItem(ddlArry(2))
                'End If
                ddlTrade.Items.Insert(0, defaultItem)
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Change find button state"
    Sub changeFindButtonState(ByVal chkState As Boolean)

        'If (isDropdownsStateValid() Or isTextBoxesStateValid()) Then
        '    btnFind.Enabled = True
        'Else
        btnFind.Enabled = chkState
        'End If

    End Sub
#End Region

#Region "Validate dropdowns state"
    Function isDropdownsStateValid() As Boolean
        If (ddlPatch.SelectedIndex > 0 And ddlTrade.SelectedIndex > 0) Then
            Return True
        End If
        Return False
    End Function
#End Region

#Region "Validate text boxes state"
    Function isTextBoxesStateValid() As Boolean
        If (Not String.IsNullOrWhiteSpace(txtJSN.Text) Or Not String.IsNullOrEmpty(txtJSN.Text) Or Not String.IsNullOrWhiteSpace(txtAddress.Text) Or Not String.IsNullOrEmpty(txtAddress.Text) Or Not String.IsNullOrWhiteSpace(txtOperative.Text) Or Not String.IsNullOrEmpty(txtOperative.Text) Or Not String.IsNullOrWhiteSpace(txtTenant.Text) Or Not String.IsNullOrEmpty(txtTenant.Text)) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Search Record"
    Protected Sub searchRecord()

        If (txtAddress.Text.Trim.Length = 0 And txtJSN.Text.Trim.Length = 0 And txtOperative.Text.Trim.Length = 0 And txtTenant.Text.Length = 0 And ddlPatch.SelectedItem.Value = -1 And ddlScheme.SelectedItem.Value = -1 And ddlTrade.SelectedItem.Value = -1) Then
            pnlGridView.Visible = False
            pnlCalendar.Visible = False
        End If

        'Display Reports
        If (txtJSN.Text <> "" Or txtAddress.Text <> "" Or txtTenant.Text <> "" Or txtOperative.Text <> "" Or txtAddress.Text <> "") Then
            pnlGridView.Visible = True
            pnlCalendar.Visible = False
            Me.populateGrid()
        ElseIf ((ddlPatch.SelectedItem.Value <> -1 And ddlScheme.SelectedItem.Value <> -1) Or ddlTrade.SelectedItem.Value <> -1) Then
            pnlCalendar.Visible = True
            pnlGridView.Visible = False
            Me.displayCalendarScheduleAppointment()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidInput, True)
        End If

    End Sub
#End Region

#Region "Reset Job sheet summary"

    Sub resetJobsheetControls()

        ' reBindGridForLastPage()

        ' Job Sheet Detail
        lblFaultId.Text = String.Empty
        lblContractor.Text = String.Empty
        lblOperativeName.Text = String.Empty
        lblFaultPriority.Text = String.Empty
        lblFaultCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblFaultLocation.Text = String.Empty
        lblFaultDescription.Text = String.Empty
        lblFaultOperatorNote.Text = String.Empty
        lblTrade.Text = String.Empty
        lblFaultStatus.Text = String.Empty

        ' Header Customer Labels
        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        ' Body customer Labels

        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmailId.Text = String.Empty
        grdAsbestos.DataSource = Nothing

        lblTime.Text = String.Empty
        lblFaultAppointmentDateTime.Text = String.Empty
        txtAppointmentNote.Text = String.Empty
    End Sub
#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        'Rebind the Grid to prevent show empty lines for last page only
        'Start - Changes By Aamir Waheed on April 11 2013

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdSearchResult.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdSearchResult.PageCount Then
            grdSearchResult.VirtualItemCount = getVirtualItemCountViewState()
            grdSearchResult.PageIndex = objPageSortBo.PageNumber - 1
            grdSearchResult.DataSource = getResultDataSetViewState()
            grdSearchResult.DataBind()
        End If

        'End - Changes By Aamir Waheed on April 11 2013

    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region

End Class
Enum VoidAppointmentTypes
    VoidInspection
    PostVoidInspection
    VoidWorks
    VoidGasCheck
    VoidElectricCheck
End Enum

