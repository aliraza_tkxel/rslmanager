﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/Fault.Master"
    CodeBehind="SchedularSearch.aspx.vb" Inherits="FaultScheduling.SchedularSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelRecallDetails" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <table id="more_details_table_1" width="100%" 
                style="table-layout: auto; border-collapse: separate; empty-cells: show">
                <tr>
                    <td colspan="7">
                        <strong>Scheduling :</strong>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp; Patch :</td>
                    <td class="style1">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlPatch" runat="server">
                        </asp:DropDownList>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp; JSN :</td>
                    <td>
                        <asp:TextBox ID="txtJsn" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Tenant :
                    </td>
                    <td>
                        <asp:TextBox ID="txtTenant" runat="server"></asp:TextBox>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;Scheme :</td>
                    <td class="style1">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="ddlScheme" runat="server">
                        </asp:DropDownList>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;Operative :
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtOperative" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        Address :</td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp; Trade :</td>
                    <td class="style1">
                        &nbsp;
                        <asp:DropDownList ID="ddlTrade" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;<asp:Button ID="btnFind" runat="server" Text="Find" />
&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td class="style1">
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
