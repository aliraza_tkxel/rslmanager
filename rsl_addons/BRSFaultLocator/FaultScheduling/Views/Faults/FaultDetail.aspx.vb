﻿Imports FLS_BusinessObject
Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class FaultDetail
    Inherits PageBase

#Region "Local Variables Declaration"
    'Used to create Lookup List of Duration (Double as Value Field and String as Text Field)
    Public Class Duration
        Public Property LookUpValue() As Double
        Public Property LookUpName() As String
    End Class
#End Region

#Region "Events"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then

                If IsNothing(SessionManager.getFaultBo()) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidFaultData, True)
                    disableControls()
                Else
                    populateAllDropdownLists()
                    populateDefaults()
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Add Trade button "
    Protected Sub btnAddTrade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTrade.Click
        Try
            If validateAddTrade() Then
                addFaultTrade()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Add Repair button "
    Protected Sub btnAddRepair_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddRepair.Click
        Try
            If validateAddRepair() Then
                addFaultRepair()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Remove trade button "
    Protected Sub btnRemoveTrade_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnRemoveTrade As Button = DirectCast(sender, Button)
            Dim tradeId As Integer = CType(btnRemoveTrade.CommandArgument, Integer)
            removeTrade(tradeId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Remove repair button "
    Protected Sub btnRemoveRepair_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnRemoveRepair As Button = DirectCast(sender, Button)
            Dim repairId As Integer = CType(btnRemoveRepair.CommandArgument, Integer)
            removeRepair(repairId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Save Changes button "
    Protected Sub btnSaveChanges_OnClick(ByVal sender As Object, ByVal e As EventArgs)
        Try

            If validateFaultInfo() Then
                saveFaultInfo()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Back button click event"
    Protected Sub btnBack_OnClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Try
            navigateToFaultManagementReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Net Text Changes Event"
    Protected Sub txtNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNet.TextChanged
        Try
            populateVatValues()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Vat dropdown Index Changed"

    Protected Sub ddlVatRate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlVatRate.SelectedIndexChanged
        Try
            populateVatValues()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Trade Grid Row Data bound"
    Protected Sub grdTrades_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTrades.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
                Dim btnRemoveTrade As Button = DirectCast(e.Row.FindControl("btnRemoveTrade"), Button)
                If objFaultBo.IsFlagStatus = True And objFaultBo.IsNewFault = True Then
                    btnRemoveTrade.Enabled = False
                Else
                    btnRemoveTrade.Enabled = True
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Repair Grid Row Data bound"
    Protected Sub grdFaultRepairs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdFaultRepairs.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
                Dim btnRemoveRepair As Button = DirectCast(e.Row.FindControl("btnRemoveRepair"), Button)
                If objFaultBo.IsFlagStatus = True And objFaultBo.IsNewFault = True Then
                    btnRemoveRepair.Enabled = False
                Else
                    btnRemoveRepair.Enabled = True
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Navigate To Fault Management Report"

    Private Sub navigateToFaultManagementReport()
        Response.Redirect(PathConstants.FaultManagementReportPath)
    End Sub

#End Region

#Region "Populate Fault Info"

    Private Sub populateFaultInfo()

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo
        Dim objFaultManagementBL As New FaultManagementBL
        objFaultManagementBL.GetFaultValues(objFaultBo)
        SessionManager.setFaultBo(objFaultBo)

        lblSelectedFault.Text = objFaultBo.Description
        txtDescription.Text = objFaultBo.Description
        ddlPriority.SelectedValue = objFaultBo.PriorityID.ToString()
        ddlDuration.SelectedValue = objFaultBo.Duration.ToString()
        ddlVatRate.SelectedValue = objFaultBo.VatRateID.ToString()

        If Not IsNothing(objFaultBo.Trades) Then
            grdTrades.DataSource = objFaultBo.Trades
            grdTrades.DataBind()
        End If

        chkBoxRecharge.Checked = objFaultBo.Recharge
        chkBoxSubContractor.Checked = objFaultBo.IsContractor
        chkBoxFaultActive.Checked = objFaultBo.FaultActive
        chkTenantsOnline.Checked = objFaultBo.TenantsOnline
        
        chkBoxGasSafe.Checked = objFaultBo.isGasSafe
        chkBoxOFTEC.Checked = objFaultBo.isOFTEC

        txtNet.Text = objFaultBo.NetCost
        txtVat.Text = objFaultBo.Vat
        txtGross.Text = objFaultBo.Gross

        If objFaultBo.IsFlagStatus = True And objFaultBo.IsNewFault = True Then
            disableControls()
        End If
        txtSearch.Text = String.Empty
        hdnSelectedRepairId.Value = String.Empty
        hdnSelectedRepairDescription.Value = String.Empty
        populateFaultRepairs()

    End Sub

#End Region

#Region "Save Fault Info"

    Private Sub saveFaultInfo()

        Dim objFaultManagementBL As FaultManagementBL = New FaultManagementBL()
        Dim objFaultBo As FaultBO = SessionManager.getFaultBo

        objFaultBo.Description = txtDescription.Text.Trim()
        objFaultBo.PriorityID = CType(ddlPriority.SelectedValue, Int32)
        objFaultBo.Duration = CType(ddlDuration.SelectedValue, Double)
        objFaultBo.Recharge = chkBoxRecharge.Checked

        objFaultBo.NetCost = IIf(IsNothing(txtNet.Text), Nothing, CType(txtNet.Text, Double))
        objFaultBo.VatRateID = IIf(ddlVatRate.SelectedValue = ApplicationConstants.DefaultValue, Nothing, CType(ddlVatRate.SelectedValue, Int32))
        objFaultBo.Vat = IIf(IsNothing(txtVat.Text), Nothing, CType(txtVat.Text, Double))
        objFaultBo.Gross = IIf(IsNothing(txtGross.Text), Nothing, CType(txtGross.Text, Double))

        objFaultBo.IsContractor = chkBoxSubContractor.Checked
        objFaultBo.FaultActive = chkBoxFaultActive.Checked
        objFaultBo.TenantsOnline = chkTenantsOnline.Checked
        
        objFaultBo.isGasSafe = chkBoxGasSafe.Checked
        objFaultBo.isOFTEC = chkBoxOFTEC.Checked

        objFaultBo.SubmitDate = CalendarUtilities.FormatDate(System.DateTime.Now.ToShortDateString())
        objFaultBo.UserID = SessionManager.getUserEmployeeId()
        objFaultManagementBL.saveFaultInfo(objFaultBo)
        SessionManager.setFaultBo(objFaultBo)

        If objFaultBo.IsFlagStatus = True Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AddFaultSuccess, False)
            populateFaultInfo()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AddFaultFailure, True)
        End If

    End Sub

#End Region

#Region "Validate Fault Info"

    Private Function validateFaultInfo()
        Dim isValid As Boolean = True
        Dim numericReg As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        Dim allowedCharactersReg As Regex = New Regex("[^-A-Za-z0-9 _./,()£&]")

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim dtFaultRepair As DataTable = New DataTable
        dtFaultRepair = objFaultBo.FaultRepairs

        Dim deletedRows As DataRow() = dtFaultRepair.Select(String.Format("{0} = 1", ApplicationConstants.isRepairDeletedColumn))

        If txtDescription.Text.Trim.Equals(String.Empty) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EnterFaultDescription, True)
            isValid = False
        ElseIf allowedCharactersReg.IsMatch(txtDescription.Text) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SpecialCharacterNotAllowed, True)
            isValid = False
        ElseIf ddlPriority.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectFaultPriority, True)
            isValid = False
        ElseIf ddlDuration.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectFaultDuration, True)
            isValid = False
        ElseIf ddlVatRate.SelectedValue = ApplicationConstants.DropDownDefaultValue Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectVatRate, True)
            isValid = False
        ElseIf Not (numericReg.IsMatch(txtNet.Text)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EnterNetAmount, True)
            isValid = False
        ElseIf Not (numericReg.IsMatch(txtVat.Text)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EnterVatAmount, True)
            isValid = False
        ElseIf Not (numericReg.IsMatch(txtGross.Text)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EnterGrossAmount, True)
            isValid = False        
        End If
        Return isValid

    End Function

#End Region

#Region "Add Trade to Trade Grid"

    Private Sub addFaultTrade()

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim selectedTradeId As Int32 = Convert.ToInt32(ddlTrade.SelectedValue)
        Dim dtFaultTrade As DataTable = New DataTable
        dtFaultTrade = objFaultBo.Trades
        dtFaultTrade.Columns(ApplicationConstants.isDeletedColumn).ReadOnly = False
        Dim selectedTradeRow As DataRow = dtFaultTrade.Select(String.Format("{0} = {1}", ApplicationConstants.tradeIdColumn, selectedTradeId.ToString())).FirstOrDefault()

        If (IsNothing(selectedTradeRow)) Then
            Dim drFaultTrade = dtFaultTrade.NewRow
            drFaultTrade(ApplicationConstants.tradeNameColumn) = ddlTrade.SelectedItem.Text
            drFaultTrade(ApplicationConstants.tradeIdColumn) = selectedTradeId
            drFaultTrade(ApplicationConstants.faultTradeIdColumn) = ApplicationConstants.DefaultValue
            drFaultTrade(ApplicationConstants.isDeletedColumn) = 0
            dtFaultTrade.Rows.Add(drFaultTrade)
        ElseIf (Convert.ToBoolean(selectedTradeRow(ApplicationConstants.isDeletedColumn)) = True) Then
            selectedTradeRow(ApplicationConstants.isDeletedColumn) = False
        End If

        ddlTrade.SelectedIndex = 0
        objFaultBo.Trades = dtFaultTrade
        SessionManager.setFaultBo(objFaultBo)
        populateFaultTrades()
    End Sub

#End Region

#Region "Remove Trade from Trade Grid"

    Sub removeTrade(ByVal tradeId As Integer)

        Dim objFaultManagementBL As New FaultManagementBL()
        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim dtFaultTrades As DataTable = objFaultBo.Trades
        dtFaultTrades.Columns(ApplicationConstants.isDeletedColumn).ReadOnly = False
        Dim filteredRows As DataRow = dtFaultTrades.Select(String.Format("{0} = {1}", ApplicationConstants.tradeIdColumn, tradeId.ToString())).FirstOrDefault()
        Dim faultTradeId As Integer = Convert.ToInt32(filteredRows(ApplicationConstants.faultTradeIdColumn))

        If faultTradeId <= 0 Then
            dtFaultTrades.Rows.Remove(filteredRows)
        ElseIf objFaultManagementBL.checkFaultTradeInUse(faultTradeId) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.TradeDeletionError, True)
        Else
            filteredRows(ApplicationConstants.isDeletedColumn) = True
        End If

        objFaultBo.Trades = dtFaultTrades
        SessionManager.setFaultBo(objFaultBo)
        populateFaultTrades()

    End Sub

#End Region

#Region "Validate Add Trade"

    Private Function validateAddTrade()

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim selectedTradeId As Int32 = Convert.ToInt32(ddlTrade.SelectedValue)
        Dim isValid As Boolean = True

        If selectedTradeId = ApplicationConstants.DefaultValue Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectTrade, True)
            isValid = False
        Else
            Dim dtFaultTrade As DataTable = New DataTable
            dtFaultTrade = objFaultBo.Trades
            Dim selectedTradeRow As DataRow = dtFaultTrade.Select(String.Format("{0} = {1}", ApplicationConstants.tradeIdColumn, selectedTradeId.ToString())).FirstOrDefault()

            If (Not IsNothing(selectedTradeRow)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.TradeAlreadyAdded, True)
                isValid = False
            End If
        End If

        Return isValid
    End Function

#End Region

#Region "Add Repair to Repair Grid"

    Private Sub addFaultRepair()

        Dim selectedRepairId As Int32 = Convert.ToInt32(hdnSelectedRepairId.Value)

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim dtFaultRepair As DataTable = New DataTable
        dtFaultRepair = objFaultBo.FaultRepairs
        dtFaultRepair.Columns(ApplicationConstants.isRepairDeletedColumn).ReadOnly = False
        Dim filteredRow As DataRow = dtFaultRepair.Select(String.Format("{0} = {1}" _
                                                                                     , ApplicationConstants.repairIdColumn _
                                                                                     , selectedRepairId.ToString())).FirstOrDefault()

        If (IsNothing(filteredRow)) Then
            Dim drFaultRepair = dtFaultRepair.NewRow
            drFaultRepair(ApplicationConstants.faultRepairIdColumn) = ApplicationConstants.DefaultValue
            drFaultRepair(ApplicationConstants.repairIdColumn) = Convert.ToInt32(hdnSelectedRepairId.Value)
            drFaultRepair(ApplicationConstants.repairColumn) = hdnSelectedRepairDescription.Value
            drFaultRepair(ApplicationConstants.isRepairDeletedColumn) = False
            dtFaultRepair.Rows.Add(drFaultRepair)
        ElseIf (Convert.ToBoolean(filteredRow(ApplicationConstants.isRepairDeletedColumn)) = True) Then
            filteredRow(ApplicationConstants.isRepairDeletedColumn) = False
        End If
        txtSearch.Text = String.Empty
        hdnSelectedRepairDescription.Value = String.Empty
        hdnSelectedRepairId.Value = String.Empty
        objFaultBo.FaultRepairs = dtFaultRepair
        SessionManager.setFaultBo(objFaultBo)
        populateFaultRepairs()

    End Sub

#End Region

#Region "Remove Repair from Repair Grid"

    Sub removeRepair(ByVal repairId As Integer)

        Dim objFaultManagementBL As New FaultManagementBL()
        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim dtFaultRepairs As DataTable = objFaultBo.FaultRepairs
        dtFaultRepairs.Columns(ApplicationConstants.isRepairDeletedColumn).ReadOnly = False
        Dim filteredRow As DataRow = dtFaultRepairs.Select(String.Format("{0} = {1}" _
                                                                             , ApplicationConstants.repairIdColumn _
                                                                             , repairId.ToString())).FirstOrDefault()
        Dim faultRepairId As Integer = Convert.ToInt32(filteredRow(ApplicationConstants.faultRepairIdColumn))

        If faultRepairId <= 0 Then
            dtFaultRepairs.Rows.Remove(filteredRow)
        Else
            filteredRow(ApplicationConstants.isRepairDeletedColumn) = True
        End If

        objFaultBo.FaultRepairs = dtFaultRepairs
        SessionManager.setFaultBo(objFaultBo)
        populateFaultRepairs()

    End Sub

#End Region

#Region "Validate Add Repair"

    Private Function validateAddRepair()


        Dim isValid As Boolean = True

        If String.IsNullOrEmpty(hdnSelectedRepairId.Value) Or String.IsNullOrEmpty(txtSearch.Text) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectRepair, True)
            isValid = False
        ElseIf Not hdnSelectedRepairDescription.Value.Trim.Equals(txtSearch.Text.Trim) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidRepair, True)
            isValid = False
        Else
            Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
            Dim selectedRepairId As Int32 = Convert.ToInt32(hdnSelectedRepairId.Value)
            Dim dtFaultRepair As DataTable = New DataTable
            dtFaultRepair = objFaultBo.FaultRepairs

            Dim filteredRow As DataRow = dtFaultRepair.Select(String.Format("{0} = {1}", ApplicationConstants.repairIdColumn, selectedRepairId.ToString())).FirstOrDefault()

            If (Not IsNothing(filteredRow)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultRepairAlreadyAdded, True)
                isValid = False
            End If

        End If

        Return isValid

    End Function

#End Region

#Region "Method to get Vat Values"

    Private Sub populateVatValues()
        Dim objFault As New FaultManagementBL
        Dim vatBO As New VatBO()
        vatBO.VatRate = 0

        If (Not ddlVatRate.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue)) Then
            vatBO.VatID = CType(ddlVatRate.SelectedValue, Int32)
            objFault.GetVatValues(vatBO)
            calculateVat(vatBO)
        End If

    End Sub

#End Region

#Region "Method to Calculate Vat"

    Private Sub calculateVat(ByRef vatbo As VatBO)

        Dim vatAmount As Double
        Dim numericReg As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")

        If Not txtNet.Text.Trim.Equals(String.Empty) And numericReg.IsMatch(txtNet.Text.Trim) Then
            vatAmount = vatbo.VatRate
            txtVat.Text = Math.Round(((CType(txtNet.Text, Double) * vatAmount) / 100), 2).ToString()
            txtGross.Text = Math.Round(Double.Parse(txtNet.Text) + ((Double.Parse(txtNet.Text) * vatAmount) / 100), 2)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, "Please enter correct net cost", True)
            txtNet.Focus()
        End If

    End Sub

#End Region

#Region "Dropdown section"

#Region "Populate All Dropdown Lists"

    Private Sub populateAllDropdownLists()
        populatePriorityDropdownList(ddlPriority)
        populateDurationDropdownList(ddlDuration)
        populateTradeDropdownList(ddlTrade)
        populateVatRateDropdownList(ddlVatRate)
    End Sub

#End Region

#Region "Populate Dropdown List"
    Private Sub populateDropdownList(ByRef ddlLookup As DropDownList, ByRef lstLookup As List(Of LookUpBO))
        ddlLookup.Items.Clear()
        If (Not (lstLookup Is Nothing) AndAlso (lstLookup.Count > 0)) Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
        End If
        ddlLookup.DataBind()
        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlLookup.Items.Insert(0, item)

    End Sub
#End Region

#Region "Populate Duration"

    Private Sub populateDurationDropdown(ByRef ddlLookup As DropDownList)

        Dim lstLookup = New List(Of Duration)
        ddlLookup.Items.Clear()
        Dim startingLimitFaultDuration As Double = 0.5
        Dim endLimitFaultDuration As Double = 8.0
        Dim incrementStepFaultDuration As Double = 0.5
        For duration As Double = startingLimitFaultDuration To endLimitFaultDuration Step incrementStepFaultDuration
            Dim objDuration As Duration = New Duration()
            objDuration.LookUpValue = duration
            objDuration.LookUpName = duration.ToString() + vbTab + vbTab + IIf(duration > 1, "hours", "hour")
            lstLookup.Add(objDuration)
        Next

        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.DataBind()
        End If

        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlLookup.Items.Insert(0, item)

    End Sub
#End Region

#Region "Populate Priority Dropdown List"

    Private Sub populatePriorityDropdownList(ByRef ddlname As DropDownList)
        Dim objFaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        objFaultManagementBL.getPriorityLookUpValuesAll(lstLookUp)
        populateDropdownList(ddlname, lstLookUp)
    End Sub

#End Region

#Region "Populate Duration Dropdown List"

    Private Sub populateDurationDropdownList(ByRef ddlname As DropDownList)
        populateDurationDropdown(ddlname)
    End Sub

#End Region

#Region "Populate trade Dropdown List"

    Private Sub populateTradeDropdownList(ByRef ddlname As DropDownList)
        Dim objFaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        objFaultManagementBL.getTradeLookUpValuesAll(lstLookUp)
        populateDropdownList(ddlname, lstLookUp)
    End Sub

#End Region

#Region "Populate VatRate Dropdown List"
    Private Sub populateVatRateDropdownList(ByRef ddlname As DropDownList)
        Dim objFaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        objFaultManagementBL.getVatRateLookUpValuesAll(lstLookUp)
        populateDropdownList(ddlname, lstLookUp)
    End Sub
#End Region

#Region "Method to get Vat Values"

    Private Sub populateVatValues(ByVal ddlName As String)
        Dim objFault As New FaultManagementBL
        Dim vatBO As New VatBO()
        vatBO.VatRate = 0
        If (Not ddlVatRate.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue)) Then
            vatBO.VatID = CType(ddlVatRate.SelectedValue, Int32)
            objFault.GetVatValues(vatBO)
            calculateVat(vatBO)
        End If

    End Sub

#End Region

#End Region

#Region "Disable controls"

    Private Sub disableControls()
        btnAddRepair.Enabled = False
        btnAddTrade.Enabled = False
        btnSaveChanges.Enabled = False
        txtDescription.ReadOnly = True
        ddlPriority.Enabled = False
        ddlDuration.Enabled = False
        ddlTrade.Enabled = False
        txtNet.ReadOnly = True
        ddlVatRate.Enabled = False
        txtVat.ReadOnly = True
        txtGross.ReadOnly = True
        chkBoxFaultActive.Enabled = False
        chkTenantsOnline.Enabled = False
        chkBoxGasSafe.Enabled = False
        chkBoxOFTEC.Enabled = False
        chkBoxRecharge.Enabled = False
        chkBoxSubContractor.Enabled = False
        txtSearch.Enabled = False
    End Sub

#End Region

#Region "Populate Defaults"

    Private Sub populateDefaults()

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        If objFaultBo.IsNewFault = True Then
            pnlSelectedFault.Visible = False
            lblTitle.Text = ApplicationConstants.NewFaultTitle
        Else
            pnlSelectedFault.Visible = True
            lblTitle.Text = ApplicationConstants.AmendFaultTitle
        End If
        populateFaultInfo()
    End Sub

#End Region

#Region "Populate Location Repairs"

    Sub populateFaultRepairs()

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim dtFaultRepairs As DataTable = objFaultBo.FaultRepairs
        Dim selectedFaultRepairRows As DataRow() = dtFaultRepairs.Select(String.Format("{0} <> 1 ", ApplicationConstants.isRepairDeletedColumn))
        Dim dtResult As DataTable = objFaultBo.FaultRepairs.Clone
        If Not IsNothing(selectedFaultRepairRows) And selectedFaultRepairRows.Count > 0 Then
            dtResult = selectedFaultRepairRows.CopyToDataTable()
        End If
        grdFaultRepairs.DataSource = dtResult
        grdFaultRepairs.DataBind()

    End Sub

#End Region

#Region "Populate Fault Trades"

    Sub populateFaultTrades()

        Dim objFaultBo As FaultBO = SessionManager.getFaultBo()
        Dim dtFaultTrades As DataTable = objFaultBo.Trades
        Dim faultTradeRows As DataRow() = dtFaultTrades.Select(String.Format("{0} <> 1", ApplicationConstants.isDeletedColumn))
        Dim dtResult As DataTable = objFaultBo.Trades.Clone
        If Not IsNothing(faultTradeRows) And faultTradeRows.Count > 0 Then
            dtResult = faultTradeRows.CopyToDataTable()
        End If
        grdTrades.DataSource = dtResult
        grdTrades.DataBind()

    End Sub

#End Region

#Region "Search Repair"
    ''' <summary>
    ''' Returns repairs 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getRepairSearchResult(ByVal searchText As String) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()

        Dim objFaultManagementBL As New FaultManagementBL()
        objFaultManagementBL.getRepairSearchResult(resultDataset, searchText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            result.Add(String.Format("{0}^{1}", row("value"), row("id")))
        Next row

        Return result

    End Function

#End Region

#End Region

End Class