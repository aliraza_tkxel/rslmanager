﻿Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject
Imports FLS_Utilities
Imports System.Xml
Imports System.Xml.Linq
Imports System.Globalization
Imports System.Drawing


Public Class AvailableSbAppointments
    Inherits PageBase

#Region "Properties"
    Public tempFaultDs As New DataSet()
    Public operativeDs As DataSet = New DataSet()
    Public schemeId As Integer = 0

    Public tempFaultArray As ArrayList = New ArrayList()
    Public tempFaultTradeIds As StringBuilder = New StringBuilder
    Public tempFaultIds As StringBuilder = New StringBuilder
    Public appointmentInfoSeparater As String = ":::"
    Public selectedTempFaultIdsSeparater As String = ","
    Dim appointmentBo As AppointmentBO = New AppointmentBO()
    Dim blockId As Integer
    Public appointmentTimeSeparator As String = "-"
    Dim isRecall As Boolean = False
    Public groupBy As Boolean = False
    'we are considering fault and their respective operative list as block so 
    'these varaibles 'll be used to recognize each block when some button/checkbox 'll be clicked e.g refresh list button
    Dim faultAptBlockId As Integer = 0
    Dim divContainerCounter As Integer = 0
    ' to save the customer's property patch id and patch name
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
#End Region

#Region "Events"

#Region "Page Events"

#Region "Page Load"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            Me.clearDivPageContainer()
            If Not IsPostBack Then
                'Remove the existing appointments from the session, to show new pins of map.
                removeMarkersJavaScript()
                SessionManager.removeSbExistingAppointmentsForAvailableAppointmentsMap()

                'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
                'we are removing this from session (if old data exists)
                Me.clearTempAndCofirmBlockFromSession()
            End If
            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall) = PathConstants.Yes, True, False)
            End If

            Me.getSessionValues()

            If (uiMessageHelper.IsError = False And Not IsPostBack) Then
                Me.getFaults()
                Me.getAvailableOperatives()
                Me.populateConfirmedFaultsAndAppointment()
                Me.populateTempFaultsAndAppointments()
            ElseIf (uiMessageHelper.IsError = False) Then
                'get the temporary faults & available operatives
                Me.tempFaultDs = SessionManager.getTempSbFaultsDs()
                Me.operativeDs = SessionManager.getSbAvailableOperativesDs()

                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for confirmed faults and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawConfirmedFaultAppointmentGrid()

                're bind the previously displayed datatables with grid, events with buttons 
                '& redraw the controls for temporary faults and appointment list (this is requirement of dynamic controls on post back)
                Me.reDrawTempFaultAppointmentGrid()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try

    End Sub

#End Region

#Region "Pre Render"

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            saveExistingAppointmentForVisibleOperatives()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "confirmed Fault Grid Row Data Bound"
    ''' <summary>
    ''' This event makes the descirption text bold/strong
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub confirmedFaultGrid_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(1).Text = Server.HtmlDecode(e.Row.Cells(1).Text)
        End If

    End Sub
#End Region

#Region "temp Fault Grid Row Data Bound"
    ''' <summary>
    ''' his event makes the descirption text bold/strong
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tempFaultGrid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Text = Server.HtmlDecode(e.Row.Cells(0).Text)
        End If
    End Sub
#End Region

#Region "btn Back Click"
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs)
        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)
        Me.clearTempAndCofirmBlockFromSession()
        If isRecall Then
            Response.Redirect(PathConstants.SbFaultBasket + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.SbFaultBasket + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal))
        End If
    End Sub
#End Region

#Region "btn Select Operative Click"
    Protected Sub btnSelectOperative_Click(ByVal sender As Object, ByVal e As EventArgs)

        'This a sample string that will be received on this event
        'Sample ---- operative Id:::Operative Name:::Time:::Selected Date:::Comma separated temprary Fault Ids:::Trade Id
        'Values ---- 468:::Jade Burrell:::09:00 AM - 12:00 PM:::25/02/2013:::37615,43057,:::1
        Dim parameters As String = String.Empty
        Dim btnSelectOperative As Button = DirectCast(sender, Button)
        parameters = btnSelectOperative.CommandArgument

        Me.setAppointmentBoAndTempFaults(parameters)
        Me.setSessionValues()
        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)
        Me.clearTempAndCofirmBlockFromSession()
        Me.redirectToAppointmentSummary()

    End Sub
#End Region

#Region "btn Refresh List Click"
    ''' <summary>
    ''' This function handles the refresh list button event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRefreshList_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            Dim btnRefresh As Button = DirectCast(sender, Button)
            Dim selectedFaultAptBlockId As Integer = Integer.Parse(btnRefresh.CommandArgument)

            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            Me.clearDivPageContainer()

            're bind the previously displayed datatables with grid, events with buttons 
            '& redraw the controls for confirmed faults and appointment list (this is requirement of dynamic controls on post back)
            Me.reDrawConfirmedFaultAppointmentGrid()

            'this function 'll get the already temporary fault and appointment list then 
            'it 'll add five more operatives in the list of selected (fault/operative) block
            Me.refreshTheOpertivesList(selectedFaultAptBlockId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try


    End Sub
#End Region

#Region "btn View Calender Click"
    Protected Sub btnViewCalender_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim btnViewCalender As Button = DirectCast(sender, Button)
        Dim selectedTempFaultIds As String = btnViewCalender.CommandArgument.ToString()

        Dim dv As DataView = Me.createSbSelectedTempFaultDv(selectedTempFaultIds)
        SessionManager.setSbSelectedTempFaultDv(dv)

        Dim dt As DataTable = dv.ToTable
        If (dt.Rows.Count > 0) Then
            SessionManager.setTradeId(dt.Rows(0).Item("TradeId"))
        Else
            SessionManager.removeTradeId()
        End If

        'If appointment id 'll be set to zero the user 'll be able to create the appointment 
        SessionManager.setAppointmentId(0)
        SessionManager.setIsAppointmentRearrange(False)

        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)
        Me.clearTempAndCofirmBlockFromSession()

        'Redirect to Calender Page
        'Response.Redirect(PathConstants.ScheduleCalender)
        If isRecall Then
            Response.Redirect(PathConstants.SbScheduleCalender + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes + "&" + PathConstants.openPopup + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.SbScheduleCalender + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal) + "&" + PathConstants.openPopup + "=" + PathConstants.Yes)
        End If
    End Sub
#End Region

#Region "chk Patch Checked Changed "
    Protected Sub chkPatch_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            Me.clearDivPageContainer()
            Dim chkPatch As CheckBox = TryCast(sender, CheckBox)

            If chkPatch.Checked = True Then
                'If patch is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForAvailableAppointmentsMap()
                'remove pins from map.
                removeMarkersJavaScript()
            End If

            Dim selectedFaultAptBlockId As Integer = Integer.Parse(chkPatch.Attributes("CommandArgument"))

            're bind the previously displayed datatables with grid, events with buttons 
            '& redraw the controls for confirmed faults and appointment list (this is requirement of dynamic controls on post back)
            Me.reDrawConfirmedFaultAppointmentGrid()

            Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()

            If selectedFaultAptBlockId > 0 Then
                tempFaultAptList = SessionManager.getTempFaultAptBlockList()
            End If

            'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
            'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
            Dim index As Integer = Me.getBlockListIndex(selectedFaultAptBlockId)
            tempFaultAptBo = tempFaultAptList(index)
            tempFaultAptBo.IsPatchSelected = chkPatch.Checked

            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date

            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
            'Before calling the addMoreAppointment check for valid operatives dataset.
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)
            End If
            'save in temporary fault operative list
            tempFaultAptList(index) = tempFaultAptBo

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment            
            faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)

            'pouplate the temporary fault appointment grid
            Me.bindTempFaultsAppointment(tempFaultAptList)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "chk Date Checked Changed"
    Protected Sub chkDate_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
            Me.clearDivPageContainer()
            Dim chkDate As CheckBox = TryCast(sender, CheckBox)

            If chkDate.Checked = True Then
                'If expiry date is unchecked remove existing appointments for map.
                SessionManager.removeExistingAppointmentsForAvailableAppointmentsMap()
                'remove pins from map.
                removeMarkersJavaScript()
            End If

            Dim selectedFaultAptBlockId As Integer = Integer.Parse(chkDate.Attributes("CommandArgument"))

            're bind the previously displayed data tables with grid, events with buttons 
            '& redraw the controls for confirmed faults and appointment list (this is requirement of dynamic controls on post back)
            Me.reDrawConfirmedFaultAppointmentGrid()

            Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()

            If selectedFaultAptBlockId > 0 Then
                tempFaultAptList = SessionManager.getTempFaultAptBlockList()
            End If

            'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
            'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
            Dim index As Integer = Me.getBlockListIndex(selectedFaultAptBlockId)
            tempFaultAptBo = tempFaultAptList(index)
            tempFaultAptBo.IsDateSelected = chkDate.Checked

            'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()

            'Before calling the addMoreAppointment check for valid operatives dataset.
            If (Not IsNothing(operativeDs) AndAlso operativeDs.Tables.Count >= 3) Then
                faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)
            End If
            'save in temporary fault operative list
            tempFaultAptList(index) = tempFaultAptBo

            'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment             
            faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)

            'pouplate the temporary fault appointment grid
            Me.bindTempFaultsAppointment(tempFaultAptList)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Back To Basket Click"
    Protected Sub btnBackToBasket_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBackToBasket.Click
        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)
        Me.clearTempAndCofirmBlockFromSession()
        If isRecall Then
            Response.Redirect(PathConstants.SbFaultBasket + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.SbFaultBasket + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal))
        End If
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "get Faults"
    ''' <summary>
    ''' This function 'll get the confirmed and un confirmed faults from the database
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getFaults()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objFaultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()

        objFaultAppointmentBl.getTemporaryFaultBasket(tempFaultDs, Me.tempFaultIds, Me.tempFaultTradeIds, Me.schemeId, Me.blockId, isRecall)

        If (tempFaultDs.Tables(ApplicationConstants.TempFaultDataTable).Rows.Count = 0 And tempFaultDs.Tables(ApplicationConstants.ConfirmFaultDataTable).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        Else
            SessionManager.setTempSbFaultsDs(Me.tempFaultDs)
        End If
    End Sub
#End Region

#Region "get Available Operatives"

    Private Sub getAvailableOperatives()

        Dim objFaultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
        objFaultAppointmentBl.getAvailableOperatives(operativeDs, Me.tempFaultIds, Me.isRecall)

        If (operativeDs.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.OperativeNotFound, True)
        End If

        SessionManager.setSbAvailableOperativesDs(operativeDs)

    End Sub
#End Region

#Region "Region For Confirm Faults And Appointment"

#Region "populate Confirmed Faults And Appointment"

    ''' <summary>
    ''' This fucntion populates the confrimed faults in asp.net/html controls
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populateConfirmedFaultsAndAppointment()
        'check if the confirmed faults exists in table
        If tempFaultDs.Tables.Count > 0 Then
            If tempFaultDs.Tables(1).Rows.Count > 0 Then

                'if confirmed faults exists then create a list 
                Dim objFaultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
                Dim confirmFaultAptBoList As List(Of ConfirmFaultAppointmentBO) = New List(Of ConfirmFaultAppointmentBO)
                'this function 'll loop through dataset for confirmed faults and it 'll return the list 
                'of faults and confirmed appointments against them
                confirmFaultAptBoList = objFaultAppointmentBl.getConfirmedFaultsAppointments(tempFaultDs)
                'bind the confirmed faults with grid
                Me.bindConfirmFaultsAppointment(confirmFaultAptBoList)
                SessionManager.setConfirmFaultAptBlockList(confirmFaultAptBoList)
            End If
        End If
    End Sub

#End Region

#Region "bind Confirm Faults Appointment"
    Private Sub bindConfirmFaultsAppointment(ByVal confirmFaultAptBoList As List(Of ConfirmFaultAppointmentBO))
        If confirmFaultAptBoList.Count() > 0 Then
            Dim confirmFaultAptBo As ConfirmFaultAppointmentBO = New ConfirmFaultAppointmentBO()
            Dim counter As Integer = 0
            'panel control 
            Dim panelContainer As Panel
            'bind this above data with grid view             
            For Each item As ConfirmFaultAppointmentBO In confirmFaultAptBoList
                counter = counter + 1
                'create container for every block of confirmed fault and appointment 
                panelContainer = Me.createContainer()
                'Create object of fault grid
                Dim confirmedFaultGrid As New GridView()
                AddHandler confirmedFaultGrid.RowDataBound, AddressOf confirmedFaultGrid_RowDataBound
                confirmedFaultGrid.ID = "grdConfirmFaults" + counter.ToString()
                confirmedFaultGrid.EnableViewState = True
                confirmedFaultGrid.CssClass = "grid_confirm_faults"
                confirmedFaultGrid.HeaderStyle.CssClass = "available-apptmts-frow"
                confirmedFaultGrid.DataSource = item.confirmFaultDtBo.dt
                confirmedFaultGrid.DataBind()
                'Add the GridView control object dynamically into the div control
                panelContainer.Controls.Add(confirmedFaultGrid)

                'Create object of appointment grid
                Dim confirmedAptGrid As New GridView()
                confirmedAptGrid.ID = "grdConfirmAppointments" + counter.ToString()
                confirmedAptGrid.EnableViewState = True
                confirmedAptGrid.CssClass = "grid_confirm_appointment available-apptmts-optable"
                confirmedAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
                confirmedAptGrid.DataSource = item.confirmAppointmentDtBo.dt
                confirmedAptGrid.DataBind()

                'Add the GridView control object dynamically into the div control
                panelContainer.Controls.Add(confirmedAptGrid)
                'Add this panel container to div page container
                divPageContainer.Controls.Add(panelContainer)
            Next
        End If

    End Sub
#End Region

#Region "reDraw Confirmed Fault Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then redraw the controls of confirmed faults and appointment list''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawConfirmedFaultAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for confirmed faults and appointment list
        Dim confirmFaultAptBoList As List(Of ConfirmFaultAppointmentBO) = New List(Of ConfirmFaultAppointmentBO)
        confirmFaultAptBoList = SessionManager.getConfirmFaultAptBlockList()
        Me.bindConfirmFaultsAppointment(confirmFaultAptBoList)
    End Sub
#End Region
#End Region

#Region "Region For Temporary Faults And Appointments"

#Region "Populate temp Faults And Appointment"
    Private Sub populateTempFaultsAndAppointments()

        Dim tempFaultsExist As Boolean = False
        'check if the unconfirmed faults exists in table
        If tempFaultDs.Tables.Count > 0 Then
            If tempFaultDs.Tables(0).Rows.Count > 0 Then
                tempFaultsExist = True
            Else
                Me.createSingleBackButton()
            End If
        End If

        'if unconfirmed faults exists then create a list 
        If tempFaultsExist = True Then            '
            Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
            Dim tempFaultAptBoList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            'this function 'll process the faults and 'll create the appointments
            tempFaultAptBoList = faultAppointmentBl.getTempFaultsAppointments(Me.tempFaultDs.Tables(0), Me.operativeDs.Tables(0), Me.operativeDs.Tables(1), Me.operativeDs.Tables(2), Me.groupBy, Me.commonAddressBo)
            'save list in session 
            SessionManager.setTempFaultAptBlockList(tempFaultAptBoList)
            Me.bindTempFaultsAppointment(tempFaultAptBoList)
        End If

    End Sub
#End Region

#Region "bind Temp Faults Appointment"
    Private Sub bindTempFaultsAppointment(ByVal tempFaultAptBoList As List(Of TempFaultAppointmentBO))
        If IsNothing(tempFaultAptBoList) Then
            Me.createSingleBackButton()
        Else
            Dim counter As Integer = 0
            'panel control 
            Dim panelContainer As Panel

            'bind this above data with grid view             
            For Each item As TempFaultAppointmentBO In tempFaultAptBoList
                Dim selectedTempFaultIds As String = String.Empty
                'we are considering fault and their respective operative list as block so 
                'this varaible 'll be used to recognize each block whenever some button/checkbox event 'll be fired
                faultAptBlockId += 1
                counter = counter + 1
                'create container for every block of temporary faults and appointment 
                panelContainer = Me.createContainer()

                'Create object of fault grid
                Dim tempFaultGrid As New GridView
                tempFaultGrid.ID = "grdTempFaults" + counter.ToString()
                AddHandler tempFaultGrid.RowDataBound, AddressOf tempFaultGrid_RowDataBound
                tempFaultGrid.EnableViewState = True
                tempFaultGrid.CssClass = "grid_temp_faults"
                tempFaultGrid.HeaderStyle.CssClass = "available-apptmts-frow"
                tempFaultGrid.DataSource = item.tempFaultDtBo.dt
                tempFaultGrid.DataBind()
                'Add the GridView control object dynamically into the div control
                panelContainer.Controls.Add(tempFaultGrid)

                Dim headerRow As GridViewRow
                headerRow = Me.createFilterHeader(item.IsPatchSelected, item.IsDateSelected)

                Dim t As Table = TryCast(tempFaultGrid.Controls(0), Table)
                If t IsNot Nothing Then
                    t.Rows.AddAt(0, headerRow)
                End If

                'get the tempfaultids and current trade id 
                For Each dr As DataRow In item.tempFaultDtBo.dt.Rows
                    'make comma separated string of tempfault ids so those can be attached in command argument with select button
                    selectedTempFaultIds = selectedTempFaultIds + Convert.ToString(dr.Item(TempFaultDtBO.tempFaultIdColName)) + Me.selectedTempFaultIdsSeparater
                Next

                'hide the last column from temporary fault grid(that are not required in grid view) because those columns 
                'were there in datatable and those columns have been automatically created, when we bound datatable with grid view
                'the columns of these data table 'll be used through sessoion
                tempFaultGrid.HeaderRow.Cells(4).Visible = False
                tempFaultGrid.HeaderRow.Cells(5).Visible = False
                tempFaultGrid.HeaderRow.Cells(6).Visible = False
                tempFaultGrid.HeaderRow.Cells(7).Visible = False 'TradeID
                'tempFaultGrid.HeaderRow.Cells(8).Visible = False 'Select

                'run the loop on grid view rows to remove the cells (that are not required in grid view) from each row 
                'and also to add the select button in each row
                Dim uniqueBlockId As Integer = 0
                For Each gvr As GridViewRow In tempFaultGrid.Rows
                    gvr.Cells(4).Visible = False
                    gvr.Cells(5).Visible = False
                    gvr.Cells(6).Visible = False
                    gvr.Cells(7).Visible = False 'TradeID
                    gvr.Cells(1).Controls.Add(createDurationControls(uniqueBlockId, item))
                    uniqueBlockId += 1
                Next

                'Create object of appointment grid
                Dim isEmptyGrid As Boolean = False
                Dim tempAptGrid As New GridView()
                tempAptGrid.ID = "grdTempAppointments" + counter.ToString()
                tempAptGrid.ShowFooter = True
                tempAptGrid.EnableViewState = True
                tempAptGrid.CssClass = "grid_appointment available-apptmts-optable"
                tempAptGrid.HeaderStyle.CssClass = "available-apptmts-frow"
                'Check if , are there any appointments or not .If not then display message
                If item.tempAppointmentDtBo.dt.Rows.Count = 0 Then
                    Me.displayMsgInEmptyGrid(tempAptGrid, item, UserMessageConstants.NoOperativesExistWithInTime)
                    isEmptyGrid = True
                Else
                    tempAptGrid.DataSource = item.tempAppointmentDtBo.dt
                    tempAptGrid.DataBind()
                    'add row span on footer and add back button, refresh list button and view calendar button
                    tempAptGrid.FooterRow.Cells(0).Controls.Add(createAppointmentButtons(selectedTempFaultIds))
                End If

                'Add the GridView control into the div control/panel
                panelContainer.Controls.Add(tempAptGrid)

                If tempAptGrid.Rows.Count > 0 Then
                    Dim m As Integer = tempAptGrid.FooterRow.Cells.Count
                    For i As Integer = m - 1 To 1 Step -1
                        tempAptGrid.FooterRow.Cells.RemoveAt(i)
                    Next i
                    tempAptGrid.FooterRow.Cells(0).ColumnSpan = 7 '6 is the number of visible columns to span.
                End If

                'declare the variables that 'll store the information required to bind with select button in grid
                Dim operativeId As String = String.Empty
                Dim operativeName As String = String.Empty
                Dim aptTimeString As String = String.Empty
                Dim aptDateString As String = String.Empty
                Dim dtCounter As Integer = 0
                Dim completeAppointmentInfo As String = String.Empty

                'remove the header of unwanted columns
                tempAptGrid.HeaderRow.Cells(7).Visible = False
                tempAptGrid.HeaderRow.Cells(8).Visible = False
                tempAptGrid.HeaderRow.Cells(9).Visible = False
                tempAptGrid.HeaderRow.Cells(10).Visible = False
                tempAptGrid.HeaderRow.Cells(11).Visible = False
                'if grid is not emtpy then remove the cells and also create select button and set command argument with that
                If isEmptyGrid = False Then
                    'run the loop on grid view rows to remove the cells from each row 
                    'and also to add the select button in each row
                    For Each gvr As GridViewRow In tempAptGrid.Rows
                        gvr.Cells(7).Visible = False
                        gvr.Cells(8).Visible = False
                        gvr.Cells(9).Visible = False
                        gvr.Cells(10).Visible = False
                        gvr.Cells(11).Visible = False
                        'get the information that should be set with select button 
                        operativeId = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeIdColName)
                        operativeName = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.operativeColName)
                        aptTimeString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.startDateColName)
                        aptDateString = item.tempAppointmentDtBo.dt.Rows(dtCounter).Item(TempAppointmentDtBO.endDateColName)
                        dtCounter += 1

                        'save this operative and time slot information in a string, after that it 'll be bind with select button
                        completeAppointmentInfo = operativeId + Me.appointmentInfoSeparater + operativeName + Me.appointmentInfoSeparater + aptTimeString + Me.appointmentInfoSeparater + aptDateString + Me.appointmentInfoSeparater + selectedTempFaultIds + Me.appointmentInfoSeparater + item.TradeId.ToString()

                        'Add the select button 
                        Dim btnSelectOperative As Button = New Button()
                        btnSelectOperative.ID = "btnSelectOperative" + counter.ToString()
                        btnSelectOperative.Text = "Select"
                        btnSelectOperative.CommandArgument = completeAppointmentInfo
                        AddHandler btnSelectOperative.Click, AddressOf btnSelectOperative_Click
                        gvr.Cells(6).Controls.Add(btnSelectOperative)
                    Next
                End If

                'Add this panel container to div page container
                divPageContainer.Controls.Add(panelContainer)

                'Call the update markers for any refresh in appointment slots.
                If Page.IsPostBack Then
                    updateMarkersJavaScript()
                End If
            Next
        End If


    End Sub
#End Region

#Region "reDraw Temp Fault Appointment Grid"
    ''' <summary>
    ''' re bind the previously displayed datatables with grid, events with buttons then
    ''' redraw the controls for temporary faults and appointment list
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reDrawTempFaultAppointmentGrid()
        're bind the previously displayed datatables with grid, events with buttons 
        '& redraw the controls for temporary faults and appointment list
        Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
        tempFaultAptList = SessionManager.getTempFaultAptBlockList()
        Me.bindTempFaultsAppointment(tempFaultAptList)
    End Sub
#End Region

#Region "Display Msg In Empty Grid"
    Private Sub displayMsgInEmptyGrid(ByRef grid As GridView, ByRef tempFaultAptBo As TempFaultAppointmentBO, ByVal msg As String)
        'create the new empty row in data table to display the message
        tempFaultAptBo.tempAppointmentDtBo.dt.Rows.Add(tempFaultAptBo.tempAppointmentDtBo.dt.NewRow())
        grid.DataSource = tempFaultAptBo.tempAppointmentDtBo.dt
        grid.DataBind()

        'clear the empty row so it should not create problem in re-drawing the controls 
        'after post back
        tempFaultAptBo.tempAppointmentDtBo.dt.Clear()
        'create label to display message
        Dim lblmsg As HtmlGenericControl = New HtmlGenericControl("div")
        lblmsg.InnerText = msg
        lblmsg.Attributes.Add("class", "lblmessage")
        'merge the grid cells so that we can display the message with header
        Dim columncount As Integer = grid.Rows(0).Cells.Count
        grid.Rows(0).Cells.Clear()
        grid.Rows(0).Cells.Add(New TableCell())
        grid.Rows(0).Cells(0).ColumnSpan = columncount
        grid.Rows(0).Cells(0).Controls.Add(lblmsg)
    End Sub
#End Region

#Region "create Container"
    ''' <summary>
    ''' This function creates the html container for every block of confirmed fault and confirmed appointment 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function createContainer()
        divContainerCounter += 1
        Dim divContainer As Panel = New Panel()
        divContainer.ID = "divContainer" + divContainerCounter.ToString()
        divContainer.CssClass = "fl_width_100_text_a_2"
        Return divContainer
    End Function
#End Region

#Region "clear Div Page Container"
    ''' <summary>
    ''' 'To avoid duplication of controls it is necessary to clear the container or place holder of dynamic controls on post back
    ''' This control(divPageContainer) contains all the grids that 'll be displayed on page.
    ''' This function should be called on page load and on every event handler
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearDivPageContainer()
        divPageContainer.Controls.Clear()
    End Sub
#End Region

#Region "create Filter Header"
    Private Function createFilterHeader(ByVal isPatchSelected As Boolean, ByVal isDateSelected As Boolean)

        Dim headerRow As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
        Dim headerCell As New TableCell()
        headerCell.ColumnSpan = 4
        headerCell.Style.Add("background-color", "lightgray")
        headerCell.Style.Add("font-weight", "bold")
        headerCell.CssClass = "available-apptmts-frow"

        'add filter div
        Dim divFilter As HtmlGenericControl = New HtmlGenericControl("div")
        divFilter.InnerText = "Filter Available Operatives By: "
        divFilter.Attributes.Add("class", "grid_temp_faults_filter")
        headerCell.Controls.Add(divFilter)

        'add patch label
        Dim divPatch As HtmlGenericControl = New HtmlGenericControl("div")
        divPatch.InnerText = "Patch: "
        divPatch.Attributes.Add("class", "grid_temp_faults_patch")

        'add patch checkbox
        Dim chkPatch As CheckBox = New CheckBox()
        chkPatch.ID = "chkPatch"
        chkPatch.AutoPostBack = True
        chkPatch.Checked = isPatchSelected
        chkPatch.Attributes("CommandArgument") = faultAptBlockId
        AddHandler chkPatch.CheckedChanged, AddressOf chkPatch_CheckedChanged

        'add check box to patch label
        divPatch.Controls.Add(chkPatch)
        headerCell.Controls.Add(divPatch)

        'add date label
        Dim divDate As HtmlGenericControl = New HtmlGenericControl("div")
        divDate.InnerText = "Due Date: "
        divDate.Attributes.Add("class", "grid_temp_faults_date")

        'add date checkbox
        Dim chkDate As CheckBox = New CheckBox()
        chkDate.ID = "chkDate"
        chkDate.AutoPostBack = True
        chkDate.Checked = isDateSelected
        chkDate.Attributes("CommandArgument") = faultAptBlockId
        AddHandler chkDate.CheckedChanged, AddressOf chkDate_CheckedChanged

        'add check box to date label
        divDate.Controls.Add(chkDate)
        headerCell.Controls.Add(divDate)

        'add dafault label
        Dim divDefault As HtmlGenericControl = New HtmlGenericControl("div")
        divDefault.InnerText = "Default: "
        divDefault.Attributes.Add("class", "grid_temp_faults_default")

        'add default value under default label
        Dim lblDefault As Label = New Label()
        lblDefault.Text = Me.commonAddressBo.PatchName
        divDefault.Controls.Add(lblDefault)
        headerCell.Controls.Add(divDefault)

        'add the addition in cell to header row
        headerRow.Cells.Add(headerCell)
        Return headerRow
    End Function
#End Region

#Region "create Appointment Buttons"
    Private Function createAppointmentButtons(ByVal selectedTempFaultIds As String)
        Dim buttonsContainer As Panel = New Panel()
        Dim btnBack As Button = New Button()
        Dim btnRefreshList As Button = New Button()
        Dim btnViewCalendar As Button = New Button()
        'set the back button 
        btnBack.Text = "< Back To Basket"
        btnBack.Style.Add("margin-right", "5px")
        btnRefreshList.Style.Add("margin-right", "5px")
        btnViewCalendar.Style.Add("margin-right", "5px")
        AddHandler btnBack.Click, AddressOf btnBackToBasket_Click
        'set the refresh list button 
        btnRefreshList.Text = "Refresh List"
        btnRefreshList.CommandArgument = faultAptBlockId
        AddHandler btnRefreshList.Click, AddressOf btnRefreshList_Click
        'set the view calendar button
        btnViewCalendar.Text = "View Calendar"
        btnViewCalendar.CommandArgument = selectedTempFaultIds
        If Not (SessionManager.getLoggedInUserType().Equals(ApplicationConstants.LoggedInUserTypes.Manager.ToString())) Then
            btnViewCalendar.Visible = False
        End If
        AddHandler btnViewCalendar.Click, AddressOf btnViewCalender_Click
        'set the css for div/panel that 'll contain all buttons
        buttonsContainer.CssClass = "text_a_r_padd__r_20px"
        'add all buttons to container 
        buttonsContainer.Controls.Add(btnBack)
        buttonsContainer.Controls.Add(btnRefreshList)
        buttonsContainer.Controls.Add(btnViewCalendar)
        Return buttonsContainer
    End Function
#End Region

#Region "create Single Back Button"
    Private Sub createSingleBackButton()
        Me.btnBackToBasket.Visible = True
        'Dim panelContainer As Panel = New Panel()
        'Dim btnBack As Button = New Button()
        'btnBack.ID = "btnBackToBasket"
        ''set the back button 
        'btnBack.Text = "< Back To Basket"
        'AddHandler btnBack.Click, AddressOf btnBack_Click
        'panelContainer.Controls.Add(btnBack)
        ''Add this panel container to div page container
        'divPageContainer.Controls.Add(panelContainer)
    End Sub
#End Region

#Region "refresh The Opertives List"
    Private Sub refreshTheOpertivesList(ByVal selectedFaultAptBlockId As Integer)
        Dim tempFaultAptList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
        Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()

        If selectedFaultAptBlockId > 0 Then
            tempFaultAptList = SessionManager.getTempFaultAptBlockList()
        End If

        'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
        'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultAptList 
        Dim index As Integer = Me.getBlockListIndex(selectedFaultAptBlockId)
        tempFaultAptBo = tempFaultAptList(index)

        'get the datatable from the object
        Dim tempAptDt As DataTable = tempFaultAptBo.tempAppointmentDtBo.dt

        'increase the count of operatives by 
        tempFaultAptBo.DisplayCount = tempFaultAptBo.DisplayCount + 5
        'This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        Dim faultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
        faultAppointmentBl.addMoreAppointments(operativeDs.Tables(0), operativeDs.Tables(2), operativeDs.Tables(1), commonAddressBo, tempFaultAptBo)

        'save in temporary fault operative list
        tempFaultAptList(index) = tempFaultAptBo

        'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment         
        faultAppointmentBl.orderAppointmentsByDistance(tempFaultAptBo)

        'pouplate the temporary fault appointment grid
        Me.bindTempFaultsAppointment(tempFaultAptList)

    End Sub
#End Region

#Region "get Block List Index"
    ''' <summary>
    ''' 'indexes of tempFaultAptList starts from zero where as selectedFaultAptBlockId starts from 1 
    ''' 'to make this equal subtracting (1) from selectedFaultAptBlockId 'll represent the exact index of tempFaultApList 
    ''' </summary>
    ''' <param name="selectedFaultAptBlockId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function getBlockListIndex(ByVal selectedFaultAptBlockId As Integer)
        Return selectedFaultAptBlockId - 1
    End Function
#End Region

#Region "set AppointmentBo And Temp Faults"

    Private Sub setAppointmentBoAndTempFaults(ByRef parameters As String)
        Dim parameterArray As String()
        ' Dim appointmentTime As String()

        Dim separater As String() = {Me.appointmentInfoSeparater}
        parameterArray = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries)
        'Parameter array has the following indexes
        '0 index = operative id, 
        '1 index = operative name, 
        '2 index = appointment start date time, 
        '3 index = appointment end date time
        '4 index = selected temp fault ids, 
        '5 index = trade ids

        Dim lAppointmentStartDateTime As Date = Date.ParseExact(parameterArray(appointmentBo.AppointmentParameter.AppointmentStartDateTime).Trim(), ApplicationConstants.AppointmentDateTimeFormat, CultureInfo.CreateSpecificCulture("en-GB"))
        Dim lAppointmentEndDateTime As Date = Date.ParseExact(parameterArray(appointmentBo.AppointmentParameter.AppointmentEndDateTime).Trim(), ApplicationConstants.AppointmentDateTimeFormat, CultureInfo.CreateSpecificCulture("en-GB"))

        Me.appointmentBo.OperativeId = parameterArray(appointmentBo.AppointmentParameter.OperativeId)
        Me.appointmentBo.Operative = parameterArray(appointmentBo.AppointmentParameter.Operative)

        Me.appointmentBo.StartTime = lAppointmentStartDateTime.ToString("HH:mm")
        Me.appointmentBo.EndTime = lAppointmentEndDateTime.ToString("HH:mm")
        Me.appointmentBo.AppointmentStartDate = lAppointmentStartDateTime
        Me.appointmentBo.AppointmentEndDate = lAppointmentEndDateTime

        Dim selectedTempFaultIds As String = String.Empty
        selectedTempFaultIds = parameterArray(appointmentBo.AppointmentParameter.selectedTempFaultIds).Remove(parameterArray(4).Length - 1)
        Dim dv As DataView = Me.createSbSelectedTempFaultDv(selectedTempFaultIds)
        SessionManager.setSbSelectedTempFaultDv(dv)
        SessionManager.setSelectedTempFaultIds(selectedTempFaultIds)

    End Sub

#End Region

#Region "Create Selected Temp Faults Data View"
    Private Function createSbSelectedTempFaultDv(ByVal tempFaultIds As String)

        Me.tempFaultDs = SessionManager.getTempSbFaultsDs()
        Dim dv As DataView = New DataView()
        dv = tempFaultDs.Tables(ApplicationConstants.TempFaultDataTable).DefaultView
        dv.RowFilter = "TempFaultId IN (" + tempFaultIds + ")"

        Return dv

    End Function
#End Region

#Region "Set Session Values"
    Private Sub setSessionValues()
        SessionManager.setAptDataForSbAptSummary(Me.appointmentBo)
    End Sub

#End Region

#Region "Redirect to Appointment Summary And Confirmation"
    Private Sub redirectToAppointmentSummary()
        If isRecall = True Then
            Response.Redirect(PathConstants.SbAppointmentSummary + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes, True)
        Else
            Response.Redirect(PathConstants.SbAppointmentSummary + GeneralHelper.getSrc(PathConstants.AvailableSbAppointmentsSrcVal), True)
        End If

    End Sub
#End Region
#End Region

#Region "get Session Values"
    Private Sub getSessionValues()

        '--- TODO: comment these below lines when in deployment
        'SessionManager.setPropertyId("A720040007")
        'SessionManager.setCustomerId("2043")
        'Dim tempFaultBo As TempFaultBO = New TempFaultBO
        'Dim tempFaultArray As ArrayList = New ArrayList()
        'tempFaultBo.Contractor = 0
        'tempFaultBo.TempFaultId = 44869
        'tempFaultBo.Trade = 2
        'tempFaultArray.Add(tempFaultBo)

        'tempFaultBo = New TempFaultBO
        'tempFaultBo.Contractor = 0
        'tempFaultBo.TempFaultId = 44873
        'tempFaultBo.Trade = 2
        'tempFaultArray.Add(tempFaultBo)

        'tempFaultBo = New TempFaultBO
        'tempFaultBo.Contractor = 0
        'tempFaultBo.TempFaultId = 44877
        'tempFaultBo.Trade = 2
        'tempFaultArray.Add(tempFaultBo)

        'tempFaultBo = New TempFaultBO
        'tempFaultBo.Contractor = 0
        'tempFaultBo.TempFaultId = 44878
        'tempFaultBo.Trade = 3
        'tempFaultArray.Add(tempFaultBo)

        'SessionManager.setTemporaryFaultBo(tempFaultArray)

        'Dim customerObj As CustomerBO = New CustomerBO()
        'customerObj.CustomerId = 2043
        'customerObj.PatchId = 18
        'customerObj.PatchName = "Central"
        'customerObj.PostCode = "NR1 2BN"
        'SessionManager.setCustomerData(customerObj)
        'SessionManager.setLoggedInUserType("Manager")
        'SessionManager.setGroupBy(True)
        '--- TODO: comment these above lines when in deployment

        Me.schemeId = SessionManager.getSchemeId()
        Me.blockId = SessionManager.getBlockId()
        Me.tempFaultArray = SessionManager.getSbTemporaryFaultBo()
        Me.groupBy = SessionManager.getGroupBy()
        Me.commonAddressBo = SessionManager.getCommonAddressBO()
        'validate the session values
        If Not IsNothing(tempFaultArray) And Me.schemeId > 0 And String.IsNullOrEmpty(Me.groupBy) = False Then

            'Make comma separated list of temporary fault ids and fault trade ids
            Dim item As TempFaultBO = New TempFaultBO()
            For Each item In tempFaultArray
                'make a string
                Me.tempFaultIds.Append(item.TempFaultId.ToString()).Append(",")
                Me.tempFaultTradeIds.Append(item.Trade.ToString()).Append(",")
            Next

            'remove the last comma
            Me.tempFaultIds = Me.tempFaultIds.Remove(tempFaultIds.Length - 1, 1)
            Me.tempFaultTradeIds = Me.tempFaultTradeIds.Remove(tempFaultTradeIds.Length - 1, 1)
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidGroupByPropFaults, True)
        End If

    End Sub

#End Region

#Region "clear Temp And Cofirm Block From Session"
    ''' <summary>
    ''' we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
    ''' we are removing this from session (if old data exists)
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub clearTempAndCofirmBlockFromSession()
        'we are saving faults and appointmnet in session. In order to make sure that user should not get the old data 
        'we are removing this from session (if old data exists)
        'removing the confirm appointments from session 
        SessionManager.removeConfirmSbFaultAptBlockList()
        'removing the temporary appointments from session
        SessionManager.removeTempSbFaultAptBlockList()

    End Sub
#End Region

#Region "Google Map Functions"

#Region "Save existing appointments in session to show on map"

    Private Sub saveExistingAppointmentForVisibleOperatives()
        Dim operativesDs As DataSet = SessionManager.getSbAvailableOperativesDs()
        Dim existingAppointments As New DataTable()

        If Not IsNothing(operativesDs) AndAlso operativesDs.Tables.Count >= 3 Then
            existingAppointments = operativeDs.Tables(2)
        End If

        Dim tempFaultAptBoList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)

        tempFaultAptBoList = SessionManager.getTempFaultAptBlockList()

        Dim dt As DataTable
        Dim operativeId As Integer
        Dim operativesList As New List(Of Integer)
        For Each tempFaultAptBO As TempFaultAppointmentBO In tempFaultAptBoList
            dt = tempFaultAptBO.tempAppointmentDtBo.dt

            For Each row As DataRow In dt.Rows
                operativeId = row(TempAppointmentDtBO.operativeIdColName)
                If Not operativesList.Contains(operativeId) Then
                    operativesList.Add(operativeId)
                End If
            Next
        Next

        SessionManager.removeExistingAppointmentsForAvailableAppointmentsMap()
        If existingAppointments.Columns.Count > 0 And existingAppointments.Columns.Contains("OperativeId") Then
            Dim appointmentsForMap = From extapt In existingAppointments.AsEnumerable()
                                     Where operativesList.Contains(extapt("OperativeId"))
                                     Select extapt

            If appointmentsForMap.Count() > 0 Then
                SessionManager.setExistingAppointmentsForAvailableAppointmentsMap(appointmentsForMap.CopyToDataTable())
            End If

        End If

    End Sub

#End Region

#Region "Web Method - Get Existing Appointments Data (Property Address)"

    <System.Web.Services.WebMethod(EnableSession:=True)> _
    Public Shared Function getExistingAppointmentsData() As String
        Dim returnJSON As New StringBuilder()
        Dim serializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim appointmentsForMap = SessionManager.getExistingAppointmentsForAvailableAppointmentsMap()

        Dim rows = New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        If Not IsNothing(appointmentsForMap) Then
            For Each dr As DataRow In appointmentsForMap.Rows
                row = New Dictionary(Of String, Object)()
                For Each col As DataColumn In appointmentsForMap.Columns
                    row.Add(col.ColumnName, dr(col))
                Next
                rows.Add(row)
            Next
        End If

        Return serializer.Serialize(rows)
    End Function

#End Region

#Region "Register client side script to update markers on Google Map. on appointments refresh."

    Private Sub updateMarkersJavaScript()
        'Register client side script to update markers on Google Map. on appointments refresh.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "updateMarkersAptA", "addOriginMarkers();", True)
    End Sub

#End Region

#Region "Register client side script to remove existing markers from Map"

    Private Sub removeMarkersJavaScript()
        'Register client side script to remove markers on Google Map.
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "deleteMarkersAptA", "deleteMarkers();", True)
    End Sub

#End Region

#End Region

#Region "Scheduling Duration Edit Functions"

#Region "Create Duration Controls"

    ''' <summary>
    ''' This function 'll create the duration control on the top of trade grid
    ''' </summary>
    ''' <param name="uniqueBlockId"></param>
    ''' <remarks></remarks>
    Protected Function createDurationControls(ByVal uniqueBlockId As String, item As TempFaultAppointmentBO)

        'Edit ImageButton
        Dim ucDurationControl As FaultDuration
        ucDurationControl = Page.LoadControl("~/Controls/Fault/FaultDuration.ascx")
        ucDurationControl.ID = ApplicationConstants.DurationUserControlId + uniqueBlockId

        AddHandler ucDurationControl.DoneButton.Click, AddressOf imgBtnDone_Click

        With item.tempFaultDtBo.dt.Rows(uniqueBlockId)
            Dim durationString As String = .Item(TempFaultDtBO.durationStringColName)
            Dim duration As Decimal = durationString.Split(" ").ElementAtOrDefault(0)
            Dim tempFaultId As Integer = .Item(TempFaultDtBO.tempFaultIdColName)
            ucDurationControl.populateDurationControls(duration, tempFaultId, durationString, "hr")
        End With

        Return ucDurationControl
    End Function

#End Region

#Region "Image btn Done Click"
    Protected Sub imgBtnDone_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim selectedImgBtnDone As ImageButton = DirectCast(sender, ImageButton)
            Dim tempFaultId As Integer = Integer.Parse(selectedImgBtnDone.CommandArgument)

            Dim ucDurationControl As FaultDuration = DirectCast(selectedImgBtnDone.NamingContainer, FaultDuration)
            Dim objFaultAppointmentBL As New FaultAppointmentBL()
            objFaultAppointmentBL.updateTempFaultDuration(tempFaultId, ucDurationControl.Duration)
            Me.reloadCurrentWebPage()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            If ApplicationConstants.distanceError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.DistanceError, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Reload the Current Web Page"
    ''' <summary>
    ''' This function 'll reload the current page. This is being used when user rearrange the scheduled appointment
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub reloadCurrentWebPage()
        Response.Redirect(Request.RawUrl)
    End Sub
#End Region

#End Region

End Class