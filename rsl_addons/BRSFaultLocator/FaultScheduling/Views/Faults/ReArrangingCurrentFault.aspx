﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="ReArrangingCurrentFault.aspx.vb" Inherits="FaultScheduling.ReArrangingCurrentFault" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="custom" TagName="CustomerDetails" Src="~/Controls/Common/CustomerDetail.ascx" %>
<%@ Register TagPrefix="uc" TagName="faultDuration" Src="~/Controls/Fault/FaultDuration.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="../../Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&client=gme-broadlandhousing&v=3.17"></script>
    <script type="text/javascript">
        var map;
        var markers = [];
        var markersAddress = [];
        function showMap() {
            var destinationAddress = document.getElementById("<%= custDetails.ClientStreetAddressLabel.ClientID %>").innerHTML;
            destinationAddress += ", " + document.getElementById("<%= custDetails.ClientClientCityLabel.ClientID %>").innerHTML;
            destinationAddress = destinationAddress.replace("\'", "");

            var map_canvas = document.getElementById("mapContainer");
            var map_options = {
                center: new google.maps.LatLng(52.630886, -0.4573616),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, map_options);

            addOriginMarkers();

            var maxZindex = google.maps.Marker.MAX_ZINDEX;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': destinationAddress }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: destinationAddress,
                        zIndex: maxZindex,
                        icon: "https://maps.google.com/mapfiles/kml/paddle/D.png"
                    });
                }
                console.log(status + ":(destination address) " + destinationAddress + " Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
            });
        }

        function addOriginMarkers() {

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "ReArrangingCurrentFault.aspx/getExistingAppointmentsData",
                data: "{}",
                dataType: "json",
                success: function (response) {
                    var appointments = JSON.parse(response.d);
                    var counter = 0;
                    var timeout = 1000;
                    for (var i = 0; i < appointments.length; i++) {

                        if (counter > 0 && counter % 5 == 0) {
                            timeout += 1000;
                        }
                        setTimeout(setMarker(appointments[i]), timeout);
                        counter++;
                    }
                }
            });
        }

        function setMarker(appointment) {
            var adrs = appointment.Address + ", " + appointment.TownCity + ", " + appointment.County;

            // Check if a property address is already processed cancel geocode call and return.
            if ($.inArray(adrs, markersAddress) >= 0) {
                console.log("skiped:" + adrs);
                return;
            }

            geocoder = new google.maps.Geocoder();

            geocoder.geocode({ 'address': adrs }, function (results, status) {
                geoCodeStatus = status;
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(status + ":" + adrs + "Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: adrs,
                        icon: "https://maps.google.com/mapfiles/kml/paddle/ylw-circle.png"
                    });
                    markers.push(marker);
                    markersAddress.push(adrs);
                }
                else {
                    console.log(status + ":" + adrs);
                }
            });
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setAllMap(null);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
            markersAddress = [];
        }

        google.maps.event.addDomListener(window, 'load', showMap);
    
    </script>
    <style>
        .main
        {
            width: 100%;
            overflow: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server" ID="updPanelControls">
        <ContentTemplate>
        
    <asp:Panel ID="pnlControls" runat="server">
        <asp:Panel ID="pnlMessage" runat="server" Visible="false" style=" margin-left:21px;">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <custom:CustomerDetails ID="custDetails" runat="server" StartString="Next available appointment for:">
        </custom:CustomerDetails>
        <table width="100%">
            <tr>
                <td style="min-width: 700px;">
                    <div class="OuterBorder">
                        <asp:UpdatePanel ID="updPanelAvailableAppointments" runat="server">
                            <ContentTemplate>
                                <div class="gridBorder">
                                    <div style="width: 100%; background-color: Silver; height: 25px;">
                                        <div class="grid_temp_faults_filter" style="margin-left: 10px;">
                                            <b>Filter available operatives By:</b>
                                        </div>
                                        <div class="grid_temp_faults_patch">
                                            <b>Patch:</b>
                                            <asp:CheckBox ID="chkPatch" runat="server" Checked="true" OnCheckedChanged="chkPatch_CheckedChanged"
                                                AutoPostBack="true" />
                                        </div>
                                        <div class="grid_temp_faults_date">
                                            <b>Due Date:</b>
                                            <asp:CheckBox ID="chkDueDate" runat="server" Checked="true" OnCheckedChanged="chkDueDate_CheckedChanged"
                                                AutoPostBack="true" />
                                        </div>
                                        <div class="grid_temp_faults_fromdate_filter">
                                            <b>From Date:</b>
                                            <asp:TextBox runat="server" ID="txtFromDate" Width="100" OnTextChanged="txtfromDate_TextChanged" AutoPostBack="true"  />
                                            <cc1:CalendarExtender ID="calExtendettxtFromDate" runat="server" TargetControlID="txtFromDate"
                                                Format="dd/MM/yyyy"></cc1:CalendarExtender> 
                                        </div>
                                        <div class="grid_temp_faults_default">
                                            <b>Default Patch:
                                                <asp:Label ID="lblDefaultPatch" runat="server"></asp:Label></b>
                                        </div>
                                    </div>
                                    <asp:GridView ID="grdFaults" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                        EmptyDataText="No fault data found!" GridLines="None" CssClass="gridfaults">
                                        <Columns>
                                            <asp:BoundField HeaderText="JSN:" DataField="JobSheetNumber" />
                                            <asp:TemplateField HeaderText="Location:">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                                    >
                                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>' Font-Bold="True"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Wrap="False" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Duration:">
                                                <ItemTemplate>
                                                    <uc:faultDuration ID="ucFaultDuration" runat="server" Duration='<%# Eval("Duration") %>'
                                                        DoneButtonCommandArguments='<%# Eval("FaultLogId") %>' OnDoneButtonClick="DoneButton_Clicked" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Trade:" DataField="TradeName" />
                                            <asp:BoundField HeaderText="Due:" DataField="DueDate" />
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:GridView>
                                </div>
                                <br />
                                <div class="gridBorder">
                                    <asp:GridView ID="grdOperative" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                        GridLines="None" CssClass="gridfaults" EmptyDataText="No Operative data exist!">
                                        <Columns>
                                            <asp:BoundField HeaderText="Operative:" DataField="operative:" />
                                            <asp:BoundField HeaderText="Patch:" DataField="Patch:" />
                                            <asp:BoundField HeaderText="Start Date:" DataField="Start Date:" />
                                            <asp:BoundField HeaderText="End Date:" DataField="End Date:" />
                                            <asp:BoundField HeaderText="From:" DataField="From:" />
                                            <asp:BoundField HeaderText="To:" DataField="To:" />
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnSelect" runat="server" CausesValidation="false" Text="Select"
                                                        CommandName="Select" CommandArgument='<%# String.Format("{0} | {1} |{2} | {3}", Eval("Id"), Eval("operative:"), Eval("Start Date:"), Eval("End Date:")) %>'
                                                        OnClick="btnSelect_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:GridView>
                                </div>
                                <div style="float: right; margin-right: 6px; margin-top: 2px;">
                                    <asp:Button Text="< Back" ID="btnBack" runat="server" CssClass="padButton" />
                                    <asp:Button Text="Refresh List" ID="btnRefresh" runat="server" CssClass="padButton" />
                                    <asp:Button Text="View Calendar" ID="btnViewCalender" runat="server" CssClass="padButton" />
                                </div>
                                <div class="clear" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
                <td style="vertical-align: top;">
                    <div class="appointmentsMap" id="mapContainer">
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    <!-- ModalPopupExtender -->
    <!-- POPUP Appointment Arranged (START) -->
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnHidden2" runat="server" Text="" Style="display: none;" />
            <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
            <ajaxtoolkit:modalpopupextender id="popupAppointmentConfirmed" runat="server" popupcontrolid="pnlAppointmentArranged"
                targetcontrolid="btnHidden2" cancelcontrolid="btnHidden3" backgroundcssclass="modalBackground">
            </ajaxtoolkit:modalpopupextender>
            <asp:Panel ID="pnlAppointmentArranged" runat="server" CssClass="modalPopup" Style="height: 200px;
                width: 400px; overflow: auto;">
                <b>Appointment arranged: </b>
                <hr style="height: 2px;" />
                <br />
                <br />
                <br />
                <div style="height: auto; clear: both; text-align: center;">
                    Your appointment has been scheduled, and
                    <br />
                    <asp:Label ID="lblAptArrangedOperative" runat="server" Text="" Font-Bold="true"></asp:Label>
                    <br />
                    has received a notification alert on his iPhone.
                </div>
                <br />
                <br />
                <div style="width: 100%; text-align: left; clear: both;">
                    <div style="float: right;">
                        <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="margin_right20" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--  POPUP Appointment Arranged (END) -->
    <!-- POPUP Confirm Appointment Detail (START) -->
    <asp:Button ID="btnHidden1" runat="server" Text="" Style="display: none;" />
    <ajaxtoolkit:modalpopupextender id="popupConfirmAppointmentDetail" runat="server"
        popupcontrolid="pnlConfirmAppointmentDetail" targetcontrolid="btnHidden1" cancelcontrolid="btnGoBack"
        backgroundcssclass="modalBackground">
    </ajaxtoolkit:modalpopupextender>
    <asp:Panel ID="pnlConfirmAppointmentDetail" runat="server" CssClass="modalPopup"
        Style="height: 265px; width: 400px;">
        <asp:ImageButton ID="imgBtnCloseAppointmentDetail" runat="server" Style="position: absolute;
            top: -13px; right: -15px; width: 22px; height: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
            BorderWidth="0" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="height: auto; clear: both;">
                    <table id="tblConfirmAppointment" style="width: 400px; margin-top: 10px; text-align: left;">
                        <tr>
                            <td colspan="2">
                                <b>Confirm appointment details: </b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr style="border-width: 2px; height: 2px" />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div style="float: right;">
                                    <asp:LinkButton ID="lnkBtnNextPopupFault" runat="server">Next Fault &gt;</asp:LinkButton></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Operative:
                            </td>
                            <td>
                                <asp:Label ID="lblPopOperative" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Location:
                            </td>
                            <td>
                                <asp:Label ID="lblPopupAreaName" runat="server"></asp:Label>
                                &nbsp;&gt;
                                <asp:Label ID="lblPopupFaultDescription" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Duration:
                            </td>
                            <td>
                                <asp:Label ID="lblPopDuration" runat="server" Text=""></asp:Label>
                                &nbsp hours
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trade:
                            </td>
                            <td>
                                <asp:Label ID="lblPopTrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Due:
                            </td>
                            <td>
                                <asp:Label ID="lblPopDue" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date:
                            </td>
                            <td>
                                <asp:Label ID="lblPopAppointmentStartDateTime" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                End Date:
                            </td>
                            <td>
                                <asp:Label ID="lblPopAppointmentEndDateTime" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div style="text-align: left; clear: both;">
                        <div style="float: right;">
                            <asp:Button ID="btnGoBack" runat="server" Text="Go Back" CssClass="margin_right20" />
                            <asp:Button ID="btnPopupConfirmAppointment" runat="server" Text="Confirm Appointment"
                                CssClass="margin_right20" />
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!-- POPUP Confirm Appointment Detail (END) -->
    <!-- ModalPopupExtender -->
</asp:Content>
