﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject

Public Class PrintSubContractorJobSheetSummaryUpdate
    Inherits PageBase

#Region "Data Members"
    Dim FaultBL As New FaultBL()
    Dim jSNClass As String = String.Empty
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                Me.getSetQueryStringParams()
                If uiMessageHelper.IsError = False Then
                    Me.populateJobSheetSummary(jSNClass)
                End If
            End If
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Script", "Clickheretoprint()", True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString("JobSheetNumber")) Then
            jSNClass = Request.QueryString("JobSheetNumber").ToString
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidJobSheetNumber
        End If
    End Sub


    Private Sub populateJobSheetSummary(ByRef jsn As String)

        Dim dsJobSheetSummary As New DataSet()

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)

        FaultBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)

        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0)

                lblFaultLogID.Text = .Item("FaultLogID")
                lblJSN.Text = jsn
                lblStatus.Text = .Item("FaultStatus") '"In Progress"
                lblContractor.Text = .Item("Contractor")
                lblPriority.Text = .Item("priority")
                lblCompletionDateTime.Text = .Item("completionDue")
                lblOrderDate.Text = .Item("orderDate")
                lblLocation.Text = .Item("Location")
                lblDescription.Text = .Item("Description")
                lblTrade.Text = .Item("Trade")
                lblNotes.Text = .Item("Notes")

                lblClientName.Text = .Item("ClientName")
                lblClientNameHeader.Text = lblClientName.Text

                lblClientStreetAddress.Text = .Item("ClientStreetAddress")
                lblClientStreetAddressHeader.Text = lblClientStreetAddress.Text

                lblClientCity.Text = .Item("ClientCity")
                lblClientCityHeader.Text = lblClientCity.Text

                lblClientPostCode.Text = .Item("ClientPostCode")
                lblClientRegion.Text = .Item("ClientRegion")

                lblClientTelPhoneNumber.Text = .Item("ClientTel")
                lblClientTelPhoneNumberHeader.Text = lblClientTelPhoneNumber.Text

                lblClientMobileNumber.Text = .Item("ClientMobile")
                lblClientEmail.Text = .Item("ClientEmail")

            End With
        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

End Class