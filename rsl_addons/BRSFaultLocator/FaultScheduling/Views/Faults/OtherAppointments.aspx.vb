﻿Imports FLS_BusinessLogic
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class OtherAppointments
    Inherits PageBase
    'TODO (BAL) we need to disscuss about the planned maintainance protion of the form.
#Region "Properties "
    Dim objFaultBL As FaultBL = New FaultBL()
    Dim faultLogId As Integer = 0
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' This event handles the load event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'TODO we need to put update panel across the gridviews.
        Try
            If Not IsPostBack Then

                'Changed - By Behroz - 19/02/2013 - Start
                'Dim customerId As Integer = 2043
                'Dim propertyId As String = "A010060001"
                Dim propertyId As String = SessionManager.getPropertyId()
                Dim customerId As String = SessionManager.getCustomerId()

                'fillGrids(customerId, propertyId)
                fillGrids(customerId, propertyId)
                'Changed - By Behroz - 19/02/2013 - End
                If Not (objFaultBL.GetWarratiesList(propertyId).Tables(0).Rows.Count = 0) Then
                    popupWarranties.Show()
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Pre Render"
    ''' <summary>
    ''' This event handles the pre-render event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim faultId As Integer = CType(SessionManager.getFaultId(), Integer)
        If faultId = 0 Then
            btnContinue.Enabled = False
        Else
            btnContinue.Enabled = True
            btnContinue.PostBackUrl = PathConstants.MoreDetails + GeneralHelper.getSrc(PathConstants.OtherAppointmentSrcVal)
        End If
        Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.SearchFault, GeneralHelper.getSrc(PathConstants.OtherAppointmentSrcVal))

    End Sub
#End Region

#Region "Controls Events"

#Region "grd Faults Row Command"
    ''' <summary>
    ''' This event handles the rowCommand event of the grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdFaults_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        Try
            'from there we move to the next page where required.
            Dim grd As GridView = CType(e.CommandSource, GridView)
            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim keyValue As String = grd.DataKeys(index).Value
            'Changed - By behroz - 19/02/2013 - Start
            SessionManager.setFaultLogId(keyValue)
            Response.Redirect(PathConstants.RecallDetails + GeneralHelper.getSrc(PathConstants.OtherAppointmentSrcVal), True)
            'Changed - By behroz - 19/02/2013 - End

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try
    End Sub
#End Region

#Region "grd Current Faults Row Command"
    ''' <summary>
    ''' This event handles the View button click of grdCurrentFaults button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdCurrentFaults_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCurrentFaults.RowCommand
        Try
            Dim grd As GridView = CType(e.CommandSource, GridView)
            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim keyValue As String = grd.DataKeys(index).Value

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try

    End Sub
#End Region

#Region "btn View Current Faults Click"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnViewCurrentFaults_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim appointmentId As Integer = Convert.ToInt32(sender.CommandArgument)
            Dim dt As DataTable = New DataTable()
            dt = Me.filterCurrentFaults(appointmentId)
            Me.SetCurrentFaultsInSession(dt)

            'Set tradeid in session, it is specifically used to select patch filters on calendar.
            If dt.Rows.Count > 0 Then
                SessionManager.setTradeId(dt.Rows(0).Item("TradeID"))
            Else
                SessionManager.removeTradeId()
            End If

            Response.Redirect(PathConstants.CancellationAppointmentSummary + GeneralHelper.getSrc(PathConstants.OtherAppointmentSrcVal) + "&" + PathConstants.AppointmentId & "=" + appointmentId.ToString())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try


    End Sub
#End Region

#Region "btn View Current Faults Click"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnViewCurrentGasAppointment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim appointmentId As Integer = Convert.ToInt32(sender.CommandArgument)
            Dim dt As DataTable = New DataTable()
            dt = Me.filterCurrentGasAppointment(appointmentId)
            Me.SetCurrentGasAppointmentInSession(dt)

            Response.Redirect(PathConstants.CancellationAppointmentSummaryForGas + GeneralHelper.getSrc(PathConstants.OtherAppointmentSrcVal) + "&" + PathConstants.AppointmentId & "=" + appointmentId.ToString())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try


    End Sub
#End Region

#Region "grd Current Faults Row Data Bound"

    Protected Sub grdCurrentFaults_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCurrentFaults.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblAppointmentStatus As Label = CType(e.Row.FindControl("lblAppointmentStatus"), Label)

                Dim appointmentStatus As String = lblAppointmentStatus.Text
                If appointmentStatus = "Assigned To Contractor" Then
                    Dim btnView As Button = CType(e.Row.FindControl("btnViewCurrentFaults"), Button)
                    btnView.Visible = False
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try
    End Sub
#End Region

#End Region

#End Region

#Region "functions"

#Region "Grid Functions "
    'Change - Behroz - 20/02/2013 - Start
    'Private Sub fillGrids(ByVal customerId As Integer, ByVal propertyId As String)
    Private Sub fillGrids(ByVal customerId As Integer, ByVal propertyId As String)
        'Change - Behroz - 20/02/2013 - End
        Dim dsAppointments As DataSet = New DataSet()
        dsAppointments = objFaultBL.GetFaultsAndAppointments(customerId, propertyId)
        If (dsAppointments.Tables.Count > 0) Then
            BindGrid(grdCurrentFaults, dsAppointments.Tables(ApplicationConstants.CurrentFaultsDataTable))
            BindGrid(grdRecall, dsAppointments.Tables(ApplicationConstants.RecallFaultsDataTable))
            BindGrid(grdGasServicingAppointments, dsAppointments.Tables(ApplicationConstants.GasAppointmentssDataTable))
            BindGrid(grdPlannedAppointments, dsAppointments.Tables(ApplicationConstants.PlannedDataTable))
            Me.SetCurrentFaultsInSession(dsAppointments.Tables(ApplicationConstants.CurrentFaultsDataTable))
            Me.SetCurrentGasAppointmentInSession(dsAppointments.Tables(ApplicationConstants.GasAppointmentssDataTable))
        End If
    End Sub

    ''' <summary>
    ''' This method binds the source to the grid
    ''' </summary>
    ''' <param name="grid"></param>
    ''' <param name="datasource"></param>
    ''' <remarks></remarks>
    Private Sub BindGrid(ByRef grid As GridView, ByVal datasource As DataTable)
        grid.DataSource = datasource
        grid.DataBind()
    End Sub
#End Region

#Region "Fill warranties Modal Popup "
    'Change - Behroz - 20/02/2013 - Start
    'Private Sub fillWarrantiesList(ByVal propertyId As String)
    Private Sub fillWarrantiesList(ByVal propertyId As String)
        'Change - Behroz - 20/02/2013 - End
        Dim dsWarranties As DataSet = New DataSet()
        dsWarranties = objFaultBL.GetWarratiesList(propertyId)
        If (dsWarranties.Tables.Count > 0) Then
            BindGrid(grdCurrentFaults, dsWarranties.Tables(ApplicationConstants.CurrentFaultsDataTable))
        End If
    End Sub

#End Region

#Region "Save Current Faults In Session"
    Private Sub SetCurrentFaultsInSession(ByRef dt As DataTable)
        SessionManager.setConfirmedFaults(dt)
    End Sub
#End Region

#Region "get Current FaultscFrom Session"
    Private Function getCurrentFaultsFromSession() As DataTable
        Return SessionManager.getConfirmedFaults()
    End Function
#End Region

#Region "get Filtered Current Faults"
    Private Function filterCurrentFaults(ByVal appointmentId As Integer) As DataTable

        SessionManager.setAppointmentId(appointmentId)
        Dim currentFaultsDt As DataTable = New DataTable()
        currentFaultsDt = Me.getCurrentFaultsFromSession()
        SessionManager.setIsAppointmentRearrange(True)
        Dim dv As DataView = New DataView()
        dv = currentFaultsDt.DefaultView()
        dv.RowFilter = "AppointmentId = " + appointmentId.ToString()
        currentFaultsDt = dv.ToTable()
        Return currentFaultsDt
    End Function
#End Region

#Region "Save Current Gas Appointments In Session"
    Private Sub SetCurrentGasAppointmentInSession(ByRef dt As DataTable)
        SessionManager.setConfirmedGasAppointment(dt)
    End Sub
#End Region

#Region "get Current FaultscFrom Session"
    Private Function getCurrentGasAppointmentFromSession() As DataTable
        Return SessionManager.getConfirmedGasAppointment()
    End Function
#End Region

#Region "get Filtered Current Faults"
    Private Function filterCurrentGasAppointment(ByVal appointmentId As Integer) As DataTable

        SessionManager.setAppointmentId(appointmentId)
        Dim currentFaultsDt As DataTable = New DataTable()
        currentFaultsDt = Me.getCurrentGasAppointmentFromSession()
        SessionManager.setIsAppointmentRearrange(True)
        Dim dv As DataView = New DataView()
        dv = currentFaultsDt.DefaultView()
        dv.RowFilter = "APPOINTMENTID = " + appointmentId.ToString()
        currentFaultsDt = dv.ToTable()
        Return currentFaultsDt
    End Function
#End Region

#Region "Replace Keywords"
    Public Function ReplaceKeywords(ByVal time As String) As String

        Return "<span class='highlight'>" + time + "</span>"

    End Function
#End Region

#Region "Highlight Text"
    Protected Function HighlightText(ByVal inputText As Object) As String

        If (Not IsDBNull(inputText)) Then
            Dim dateTime As String = inputText.ToString()
            Dim dateTimeArr() As String = dateTime.Split(" ")
            Dim timeStr As String = dateTimeArr(0)
            Dim dateStr As String = dateTimeArr(1)
            dateTime = ReplaceKeywords(timeStr) + " " + dateStr
            Return dateTime
        Else
            Return ""
        End If

    End Function
#End Region

#Region "Warranties Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

    Protected Sub AssignToContractorBtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucWarranties.CloseButtonClicked
        Try
            popupWarranties.Hide()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region


End Class