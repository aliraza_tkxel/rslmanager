﻿Imports System
Imports FLS_BusinessObject
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_Utilities
Imports System.Drawing
Imports System.Threading

Public Class FaultBasket
    Inherits PageBase

    Dim isRecall As Boolean = False
    Dim source As String = String.Empty


#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        

        If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
            isRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
        End If

        If (Not IsPostBack) Then
            prePopulatedValues()
            populateCustomerObject()
            populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
        End If
        AddHandler ucFaultNotes.CloseButtonClicked, AddressOf btnClose_Click
    End Sub

#End Region

#Region "Sort basket"
    Protected Sub gdFaultBasket_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gdFaultBasket.Sorting
        ViewState(ViewStateConstants.SortExpression) = e.SortExpression
        ViewState(ViewStateConstants.SortDirection) = "asc"
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
    End Sub
#End Region

#Region "Page Index Changing"
    Protected Sub gdFaultBasket_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdFaultBasket.PageIndexChanging
        gdFaultBasket.PageIndex = e.NewPageIndex
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
    End Sub
#End Region

#Region "Bind Row Data"
    Protected Sub gdFaultBasket_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gdFaultBasket.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim lbIsConfirmed As LinkButton = DirectCast(e.Row.FindControl("isAppointmentConfirmed"), LinkButton)
                Dim lbIsContractor As String = DirectCast(e.Row.FindControl("isContractor"), Label).Text
                Dim checkBox As CheckBox = DirectCast(e.Row.FindControl("chkGroup"), CheckBox)
                checkBox.Attributes.Add("onClick", "highlightDescription(" + e.Row.RowIndex.ToString + ",this.checked);")

                validateDurationFormat(e.Row)

                'Enable Assign to Contractor button only if Sub-Contractor is checked in Repairs with Faults
                Dim isSubContractor As String = DirectCast(e.Row.FindControl("isContractor"), Label).Text
                If isSubContractor.Equals("True") Then

                    Dim btnAssignToContractor As Button = DirectCast(e.Row.FindControl("btnAssignToContractor"), Button)
                    btnAssignToContractor.Visible = True
                Else
                    Dim btnAssignToContractor As Button = DirectCast(e.Row.FindControl("btnAssignToContractor"), Button)
                    btnAssignToContractor.Visible = False
                End If

                ' Check whether appointment is confirmed against this fault
                Dim checkConfirmed As Boolean
                Boolean.TryParse(lbIsConfirmed.CommandArgument.ToString.ToLower, checkConfirmed)

                Dim checkIsContractor As Boolean
                Boolean.TryParse(lbIsContractor.ToString.ToLower, checkIsContractor)

                If (checkConfirmed) Then
                    Dim lbFaultLogId As LinkButton = DirectCast(e.Row.FindControl("imgCompleted"), LinkButton)

                    If (lbFaultLogId.CommandArgument = Nothing) Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.JSNNotAvailable, True)
                    Else
                        Dim faultLogId As Integer = Convert.ToInt32(lbFaultLogId.CommandArgument.ToString)
                        If (faultLogId > 0) Then
                            'Populate Appointment Confirmed Controls
                            manipulateAppointmentConfirmedControls(e.Row, faultLogId)
                            
                        Else
                            Dim btnAssignToContractor As Button = DirectCast(e.Row.FindControl("btnAssignToContractor"), Button)
                            If isSubContractor.Equals("True") Then
                                btnAssignToContractor.Visible = True
                            Else
                                btnAssignToContractor.Visible = False
                            End If

                        End If


                    End If


                    'Else
                    '    'Populate Contractors Dropdown

                    '    ' If isContractor = 1 then populate contractor drop down
                    '    'populateContractorsDropdownList(e.Row, checkIsContractor)


                    '    Dim btnAssignToContractor As Button = DirectCast(e.Row.FindControl("btnAssignToContractor"), Button)
                    '    btnAssignToContractor.Visible = True

                End If

               


            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Check Box Event"
    Protected Sub chkGroup_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim chkStatus As CheckBox = DirectCast(sender, CheckBox)
            Dim row As GridViewRow = DirectCast(chkStatus.NamingContainer, GridViewRow)
            Dim lblDescription As Label = DirectCast(row.FindControl("lblDescription"), Label)

            If chkStatus.Checked = True Then
                lblDescription.Font.Bold = True
            Else
                lblDescription.Font.Bold = False

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub


#End Region


#Region "Add More Faults"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs)
        mdlPopupFaultNotes.Hide()
        updPanelFaultBasket.Update()
    End Sub
#End Region
#Region "ModalPopups"
    Protected Sub gdFaultBasket_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        If e.CommandName.Equals("View") Then
            pnlFaultNotes.Visible = True
            'ucFaultNotes.PopulateControl()
            mdlPopupFaultNotes.Show()
        End If
    End Sub

    Protected Sub lblDescription_Clicked(ByVal sender As Object, ByVal e As EventArgs)
        'Get Row Number
        Dim lnkBtnDescription As LinkButton = DirectCast(sender, LinkButton)
        Dim row As GridViewRow = DirectCast(lnkBtnDescription.NamingContainer, GridViewRow)
        Dim rowNumber As Integer = row.RowIndex

        'Get Location,Notes
        Dim Location, Notes As String
        Dim TempFaultID As Integer


        'Location = gdFaultBasket.Rows(rowNumber).Cells(1).Text
        'Notes = gdFaultBasket.Rows(rowNumber).Cells(2).Text

        Location = DirectCast(row.FindControl("lblLocation"), Label).Text
        Notes = DirectCast(row.FindControl("hfNotes"), HiddenField).Value
        TempFaultID = DirectCast(row.FindControl("hfTempFaultID"), HiddenField).Value

        pnlFaultNotes.Visible = True
        ucFaultNotes.PopulateControl(Location, Notes, TempFaultID) 'location & notes
        mdlPopupFaultNotes.Show()

    End Sub
#End Region

#End Region
#Region "Schedule Selected Faults"
    Protected Sub btnScheduleSelectedFaults_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScheduleSelectedFaults.Click

        Dim checkedTempFaultList As New ArrayList
        Dim jobType As Integer = 0

        Try
            If (Not checkAnySelected(checkedTempFaultList)) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectFault, True)
                'ElseIf (Not checkJobSimilarity(checkedTempFaultList, jobType)) Then
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.JobsNotSame, True)
            Else
                If (jobType = 0) Then
                    SessionManager.setTemporaryFaultBo(checkedTempFaultList)

                    If (rbtnGroupTradeYes.Checked) Then
                        SessionManager.setGroupBy(True)
                    Else
                        SessionManager.setGroupBy(False)
                    End If

                    If isRecall Then
                        Response.Redirect(PathConstants.AvailableAppointments + GeneralHelper.getSrc(PathConstants.FaultBasketSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
                    Else
                        Response.Redirect(PathConstants.AvailableAppointments + GeneralHelper.getSrc(PathConstants.FaultBasketSrcVal))
                    End If


                Else

                    saveSubcontractors()
                    SessionManager.setTemporaryFaultBo(checkedTempFaultList)

                    If (Not IsNothing(Request.QueryString(PathConstants.IsRecall))) Then
                        Response.Redirect(PathConstants.JobSheetSummarySubcontractor + GeneralHelper.getSrc(PathConstants.FaultBasketSrcVal) + "&" + PathConstants.IsRecall + "=" + Request.QueryString(PathConstants.IsRecall))
                    Else
                        Response.Redirect(PathConstants.JobSheetSummarySubcontractor + GeneralHelper.getSrc(PathConstants.FaultBasketSrcVal))
                    End If
                End If
            End If
        Catch ex As ThreadAbortException
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub
#End Region

#Region "Add More Faults"
    Protected Sub btnAddMoreFaults_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddMoreFaults.Click
        redirectToSearch()
    End Sub
#End Region

#Region "Assign To Contractor Click"
    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim objFaultBL As FaultBL = New FaultBL()
        Dim dsFaultAssignToContractor As DataSet = New DataSet()
        Dim btnAssignToContractor As Button = DirectCast(sender, Button)
        Dim tempFaultId As Int32 = Convert.ToInt32(btnAssignToContractor.CommandArgument)

        objFaultBL.getFaultAssignToContractor(dsFaultAssignToContractor, tempFaultId)
        Dim dtFaultAssignToContractor = dsFaultAssignToContractor.Tables(ApplicationConstants.FaultInfoDt)
        Dim objAssignToContractorBo As AssignToContractorBO = New AssignToContractorBO()


        If (dtFaultAssignToContractor.Rows.Count > 0) Then

            objAssignToContractorBo.ReportedFault = dtFaultAssignToContractor.Rows(0)("Description")
            objAssignToContractorBo.FaultId = Convert.ToInt32(dtFaultAssignToContractor.Rows(0)("FaultId"))
            objAssignToContractorBo.FaultWorksRequired = dtFaultAssignToContractor.Rows(0)("WorksRequired")
            objAssignToContractorBo.DefaultNetCost = dtFaultAssignToContractor.Rows(0)("NetCost")
            objAssignToContractorBo.DefaultVat = dtFaultAssignToContractor.Rows(0)("Vat")
            objAssignToContractorBo.DefaultGross = dtFaultAssignToContractor.Rows(0)("Gross")
            objAssignToContractorBo.DefaultVatId = dtFaultAssignToContractor.Rows(0)("VatRateId")
            objAssignToContractorBo.PropertyId = dtFaultAssignToContractor.Rows(0)("PropertyId")
            objAssignToContractorBo.TempFaultId = tempFaultId

            SessionManager.removeAssignToContractorBO()
            SessionManager.setAssignToContractorBO(objAssignToContractorBo)

            pnlAssignToContractor.Visible = True
            ucAssignToContractor.PopulateControl()
            popupAssignToContractor.Show()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidTempFaultId, True)
        End If

    End Sub
#End Region


#Region "Close basket window and return to search screen"
    Protected Sub btnCloseWindow_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCloseWindow.Click
        'redirectToCustomer()
    End Sub
#End Region




#Region "Functions"


#Region "Assign to Contractor Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

    Protected Sub AssignToContractorBtnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ucAssignToContractor.CloseButtonClicked
        Try

            popupAssignToContractor.Hide()
            Response.Redirect(Request.RawUrl)
            populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Populate Fault Basket"
    Sub populateFaultBasket(ByVal sortExp As String, ByVal sortDir As String)

        Dim confirmedJsn As ArrayList

        If (ViewState(ViewStateConstants.ConfirmedJsnList) Is Nothing) Then
            confirmedJsn = New ArrayList
        Else
            confirmedJsn = DirectCast(ViewState(ViewStateConstants.ConfirmedJsnList), ArrayList)
        End If

        confirmedJsn.Clear()
        ViewState.Add(ViewStateConstants.ConfirmedJsnList, confirmedJsn)

        Dim objFaultBO As FaultBO = New FaultBO()
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim dsFaultBasket As DataSet = New DataSet()
        Dim basketDataView As New DataView()

        objFaultBO.IsRecall = isRecall

        objFaultBO.CustomerId = SessionManager.getCustomerId()
        objFaultBO.PropertyId = SessionManager.getPropertyId()

        objFaultBL.getFaultBasket(objFaultBO, dsFaultBasket)
        basketDataView = dsFaultBasket.Tables(0).DefaultView

        If Not String.IsNullOrEmpty(sortExp) Then
            basketDataView.Sort = String.Format("{0} {1}", sortExp, sortDir)
        End If

        If (dsFaultBasket.Tables(0).Rows.Count = 0) Then
            btnScheduleSelectedFaults.Enabled = False
        Else
            btnScheduleSelectedFaults.Enabled = True
        End If

        gdFaultBasket.DataSource = basketDataView
        gdFaultBasket.DataBind()

        checkAllFaultsConfirmed(dsFaultBasket)

    End Sub
#End Region

#Region "Check all faults confirmed"
    Sub checkAllFaultsConfirmed(ByVal dsFaultBasket As DataSet)

        Dim confirmedJsn As ArrayList
        confirmedJsn = DirectCast(ViewState(ViewStateConstants.ConfirmedJsnList), ArrayList)


        ' Check for all appointments confirmed
        If (confirmedJsn.Count = dsFaultBasket.Tables(0).Rows.Count And Not confirmedJsn.Count = 0) Then

            ' Hide AddMoreFault , ScheduleSelectedFaults Buttons & RadioBox
            btnAddMoreFaults.Visible = False
            btnScheduleSelectedFaults.Visible = False
            tradeGroupPanel.Visible = False

            ' Show Success Panel Section with JSN list
            dlSubmittedJsn.DataSource = confirmedJsn
            dlSubmittedJsn.DataBind()
            tblSubmitted.Visible = True
            btnCloseWindow.Visible = True

            ' Hide Header Text
            gdFaultBasket.HeaderRow.Cells(6).Visible = False
            deleteAllConfirmedFaults()

        End If


    End Sub
#End Region

#Region "Populate Customer Info From Session"
    Sub prePopulatedValues()
        source = SessionManager.getPageSource()
        ViewState(ViewStateConstants.SortExpression) = String.Empty
        ViewState(ViewStateConstants.SortDirection) = String.Empty
    End Sub
#End Region

#Region "Redirect to Search screen"
    Private Sub redirectToSearch()
        If isRecall Then
            Response.Redirect(PathConstants.SearchFault + GeneralHelper.getSrc(PathConstants.FaultBasketSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.SearchFault + GeneralHelper.getSrc(PathConstants.FaultBasketSrcVal))
        End If
    End Sub
#End Region

#Region "Redirect to Customer Records"
    Private Sub redirectToCustomer()
        Dim customerId As Integer = SessionManager.getCustomerId()
        Response.Redirect(PathConstants.CustomerRecords + Convert.ToString(customerId))
    End Sub
#End Region

#Region "Delete All Confirmed Faults"
    Sub deleteAllConfirmedFaults()
        Dim linkBtn As LinkButton
        Dim tempFaultId As Integer

        ' Delete Faults From Basket
        For Each basketGridRows As GridViewRow In gdFaultBasket.Rows
            linkBtn = basketGridRows.FindControl("imgDelete")
            tempFaultId = Convert.ToInt32(linkBtn.CommandArgument.ToString)
            deleteFaultFromBasket(tempFaultId, 0)
        Next
    End Sub
#End Region

#Region "Delete Fault From Basket"

    Protected Sub deleteFaultFromBasket(ByVal tempFaultId As Integer, ByVal type As Integer)

        Dim objFaultBL As FaultBL = New FaultBL()

        Try
            Dim result As Integer = objFaultBL.deleteFaultFromBasket(tempFaultId)

            If (type = 1) Then

                If result = -1 Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultDoesNotExist, True)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultDeletedSuccessfuly, False)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Delete Fault From Basket"

    Protected Sub deleteFault(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim tempFaultId As Integer = CType(CType(sender, LinkButton).CommandArgument, Int32)
        deleteFaultFromBasket(tempFaultId, 1)
        populateFaultBasket(ViewState(ViewStateConstants.SortExpression), ViewState(ViewStateConstants.SortDirection))


    End Sub
#End Region

#Region "Check if there is any fault slected"
    Function checkAnySelected(ByRef checkedTempFaultList As ArrayList) As Boolean

        Dim chkStatus As CheckBox
        Dim linkBtn As LinkButton
        Dim tempFaultId As Integer
        Dim btnTrade As Button
        Dim tradeId As Integer
        'Dim contractorIndex As Integer
        'Dim ddlContractor As DropDownList

        For Each basketGridRows As GridViewRow In gdFaultBasket.Rows

            chkStatus = basketGridRows.FindControl("chkGroup")
            If (chkStatus.Checked = True) Then

                linkBtn = basketGridRows.FindControl("imgDelete")
                tempFaultId = Convert.ToInt32(linkBtn.CommandArgument.ToString)

                btnTrade = DirectCast(basketGridRows.FindControl("btnTrade"), Button)
                tradeId = Convert.ToInt32(btnTrade.CommandArgument)

                'ddlContractor = DirectCast(basketGridRows.FindControl("ddlContractors"), DropDownList)
                'contractorIndex = ddlContractor.SelectedIndex

                Dim checkedTempFaultBO As New TempFaultBO
                checkedTempFaultBO.TempFaultId = tempFaultId
                checkedTempFaultBO.Trade = tradeId
                'checkedTempFaultBO.Contractor = contractorIndex
                checkedTempFaultList.Add(checkedTempFaultBO)

            End If
        Next

        If (checkedTempFaultList.Count > 0) Then
            Return True
        Else
            Return False
        End If

    End Function
#End Region

#Region "Check all selected faults are of the same job type"
    Function checkJobSimilarity(ByVal checkedTempFaultList As ArrayList, ByRef jobType As Integer) As Boolean

        Dim tempFaultBO As TempFaultBO
        Dim currentIndexType As Integer

        'Get first index value as flag
        Dim firstIndexType As Integer = DirectCast(checkedTempFaultList.Item(0), TempFaultBO).Contractor
        If (firstIndexType > 0) Then
            firstIndexType = -1
        Else
            firstIndexType = 0
        End If

        For i As Integer = 0 To checkedTempFaultList.Count - 1
            tempFaultBO = checkedTempFaultList.Item(i)

            If (tempFaultBO.Contractor = 0) Then
                currentIndexType = 0
            Else
                currentIndexType = -1
            End If

            If (Not currentIndexType = firstIndexType) Then
                Return False
            End If
        Next

        If (firstIndexType = 0) Then
            jobType = 0
        Else
            jobType = 1
        End If

        Return True
    End Function
#End Region

#Region "Save Subcontractors in Basket"
    Sub saveSubcontractors()

        Dim chkStatus As CheckBox
        Dim linkBtn As LinkButton
        Dim ddl As DropDownList
        Dim tempFaultId As Integer
        Dim orgId As Integer
        Dim objFaultBL As FaultBL = New FaultBL()

        For Each basketGridRows As GridViewRow In gdFaultBasket.Rows
            chkStatus = basketGridRows.FindControl("chkGroup")
            If (chkStatus.Checked = True) Then

                linkBtn = basketGridRows.FindControl("imgDelete")
                tempFaultId = Convert.ToInt32(linkBtn.CommandArgument.ToString)

                ddl = basketGridRows.FindControl("ddlContractors")
                If (ddl.SelectedIndex > 0) Then

                    orgId = Convert.ToInt32(ddl.SelectedValue)
                    objFaultBL.updateContractor(tempFaultId, orgId)
                End If
            End If
        Next
    End Sub
#End Region

#Region "Validate duration format"
    Sub validateDurationFormat(ByVal row As GridViewRow)

        Dim btnDuration As Button = DirectCast(row.FindControl("btnDuration"), Button)
        Dim lblDuration As Label = DirectCast(row.FindControl("lblDuration"), Label)

        'Adding 'hours' at end of duration label
        If (btnDuration.CommandArgument = "") Then
            lblDuration.Text = "N/A"
        ElseIf (Convert.ToDouble(btnDuration.CommandArgument.ToString) > 1) Then
            lblDuration.Text = btnDuration.CommandArgument.ToString + " hours"
        Else
            lblDuration.Text = btnDuration.CommandArgument.ToString + " hour"
        End If

    End Sub
#End Region

#Region "Populate Contractors DropdownList"
    Sub populateContractorsDropdownList(ByVal row As GridViewRow, ByVal contractor As Boolean)

        ' Get Trade Type of this row
        Dim tradeType As String = DirectCast(row.FindControl("lblTrade"), Label).Text


        ' Get Contractor's list
        Dim resultDataSet As DataSet = New DataSet()
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim ddl As DropDownList = DirectCast(row.FindControl("ddlContractors"), DropDownList)

        objFaultBL.getContractorsByTrade(tradeType, resultDataSet)

        If (contractor And resultDataSet.Tables(0).Rows.Count > 0) Then
            ddl.DataSource = resultDataSet.Tables(0).DefaultView
            ddl.DataValueField = "orgid"
            ddl.DataTextField = "name"
            ddl.DataBind()
        End If

        Dim item As ListItem = New ListItem("Please Select", "-1")
        ddl.Items.Insert(0, item)

    End Sub
#End Region

#Region "Manipulate Appointment Confirmed Controls"
    Sub manipulateAppointmentConfirmedControls(ByVal row As GridViewRow, ByVal faultLogId As Integer)

        'Fetch Job Sheet Number & Appointment time from DB
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim dsAppointmentDetail As DataSet = New DataSet()
        Dim jsn As String = String.Empty
        Dim time As String = String.Empty
        Dim confirmedJsn As ArrayList = DirectCast(ViewState(ViewStateConstants.ConfirmedJsnList), ArrayList)

        dsAppointmentDetail = objFaultBL.GetAppointmentDetail(faultLogId)

        If (dsAppointmentDetail.Tables(0).Rows.Count > 0) Then
            jsn = dsAppointmentDetail.Tables("AppointmentDetail").Rows(0)(0).ToString()
            time = dsAppointmentDetail.Tables("AppointmentDetail").Rows(0)(1).ToString()

            'Add to Confirmed List
            confirmedJsn.Add(jsn)

        End If

        ViewState.Add(ViewStateConstants.ConfirmedJsnList, confirmedJsn)


        'Hide check box , DropDownList , Delete Button
        Dim cb_Fault As CheckBox = DirectCast(row.FindControl("chkGroup"), CheckBox)
        Dim ddlContractor As DropDownList = DirectCast(row.FindControl("ddlContractors"), DropDownList)
        Dim btnAssignToContractor As Button = DirectCast(row.FindControl("btnAssignToContractor"), Button)
        Dim lbDelete As LinkButton = DirectCast(row.FindControl("imgDelete"), LinkButton)

        cb_Fault.Visible = False
        ddlContractor.Visible = False
        btnAssignToContractor.Visible = False
        lbDelete.Visible = False

        'Show Confirmed Button , JSN , Appointment Time
        Dim lblJsn As Label = DirectCast(row.FindControl("lblJsnCompleted"), Label)
        Dim lblTime As Label = DirectCast(row.FindControl("lblAptTime"), Label)

        lblJsn.Text = jsn
        lblTime.Text = time

        Dim tblConfirmed As Table = DirectCast(row.FindControl("tblSuccess"), Table)
        tblConfirmed.Visible = True

        'Highlight Description
        Dim lblDescription As LinkButton = DirectCast(row.FindControl("lblDescription"), LinkButton)
        lblDescription.Font.Bold = True

    End Sub
#End Region

#End Region
#Region "populate Customer Object"
    Private Sub populateCustomerObject() 'ByRef resultDataSet As DataSet
        Dim resultDataSet As New DataSet()
        Dim objSerFaultBl As SearchFaultBL = New SearchFaultBL()
        Dim customerId As Integer = SessionManager.getCustomerId()
        objSerFaultBl.getRecentFaults(resultDataSet, SessionManager.getPropertyId(), SessionManager.getCustomerId(), "", "")
        Dim customerObj As CustomerBO = New CustomerBO()
        Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()

        customerObj.CustomerId = customerId
        customerObj.Name = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Name").ToString()
        customerObj.Street = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("Address").ToString()
        customerObj.Address = String.Empty
        customerObj.Area = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("County").ToString()
        customerObj.Telephone = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Telephone").ToString()
        customerObj.Mobile = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Mobile").ToString()
        customerObj.Email = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Email").ToString()
        customerObj.City = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("TownCity").ToString()
        customerObj.PostCode = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PostCode").ToString()
        customerObj.PatchId = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchId").ToString()
        customerObj.PatchName = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchName").ToString()
        'fill object for common item of property,scheme and Block
        commonAddressBo.Street = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Address = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Area = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("County").ToString()
        commonAddressBo.City = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("TownCity").ToString()
        commonAddressBo.PostCode = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PostCode").ToString()
        commonAddressBo.PatchId = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchId").ToString()
        commonAddressBo.PatchName = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchName").ToString()
        'store dataset in session for further use in intelligent scheduling
        SessionManager.setCustomerData(customerObj)
        SessionManager.setCommonAddressBO(commonAddressBo)
    End Sub
#End Region
End Class
