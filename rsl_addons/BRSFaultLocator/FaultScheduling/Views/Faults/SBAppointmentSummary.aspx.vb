﻿Imports FLS_BusinessObject
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports System.Net
Imports System.IO
Imports System.Net.Mail
Imports System.Text


Public Class SbAppointmentSummary
    Inherits PageBase

#Region "Properties "


    Dim objFaultBl As FaultBL = New FaultBL()
    Dim isRecall As Boolean = False
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
#End Region

#Region "Events"

#Region "Page Pre Render"
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Dim url As String = Page.Request.UrlReferrer.AbsoluteUri

        If url.Contains(PathConstants.IsRecall) Then
            Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbAppointmentSummarySrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.SbAppointmentSummarySrcVal))
        End If

    End Sub
#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            commonAddressBo = SessionManager.getCommonAddressBO()
            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall) = PathConstants.Yes, True, False)
            End If

            If Not IsPostBack Then
                prePopulatedValues()
                populateAppointmentSummary()
                populateAppointmentDetails()
                btnPopupConfirmAppointment.Enabled = True
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                lnkBtnNextFault.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Button Update Customer Detail Click Event"

    'Protected Sub btnUpdateCustomerDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateCustomerDetail.Click
    '    Try
    '        updateAddress()
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
    '        End If
    '    End Try
    'End Sub

#End Region

#Region "Link Button Next Fault Click Event"

    Protected Sub lnkBtnNextFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnNextFault.Click

        Try
            scrollFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Popup Link Button Next Fault Click Event"

    Protected Sub lnkBtnNextPopupFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnNextPopupFault.Click

        Try

            Me.scrollPopupFaults()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Confirm Appointment Detail Click Event"

    Protected Sub btnConfirmAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirmAppointment.Click

        Try
            Me.populateAppointmentDetailPopup()
            'popupAppointmentConfirmed.Show()
            'popupConfirmAppointmentDetail.Show()
            'pushAppointmentConfirmed(1, 1)
            'confirmAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Popup Confirm Appointment Button Click Event"

    Protected Sub btnPopupConfirmAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupConfirmAppointment.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            Me.confirmAppointment()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Continue button after appointment confirmed."

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinue.Click

        Try

            popupAppointmentConfirmed.Hide()
            Me.navigate()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Go Back Event"

    Protected Sub btnGoBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoBack.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            ViewState.Item(ViewStateConstants.PopupSlideIndex) = 0
            lnkBtnNextPopupFault.Visible = True
            lnkBtnNextPopupFault.Enabled = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Close Event"

    Protected Sub imgBtnCloseAppointmentDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgBtnCloseAppointmentDetail.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            ViewState.Item(ViewStateConstants.PopupSlideIndex) = 0
            lnkBtnNextPopupFault.Enabled = True
            lnkBtnNextPopupFault.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    

#End Region

#Region "Functions"

#Region "Pre Populate Values"

    Sub prePopulatedValues()

        resetSchemeBlockDetail()

        Dim schemeId As Integer = SessionManager.getSchemeId()
        Dim blockId As Integer = SessionManager.getBlockId()
        Dim resultDataSet As New DataSet()
        objFaultBl.getSchemeBlockAddress(resultDataSet, schemeId, blockId)
        If commonAddressBo.Address <> "" Then

            lblScheme.Text = commonAddressBo.Address
            lblSchemeBlockAddress.Text = commonAddressBo.City
            lblCOUNTY.Text = commonAddressBo.Area
            lblPostCode.Text = commonAddressBo.PostCode
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If
       

    End Sub

#End Region



#Region "Populate Appointment Summary"

    Sub populateAppointmentSummary()

        Dim faultsDataView As DataView = New DataView()
        faultsDataView = SessionManager.getSbSelectedTempFaultDv()
       
        If IsNothing(faultsDataView) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else

          
            grdAppointments.DataSource = faultsDataView
            grdAppointments.DataBind()

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.SlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = faultsDataView.ToTable()

            changeFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count = 1 Then
                lnkBtnNextFault.Enabled = False
            End If

            'End If

        End If

    End Sub

#End Region

#Region "Populate Appointment Details"

    Sub populateAppointmentDetails()

        Dim resultDataSet As DataView = New DataView()
        resultDataSet = SessionManager.getSbSelectedTempFaultDv()

        If IsNothing(resultDataSet) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        Dim durationTable As DataTable = New DataTable()
        durationTable = resultDataSet.ToTable()

        Dim length As Integer = 0

        If Not IsNothing(resultDataSet) Then

            length = durationTable.Rows.Count

        End If

        Dim duration As Double = 0

        For i As Integer = 0 To length - 1
            Dim dur As Double = 0
            Double.TryParse(durationTable.Rows(i).Item("Duration").ToString, dur)
            duration = duration + dur
        Next

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAptDataForSbAptSummary()
        '' The below data is hardcoded as there is currently no data in Session **** Abdul Wahhab 14/02/2013 ****
        'appointmentData.Operative = "Jade Burrell"
        'appointmentData.OperativeId = 480
        'appointmentData.Duration = duration
        'appointmentData.Time = "9:00 AM"
        'appointmentData.AppointmentDate = appointmentData.AppointmentDate.ToShortDateString
        'appointmentData.FaultNotes = "The tenant thinks that she has overtightened the hot tap"

        'SessionManager.setSBAptDataForSbAptSummary(appointmentData)
        '' These code lines will need to be removed **** Abdul Wahhab 14/02/2013 ****

        If appointmentData.OperativeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        lblOperative.Text = appointmentData.Operative
        If duration < 1 Or duration = 1 Then
            lblDuration.Text = duration.ToString() + " Hour Approx"
        Else
            lblDuration.Text = duration.ToString() + " Hours Approx"
        End If
        lblTime.Text = appointmentData.StartTime
        lblEndTime.Text = appointmentData.EndTime
        lblDate.Text = Format(appointmentData.AppointmentStartDate, "d MMMM yyyy")
        'lblFaultNotes.Text = appointmentData.FaultNotes

    End Sub

#End Region

#Region "Scroll Faults"

    Sub scrollFaults()

        Dim resultDataSet As DataView = New DataView()
        resultDataSet = SessionManager.getSbSelectedTempFaultDv()

        If IsNothing(resultDataSet) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = resultDataSet.ToTable()

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.SlideIndex)

            changeFaultValues(slideDataSet, slideIndex)

            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                lnkBtnNextFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.SlideIndex) = slideIndex
            End If

        End If

    End Sub

#End Region

#Region "Confirm Appointment"

    Sub confirmAppointment()

        Dim appointmentId As Integer = -1
        Dim tempCount As Integer = 0
        Dim isAppointmentPushed As Boolean = False
        Dim objFaultBL As FaultBL = New FaultBL
        Dim resultDataset As DataSet = New DataSet
        Dim jobSheetList As String = ""

        Me.saveAppointment(appointmentId, tempCount)

        If (Not appointmentId = -1) Then

            'When appointment is confirmed and saved sucessfully, disable confirm appointment button on appointment summary.
            btnConfirmAppointment.Enabled = False

            isAppointmentPushed = Me.pushAppointmentConfirmed(appointmentId, ApplicationConstants.NewAppointmentType)

            If (Not isAppointmentPushed) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.PushNotificationFailed
            End If

            objFaultBL.getJobsheetlistByAppointment(resultDataset, appointmentId)

            If (resultDataset.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.message += "<br />" + UserMessageConstants.NoJobSheetFound
            Else
                Try
                    jobSheetList = Me.getCommaSeparatedJobsheetList(resultDataset)
                    'Me.sendEmail(jobSheetList)
                    'Me.sendSMS(jobSheetList)
                    ViewState.Add(ViewStateConstants.RedirectTempCount, tempCount)
                    Me.populateAppointmentConfirmedPopup()
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message += "<br />" + ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                End Try

            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
        End If

    End Sub

#End Region

#Region "Get comma separated jobsheet list"

    Function getCommaSeparatedJobsheetList(ByVal resultDataset As DataSet) As String

        Dim length As Integer = resultDataset.Tables(0).Rows.Count
        Dim jobSheetList As StringBuilder = New StringBuilder()

        For i As Integer = 0 To length - 1
            jobSheetList.Append(resultDataset.Tables(0).Rows(i).Item("JobSheetNumber").ToString() + ",")
        Next

        jobSheetList.Remove(jobSheetList.ToString().Length - 1, 1)

        Return jobSheetList.ToString()

    End Function

#End Region

#Region "Save Appointment"

    Sub saveAppointment(ByRef appointmentId As Integer, ByRef tempCount As Integer)


        Dim faultsDataView As DataView = New DataView()
        faultsDataView = SessionManager.getSbSelectedTempFaultDv()
        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAptDataForSbAptSummary()
        Dim schemeId As Integer = SessionManager.getSchemeId()
        Dim blockId As String = SessionManager.getBlockId()
        Dim isCalendarAppointment As Boolean = False
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        If IsNothing(faultsDataView) Or appointmentData.OperativeId = 0 Or schemeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else

            Dim faultsDataTable As DataTable = New DataTable()
            faultsDataTable = faultsDataView.ToTable()

            Dim length As Integer = faultsDataTable.Rows.Count

            Dim faultsList As StringBuilder = New StringBuilder()

            For i As Integer = 0 To length - 1
                faultsList.Append(faultsDataTable.Rows(i).Item("TempFaultId").ToString() + ",")
            Next

            faultsList.Remove(faultsList.ToString().Length - 1, 1)

            Dim objappointmentSummaryBO As New AppointmentSummaryBO()
            objappointmentSummaryBO.OperativeId = appointmentData.OperativeId
            objappointmentSummaryBO.AppointmentStartDate = appointmentData.AppointmentStartDate
            objappointmentSummaryBO.Duration = appointmentData.Duration
            objappointmentSummaryBO.FaultNotes = appointmentData.FaultNotes
            objappointmentSummaryBO.FaultsList = faultsList.ToString()
            objappointmentSummaryBO.Time = appointmentData.StartTime
            objappointmentSummaryBO.EndTime = appointmentData.EndTime
            objappointmentSummaryBO.Schemeid = schemeId
            objappointmentSummaryBO.Blockid = blockId
            objappointmentSummaryBO.IsRecall = isRecall
            objappointmentSummaryBO.IsCalendarAppointment = isCalendarAppointment
            objappointmentSummaryBO.AppointmentNotes = txtAppointmentNotes.Text
            objappointmentSummaryBO.UserId = userId

            objFaultBl.saveSBAppointmentDetails(objappointmentSummaryBO, appointmentId, tempCount)

            If IsDBNull(appointmentId) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
                appointmentId = -1
            Else
                ' send ICAL file to show appointment on outlook calendar
                sendICalFileViaEmail(objappointmentSummaryBO)
                Dim appointmentList As List(Of AppointmentBO) = New List(Of AppointmentBO)
                appointmentList = SessionManager.getSbConfirmedAptIdList()

                If IsNothing(appointmentList) Then
                    appointmentList = New List(Of AppointmentBO)
                    Dim appointmentBO As AppointmentBO = New AppointmentBO()
                    appointmentBO.AppointmentId = appointmentId
                    appointmentList.Add(appointmentBO)
                    SessionManager.setSbConfirmedAptIdList(appointmentList)
                Else
                    Dim appointmentBO As AppointmentBO = New AppointmentBO()
                    appointmentBO.AppointmentId = appointmentId
                    appointmentList.Add(appointmentBO)
                    SessionManager.setSbConfirmedAptIdList(appointmentList)
                End If
            End If

        End If

    End Sub

#End Region

#Region "Change Fault Values"

    Sub changeFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        lblAreaName.Text = slideDataSet.Rows(slideIndex).Item("AreaName").ToString()
        lblDescription.Text = slideDataSet.Rows(slideIndex).Item("Description").ToString()

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("ProblemDays")) Then
            lblProblemDays.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() = 1 Then
                lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " day"
            Else
                lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " days"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("RecuringProblem")) Then
            lblReccuringProblem.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("RecuringProblem").ToString() = True Then
                lblReccuringProblem.Text = "This is a recurring problem"
            Else
                lblReccuringProblem.Text = "This is not a recurring problem"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("CommunalProblem")) Then
            lblCommunalProblem.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("CommunalProblem").ToString() = True Then
                lblCommunalProblem.Text = "This is a communal problem"
            Else
                lblCommunalProblem.Text = "This is not a communal problem"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Notes")) Then
            lblFaultNotes.Text = "NA"
        Else
            lblFaultNotes.Text = slideDataSet.Rows(slideIndex).Item("Notes").ToString()
        End If

    End Sub

#End Region

#Region "Navigate to other page"

    Sub navigate()

        'First check if the current appointment is for followon, navigate to follow on report.
        If SessionManager.getIsFollowOn() Then
            Response.Redirect(PathConstants.ReportAreaPath + "?" + PathConstants.RepairsDashboard + "=fow", True)
        End If

        Dim tempCount As Integer = ViewState.Item(ViewStateConstants.RedirectTempCount)

        If tempCount > 0 Then

            If isRecall Then
                Response.Redirect(PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbAppointmentSummarySrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
            Else
                Response.Redirect(PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbAppointmentSummarySrcVal))
            End If

        Else

            If isRecall Then
                Response.Redirect(PathConstants.SbFaultBasketPath + GeneralHelper.getSrc(PathConstants.SbAppointmentSummarySrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
            Else
                Response.Redirect(PathConstants.SbFaultBasketPath + GeneralHelper.getSrc(PathConstants.SbAppointmentSummarySrcVal))
            End If

        End If


    End Sub

#End Region

#Region "Popups"

#Region "Populate appointment detail popup"

    Sub populateAppointmentDetailPopup()

        Me.resetAppointmentDetailPopup()

        Dim faultsDataView As DataView = New DataView()
        faultsDataView = SessionManager.getSbSelectedTempFaultDv()
        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAptDataForSbAptSummary()

        If appointmentData.OperativeId = 0 Or IsNothing(faultsDataView) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else

            lblPopOperative.Text = appointmentData.Operative
            lblPopAppointmentTime.Text = appointmentData.StartTime + "-" + appointmentData.EndTime
            Dim aptDate As DateTime = Convert.ToDateTime(appointmentData.AppointmentStartDate)
            Dim format As String = "ddddddddd d  MMMMMMMMMM yyyy"
            lblPopAppointmentDate.Text = aptDate.ToString(format)

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.PopupSlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = faultsDataView.ToTable()

            changePopupFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count <= 1 Then
                lnkBtnNextPopupFault.Visible = False
                lnkBtnNextPopupFault.Enabled = False
            End If
            popupConfirmAppointmentDetail.Show()

        End If


    End Sub

#End Region

#Region "Reset appointment detail popup controls"

    Sub resetAppointmentDetailPopup()
        lblPopOperative.Text = String.Empty
        lblPopupAreaName.Text = String.Empty
        lblPopupFaultDescription.Text = String.Empty
        lblPopDuration.Text = String.Empty
        lblPopTrade.Text = String.Empty
        lblPopDue.Text = String.Empty
        lblPopAppointmentTime.Text = String.Empty
        lblPopAppointmentDate.Text = String.Empty
    End Sub

#End Region

#Region "Scroll Popup Faults"

    Sub scrollPopupFaults()

        Dim resultDataSet As DataView = New DataView()
        resultDataSet = SessionManager.getSbSelectedTempFaultDv()
        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = resultDataSet.ToTable()

        If (IsNothing(resultDataSet)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.PopupSlideIndex)
            changePopupFaultValues(slideDataSet, slideIndex)
            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                lnkBtnNextPopupFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.PopupSlideIndex) = slideIndex
            End If

        End If
    End Sub

#End Region

#Region "Change Popup Fault Values"

    Sub changePopupFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("AreaName")) Then
            lblPopupAreaName.Text = "NA"
        Else
            lblPopupAreaName.Text = slideDataSet.Rows(slideIndex).Item("AreaName").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Description")) Then
            lblPopupFaultDescription.Text = "NA"
        Else
            lblPopupFaultDescription.Text = slideDataSet.Rows(slideIndex).Item("Description").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Duration")) Then
            lblPopDuration.Text = "NA"
        Else
            lblPopDuration.Text = slideDataSet.Rows(slideIndex).Item("Duration").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("TradeName")) Then
            lblPopTrade.Text = "NA"
        Else
            lblPopTrade.Text = slideDataSet.Rows(slideIndex).Item("TradeName").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("CompleteDueDate")) Then
            lblPopDue.Text = "NA"
        Else

            Dim aptDueDate As DateTime = Convert.ToDateTime(slideDataSet.Rows(slideIndex).Item("CompleteDueDate").ToString())
            Dim format As String = "d  MMMMMMMMMM yyyy"
            aptDueDate.ToString()
            lblPopDue.Text = aptDueDate.ToString(format)
        End If

    End Sub

#End Region

#Region "Push appointment confirmed"

    Public Function pushAppointmentConfirmed(ByVal appointmentId As Integer, ByVal type As Integer) As Boolean
        Dim URL As String = String.Format("{0}?appointmentId={1}&type={2}&isSchemeBlockAppointment=true", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentId, type)
        Dim request As WebRequest = Net.WebRequest.Create(URL)
        request.Credentials = CredentialCache.DefaultCredentials
        Dim response As WebResponse = request.GetResponse()
        Dim dataStream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        response.Close()

        If (responseFromServer.ToString.Equals("true")) Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "Populate appointment confirmed popup"

    Sub populateAppointmentConfirmedPopup()

        lblAptArrangedOperative.Text = String.Empty

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAptDataForSbAptSummary()

        If appointmentData.OperativeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else
            lblAptArrangedOperative.Text = appointmentData.Operative
            popupAppointmentConfirmed.Show()
        End If


    End Sub

#End Region

#End Region

#Region "Email Functionality"
#Region "Send iCal File via Email"
    ''' <summary>
    ''' Send iCal File via Email
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub sendICalFileViaEmail(ByVal objappointmentSummaryBO As AppointmentSummaryBO)



        Dim resultDataset As DataSet = New DataSet()
        Dim objReplacementBL As FaultBL = New FaultBL()
        'get detail for selected operative
        objReplacementBL.getOperativeInformation(resultDataset, objappointmentSummaryBO.OperativeId)

        Dim operativeName As String = String.Empty
        Dim operativeEmail As String = String.Empty
        operativeName = resultDataset.Tables(0).Rows(0)("Name")
        If (resultDataset.Tables(0).Rows(0)("Email").Equals(DBNull.Value)) Then
            operativeEmail = String.Empty
        Else
            operativeEmail = resultDataset.Tables(0).Rows(0)("Email")
        End If
        ' get scheme block address to display in email body.
        Dim address As String = commonAddressBo.Address + ", " + commonAddressBo.City + ", " + commonAddressBo.Area + ", " + commonAddressBo.PostCode

        Dim body As String = Me.populateBody(objappointmentSummaryBO, address)
        Dim subject As String = "Scheme/Block Appointment Arranged"
        Dim recepientEmail As String = operativeEmail

        Try
            If Validation.isEmail(recepientEmail) Then

                populateiCalFile(operativeName, operativeEmail, objappointmentSummaryBO)
                EmailHelper.sendHtmlFormattedEmailWithAttachment(operativeName, recepientEmail, subject, body, Server.MapPath(PathConstants.ICalFilePath))

            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.InvalidEmail, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.AppointmentArrangedSavedEmailError + ex.Message
            'uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InspectionSavedEmailError + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
            btnPopupConfirmAppointment.Enabled = False
        End Try

    End Sub
#End Region
#Region "Populate Email body"
    ''' <summary>
    ''' Populate Email body
    ''' </summary>
    ''' <param name="ArrangeInspectionBO"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function populateBody(ArrangeInspectionBO As AppointmentSummaryBO, PropertyAddress As String) As String
        Dim body As New StringBuilder
        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/SbAppointmentConfirmation.htm"))
        body.Append(reader.ReadToEnd.ToString)
        body.Replace("{AppointmentTime}", ArrangeInspectionBO.Time + " - " + ArrangeInspectionBO.EndTime)
        body.Replace("{AppointmentDate}", ArrangeInspectionBO.AppointmentStartDate)
        body.Replace("{Address}", PropertyAddress)
        Return body.ToString()
    End Function
#End Region
#Region "Populate iCal File"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateiCalFile(ByVal operativeName As String, ByVal operativeEmail As String, ByVal objappointmentSummaryBO As AppointmentSummaryBO)

      
        Dim startTime As DateTime = DateTime.Parse(Convert.ToDateTime(objappointmentSummaryBO.AppointmentStartDate.ToShortDateString() + " " + objappointmentSummaryBO.Time))
        ' Dim startTime As String = concatDateTime.ToString("dd/MM/YYYY HH:mm")
        Dim endTime As DateTime = DateTime.Parse(Convert.ToDateTime(objappointmentSummaryBO.AppointmentEndDate.ToShortDateString() + " " + objappointmentSummaryBO.EndTime))

        Dim iCalString As StringBuilder = New StringBuilder()
        iCalString.AppendLine("BEGIN:VCALENDAR")
        iCalString.AppendLine("PRODID:-//Apple Inc.//iCal 5.0.2//EN")
        iCalString.AppendLine("VERSION:2.0")
        iCalString.AppendLine("CALSCALE:GREGORIAN")
        iCalString.AppendLine("METHOD:PUBLISH")
        iCalString.AppendLine("X-WR-CALNAME:BHGcalendar")
        iCalString.AppendLine("X-WR-TIMEZONE:" + TimeZone.CurrentTimeZone.ToString())
        iCalString.AppendLine("X-WR-CALDESC:BHG Calendar")
        iCalString.AppendLine("BEGIN:VEVENT")
        iCalString.AppendLine("ATTENDEEROLE=REQ-PARTICIPANTCN=" + operativeName + ":MAILTO:" + operativeEmail)
        'iCalString.AppendLine("ORGANIZERCN=" + aaveData.inviterName + ":MAILTO:" + aaveData.inviterEmail)
        iCalString.AppendLine("DTSTART:" + startTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
        iCalString.AppendLine("DTEND:" + endTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
        iCalString.AppendLine("DTSTAMP:20130127T040705Z")
        iCalString.AppendLine("UID:" + Convert.ToString(DateTime.Now.Ticks))
        iCalString.AppendLine("CREATED:" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
        iCalString.AppendLine("DESCRIPTION:" + "An Inspection has been arranged.")
        iCalString.AppendLine("LAST-MODIFIED:" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"))
        ' iCalString.AppendLine("LOCATION:" + aaveData.residenceAddress)
        iCalString.AppendLine("SEQUENCE:0")

        iCalString.AppendLine("STATUS:CONFIRMED")
        'iCalString.AppendLine("SUMMARY:" + aaveData.prospectName + " Viewing")

        iCalString.AppendLine("TRANSP:TRANSPARENT")
        iCalString.AppendLine("END:VEVENT")
        iCalString.AppendLine("END:VCALENDAR")


        Dim iCalTextWriter As TextWriter = New StreamWriter(Server.MapPath(PathConstants.ICalFilePath))
        iCalTextWriter.WriteLine(iCalString)
        iCalTextWriter.Close()
    End Sub

#End Region
#End Region

#Region "Send SMS"
    'Private Sub sendSMS(ByVal jobSheetNumberList As String)

    '    Dim appointmentData As AppointmentBO = New AppointmentBO()
    '    appointmentData = SessionManager.getAptDataForSbAptSummary()


    '    Dim recepientMobile As String = lblCustomerMobile.Text
    '    Dim recepientName As String = lblCustomerName.Text

    '    Try
    '        If Validation.isMobile(recepientMobile) Then

    '            Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(jobSheetNumberList, appointmentData.Time, appointmentData.AppointmentDate)
    '            GeneralHelper.sendSMS(body, recepientMobile.Replace(" ", String.Empty))
    '            'Dim request As WebRequest = WebRequest.Create(ApplicationConstants.SMSUrl)
    '            '' Set the Method property of the request to POST.
    '            'request.Method = "POST"
    '            '' Create POST data and convert it to a byte array.

    '            'Dim strdata As String

    '            'strdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(recepientMobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)

    '            'Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strdata)
    '            '' Set the ContentType property of the WebRequest.
    '            'request.ContentType = "application/x-www-form-urlencoded"
    '            '' Set the ContentLength property of the WebRequest.
    '            'request.ContentLength = byteArray.Length
    '            '' Get the request stream.
    '            'Dim dataStream As Stream = request.GetRequestStream()
    '            '' Write the data to the request stream.
    '            'dataStream.Write(byteArray, 0, byteArray.Length)
    '            '' Close the Stream object.
    '            'dataStream.Close()
    '            '' Get the response.
    '            'Dim response As WebResponse = request.GetResponse()
    '            '' Display the status.
    '            ''Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
    '            '' Get the stream containing content returned by the server.f
    '            'dataStream = response.GetResponseStream() ' Open the stream using a StreamReader for easy access.
    '            'Dim reader As New StreamReader(dataStream) ' Read the content.
    '            'Dim responseFromServer As String = reader.ReadToEnd()
    '            '' Display the content.
    '            ''Console.WriteLine(responseFromServer)
    '            '' Clean up the streams.
    '            'reader.Close()
    '            'dataStream.Close()
    '            'response.Close()





    '        Else
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
    '        End If
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If
    '    End Try

    'End Sub
#End Region

#Region "reset SchemeBlock Detail"
    Sub resetSchemeBlockDetail()
        lblScheme.Text = String.Empty
        lblSchemeBlockAddress.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblCOUNTY.Text = String.Empty
    End Sub

#End Region

#End Region

End Class