﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="AvailableAppointments.aspx.vb" Inherits="FaultScheduling.AvailableAppointments" %>

<%@ Register TagPrefix="custom" TagName="CustomerDetails" Src="~/Controls/Common/CustomerDetail.ascx" %>
<%@ Register TagPrefix="contractor" TagName="AssignToContractor" Src="~/Controls/Fault/AssignToContractor.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        /* Over ridding default styles */
        .main
        {
            width: auto;
            height: auto;
        }
        .assignToContractor
        {
            margin-left:85%;
            background-color : White;
            border-radius : 5px;
            padding : 4px;
        }
    </style>
    <script type="text/javascript" src="../../Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&client=gme-broadlandhousing&v=3.17"></script>
    <script type="text/javascript">
        var map;
        var markers = [];
        var markersAddress = [];
        function showMap() {
            var destinationAddress = document.getElementById("<%= custDetails.ClientStreetAddressLabel.ClientID %>").innerHTML;
            destinationAddress += ", " + document.getElementById("<%= custDetails.ClientClientCityLabel.ClientID %>").innerHTML;
            destinationAddress = destinationAddress.replace("\'", "");

            var map_canvas = document.getElementById("mapContainer");
            var map_options = {
                center: new google.maps.LatLng(52.630886, -0.4573616),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, map_options);

            addOriginMarkers();

            var maxZindex = google.maps.Marker.MAX_ZINDEX;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': destinationAddress }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: destinationAddress,
                        zIndex: maxZindex,
                        icon: "https://maps.google.com/mapfiles/kml/paddle/D.png"
                    });
                }
                console.log(status + ":(destination address) " + destinationAddress + " Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
            });
        }

        function addOriginMarkers() {

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "AvailableAppointments.aspx/getExistingAppointmentsData",
                data: "{}",
                dataType: "json",
                success: function (response) {
                    var appointments = JSON.parse(response.d);
                    var counter = 0;
                    var timeout = 1000;
                    for (var i = 0; i < appointments.length; i++) {
                        
                        if (counter > 0 && counter % 5 == 0) {
                            timeout += 1000;
                        }
                        setTimeout(setMarker(appointments[i]), timeout);
                        counter++;
                    }
                }
            });
        }

        function setMarker(appointment) {
            var adrs = appointment.Address + ", " + appointment.TownCity + ", " + appointment.County;

            // Check if a property address is already processed cancel geocode call and return.
            if ($.inArray(adrs, markersAddress) >= 0) {
                console.log("skiped:" + adrs);
                return;
            }

            geocoder = new google.maps.Geocoder();

            geocoder.geocode({ 'address': adrs }, function (results, status) {
                geoCodeStatus = status;
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(status + ":" + adrs + "Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: adrs,
                        icon: "https://maps.google.com/mapfiles/kml/paddle/ylw-circle.png"
                    });
                    markers.push(marker);
                    markersAddress.push(adrs);
                }
                else {
                    console.log(status + ":" + adrs);
                }
            });
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setAllMap(null);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
            markersAddress = [];
        }

        google.maps.event.addDomListener(window, 'load', showMap);
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel runat="server" ID="updPanelControl">
        <ContentTemplate>
       
    <asp:Panel ID="pnlMessage" runat="server" Visible="false" style=" margin-left:21px;">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div class="clear"></div>
    <div style="margin: 0 0 0 12px">
        <custom:CustomerDetails ID="custDetails" runat="server" StartString="Next available appointments for:">
        </custom:CustomerDetails>
    </div>
    <table width="100%">
        <tr>
            <td style="min-width: 700px;">
                <div class="availableAptDivContainer">
                    <asp:UpdatePanel ID="updPanelMain" runat="server">
                        <ContentTemplate>
                            <div class="border_height40pc" style="margin: -24px 0px" id="divPageContainer" runat="server">
                            </div>
                            <asp:Button ID="btnBackToBasket" runat="server" Text="< Back to Basket" CssClass="backToBasket" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td style="vertical-align: top;">
                <div class="appointmentsMap" id="mapContainer">
                </div>
            </td>
        </tr>
    </table>
     </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="pnlAssignToContractor" runat="server" CssClass="modalPopup" Style="width: 47%;
        display: none; padding: 0px !important;">
        <div style="width: 100%; text-align: left; clear: both; overflow: auto; max-height: 620px;margin-left:0px;">
            <contractor:AssignToContractor ID="ucAssignToContractor" runat="server"></contractor:AssignToContractor>
        </div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <asp:Label ID="lblFaultIdHidden" runat="server" Text="" Style="display: none;" ClientIDMode="Static" />
    <cc1:ModalPopupExtender ID="popupAssignToContractor" runat="server" PopupControlID="pnlAssignToContractor"
        TargetControlID="lblFaultIdHidden" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>
