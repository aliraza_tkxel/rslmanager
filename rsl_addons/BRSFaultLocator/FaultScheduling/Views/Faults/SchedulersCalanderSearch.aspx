﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/RDMasterPage.Master"
    CodeBehind="SchedulersCalanderSearch.aspx.vb" Inherits="FaultScheduling.SchedulersCalanderSearch"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../Styles/aptCalendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function displayMore(value) {
            document.getElementById("display" + value).style.display = 'block';
            document.getElementById("arrow-left" + value).style.display = 'none';
            document.getElementById("arrow-down" + value).style.display = 'block';
            document.getElementById("addressDiv" + value).style.display = 'none';

        }

        function displayMoreHide(value) {
            document.getElementById("display" + value).style.display = 'none';
            document.getElementById("arrow-left" + value).style.display = 'block';
            document.getElementById("arrow-down" + value).style.display = 'none';
            document.getElementById("addressDiv" + value).style.display = 'block';

        }

        function PrintJobSheet() {
            javascript: window.open('PrintJobSheetDetail.aspx?JobSheetNumber=' + document.getElementById("lblFaultId").innerHTML);
            return false;
        }
        function setJsn(jsn) {            
            document.getElementById('<%= txtJsnumber.ClientID %>').value = jsn;
            __doPostBack('<%= txtJsnumber.ClientID %>', '');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="fl_width_100_text_a_l">
        <div class=" fl_heading">
            Scheduling
        </div>
        <hr />
        <br />
        <table class="searchInputTable">
            <tr>
                <td class="searchTableLabel">
                    Patch:
                    <%--   <div style="margin-left: 10%;">
                        Patch:
                    </div>--%>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPatch" runat="server" AutoPostBack="True" Width="150px">
                    </asp:DropDownList>
                </td>
                <td class="searchTableLabel">
                    JSN:
                    <%-- <div style="margin-left: 10%;">
                        JSN:
                    </div>--%>
                </td>
                <td>
                    <asp:TextBox ID="txtJSN" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td class="searchTableLabel">
                    Tenant:
                    <%-- <div style="margin-left: 10%;">
                        Tenant:
                    </div>--%>
                </td>
                <td>
                    <asp:TextBox ID="txtTenant" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="searchTableLabel">
                    Scheme:
                    <%--  <div style="margin-left: 10%;">
                        Scheme:
                    </div>--%>
                </td>
                <td>
                    <asp:DropDownList ID="ddlScheme" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
                <td class="searchTableLabel">
                    Operative:
                    <%-- <div style="margin-left: 10%;">
                        Operative:
                    </div>--%>
                </td>
                <td>
                    <asp:TextBox ID="txtOperative" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td class="searchTableLabel">
                    Address:
                    <%--<div style="margin-left: 10%;">
                        Address:
                    </div>--%>
                </td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server" Width="150px"></asp:TextBox>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="searchTableLabel">
                    Trade:
                    <%--    <div style="margin-left: 10%;">
                        Trade:
                    </div>--%>
                </td>
                <td>
                    <asp:DropDownList ID="ddlTrade" runat="server" Width="150px">
                    </asp:DropDownList>
                </td>
                <td>
                    <%--  <input type="text" ID="txtJsnumber" runat="server" OnTextChanged="txtJsnumber_TextChanged"  /> --%>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnFind" runat="server" Text="Find" />
                </td>
            </tr>
        </table>
    </div>
    <br />
    <br />
    <asp:Panel ID="pnlGridView" runat="server">
        <div class="bottom_line fl_width_100_text_a_l">
            <div id="fault_result_header" style="padding-left: 0px;">
                <b>Search results</b>
            </div>
            <cc2:PagingGridView ID="grdSearchResult" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                CellPadding="4" ForeColor="Black" GridLines="None" orderby="" PageSize="30" HeaderStyle-CssClass="gridfaults-header"
                CssClass="searchGrid" Width="100%">
                <Columns>
                    <asp:TemplateField HeaderText="JSN:" SortExpression="JobSheetNumber">
                        <ItemTemplate>
                            <asp:Label ID="lblJobSheetNumber" runat="server" Text='<%# Bind("JobSheetNumber") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="20%" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location:" SortExpression="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="20%" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trade:" SortExpression="Trade">
                        <ItemTemplate>
                            <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("Trade") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Start Date:" SortExpression="AppointmentSort">
                        <ItemTemplate>
                            <asp:Label ID="lblAppointmentStartDateTime" runat="server" Text='<%# Bind("AppointmentStartDateTime") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Wrap="false" />                        
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Date:" SortExpression="AppointmentEndSort">
                        <ItemTemplate>
                            <asp:Label ID="lblAppointmentEndDateTime" runat="server" Text='<%# Bind("AppointmentEndDateTime") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Wrap="false" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status:" SortExpression="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnJobSheetNumber" runat="server" Text="View" CommandArgument='<%#Eval("JobSheetNumber")%>'
                                OnClick="ViewFault" />
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="gridfaults-header" />
                <PagerSettings Mode="NumericFirstLast">
                </PagerSettings>
            </cc2:PagingGridView>
        </div>
    </asp:Panel>
    <!--Calendar Code Start Here-->
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server">
        </asp:Label>
    </asp:Panel>
    <asp:Panel ID="pnlCalendar" runat="server" Visible="false">
        <div style="float: left; width: 100%;">
            <div style="float: left; width: 99%; padding: 4px; margin-top: 10px; border: 1px solid #000">
                <div style="float: left; width: 91%;">
                    <asp:Label ID="lblMonth" runat="server" Font-Bold="True"></asp:Label>
                </div>
                <div style="float: left; width: 8%; text-align: right;">
                    <span style="border: solid 1px gray; padding: 0px !important; float: left;">
                        <asp:HyperLink ID="hrPrevious" runat="server" NavigateUrl="SchedulersCalanderSearch.aspx?mov=p"><img src="../../images/arrow_left.png" border="0" style="padding-top: 2px;
    vertical-align: text-top;float:left" /></asp:HyperLink>
                        <span style="border-left: solid 1px gray; border-right: solid 1px gray; float: left;
                            font-size: 12px; height: 17px;">
                            <asp:HyperLink ID="hlToday" runat="server" NavigateUrl="SchedulersCalanderSearch.aspx?mov=t">Today</asp:HyperLink>
                        </span>
                        <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="SchedulersCalanderSearch.aspx?mov=n"><img src="../../images/arrow_right.png" border="0" style="padding-top: 2px;padding-left: 2px;
    vertical-align: text-top;float:left" alt=""/></asp:HyperLink>
                    </span>
                </div>
            </div>
            <div>
                <asp:Panel ID="Panel2" runat="server" Visible="false"> 
                    <asp:Label ID="Label2" runat="server">
                    </asp:Label>
                </asp:Panel>
                <% 
                    
                    If varWeeklyDT.Rows.Count > 0 Then%>
                <table width="100%" border="0" style="border-left: solid 1px gray; border-top: solid 1px gray;
                    float: left;" cellspacing="0">
                    <tr>
                        <%  Dim empArray() As String
                            Dim bgColor As String
                            For dtCount = 0 To varWeeklyDT.Columns.Count - 1%>
                        <td style="font-weight: bold; border-left: solid 1px gray; border-right: solid 1px gray;
                            border-bottom: solid 1px gray; text-align: center;">
                            <span style="margin-bottom: 10px;">
                                <%  If varWeeklyDT.Columns(dtCount).ToString() <> " " Then
                                        empArray = varWeeklyDT.Columns(dtCount).ToString().Split(":")
                                %>
                                <%Response.Write(empArray(1).Trim())%></span>&nbsp;
                            <%-- <a href="SchedulingCalendar.aspx?opr=<%=empArray(1).Trim()+"&id="+empArray(0) %>">
                        <img border="0" src="../../images/btn_add.png" alt=""  />
                    </a>--%>
                            <%--<asp:ImageButton ID="imgAddAppointment" runat="server" OnClick="imgAddAppointment_Click" BorderStyle="None" ImageUrl='../../images/btn_add.png' CommandArgument="<%=empArray(1).Trim() %>"   />--%>
                            <%End If%>
                        </td>                        
                        <%Next%>
                    </tr>
                    <%  For rdCount = 0 To varWeeklyDT.Rows.Count - 1
                            If (rdCount Mod 2 = 0) Then
                                bgColor = "#E8E9EA"
                            Else
                                bgColor = "#FFFFFF"
                            End If
                    %>
                    <tr style='<%response.write("background-color:"+ bgColor+";")%>'>
                        <%  For rdItemCount = 0 To varWeeklyDT.Columns.Count - 1
                                If Not String.IsNullOrEmpty(varWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then

                                    If rdItemCount = 0 Then
                             
                        %>
                        <td style="border-right: 1px solid gray; border-bottom: 1px solid gray;">
                            <div class="subHeading">
                                <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%>
                            </div>
                        </td>
                        <%Else%>
                        <td style="border-right: 1px solid gray; border-bottom: 1px solid gray;">
                            <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%>
                        </td>
                        <%End If%><%Else%>
                        <td style="border-right: 1px solid gray; border-bottom: 1px solid gray;">
                            &nbsp;
                        </td>
                        <%End If%><%
                                  Next%>
                    </tr>
                    <%  Next%>
                </table>
                <%  End If
                %>
                <div style="float: right;">
                </div>
            </div>
        </div>
    </asp:Panel>
    <!--Calendar Code End Here -->
    <!-- ModalPopupExtender -->
    <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popupJobSheet" runat="server" PopupControlID="pnlJobSheet"
        TargetControlID="btnHidden" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="height: 90%;
        overflow: auto;">
        <div id="header">
            <p id="header_label">
                BHG Fault Locator
            </p>
            <div id="header_box">
            </div>
        </div>
        <br />
        <br />
        <%--     <asp:Panel ID="Panel1" runat="server" Visible="False">
            <asp:Label ID="Label1" runat="server"></asp:Label>
        </asp:Panel>--%>
        <div style="height: auto; clear: both;">
            <table id="jobsheet_detail_table" style="width: 100%; margin-top: 10px; text-align: left;">
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : "
                            Font-Bold="true"></asp:Label>
                        <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;,
                        <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;,
                        <asp:Label ID="lblClientPostalcodeHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel : " Font-Bold="true"> </asp:Label><asp:Label
                            ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                    <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static" Font-Bold="true"></asp:Label>
                       
                    </td>
                    <td>
                    </td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                         Status:
                    </td>
                    <td>
                    <asp:Label ID="lblFaultStatus" runat="server" ClientIDMode="Static"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contractor :
                    </td>
                    <td>
                        <asp:Label ID="lblContractor" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Operative :
                    </td>
                    <td>
                        <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Priority :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Completion due :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Order date :
                    </td>
                    <td>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                    </td>
                    <td>
                        Tel :
                        <asp:Label ID="lblClientTelPhoneNumber" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Location :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                    </td>
                    <td>
                        Mobile :
                        <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultDescription" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                        Email :
                        <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Trade :
                    </td>
                    <td>
                        <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Note :
                    </td>
                    <td>
                        <asp:TextBox Enabled="false" ID="lblFaultOperatorNote" CssClass="roundcornerby5"
                            runat="server" TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Appointment :</b>
                    </td>
                    <td>
                        <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                        <b>Asbestos :</b>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblFaultAppointmentDateTime" runat="server" Text=""></asp:Label>
                    </td>
                    <td rowspan="3">
                        <div style="height: 165px; overflow: auto;">
                            <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                BorderStyle="None" CellSpacing="3" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="AsbRiskID" />
                                    <asp:BoundField DataField="Description" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtAppointmentNote" Enabled="false" runat="server" TextMode="MultiLine"
                            CssClass="roundcornerby5" Height="53px" Width="366px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                        <asp:TextBox ID="txtJsnumber" runat="server" OnTextChanged="txtJsnumber_TextChanged"
                            BorderStyle="None" ForeColor="White"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <%--<asp:TextBox ID="txtJsnumber" runat="server" OnTextChanged="txtJsnumber_TextChanged"></asp:TextBox>--%>
        </div>
        <div style="width: 100%; text-align: left; clear: both;">
            <div style="float: right;">
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="margin_right20" />
                <input id="btnPrintJobSheet" type="button" value="Print Job Sheet" class="margin_right20"
                    onclick="PrintJobSheet()" />
            </div>
        </div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
</asp:Content>
