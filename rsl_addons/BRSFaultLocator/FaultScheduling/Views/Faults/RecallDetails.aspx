﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="RecallDetails.aspx.vb" Inherits="FaultScheduling.RecallDetails" %>

<%@ Register TagPrefix="custom" TagName="CustomerDetails" Src="~/Controls/Common/CustomerDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelRecallDetails" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <table id="recall_detail_table" style="width: 100%; min-height: 100%;">
                <tr>
                    <td colspan="3">
                        <%--Change - Added by Behroz - 18/02/2013 - Start--%>
                        <custom:CustomerDetails ID="custDetails" runat="server" StartString="Recall details for:">
                        </custom:CustomerDetails>
                        <%--<asp:Label ID="lblRecallDetailsFor" runat="server" Text="Recall details for : " Font-Bold="true"></asp:Label>
                         <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                         &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                         &nbsp;, <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                         &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel :" Font-Bold="true"> </asp:Label><asp:Label ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>--%>
                        <%--Change - Added by Behroz - 18/02/2013 - End--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFaultId" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contractor :
                    </td>
                    <td>
                        <asp:Label ID="lblContractor" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Operative :
                    </td>
                    <td>
                        <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Priority :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Completion :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Order date :
                    </td>
                    <td>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <div class="fault_detail_right_col_first_div">
                            Tel:</div>
                        <div class="fault_detail_right_col_second_div">
                            <asp:Label ID="lblClientTelPhoneNumber" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        Location :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                    </td>
                    <td>
                        <div class="fault_detail_right_col_first_div">
                            Mobile:</div>
                        <div class="fault_detail_right_col_second_div">
                            <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                        </div>
                        <%--Mobile:&nbsp;&nbsp;&nbsp; 
                       
                            <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultDescription" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                        <div class="fault_detail_right_col_first_div">
                            Email:</div>
                        <div class="fault_detail_right_col_second_div">
                            <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">
                        Notes :
                    </td>
                    <td>
                        <asp:TextBox ID="lblFaultOperatorNote" Enabled="false" CssClass="roundcornerby5"
                            runat="server" TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Appointment:</b>
                    </td>
                    <td>
                        <asp:Label ID="lblTime" runat="server" Text=""></asp:Label><br />
                        <asp:Label ID="lblFaultAppointmentDateTime" runat="server" Text=""></asp:Label><br />
                    </td>
                    <td rowspan="2">
                        <b>Asbestos:</b>
                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            BorderStyle="None" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID">
                                <ItemStyle Width="30px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Description" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtBoxAppointmentNote" runat="server" TextMode="MultiLine" CssClass="roundcornerby5"
                            Enabled="false" Height="53px" Width="366px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
            </table>
            <div style="width: 100%; text-align: left; clear: both;">
                <div>
                    <b>Repair Details: </b>
                </div>
                <div>
                    <asp:GridView ID="grdRepairDetails" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                        BorderStyle="None" GridLines="None" CellSpacing="15">
                        <Columns>
                            <asp:BoundField DataField="RepairCompletionDateTime" DataFormatString="{0:d/MM/yyyy}" HtmlEncode="false" />
                            <asp:BoundField DataField="Description" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="text_a_r_padd__r_20px">
                    <asp:Button ID="btnBack" runat="server" Text="< Back" CssClass="margin_right20" />
                    <asp:Button ID="btnScheduleNewFault" runat="server" Text="Schedule a New Fault" CssClass="margin_right20" />
                    <asp:Button ID="btnScheduleRecal" runat="server" Text="Schedule Recall" CssClass="margin_right20" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
