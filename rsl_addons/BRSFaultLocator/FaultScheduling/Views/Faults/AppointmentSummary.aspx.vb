﻿Imports FLS_BusinessObject
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports System.Net
Imports System.IO
Imports System.Net.Mail
Imports System.Text


Public Class AppointmentSummary
    Inherits PageBase

#Region "Properties "

    Dim objCustomerBL As CustomerBL = New CustomerBL()
    Dim objFaultBl As FaultBL = New FaultBL()
    Dim isRecall As Boolean = False

#End Region

#Region "Events"

#Region "Page Pre Render"
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Dim url As String = Page.Request.UrlReferrer.AbsoluteUri

        If url.Contains(PathConstants.IsRecall) Then
            Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.AvailableAppointments + GeneralHelper.getSrc(PathConstants.AppointmentSummarySrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.AppointmentSummarySrcVal))
        End If

    End Sub
#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall) = PathConstants.Yes, True, False)
            End If

            If Not IsPostBack Then
                prePopulatedValues()
                populateAppointmentSummary()
                populateAppointmentDetails()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                lnkBtnNextFault.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Button Update Customer Detail Click Event"

    Protected Sub btnUpdateCustomerDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateCustomerDetail.Click
        Try
            updateAddress()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Link Button Next Fault Click Event"

    Protected Sub lnkBtnNextFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnNextFault.Click

        Try
            scrollFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Popup Link Button Next Fault Click Event"

    Protected Sub lnkBtnNextPopupFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnNextPopupFault.Click

        Try

            Me.scrollPopupFaults()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Confirm Appointment Detail Click Event"

    Protected Sub btnConfirmAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirmAppointment.Click

        Try
            Me.populateAppointmentDetailPopup()
            'popupAppointmentConfirmed.Show()
            'popupConfirmAppointmentDetail.Show()
            'pushAppointmentConfirmed(1, 1)
            'confirmAppointment()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Popup Confirm Appointment Button Click Event"

    Protected Sub btnPopupConfirmAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupConfirmAppointment.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            Me.confirmAppointment()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Continue button after appointment confirmed."

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinue.Click

        Try

            popupAppointmentConfirmed.Hide()
            Me.navigate()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Go Back Event"

    Protected Sub btnGoBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoBack.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            ViewState.Item(ViewStateConstants.PopupSlideIndex) = 0
            lnkBtnNextPopupFault.Visible = True
            lnkBtnNextPopupFault.Enabled = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Close Event"

    Protected Sub imgBtnCloseAppointmentDetail_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgBtnCloseAppointmentDetail.Click

        Try
            popupConfirmAppointmentDetail.Hide()
            ViewState.Item(ViewStateConstants.PopupSlideIndex) = 0
            lnkBtnNextPopupFault.Enabled = True
            lnkBtnNextPopupFault.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    '#Region "Button Back Click Event"

    '    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click

    '        If isRecall Then
    '            Response.Redirect(PathConstants.AvailableAppointments & "?" & PathConstants.IsRecall & "=" & PathConstants.Yes)
    '        Else
    '            Response.Redirect(PathConstants.AvailableAppointments)
    '        End If

    '    End Sub

    '#End Region

    '#Region "Button Cancel Reported Faults Click Event"

    '    Protected Sub btnCancelReportedFaults_Click(sender As Object, e As EventArgs) Handles btnCancelReportedFaults.Click

    '        Response.Redirect(PathConstants.CancellationAppointmentSummary & "?faultsList=" & SessionManager.getTemporaryFaultIds() & "&appointmentId=")

    '    End Sub

    '#End Region

#End Region

#Region "Functions"

#Region "Pre Populate Values"

    Sub prePopulatedValues()

        Dim customerData As CustomerBO = New CustomerBO()
        customerData = SessionManager.getCustomerData()
        '' The below data is hardcoded as there is currently no data in Session **** Abdul Wahhab 14/02/2013 ****
        'customerData.CustomerId = 1
        'customerData.Name = "Emma Howard"
        'customerData.Street = "54B High Street"
        'customerData.Address = "Dereham NR1 2DD"
        'customerData.Area = "Norfolk"
        'customerData.Telephone = "01603 1234455"
        'customerData.Mobile = "07788131212"
        'customerData.Email = "emma.h@hotmail.com"

        'SessionManager.setCustomerData(customerData)
        '' These code lines will need to be removed **** Abdul Wahhab 14/02/2013 ****

        ' Customer Labels

        If customerData.CustomerId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingCustomerData, True)
        End If

        lblCustomerName.Text = customerData.Name
        lblCustomerStreet.Text = customerData.Street
        lblCustomerAddress.Text = customerData.PostCode
        lblCustomerArea.Text = customerData.Area
        lblCustomerTelephone.Text = customerData.Telephone
        lblCustomerMobile.Text = customerData.Mobile
        lblCustomerEmail.Text = customerData.Email
        If (lblCustomerMobile.Text.Equals(String.Empty)) Then
            mobileCheckBox.Enabled = False
            mobileCheckBox.Checked = False
        End If
        If (lblCustomerEmail.Text.Equals(String.Empty)) Then
            emailCheckBox.Enabled = False
            emailCheckBox.Checked = False
        End If

    End Sub

#End Region

#Region "Update Customer Address"

    Sub updateAddress()

        If btnUpdateCustomerDetail.Text = "Update Customer Detail" Then

            txtCustomerTelephone.Text = lblCustomerTelephone.Text
            lblCustomerTelephone.Visible = False
            txtCustomerTelephone.Visible = True

            txtCustomerMobile.Text = lblCustomerMobile.Text
            lblCustomerMobile.Visible = False
            txtCustomerMobile.Visible = True

            txtCustomerEmail.Text = lblCustomerEmail.Text
            lblCustomerEmail.Visible = False
            txtCustomerEmail.Visible = True

            btnUpdateCustomerDetail.Text = "Save Changes"

        Else

            If lblCustomerTelephone.Text <> txtCustomerTelephone.Text Or lblCustomerMobile.Text <> txtCustomerMobile.Text Or lblCustomerEmail.Text <> txtCustomerEmail.Text Then

                Dim telephone As String = txtCustomerTelephone.Text
                Dim mobile As String = txtCustomerMobile.Text
                Dim email As String = txtCustomerEmail.Text

                telephone = telephone.Trim()
                mobile = mobile.Trim()
                email = email.Trim()

                telephone = telephone.Replace(" ", "")
                mobile = mobile.Replace(" ", "")

                Dim phoneRegex As Regex = New Regex(RegularExpConstants.numbersExp)
                Dim emailRegex As Regex = New Regex(RegularExpConstants.emailExp)

                Dim phoneMatch As Match = phoneRegex.Match(telephone)
                Dim mobileMatch As Match = phoneRegex.Match(mobile)
                Dim emailMatch As Match = emailRegex.Match(email)

                Dim isError As Boolean = False

                If Not phoneMatch.Success And telephone <> "" Then
                    isError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidTelephone, True)
                ElseIf Not mobileMatch.Success And mobile <> "" Then
                    isError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
                ElseIf Not emailMatch.Success And email <> "" Then
                    isError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
                End If

                If Not isError Then

                    Dim customerData As CustomerBO = New CustomerBO()
                    customerData = SessionManager.getCustomerData()

                    If customerData.CustomerId = 0 Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingCustomerData, True)
                    End If

                    Dim objCustomerBO As CustomerBO = New CustomerBO()
                    objCustomerBO.CustomerId = customerData.CustomerId
                    objCustomerBO.Telephone = txtCustomerTelephone.Text
                    objCustomerBO.Mobile = txtCustomerMobile.Text
                    objCustomerBO.Email = txtCustomerEmail.Text

                    If IsDBNull(objCustomerBL.updateAddress(objCustomerBO)) Then

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorUserUpdate, True)

                    Else

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)

                        lblCustomerTelephone.Text = txtCustomerTelephone.Text
                        lblCustomerMobile.Text = txtCustomerMobile.Text
                        lblCustomerEmail.Text = txtCustomerEmail.Text

                        customerData.Telephone = txtCustomerTelephone.Text
                        customerData.Mobile = txtCustomerMobile.Text
                        customerData.Email = txtCustomerEmail.Text

                        SessionManager.setCustomerData(customerData)

                        custDetails.setCustomerLabels()

                        txtCustomerTelephone.Visible = False
                        lblCustomerTelephone.Visible = True

                        txtCustomerMobile.Visible = False
                        lblCustomerMobile.Visible = True

                        txtCustomerEmail.Visible = False
                        lblCustomerEmail.Visible = True

                        If (lblCustomerEmail.Text.Equals(String.Empty)) Then
                            emailCheckBox.Enabled = False
                            emailCheckBox.Checked = False
                        Else
                            emailCheckBox.Enabled = True
                            emailCheckBox.Checked = True

                        End If
                        If (lblCustomerMobile.Text.Equals(String.Empty)) Then
                            mobileCheckBox.Enabled = False
                            mobileCheckBox.Checked = False
                        Else
                            mobileCheckBox.Enabled = True
                            mobileCheckBox.Checked = True
                        End If

                        btnUpdateCustomerDetail.Text = "Update Customer Detail"

                    End If

                End If

            Else

                txtCustomerTelephone.Visible = False
                lblCustomerTelephone.Visible = True

                txtCustomerMobile.Visible = False
                lblCustomerMobile.Visible = True

                txtCustomerEmail.Visible = False
                lblCustomerEmail.Visible = True

                If (lblCustomerEmail.Text.Equals(String.Empty)) Then
                    emailCheckBox.Enabled = False
                    emailCheckBox.Checked = False
                Else
                    emailCheckBox.Enabled = True
                    emailCheckBox.Checked = True
                End If
                If (lblCustomerMobile.Text.Equals(String.Empty)) Then
                    mobileCheckBox.Enabled = False
                    mobileCheckBox.Checked = False
                Else
                    mobileCheckBox.Enabled = True
                    mobileCheckBox.Checked = True
                End If

                btnUpdateCustomerDetail.Text = "Update Customer Detail"

            End If

        End If
        If (lblCustomerEmail.Text.Equals(String.Empty)) Then
            emailCheckBox.Enabled = False
            emailCheckBox.Checked = False
        End If
        If (lblCustomerEmail.Text.Equals(String.Empty)) Then
            emailCheckBox.Enabled = False
            emailCheckBox.Checked = False
        End If

    End Sub

#End Region

#Region "Populate Appointment Summary"

    Sub populateAppointmentSummary()

        Dim faultsDataView As DataView = New DataView()
        faultsDataView = SessionManager.getSelectedTempFaultsDv
        '' The below values are added as currently there is no info in session **** Abdul Wahhab 14/02/2013 ****
        'faultsListString = "283,5109,36448,1028,1450"

        'SessionManager.setSelectedTempFaultIds(faultsListString)
        '' These need to be removed when original data is available

        If IsNothing(faultsDataView) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingSelectedFaultData, True)
        Else

            'Dim resultDataSet As DataSet = New DataSet()

            'objFaultBl.getFaultsList(resultDataSet, faultsListString)

            'If resultDataSet.Tables(0).Rows.Count = 0 Then
            '    ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            '    lnkBtnNextFault.Enabled = False
            '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            'Else

            'ViewState.Add(ViewStateConstants.ResultDataSet, faultsDataView)

            grdAppointments.DataSource = faultsDataView
            grdAppointments.DataBind()

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.SlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = faultsDataView.ToTable()

            changeFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count = 1 Then
                lnkBtnNextFault.Enabled = False
            End If

            'End If

        End If

    End Sub

#End Region

#Region "Populate Appointment Details"

    Sub populateAppointmentDetails()

        Dim resultDataSet As DataView = New DataView()
        resultDataSet = SessionManager.getSelectedTempFaultsDv

        If IsNothing(resultDataSet) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingSelectedFaultData, True)
        End If

        Dim durationTable As DataTable = New DataTable()
        durationTable = resultDataSet.ToTable()

        Dim length As Integer = 0

        If Not IsNothing(resultDataSet) Then

            length = durationTable.Rows.Count

        End If

        Dim duration As Double = 0

        For i As Integer = 0 To length - 1
            Dim dur As Double = 0
            Double.TryParse(durationTable.Rows(i).Item("Duration").ToString, dur)
            duration = duration + dur
        Next

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()
        '' The below data is hardcoded as there is currently no data in Session **** Abdul Wahhab 14/02/2013 ****
        'appointmentData.Operative = "Jade Burrell"
        'appointmentData.OperativeId = 480
        'appointmentData.Duration = duration
        'appointmentData.Time = "9:00 AM"
        'appointmentData.AppointmentDate = appointmentData.AppointmentDate.ToShortDateString
        'appointmentData.FaultNotes = "The tenant thinks that she has overtightened the hot tap"

        'SessionManager.setAppointmentDataForAppointmentSummary(appointmentData)
        '' These code lines will need to be removed **** Abdul Wahhab 14/02/2013 ****

        If appointmentData.OperativeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingOperativeData, True)
        End If

        lblOperative.Text = appointmentData.Operative
        If duration < 1 Or duration = 1 Then
            lblDuration.Text = duration.ToString() + " Hour Approx"
        Else
            lblDuration.Text = duration.ToString() + " Hours Approx"
        End If
        lblStartTime.Text = appointmentData.StartTime
        lblEndTime.Text = appointmentData.EndTime

        Dim dateStyleString = "d MMMM yyyy"


        lblStartDate.Text = Format(appointmentData.AppointmentStartDate, dateStyleString)
        lblEndDate.Text = Format(appointmentData.AppointmentEndDate, dateStyleString)
        'lblFaultNotes.Text = appointmentData.FaultNotes

    End Sub

#End Region

#Region "Scroll Faults"

    Sub scrollFaults()

        Dim resultDataSet As DataView = New DataView()
        resultDataSet = SessionManager.getSelectedTempFaultsDv

        If IsNothing(resultDataSet) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingSelectedFaultData, True)
        End If

        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = resultDataSet.ToTable()

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.SlideIndex)

            changeFaultValues(slideDataSet, slideIndex)

            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                lnkBtnNextFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.SlideIndex) = slideIndex
            End If

        End If

    End Sub

#End Region

#Region "Confirm Appointment"

    Sub confirmAppointment()

        Dim appointmentId As Integer = -1
        Dim tempCount As Integer = 0
        Dim isAppointmentPushed As Boolean = False
        Dim objFaultBL As FaultBL = New FaultBL
        Dim resultDataset As DataSet = New DataSet
        Dim jobSheetList As String = ""
        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        Me.saveAppointment(appointmentId, tempCount)

        If (Not appointmentId = -1) Then

            'When appointment is confirmed and saved sucessfully, disable confirm appointment button on appointment summary.
            btnConfirmAppointment.Enabled = False

            If (appointmentData.AppointmentStartDate = Today) Then
                isAppointmentPushed = Me.pushAppointmentConfirmed(appointmentId, ApplicationConstants.NewAppointmentType)

                If (Not isAppointmentPushed) Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message = UserMessageConstants.PushNotificationFailed
                End If
            End If

            objFaultBL.getJobsheetlistByAppointment(resultDataset, appointmentId)

            If (resultDataset.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.IsError = True
                uiMessageHelper.message += "<br />" + UserMessageConstants.NoJobSheetFound
            Else
                Try
                    jobSheetList = Me.getCommaSeparatedJobsheetList(resultDataset)
                    If (emailCheckBox.Checked) Then
                        Me.sendEmail(jobSheetList)
                    End If
                    If (mobileCheckBox.Checked) Then
                        Me.sendSMS(jobSheetList)
                    End If

                    ViewState.Add(ViewStateConstants.RedirectTempCount, tempCount)
                    Me.populateAppointmentConfirmedPopup()
                Catch ex As Exception
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message += "<br />" + ex.Message

                    If uiMessageHelper.IsExceptionLogged = False Then
                        ExceptionPolicy.HandleException(ex, "Exception Policy")
                    End If

                End Try

            End If
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
        End If

    End Sub

#End Region

#Region "Get comma separated jobsheet list"

    Function getCommaSeparatedJobsheetList(ByVal resultDataset As DataSet) As String

        Dim length As Integer = resultDataset.Tables(0).Rows.Count
        Dim jobSheetList As StringBuilder = New StringBuilder()

        For i As Integer = 0 To length - 1
            jobSheetList.Append(resultDataset.Tables(0).Rows(i).Item("JobSheetNumber").ToString() + ",")
        Next

        jobSheetList.Remove(jobSheetList.ToString().Length - 1, 1)

        Return jobSheetList.ToString()

    End Function

#End Region

#Region "Save Appointment"

    Sub saveAppointment(ByRef appointmentId As Integer, ByRef tempCount As Integer)


        Dim faultsDataView As DataView = New DataView()
        faultsDataView = SessionManager.getSelectedTempFaultsDv
        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()
        Dim customerId As Integer = SessionManager.getCustomerId()
        Dim propertyId As String = SessionManager.getPropertyId()
        Dim isCalendarAppointment As Boolean = False
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        If IsNothing(faultsDataView) Or appointmentData.OperativeId = 0 Or customerId = 0 Or propertyId = String.Empty Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else

            Dim faultsDataTable As DataTable = New DataTable()
            faultsDataTable = faultsDataView.ToTable()

            Dim length As Integer = faultsDataTable.Rows.Count

            Dim faultsList As StringBuilder = New StringBuilder()

            For i As Integer = 0 To length - 1
                faultsList.Append(faultsDataTable.Rows(i).Item("TempFaultId").ToString() + ",")
            Next

            faultsList.Remove(faultsList.ToString().Length - 1, 1)

            Dim objappointmentSummaryBO As New AppointmentSummaryBO()
            objappointmentSummaryBO.OperativeId = appointmentData.OperativeId
            objappointmentSummaryBO.AppointmentStartDate = appointmentData.AppointmentStartDate
            objappointmentSummaryBO.Duration = appointmentData.Duration
            objappointmentSummaryBO.FaultNotes = appointmentData.FaultNotes
            objappointmentSummaryBO.FaultsList = faultsList.ToString()
            objappointmentSummaryBO.Time = appointmentData.StartTime
            objappointmentSummaryBO.EndTime = appointmentData.EndTime
            objappointmentSummaryBO.PropertyId = propertyId
            objappointmentSummaryBO.CustomerId = customerId
            objappointmentSummaryBO.IsRecall = isRecall
            objappointmentSummaryBO.IsCalendarAppointment = isCalendarAppointment
            objappointmentSummaryBO.AppointmentNotes = txtAppointmentNotes.Text
            objappointmentSummaryBO.UserId = userId
            objappointmentSummaryBO.AppointmentEndDate = appointmentData.AppointmentEndDate

            objFaultBl.saveAppointmentDetails(objappointmentSummaryBO, appointmentId, tempCount)

            If IsDBNull(appointmentId) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SaveAppointmentFailed, True)
                appointmentId = -1
            Else
                Dim appointmentList As List(Of AppointmentBO) = New List(Of AppointmentBO)
                appointmentList = SessionManager.getConfirmedAppointmentIdList()

                If IsNothing(appointmentList) Then
                    appointmentList = New List(Of AppointmentBO)
                    Dim appointmentBO As AppointmentBO = New AppointmentBO()
                    appointmentBO.AppointmentId = appointmentId
                    appointmentList.Add(appointmentBO)
                    SessionManager.setConfirmedAppointmentIdList(appointmentList)
                Else
                    Dim appointmentBO As AppointmentBO = New AppointmentBO()
                    appointmentBO.AppointmentId = appointmentId
                    appointmentList.Add(appointmentBO)
                    SessionManager.setConfirmedAppointmentIdList(appointmentList)
                End If
            End If

        End If

    End Sub

#End Region

#Region "Change Fault Values"

    Sub changeFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        lblAreaName.Text = slideDataSet.Rows(slideIndex).Item("AreaName").ToString()
        lblDescription.Text = slideDataSet.Rows(slideIndex).Item("Description").ToString()

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("ProblemDays")) Then
            lblProblemDays.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() = 1 Then
                lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " day"
            Else
                lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " days"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("RecuringProblem")) Then
            lblReccuringProblem.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("RecuringProblem").ToString() = True Then
                lblReccuringProblem.Text = "This is a recurring problem"
            Else
                lblReccuringProblem.Text = "This is not a recurring problem"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("CommunalProblem")) Then
            lblCommunalProblem.Text = "NA"
        Else
            If slideDataSet.Rows(slideIndex).Item("CommunalProblem").ToString() = True Then
                lblCommunalProblem.Text = "This is a communal problem"
            Else
                lblCommunalProblem.Text = "This is not a communal problem"
            End If
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Notes")) Then
            lblFaultNotes.Text = "NA"
        Else
            lblFaultNotes.Text = slideDataSet.Rows(slideIndex).Item("Notes").ToString()
        End If

    End Sub

#End Region

#Region "Navigate to other page"

    Sub navigate()

        'First check if the current appointment is for followon, navigate to follow on report.
        If SessionManager.getIsFollowOn() Then
            Response.Redirect(PathConstants.ReportAreaPath + "?" + PathConstants.RepairsDashboard + "=fow", True)
        End If

        Dim tempCount As Integer = ViewState.Item(ViewStateConstants.RedirectTempCount)

        If tempCount > 0 Then

            If isRecall Then
                Response.Redirect(PathConstants.AvailableAppointments + GeneralHelper.getSrc(PathConstants.AppointmentSummarySrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
            Else
                Response.Redirect(PathConstants.AvailableAppointments + GeneralHelper.getSrc(PathConstants.AppointmentSummarySrcVal))
            End If

        Else

            If isRecall Then
                Response.Redirect(PathConstants.FaultBasketPath + GeneralHelper.getSrc(PathConstants.AppointmentSummarySrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
            Else
                Response.Redirect(PathConstants.FaultBasketPath + GeneralHelper.getSrc(PathConstants.AppointmentSummarySrcVal))
            End If

        End If


    End Sub

#End Region

#Region "Popups"

#Region "Populate appointment detail popup"

    Sub populateAppointmentDetailPopup()

        Me.resetAppointmentDetailPopup()

        Dim faultsDataView As DataView = New DataView()
        faultsDataView = SessionManager.getSelectedTempFaultsDv
        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        If appointmentData.OperativeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingOperativeData, True)
        ElseIf IsNothing(faultsDataView) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingSelectedFaultData, True)
        Else

            lblPopOperative.Text = appointmentData.Operative
            lblPopAppointmentStartDateTime.Text = String.Format("{0} {1}", appointmentData.StartTime, appointmentData.AppointmentStartDate.ToString(ApplicationConstants.CustomLongDateFormat))
            lblPopAppointmentEndDateTime.Text = String.Format("{0} {1}", appointmentData.EndTime, appointmentData.AppointmentEndDate.ToString(ApplicationConstants.CustomLongDateFormat))

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.PopupSlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = faultsDataView.ToTable()

            changePopupFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count <= 1 Then
                lnkBtnNextPopupFault.Visible = False
                lnkBtnNextPopupFault.Enabled = False
            End If
            popupConfirmAppointmentDetail.Show()

        End If


    End Sub

#End Region

#Region "Reset appointment detail popup controls"

    Sub resetAppointmentDetailPopup()
        lblPopOperative.Text = String.Empty
        lblPopupAreaName.Text = String.Empty
        lblPopupFaultDescription.Text = String.Empty
        lblPopDuration.Text = String.Empty
        lblPopTrade.Text = String.Empty
        lblPopDue.Text = String.Empty
        lblPopAppointmentStartDateTime.Text = String.Empty
        lblPopAppointmentEndDateTime.Text = String.Empty
    End Sub

#End Region

#Region "Scroll Popup Faults"

    Sub scrollPopupFaults()

        Dim resultDataSet As DataView = New DataView()
        resultDataSet = SessionManager.getSelectedTempFaultsDv
        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = resultDataSet.ToTable()

        If (IsNothing(resultDataSet)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingSelectedFaultData, True)
        End If

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.PopupSlideIndex)
            changePopupFaultValues(slideDataSet, slideIndex)
            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                lnkBtnNextPopupFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.PopupSlideIndex) = slideIndex
            End If

        End If
    End Sub

#End Region

#Region "Change Popup Fault Values"

    Sub changePopupFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("AreaName")) Then
            lblPopupAreaName.Text = "NA"
        Else
            lblPopupAreaName.Text = slideDataSet.Rows(slideIndex).Item("AreaName").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Description")) Then
            lblPopupFaultDescription.Text = "NA"
        Else
            lblPopupFaultDescription.Text = slideDataSet.Rows(slideIndex).Item("Description").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Duration")) Then
            lblPopDuration.Text = "NA"
        Else
            lblPopDuration.Text = slideDataSet.Rows(slideIndex).Item("Duration").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("TradeName")) Then
            lblPopTrade.Text = "NA"
        Else
            lblPopTrade.Text = slideDataSet.Rows(slideIndex).Item("TradeName").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("CompleteDueDate")) Then
            lblPopDue.Text = "NA"
        Else

            Dim aptDueDate As DateTime = Convert.ToDateTime(slideDataSet.Rows(slideIndex).Item("CompleteDueDate").ToString())
            Dim format As String = "d  MMMMMMMMMM yyyy"
            aptDueDate.ToString()
            lblPopDue.Text = aptDueDate.ToString(format)
        End If

    End Sub

#End Region

#Region "Push appointment confirmed"

    Public Function pushAppointmentConfirmed(ByVal appointmentId As Integer, ByVal type As Integer) As Boolean
        Dim URL As String = String.Format("{0}?appointmentId={1}&type={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentId, type)
        Dim request As WebRequest = Net.WebRequest.Create(URL)
        request.Credentials = CredentialCache.DefaultCredentials
        Dim response As WebResponse = request.GetResponse()
        Dim dataStream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        response.Close()

        If (responseFromServer.ToString.Equals("true")) Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "Populate appointment confirmed popup"

    Sub populateAppointmentConfirmedPopup()

        lblAptArrangedOperative.Text = String.Empty

        Dim appointmentData As AppointmentBO = New AppointmentBO()
        appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

        If appointmentData.OperativeId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingOperativeData, True)
        Else
            lblAptArrangedOperative.Text = appointmentData.Operative + " "
        popupAppointmentConfirmed.Show()
        End If


    End Sub

#End Region

#End Region

#Region "Email Functionality"

#Region "Send Email"
    Private Sub sendEmail(ByVal jobSheetNumberList As String)
        If (emailCheckBox.Checked) Then

            Dim appointmentData As AppointmentBO = New AppointmentBO()
            appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()

            'Dim filepath As String = Server.MapPath("~/Email/AppointmentConfirmation.htm")


            Dim recepientEmail As String = lblCustomerEmail.Text
            Dim recepientName As String = lblCustomerName.Text

            Try
                If Validation.isEmail(recepientEmail) Then
                    'Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheetNumberList, appointmentData.Time, appointmentData.AppointmentDate, lblCustomerName.Text)

                    Dim alternateView = GeneralHelper.populateBodyAppointmentConfirmationEmail(jobSheetNumberList, appointmentData.StartTime, appointmentData.AppointmentStartDate, recepientName)
                    Dim subject As String = ApplicationConstants.SubjectAppointmentConfirmationEmail

                    Dim mailMessage As New Mail.MailMessage

                    mailMessage.Subject = subject
                    mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
                    mailMessage.AlternateViews.Add(alternateView)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)
                    'EmailHelper.sendHtmlFormattedEmail(lblCustomerName.Text, recepientEmail, subject, body)
                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
                End If
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedEmailError + ex.Message, True)

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If
            End Try

        End If

    End Sub
#End Region

#End Region
#Region "Send SMS"
    Private Sub sendSMS(ByVal jobSheetNumberList As String)
        If (mobileCheckBox.Checked) Then
            Dim appointmentData As AppointmentBO = New AppointmentBO()
            appointmentData = SessionManager.getAppointmentDataForAppointmentSummary()


            Dim recepientMobile As String = lblCustomerMobile.Text
            Dim recepientName As String = lblCustomerName.Text

            Try
                'If Validation.isMobile(recepientMobile) Then
                If recepientMobile.Length >= 10 Then

                    Dim body As String = GeneralHelper.populateBodyAppointmentConfirmationSMS(jobSheetNumberList, appointmentData.StartTime, appointmentData.AppointmentStartDate)
                    'Dim smsUrl As String = ResolveClientUrl(PathConstants.SMSURL)

                    'Dim javascriptMethod As String = "sendSMS('" + recepientMobile + "','" + body + "','" + smsUrl + "')"
                    'ScriptManager.RegisterStartupScript(Me, Page.GetType, "smsScript", javascriptMethod, True)
                    GeneralHelper.sendSmsUpdatedAPI(body, recepientMobile)

                Else
                    uiMessageHelper.IsError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
                End If

                'GeneralHelper.sendSMS(body, recepientMobile.Replace(" ", String.Empty))
                'Dim request As WebRequest = WebRequest.Create(ApplicationConstants.SMSUrl)
                '' Set the Method property of the request to POST.
                'request.Method = "POST"
                '' Create POST data and convert it to a byte array.

                'Dim strdata As String

                'strdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(recepientMobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)

                'Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strdata)
                '' Set the ContentType property of the WebRequest.
                'request.ContentType = "application/x-www-form-urlencoded"
                '' Set the ContentLength property of the WebRequest.
                'request.ContentLength = byteArray.Length
                '' Get the request stream.
                'Dim dataStream As Stream = request.GetRequestStream()
                '' Write the data to the request stream.
                'dataStream.Write(byteArray, 0, byteArray.Length)
                '' Close the Stream object.
                'dataStream.Close()
                '' Get the response.
                'Dim response As WebResponse = request.GetResponse()
                '' Display the status.
                ''Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
                '' Get the stream containing content returned by the server.f
                'dataStream = response.GetResponseStream() ' Open the stream using a StreamReader for easy access.
                'Dim reader As New StreamReader(dataStream) ' Read the content.
                'Dim responseFromServer As String = reader.ReadToEnd()
                '' Display the content.
                ''Console.WriteLine(responseFromServer)
                '' Clean up the streams.
                'reader.Close()
                'dataStream.Close()
                'response.Close()





                'End If
            Catch ex As Exception
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentSavedSMSError + ex.Message, True)

                If uiMessageHelper.IsExceptionLogged = False Then
                    ExceptionPolicy.HandleException(ex, "Exception Policy")
                End If
            End Try
        End If


    End Sub
#End Region

#End Region

    Protected Sub mobileCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles mobileCheckBox.CheckedChanged

    End Sub

    Protected Sub emailCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles emailCheckBox.CheckedChanged

    End Sub
End Class