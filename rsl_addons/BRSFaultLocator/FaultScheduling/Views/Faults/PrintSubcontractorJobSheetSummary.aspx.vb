﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject

Public Class PrintSubcontractorJobSheetSummary
    Inherits PageBase

#Region "Data Members"
    Dim customerObj As CustomerBO = New CustomerBO()
#End Region

#Region "Events"

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                If (populateSessionValues()) Then

                    ' Populate customer Information
                    populateCustomerInformation()

                    ' Populate Contractor JobSheet 
                    populateContractorJobsheet(ViewState(ViewStateConstants.CurrentIndex))

                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions "

#Region "Populate contractor jobsheet"
    Private Sub populateContractorJobsheet(ByVal currentIndex As Integer)

        ' Get array from Viewstate
        Dim arrTempFaultBO As ArrayList = DirectCast(SessionManager.getTemporaryFaultBo, ArrayList)
        ' Get current temp fault BO 
        Dim tempFaultBO As TempFaultBO = arrTempFaultBO.Item(currentIndex)
        Dim propertyId As String = ViewState(ViewStateConstants.PropertyId)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objFaultsBl As FaultBL = New FaultBL()

        objFaultsBl.getSubcontractorJobSheetDetails(resultDataSet, tempFaultBO, propertyId)


        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdAsbestos.Visible = False
        Else

            ' Populating visible fields

            grdAsbestos.Visible = True
            grdAsbestos.DataSource = resultDataSet.Tables("Asbestos")
            grdAsbestos.DataBind()

            lblContractor.Text = resultDataSet.Tables("FaultDetail").Rows(0)(1).ToString
            lblOperativeName.Text = "-"
            lblFaultPriority.Text = resultDataSet.Tables("FaultDetail").Rows(0)(3).ToString
            lblFaultCompletionDateTime.Text = resultDataSet.Tables("FaultDetail").Rows(0)(4).ToString
            lblOrderDate.Text = resultDataSet.Tables("FaultDetail").Rows(0)(5).ToString
            lblFaultLocation.Text = resultDataSet.Tables("FaultDetail").Rows(0)(6).ToString
            lblFaultDescription.Text = resultDataSet.Tables("FaultDetail").Rows(0)(7).ToString
            lblTrade.Text = resultDataSet.Tables("FaultDetail").Rows(0)(8).ToString
            lblFaultOperatorNote.Text = resultDataSet.Tables("FaultDetail").Rows(0)(11).ToString

        End If

    End Sub
#End Region

#Region "Populate Session Value"
    Private Function populateSessionValues() As Boolean
        If (validateSessionValues()) Then
            ViewState.Add(ViewStateConstants.PropertyId, SessionManager.getPropertyId())
            customerObj = SessionManager.getCustomerData()
            ViewState.Add(ViewStateConstants.CurrentIndex, SessionManager.getSubcontractorJobSheetIndex)
            Return True
        End If

        Return False
    End Function
#End Region

#Region "Validate sessions values"
    Private Function validateSessionValues() As Boolean

        If (SessionManager.getSubcontractorJobSheetIndex < 0) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidSubcontractorJobSheetIndex
            Return False
        End If

        If (SessionManager.getPropertyId = Nothing) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidPropertyId
            Return False
        End If


        If (DirectCast(SessionManager.getTemporaryFaultBo(), ArrayList).Count = 0) Then
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.NoTempFaultData
            Return False
        End If

        Return True

    End Function
#End Region

#Region "Populate customer information"
    Private Sub populateCustomerInformation()

        ' Header Customer Labels
        lblClientNameHeader.Text = customerObj.Name
        lblClientStreetAddressHeader.Text = customerObj.Street
        lblClientCityHeader.Text = customerObj.City
        lblClientTelPhoneNumberHeader.Text = customerObj.Telephone

        ' Body customer Labels

        lblClientName.Text = customerObj.Name
        lblClientStreetAddress.Text = customerObj.Street
        lblClientCity.Text = customerObj.City
        lblClientPostCode.Text = customerObj.PostCode
        lblClientRegion.Text = customerObj.Area
        lblClientTelPhoneNumber.Text = customerObj.Telephone
        lblClientMobileNumber.Text = customerObj.Mobile
        lblClientEmailId.Text = customerObj.Email

    End Sub
#End Region


#End Region

End Class