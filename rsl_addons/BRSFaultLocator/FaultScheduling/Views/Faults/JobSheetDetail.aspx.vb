﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class JobSheetDetail
    Inherits PageBase


#Region "Events"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                Me.getSetQueryStringParams()
                If uiMessageHelper.IsError = False Then
                    Dim jobSheetNumber As String = SessionManager.getJobSheetNumber()
                    Me.PopulateJobSheetDetails(jobSheetNumber)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try



    End Sub


#End Region

#Region "Functions "

#Region "get Set Query String Params"
    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString(ApplicationConstants.JobSheetNumber)) Then
            SessionManager.setJobSheetNumber(Convert.ToString(Request.QueryString(ApplicationConstants.JobSheetNumber)))

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidJobSheetNumber
        End If
    End Sub
#End Region

#Region "Populate Values"

    Sub PopulateJobSheetDetails(ByVal jobSheetNumber As String)

        Try
            ' Populate appointment and fault info from DB

            Dim resultDataSet As DataSet = New DataSet()
            Dim objFaultsBl As FaultBL = New FaultBL()

            objFaultsBl.getJobSheetDetails(resultDataSet, jobSheetNumber)


            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                grdAsbestos.Visible = False
            Else
                grdAsbestos.Visible = True
                grdAsbestos.DataSource = resultDataSet.Tables("Asbestos")
                grdAsbestos.DataBind()

                lblFaultId.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(0).ToString
                lblContractor.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(1).ToString
                lblOperativeName.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(2).ToString
                lblFaultPriority.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(3).ToString
                lblFaultCompletionDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(4).ToString
                lblOrderDate.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(5).ToString
                lblFaultLocation.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(6).ToString
                lblFaultDescription.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(7).ToString
                lblFaultOperatorNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(8).ToString
                lblFaultAppointmentDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(9).ToString
                txtAppointmentNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(10).ToString
                lblTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(11).ToString
                lblTrade.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(12).ToString


            End If


            If (resultDataSet.Tables("Customer").Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                grdAsbestos.Visible = False
            Else

                ' Header Customer Labels
                lblClientNameHeader.Text = resultDataSet.Tables("Customer").Rows(0)(0).ToString
                lblClientStreetAddressHeader.Text = resultDataSet.Tables("Customer").Rows(0)(1).ToString
                lblClientCityHeader.Text = resultDataSet.Tables("Customer").Rows(0)(2).ToString
                lblClientTelPhoneNumberHeader.Text = resultDataSet.Tables("Customer").Rows(0)(5).ToString

                ' Body customer Labels

                lblClientName.Text = resultDataSet.Tables("Customer").Rows(0)(0).ToString
                lblClientStreetAddress.Text = resultDataSet.Tables("Customer").Rows(0)(1).ToString
                lblClientCity.Text = resultDataSet.Tables("Customer").Rows(0)(2).ToString
                lblClientPostCode.Text = resultDataSet.Tables("Customer").Rows(0)(3).ToString
                lblClientRegion.Text = resultDataSet.Tables("Customer").Rows(0)(4).ToString
                lblClientTelPhoneNumber.Text = resultDataSet.Tables("Customer").Rows(0)(5).ToString
                lblClientMobileNumber.Text = resultDataSet.Tables("Customer").Rows(0)(6).ToString
                lblClientEmailId.Text = resultDataSet.Tables("Customer").Rows(0)(7).ToString

            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try



    End Sub


#End Region


#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click

    End Sub
End Class