﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/BlankMasterPage.Master"
    CodeBehind="PrintSubContractorJobSheetSummaryUpdate.aspx.vb" Inherits="FaultScheduling.PrintSubContractorJobSheetSummaryUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function Clickheretoprint() {
            self.print();

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label ID="lblRecallDetailsFor" runat="server" Font-Bold="true" Text="Job sheet summary for : "></asp:Label>
                <asp:Label ID="lblClientNameHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Font-Bold="true"
                    Text=""></asp:Label>
                &nbsp;,
                <asp:Label ID="lblClientCityHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Font-Bold="true" Text=" Tel :"> </asp:Label>
                <asp:Label ID="lblClientTelPhoneNumberHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblFaultLogID" runat="server" Visible="false" />
                <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Font-Bold="True">Status : </asp:Label>
                <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Contractor :
            </td>
            <td>
                <asp:Label ID="lblContractor" runat="server" Font-Bold="true"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Operative :
            </td>
            <td>
                <asp:Label ID="lblOperativeName" runat="server">-</asp:Label>
            </td>
            <td>
                <asp:Label ID="lblClientCity" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Priority :
            </td>
            <td>
                <asp:Label ID="lblPriority" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Completion Due :
            </td>
            <td>
                <asp:Label ID="lblCompletionDateTime" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Order Date :
            </td>
            <td>
                <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
            </td>
            <td>
                Tel : &nbsp;<asp:Label ID="lblClientTelPhoneNumber" runat="server" Font-Bold="true"
                    Text=" "></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Location :
            </td>
            <td>
                <asp:Label ID="lblLocation" runat="server"></asp:Label>
            </td>
            <td>
                Mobile: &nbsp;
                <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Description:
            </td>
            <td>
                <asp:Label ID="lblDescription" runat="server" Font-Bold="True"></asp:Label>
            </td>
            <td>
                Email :
                <asp:Label ID="lblClientEmail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Trade :
            </td>
            <td>
                <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Note :
            </td>
            <td>
                <asp:TextBox ID="lblNotes" runat="server" CssClass="roundcornerby5" Enabled="false"
                    Height="65px" TextMode="MultiLine" Width="248px"></asp:TextBox>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <b>Appointment</b>
            </td>
            <td valign="top">
                <asp:Label ID="lblTime" runat="server" Text="N/A"></asp:Label>
                <br />
                <br />
            </td>
            <td>
                <b>Asbestos</b>
                <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    ShowHeader="false">
                    <Columns>
                        <asp:BoundField DataField="AsbRiskID" />
                        <asp:BoundField DataField="Description" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
