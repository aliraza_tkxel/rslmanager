﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SbPrintJobSheetDetail.aspx.vb"
    Inherits="FaultScheduling.SbPrintJobSheetDetail" %>

<form id="form1" runat="server">
<asp:panel id="pnlMessage" runat="server" visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:panel>
<asp:scriptmanager runat="server"></asp:scriptmanager>
<table id="jobsheet_detail_table" style="width: 100%; margin-top: 10px; text-align: left;">
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : "
                            Font-Bold="true"></asp:Label>
                        <asp:Label ID="lblSchemeLabel" runat="server" Text="Scheme:" Font-Bold="true" ForeColor="LightGray"></asp:Label>
                        <asp:Label ID="lblClientNameHeader" runat="server" Text=" " Font-Bold="true"></asp:Label>
                        <asp:Label ID="lblBlockLabel" runat="server" Text="; Block:" Font-Bold="true" ForeColor="LightGray"></asp:Label>
                        &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contractor :
                    </td>
                    <td>
                        <asp:Label ID="lblContractor" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Operative :
                    </td>
                    <td>
                        <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Priority :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Completion due :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Order date :
                    </td>
                    <td>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Location :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultDescription" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Trade :
                    </td>
                    <td>
                        <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Note :
                    </td>
                    <td>
                        <asp:TextBox Enabled="false" ID="lblFaultOperatorNote" CssClass="roundcornerby5"
                            runat="server" TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Appointment :</b>
                    </td>
                    <td>
                        <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblFaultAppointmentDateTime" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtAppointmentNote" Enabled="false" runat="server" TextMode="MultiLine"
                            CssClass="roundcornerby5" Height="53px" Width="366px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;
                    </td>
                </tr>
            </table>
<div style="width: 100%; text-align: left; clear: both;">
</div>
</form>
