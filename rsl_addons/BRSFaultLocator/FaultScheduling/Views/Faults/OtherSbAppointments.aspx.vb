﻿Imports FLS_BusinessLogic
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class OtherSbAppointments
    Inherits PageBase
    'TODO (BAL) we need to disscuss about the planned maintainance protion of the form.
#Region "Properties "
    Dim objFaultBL As FaultBL = New FaultBL()
    Dim faultLogId As Integer = 0
#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' This event handles the load event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'TODO we need to put update panel across the gridviews.
        Try
            If Not IsPostBack Then
                Dim schemeId As Integer = SessionManager.getSchemeId()
                Dim blockId As Integer = SessionManager.getBlockId()
                fillGrids(schemeId, blockId)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Pre Render"
    ''' <summary>
    ''' This event handles the pre-render event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim faultId As Integer = CType(SessionManager.getSbFaultId(), Integer)
        If faultId = 0 Then
            btnContinue.Enabled = False
        Else
            btnContinue.Enabled = True
            btnContinue.PostBackUrl = PathConstants.MoreSbDetails + GeneralHelper.getSrc(PathConstants.OtherSbAppointmentSrcVal)
        End If
        Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.SearchSbFault, GeneralHelper.getSrc(PathConstants.OtherSbAppointmentSrcVal))

    End Sub
#End Region

#Region "Controls Events"

#Region "grd Faults Row Command"
    ''' <summary>
    ''' This event handles the rowCommand event of the grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdFaults_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)

        Try
            'from there we move to the next page where required.
            Dim grd As GridView = CType(e.CommandSource, GridView)
            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim keyValue As String = grd.DataKeys(index).Value
            'Changed - By behroz - 19/02/2013 - Start
            SessionManager.setSbFaultLogId(keyValue)
            Response.Redirect(PathConstants.RecallSbDetails + GeneralHelper.getSrc(PathConstants.OtherSbAppointmentSrcVal), True)
            'Changed - By behroz - 19/02/2013 - End

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try
    End Sub
#End Region

#Region "grd Current Faults Row Command"
    ''' <summary>
    ''' This event handles the View button click of grdCurrentFaults button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdCurrentFaults_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdCurrentFaults.RowCommand
        Try
            Dim grd As GridView = CType(e.CommandSource, GridView)
            Dim index = Convert.ToInt32(e.CommandArgument)
            Dim keyValue As String = grd.DataKeys(index).Value

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try

    End Sub
#End Region

#Region "btn View Current Faults Click"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnViewCurrentFaults_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim appointmentId As Integer = Convert.ToInt32(sender.CommandArgument)
            Dim dt As DataTable = New DataTable()
            dt = Me.filterCurrentFaults(appointmentId)
            Me.SetCurrentFaultsInSession(dt)

            'Set tradeid in session, it is specifically used to select patch filters on calendar.
            If dt.Rows.Count > 0 Then
                SessionManager.setSbTradeId(dt.Rows(0).Item("TradeID"))
            Else
                SessionManager.removeSbTradeId()
            End If

            Response.Redirect(PathConstants.CancellationSbAppointmentSummary + GeneralHelper.getSrc(PathConstants.OtherSbAppointmentSrcVal) + "&" + PathConstants.AppointmentId & "=" + appointmentId.ToString())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try


    End Sub
#End Region

#Region "grd Current Faults Row Data Bound"

    Protected Sub grdCurrentFaults_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdCurrentFaults.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lblAppointmentStatus As Label = CType(e.Row.FindControl("lblAppointmentStatus"), Label)

                Dim appointmentStatus As String = lblAppointmentStatus.Text
                If appointmentStatus = "Assigned To Contractor" Then
                    Dim btnView As Button = CType(e.Row.FindControl("btnViewCurrentFaults"), Button)
                    btnView.Visible = False
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)

            End If
        End Try
    End Sub
#End Region

#End Region

#End Region

#Region "functions"

#Region "Grid Functions "
    
    Private Sub fillGrids(ByVal schemeId As Integer, ByVal blockId As Integer)
        Dim dsAppointments As DataSet = New DataSet()
        dsAppointments = objFaultBL.GetFaultsAndAppointments(schemeId, blockId)
        If (dsAppointments.Tables.Count > 0) Then
            BindGrid(grdCurrentFaults, dsAppointments.Tables(ApplicationConstants.CurrentFaultsDataTable))
            BindGrid(grdRecall, dsAppointments.Tables(ApplicationConstants.RecallFaultsDataTable))
            BindGrid(grdGasServicingAppointments, dsAppointments.Tables(ApplicationConstants.GasAppointmentssDataTable))
            BindGrid(grdPlannedAppointments, dsAppointments.Tables(ApplicationConstants.PlannedDataTable))
            Me.SetCurrentFaultsInSession(dsAppointments.Tables(ApplicationConstants.CurrentFaultsDataTable))
        End If
    End Sub

    ''' <summary>
    ''' This method binds the source to the grid
    ''' </summary>
    ''' <param name="grid"></param>
    ''' <param name="datasource"></param>
    ''' <remarks></remarks>
    Private Sub BindGrid(ByRef grid As GridView, ByVal datasource As DataTable)
        grid.DataSource = datasource
        grid.DataBind()
    End Sub
#End Region

#Region "Save Current Faults In Session"
    Private Sub SetCurrentFaultsInSession(ByRef dt As DataTable)
        SessionManager.setSbConfirmedFaults(dt)
    End Sub
#End Region

#Region "get Current FaultscFrom Session"
    Private Function getCurrentFaultsFromSession() As DataTable
        Return SessionManager.getSbConfirmedFaults()
    End Function
#End Region

#Region "get Filtered Current Faults"
    Private Function filterCurrentFaults(ByVal appointmentId As Integer) As DataTable

        SessionManager.setSbAppointmentId(appointmentId)
        Dim currentFaultsDt As DataTable = New DataTable()
        currentFaultsDt = Me.getCurrentFaultsFromSession()
        SessionManager.setIsSbAppointmentRearrange(True)
        Dim dv As DataView = New DataView()
        dv = currentFaultsDt.DefaultView()
        dv.RowFilter = "AppointmentId = " + appointmentId.ToString()
        currentFaultsDt = dv.ToTable()
        Return currentFaultsDt
    End Function
#End Region

#Region "Replace Keywords"
    Public Function ReplaceKeywords(ByVal time As String) As String

        Return "<span class='highlight'>" + time + "</span>"

    End Function
#End Region

#Region "Highlight Text"
    Protected Function HighlightText(ByVal inputText As Object) As String

        If (Not IsDBNull(inputText)) Then
            Dim dateTime As String = inputText.ToString()
            Dim dateTimeArr() As String = dateTime.Split(" ")
            Dim timeStr As String = dateTimeArr(0)
            Dim dateStr As String = dateTimeArr(1)
            dateTime = ReplaceKeywords(timeStr) + " " + dateStr
            Return dateTime
        Else
            Return ""
        End If

    End Function
#End Region

#End Region


End Class