﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="AvailableSbAppointments.aspx.vb" Inherits="FaultScheduling.AvailableSbAppointments" %>

<%@ Register TagPrefix="custom" TagName="SchemeBlockDetails" Src="~/Controls/Common/SchemeBlockHeader.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        /* Over ridding default styles */
        .main
        {
            width: auto;
            height: auto;
        }
        
        .grid_temp_faults th:nth-child(2)
        {
            padding-left: 5px !important;
        }
        .text_a_r_padd__r_20px
        {
            padding-right: 1px !important;
        }
        
    </style>
    <script type="text/javascript" src="../../Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&v=3.17"></script>
    <script type="text/javascript">
        var map;
        var markers = [];
        var markersAddress = [];
        function showMap() {
            var destinationAddress = '<%= schemeBlockDetails.ClientStreetAddressLabel %>';

            destinationAddress = destinationAddress.replace("\'", "");

            var map_canvas = document.getElementById("mapContainer");
            var map_options = {
                center: new google.maps.LatLng(52.630886, -0.4573616),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(map_canvas, map_options);

            addOriginMarkers();

            var maxZindex = google.maps.Marker.MAX_ZINDEX;
            geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': destinationAddress }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: destinationAddress,
                        zIndex: maxZindex,
                        icon: "https://maps.google.com/mapfiles/kml/paddle/D.png"
                    });
                }
                console.log(status + ":(destination address) " + destinationAddress + " Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
            });
        }

        function addOriginMarkers() {

            $.ajax({
                type: "POST",
                contentType: "application/json",
                url: "AvailableAppointments.aspx/getExistingAppointmentsData",
                data: "{}",
                dataType: "json",
                success: function (response) {
                    var appointments = JSON.parse(response.d);
                    var counter = 0;
                    var timeout = 1000;
                    for (var i = 0; i < appointments.length; i++) {

                        if (counter > 0 && counter % 5 == 0) {
                            timeout += 1000;
                        }
                        setTimeout(setMarker(appointments[i]), timeout);
                        counter++;
                    }
                }
            });
        }

        function setMarker(appointment) {
            var adrs = appointment.Address + ", " + appointment.TownCity + ", " + appointment.County;

            // Check if a property address is already processed cancel geocode call and return.
            if ($.inArray(adrs, markersAddress) >= 0) {
                console.log("skiped:" + adrs);
                return;
            }

            geocoder = new google.maps.Geocoder();

            geocoder.geocode({ 'address': adrs }, function (results, status) {
                geoCodeStatus = status;
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(status + ":" + adrs + "Info(" + "latlong:" + results[0].geometry.location.lat() + "," + results[0].geometry.location.lng() + ")");
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        title: adrs,
                        icon: "https://maps.google.com/mapfiles/kml/paddle/ylw-circle.png"
                    });
                    markers.push(marker);
                    markersAddress.push(adrs);
                }
                else {
                    console.log(status + ":" + adrs);
                }
            });
        }

        // Sets the map on all markers in the array.
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setAllMap(null);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
            markersAddress = [];
        }

        google.maps.event.addDomListener(window, 'load', showMap);
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div class="clear" />
    <div style="margin: 0 0 0 12px">
        <custom:SchemeBlockDetails ID="schemeBlockDetails" runat="server" StartString="Next available appointments for:">
        </custom:SchemeBlockDetails>
    </div>
    <table width="100%">
        <tr>
            <td style="width: 700px;">
                <div class="availableAptDivContainer">
                    <asp:UpdatePanel ID="updPanelMain" runat="server">
                        <ContentTemplate>
                            <div class="border_height40pc" style="margin: -24px 0px" id="divPageContainer" runat="server">
                            </div>
                            <asp:Button ID="btnBackToBasket" runat="server" Text="< Back to Basket" CssClass="backToBasket" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td style="vertical-align: top;">
                <div class="appointmentsMap" id="mapContainer">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
