﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/Fault.Master"
    CodeBehind="SbFaultBasket.aspx.vb" Inherits="FaultScheduling.SbFaultBasket" %>

<%@ Register TagPrefix="tgFaultNotes" Src="~/Controls/Fault/FaultNotes.ascx" TagName="FaultNotes" %>
<%@ Register TagPrefix="header" TagName="SbHeader" Src="~/Controls/Common/SchemeBlockHeader.ascx" %>
<%@ Register TagPrefix="contractor" TagName="AssignToContractor" Src="~/Controls/Fault/SbAssignToContractor.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function highlightDescription(index, state) {
            var description, cell, startBoldTag, endBoldTag, browserName;
            var nAgt = navigator.userAgent;

            // In Internet Explorer, the true version is after "MSIE" in userAgent
            if ((verOffset = nAgt.indexOf("MSIE")) != -1) {

                var ver = getInternetExplorerVersion();

                if (ver > -1) {
                    if (ver >= 9.0) {
                        startBoldTag = "<b>"
                        endBoldTag = "</b>"
                    }
                    else {
                        startBoldTag = "<B>"
                        endBoldTag = "</B>"
                    }
                }

            } else {
                startBoldTag = "<b>"
                endBoldTag = "</b>"
            }

            if (state == true) {

                cell = document.getElementById('<%=gdFaultBasket.ClientID %>').rows[parseInt(index) + 1].cells[2];
                description = cell.innerHTML;
                description = startBoldTag + description + endBoldTag;
                cell.innerHTML = description;

            } else {
                cell = document.getElementById('<%=gdFaultBasket.ClientID %>').rows[parseInt(index) + 1].cells[2];
                description = cell.innerHTML;
                description = description.replace(startBoldTag, "")
                description = description.replace(endBoldTag, "")
                cell.innerHTML = description;

            }
        }


        function getInternetExplorerVersion()
        // Returns the version of Internet Explorer or a -1
        // (indicating the use of another browser).
        {
            var rv = -1; // Return value assumes failure.
            if (navigator.appName == 'Microsoft Internet Explorer') {
                var ua = navigator.userAgent;
                var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                if (re.exec(ua) != null)
                    rv = parseFloat(RegExp.$1);
            }
            return rv;
        }

        function closeWindow() {
            var browserName = navigator.appName;
            var browserVer = parseInt(navigator.appVersion);

            if (browserName == "Microsoft Internet Explorer") {
                var ie7 = (document.all && !window.opera && window.XMLHttpRequest) ? true : false;
                if (ie7) {
                    //This method is required to close a window without any prompt for IE7 & greater versions.
                    window.open('', '_parent', '');
                    window.close();
                }
                else {
                    //This method is required to close a window without any prompt for IE6
                    this.focus();
                    self.opener = this;
                    self.close();
                }
            } else {
                //For NON-IE Browsers except Firefox which doesnt support Auto Close
                try {
                    this.focus();
                    self.opener = this;
                    self.close();
                }
                catch (e) {

                }

                try {
                    window.open('', '_self', '');
                    window.close();
                }
                catch (e) {

                }
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelRecallDetails" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <table id="more_details_table_1">
                <tr>
                    <td colspan="2">
                        <header:SbHeader ID="SbHeader" runat="server" StartString="More fault details for: " />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="gdFaultBasket" runat="server" AutoGenerateColumns="False" Width="100%"
                            AllowSorting="True" AllowPaging="False" BorderStyle="None" GridLines="None" CellSpacing="6"
                            HorizontalAlign="Left">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkGroup" runat="server" CommandArgument='<%#Eval("TempFaultID")%>'>
                                        </asp:CheckBox>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="10px" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("AreaName") %>'></asp:Label>
                                        <asp:HiddenField ID="hfNotes" runat="server" Value='<%#Eval("Notes") %>' />
                                        <asp:HiddenField ID="hfTempFaultID" runat="server" Value='<%#Eval("TempFaultID") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="120px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description:">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lblDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>--%>
                                       <asp:LinkButton ID="lnkBtnDescription" runat="server" Text='<%#Eval("Description") %>'
                                            OnClick="lnkBtnDescription_Clicked"></asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="350px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration:">
                                    <ItemTemplate>
                                        <asp:Button ID="btnDuration" CommandArgument='<%#Eval("duration")%>' runat="server"
                                            Visible="False"></asp:Button>
                                        <asp:Label ID="lblDuration" runat="server" Text=''></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField SortExpression="Trade" HeaderText="Trade:" HeaderStyle-ForeColor="Black"
                                    ConvertEmptyStringToNull="False" HeaderStyle-BorderStyle="None" FooterStyle-VerticalAlign="Middle"
                                    HeaderStyle-CssClass="sort-expression">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrade" runat="server" Text='<%#Eval("Trade") %>'></asp:Label>
                                        <asp:Button ID="btnTrade" CommandArgument='<%#Eval("TradeId")%>' runat="server" Visible="False">
                                        </asp:Button>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="sort-expression" HorizontalAlign="left" />
                                    <ItemStyle Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Response:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblResponse" runat="server" Text='<%#Eval("ResponseTime").Tostring()%>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="80px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" Contractor:">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="isAppointmentConfirmed" CommandArgument='<%#Eval("isAppointmentConfirmed")%>'
                                            runat="server" Visible="False"></asp:LinkButton>
                                        <asp:DropDownList ID="ddlContractors" runat="server" Visible="True" Width="220px"
                                            Align="center">
                                        </asp:DropDownList>
                                        <asp:Table ID="tblSuccess" Align="left" Width="220px" runat="server" BorderColor="#666666"
                                            BorderWidth="0px" Visible="False">
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <asp:LinkButton ID="imgCompleted" runat="server" CommandName="btnCompleted" CommandArgument='<%#Eval("faultLogId")%>'
                                                        Visible="True" Style="float: left; margin-right: 15px;"><img src="../../Images/completed.png" alt="Completed"   style="border:none;" />  </asp:LinkButton>
                                                    <asp:Label ID="lblJsnCompleted" runat="server" Text="" Style="float: left"></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblAptTime" runat="server" Text="" Style="float: left;"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="220px" />
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="imgDelete" runat="server" CommandName="btnDelete" CommandArgument='<%#Eval("TempFaultID")%>'
                                            OnClientClick="javascript:return confirm('Do you really want to delete the item?');"
                                            OnClick="deleteFault"><img src="../../Images/cross.png" alt="Delete Fault"  style="border:none;" />  </asp:LinkButton>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="10px" BorderStyle="None" HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle Wrap="True" />
                            <HeaderStyle BorderStyle="None" Font-Bold="True" />
                            <RowStyle BorderStyle="None" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="tradeGroupPanel" runat="server" BorderColor="White">
                            * Would you like to group trades?
                            <asp:RadioButton ID="rbtnGroupTradeYes" runat="server" GroupName="GroupTrades" CssClass="margin_right20"
                                Text="Yes" />
                            <asp:RadioButton ID="rbtnGroupTradeNo" runat="server" GroupName="GroupTrades" Text="No"
                                Checked="True" />
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Table ID="tblSubmitted" Width="60%" CssClass="ConfirmedTable" runat="server"
                            BorderColor="#666666" BorderWidth="1px" Visible="False" HorizontalAlign="center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="center">
                                    <p style="text-align: center; padding-left: 20px; padding-right: 20px; padding-top: 20px;
                                        padding-bottom: 10px;">
                                        <b>Faults have been submitted successfully!</b>
                                        <br />
                                        <br />
                                        The Fault(s) in your fault basket have been recorded and scheduled and the following
                                        JS number(s) assigned
                                        <br />
                                    </p>
                                    <asp:DataList ID="dlSubmittedJsn" runat="server" HorizontalAlign="center" CssClass="ConfirmedTable">
                                        <ItemTemplate>
                                            <%# Container.DataItem %>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" CssClass="ConfirmedTable" />
                                    </asp:DataList>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="width: 100%; text-align: right;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="margin-left: 40px">
                        <asp:Button ID="btnScheduleSelectedFaults" runat="server" Text="Schedule Selected Faults"
                            Enabled="true" CssClass="margin_right20" Style="float: right;" />
                        <asp:Button ID="btnAddMoreFaults" runat="server" Text="Add More Faults" CssClass="margin_right20"
                            Style="float: right;" />
                        <asp:Button ID="btnCloseWindow" runat="server" Text="Close Window" Visible="false"
                            Style="float: right;" OnClientClick="closeWindow()" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel ID="pnlFaultNotes" runat="server" CssClass="modalPopup" Style="width: 50%;
        display: none; padding: 0px !important;">
        <tgFaultNotes:FaultNotes runat="server" ID="ucFaultNotes"></tgFaultNotes:FaultNotes>
    </asp:Panel>
    <asp:Label ID="lblHiddenFaultNotesOpener" runat="server" Text="" Style="display: none;"
        ClientIDMode="Static" />
    <asp:ModalPopupExtender ID="mdlPopupFaultNotes" runat="server" PopupControlID="pnlFaultNotes"
        TargetControlID="lblHiddenFaultNotesOpener" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>

    <asp:Panel ID="pnlAssignToContractor" runat="server" CssClass="modalPopup" Style="
        width: 55%; display:none;  ">
      
        <div style="width: 100%; text-align: left; clear: both; overflow: auto; height: 500px;">
            <contractor:AssignToContractor ID="ucAssignToContractor" runat="server"></contractor:AssignToContractor>
        </div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <asp:Label ID="lblFaultIdHidden" runat="server" Text="" Style="display: none;" ClientIDMode="Static" />
    <asp:ModalPopupExtender ID="popupAssignToContractor" runat="server" PopupControlID="pnlAssignToContractor"
        TargetControlID="lblFaultIdHidden" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
</asp:Content>
