﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="JobSheetSummarySubcontractor.aspx.vb" Inherits="FaultScheduling.JobSheetSummarySubcontractor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function PrintJobSheet() {
            javascript: window.open('PrintSubcontractorJobSheetSummary.aspx');
        }    

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelRecallDetails" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <table id="jobsheet_detail_table" style="width: 100%; min-height: 100%;">
                <tr>
                    <td colspan="3">
                        <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : " Font-Bold="true"></asp:Label>
                        <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;,
                        <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel :" Font-Bold="true"> </asp:Label><asp:Label
                            ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                         
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        &nbsp;<asp:Button ID="btnPrevious" runat="server" Text="&lt; Previous" Width="100px" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnNext" runat="server" Text="Next >" Width="100px" OnClick="btnNext_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />
                    </td>
                </tr>
          
                <tr>
                    <td>
                        <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static" Font-Bold="True"
                            Visible="False"></asp:Label>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Contractor :
                    </td>
                    <td>
                        <asp:Label ID="lblContractor" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Operative :
                    </td>
                    <td>
                        <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Priority :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Completion due :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Order date :
                    </td>
                    <td>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <div class="fault_detail_right_col_first_div">
                            Tel :</div>
                        <div class="fault_detail_right_col_second_div">
                            <asp:Label ID="lblClientTelPhoneNumber" runat="server" Text=" " Font-Bold="true"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        Location :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                    </td>
                    <td>
                        Mobile :
                        <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Description :
                    </td>
                    <td>
                        <asp:Label ID="lblFaultDescription" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </td>
                    <td>
                        Email :
                        <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        Trade :
                    </td>
                    <td>
                        <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Note :
                    </td>
                    <td>
                        <asp:TextBox ID="txtBoxFaultOperatorNote" CssClass="roundcornerby5" runat="server"
                            Enabled="false" TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Appointment :</b></td>
                    <td>
                        <asp:Label ID="lblTime" runat="server" Text="N/A"></asp:Label>
                    </td>
                    <td>
                        <b>Asbestos :</b>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                            BorderStyle="None" CellSpacing="3" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="AsbRiskID" />
                                <asp:BoundField DataField="Description" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                  <tr>
                    <td colspan="3">
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
            <div style="width: 100%; text-align: left; clear: both;">
                <div class="text_a_r_padd__r_20px">
                    <asp:Button ID="btnBack" runat="server" Text=" &lt; Back" CssClass="margin_right20"
                        Width="100px" />
                    <asp:Button ID="btnConfirm" runat="server" CssClass="margin_right20" Text="Confirm"
                        Width="100px" Height="24px" />
                    <asp:Button ID="btnPrintJobSheet" runat="server" Text="Print Job Sheet" CssClass="margin_right20"
                        OnClientClick="PrintJobSheet()" Width="120px" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
