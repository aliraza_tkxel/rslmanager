﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="OtherAppointments.aspx.vb" Inherits="FaultScheduling.OtherAppointments" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="warranties" TagName="Warranties" Src="~/Controls/Fault/Warranties.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div class="fl_width_100_text_a_l" style="font-weight: bold;">
        Existing Faults
    </div>
    <br />
    <div class="fl_width_100_text_a_l grid_height" >
        <asp:UpdatePanel ID="updCurrentFaults" runat="server">
            <ContentTemplate>
                <asp:GridView ID="grdCurrentFaults" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="gridfaults" GridLines="None" DataKeyNames="FaultLogID"  EmptyDataText="No current fault exist!" EmptyDataRowStyle-ForeColor="Red"
                     Height="50px" HeaderStyle-VerticalAlign="Top" HeaderStyle-CssClass="gridfaults-header" HeaderStyle-HorizontalAlign="Left">
                    <Columns>

                        <asp:BoundField HeaderText="Recorded" ItemStyle-Width="100px" 
                            DataField="SubmitDate" DataFormatString="{0:dd/MM/yyyy}"
                            HtmlEncode="false"  HeaderStyle-Font-Bold="false">
                        <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                        <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Fault Details" HeaderStyle-Font-Bold="false" >
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label> >
                                <span class='highlight'>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                </span>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="350px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Appointment" HeaderStyle-Font-Bold="false" >
                            <ItemTemplate>
                                <asp:Label ID="lblAppointmentDateCurrent" runat="server" Text='<%# HighlightText(Eval("AppointmentDate"))%>'></asp:Label>                                                             
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>

                       <%-- <asp:BoundField HeaderText="Appointment" ItemStyle-Width="150px" 
                            DataField="AppointmentDate" 
                            HtmlEncode="false" HeaderStyle-Font-Bold="false" >
                        <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                        <ItemStyle Width="150px" />
                        </asp:BoundField>--%>
                        <asp:BoundField HeaderText="Status" DataField="FaultStatus" 
                            ItemStyle-Width="200px" HeaderStyle-Font-Bold="false" >
                        <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                        <ItemStyle Width="200px"  />
                        </asp:BoundField>
                        <asp:TemplateField ShowHeader="False" HeaderStyle-Font-Bold="false">
                            <ItemTemplate>
                                <asp:Button ID="btnViewCurrentFaults" runat="server" CausesValidation="False" 
                                    CommandArgument='<%# Eval("AppointmentId") %>' CommandName="view" 
                                    onclick="btnViewCurrentFaults_Click" Text="Manage" Width="100px" />
                                &nbsp;<asp:Label ID="lblAppointmentStatus" runat="server" 
                                    Text='<%# Eval("FaultStatus") %>' Visible="False"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="100px"  HorizontalAlign="right"/>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle ForeColor="Red" />
                    <HeaderStyle CssClass="gridfaults-header" VerticalAlign="Top" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <br />
    <hr style="border: 1px double #808080" />

    <br />
    <br />

    <b>
        Is this a recall?
    </b> (Repairs completed within the past 3 months)

    <div class="fl_width_100_text_a_l grid_height">
        <asp:UpdatePanel ID="updRecallFault" runat="server">
            <ContentTemplate>
                <asp:GridView ID="grdRecall" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                    CssClass="gridfaults" Width="100%" GridLines="None" 
                    DataKeyNames="FaultLogID" OnRowCommand="grdFaults_RowCommand" 
                    EmptyDataText="No recall exist!" EmptyDataRowStyle-ForeColor="Red" 
                    CellSpacing="5">
                    <Columns>
                        <asp:BoundField HeaderText="Recorded" ItemStyle-Width="100px"  DataField="SubmitDate"
                            HeaderStyle-Font-Bold="false"  HtmlEncode="false" />
                        <asp:TemplateField HeaderText="Fault Details" HeaderStyle-Font-Bold="false">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ElementName") %>'></asp:Label>
                                >
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("FaultDetail") %>' Font-Bold="true"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="350px" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Appointments" HeaderStyle-Font-Bold="false" >
                            <ItemTemplate>
                                <asp:Label ID="lblAppointmentDateRecall" runat="server" Text='<%# HighlightText(Eval("AppointmentDate"))%>'></asp:Label>                                                            
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>

                     <%--   <asp:BoundField HeaderText="Appointments"  ItemStyle-Width="150px"  DataField="AppointmentDate"
                            HtmlEncode="false" HeaderStyle-Font-Bold="false" />--%>
                        <asp:BoundField HeaderText="Status" DataField="FaultStatus" ItemStyle-Width="200px" HeaderStyle-Font-Bold="false" />
                        <asp:ButtonField ButtonType="Button" CommandName="view" Text="View &gt;"  ControlStyle-Width="100px" HeaderStyle-Font-Bold="false" ItemStyle-HorizontalAlign="Right" />
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <br />
   <hr style="border: 1px double #808080" />
   <br />
   <br />
    <div class="fl_width_100_text_a_l" style="font-weight: bold;">
        Gas servicing appointments:
    </div>
    <div class="fl_width_100_text_a_l grid_height">
        <asp:UpdatePanel ID="updGassServiceAppointments" runat="server">
            <ContentTemplate>
                <asp:GridView ID="grdGasServicingAppointments" runat="server" AutoGenerateColumns="False"
                    Width="100%" ShowHeader="False" GridLines="None" EmptyDataText="No gas appointment exists!"
                    DataKeyNames="APPOINTMENTID" CssClass="gassServiceAppointment" 
                    EmptyDataRowStyle-ForeColor="Red" CellSpacing="5" >
                    <Columns>
                    <asp:TemplateField>
                    <ItemTemplate>Annual Gas Service</ItemTemplate>
                    <ItemStyle Width="450px" />
                    </asp:TemplateField>
                       
                        
                        
                        <asp:TemplateField HeaderText="" HeaderStyle-Font-Bold="false" >
                            <ItemTemplate>
                                <asp:Label ID="lblAppointmentDateGas" runat="server" Text='<%# HighlightText(Eval("AppointmentDate"))%>'></asp:Label>                                                            
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="150px" />
                        </asp:TemplateField>

                        
                                      
                       <%-- <asp:BoundField DataField="AppointmentDate"  
                            HtmlEncode="false" >
                        <ItemStyle Width="10%" />
                        </asp:BoundField>--%>
                        <asp:BoundField HeaderText="Status" DataField="Status" >
                        <ItemStyle HorizontalAlign="Center"  Width="200px"/>
                        </asp:BoundField>

                        <asp:TemplateField HeaderText="" HeaderStyle-Font-Bold="false" >
                            <ItemTemplate>
                                <asp:Label ID="lblOperative" runat="server" Text='<%# Bind("Operative") %>'></asp:Label>                                                            
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="200px" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False" HeaderStyle-Font-Bold="false">
                            <ItemTemplate>
                                <asp:Button ID="btnViewCurrentFaultsForGas" runat="server" CausesValidation="False" 
                                    CommandArgument='<%# Eval("APPOINTMENTID") %>' CommandName="view" 
                                    onclick="btnViewCurrentGasAppointment_Click" Text="Manage" Width="100px" />
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="False" HorizontalAlign="Left" />
                            <ItemStyle Width="100px"  HorizontalAlign="right"/>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <hr />
    <div class="fl_width_100_text_a_l" style="font-weight: bold;">
        Planned maintainance contracts/appointments (within current year )        
    </div>

     <div class="fl_width_100_text_a_l grid_height" >
        <asp:UpdatePanel ID="udpPlannedAppointments" runat="server">
            <ContentTemplate>
                <asp:GridView ID="grdPlannedAppointments" runat="server" AutoGenerateColumns="False" 
                    CssClass="gridfaults" Width="100%" GridLines="None"    
                    HeaderStyle-VerticalAlign="Top" HeaderStyle-CssClass="gridfaults-header"            
                    EmptyDataText="No appointment exist!" EmptyDataRowStyle-ForeColor="Red" 
                    CellSpacing="5">
                    <Columns>
                        <asp:BoundField HeaderText="PMO:" DataField="PMO" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="JSN:" DataField="JSN" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="Component:" DataField="Component" ItemStyle-Width="25%" HeaderStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="Trade:" DataField="Trade" ItemStyle-Width="15%" HeaderStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="Date:" DataField="AppointmentDate" ItemStyle-Width="30%" HeaderStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="Time:" DataField="AppointmentTime" ItemStyle-Width="10%" HeaderStyle-Font-Bold="true" />
                    </Columns>
                    <HeaderStyle CssClass="gridfaults-header" VerticalAlign="Top" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <%--<div class="fl_width_100_text_a_l grid_height">
        <asp:GridView ID="grdContractAppointments" runat="server" AutoGenerateColumns="False"
            Width="100%" ShowHeader="false" EmptyDataText="0 Planned maintanance appointment found"
            GridLines="None" >
            <Columns>
                <asp:BoundField HeaderText="APPOINTMENTID" DataField="APPOINTMENTID" />
                <asp:BoundField DataField="AppointmentDate" DataFormatString="{0:MMMM d, yyyy}" HtmlEncode="false" />
                <asp:BoundField HeaderText="Status" DataField="Status" />
            </Columns>
        </asp:GridView>
    </div>--%>
    <div style="float:right">
        <asp:Button ID="btnBack" Text="< Back" runat="server" Enabled="True"/> &nbsp &nbsp
        <asp:Button ID="btnContinue" Text="Report a New Fault" runat="server" Enabled="True"/>
    </div>
     <asp:Panel ID="pnlWarranties" runat="server" CssClass="modalPopup" Style="width: 50%;
        display: none; padding: 0px !important;">
        <div style="width: 100%; text-align: left; clear: both; overflow: auto; max-height: 620px;">
            <warranties:Warranties ID="ucWarranties" runat="server"></warranties:Warranties>
        </div>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <asp:Label ID="lblFaultIdHidden" runat="server" Text="" Style="display: none;" ClientIDMode="Static" />
    <cc1:ModalPopupExtender ID="popupWarranties" runat="server" PopupControlID="pnlWarranties"
        TargetControlID="lblFaultIdHidden" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
</asp:Content>
