﻿Imports FLS_BusinessObject
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports System.Net
Imports System.IO

Public Class CancellationAppointmentSummaryForGas
    Inherits PageBase

#Region "Properties "

    Dim objCustomerBL As CustomerBL = New CustomerBL()
    Dim objFaultBl As FaultBL = New FaultBL()
    Dim rearrange As String



#End Region

#Region "Events"

#Region "Page Pre Render"
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Try
            'btnRescheduleAppointment.PostBackUrl = PathConstants.RearrangCurrentFault + GeneralHelper.getSrc(PathConstants.CancelAppointmentSummaryForGasSrcVal)
            Me.setBackButtonPostBackUrl(Me.btnBack, "", GeneralHelper.getSrc(PathConstants.CancelAppointmentSummarySrcVal))

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                'createData()
                prePopulatedValues()
                populateAppointmentSummary()
                populateAppointmentDetails()
                mdlPopupFaultNotes.Hide()

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Button Update Customer Detail Click Event"

    Protected Sub btnUpdateCustomerDetail_Click(sender As Object, e As EventArgs) Handles btnUpdateCustomerDetail.Click
        Try
            updateAddress()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

    '#Region "Link Button Next Fault Click Event"

    '    Protected Sub lnkBtnNextFault_Click(sender As Object, e As EventArgs) Handles lnkBtnNextFault.Click
    '        Try
    '            scrollFaults()
    '        Catch ex As Exception
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.message = ex.Message

    '            If uiMessageHelper.IsExceptionLogged = False Then
    '                ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            End If

    '        Finally
    '            If uiMessageHelper.IsError = True Then
    '                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
    '            End If
    '        End Try

    '    End Sub

    '#End Region

#Region "Button Cancel Reported Faults Click Event"

    Protected Sub btnCancelReportedFaults_Click(sender As Object, e As EventArgs) Handles btnCancelReportedFaults.Click
        Try
            mdlPopUpCancelFaults.Show()
            If txtBoxDescription.Enabled = False Then
                uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.CancellationError, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Button Save Click Event"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            cancelFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Link Button Click Here Click Event"

    Protected Sub lnkBtnClickHere_Click(sender As Object, e As EventArgs) Handles lnkBtnClickHere.Click
        'Commented out the code as window will be closed by Java Script.
        'Dim customerId As Integer = SessionManager.getCustomerId()

        'If customerId <> 0 Then
        '    Response.Redirect(PathConstants.CustomerRecords + customerId.ToString())
        'Else
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        'End If

    End Sub

#End Region

    '#Region "Record No Entry Button Event Handler"

    '    Protected Sub btnNoEntry_Click(sender As Object, e As EventArgs) Handles btnNoEntry.Click
    '        Response.Redirect(PathConstants.OtherAppointmentPath + GeneralHelper.getSrc(PathConstants.CancelAppointmentSummarySrcVal))
    '    End Sub

    '#End Region

#End Region

#Region "Functions"

#Region "Populate Values"

    Sub prePopulatedValues()

        Dim customerData As CustomerBO = New CustomerBO()
        customerData = SessionManager.getCustomerData()
        ' The below data is hardcoded as there is currently no data in Session **** Abdul Wahhab 14/02/2013 ****
        'customerData.CustomerId = 1
        'customerData.Name = "Emma Howard"
        'customerData.Street = "54B High Street"
        'customerData.Address = "Dereham NR1 2DD"
        'customerData.Area = "Norfolk"
        'customerData.Telephone = "01603 1234455"
        'customerData.Mobile = "07788131212"
        'customerData.Email = "emma.h@hotmail.com"

        'SessionManager.setCustomerData(customerData)
        ' These code lines will need to be removed **** Abdul Wahhab 14/02/2013 ****

        If customerData.CustomerId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidCustomerData, True)
        Else
            ' Customer Labels
            lblCustomerName.Text = customerData.Name
            lblCustomerStreet.Text = customerData.Street
            lblCustomerAddress.Text = customerData.PostCode
            lblCustomerArea.Text = customerData.Area
            lblCustomerTelephone.Text = customerData.Telephone
            lblCustomerMobile.Text = customerData.Mobile
            lblCustomerEmail.Text = customerData.Email
        End If
    End Sub

#End Region

#Region "Update Customer Address"

    Sub updateAddress()


        If btnUpdateCustomerDetail.Text = "Update Customer Detail" Then

            txtCustomerTelephone.Text = lblCustomerTelephone.Text
            lblCustomerTelephone.Visible = False
            txtCustomerTelephone.Visible = True

            txtCustomerMobile.Text = lblCustomerMobile.Text
            lblCustomerMobile.Visible = False
            txtCustomerMobile.Visible = True

            txtCustomerEmail.Text = lblCustomerEmail.Text
            lblCustomerEmail.Visible = False
            txtCustomerEmail.Visible = True

            btnUpdateCustomerDetail.Text = "Save Changes"

        Else

            If lblCustomerTelephone.Text <> txtCustomerTelephone.Text Or lblCustomerMobile.Text <> txtCustomerMobile.Text Or lblCustomerEmail.Text <> txtCustomerEmail.Text Then

                Dim telephone As String = txtCustomerTelephone.Text
                Dim mobile As String = txtCustomerMobile.Text
                Dim email As String = txtCustomerEmail.Text

                telephone = telephone.Trim()
                mobile = mobile.Trim()
                email = email.Trim()

                telephone = telephone.Replace(" ", "")
                mobile = mobile.Replace(" ", "")

                Dim phoneRegex As Regex = New Regex(RegularExpConstants.numbersExp)
                Dim emailRegex As Regex = New Regex(RegularExpConstants.emailExp)

                Dim phoneMatch As Match = phoneRegex.Match(telephone)
                Dim mobileMatch As Match = phoneRegex.Match(mobile)
                Dim emailMatch As Match = emailRegex.Match(email)

                Dim isError As Boolean = False

                If Not phoneMatch.Success And telephone <> "" Then
                    isError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidTelephone, True)
                ElseIf Not mobileMatch.Success And mobile <> "" Then
                    isError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidMobile, True)
                ElseIf Not emailMatch.Success And email <> "" Then
                    isError = True
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidEmail, True)
                End If

                If Not isError Then

                    Dim customerData As CustomerBO = New CustomerBO()
                    customerData = SessionManager.getCustomerData()

                    If customerData.CustomerId = 0 Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
                    End If

                    Dim objCustomerBO As CustomerBO = New CustomerBO()
                    objCustomerBO.CustomerId = customerData.CustomerId
                    objCustomerBO.Telephone = txtCustomerTelephone.Text
                    objCustomerBO.Mobile = txtCustomerMobile.Text
                    objCustomerBO.Email = txtCustomerEmail.Text

                    If IsDBNull(objCustomerBL.updateAddress(objCustomerBO)) Then

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ErrorUserUpdate, True)

                    Else

                        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserSavedSuccessfuly, False)

                        lblCustomerTelephone.Text = txtCustomerTelephone.Text
                        lblCustomerMobile.Text = txtCustomerMobile.Text
                        lblCustomerEmail.Text = txtCustomerEmail.Text

                        customerData.Telephone = txtCustomerTelephone.Text
                        customerData.Mobile = txtCustomerMobile.Text
                        customerData.Email = txtCustomerEmail.Text

                        SessionManager.setCustomerData(customerData)

                        custDetails.setCustomerLabels()

                        txtCustomerTelephone.Visible = False
                        lblCustomerTelephone.Visible = True

                        txtCustomerMobile.Visible = False
                        lblCustomerMobile.Visible = True

                        txtCustomerEmail.Visible = False
                        lblCustomerEmail.Visible = True

                        btnUpdateCustomerDetail.Text = "Update Customer Detail"

                    End If

                End If

            Else

                txtCustomerTelephone.Visible = False
                lblCustomerTelephone.Visible = True

                txtCustomerMobile.Visible = False
                lblCustomerMobile.Visible = True

                txtCustomerEmail.Visible = False
                lblCustomerEmail.Visible = True

                btnUpdateCustomerDetail.Text = "Update Customer Detail"

            End If

        End If
    End Sub

#End Region

#Region "Populate Appointment Summary"

    Sub populateAppointmentSummary()
        Dim faultsDataTable As DataTable = New DataTable()
        faultsDataTable = SessionManager.getConfirmedGasAppointment()

        If IsNothing(faultsDataTable) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else
            grdAppointments.DataSource = faultsDataTable
            grdAppointments.DataBind()

            Dim slideIndex As Integer = 0
            ViewState.Add(ViewStateConstants.SlideIndex, 1)
            Dim slideDataSet As DataTable = New DataTable()
            slideDataSet = faultsDataTable

            changeFaultValues(slideDataSet, slideIndex)

            If slideDataSet.Rows.Count = 1 Then
                'lnkBtnNextFault.Enabled = False
            End If
        End If


    End Sub

#End Region

#Region "Populate Appointment Details"

    Sub populateAppointmentDetails()

        Dim appointmentId As Integer = Request.QueryString.Get(PathConstants.AppointmentId)

        If appointmentId = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AppointmentIdNotProvided, True)
        Else
            Dim resultDataSet As DataSet = New DataSet()

            objFaultBl.getGasAppointmentData(resultDataSet, appointmentId)

            If resultDataSet.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoAppointmentRecord, True)
            Else
                setAppointmentDataViewState(resultDataSet)
                Dim appointmentDataSet = resultDataSet.Tables(0)

                Dim durationTable As DataTable = New DataTable()
                durationTable = SessionManager.getConfirmedGasAppointment()

                Dim length As Integer = 0

                If Not IsNothing(durationTable) Then
                    length = durationTable.Rows.Count
                End If

                Dim duration As Double = 0

                For i As Integer = 0 To length - 1
                    Dim dur As Double = 0
                    Double.TryParse(durationTable.Rows(i).Item("Duration").ToString, dur)
                    duration = duration + dur
                Next

                lblOperative.Text = appointmentDataSet.Rows(0).Item("operative").ToString()
                If duration < 1 Or duration = 1 Then
                    lblDuration.Text = duration.ToString() + " Hour Approx"
                Else
                    lblDuration.Text = duration.ToString() + " Hours Approx"
                End If


                lblStartTime.Text = appointmentDataSet.Rows(0).Item("startTime").ToString()
                Dim appointmentStartDate As DateTime = appointmentDataSet.Rows(0).Item("appointmentDate").ToString
                Dim appointmentEndDate As DateTime = appointmentDataSet.Rows(0).Item("appointmentDate").ToString()
                lblStartDate.Text = Format(appointmentStartDate, ApplicationConstants.CustomLongDateFormat)
                lblEndDate.Text = Format(appointmentEndDate, ApplicationConstants.CustomLongDateFormat)
                lblAccessNote.Text = appointmentDataSet.Rows(0).Item("Notes").ToString()


                If Not IsDBNull(appointmentDataSet.Rows(0).Item("endTime")) Then
                    lblEndTime.Text = appointmentDataSet.Rows(0).Item("endTime")
                Else
                    'In case End time is not available for old appointments.
                    Dim EndTime As TimeSpan
                    Dim sTimeFrom As TimeSpan
                    TimeSpan.TryParse(lblStartTime.Text, sTimeFrom)
                    Dim result As TimeSpan = TimeSpan.FromHours(duration)

                    EndTime = sTimeFrom.Add(result)

                    lblEndTime.Text = EndTime.Hours.ToString() + ":" + EndTime.Minutes.ToString("00")
                End If
            End If
        End If
    End Sub

#End Region

#Region "Scroll Faults"

    Sub scrollFaults()

        Dim slideDataSet As DataTable = New DataTable()
        slideDataSet = SessionManager.getConfirmedFaults

        If IsNothing(slideDataSet) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

        If slideDataSet.Rows.Count <> 0 Then

            Dim slideIndex As Integer = ViewState.Item(ViewStateConstants.SlideIndex)

            changeFaultValues(slideDataSet, slideIndex)

            slideIndex = slideIndex + 1

            If slideDataSet.Rows.Count = slideIndex Then
                'lnkBtnNextFault.Enabled = False
            Else
                ViewState.Item(ViewStateConstants.SlideIndex) = slideIndex
            End If

        End If

    End Sub

#End Region

#Region "Cancel Faults"

    Sub cancelFaults()
        If txtBoxDescription.Text = "" Then
            mdlPopUpCancelFaults.Show()
            uiMessageHelper.setMessage(lblErrorMessage, pnlErrorMessage, UserMessageConstants.EnterReason, True)
        Else
            Dim resultDataSet As DataTable = New DataTable()
            resultDataSet = SessionManager.getConfirmedGasAppointment()

            If IsNothing(resultDataSet) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            Else
                mdlPopUpCancelFaults.Show()
                pnlErrorMessage.Visible = False
                pnlCancelMessage.Visible = True
                pnlCustomerMessage.Visible = True
                pnlSaveButton.Visible = False
                txtBoxDescription.Enabled = False

                Dim length As Integer = resultDataSet.Rows.Count

                Dim faultsList As StringBuilder = New StringBuilder()

                For i As Integer = 0 To length - 1
                    faultsList.Append(resultDataSet.Rows(i).Item("JOURNALID").ToString() + ",")
                Next

                faultsList.Remove(faultsList.ToString().Length - 1, 1)

                Dim objAppointmentCancellationBO As New AppointmentCancellationBO()

                objAppointmentCancellationBO.FaultsList = faultsList.ToString()
                objAppointmentCancellationBO.Notes = txtBoxDescription.Text
                objAppointmentCancellationBO.userId = SessionManager.getFaultSchedulingUserId()




                If IsDBNull(objFaultBl.saveGasServicingCancellation(objAppointmentCancellationBO)) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FaultNotCancelled, True)
                    pnlErrorMessage.Visible = False
                    pnlCancelMessage.Visible = False
                    pnlCustomerMessage.Visible = False
                    pnlSaveButton.Visible = True
                    txtBoxDescription.Text = ""
                    txtBoxDescription.Enabled = True
                    mdlPopUpCancelFaults.Hide()
                Else
                    pushAppointmnetCancellation()
                    pnlMessage.Visible = False
                End If

            End If

        End If

    End Sub

#End Region

#Region "Change Fault Values"

    Sub changeFaultValues(ByRef slideDataSet As DataTable, ByRef slideIndex As Integer)

        'lblJSN.Text = slideDataSet.Rows(slideIndex).Item("JobSheetNumber").ToString()

        'If IsDBNull(slideDataSet.Rows(slideIndex).Item("ProblemDays")) Then
        '    lblProblemDays.Text = "NA"
        'Else
        '    If slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() = 1 Then
        '        lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " day"
        '    Else
        '        lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item("ProblemDays").ToString() + " days"
        '    End If
        'End If

        'If IsDBNull(slideDataSet.Rows(slideIndex).Item("RecuringProblem")) Then
        '    lblReccuringProblem.Text = "NA"
        'Else
        '    If slideDataSet.Rows(slideIndex).Item("RecuringProblem").ToString() = True Then
        '        lblReccuringProblem.Text = "This is a recurring problem"
        '    Else
        '        lblReccuringProblem.Text = "This is not a recurring problem"
        '    End If
        'End If

        'If IsDBNull(slideDataSet.Rows(slideIndex).Item("CommunalProblem")) Then
        '    lblCommunalProblem.Text = "NA"
        'Else
        '    If slideDataSet.Rows(slideIndex).Item("CommunalProblem").ToString() = True Then
        '        lblCommunalProblem.Text = "This is a communal problem"
        '    Else
        '        lblCommunalProblem.Text = "This is not a communal problem"
        '    End If
        'End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("Notes")) Then
            lblFaultNotes.Text = ""
        Else
            lblFaultNotes.Text = slideDataSet.Rows(slideIndex).Item("Notes").ToString()
        End If

        If IsDBNull(slideDataSet.Rows(slideIndex).Item("STATUS")) Then
            btnCancelReportedFaults.Visible = True
        ElseIf (slideDataSet.Rows(slideIndex).Item("STATUS").ToString() = ApplicationConstants.CancelledStatus Or slideDataSet.Rows(slideIndex).Item("STATUS").ToString() = ApplicationConstants.AssignedtoContractor) Then
            btnCancelReportedFaults.Visible = False
        End If

    End Sub

#End Region

#Region "Button Update Click"
    ''' <summary>
    ''' Button Update Click.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateClick(ByVal sender As Object, ByVal e As EventArgs)
        txtFaultNotes.Text = lblFaultNotes.Text
        mdlPopupFaultNotes.Show()

    End Sub

#End Region


#Region "Button Back Fault Click"
    ''' <summary>
    ''' Button Back Fault Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFaultBack(ByVal sender As Object, ByVal e As EventArgs)
        pnlFaultNotesErrorMessage.Visible = False
        mdlPopupFaultNotes.Hide()

    End Sub

#End Region


#Region "Button Save Fault Click"
    ''' <summary>
    ''' Button Save Fault Click.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnFaultSave(ByVal sender As Object, ByVal e As EventArgs)
        If txtFaultNotes.Text.Length >= 500 Then
            lblFaultNotesErrorMessage.Text = UserMessageConstants.NoteLength
            pnlFaultNotesErrorMessage.Visible = True
            mdlPopupFaultNotes.Show()
        Else
            Dim appointmentId As Integer = Request.QueryString.Get(PathConstants.AppointmentId)
            Dim updated As Integer
            updated = objFaultBl.updateGasNotes(txtFaultNotes.Text, appointmentId)
            If updated = 1 Then
                lblFaultNotes.Text = txtFaultNotes.Text
                pnlFaultNotesErrorMessage.Visible = False
                mdlPopupFaultNotes.Hide()
            Else
                lblFaultNotesErrorMessage.Text = UserMessageConstants.AddFaultFailure
                pnlFaultNotesErrorMessage.Visible = True
                mdlPopupFaultNotes.Show()
            End If
        End If

    End Sub

#End Region


#Region "Create Data"

    Sub createData()

        Dim resultDataSet As DataSet = New DataSet()

        Dim resultTable As DataTable = New DataTable()
        resultTable.Columns.Add("FaultLogID")
        resultTable.Columns.Add("FaultId")
        resultTable.Columns.Add("JobSheetNumber")
        resultTable.Columns.Add("AreaName")
        resultTable.Columns.Add("Description")
        resultTable.Columns.Add("Duration")
        resultTable.Columns.Add("Trade")
        resultTable.Columns.Add("ProblemDays")
        resultTable.Columns.Add("RecuringProblem")
        resultTable.Columns.Add("CommunalProblem")
        resultTable.Columns.Add("DueDate")
        resultTable.Columns.Add("Notes")

        resultDataSet.Tables.Add(resultTable)

        Dim dataRow As DataRow = resultDataSet.Tables(0).NewRow()
        dataRow.ItemArray = {112, 870, "JS112", "Living Room", "The radiator is leaking", 3, "Carpenter", 1, 0, 1, "10 May", "major radiator leak in number 45. Due to a mobility scooter crashing into radiator. water gushing out. no electrics wet at the moment. please pass js"}
        resultDataSet.Tables(0).Rows.Add(dataRow)

        Dim dataRow1 As DataRow = resultDataSet.Tables(0).NewRow()
        dataRow1.ItemArray = {179, 870, "JS179", "Living Room", "The radiator is leaking", 3, "Carpenter", 2, 0, 0, "12 May", "concerns that the pressure on boiler dropping due to the leak on radiator."}
        resultDataSet.Tables(0).Rows.Add(dataRow1)

        Dim dataRow2 As DataRow = resultDataSet.Tables(0).NewRow()
        dataRow2.ItemArray = {194, 870, "JS194", "Living Room", "The radiator is leaking", 3, "Carpenter", 3, 0, 0, "13 May", "please note this radiator is brand new and was only fitted last week and it is now leaking. pls contact on 01493 664337. hk"}
        resultDataSet.Tables(0).Rows.Add(dataRow2)

        SessionManager.setConfirmedFaults(resultDataSet.Tables(0))

    End Sub

#End Region

#Region "Push Appointment Cancellation Push Notification"

    Public Function pushAppointmnetCancellation() As Boolean
        Dim status As Boolean = True

        Dim appointmentDataSet = getAppointmentDataViewState()

        Dim appointmentId As Integer = Request.QueryString.Get(PathConstants.AppointmentId)
        Dim type As Integer = ApplicationConstants.CancelAppointmentType

        If Not IsNothing(appointmentDataSet) Then
            Dim appointmentStartDate As DateTime = appointmentDataSet.Tables(0).Rows(0).Item("AppointmentDate").ToString
            ' Push notification is only sent when an appointment is cancelled with today(current date) as start date.
            If appointmentStartDate.Date = Today.Date Then
                status = pushAppointmentConfirmed(appointmentId, type)
            End If
        End If
        ' Status may be only false when there is an issue in StockCondidtionOfflineAPI
        Return status
    End Function

    Public Function pushAppointmentConfirmed(ByVal appointmentId As Integer, ByVal type As Integer) As Boolean
        Dim URL As String = String.Format("{0}?appointmentId={1}&type={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentId, type)
        Dim request As WebRequest = Net.WebRequest.Create(URL)
        request.Credentials = CredentialCache.DefaultCredentials
        Dim response As WebResponse = request.GetResponse()
        Dim dataStream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        response.Close()

        If (responseFromServer.ToString.Equals("true")) Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "View State Function"

#Region "Appointment Data View State Get, Set and Remove Method/Function(s)"

    Protected Sub setAppointmentDataViewState(ByVal EmailSend As DataSet)
        ViewState(ViewStateConstants.AppointmentData) = EmailSend
    End Sub

    Protected Function getAppointmentDataViewState() As DataSet
        Return CType(ViewState(ViewStateConstants.AppointmentData), DataSet)
    End Function

    Protected Sub removeAppointmentDataViewState()
        ViewState.Remove(ViewStateConstants.AppointmentData)
    End Sub

#End Region

#End Region

#End Region

End Class