﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="AppointmentSummary.aspx.vb" Inherits="FaultScheduling.AppointmentSummary" %>

<%@ Register TagPrefix="custom" TagName="CustomerDetails" Src="~/Controls/Common/CustomerDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="../../Scripts/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">   

 
<script type="text/javascript">

    function sendSMS(mobile, body, smsurl) {
        
        $.ajax({
            type: "GET",
            url: smsurl,
            data: "mobile=" + mobile + "&message=" + body + "",
            dataType: 'json',
            success: function (data, textStatus, xhr) {
            },
            error: function(xhr, error){
            },
        });
    }
</script>
    <div class="fl_width_100_text_a_l">
        <asp:UpdatePanel ID="updPanelFaults" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <div class="fl_width_100_text_a_l">
                    <asp:Panel ID="pnlCustomerDetails" runat="server">
                        <custom:CustomerDetails ID="custDetails" runat="server" StartString="Appointment summary for : ">
                        </custom:CustomerDetails>
                    </asp:Panel>
                    <div style="height: 10px;">
                    </div>
                    <hr />
                    <div style="height: 10px;">
                    </div>
                    <div style="height: 50px; overflow-y: auto; border: 1px solid #ADADAD; padding: 5px;">
                        <asp:GridView ID="grdAppointments" runat="server" AutoGenerateColumns="False" Width="100%"
                            CssClass="gridfaults" GridLines="None" DataKeyNames="FaultID" ShowHeader="False">
                            <Columns>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                                        >
                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>' Font-Bold="True"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDurationText" runat="server" Text="Duration:"></asp:Label>
                                        <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                                        <asp:Label ID="lblHoursText" runat="server" Text="Hrs"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTrade" runat="server" Text='<%# Bind("TradeName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDueText" runat="server" Text="Due:"></asp:Label>
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%# Bind("CompleteDueDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" HorizontalAlign="Left" Width="120px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="fl_width_100_text_a_l bottom_line">
        <div class="fl_heading">
            Appointment:
        </div>
        <div class="fl_width_100_text_a_l">
            <asp:UpdatePanel ID="updFaultViewer" runat="server">
                <ContentTemplate>
                    <table id="operativeSummary">
                        <tr>
                            <td>
                                Operative:
                            </td>
                            <td>
                                <asp:Label ID="lblOperative" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Duration:
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date:
                            </td>
                            <td>
                                <asp:Label ID="lblStartTime" runat="server"></asp:Label>&nbsp;
                                <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                End Date:
                            </td>
                            <td>
                                <asp:Label ID="lblEndTime" runat="server"></asp:Label>&nbsp;
                                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Fault Notes:
                            </td>
                            <td>
                                <asp:Label ID="lblFaultNotes" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <div id="faultviewer" class="roundcorner">
                        <asp:UpdatePanel ID="updPanelFaultArea" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblAreaName" runat="server"></asp:Label>
                                &nbsp;&gt;
                                <asp:Label ID="lblDescription" runat="server" Font-Bold="True"></asp:Label>
                                <asp:LinkButton ID="lnkBtnNextFault" runat="server">Next Fault &gt;</asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="updPanelFaultInfo" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblProblemDays" runat="server"></asp:Label><br />
                                <asp:Label ID="lblReccuringProblem" runat="server"></asp:Label><br />
                                <asp:Label ID="lblCommunalProblem" runat="server"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="fl_width_100_text_a_l bottom_line">
        <div id="customerheading" style="float: left;">
            <b>Customer :</b>
        </div>
        <table id="customerSummary">
            <tr>
                <td>
                    <asp:Label ID="lblCustomerName" runat="server"></asp:Label>
                </td>
                <td>
                    <div class="fl_width_100_text_a_l">
                        <div class="fl_width_20pc_text_a_t">
                            Telephone:</div>
                        <div class="fl_width_80pc_text_a_t">
                            <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                            <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCustomerStreet" runat="server"></asp:Label>
                </td>
                <td>
                    <div class="fl_width_100_text_a_l">
                        <div class="fl_width_20pc_text_a_t">
                            Mobile:</div>
                        <div class="fl_width_80pc_text_a_t">
                            <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                            <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                            <asp:CheckBox ID="mobileCheckBox" Checked="true" runat="server"  />
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCustomerAddress" runat="server"></asp:Label>
                </td>
                <td>
                    <div class="fl_width_100_text_a_l">
                        <div class="fl_width_20pc_text_a_t">
                            Email:</div>
                        <div class="fl_width_80pc_text_a_t">
                            <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                            <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                            <asp:CheckBox ID="emailCheckBox" Checked="true" runat="server" />
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblCustomerArea" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <div class="text_a_r_padd__r_20px">
            <asp:Button ID="btnUpdateCustomerDetail" runat="server" Text="Update Customer Detail" />
        </div>
    </div>
    <div class="fl_width_100_text_a_l">
        <table>
            <tr>
                <td>
                    Appointment Notes:
                </td>
                <td>
                    <asp:TextBox ID="txtAppointmentNotes" runat="server" Height="52px" TextMode="MultiLine"
                        Width="267px" MaxLength="500"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div class="text_a_r_padd__r_20px">
            <asp:UpdatePanel runat="server" ID="updpnlButton">
                <ContentTemplate>
                    <asp:Button ID="btnBack" runat="server" Text="&lt; Back" />
                    <asp:Button ID="btnCancelReportedFaults" runat="server" Text="Cancel Reported Faults"
                        Enabled="false" Visible="False" />
                    <asp:Button ID="btnConfirmAppointment" runat="server" Text="Confirm Appointment" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- ModalPopupExtender -->
    <!-- POPUP Appointment Arranged (START) -->
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnHidden2" runat="server" Text="" Style="display: none;" />
            <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
            <ajaxToolkit:ModalPopupExtender ID="popupAppointmentConfirmed" runat="server" PopupControlID="pnlAppointmentArranged"
                TargetControlID="btnHidden2" CancelControlID="btnHidden3" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlAppointmentArranged" runat="server" CssClass="modalPopup" Style="height: 200px;
                width: 400px; overflow: auto;">
                <b>Appointment arranged: </b>
                <hr style="height: 2px;" />
                <br />
                <br />
                <br />
                <div style="height: auto; clear: both; text-align: center;">
                    Your appointment has been scheduled, and
                    <br />
                    <asp:Label ID="lblAptArrangedOperative" runat="server" Text="" Font-Bold="true"></asp:Label>
                    has received a notification alert on his iPhone.
                    <br />
                </div>
                <br />
                <br />
                <div style="width: 100%; text-align: left; clear: both;">
                    <div style="float: right;">
                        <asp:Button ID="btnContinue" runat="server" Text="Continue" CssClass="margin_right20" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--  POPUP Appointment Arranged (END) -->
    <!-- POPUP Confirm Appointment Detail (START) -->
    <asp:Button ID="btnHidden1" runat="server" Text="" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popupConfirmAppointmentDetail" runat="server"
        PopupControlID="pnlConfirmAppointmentDetail" TargetControlID="btnHidden1" CancelControlID="btnGoBack"
        BackgroundCssClass="modalBackground">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlConfirmAppointmentDetail" runat="server" CssClass="modalPopup"
        Style="height: 265px; width: 400px;">
        <asp:ImageButton ID="imgBtnCloseAppointmentDetail" runat="server" Style="position: absolute;
            top: -13px; right: -15px; width: 22px; height: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
            BorderWidth="0" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="height: auto; clear: both;">
                    <table id="tblConfirmAppointment" style="width: 400px; margin-top: 10px; text-align: left;">
                        <tr>
                            <td colspan="2">
                                <b>Confirm appointment details: </b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr style="border-width: 2px; height: 2px" />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div style="float: right;">
                                    <asp:LinkButton ID="lnkBtnNextPopupFault" runat="server">Next Fault &gt;</asp:LinkButton></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Operative:
                            </td>
                            <td>
                                <asp:Label ID="lblPopOperative" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Location:
                            </td>
                            <td>
                                <asp:Label ID="lblPopupAreaName" runat="server"></asp:Label>
                                &nbsp;&gt;
                                <asp:Label ID="lblPopupFaultDescription" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Duration:
                            </td>
                            <td>
                                <asp:Label ID="lblPopDuration" runat="server" Text=""></asp:Label>
                                &nbsp hours
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trade:
                            </td>
                            <td>
                                <asp:Label ID="lblPopTrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Due:
                            </td>
                            <td>
                                <asp:Label ID="lblPopDue" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Date:
                            </td>
                            <td>
                                <asp:Label ID="lblPopAppointmentStartDateTime" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                End Date:
                            </td>
                            <td>
                                <asp:Label ID="lblPopAppointmentEndDateTime" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div style="text-align: left; clear: both;">
                        <div style="float: right;">
                            <asp:Button ID="btnGoBack" runat="server" Text="Go Back" CssClass="margin_right20" />
                            <asp:Button ID="btnPopupConfirmAppointment" runat="server" Text="Confirm Appointment"
                                CssClass="margin_right20" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!-- POPUP Confirm Appointment Detail (END) -->
    <!-- ModalPopupExtender -->
</asp:Content>
