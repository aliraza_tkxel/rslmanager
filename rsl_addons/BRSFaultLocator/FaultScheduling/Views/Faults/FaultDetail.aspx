﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FaultDetail.aspx.vb" MasterPageFile="~/MasterPage/RDMasterPage.Master"
    Inherits="FaultScheduling.FaultDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitAutoCompl();

        });
        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoCompl();
        }

        function InitAutoCompl() {

            $('#<%= txtSearch.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "FaultDetail.aspx/getRepairSearchResult",
                        data: "{'searchText':'" + $('#<%= txtSearch.ClientID %>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('^')[0],
                                    val: item.split('^')[1]
                                }
                            }));

                        },
                        error: function (result) {

                        }
                    });
                },
                select: function (event, ui) {

                    document.getElementById('<%= hdnSelectedRepairId.ClientID %>').value = ui.item.val
                    document.getElementById('<%= hdnSelectedRepairDescription.ClientID %>').value = ui.item.label
                    document.getElementById('<%= txtSearch.ClientID %>').value = ui.item.label
                    __doPostBack("<%=btnHidden.UniqueID %>", "_Image");

                }
            });
        }
       

    </script>
    <style type="text/css">
        #mainContainer
        {
            width: 100%;
            height: 800px;
            border: 1px solid black;
            position: relative;
        }
        
        #rightContainer
        {
            float: left;
            width: 48%;
            padding-left: 20px;
        }
        
        #leftContainer
        {
            float: left;
            width: 48%;
            padding-left: 20px;
        }
        
        #header-title
        {
            width: 100%;
            height: 30px;
            background: black;
            margin-bottom: 10px;
            padding-top: 10px;
        }
        
        #main-footer
        {
            margin-left: 20px;
            margin-bottom: 20px;
            position: absolute;
            bottom: 0;
        }
        
        table
        {
            border-spacing: 10px;
        }
        
        #fault-recharge-detail table
        {
            border-spacing: 5px;
        }
        
        td
        {
            padding: 5px;
        }
        
        #fault-recharge-detail td
        {
            padding: 5px;
        }
        
        #fault-recharge-detail input[type="text"]
        {
            width: 150px;
        }
        
        input[type="text"]
        {
            border: 1px solid #A9A9A9;
        }
        
        select
        {
            padding: 5px;
        }
        
        .long-select
        {
            width: 80%;
        }
        
        .short-select
        {
            width: 162px;
        }
        
        input
        {
            padding: 5px;
        }
        
        button
        {
            padding: 5px;
        }
        
        .fault-header
        {
            margin-left: 20px;
            margin-bottom: 15px;
        }
        
        .error-message
        {
            margin-left: 20px;
        }
        
        .grid-summary-header
        {
            font-weight: normal;
            font-size: 13px;
            border-bottom: 1px solid #BBB;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelReportArea" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="mainContainer">
                <div id="header-title">
                    &nbsp &nbsp &nbsp
                    <asp:Label ID="lblTitle" runat="server" Text="Add Fault" Font-Bold="true" ForeColor="White"></asp:Label>
                </div>
                <asp:Panel ID="pnlMessage" runat="server" CssClass="error-message" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlSelectedFault" runat="server" Visible="false" CssClass="fault-header"
                    Width="50%">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td style="width: 20%">
                                </td>
                                <td style="width: 80%">
                                </td>
                            </tr>
                        </thead>
                        <tr>
                            <td>
                                <asp:Label ID="lblSelectedFaultTitle" runat="server" Text="Selected Fault:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblSelectedFault" runat="server" Text="The door frame is damaged"
                                    Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <div id="leftContainer">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td style="width: 20%">
                                </td>
                                <td style="width: 80%">
                                </td>
                            </tr>
                        </thead>
                        <tr>
                            <td>
                                Description:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescription" Width="80%" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Priority:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPriority" class="short-select" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Duration:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDuration" class="short-select" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Trade:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTrade" class="short-select" runat="server">
                                </asp:DropDownList>
                                &nbsp &nbsp
                                <asp:Button ID="btnAddTrade" runat="server" Text="Add" UseSubmitBehavior="false"
                                    OnClick="btnAddTrade_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <div style="height: 150px; overflow: auto; border: 1px solid #A9A9A9; width: 80%;">
                                    <asp:GridView ID="grdTrades" runat="server" AutoGenerateColumns="False" Width="100%"
                                        OnRowDataBound="grdTrades_RowDataBound" GridLines="None" ShowHeader="False" BorderStyle="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="FaultTradeID" ShowHeader="False" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFaultTradeId" runat="server" Text='<%# Bind("FaultTradeId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTradeName" runat="server" Text='<%# Bind("TradeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnRemoveTrade" runat="server" Text="Remove" OnClick="btnRemoveTrade_OnClick"
                                                        CommandArgument='<%# Bind("TradeId") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#E6E6E6"></RowStyle>
                                        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" style="padding-top: 24px;">
                                Recharge:
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxRecharge" runat="server" Visible="false" />
                                <table id="fault-recharge-detail" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 40%">
                                            </td> 
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            Net £:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNet" runat="server" AutoPostBack="true" OnTextChanged="txtNet_TextChanged"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkBoxSubContractor" runat="server" />
                                            Sub-contractor
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            VAT Rate:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlVatRate" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVatRate_SelectedIndexChanged"
                                                class="short-select">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkBoxFaultActive" runat="server" />
                                            Active
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            VAT £:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtVat" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkBoxGasSafe" runat="server" />
                                            Gas Safe
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gross £:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtGross" runat="server" ReadOnly="True"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkBoxOFTEC" runat="server" />
                                            OFTEC
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkTenantsOnline" runat="server" />
                                            Tenants Online
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="rightContainer">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <td style="width: 20%">
                                </td>
                                <td style="width: 80%">
                                </td>
                            </tr>
                        </thead>
                        <tr>
                            <td>
                                Repairs:
                            </td>
                            <td>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 85%;">
                                            <asp:TextBox ID="txtSearch" runat="server" Width="90%" CssClass="searchbox" AutoPostBack="false"
                                                AutoCompleteType="Search"></asp:TextBox>
                                            <ajaxToolkit:TextBoxWatermarkExtender ID="txtBoxRepair" runat="server" TargetControlID="txtSearch"
                                                WatermarkText="Search repair" WatermarkCssClass="searchbox searchText">
                                            </ajaxToolkit:TextBoxWatermarkExtender>
                                            <asp:HiddenField ID="hdnSelectedRepairId" runat="server" />
                                            <asp:HiddenField ID="hdnSelectedRepairDescription" runat="server" />
                                            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                                        </td>
                                        <td style="width: 15%;">
                                            <asp:Button ID="btnAddRepair" runat="server" Text="Add" OnClick="btnAddRepair_Click" style="float:right;"
                                                UseSubmitBehavior="false" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <div style="height: 200px; overflow: auto; border: 1px solid #A9A9A9; width: 100%">
                                    <asp:GridView ID="grdFaultRepairs" runat="server" AutoGenerateColumns="False" OnRowDataBound="grdFaultRepairs_RowDataBound"
                                        Width="100%" GridLines="None" ShowHeader="False" BorderStyle="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="FaultTradeID" ShowHeader="False" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFaultRepairId" runat="server" Text='<%# Bind("faultRepairId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trade Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRepair" runat="server" Text='<%# Bind("repair") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnRemoveRepair" runat="server" Text="Remove" OnClick="btnRemoveRepair_OnClick"
                                                        CommandArgument='<%# Bind("repairId") %>' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle BackColor="#E6E6E6"></RowStyle>
                                        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td style="text-align: right;">
                                <div style="width: 100%;">
                                    <asp:Button ID="btnSaveChanges" runat="server" Text="Save Changes" UseSubmitBehavior="false"
                                        OnClick="btnSaveChanges_OnClick" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                &nbsp
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="main-footer">
                    <asp:Button ID="btnBack" runat="server" Text=" < Back " />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
