﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/RDMasterPage.Master"
    CodeBehind="ReportsArea.aspx.vb" Inherits="FaultScheduling.ReportsArea" EnableEventValidation="false"   %>

<%@ Register Src="~/Controls/Reports/FollowOnListControl.ascx" TagName="FollowOnListControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Reports/NoEntryListControl.ascx" TagName="NoEntryListControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Reports/PausedFaultsListControl.ascx" TagName="PausedFaultsListControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/Controls/Reports/RecallListControl.ascx" TagName="RecallListControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/Controls/Reports/FaultManagementListControl.ascx" TagName="FaultManagementListControl"
    TagPrefix="uc5" %>
<%@ Register Src="~/Controls/Reports/SubcontractorListControl.ascx" TagName="SubcontractorListControl"
    TagPrefix="uc6" %>
<%@ Register Src="~/Controls/Reports/RepairDatabase.ascx" TagName="RepairDatabase"
    TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/GridViewCustomPager.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 152px;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
           width: 150px !important;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;               //timer identifier
        var doneTypingInterval = 1000; //time in ms, 5 second for example is 5000

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }

        function PrintJobSheet() {
            var str_win
            str_win = '../Faults/PrintSubContractorJobSheetSummaryUpdate.aspx?JobSheetNumber=' + document.getElementById("lblJSN").innerHTML
            window.open(str_win, "display", 'width=800,height=800, left=200,top=200, scrollbars=1');
            //javascript: window.open('../Faults/PrintSubContractorJobSheetSummaryUpdate.aspx?JobSheetNumber=' + document.getElementById("lblJSN").innerHTML);
            return false;
        }

        $(document).ready(function () {
            var ddlOperativeElement = document.getElementById("ContentPlaceHolder1_ddlOperative");
            if (ddlOperativeElement == null) {
                $("#divOperative").hide()
            }
        });
        
    </script>
    <script type="text/javascript">
        function ClientItemSelected(sender, e) {
            $get("<%=hdnSelectedRepairId.ClientID %>").value = e.get_value();
        }
    </script>
</asp:Content>
<asp:Content ID="leftsubMenu" ContentPlaceHolderID="leftSubMenu" runat="server">
    <div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="width: 100%; float: left;">
    </div>
    <asp:UpdatePanel ID="updPanelReportArea" runat="server" UpdateMode="Always" >
        <ContentTemplate>
            <div class="portlet">
                <div class="header pnlHeading">
                    <asp:Label ID="lblReportHeading" CssClass="header-label" Text="Follow On Works List" runat="server" Font-Bold="true" />
                    <div class="field right">
                        <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;" Visible="True">
                            <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                                class="searchbox styleselect-control searchbox right" onkeyup="TypingInterval();" PlaceHolder="Search User" runat="server">
                            </asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                            <asp:Button runat="server" ID="btnGo" UseSubmitBehavior="false" Text="GO" OnClick="btnGO_Click"
                                CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0; position: absolute; margin: 38px 0px 0 -51px;" />
                        </asp:Panel>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </asp:Panel>
                    <div class="form-control" id="divOperative">
                        <div class="select_div">
                            <div class="">
                                <asp:DropDownList ID="ddlOperative" runat="server" AutoPostBack="True" Visible="False">
                                    <asp:ListItem Value="-1">Select Operative</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div>
                        <uc1:FollowOnListControl ID="FollowOnListControl" runat="server" Visible="True" />
                        <uc2:NoEntryListControl ID="NoEntryListControl" runat="server" Visible="False" />
                        <uc3:PausedFaultsListControl ID="PausedFaultsListControl" runat="server" Visible="False" />
                        <uc4:RecallListControl ID="RecallListControl" runat="server" Visible="False" />
                        <uc5:FaultManagementListControl ID="FaultManagementListControl" runat="server" Visible="false" />
                        <uc6:SubcontractorListControl ID="SubcontractorListControl" runat="server" Visible="false" />
                        <uc7:RepairDatabase ID="RepairDatabase" runat="server" Visible="true" />
                        <div style="display: none;">
                            <asp:HiddenField ID="hdnSelectedRepairId" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
