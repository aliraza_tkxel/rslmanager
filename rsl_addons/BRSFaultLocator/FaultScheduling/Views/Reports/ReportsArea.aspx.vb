﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic

Public Class ReportsArea
    Inherits PageBase

#Region "Properties "
    Dim objReportsBl As ReportsBL = New ReportsBL()
    Private varBackgroundGrayColor As String = "#e6e6e6"
    Private varBackgroundWhiteColor As String = "#FFFFFF"
    Private varBackgroundProperty As String = "background-color"
    Private varPageUrl As String
    Private index As Integer = 0
#End Region

#Region "Events Handling"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                populateReports()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearch.TextChanged

        Try
            searchResults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Dropdown List Operative Selected Index Changed Event"

    Protected Sub ddlOperative_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlOperative.SelectedIndexChanged

        Try
            changeOperative()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Filtered Data by Scheme And Block"

    ''' <summary>
    ''' Filter Report on the basis of Scheme and Block id
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnGO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGo.Click _
        , FollowOnListControl.FiltersChanged _
        , NoEntryListControl.FiltersChanged _
        , PausedFaultsListControl.FiltersChanged _
        , RecallListControl.FiltersChanged _
        , SubcontractorListControl.FiltersChanged

        Try
            pnlMessage.Visible = False
            Dim resultDataSet As DataSet = New DataSet()
            If Request.QueryString(PathConstants.RepairsDashboard) = "fow" Then
                FollowOnListControl.populateFollowOnList(resultDataSet, txtSearch.Text, True)
            ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "ne" Then
                NoEntryListControl.populateNoEntryList(resultDataSet, txtSearch.Text, True)
            ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "rc" Then
                RecallListControl.populateRecallList(resultDataSet, txtSearch.Text, CInt(ddlOperative.SelectedItem.Value), True)
            ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "pf" Then
                PausedFaultsListControl.populatePausedFaultList(resultDataSet, txtSearch.Text, CInt(ddlOperative.SelectedItem.Value), True)
            ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "fm" Then
                FaultManagementListControl.PopulateSearchResults(txtSearch.Text.ToString())
            ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "rdl" Then
                RepairDatabase.populateRepairDatabaseList(resultDataSet, txtSearch.Text, True)
            ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "atc" Then
                SubcontractorListControl.Visible = True
                SubcontractorListControl.populateSubcontractorList(resultDataSet, txtSearch.Text, True)
            ElseIf String.IsNullOrEmpty(Request.QueryString(PathConstants.RepairsDashboard)) Then
                FollowOnListControl.populateFollowOnList(resultDataSet, txtSearch.Text, True)
            End If

            If resultDataSet.Tables.Count > 0 Then
                If resultDataSet.Tables(0).Rows.Count = 0 Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Region for populate reports"

#Region "Populate Follow On List"

    Sub populateFollowOnList()

        Dim resultDataSet As DataSet = New DataSet()

        lblReportHeading.Text = ApplicationConstants.FollowonWorksReport
        FollowOnListControl.Visible = True

        FollowOnListControl.populateFollowOnList(resultDataSet, txtSearch.Text, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.FollowOnDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Subcontractor List"

    Sub populateSubcontractorList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.ContractorReport
        SubcontractorListControl.Visible = True
        SubcontractorListControl.populateSubcontractorList(resultDataSet, search, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.SubcontractorDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate No Entry List"

    Sub populateNoEntryList()

        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.NoEntryReport
        NoEntryListControl.Visible = True
        NoEntryListControl.populateNoEntryList(resultDataSet, search, True)

        If IsNothing(resultDataSet.Tables(0)) AndAlso resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.NoEntryDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Recall List"

    Sub populateRecallList()

        Dim search As String = txtSearch.Text
        Dim operativeId As Integer = -1
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.RecallsReport
        RecallListControl.Visible = True
        ddlOperative.Visible = True
        RecallListControl.populateRecallList(resultDataSet, search, operativeId, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.RecallDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Paused Faults List"

    Sub populatePausedFaultsList()

        Dim search As String = txtSearch.Text
        Dim operativeId As Integer = -1
        Dim resultDataSet As DataSet = New DataSet()
        lblReportHeading.Text = ApplicationConstants.PausedFaultsReport

        PausedFaultsListControl.populatePausedFaultList(resultDataSet, search, operativeId, True)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If
        PausedFaultsListControl.Visible = True
        ViewState.Add(ViewStateConstants.PausedDataSet, resultDataSet)

    End Sub

#End Region

#Region "Populate Repair Database List"

    Public Sub populateRepairDatabaseList()
        Dim search As String = txtSearch.Text
        Dim resultDataSet As DataSet = New DataSet()
        RepairDatabase.populateRepairDatabaseList(resultDataSet, search, True)
        lblReportHeading.Text = ApplicationConstants.RepairDatabaseReport
        RepairDatabase.Visible = True

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        ViewState.Add(ViewStateConstants.RepairsResultDataSet, resultDataSet)

    End Sub

#End Region

#End Region

#Region "Search Results"

    Sub searchResults()
        index = ViewState("Index")
        Dim resultDataSet As DataSet = New DataSet()

        If index = 0 Then
            FollowOnListControl.populateFollowOnList(resultDataSet, txtSearch.Text, True)
        ElseIf index = 1 Then
            NoEntryListControl.populateNoEntryList(resultDataSet, txtSearch.Text, True)
        ElseIf index = 2 Then
            ddlOperative.SelectedIndex = 0
            RecallListControl.populateRecallList(resultDataSet, txtSearch.Text, CInt(ddlOperative.SelectedItem.Value), True)
        ElseIf index = 3 Then
            ddlOperative.SelectedIndex = 0
            PausedFaultsListControl.populatePausedFaultList(resultDataSet, txtSearch.Text, CInt(ddlOperative.SelectedItem.Value), True)
        ElseIf index = 4 Then
            FaultManagementListControl.PopulateSearchResults(txtSearch.Text.ToString())
        ElseIf index = 5 Then
            SubcontractorListControl.Visible = True
            SubcontractorListControl.populateSubcontractorList(resultDataSet, txtSearch.Text, True)
        ElseIf index = 9 Then
            RepairDatabase.Visible = True
            pnlMessage.Visible = False
            If txtSearch.Text = "" Then
                RepairDatabase.populateRepairDatabaseList(resultDataSet, txtSearch.Text, True)
            Else
                RepairDatabase.populateRepairDatabaseList(resultDataSet, txtSearch.Text)
            End If
        End If

        If resultDataSet.Tables.Count > 0 Then
            If resultDataSet.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        End If

    End Sub

#End Region

#Region "Change Report"

    Sub changeReport()

        'Hide all reports
        FollowOnListControl.Visible = False
        NoEntryListControl.Visible = False
        RecallListControl.Visible = False
        FaultManagementListControl.Visible = False
        PausedFaultsListControl.Visible = False
        SubcontractorListControl.Visible = False
        RepairDatabase.Visible = False

        pnlMessage.Visible = False
        txtSearch.Text = ""
        ddlOperative.SelectedIndex = 0
        ddlOperative.Visible = False

        If index = 0 Then
            If Not IsPostBack Then
                FollowOnListControl.loadFinancialYears()
            End If
            populateFollowOnList()
        ElseIf index = 1 Then
            If Not IsPostBack Then
                NoEntryListControl.loadFinancialYears()
            End If
            populateNoEntryList()
        ElseIf index = 2 Then
            If Not IsPostBack Then
                RecallListControl.loadFinancialYears()
                populateOperativeList()
            End If
            populateRecallList()
        ElseIf index = 3 Then
            If Not IsPostBack Then
                PausedFaultsListControl.loadFinancialYears()
            End If
            ddlOperative.Visible = True
            populatePausedFaultOperativeList()
            populatePausedFaultsList()
        ElseIf index = 4 Then
            lblReportHeading.Text = ApplicationConstants.FaultDatabaseReport
            FaultManagementListControl.PopulateSearchResults(txtSearch.Text.ToString())
            FaultManagementListControl.Visible = True
        ElseIf index = 5 Then
            If Not IsPostBack Then
                SubcontractorListControl.loadFinancialYears()
            End If
            populateSubcontractorList()
        ElseIf index = 6 Then
            Response.Redirect(PathConstants.PricingControlReport, True)
        ElseIf index = 7 Then
            Response.Redirect(PathConstants.RepairManagementReport, True)
        ElseIf index = 8 Then
            Response.Redirect(PathConstants.PrioritySettingsReport, True)
        ElseIf index = 9 Then
            populateRepairDatabaseList()
        End If
    End Sub

#End Region

#Region "Change Operative"

    Sub changeOperative()
        pnlMessage.Visible = False
        Dim resultDataSet As DataSet = New DataSet()
        index = ViewState("Index")
        Dim operativeId As Integer = CInt(ddlOperative.SelectedItem.Value)
        If index = 2 Then
            RecallListControl.populateRecallList(resultDataSet, txtSearch.Text, operativeId, True)
        ElseIf index = 3 Then
            PausedFaultsListControl.populatePausedFaultList(resultDataSet, txtSearch.Text, operativeId, True)
        End If

        If resultDataSet.Tables.Count > 0 Then
            If resultDataSet.Tables(0).Rows.Count = 0 Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        End If
    End Sub

#End Region

#Region "Populate Reports via Query string"

    Sub populateReports()

        If Request.QueryString(PathConstants.RepairsDashboard) = "ne" Then
            index = 1
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "fow" Then
            index = 0
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "rc" Then
            index = 2
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "atc" Then
            index = 5
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "pf" Then
            index = 3
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "fm" Then
            index = 4
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "pc" Then
            index = 6
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "frm" Then
            index = 7
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "ps" Then
            index = 8
        ElseIf Request.QueryString(PathConstants.RepairsDashboard) = "rdl" Then
            index = 9
        End If
        ViewState("Index") = index
        changeReport()

    End Sub

#End Region

#Region "Search Property"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()> _
    Public Shared Function getRepairSearchResult(ByVal prefixText As String, ByVal count As Integer) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()

        Dim objReportsBl As ReportsBL = New ReportsBL()
        objReportsBl.getRepairSearchResult(resultDataset, prefixText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            'result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
            Dim item As String = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row("Description").ToString, row("FaultRepairListID").ToString)
            result.Add(item)
        Next row

        Return result

    End Function

#End Region

#Region "Populate dropdowns"

#Region "Populate Recall Operative List"

    Sub populateOperativeList()

        Dim resultDataSet As DataSet = New DataSet()

        objReportsBl.getOperativeList(resultDataSet)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            ddlOperative.Enabled = False
        Else
            Dim ddlDataSet = resultDataSet.Tables(0)

            For i As Integer = 0 To ddlDataSet.Rows.Count - 1
                Dim listItem As New ListItem()
                listItem.Text = ddlDataSet.Rows(i).Item(2).ToString()
                listItem.Value = ddlDataSet.Rows(i).Item(0).ToString()
                ddlOperative.Items.Add(listItem)
            Next

        End If

    End Sub

#End Region

#Region "Populate Paused Fault Operative List"

    Sub populatePausedFaultOperativeList()

        Dim resultDataSet As DataSet = New DataSet()

        objReportsBl.getPausedOperativeList(resultDataSet)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            ddlOperative.Enabled = False
        Else
            Dim ddlDataSet = resultDataSet.Tables(0)

            For i As Integer = 0 To ddlDataSet.Rows.Count - 1
                Dim listItem As New ListItem()
                listItem.Text = ddlDataSet.Rows(i).Item(2).ToString()
                listItem.Value = ddlDataSet.Rows(i).Item(0).ToString()
                ddlOperative.Items.Add(listItem)
            Next

        End If

    End Sub

#End Region

#End Region

#End Region

End Class