﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject

Public Class SearchSbFault
    Inherits PageBase

#Region "Data Members"

    Dim isSbRecall As Boolean = False
    Dim schemeId As Integer = 0
    Dim blockId As Integer = Nothing
    Dim Jsn As String = String.Empty
    Dim sbRedirectCheck As Boolean = False
    Dim schemeBlockBo As SchemeBlockBO = New SchemeBlockBO()
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' 'This function will be called on page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                SessionManager.removeIsSbFollowOn()

                Me.getSetQueryStringParams()
                If Not isSbRecall And Jsn = String.Empty Then
                    mdlpopupScheduler.Show()
                End If
                If uiMessageHelper.IsError = False Then
                    Me.populateRecentFaults(schemeId, blockId, String.Empty, Jsn)
                    pnlControls.Enabled = True
                Else
                    'Disable screen
                    pnlControls.Enabled = False
                End If
            Else
                'Get Session Values
                getSessionValues()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                pnlControls.Enabled = False
            End If
        End Try
    End Sub

#End Region

#Region "search Fault Pre Render"
    ''' <summary>
    ''' This event handles the render event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub searchFault_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Set Values In Session
        setSessionValues()

        If sbRedirectCheck = True Then
            btnSearch.Visible = False
        Else
            btnSearch.Visible = True
        End If
    End Sub
#End Region

#Region "btn Search Click"
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            'Me.getSetQueryStringParams()
            If uiMessageHelper.IsError = False Then                
                If txtSearchHidden.Text.Trim <> String.Empty Then
                    lblSearchStatus.Text = "Results :"
                    Me.populateRecentFaults(schemeId, blockId, txtSearchHidden.Text, Jsn)
                Else
                    lblSearchStatus.Text = "Recently reported faults :"
                    Me.populateRecentFaults(schemeId, blockId, String.Empty, Jsn)
                End If
            End If

            Me.updPanelSearchBox.Update()


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Existing Fault Click"

    Protected Sub btnExistingFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExistingFault.Click
        'Set isFaultLog = 0, so, that on the next screen we show all the faults
        SessionManager.setSbFaultLogId(0)
        SessionManager.setSbFaultId(0)
        Response.Redirect(PathConstants.OtherSbAppointmentPath + GeneralHelper.getSrc(PathConstants.SearchSbFaultSrcVal), True)
    End Sub
#End Region

#Region "grd Recent Faults Row DataBound"

    Protected Sub grdRecentFaults_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRecentFaults.RowDataBound

        Dim lnkBtnDescription As LinkButton = New LinkButton()
        Dim lblDescription As Label = New Label()

        If e.Row.RowType = DataControlRowType.DataRow Then
            lnkBtnDescription = CType(e.Row.FindControl("lnkBtnDescription"), LinkButton)            

            If Not IsPostBack Then
                'If isRecall And Not IsPostBack Then
                Dim source As DataTable = CType(grdRecentFaults.DataSource, DataTable)

                If Jsn <> String.Empty Then
                    txtSearchHidden.Text = source.Rows(e.Row.RowIndex)("DescriptionOnly").ToString
                End If
            End If
        End If
    End Sub
#End Region

#Region "grd Recent Faults Row Command"
    Protected Sub grdRecentFaults_RowCommand(ByVal e As Object, ByVal sender As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRecentFaults.RowCommand
        Dim grd As LinkButton = CType(sender.CommandSource, LinkButton)
        Dim faultId = Convert.ToInt32(sender.CommandArgument)

        SessionManager.setSbFaultId(faultId)

        If (sbRedirectCheck) Then
            Response.Redirect(PathConstants.MoreSbDetailsFromStart + GeneralHelper.getSrc(PathConstants.SearchSbFaultSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.OtherSbAppointmentPath + GeneralHelper.getSrc(PathConstants.SearchSbFaultSrcVal))
        End If
    End Sub
#End Region

#End Region

#Region "Functions "

#Region "get Recent Faults "
    ''' <summary>
    ''' 'This function will return the rec
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateRecentFaults(ByRef schemeId As Integer, ByRef blockId As Integer, ByRef searchText As String, ByRef jsn As String)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objSerFaultBl As SearchFaultBL = New SearchFaultBL()

        objSerFaultBl.getSbRecentFaults(resultDataSet, schemeId, blockId, searchText, jsn)

        If (resultDataSet.Tables(ApplicationConstants.RecentFaultsDtName).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdRecentFaults.Visible = False
        Else
            'If isRecall And Not IsPostBack Then
            If isSbRecall Then
                sbRedirectCheck = True
            Else
                sbRedirectCheck = False
            End If

            If (resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows.Count > 0) Then
                Me.populateSchemeBlockObject(resultDataSet)
                Me.populateSchemeBlockName()
                'Set PatchId in session, it is mainly used on calendar to set patch filter for current property.
                'SessionManager.setPatchId(customerObj.PatchId)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidSbId, True)
                grdRecentFaults.Visible = False
            End If

            grdRecentFaults.Visible = True
            grdRecentFaults.DataSource = resultDataSet.Tables(ApplicationConstants.RecentFaultsDtName)
            grdRecentFaults.DataBind()
        End If
    End Sub
#End Region

#Region "populate scheme Block Object"
    Private Sub populateSchemeBlockObject(ByRef resultDataSet As DataSet)

        schemeBlockBo.SchemeName = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("SchemeName").ToString()
        schemeBlockBo.SchemeId = Int32.Parse(resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("SchemeId"))
        schemeBlockBo.BlockName = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("BlockName").ToString()
        schemeBlockBo.BlockId = Int32.Parse(resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("BlockId"))
        'fill object for common item of property,scheme and Block
        commonAddressBo.Street = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Address = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Area = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("County").ToString()
        commonAddressBo.City = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("TownCity").ToString()
        commonAddressBo.PostCode = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("PostCode").ToString()
        commonAddressBo.PatchId = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("PatchId").ToString()
        commonAddressBo.PatchName = resultDataSet.Tables(ApplicationConstants.SchemeBlockDtName).Rows(0).Item("PatchName").ToString()
        'store dataset in session for further use in intelligent scheduling
        SessionManager.setCommonAddressBO(Me.commonAddressBo)
        SessionManager.setSbData(Me.schemeBlockBo)
    End Sub
#End Region

#Region "Populate Scheme Block Name"
    Private Sub populateSchemeBlockName()
        lblSchemeName.Text = schemeBlockBo.SchemeName
        lblBlockName.Text = schemeBlockBo.BlockName
    End Sub

#End Region

#Region "get Set Query String Params"
    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString(PathConstants.Sid)) Then
            'Get Value from Query String
            Me.schemeId = Convert.ToString(Request.QueryString(PathConstants.Sid))
            If Not IsNothing(Request.QueryString(PathConstants.Bid)) Then
                Me.blockId = Convert.ToInt32(Request.QueryString(PathConstants.Bid))
            End If
        ElseIf SessionManager.getSchemeId() <> Nothing Then
            'If scheme id and block id doesnot exist in the querystring then check in session.(This sceneior will occur when some other screen will redirect to Screen#1)
            'Fetch IsRecall and JSN from Query String
            schemeId = SessionManager.getSchemeId()

            If SessionManager.getBlockId() <> Nothing Or SessionManager.getBlockId() <> 0 Then
                blockId = SessionManager.getBlockId()
            End If

            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) And Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
                isSbRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
                Jsn = Request.QueryString(PathConstants.Jsn).ToString

                If Jsn.Trim = String.Empty Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message = UserMessageConstants.InvalidRecallJsnId
                End If
            ElseIf ((Not IsNothing(Request.QueryString(PathConstants.isFollowOn))) _
                        AndAlso (Request.QueryString(PathConstants.isFollowOn).ToString() = PathConstants.Yes) _
                        AndAlso (Not IsNothing(Request.QueryString(PathConstants.Jsn)))) Then
                Jsn = Request.QueryString(PathConstants.Jsn).ToString
                If (Jsn = "JS" + SessionManager.getFollowOnFaultLogId().ToString()) Then
                    SessionManager.setIsSchmeBlockFollowOn(True)
                End If

            ElseIf Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
                Jsn = Request.QueryString(PathConstants.Jsn).ToString

            ElseIf Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isSbRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
            End If
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidSbId
        End If
    End Sub
#End Region

#Region "Get Session Values"
    ''' <summary>
    ''' This function gets the values from session and stores in local variables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getSessionValues()
        schemeId = SessionManager.getSchemeId()
        blockId = SessionManager.getBlockId()
        Jsn = SessionManager.getSbJobSheetNumber()
        isSbRecall = SessionManager.getIsSbRecall()
        sbRedirectCheck = SessionManager.getSbRedirectCheck()
    End Sub
#End Region

#Region "Set Session Values"

    ''' <summary>
    ''' This function stores the values in session variables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setSessionValues()
        SessionManager.setSchemeId(schemeId)
        SessionManager.setBlockId(blockId)
        SessionManager.setSbJobSheetNumber(Jsn)
        SessionManager.setIsSbRecall(isSbRecall)
        SessionManager.setSbRedirectCheck(sbRedirectCheck)
    End Sub
#End Region

#Region "Highlight Text"
    Protected Function HighlightText(ByVal inputText As String) As String

        Dim expression As Regex = New Regex(txtSearchHidden.Text.Replace(" ", "|"), RegexOptions.IgnoreCase)
        Return expression.Replace(inputText, New MatchEvaluator(AddressOf ReplaceKeywords))

    End Function
#End Region

#Region "Replace Keywords"
    Public Function ReplaceKeywords(ByVal m As Match) As String

        Return "<span class='highlight'>" + m.Value + "</span>"

    End Function
#End Region

#End Region

End Class