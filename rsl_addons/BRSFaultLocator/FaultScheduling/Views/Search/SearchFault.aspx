﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="SearchFault.aspx.vb" Inherits="FaultScheduling.SearchFault" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="custom" TagName="CustomerDetails" Src="~/Controls/Common/CustomerDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript">

        //setup before functions
//        var typingTimer;                //timer identifier
//        var doneTypingInterval = 1000;  //time in ms, 5 second for example

//        function TypingInterval() {
//            //alert("test");
//            clearTimeout(typingTimer);
//            if ($("#<%= txtSearchHidden.ClientID %>").val()) {
//                typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
//            }
//        }

//        function searchTextChanged() {
//            __doPostBack('<%= txtSearchHidden.ClientID %>', '');
//        }
        function pageLoad(){
            setFocus();            
            setResultDivHeight();
        }
        function setFocus() {
            //$("#<%= txtSearchHidden.ClientID %>").focus();
            $("#<%= txtSearchHidden.ClientID %>").putCursorAtEnd();
        }

        jQuery.fn.putCursorAtEnd = function () {

            return this.each(function () {

                $(this).focus()

                // If this function exists...
                if (this.setSelectionRange) {
                    // ... then use it (Doesn't work in IE)

                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                    var len = $(this).val().length * 2;

                    this.setSelectionRange(len, len);

                } else {
                    // ... otherwise replace the contents with itself
                    // (Doesn't work in Google Chrome)

                    $(this).val($(this).val());

                }

                // Scroll to the bottom, in case we're in a tall textarea
                // (Necessary for Firefox and Google Chrome)
                this.scrollTop = 999999;

            });

        };

        function setResultDivHeight() {
            $(document).ready(function () {

                var offset = $("#faults_Grid").offset();
                var divHeight = $("#faults_Grid").height();
                if (divHeight > ($(window).height() - offset.top)) {
                    $("#faults_Grid").height($(window).height() - offset.top - 25);
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <asp:Panel ID="pnlControls" runat="server">
                <custom:CustomerDetails ID="custDetails" runat="server" StartString="Report a fault for:">
                </custom:CustomerDetails>
                <asp:UpdatePanel ID="updPanelSearchBox" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="fault_search_controls">
                            <asp:TextBox ID="txtSearchHidden" runat="server" Width="80%" AutoPostBack="false"
                                ToolTip="Search for a fault" AutoCompleteType="Search" class="searchbox"></asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtSearchHidden" WatermarkText="Search for a fault" WatermarkCssClass="searchbox searchText">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                            &nbsp;
                            <asp:Button ID="btnSearch" runat="server" Text="Search"  Width="122px" Height="27px"
                                OnClientClick="setFocus()" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="roundcorner" id="fault_search_result">
                    <div id="fault_result_header">
                        <b>
                            <asp:Label ID="lblSearchStatus" runat="server" Text="Recently reported faults :"></asp:Label>
                        </b>
                    </div>
                    <div style="overflow: auto;" id="faults_Grid">
                    <asp:GridView ID="grdRecentFaults" runat="server" AutoGenerateColumns="False" GridLines="None"
                        Style="margin-left: 10px;" ShowHeaderWhenEmpty="True" DataKeyNames="FaultId" Height="50px" >
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton Font-Underline="false" ID="lnkBtnDescription" runat="server" Text='<%# HighlightText(Eval("Description"))%>'
                                        ForeColor="Black" CommandArgument='<%# EVAL("FaultId") %>'></asp:LinkButton>
                                 </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" BorderColor="Black" />
                        <RowStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:GridView>
                    </div> 
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnHiddenEntry" UseSubmitBehavior="false" runat="server" Text=""
        Style="display: none;" />
    <cc1:ModalPopupExtender ID="mdlpopupScheduler" runat="server" PopupControlID="pnlScheduler"
        TargetControlID="btnHiddenEntry" CancelControlID="btnNewFault" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnlScheduler" runat="server" CssClass="modalPopupSchedular" Style="height: 150px;
        width: 350px; border-color:#CCCC; border-width:1px;">
        <div style="height: auto; clear: both;">
            <table id="tblPausedFault" style="width: 350px;text-align: left; margin-right:10px;">
                <tr>
                    <td colspan="2">
                        <b>Fault Locator:</b>
                    </td>
                </tr>
                 <tr>
                        <td colspan="2" >
                            <hr class="searchFaultHr" />
                            <br />
                        </td>
                    </tr>
                     <tr>
                        <td colspan="2" align="center" >
                         <b>Is this a new or existing fault?</b>
                         <br />
                          <br />
                        </td>
                    </tr>
                     
                <tr>
                    <td align="right" style=" width:40%" >
                        <asp:Button ID="btnNewFault" runat="server" Text="New Fault" CssClass="margin_right20" OnClientClick="setFocus()" />
                       
                    </td>
                    <td align="left" style=" width:40%">
                      
                            <asp:Button ID="btnExistingFault" runat="server" CssClass="margin_right20"
                            Text="Existing Fault"  />
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:Button ID="btnClose" runat="server" Style="display: none;" Text="Close" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {

            var offset = $("#faults_Grid").offset();
            var divHeight = $("#faults_Grid").height();
            if (divHeight > ($(window).height() - offset.top)) {
                $("#faults_Grid").height($(window).height() - offset.top);
            }
        });
    </script>
</asp:Content>

