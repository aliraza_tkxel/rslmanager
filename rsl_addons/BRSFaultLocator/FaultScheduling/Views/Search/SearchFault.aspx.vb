﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject

Public Class SearchFault
    Inherits PageBase

#Region "Data Members"

    Dim isRecall As Boolean = False
    Dim propertyId As String = String.Empty
    Dim customerId As Integer
    Dim Jsn As String = String.Empty
    Dim redirectCheck As Boolean = False
    Dim customerObj As CustomerBO = New CustomerBO()
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()

#End Region

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' 'This function will be called on page load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                SessionManager.removeIsFollowOn()

                Me.getSetQueryStringParams()
                If Not isRecall And Jsn = String.Empty Then
                    mdlpopupScheduler.Show()
                End If
                If uiMessageHelper.IsError = False Then
                    Me.populateRecentFaults(propertyId, customerId, String.Empty, Jsn)
                    pnlControls.Enabled = True
                Else
                    'Disable screen
                    pnlControls.Enabled = False
                End If
            Else
                'Get Session Values
                getSessionValues()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                pnlControls.Enabled = False
            End If
        End Try
    End Sub

#End Region

#Region "search Fault Pre Render"
    ''' <summary>
    ''' This event handles the render event of the page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub searchFault_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Set Values In Session
        setSessionValues()

        If redirectCheck = True Then
            btnSearch.Visible = False
        Else
            btnSearch.Visible = True
        End If
    End Sub
#End Region

#Region "btn Search Click"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            'Me.getSetQueryStringParams()
            If uiMessageHelper.IsError = False Then
                'Dim propertyId As String = SessionManager.getPropertyId()
                'Dim customerId As String = SessionManager.getCustomerId()
                If txtSearchHidden.Text.Trim <> String.Empty Then
                    lblSearchStatus.Text = "Results :"
                    Me.populateRecentFaults(propertyId, customerId, txtSearchHidden.Text, Jsn)
                Else
                    lblSearchStatus.Text = "Recently reported faults :"
                    Me.populateRecentFaults(propertyId, customerId, String.Empty, Jsn)
                End If
            End If

            Me.updPanelSearchBox.Update()


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Existing Fault Click"

    Protected Sub btnExistingFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExistingFault.Click
        'Set isFaultLog = 0, so, that on the next screen we show all the faults
        SessionManager.setFaultLogId(0)
        SessionManager.setFaultId(0)
        Response.Redirect(PathConstants.OtherAppointmentPath + GeneralHelper.getSrc(PathConstants.SearchFaultSrcVal), True)
    End Sub
#End Region

#Region "grd Recent Faults Row DataBound"

    Protected Sub grdRecentFaults_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRecentFaults.RowDataBound

        Dim lnkBtnDescription As LinkButton = New LinkButton()
        Dim lblDescription As Label = New Label()

        If e.Row.RowType = DataControlRowType.DataRow Then
            lnkBtnDescription = CType(e.Row.FindControl("lnkBtnDescription"), LinkButton)
            ' lblDescription = CType(e.Row.FindControl("lblDescription"), Label)

            If Not IsPostBack Then
                'If isRecall And Not IsPostBack Then
                Dim source As DataTable = CType(grdRecentFaults.DataSource, DataTable)

                If Jsn <> String.Empty Then
                    txtSearchHidden.Text = source.Rows(e.Row.RowIndex)("DescriptionOnly").ToString
                End If
            End If
        End If

        'If (Not String.IsNullOrEmpty(SessionManager.getPropertyId()) And SessionManager.getCustomerId() <> 0 And Not String.IsNullOrEmpty(txtSearchHidden.Text)) Then
        'If (Not String.IsNullOrEmpty(propertyId) And customerId <> 0 And Not String.IsNullOrEmpty(txtSearchHidden.Text) And Not String.IsNullOrWhiteSpace(txtSearchHidden.Text)) Then
        '    lblDescription.Visible = False
        'Else
        '    lnkBtnDescription.Visible = False
        'End If

        'If isRecall And Jsn <> String.Empty Then
        'If Jsn <> String.Empty Then
        '    lblDescription.Visible = False
        '    lnkBtnDescription.Visible = True
        'End If

    End Sub
#End Region

    '#Region "txtSearch Hidden Text Changed"
    '    Protected Sub txtSearchHidden_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearchHidden.TextChanged
    '        Try
    '            'Me.getSetQueryStringParams()
    '            If uiMessageHelper.IsError = False Then
    '                'Dim propertyId As String = SessionManager.getPropertyId()
    '                'Dim customerId As String = SessionManager.getCustomerId()
    '                If txtSearchHidden.Text.Trim <> String.Empty Then
    '                    lblSearchStatus.Text = "Results :"
    '                    Me.populateRecentFaults(propertyId, customerId, txtSearchHidden.Text, Jsn)
    '                Else
    '                    lblSearchStatus.Text = "Recent faults :"
    '                    Me.populateRecentFaults(propertyId, customerId, String.Empty, Jsn)
    '                End If
    '            End If

    '            Me.updPanelSearchBox.Update()


    '        Catch ex As Exception
    '            uiMessageHelper.IsError = True
    '            uiMessageHelper.message = ex.Message

    '            If uiMessageHelper.IsExceptionLogged = False Then
    '                ExceptionPolicy.HandleException(ex, "Exception Policy")
    '            End If

    '        Finally
    '            If uiMessageHelper.IsError = True Then
    '                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
    '            End If
    '        End Try
    '    End Sub
    '#End Region

#Region "grd Recent Faults Row Command"
    Protected Sub grdRecentFaults_RowCommand(ByVal e As Object, ByVal sender As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdRecentFaults.RowCommand
        Dim grd As LinkButton = CType(sender.CommandSource, LinkButton)
        Dim faultId = Convert.ToInt32(sender.CommandArgument)

        SessionManager.setFaultId(faultId)

        If (redirectCheck) Then
            Response.Redirect(PathConstants.MoreDetailsFromStart + GeneralHelper.getSrc(PathConstants.SearchFaultSrcVal) + "&" + PathConstants.IsRecall + "=" + PathConstants.Yes)
        Else
            Response.Redirect(PathConstants.OtherAppointmentPath + GeneralHelper.getSrc(PathConstants.SearchFaultSrcVal))
        End If
    End Sub
#End Region

#End Region

#Region "Functions "

#Region "get Recent Faults "
    ''' <summary>
    ''' 'This function will return the rec
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateRecentFaults(ByRef propertyId As String, ByRef customerId As Integer, ByRef searchText As String, ByRef jsn As String)
        Dim resultDataSet As DataSet = New DataSet()
        Dim objSerFaultBl As SearchFaultBL = New SearchFaultBL()

        objSerFaultBl.getRecentFaults(resultDataSet, propertyId, customerId, searchText, jsn)

        If (resultDataSet.Tables(ApplicationConstants.RecentFaultsDtName).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            grdRecentFaults.Visible = False
        Else
            'If isRecall And Not IsPostBack Then
            If isRecall Then
                redirectCheck = True
            Else
                redirectCheck = False
            End If

            If (resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows.Count > 0 And resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows.Count > 0) Then
                Me.populateCustomerObject(resultDataSet)

                'Set PatchId in session, it is mainly used on calendar to set patch filter for current property.
                SessionManager.setPatchId(customerObj.PatchId)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPropertyCustomerId, True)
                grdRecentFaults.Visible = False
            End If

            grdRecentFaults.Visible = True
            grdRecentFaults.DataSource = resultDataSet.Tables(ApplicationConstants.RecentFaultsDtName)
            grdRecentFaults.DataBind()
        End If
    End Sub
#End Region

#Region "populate Customer Object"
    Private Sub populateCustomerObject(ByRef resultDataSet As DataSet)
        Me.customerObj.CustomerId = Me.customerId
        customerObj.Name = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Name").ToString()
        customerObj.Street = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("Address").ToString()
        customerObj.Address = String.Empty
        customerObj.Area = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("County").ToString()
        customerObj.Telephone = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Telephone").ToString()
        customerObj.Mobile = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Mobile").ToString()
        customerObj.Email = resultDataSet.Tables(ApplicationConstants.CustomerDtName).Rows(0).Item("Email").ToString()
        customerObj.City = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("TownCity").ToString()
        customerObj.PostCode = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PostCode").ToString()
        customerObj.PatchId = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchId").ToString()
        customerObj.PatchName = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchName").ToString()
        'fill object for common item of property,scheme and Block
        commonAddressBo.Street = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Address = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("Address").ToString()
        commonAddressBo.Area = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("County").ToString()
        commonAddressBo.City = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("TownCity").ToString()
        commonAddressBo.PostCode = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PostCode").ToString()
        commonAddressBo.PatchId = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchId").ToString()
        commonAddressBo.PatchName = resultDataSet.Tables(ApplicationConstants.PropertyDtName).Rows(0).Item("PatchName").ToString()
        'store dataset in session for further use in intelligent scheduling
        SessionManager.setCustomerData(Me.customerObj)
        SessionManager.setCommonAddressBO(Me.commonAddressBo)
    End Sub
#End Region

#Region "get Set Query String Params"
    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString(PathConstants.Pid)) And Not IsNothing(Request.QueryString(PathConstants.Cid)) Then
            'Get Value from Query String
            propertyId = Convert.ToString(Request.QueryString(PathConstants.Pid))
            customerId = Convert.ToInt32(Request.QueryString(PathConstants.Cid))

        ElseIf SessionManager.getPropertyId() <> Nothing And SessionManager.getCustomerId() <> Nothing Then
            'If property id and customer id doesnot exist in the querystring then check in session.(This sceneior will occur when some other screen will redirect to Screen#1)
            'Fetch IsRecall and JSN from Query String
            propertyId = SessionManager.getPropertyId()
            customerId = SessionManager.getCustomerId()

            If Not IsNothing(Request.QueryString(PathConstants.IsRecall)) And Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
                Jsn = Request.QueryString(PathConstants.Jsn).ToString

                If Jsn.Trim = String.Empty Then
                    uiMessageHelper.IsError = True
                    uiMessageHelper.message = UserMessageConstants.InvalidRecallJsnId
                End If
            ElseIf ((Not IsNothing(Request.QueryString(PathConstants.isFollowOn))) _
                        AndAlso (Request.QueryString(PathConstants.isFollowOn).ToString() = PathConstants.Yes) _
                        AndAlso (Not IsNothing(Request.QueryString(PathConstants.Jsn)))) Then
                Jsn = Request.QueryString(PathConstants.Jsn).ToString
                If (Jsn = "JS" + SessionManager.getFollowOnFaultLogId().ToString()) Then
                    SessionManager.setIsFollowOn(True)
                End If

            ElseIf Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
                Jsn = Request.QueryString(PathConstants.Jsn).ToString

            ElseIf Not IsNothing(Request.QueryString(PathConstants.IsRecall)) Then
                isRecall = IIf(Request.QueryString(PathConstants.IsRecall).ToString = PathConstants.Yes, True, False)
            End If
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidPropertyCustomerId
        End If
    End Sub
#End Region

#Region "Get Session Values"
    ''' <summary>
    ''' This function gets the values from session and stores in local variables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getSessionValues()
        propertyId = SessionManager.getPropertyId()
        customerId = SessionManager.getCustomerId()
        Jsn = SessionManager.getJobSheetNumber()
        isRecall = SessionManager.getIsRecall()
        redirectCheck = SessionManager.getRedirectCheck()
    End Sub
#End Region

#Region "Set Session Values"

    ''' <summary>
    ''' This function stores the values in session variables
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setSessionValues()
        SessionManager.setPropertyId(propertyId)
        SessionManager.setCustomerId(customerId)
        SessionManager.setJobSheetNumber(Jsn)
        SessionManager.setIsRecall(isRecall)
        SessionManager.setRedirectCheck(redirectCheck)
    End Sub
#End Region

#Region "Highlight Text"
    Protected Function HighlightText(ByVal inputText As String) As String

        Dim expression As Regex = New Regex(txtSearchHidden.Text.Replace(" ", "|"), RegexOptions.IgnoreCase)
        Return expression.Replace(inputText, New MatchEvaluator(AddressOf ReplaceKeywords))

    End Function
#End Region

#Region "Replace Keywords"
    Public Function ReplaceKeywords(ByVal m As Match) As String

        Return "<span class='highlight'>" + m.Value + "</span>"

    End Function
#End Region

#End Region

End Class