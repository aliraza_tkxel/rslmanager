﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class SbSchedulingCalendar

    '''<summary>
    '''Button1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''SbHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SbHeader As Global.FaultScheduling.SchemeBlockHeader

    '''<summary>
    '''ddlPatches control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPatches As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlTrades control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlTrades As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''grdAppointments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdAppointments As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''lblMonth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMonth As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hrPrevious control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hrPrevious As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hlToday control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hlToday As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hlNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hlNext As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRearrange control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRearrange As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlAppointmentToBeArrangedTenantInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAppointmentToBeArrangedTenantInfo As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''tblTenantInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblTenantInfo As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''imgBtnAptbaClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnAptbaClose As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''mdlPopUpAppointmentToBeArrangedPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlPopUpAppointmentToBeArrangedPhone As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''lbldisplayAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbldisplayAppointment As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlAddAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlAddAppointment As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''imgBtnCloseAddAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnCloseAddAppointment As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''pnlPopupMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPopupMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblPopupMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPopupMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlActivityPopupMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlActivityPopupMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblActivityPopupMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblActivityPopupMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''calStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calStartDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStartDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlAppStartTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAppStartTime As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''calEndDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calEndDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtEndDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEndDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlAppEndTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAppEndTime As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblOperative control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOperative As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtFaultNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFaultNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnCancelAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancelAppointment As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSaveAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveAppointment As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mdlPopUpAddAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlPopUpAddAppointment As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''lblDispAddAppointmentPopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDispAddAppointmentPopup As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultIdHidden control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultIdHidden As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''popupJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents popupJobSheet As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''pnlJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlJobSheet As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ifrmJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifrmJobSheet As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnPrintJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPrintJobSheet As Global.System.Web.UI.WebControls.Button
End Class
