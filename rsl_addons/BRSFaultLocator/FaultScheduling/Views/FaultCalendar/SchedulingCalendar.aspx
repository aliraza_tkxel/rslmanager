﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Fault.Master"
    CodeBehind="SchedulingCalendar.aspx.vb" Inherits="FaultScheduling.SchedulingCalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="custom" TagName="CustomerDetails" Src="~/Controls/Common/CustomerDetail.ascx" %>
<%@ Import Namespace="FLS_Utilities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../Styles/aptCalendarStyle.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function displayMore(value, tenancy) {
            document.getElementById("display" + value).style.display = 'block';
            document.getElementById("arrow-left" + value).style.display = 'none';
            document.getElementById("arrow-down" + value).style.display = 'block';
            if (tenancy == "1") {
                document.getElementById("addressDiv" + value).style.display = 'none';
            }
        }

        function displayMoreHide(value) {
            document.getElementById("arrow-left" + value).style.display = 'block';
            document.getElementById("arrow-down" + value).style.display = 'none';
            document.getElementById("display" + value).style.display = 'none';
            document.getElementById("addressDiv" + value).style.display = 'block';
        }

        function PrintJobSheet() {
            javascript: window.open('../Faults/PrintJobSheetDetail.aspx?JobSheetNumber=' + document.getElementById("lblFaultIdHidden").innerHTML);
        }

        function openPopUp(tenancyId) {
            __doPostBack("<%= Button1.UniqueID %>", tenancyId);
        }    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button ID="Button1" runat="server" Text="Button" Style="display: none;" />
    <custom:CustomerDetails ID="custDetails" runat="server" StartString="Schedule appointment for:">
    </custom:CustomerDetails>
    <div class="line-border">
    </div>
    <div class="main-labels-heading">
        <div class="left-top">
            <div class="cover">
                <div class="lable1">
                    <strong>Patch:</strong>
                </div>
                <div class="lable2">
                    <strong>
                        <asp:DropDownList ID="ddlPatches" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </strong>
                </div>
            </div>
            <div class="cover">
                <div class="lable1">
                    <strong>Trade:</strong>
                </div>
                <div class="lable2">
                    <strong>
                        <asp:DropDownList ID="ddlTrades" runat="server" Style="margin-bottom: 0px" AutoPostBack="True">
                        </asp:DropDownList>
                    </strong>
                </div>
            </div>
        </div>
        <div class="right-top" style="height: 50px; overflow-y: auto">
            <asp:GridView ID="grdAppointments" runat="server" AutoGenerateColumns="False" Width="100%"
                CssClass="gridfaults" GridLines="None" DataKeyNames="FaultID" ShowHeader="False">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <%#Container.DataItemIndex + 1%>.
                            <asp:Label ID="lblAreaText" runat="server" Text='Location:'></asp:Label>
                            <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AreaName") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Wrap="False" HorizontalAlign="Left" Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:Label ID="lblDescriptionText" runat="server" Text="Description:"></asp:Label>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Wrap="False" HorizontalAlign="Left" Width="60px" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:Label ID="lblResponseText" runat="server" Text="Response:"></asp:Label>
                            <asp:Label ID="lblResponse" runat="server" Text='<%# Bind("Response") %>'></asp:Label>
                            (<asp:Label ID="lblDueDate" runat="server" Text='<%# Bind("DueDate") %>'></asp:Label>)
                        </ItemTemplate>
                        <ItemStyle Wrap="False" HorizontalAlign="Left" Width="100px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div style="float: left; width: 100%;">
        <div style="padding: 10px 0px; float: right;">
            <div style="float: left; margin: 0px 5px;">
                <div style="padding: 2px; float: left;">
                    <img src="../../Images/arranged.png" border="0" alt="Appointment Arranged" />
                </div>
                <div style="float: left;">
                    <strong>Appointment Arranged</strong>
                </div>
                <div style="clear: left">
                </div>
            </div>
            <div style="float: left; margin: 0px 5px;">
                <div style="padding: 2px; float: left;">
                    <img src="../../Images/inprogress.png" border="0" alt="In Progress" />
                </div>
                <div style="float: left;">
                    <strong>In Progress</strong>
                </div>
                <div style="clear: left">
                </div>
            </div>
            <div style="float: left; margin: 0px 5px;">
                <div style="padding: 2px; float: left;">
                    <img src="../../images/completed.png" border="0" alt="Completed" />
                </div>
                <div style="float: left;">
                    <strong>Completed</strong>
                </div>
                <div style="clear: left">
                </div>
            </div>
            <div style="float: left; margin: 0px 5px;">
                <div style="padding: 2px; float: left;">
                    <img src="../../Images/paused.png" border="0" alt="Paused" />
                </div>
                <div style="float: left; height: 15px;">
                    <strong>Paused</strong>
                </div>
                <div style="clear: left">
                </div>
            </div>
            <div style="float: left; margin: 0px 5px;">
                <div style="padding: 2px; float: left;">
                    <img src="../../Images/no_entry.png" border="0" alt="No Entry" />
                </div>
                <div style="float: left;">
                    <strong>No Entry</strong>
                </div>
                <div style="clear: left">
                </div>
            </div>
            <div style="clear: left">
            </div>
        </div>
        <div style="float: left; width: 99%; padding: 4px; margin-top: 10px; border: 1px solid #000">
            <div style="float: left; width: 91%;">
                <asp:Label ID="lblMonth" runat="server" Font-Bold="True"></asp:Label>
            </div>
            <div style="float: left; width: 8%; text-align: right;">
                <span style="border: solid 1px gray; padding: 0px !important; float: left;">
                    <asp:HyperLink ID="hrPrevious" runat="server" NavigateUrl="SchedulingCalendar.aspx?mov=p"><img alt=">" src="../../images/arrow_left.png" border="0" style="padding-top: 2px;vertical-align: text-top;float:left" /></asp:HyperLink>
                    <span style="border-left: solid 1px gray; border-right: solid 1px gray; float: left;
                        font-size: 12px; height: 17px;">
                        <asp:HyperLink ID="hlToday" runat="server" NavigateUrl="SchedulingCalendar.aspx">Today</asp:HyperLink>
                    </span>
                    <asp:HyperLink ID="hlNext" runat="server" NavigateUrl="SchedulingCalendar.aspx?mov=n"><img src="../../images/arrow_right.png" border="0" style="padding-top: 2px;padding-left: 2px;
    vertical-align: text-top;float:left" alt=""/></asp:HyperLink>
                </span>
            </div>
        </div>
        <div>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <% 
                If varWeeklyDT.Rows.Count > 0 Then%>
            <table width="100%" border="0" style="border-left: solid 1px gray; border-top: solid 1px gray;
                float: left;" cellspacing="0">
                <tr>
                    <%  Dim empArray() As String
                        Dim bgColor As String
                        For dtCount = 0 To varWeeklyDT.Columns.Count - 1%>
                    <td style="font-weight: bold; border-left: solid 1px gray; border-right: solid 1px gray;
                        border-bottom: solid 1px gray; text-align: center;" width="10%">
                        <span style="margin-bottom: 10px;">
                            <%  If varWeeklyDT.Columns(dtCount).ToString() <> " " Then
                                    empArray = varWeeklyDT.Columns(dtCount).ToString().Split(":")
                            %>
                            <%Response.Write(empArray(1).Trim())%></span>&nbsp; <a href="SchedulingCalendar.aspx?opr=<%=empArray(1).Trim()+"&id="+empArray(0) %>">
                                <img id="imgAddAppointment" border="0" src="../../images/btn_add.png" alt="" />
                            </a>
                    </td>
                    <%  End If
                    Next%>
                </tr>
                <%  For rdCount = 0 To varWeeklyDT.Rows.Count - 1
                        If (rdCount Mod 2 = 0) Then
                            bgColor = "#E8E9EA"
                        Else
                            bgColor = "#FFFFFF"
                        End If
                %>
                <tr style='<%response.write("background-color:"+ bgColor+";")%>'>
                    <%  For rdItemCount = 0 To varWeeklyDT.Columns.Count - 1
                            If Not String.IsNullOrEmpty(varWeeklyDT.Rows(rdCount).Item(rdItemCount).ToString()) Then

                                If rdItemCount = 0 Then
                             
                    %>
                    <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; vertical-align: top;"
                        width="10%">
                        <div class="subHeading">
                            <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%></div>
                    </td>
                    <%Else%>
                    <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; vertical-align: top;"
                        width="10%">
                        <%Response.Write(varWeeklyDT.Rows(rdCount).Item(rdItemCount))%>
                    </td>
                    <%End If%><%Else%>
                    <td style="border-right: 1px solid gray; border-bottom: 1px solid gray; vertical-align: top;"
                        width="10%">
                        &nbsp;
                    </td>
                    <%End If%><%
                              Next%>
                </tr>
                <%  Next%>
            </table>
            <%  End If
            %>
            <div style="float: right;">
                <asp:Button ID="btnBack" runat="server" Text="< Back" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel Fault" Visible="false" />
                <asp:Button ID="btnRearrange" runat="server" Text="Rearrange Appointment" OnClick="btnRearrange_Click"
                    Visible="false" />
            </div>
        </div>
    </div>
    <!-----------------------------------------------------Tenancy Information PopUp-------------------------------------------------->
    <asp:Panel ID="pnlAppointmentToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
        Style="padding: 10px; border: 1px solid gray;">
        <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
            <tr>
                <td style="width: 0px; height: 0px;">
                    <asp:ImageButton ID="imgBtnAptbaClose" runat="server" Style="position: absolute;
                        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdlPopUpAppointmentToBeArrangedPhone" runat="server"
        TargetControlID="lbldisplayAppointment" PopupControlID="pnlAppointmentToBeArrangedTenantInfo"
        Enabled="true" DropShadow="true" CancelControlID="imgBtnAptbaClose">
    </asp:ModalPopupExtender>
    <asp:Label ID="lbldisplayAppointment" runat="server"></asp:Label>
    <!-----------------------------------------------------Add Appointment for Fault-------------------------------------------------->
    <asp:Panel ID="pnlAddAppointment" CssClass="left" runat="server" BackColor="White"
        Style="display: none; padding: 15px 5px; border: 1px solid black;">
        <div style="width: 100%; font-weight: bold; padding-left: 10px;">
            Schedule Appointment</div>
        <div style="clear: both; height: 1px;">
        </div>
        <hr />
        <asp:ImageButton ID="imgBtnCloseAddAppointment" runat="server" Style="position: absolute;
            top: -12px; right: -12px; width: 22px; height: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
            BorderWidth="0" OnClick="imgBtnCloseAddAppointment_Click" />
        <div style="width: 100%; overflow: hidden;">
            <div style="padding-top: 5px !important;">
                <table>
                    <tr>
                        <td colspan="2" valign="top">
                            <asp:Panel ID="pnlPopupMessage" runat="server" Visible="false">
                                <asp:Label ID="lblPopupMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="2">
                            <asp:Panel ID="pnlActivityPopupMessage" runat="server" Visible="false">
                                <asp:Label ID="lblActivityPopupMessage" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Telephone :
                        </td>
                        <td>
                            <asp:Label ID="lblTelphone" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Mobile :
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMobile" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Location:
                        </td>
                        <td align="left">
                            <asp:Label ID="lblLocation" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Start Date:<span class="Required"></span>
                        </td>
                        <td align="left">
                            <asp:CalendarExtender ID="calStartDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                PopupButtonID="imgCalStartDate" PopupPosition="BottomRight" TargetControlID="txtStartDate"
                                TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" ClearTime="False">
                            </asp:CalendarExtender>
                            <asp:TextBox ID="txtStartDate" runat="server" Text="" Width="75px"></asp:TextBox>
                            &nbsp;<img alt="Open Calendar" src="../../Images/calendar.png" id="imgCalStartDate"
                                style="vertical-align: bottom;" />
                            <asp:DropDownList ID="ddlAppStartTime" runat="server" Width="65px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            End Date:
                        </td>
                        <td align="left">
                            <asp:CalendarExtender ID="calEndDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                PopupButtonID="imgCalEndDate" PopupPosition="BottomRight" TargetControlID="txtEndDate"
                                TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:TextBox ID="txtEndDate" runat="server" Text="" Width="75px"></asp:TextBox>
                            &nbsp;<img alt="Open Calendar" src="../../Images/calendar.png" id="imgCalEndDate"
                                style="vertical-align: bottom;" />
                            <asp:DropDownList ID="ddlAppEndTime" runat="server" Width="65px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Operative:
                        </td>
                        <td align="left">
                            <asp:Label ID="lblOperative" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            Appointment Notes:
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFaultNotes" runat="server" Height="52px" TextMode="MultiLine"
                                Width="267px" MaxLength="1000"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" valign="top" style="text-align: right;" colspan="2">
                            <asp:Button ID="btnCancelAppointment" runat="server" Text="Cancel" OnClick="btnCancelAppointment_Click" />&nbsp;&nbsp;
                            <asp:Button ID="btnSaveAppointment" runat="server" Text="Save Appointment" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:ModalPopupExtender ID="mdlPopUpAddAppointment" runat="server" DynamicServicePath=""
        Enabled="True" TargetControlID="lblDispAddAppointmentPopup" PopupControlID="pnlAddAppointment"
        DropShadow="true" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
    <!-----------------------------------------------------End Add Appointment for Fault------------------------------------------>
    <!-- ModalPopupExtender -->
    <asp:Label ID="lblFaultIdHidden" runat="server" Text="" Style="display: none;" ClientIDMode="Static" />
    <asp:ModalPopupExtender ID="popupJobSheet" runat="server" PopupControlID="pnlJobSheet"
        TargetControlID="lblFaultIdHidden" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </asp:ModalPopupExtender>
    <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="min-height: 600px;
        height: 90%; min-width: 900px; overflow: auto; display: none;">
        <iframe runat="server" id="ifrmJobSheet" class="ifrmJobSheet" style="min-height: 575px;
            overflow: auto; width: 100%; border: 0px none transparent; height: 96%;" />
        <div style="width: 100%; text-align: left; clear: both;">
            <div style="float: right;">
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="margin_right20" />
                <asp:Button ID="btnPrintJobSheet" runat="server" Text="Print Job Sheet" CssClass="margin_right20"
                    OnClientClick="PrintJobSheet()" />
            </div>
        </div>
        <br />
    </asp:Panel>
    <!-- ModalPopupExtender -->
</asp:Content>
