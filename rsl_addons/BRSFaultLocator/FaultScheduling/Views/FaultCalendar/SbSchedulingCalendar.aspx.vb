﻿Imports System
Imports System.Text
Imports System.Data
Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject
Imports System.Net
Imports System.IO


Public Class SbSchedulingCalendar
    Inherits PageBase
#Region "Attributes"

    Public varWeeklyDT As DataTable = New DataTable()
    Dim objCalendarBL As CalendarBL = New CalendarBL()
    Dim objFaultBL As FaultBL = New FaultBL()
    Dim objCalendarUtilities As CalendarUtilities = New CalendarUtilities()
    Dim objCalendarAppointmentsBO As CalendarAppointmentsBO = New CalendarAppointmentsBO()

    Dim dsEmployeeList As DataSet = New DataSet()
    Dim dsAppointments As DataSet = New DataSet()
    Dim dsEmployeeLeaves As DataSet = New DataSet()
    Dim dsEmployeeCoreWorkingHours As DataSet = New DataSet()
    Dim drLeaves() As DataRow
    Dim drAppointmentJSN() As DataRow

    Dim strAppointment As StringBuilder = New StringBuilder()
    Dim strEmployeeIds As StringBuilder = New StringBuilder()
    Dim faultsListString As StringBuilder = New StringBuilder()
    Dim rowCount As Integer
    'Dim objcustomerData As CustomerBO = New CustomerBO()
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
    Dim appointmentId As Integer = 0
    Dim isRecall As Boolean = False
    Public openPopup As Boolean = False

    'Query String Values
    'Source the previous screen name from which this page is being called
    Dim srcQueryString As String = String.Empty
#End Region

#Region "Page Load "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            appointmentId = SessionManager.getSbAppointmentId()
            Me.isRecall = SessionManager.getIsRecall()

            Me.getQueryStringValues()

            Me.prePopulatedValues() ' Populate Patch and Trade Dropdown list
            Me.displayFaultInfo() 'Get (,) splitted Temp Fault/Log Ids

            'For current fault display appropriate patch and trade
            If ((Not IsPostBack) AndAlso ((SessionManager.getIsSbAppointmentRearrange() AndAlso srcQueryString = PathConstants.SbRearrangeAppointmentSrcVal) _
                                  OrElse (Not SessionManager.getIsSbAppointmentRearrange() AndAlso srcQueryString = PathConstants.AvailableSbAppointmentsSrcVal))) Then
                setDropDownsforCurrentAppointment(appointmentId)
                Me.saveDropdownsState()
                Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar

                'Display New Appointment Popup Check
            ElseIf (Not IsNothing(Request.QueryString("opr"))) Then
                'Me.prePopulatedValues() ' Populate Patch and Trade Dropdown list
                Me.restoreDropdownsState()
                'Me.displayFaultInfo() 'Get (,) splitted Temp Fault/Log Ids
                Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar
                Me.OpenAppointmentPopup()

                ' Display Fault Details Against JSN
            ElseIf (Not IsNothing(Request.QueryString("jsn")) AndAlso Not IsPostBack) Then
                Me.ViewFault(Request.QueryString("jsn"))
                'Me.prePopulatedValues() ' Populate Patch and Trade Dropdown list 
                Me.restoreDropdownsState()
                'Me.displayFaultInfo() 'Get (,) splitted Temp Fault/Log Ids
                Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar

                'Calendar move forward or backward check
            ElseIf (Not IsPostBack AndAlso ((Not IsNothing(Request.QueryString("mov")) AndAlso (Request.QueryString("mov") = "n" OrElse Request.QueryString("mov") = "p")) OrElse Request.QueryString.Count = 0)) Then
                Me.restoreDropdownsState()
                Me.displayCalendarScheduleAppointment()

                'Save New Appointment Check
            ElseIf (SessionManager.getSbAppointmentId() > 0 And SessionManager.getisSuccessAdded() = False) Then
                Me.showHideButtons(True) 'Enabling screen 11b,14 buttons
                SessionManager.setisSuccessAdded(True)
                'Me.prePopulatedValues() 'Populate Patch and Trade Dropdown list
                Me.saveDropdownsState()
                'Me.restoreDropdownsState()
                'Me.displayFaultInfo() 'Get (,) splitted Temp Fault/Log Ids
                Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar

            ElseIf (Not IsPostBack) Then
                'Me.prePopulatedValues() 'Populate Patch and Trade Dropdown list
                Me.saveDropdownsState()
                'Me.displayFaultInfo() 'Get (,) splitted Temp Fault/Log Ids
                'Me.showHideButtons() 'Disabling screen 11b,14 buttons
                Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Function"

#Region "Load PrePopulated Values"
    Private Sub prePopulatedValues()
        If (ddlPatches.Items.Count <= 0) Then
            Me.getPatches(ddlPatches)
        End If
        If (Me.ddlTrades.Items.Count <= 0) Then
            Me.getTrades(ddlTrades)
        End If

        commonAddressBo = SessionManager.getCommonAddressBO()
    End Sub
#End Region

#Region "get Temp Fault/Log Ids"
    Private Sub displayFaultInfo()
        'faultsListString.Append("1485,1487") 'Temp Fault Ids for testing
        'Me.getFaultsDetail() '- for testing
        'Actual Values
        If (SessionManager.getSbAppointmentId() > 0) Then
            PopulateFaultsGrid(CType(SessionManager.getSbConfirmedFaults(), DataTable), "FaultLogId")
        Else
            PopulateFaultsGridByDv(CType(SessionManager.getSbSelectedTempFaultDv(), DataView), "TempFaultId")
        End If
    End Sub
#End Region

#Region "Populate Faults Grid"

    Sub PopulateFaultsGrid(ByVal faultIdDt As DataTable, ByVal columnName As String)
        Try
            If faultIdDt.Rows.Count > 0 And faultsListString.Length <= 0 Then
                If (faultsListString.Length <= 0) Then 'Check if Fault Log Ids are empty
                    For Each dt As DataRow In faultIdDt.Rows
                        faultsListString.Append(dt(columnName))
                        faultsListString.Append(",")
                    Next

                    faultsListString = faultsListString.Remove(faultsListString.Length - 1, 1)
                End If
                grdAppointments.DataSource = faultIdDt
                grdAppointments.DataBind()
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.NoTempFaultData
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            End If
        End Try
    End Sub

    Sub PopulateFaultsGridByDv(ByVal faultIdDv As DataView, ByVal columnName As String)
        Try
            If faultIdDv.Count > 0 Then
                If (faultsListString.Length <= 0) Then 'Check if Temp Fault Ids are empty
                    For Each dt As DataRowView In faultIdDv
                        faultsListString.Append(dt(columnName))
                        faultsListString.Append(",")
                    Next

                    faultsListString = faultsListString.Remove(faultsListString.Length - 1, 1)
                End If
                grdAppointments.DataSource = faultIdDv
                grdAppointments.DataBind()
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = UserMessageConstants.NoTempFaultData
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
            End If
        End Try
    End Sub


    Sub getFaultsDetail()

        Try
            If (faultsListString.Length > 0) Then 'Validating Fault Log Ids
                Dim resultDataSet As DataSet = New DataSet()
                objFaultBL.getFaultsList(resultDataSet, faultsListString.ToString())

                If resultDataSet.Tables(0).Rows.Count = 0 Then
                    ViewState.Add("resultDataSet", resultDataSet)
                Else

                    ViewState.Add("resultDataSet", resultDataSet)

                    grdAppointments.DataSource = resultDataSet
                    grdAppointments.DataBind()

                    Dim slideIndex As Integer = 0
                    ViewState.Add("slideIndex", 1)
                    Dim slideDataSet = resultDataSet.Tables(0)

                    If slideDataSet.Rows(slideIndex).Item(6).ToString() = 1 Then
                        'lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item(6).ToString() + " day"
                    Else
                        'lblProblemDays.Text = "The fault has existed for " + slideDataSet.Rows(slideIndex).Item(6).ToString() + " days"
                    End If

                    If slideDataSet.Rows(slideIndex).Item(7).ToString() = True Then
                        'lblReccuringProblem.Text = "This is a recurring problem"
                    Else
                        'lblReccuringProblem.Text = "This is not a recurring problem"
                    End If

                    If IsDBNull(slideDataSet.Rows(slideIndex).Item(8)) Then
                    Else
                        If slideDataSet.Rows(slideIndex).Item(8).ToString() = True Then
                            'lblCommunalProblem.Text = "This is a communal problem"
                        Else
                            'lblCommunalProblem.Text = "This is not a communal problem"
                        End If
                    End If


                    If resultDataSet.Tables(0).Rows.Count = 1 Then
                        'lbNextFault.Enabled = False
                    End If

                End If
                'Else
                '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'lbNextFault.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Load Patches Information"
    Private Sub getPatches(ByVal ddl As DropDownList)
        Dim userTypeList As List(Of CalendarAppointmentsBO) = New List(Of CalendarAppointmentsBO)()
        objCalendarBL.getPatchLocations(0, userTypeList)
        ddl.DataSource = userTypeList
        ddl.DataValueField = "PatchId"
        ddl.DataTextField = "Description"
        ddl.DataBind()

    End Sub
#End Region

#Region "Load Trades Information"
    Private Sub getTrades(ByVal ddl As DropDownList)
        Dim userTypeList As List(Of CalendarAppointmentsBO) = New List(Of CalendarAppointmentsBO)()
        objCalendarBL.getTrades(0, userTypeList)
        ddl.DataSource = userTypeList
        ddl.DataValueField = "PatchId"
        ddl.DataTextField = "Description"
        ddl.DataBind()

    End Sub
#End Region

#Region "display Schedule Appointment For Weekly Calendar"
    Protected Sub displayCalendarScheduleAppointment()
        Try
            Dim divCounter As Integer
            'Calendar code start here
            If (Request.QueryString("mov") = "n") Then 'Move to next dates
                If (Not IsPostBack) Then
                    ApplicationConstants.startDate = DateAdd("d", 7, ApplicationConstants.startDate)
                End If

            ElseIf ((Not IsPostBack) AndAlso Request.QueryString("mov") = "p") Then 'Move to previous dates
                If (Not IsPostBack) Then
                    ApplicationConstants.startDate = DateAdd("d", -7, ApplicationConstants.startDate)
                End If

            ElseIf (Not IsPostBack AndAlso (Request.QueryString.Count = 0 OrElse (Not IsNothing(Request.QueryString(PathConstants.Src)) AndAlso Not Request.QueryString(PathConstants.Src) = PathConstants.SbScheduleCalendarSrcVal))) Then
                ApplicationConstants.startDate = Date.Today

                If (Not IsNothing(SessionManager.getSbConfirmedFaults()) AndAlso (Not IsPostBack) AndAlso SessionManager.getIsSbAppointmentRearrange() AndAlso appointmentId > 0 AndAlso srcQueryString = PathConstants.OtherAppointmentSrcVal) Then
                    Dim currentFaultsDT As DataTable = SessionManager.getSbConfirmedFaults()
                    If currentFaultsDT.Rows.Count > 0 Then
                        ApplicationConstants.startDate = currentFaultsDT(0)("AppointmentDate")
                    End If
                End If

            End If
            'Seek for Monday as starting day of the Week
            If (ApplicationConstants.startDate.ToString("dddd") <> "Monday") Then
                objCalendarUtilities.calculateMonday()
            End If
            'Add 6 days in start date to get end date, i.e monday 1st + 6 days = sunday 7th
            ApplicationConstants.endDate = DateAdd("d", 6, ApplicationConstants.startDate)
            'Seek for Sunday ending day of the Week
            If (ApplicationConstants.endDate.ToString("dddd") <> "Sunday") Then
                objCalendarUtilities.calculateSunday()
            End If
            objCalendarAppointmentsBO.PatchId = ddlPatches.SelectedItem.Value
            objCalendarAppointmentsBO.TradeId = ddlTrades.SelectedItem.Value
            objCalendarBL.getFaultEngineersForAppointment(objCalendarAppointmentsBO, dsEmployeeList)
            If (varWeeklyDT.Rows.Count <= 0) Then
                If (dsEmployeeList.Tables(0).Rows.Count > 0) Then 'If Re Engineer exist
                    'Sending Start and End date
                    objCalendarAppointmentsBO.StartDate = ApplicationConstants.startDate
                    objCalendarAppointmentsBO.EndDate = DateAdd("d", 6, ApplicationConstants.startDate)
                    objCalendarBL.getFaultAppointmentsCalendarDetail(objCalendarAppointmentsBO, dsAppointments)
                    'Creating Columns based on Engineers names
                    varWeeklyDT.Columns.Add(" ")
                    For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                        objCalendarUtilities.createColumns(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ":" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(1).ToString(), varWeeklyDT)
                        strEmployeeIds.Append(dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ",")
                    Next

                    'Getting Employees Leaves Information
                    objCalendarAppointmentsBO.LeaveStartDate = ApplicationConstants.startDate
                    objCalendarAppointmentsBO.ReturnDate = ApplicationConstants.endDate
                    objCalendarAppointmentsBO.EmpolyeeIds = strEmployeeIds.ToString().TrimEnd(",")
                    objCalendarBL.getEngineerLeavesDetail(objCalendarAppointmentsBO, dsEmployeeLeaves)
                    objCalendarBL.getEmployeeCoreWorkingHours(dsEmployeeCoreWorkingHours, strEmployeeIds.ToString().TrimEnd(","))
                    'End Getting Employees Leaves Information

                    'Getting Appointments Information in Datarow for Data table {varWeeklyDT}
                    Dim scheduledAppt(varWeeklyDT.Columns.Count - 1) As String
                    'Calculating Distance via Google API
                    dsAppointments.Tables(0).Columns.Add("Distance")
                    For drdsAppointments = 0 To dsAppointments.Tables(0).Rows.Count - 1
                        dsAppointments.Tables(0).Rows(drdsAppointments).Item("Distance") = 0 'objCalendarUtilities.CalculateDistance(objcustomerData.PostCode, dsAppointments.Tables(0).Rows(drdsAppointments).Item(6).ToString())
                    Next
                    'Drawing Weekly Calendar
                    For dayCount = 0 To 6
                        scheduledAppt(0) = DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("dd") + " " + ApplicationConstants.daysArray(dayCount)
                        rowCount = 1
                        For dsColCount = 0 To dsEmployeeList.Tables(0).Rows.Count - 1
                            'Adding Leaves Information
                            drLeaves = dsEmployeeLeaves.Tables(0).Select("(StartDate<='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EndDate >='" + DateAdd("d", dayCount, ApplicationConstants.startDate).ToShortDateString() + "' AND EMPLOYEEID=" + dsEmployeeList.Tables(0).Rows(dsColCount).Item(0).ToString() + ")")
                            If (drLeaves.Count > 0) Then
                                For leaveCounter As Integer = 0 To drLeaves.Count - 1
                                    strAppointment.Append("<div class='detailBoxSmall' style='background-color:#FFF;padding:4px'><table><tr><td><img src='../../images/arrow_right.png'></td><td><span style='margin-right:10px;'>" + drLeaves(leaveCounter).Item(1).ToString() + "</span></td></tr></table></div><br/>")
                                Next
                            End If
                            For rdCount = 0 To dsAppointments.Tables(0).Rows.Count - 1
                                'Lines added by Abdul Wahhab for joint tenancy change - 08/07/2013 - START
                                Dim tenancy As Integer = 0
                                If (dsAppointments.Tables(0).Rows(rdCount).Item(13).ToString() <> "") Then
                                    tenancy = 1
                                End If
                                'Lines added by Abdul Wahhab for joint tenancy change - 08/07/2013 - END
                                'Check the appointments of the engineers in Datarow
                                Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString())
                                Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(15).ToString())
                                Dim currentDate As DateTime = DateAdd("d", dayCount, ApplicationConstants.startDate)
                                Dim operativeId As Integer = dsEmployeeList.Tables(0).Rows(dsColCount).Item(0)
                                Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + operativeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")

                                If (currentDate >= aptStartDate And currentDate <= aptEndDate _
                                And dsEmployeeList.Tables(0).Rows(dsColCount).Item(0) = dsAppointments.Tables(0).Rows(rdCount).Item(1) _
                                And dr.Count > 0) Then
                                    If (SessionManager.getSbAppointmentId() > 0 And SessionManager.getSbAppointmentId() = dsAppointments.Tables(0).Rows(rdCount).Item(8)) Then
                                        strAppointment.Append("<div class='main-box-current'><div class='box-cover'><div class='box-column1'><a href='#' id='arrow-left" + divCounter.ToString() + "' onclick='displayMore(" + divCounter.ToString() + "," + tenancy.ToString() + ");return false;'> <img src='../../images/arrow_right.png' alt='' border=0 /></a>")
                                    Else
                                        strAppointment.Append("<div class='main-box'><div class='box-cover'><div class='box-column1'><a href='#' id='arrow-left" + divCounter.ToString() + "' onclick='displayMore(" + divCounter.ToString() + "," + tenancy.ToString() + ");return false;'> <img src='../../images/arrow_right.png' alt='' border=0 /></a>")
                                    End If
                                    strAppointment.Append("<a href='#' id='arrow-down" + divCounter.ToString() + "' style='display:none;' onclick='displayMoreHide(" + divCounter.ToString() + ");return false;'> <img src='../../images/arrow_down.png' alt='' border=0 /></a></div><div class='box-column2'>" + getAppointmentStartEndTime(dsAppointments, dsEmployeeCoreWorkingHours, currentDate, rdCount, operativeId) + "</div>")
                                    'Adding Status Icons
                                    If dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "In Progress" Then
                                        strAppointment.Append("<div class='box-column3'><img src='../../images/inprogress.png'></div>")
                                    ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Paused" Then
                                        strAppointment.Append("<div class='box-column3'><img src='../../images/paused.png'></div>")
                                    ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Appointment Arranged" Or dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Arranged" Then
                                        strAppointment.Append("<div class='box-column3'><img src='../../images/arranged.png'></div>")
                                    ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "Complete" Then
                                        strAppointment.Append("<div class='box-column3'><img src='../../images/completed.png'></div>")
                                    ElseIf dsAppointments.Tables(0).Rows(rdCount).Item(7).ToString() = "No Entry" Then
                                        strAppointment.Append("<div class='box-column3'><img src='../../images/completed.png'></div>")
                                    End If

                                    ' Checking the appointment was scheduled from Calendar or not 
                                    ' If appointment was scheduled from calendar then show icon.
                                    Dim isCalendarApt As Boolean = False
                                    If (Not IsDBNull(dsAppointments.Tables(0).Rows(rdCount)("IsCalendarAppointment"))) Then
                                        isCalendarApt = dsAppointments.Tables(0).Rows(rdCount)("IsCalendarAppointment")
                                    End If

                                    If (isCalendarApt) Then
                                        strAppointment.Append("<div class='box-column3'><img src='../../Images/calendarFlag.png'></div></div>")
                                    Else
                                        strAppointment.Append("</div>")
                                    End If

                                    'Adding Address for collapsed view
                                    If (dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() <> "") Then
                                        strAppointment.Append("<div id='addressDiv" + divCounter.ToString() + "' style='display:block;'><div class='box2-column2' style='padding:5px 5px 5px 35px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() + "</div></div>")
                                        'strAppointment.Append("<div class='box-cover-des-inner-box' style="display:block;"><div class='box2-column1'></div><div class='box2-column2'>" + dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() + "</div></div>")
                                    End If
                                    'Adding cell #
                                    If (dsAppointments.Tables(0).Rows(rdCount).Item(13).ToString() <> "") Then
                                        strAppointment.Append("<div id='display" + divCounter.ToString() + "' style='display:none;'><div class='box-cover2'><div class='box2-column1'><a href='javascript:void(0)' onclick='openPopUp(" + dsAppointments.Tables(0).Rows(rdCount).Item(13).ToString() + ");return false;' ><img src='../../images/phone.png' alt='' /></a></div><div class='box2-column2'>" + dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() + " </div></div>")
                                    Else
                                        strAppointment.Append("<div id='display" + divCounter.ToString() + "' style='display:none;'>")
                                    End If
                                    'Adding Customer Name

                                    'If (dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() <> "") Then
                                    '    strAppointment.Append("<div class='box-cover-des'><div class='box2-column1'></div><div class='box2-column2'>" + dsAppointments.Tables(0).Rows(rdCount).Item(3).ToString() + "</div></div>")
                                    'End If

                                    'Adding Address
                                    'If (dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() <> "") Then
                                    '    strAppointment.Append("<div class='box2-column2' style='padding:5px 5px 5px 35px;'>" + dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() + "</div>")
                                    'strAppointment.Append("<div class='box-cover-des-inner-box' style="display:block;"><div class='box2-column1'></div><div class='box2-column2'>" + dsAppointments.Tables(0).Rows(rdCount).Item(5).ToString() + "</div></div>")
                                    'End If


                                    'Adding Job Sheet Numbers
                                    drAppointmentJSN = dsAppointments.Tables(1).Select("AppointmentId=" + dsAppointments.Tables(0).Rows(rdCount).Item(8).ToString())
                                    If (drAppointmentJSN.Count > 0) Then
                                        strAppointment.Append("<div class='inner-box'><div class='box-cover-des-inner-box'>")
                                        For i = 0 To drAppointmentJSN.Count - 1
                                            Dim jsnLink As String = String.Empty
                                            If drAppointmentJSN(i).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.SbFaultAppointment Then
                                                jsnLink = "SbSchedulingCalendar.aspx?isSb=yes&jsn=" + drAppointmentJSN(i).Item(2).ToString()
                                            ElseIf drAppointmentJSN(i).Item(ApplicationConstants.AppointmentTypeColumn).ToString() = ApplicationConstants.FaultAppointment Then
                                                jsnLink = "SbSchedulingCalendar.aspx?jsn=" + drAppointmentJSN(i).Item(2).ToString()
                                            Else
                                                jsnLink = ApplicationConstants.NumberSymbol
                                            End If

                                            Dim appointmentType As String = drAppointmentJSN(i).Item(ApplicationConstants.AppointmentTypeColumn).ToString() + " Appointment"

                                            If (SessionManager.getSbAppointmentId() > 0 And SessionManager.getSbAppointmentId() = dsAppointments.Tables(0).Rows(rdCount).Item(8)) Then
                                                strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'><a title='" + appointmentType + "' href='" + jsnLink + "' style='color:white;'><u>" + drAppointmentJSN(i).Item(2).ToString() + "</u></a></div>")
                                            Else
                                                strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'><a title='" + appointmentType + "' href='" + jsnLink + "'><u>" + drAppointmentJSN(i).Item(2).ToString() + "</u></a></div>")
                                            End If
                                            strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>" + drAppointmentJSN(i).Item(3).ToString() + "</div>")
                                        Next
                                        strAppointment.Append("</div></div>")
                                    End If
                                    'Adding Appointment Distance
                                    'strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>Distance &nbsp;" + dsAppointments.Tables(0).Rows(rdCount).Item(14).ToString() + " m</div>")
                                    'Adding Appointment Next Distance
                                    'If (rdCount + 1 < dsAppointments.Tables(0).Rows.Count) Then
                                    '    'strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>Next &nbsp;" + objCalendarUtilities.CalculateDistance(dsAppointments.Tables(0).Rows(rdCount).Item(6).ToString(), dsAppointments.Tables(0).Rows(rdCount + 1).Item(6).ToString()) + " m</div></div></div></div><br/>")
                                    '    strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>Next &nbsp; 0 m</div>")
                                    'Else
                                    '    strAppointment.Append("<div class='box2-column1'></div><div class='box2-column2'>Next &nbsp; 0 m</div>")
                                    'End If

                                    'Closing the div contain job sheet numbers.
                                    strAppointment.Append("</div>")

                                    'Closing Main box and adding a line break to seprate it from next appointment of day.
                                    strAppointment.Append("</div><br/>")

                                    strAppointment.Append("<div style='clear:both'></div>")
                                    divCounter = divCounter + 1
                                End If

                            Next
                            'End If
                            If (strAppointment.ToString() <> "") Then
                                ' strAppointment.Append(DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy"))
                                scheduledAppt(rowCount) = strAppointment.ToString()
                                strAppointment.Clear()
                            Else
                                scheduledAppt(rowCount) = "" 'DateAdd("d", dayCount, ApplicationConstants.startDate).ToString("MM/dd/yyyy") '""
                            End If
                            rowCount = rowCount + 1
                        Next

                        varWeeklyDT.Rows.Add(scheduledAppt)

                    Next
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EngNotFound, True)
                End If
                If (SessionManager.getSbAppointmentId() > 0) Then
                    Me.showHideButtons(True) 'Enabling screen 11b,14 buttons
                End If
            End If
            lblMonth.Text = ApplicationConstants.startDate.Day.ToString() + "-" + DateAdd("d", 6, ApplicationConstants.startDate).Day.ToString() + " " + MonthName(ApplicationConstants.startDate.Month) + " " + Year(ApplicationConstants.startDate).ToString()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'lbNextFault.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Get Appointment Start and End Time"

    Function getAppointmentStartEndTime(ByVal dsAppointments As DataSet, ByVal dsEmployeeCoreWorkingHours As DataSet, ByVal currentDate As DateTime, ByVal rdCount As Integer, ByVal employeeId As Integer)

        Dim aptStartDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(0).ToString())
        Dim aptEndDate As DateTime = System.DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(15).ToString())
        Dim aptStartTime As String = String.Empty
        Dim aptEndTime As String = String.Empty
        Dim empCoreStartTime As String = String.Empty
        Dim empCoreEndTime As String = String.Empty
        Dim dayStartHour As Double = GeneralHelper.getDayStartHour()
        Dim dayEndHour As Double = GeneralHelper.getDayEndHour()
        Dim ofcCoreStartTime As String = DateTime.Parse(Convert.ToInt32(dayStartHour - dayStartHour Mod 1).ToString() + ":" + IIf(dayStartHour Mod 1 = 0, "00", Convert.ToInt32((dayStartHour Mod 1) * 60).ToString())).ToString("HH:mm")
        Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + IIf(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString())).ToString("HH:mm")

        'Get employee core working hour
        Dim dr() As DataRow = dsEmployeeCoreWorkingHours.Tables(0).Select("EmployeeId=" + employeeId.ToString() + " And WeekDayName= '" + currentDate.ToString("dddd") + "'")
        If (dr.Count > 0) Then
            empCoreStartTime = DateTime.Parse(dr(0).Item(3).ToString()).ToString("HH:mm")
            empCoreEndTime = DateTime.Parse(dr(0).Item(4).ToString()).ToString("HH:mm")
        Else
            empCoreStartTime = ofcCoreStartTime
            empCoreEndTime = ofcCoreEndTime
        End If

        'Setting start and end time depending on cases
        'Case 1 : Single day appointment start time and end time exist in same day
        'Case 2 : Multiple day appointment where start time exist in current date
        'Case 3 : Multiple day appointment where end time exist in current date
        'Case 4 : Multiple day appointment where start and end time do not exist in current date

        If (aptStartDate = aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(2)).ToString("HH:mm")
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        ElseIf (currentDate = aptStartDate And currentDate <> aptEndDate) Then
            aptStartTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(2)).ToString("HH:mm")
            aptEndTime = empCoreEndTime
        ElseIf (currentDate <> aptStartDate And currentDate = aptEndDate) Then
            aptStartTime = empCoreStartTime
            aptEndTime = DateTime.Parse(dsAppointments.Tables(0).Rows(rdCount).Item(9)).ToString("HH:mm")
        Else
            aptStartTime = empCoreStartTime
            aptEndTime = empCoreEndTime
        End If

        Return aptStartTime + " - " + aptEndTime

    End Function

#End Region

#Region "Open Appointment Popup"
    Private Sub OpenAppointmentPopup()
        Try
            lblOperative.Text = Request.QueryString("opr")
          
            lblLocation.Text = commonAddressBo.Address
            btnSaveAppointment.Enabled = True
            If (txtStartDate.Text = "") Then
                txtStartDate.Text = Today.ToString("dd/MM/yyyy")
            End If
            If (txtEndDate.Text = "") Then
                txtEndDate.Text = txtStartDate.Text
            End If
            If (ddlAppStartTime.Items.Count <= 0 Or ddlAppEndTime.Items.Count <= 0) Then
                Me.PopulateAppointmentDropdown()
            End If
            SessionManager.setisSuccessAdded(False)
            Me.mdlPopUpAddAppointment.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblActivityPopupMessage, pnlActivityPopupMessage, uiMessageHelper.message, True)
            End If

        End Try
    End Sub

#End Region

#Region "Push appointment confirmed"

    Public Function pushAppointmentConfirmed(ByVal appointmentId As Integer, ByVal type As Integer) As Boolean
        Dim URL As String = String.Format("{0}?appointmentId={1}&type={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentId, type)
        Dim request As WebRequest = Net.WebRequest.Create(URL)
        request.Credentials = CredentialCache.DefaultCredentials
        Dim response As WebResponse = request.GetResponse()
        Dim dataStream As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()
        reader.Close()
        response.Close()

        If (responseFromServer.ToString.Equals("true")) Then
            Return True
        Else
            Return False
        End If

    End Function

#End Region

#Region "Save Appointment Information"

    Private Sub saveAppointmentInformation()
        Dim appointmentTypePushNotification As Integer
        Try

            Dim appointmentData As AppointmentBO = SessionManager.getAppointmentDataForAppointmentSummary()
            If faultsListString.Length > 0 Then
                Dim objappointmentSummaryBO As New AppointmentSummaryBO()
                objappointmentSummaryBO.OperativeId = Request.QueryString("id")
                objappointmentSummaryBO.AppointmentStartDate = txtStartDate.Text
                objappointmentSummaryBO.AppointmentEndDate = txtEndDate.Text
                objappointmentSummaryBO.FaultNotes = txtFaultNotes.Text.Trim()
                objappointmentSummaryBO.FaultsList = faultsListString.ToString()
                objappointmentSummaryBO.Time = ddlAppStartTime.SelectedItem.Value
                objappointmentSummaryBO.EndTime = ddlAppEndTime.SelectedItem.Value
                objappointmentSummaryBO.AppointmentEndDate = txtEndDate.Text
                objappointmentSummaryBO.Schemeid = SessionManager.getSchemeId()
                objappointmentSummaryBO.Blockid = SessionManager.getBlockId()
                objappointmentSummaryBO.IsRecall = isRecall
                objappointmentSummaryBO.IsCalendarAppointment = True
                objappointmentSummaryBO.UserId = SessionManager.getUserEmployeeId()
                objappointmentSummaryBO.AppointmentNotes = txtFaultNotes.Text.Trim

                If (validateNewAppointment()) Then
                    If (SessionManager.getIsSbAppointmentRearrange() = True) Then 'Rearrange Appointment
                        Dim appointmentBO As New AppointmentBO()
                        appointmentBO.OperativeId = objappointmentSummaryBO.OperativeId
                        appointmentBO.AppointmentStartDate = objappointmentSummaryBO.AppointmentStartDate
                        appointmentBO.AppointmentEndDate = objappointmentSummaryBO.AppointmentEndDate
                        appointmentBO.StartTime = objappointmentSummaryBO.Time
                        appointmentBO.EndTime = objappointmentSummaryBO.EndTime
                        appointmentBO.FaultNotes = objappointmentSummaryBO.FaultNotes
                        appointmentBO.IsCalendarAppointment = objappointmentSummaryBO.IsCalendarAppointment
                        appointmentId = objFaultBL.updateFaultInfoRearrange(appointmentBO, faultsListString.ToString)

                        appointmentTypePushNotification = ApplicationConstants.RearrangeAppointmentType
                    Else 'Creating New Appointment
                        appointmentId = objCalendarBL.saveSbAppointmentInformation(objappointmentSummaryBO)
                        appointmentTypePushNotification = ApplicationConstants.NewAppointmentType
                    End If

                    If (Not IsDBNull(appointmentId) And appointmentId > 0) Then
                        Me.setAppointmentInfo()
                        If (objappointmentSummaryBO.AppointmentStartDate = Today) Then
                            If (Me.pushAppointmentConfirmed(appointmentId, appointmentTypePushNotification)) Then
                                'Display successfully message
                                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.SaveAppointmentSuccessfully, False)
                            Else
                                'Display successfully message but push notification not send.
                                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.SaveAppointmentSuccessfullyNotificationFailed, False)
                            End If

                        End If
                    End If

                End If
            End If 'Validation
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Set Values After Saving/Updating Appointment"

    Private Sub setAppointmentInfo()
        Me.setNewlyCreatedAppointmentDetail()
        SessionManager.setSbAppointmentId(appointmentId)
        SessionManager.setPopupOpened(True)
        btnSaveAppointment.Enabled = False
        btnCancelAppointment.Enabled = False
        SessionManager.setIsSbAppointmentRearrange(True)
        If isRecall = True Then
            SessionManager.setIsRecall(False)
        End If
    End Sub

#End Region

#Region "View Job Sheet detail move to screen 13"

    Protected Sub ViewFault(ByVal jobSheetNumber As String)
        ' Dim jobSheetNumber As String = CType(CType(sender, Button).CommandArgument, String)
        lblFaultIdHidden.Text = jobSheetNumber
        Try
            lblFaultIdHidden.Text = jobSheetNumber
            If Not IsNothing(Request.QueryString(PathConstants.isSb)) AndAlso Request.QueryString(PathConstants.isSb) = PathConstants.Yes Then
                ifrmJobSheet.Attributes("src") = GeneralHelper.getSbJobSheetSummaryAddress() + jobSheetNumber
            Else
                ifrmJobSheet.Attributes("src") = GeneralHelper.getJobSheetSummaryAddress() + jobSheetNumber
            End If

            popupJobSheet.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

        popupJobSheet.Show()
    End Sub
#End Region

#Region "set Newly Created AppointmentDetail"
    Private Sub setNewlyCreatedAppointmentDetail()
        dsAppointments = New DataSet()
        objCalendarAppointmentsBO.AppointmentId = appointmentId
        objCalendarBL.getNewlyCreatedAppointmentDetail(objCalendarAppointmentsBO, dsAppointments)
        Me.setSbConfirmedFaultsInSession(dsAppointments.Tables(0))
    End Sub
#End Region

#Region "set Confirmed Faults In Session"
    Private Sub setSbConfirmedFaultsInSession(ByRef confirmedFaults As DataTable)
        SessionManager.setSbConfirmedFaults(confirmedFaults)
    End Sub
#End Region

#Region "Validate New Appointment Inputs"
    Protected Function validateNewAppointment()
        Dim success As Boolean = True
        If IsNothing(Me.txtStartDate.Text) Or Me.txtStartDate.Text = String.Empty Then
            success = False
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.SelectActionDate, True)

        ElseIf System.DateTime.Parse(Me.ddlAppEndTime.SelectedItem.Value).TimeOfDay <= System.DateTime.Parse(Me.ddlAppStartTime.SelectedItem.Value).TimeOfDay Then
            success = False
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.AppointmentTime, True)

        ElseIf Me.valideStartEndTime() > 0 OrElse Not isTimeSlotAvailable() Then
            'TODO: Need to dispaly seperate messages for existing appointment and operative leave.
            success = False
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.operativeNotAvailable, True)

        ElseIf Me.checkLeaveExist() Then
            success = False
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.operativeNotAvailable, True)
        End If
        Return success
    End Function
#End Region

#Region "Check leave exist"
    Protected Function checkLeaveExist()
        Dim objFaultAppointmentBL As FaultAppointmentBL = New FaultAppointmentBL()
        Dim operativeId As Integer = Convert.ToInt32(Request.QueryString("id"))
        Dim leaveExist As Boolean = False

        Dim appointmentStartDateTime As Date = Date.Parse(txtStartDate.Text + " " + ddlAppStartTime.SelectedValue)
        Dim appointmentEndDateTime As Date = Date.Parse(txtStartDate.Text + " " + ddlAppEndTime.SelectedValue)

        If (dsEmployeeLeaves.Tables(0).Rows.Count > 0) Then
            Dim leaveResult As EnumerableRowCollection(Of DataRow) = objFaultAppointmentBL.isLeaveExist(dsEmployeeLeaves.Tables(0), operativeId, appointmentStartDateTime, appointmentEndDateTime)
            'if leave exist get the new date time but still its possiblity that leave ''ll exist in the next date time so 'll also be checked again.
            If leaveResult.Count() > 0 Then
                leaveExist = True
            End If
        End If

        Return leaveExist
    End Function
#End Region

#Region "Show/Hide Screen 11b,14 buttons"
    Private Sub showHideButtons(ByVal showHide As Boolean)
        Me.btnRearrange.Visible = showHide
        Me.btnCancel.Visible = showHide
    End Sub

#End Region

#Region "Save Drop down state"
    Sub saveDropdownsState()
        Try
            SessionManager.setDDLData(ddlPatches.SelectedValue + "," + ddlTrades.SelectedValue)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Restore Drop down state"
    Sub restoreDropdownsState()
        Dim ddlArry(2) As String
        Dim patchId As Integer
        Dim tradeId As Integer

        Try
            ddlArry = SessionManager.getDDLData().Split(",")
            patchId = Convert.ToInt32(ddlArry(0))
            tradeId = Convert.ToInt32(ddlArry(1))

            Me.getPatches(ddlPatches)
            Me.getTrades(ddlTrades)

            ddlPatches.SelectedValue = patchId
            ddlTrades.SelectedValue = tradeId

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Populate Appointment Start/End Dropdowns"
    Private Sub PopulateAppointmentDropdown()
        Dim newListItem As ListItem

        For i = 0 To 95
            newListItem = New ListItem(ApplicationConstants.appointmentTime(i), ApplicationConstants.appointmentTime(i))
            ddlAppStartTime.Items.Add(newListItem)
            ddlAppEndTime.Items.Add(newListItem)
        Next
        newListItem = New ListItem("23:59", "23:59")
        ddlAppEndTime.Items.Add(newListItem)
        ddlAppEndTime.SelectedIndex = ddlAppStartTime.SelectedIndex
    End Sub
#End Region

#Region "Validate Appointment Start/End Time"
    Protected Function valideStartEndTime() As Integer
        Dim startDate As New DateTime(1970, 1, 1)
        Dim startTimeInSec, endTimeInSec As Integer
        startTimeInSec = (System.DateTime.Parse(txtStartDate.Text + " " + ddlAppStartTime.SelectedItem.Value) - startDate).TotalSeconds
        endTimeInSec = (System.DateTime.Parse(txtEndDate.Text + " " + ddlAppEndTime.SelectedItem.Value) - startDate).TotalSeconds
        Dim strStartFilter As String = "AppointmentDate= '" + System.DateTime.Parse(txtStartDate.Text) + "' and OperativeID=" + Request.QueryString("id") + " and (startTimeInSec<=" + startTimeInSec.ToString() + " and endTimeInSec>=" + startTimeInSec.ToString() + ")"
        Dim strendFilter As String = "AppointmentDate= '" + System.DateTime.Parse(txtStartDate.Text) + "' and OperativeID=" + Request.QueryString("id") + " and (startTimeInSec>=" + startTimeInSec.ToString() + " and endTimeInSec<=" + endTimeInSec.ToString() + ")"
        Return dsAppointments.Tables(0).Select(strStartFilter, "startTimeInSec ASC").Count + dsAppointments.Tables(0).Select(strendFilter, "startTimeInSec ASC").Count
    End Function
#End Region

#Region "img btn Aptba TenantInfo Click"

    Protected Sub showCustomerInfo(ByVal tenancy As String)
        Try
            Dim tenancyID As Integer = CType(tenancy, Integer)

            Dim dstenantsInfo As New DataSet()

            objCalendarBL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)

            If (dstenantsInfo.Tables.Count > 0 AndAlso dstenantsInfo.Tables(0).Rows.Count > 0) Then
                setTenantsInfo(dstenantsInfo, tblTenantInfo)
                Me.mdlPopUpAppointmentToBeArrangedPhone.Show()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.noTenantInformationFound, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub
#End Region

#Region "Set Tenant(s) Info by TenancyId - Joint Tenancy"

    Private Sub setTenantsInfo(ByRef dsTenantsInfo As DataSet, ByRef tblTenantInfo As HtmlTable)
        If dsTenantsInfo.Tables.Count > 0 AndAlso dsTenantsInfo.Tables(0).Rows.Count > 0 Then

            While (tblTenantInfo.Rows.Count > 1)
                tblTenantInfo.Rows.RemoveAt(1)
            End While

            With dsTenantsInfo.Tables(0)

                For counter As Integer = 0 To .Rows.Count - 1

                    'Add Tenant's name to table

                    Dim newRowTenantName As New HtmlTableRow()

                    Dim newCellTenantLabel As New HtmlTableCell()
                    If .Rows.Count > 1 Then
                        newCellTenantLabel.InnerText = "Tenant" & (counter + 1).ToString() & ":"
                    Else
                        newCellTenantLabel.InnerText = "Tenant:"
                    End If

                    newRowTenantName.Cells.Add(newCellTenantLabel)

                    Dim newCellTenantName As New HtmlTableCell()
                    newCellTenantName.InnerText = .Rows(counter)("CustomerName")
                    newRowTenantName.Cells.Add(newCellTenantName)

                    tblTenantInfo.Rows.Add(newRowTenantName)

                    'Add Tenant's Mobile to table

                    Dim newRowTenantMobile As New HtmlTableRow()

                    Dim newCellTenantMobileLabel As New HtmlTableCell()
                    newCellTenantMobileLabel.InnerText = "Mobile:"
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel)

                    Dim newCellTenantMobileNo As New HtmlTableCell()
                    newCellTenantMobileNo.InnerText = .Rows(counter)("MOBILE")
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo)

                    tblTenantInfo.Rows.Add(newRowTenantMobile)

                    'Add tenant's Telephone to table

                    Dim newRowTenantTel As New HtmlTableRow()

                    Dim newCellTenantTelLabel As New HtmlTableCell()
                    newCellTenantTelLabel.InnerText = "Telephone:"
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel)

                    Dim newCellTenantTelNo As New HtmlTableCell()
                    newCellTenantTelNo.InnerText = .Rows(counter)("TEL")
                    newRowTenantTel.Cells.Add(newCellTenantTelNo)

                    tblTenantInfo.Rows.Add(newRowTenantTel)

                    'Add tenant's Email to table

                    Dim newRowTenantEmail As New HtmlTableRow()

                    Dim newCellTenantEmailLabel As New HtmlTableCell()
                    newCellTenantEmailLabel.InnerText = "Email:"
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel)

                    Dim newCellTenantEmail As New HtmlTableCell()
                    newCellTenantEmail.InnerText = .Rows(counter)("EMAIL")
                    newRowTenantEmail.Cells.Add(newCellTenantEmail)

                    tblTenantInfo.Rows.Add(newRowTenantEmail)

                    'Add a row seprator to the tenants
                    If counter < .Rows.Count - 1 Then
                        Dim newRowseprator As New HtmlTableRow()

                        Dim sepratorCell As New HtmlTableCell()
                        sepratorCell.ColSpan = 2
                        sepratorCell.InnerHtml = "<hr style=""width:98%; text-align:center"" />"
                        newRowseprator.Cells.Add(sepratorCell)

                        tblTenantInfo.Rows.Add(newRowseprator)
                    End If

                Next
            End With

        End If
    End Sub

#End Region

#Region "Function to get query string values and save in form level variables"

    Private Sub getQueryStringValues()
        'Get the name of last page/screen from which this page/screen is called
        If Not IsNothing(Request.QueryString(PathConstants.Src)) Then
            srcQueryString = CType(Request.QueryString(PathConstants.Src), String)
        End If
    End Sub

#End Region

#Region "Set the drop down for the current appointment to show appropriate calendar view"

    Private Sub setDropDownsforCurrentAppointment(ByVal currentAppointmentID As Integer)

        Dim calendarAppointmentsBO As New CalendarAppointmentsBO

        If ((SessionManager.getSbPatchId() <= 0 OrElse SessionManager.getSbTradeId() <= 0) AndAlso currentAppointmentID > 0) Then
            objCalendarBL.getPidTidbyAppointmentID(currentAppointmentID, calendarAppointmentsBO)
        End If

        'If (ddlPatches.Items.Count > 0 AndAlso calendarAppointmentsBO.PatchId > 0) Then
        If (ddlPatches.Items.Count > 0 AndAlso SessionManager.getSbPatchId() > 0) Then
            ddlPatches.SelectedIndex = ddlPatches.Items.IndexOf(ddlPatches.Items.FindByValue(SessionManager.getSbPatchId()))
        ElseIf (ddlPatches.Items.Count > 0 AndAlso calendarAppointmentsBO.PatchId > 0) Then
            ddlPatches.SelectedIndex = ddlPatches.Items.IndexOf(ddlPatches.Items.FindByValue(calendarAppointmentsBO.PatchId))
        End If

        If (ddlTrades.Items.Count > 0 AndAlso SessionManager.getSbTradeId()) Then
            ddlTrades.SelectedIndex = ddlTrades.Items.IndexOf(ddlTrades.Items.FindByValue(SessionManager.getSbTradeId()))
        ElseIf (ddlTrades.Items.Count > 0 AndAlso calendarAppointmentsBO.TradeId > 0) Then
            ddlTrades.SelectedIndex = ddlTrades.Items.IndexOf(ddlTrades.Items.FindByValue(calendarAppointmentsBO.TradeId))
        End If

    End Sub

#End Region

#Region "is Time Slot Available"

    ''' <summary>
    ''' Check from database that a time slot is available, check for existing appointment and check for existing leaves.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function isTimeSlotAvailable() As Boolean
        Dim appointmentBl As New FaultAppointmentBL
        Dim operativeId = Request.QueryString("id")
        Dim startDateTime As Date = Date.Parse(txtStartDate.Text + " " + ddlAppStartTime.SelectedItem.Value)
        Dim endDateTime As Date = Date.Parse(txtEndDate.Text + " " + ddlAppEndTime.SelectedItem.Value)
        isTimeSlotAvailable = appointmentBl.isTimeSlotAvailable(operativeId, startDateTime, endDateTime)
    End Function

#End Region

#End Region

#Region "Events"
    Protected Sub btnSaveAppointment_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAppointment.Click
        saveAppointmentInformation()
    End Sub

    Protected Sub btnCancelAppointment_Click(ByVal sender As Object, ByVal e As EventArgs)
        mdlPopUpAddAppointment.Hide()
        Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar
    End Sub
    Protected Sub imgBtnCloseAddAppointment_Click(ByVal sender As Object, ByVal e As EventArgs)
        mdlPopUpAddAppointment.Hide()
        Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar
    End Sub
    Protected Sub btnRearrange_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRearrange.Click
        SessionManager.setIsSbAppointmentRearrange(True)
        Response.Redirect(PathConstants.ReArrangingSbFault + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal))


    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        If appointmentId = 0 Then
            If (Not IsNothing(Request.QueryString(PathConstants.AppointmentId))) Then
                Response.Redirect(PathConstants.CancellationSbAppointmentSummary + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal) + "&" + PathConstants.AppointmentId & "=" + Request.QueryString(PathConstants.AppointmentId))
            End If
        Else
            Response.Redirect(PathConstants.CancellationSbAppointmentSummary + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal) + "&" + PathConstants.AppointmentId & "=" + appointmentId.ToString())
        End If

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'SessionManager.setAppointmentId(appointmentId)
        Try
            Dim source As String = Request.QueryString(PathConstants.Src)

            If source <> String.Empty Then
                SessionManager.setSFSCalendar(source)
            End If

            Me.btnCancelAppointment.PostBackUrl = PathConstants.SbScheduleCalender + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal)
            Me.imgBtnCloseAddAppointment.PostBackUrl = PathConstants.SbScheduleCalender + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal)

            'set post back url for if appointment is already saved go to screen 3
            If (SessionManager.getSbAppointmentId() > 0) Then
                Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.OtherSbAppointmentPath + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal))
            Else
                If (SessionManager.getSFSCalendar = "rearrangeAppointment") Then
                    'If user has not created appointment and is coming from screen 15
                    Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.ReArrangingSbFault + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal))
                Else
                    'set post back url for if user comes from screen 7
                    'if is fault is being recalled
                    If (Me.isRecall = True) Then
                        Dim recall As String = IIf(SessionManager.getIsRecall() = True, PathConstants.Yes, PathConstants.No)
                        Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal) + "&" + PathConstants.IsRecall + "=" + recall)
                    Else
                        'if is fault is not being recalled (if user comes from screen 7)
                        Me.setBackButtonPostBackUrl(Me.btnBack, PathConstants.SbAvailableAppointments + GeneralHelper.getSrc(PathConstants.SbScheduleCalendarSrcVal))
                    End If

                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    Protected Sub ddlPatches_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPatches.SelectedIndexChanged
        Me.saveDropdownsState()
        Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar
    End Sub

    Protected Sub ddlTrades_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTrades.SelectedIndexChanged
        Me.saveDropdownsState()
        Me.displayCalendarScheduleAppointment() 'Display Appointment Calendar
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        'Me.showCustomerInfo("667684")

        Me.displayCalendarScheduleAppointment()

        Dim argument = Request.Form("__EVENTARGUMENT")

        Me.showCustomerInfo(argument.ToString)
    End Sub

#End Region

End Class