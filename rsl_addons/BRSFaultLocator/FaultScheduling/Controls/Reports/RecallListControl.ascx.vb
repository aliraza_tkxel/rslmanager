﻿Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_Utilities

Public Class RecallListControl
    Inherits UserControlBase

    'TODO - Notice When you Implement btnView(Button) Click Event that is inside grdRecallList(Grid) - Start
    'Add This code to the Click Event Implementation

    '    objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

    '    If grdSubcontractorList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdSubcontractorList.PageCount Then
    '        grdSubcontractorList.VirtualItemCount = getVirtualItemCountViewState()
    '        grdSubcontractorList.PageIndex = objPageSortBo.PageNumber - 1
    '        grdSubcontractorList.DataSource = getResultDataSetViewState()
    '        grdSubcontractorList.DataBind()
    '    End If

    'TODO - Notice When you Implement btnView(Button) Click Event that is inside grdRecallList(Grid) - END

#Region "Delegates"

    Public Delegate Sub Filters_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

#End Region

#Region "Events"

    Public Event FiltersChanged As Filters_Changed

#End Region

#Region "Properties"

    Dim objReportsBl As ReportsBL = New ReportsBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 30)
    Dim totalCount As Integer = 0

    Public ReadOnly Property SchemeBlock As schemeBlockDropdown
        Get
            Return SchemeBlockDDL
        End Get
    End Property

#End Region

#Region "Events Handling"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Grid Recall List Page Index Changing Event"

    Protected Sub grdRecallList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdRecallList.PageIndexChanging

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdRecallList.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim operativeId As Integer = ViewState.Item(ViewStateConstants.OperativeId)
            Dim resultDataSet As DataSet = New DataSet()
            populateRecallList(resultDataSet, search, operativeId, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Recall List Sorting Event"

    Protected Sub grdRecallList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdRecallList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdRecallList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim operativeId As Integer = ViewState.Item(ViewStateConstants.OperativeId)
            Dim resultDataSet As DataSet = New DataSet()

            populateRecallList(resultDataSet, search, operativeId, False)

            If e.SortExpression = "Operative" Then

                Dim dt As DataTable = New DataTable()
                dt = resultDataSet.Tables(0)

                If IsNothing(dt) = False Then
                    Dim dvSortedView As DataView = New DataView(dt)
                    dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
                    grdRecallList.DataSource = dvSortedView
                    grdRecallList.DataBind()
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdRecallList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Button View Reecall Detail"

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btn As Button = CType(sender, Button)
            Dim OriginalFaultLogId = CType(btn.CommandArgument, Integer)
            showRecallDetailPopupp(OriginalFaultLogId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessageRecallDetail, pnlMessageRecallDetail, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
            mdlPopupRecallDetail.Show()
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim operativeId As Integer = ViewState.Item(ViewStateConstants.OperativeId)
            Dim resultDataSet As DataSet = New DataSet()
            populateRecallList(resultDataSet, search, operativeId, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim operativeId As Integer = ViewState.Item(ViewStateConstants.OperativeId)
                Dim resultDataSet As DataSet = New DataSet()
                populateRecallList(resultDataSet, search, operativeId, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Scheme or Block Or Financial Year Changed"
    Private Sub Filters_FilterChanged(sender As Object, e As System.EventArgs) Handles SchemeBlockDDL.blockChanged, SchemeBlockDDL.schemeChanged _
        , ucFinancialYear.financialYearChanged
        RaiseEvent FiltersChanged(sender, e)
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"

    Sub bindToGrid()
        ViewState.Item(ViewStateConstants.Search) = ""
        ViewState.Item(ViewStateConstants.OperativeId) = -1
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdRecallList.VirtualItemCount = totalCount
        grdRecallList.DataSource = resultDataSet
        grdRecallList.DataBind()

        objPageSortBo = getPageSortBoViewState()

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdRecallList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdRecallList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Populate Recall List"

    Sub populateRecallList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal operativeId As Integer, ByVal setSession As Boolean)

        If setSession = True Then
            removePageSortBoViewState()
            getPageSortBoViewState()
        Else
            objPageSortBo = getPageSortBoViewState()
        End If

        ViewState.Add(ViewStateConstants.Search, search)
        ViewState.Add(ViewStateConstants.OperativeId, operativeId)

        totalCount = objReportsBl.getRecallList(resultDataSet, search, operativeId, objPageSortBo, SchemeBlockDDL.SchemeId, SchemeBlockDDL.BlockId, ucFinancialYear.FinancialYear)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        'If Not resultDataSet.Tables(0).Rows.Count = 0 Then

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            setPageSortBoViewState(objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        End If

        grdRecallList.VirtualItemCount = totalCount
        grdRecallList.DataSource = resultDataSet
        grdRecallList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdRecallList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdRecallList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If



    End Sub

#End Region

#Region "Show Recall Detail Popup"

    Private Sub showRecallDetailPopupp(ByRef OriginalFaultLogId As Integer)
        Dim resultDataSet As New DataSet
        'Add Tables to load from Data base.
        resultDataSet.Tables.Add(New DataTable(ApplicationConstants.PropertyDetailsDT))
        resultDataSet.Tables.Add(New DataTable(ApplicationConstants.OriginalFaultDetailsDT))
        resultDataSet.Tables.Add(New DataTable(ApplicationConstants.RecallFaultsDetailsDT))

        objReportsBl.getRecallDetailByFaultLogID(resultDataSet, OriginalFaultLogId)

        If resultDataSet.Tables.Count > 0 Then

            'Fill Header Details (Property/Customer) - Start
            If resultDataSet.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then
                With resultDataSet.Tables(ApplicationConstants.PropertyDetailsDT).Rows(0)
                    lblPropertyAddress.Text = .Item("Address")
                    lblPropertyCityHeader.Text = .Item("City")
                    lblPropertyTelHeader.Text = .Item("Tel")
                End With

            End If
            'Fill Header Details (Property/Customer) - End

            'Fill Original Fault Detail - Start
            If resultDataSet.Tables(ApplicationConstants.OriginalFaultDetailsDT).Rows.Count > 0 Then
                With resultDataSet.Tables(ApplicationConstants.OriginalFaultDetailsDT).Rows(0)
                    lblJSNOriginalFault.Text = .Item("JSN")
                    lblContractor.Text = .Item("Contractor")
                    lblOperative.Text = .Item("Operative")
                    lblPriority.Text = .Item("Priorty")
                    lblOrderDate.Text = .Item("OrderDate")
                    lblCompletion.Text = .Item("CompletionDate")
                    lblLocation.Text = .Item("Location")
                    lblDescription.Text = .Item("Description")
                    lblNotes.Text = .Item("Notes")

                    lblDateRepairDetailsOriginalFault.Text = .Item("RepairDate")
                    lblDescriptionRepairDetailOriginalFault.Text = .Item("RepairNotes")

                End With
            End If
            'Fill Original Fault Detail - End

            'Fill Recall Faults Details - Start

            If resultDataSet.Tables(ApplicationConstants.RecallFaultsDetailsDT).Rows.Count > 0 Then
                rptRecalls.DataSource = resultDataSet.Tables(ApplicationConstants.RecallFaultsDetailsDT)
                rptRecalls.DataBind()
            End If

            'Fill Recall Faults Details - End

        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.NoRecordFound
        End If

    End Sub

#End Region

#Region "Load Financial Years in Financial Years Drop down"
    Public Sub loadFinancialYears()
        ucFinancialYear.loadFinancialYears()
    End Sub
#End Region

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        Else
            pageSortBO = New PageSortBO("DESC", "Recorded", 1, 30)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

End Class