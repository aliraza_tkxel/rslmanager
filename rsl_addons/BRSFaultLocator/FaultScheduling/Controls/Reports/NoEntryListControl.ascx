﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NoEntryListControl.ascx.vb"
    Inherits="FaultScheduling.NoEntryListControl" %>
<%@ Register Src="~/Controls/Reports/schemeBlockDropdown.ascx" TagName="SchemeBlockDropdown"
    TagPrefix="ucSchemeBlockddl" %>
<%@ Register Src="~/Controls/Reports/FinancialYear.ascx" TagName="FinancialYear"
    TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<asp:Panel ID="pnlMessage1" runat="server" Visible="False">
    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
</asp:Panel>
<asp:Panel runat="server" ID="pnlSchemBlockddl">
    <div class="">
        <ucSchemeBlockddl:SchemeBlockDropdown ID="SchemeBlockDDL" runat="server"></ucSchemeBlockddl:SchemeBlockDropdown>
        <uc:FinancialYear ID="ucFinancialYear" runat="server" style="width: 95px;">
        </uc:FinancialYear>
    </div>
</asp:Panel>
<asp:HiddenField ID="hdnAppointmentType" runat="server" />
<div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
    <cc2:PagingGridView ID="grdNoEntryList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
        OnRowCreated="grdNoEntryList_RowCreated" 
        Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
        <Columns>
            <asp:TemplateField HeaderText="JSN" ItemStyle-CssClass="dashboard" SortExpression="JSN">
                <ItemTemplate>
                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle BorderStyle="None" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recorded" SortExpression="Recorded">
                <ItemTemplate>
                    <asp:Label ID="lblRecorded" runat="server" Text='<%# Bind("Recorded") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="95px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="160px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                <ItemTemplate>
                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Block" SortExpression="Block">
                <ItemTemplate>
                    <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location" SortExpression="Location">
                <ItemTemplate>
                    <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="115px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="190px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Trade" SortExpression="Trade">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Trade") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="85px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Response">
                <ItemTemplate>
                    <asp:Label ID="lblResponse" runat="server" Text='<%# Bind("Response") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="85px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Duration:" SortExpression="Duration">
                <ItemTemplate>
                    <asp:Label ID="lblDuration" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="btnSchedule" runat="server" CommandArgument='<%#Eval("JSN").ToString() + ";" + Eval("FaultLogID").ToString() + ";" + Eval("AppointmentType").ToString()%>'
                        Text="Schedule" OnClick="GetNoEntryDetail" UseSubmitBehavior="False" Enabled='<%# Not Eval("isNoEntryScheduled") %>'
                        CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Right" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc2:PagingGridView>
</div>

<%--Pager Template Start--%>
<asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
    <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
        <div class="paging-left">
            <span style="padding-right:10px;">
                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                    &lt;&lt;First
                </asp:LinkButton>
                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                    &lt;Prev
                </asp:LinkButton>
            </span>
            <span style="padding-right:10px;">
                <b>Page:</b>
                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                of
                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
            </span>
            <span style="padding-right:20px;">
                <b>Result:</b>
                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                to
                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                of
                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
            </span>
            <span style="padding-right:10px;">
                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                </asp:LinkButton>
                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                </asp:LinkButton>
            </span>
        </div>
        <div style="float: right;">
            <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
            <div class="field" style="margin-right: 10px;">
                <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
            </div>
            <span>
                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                    class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
            </span>
        </div>
    </div>
</asp:Panel>
<%--Pager Template End--%>
<!-- ModalPopupExtender -->
<asp:Button ID="btnHiddenEntry" runat="server" Text="" Style="display: none;" />
<cc1:ModalPopupExtender ID="popupNoEntry" runat="server" PopupControlID="pnlNoEntry"
    TargetControlID="btnHiddenEntry" CancelControlID="btnCloseNoEntryPopup" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlNoEntry" runat="server" CssClass="modalPopup">
    <div id="header-Popup">
        <p id="header_label">
            BHG Fault Locator
        </p>
        <div id="header_box">
        </div>
    </div>
    <br />
    <br />
    <br />
    <asp:Panel ID="pnlMessage" runat="server" Visible="False">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </asp:Panel>
    <div style="height: auto; overflow: auto; clear: both;">
        <table id="jobsheet_detail_table" style="width: 100%; margin-top: 10px;">
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblRecallDetailsFor" runat="server" Font-Bold="true" Text="Job sheet summary for : "
                        Style="float: left;"></asp:Label>
                    <asp:Panel runat="server" ID="pnlPropertyData" Style="float: left;">
                        <asp:Label ID="lblClientNameHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                        &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Font-Bold="true"
                            Text=""></asp:Label>
                        &nbsp;,
                        <asp:Label ID="lblClientCityHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                        &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Font-Bold="true" Text=" Tel :"> </asp:Label>
                        <asp:Label ID="lblClientTelPhoneNumberHeader" runat="server" Font-Bold="true" Text=""></asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlSchemeBlock" Visible="false" Style="float: left;">
                        <asp:Label ID="lblSchemeLabel" runat="server" Text="Scheme:" Font-Bold="true" ForeColor="LightGray"></asp:Label>
                        <asp:Label ID="lblSchemeName" runat="server" Text=" " Font-Bold="true"></asp:Label>
                        <asp:Label ID="lblBlockLabel" runat="server" Text="; Block:" Font-Bold="true" ForeColor="LightGray"></asp:Label>
                        &nbsp;<asp:Label ID="lblBlockName" runat="server" Text="" Font-Bold="true"></asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static" Font-Bold="true"></asp:Label>
                </td>
                <td>
                </td>
                <td rowspan="8">
                    <asp:Panel runat="server" ID="pnlPropertyDetail">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tel :
                                    <asp:Label ID="lblClientTelPhoneNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Mobile :
                                    <asp:Label ID="lblClientMobileNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email :
                                    <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    Contractor :
                </td>
                <td>
                    <asp:Label ID="lblContractor" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Operative :
                </td>
                <td>
                    <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Priority :
                </td>
                <td>
                    <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Completion Due :
                </td>
                <td>
                    <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Order Date :
                </td>
                <td>
                    <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Location :
                </td>
                <td>
                    <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Description :
                </td>
                <td>
                    <asp:Label ID="lblFaultDescription" runat="server" Font-Bold="true" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Trade :
                </td>
                <td>
                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Note :
                </td>
                <td>
                    <asp:TextBox ID="lblFaultOperatorNote" runat="server" CssClass="roundcornerby5" Enabled="false"
                        Height="65px" TextMode="MultiLine" Width="248px"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <b>No Entry details:</b>
                </td>
                <td>
                    <asp:Label ID="lblTime" runat="server" Font-Bold="true" Text=""></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Label ID="lblNoEntryNote" runat="server" Text=""></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 100%; text-align: left; clear: both;">
        <div class="text_a_r_padd__r_20px">
            <asp:Button ID="btnCloseNoEntryPopup" runat="server" Text="Close" />
            <asp:Button ID="btnScheduleAppointment" runat="server" CssClass="margin_right20"
                OnClick="ScheduleAppointment" Text="Schedule Appointment" />
        </div>
    </div>
    <br>
</asp:Panel>
<!-- ModalPopupExtender -->
