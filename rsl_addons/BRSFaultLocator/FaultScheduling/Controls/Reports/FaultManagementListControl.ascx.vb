﻿Imports FLS_BusinessLogic
Imports FLS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_Utilities

Public Class FaultManagementListControl
    Inherits UserControlBase

#Region "Local Variables Declaration"
    Protected searchTermClass As String = String.Empty
    Protected objPageSortBo As PageSortBO = New PageSortBO(ApplicationConstants.Descending, _
                                                        ApplicationConstants.faultManagementSortBy, _
                                                        ApplicationConstants.faultManagementInitialPageIndex, _
                                                        ApplicationConstants.faultManagementResultPerPage)

    'Used to create Lookup List of Duration (Double as Value Field and String as Text Field)
    Public Class Duration
        Public Property LookUpValue() As Double
        Public Property LookUpName() As String
    End Class
#End Region

#Region "Page Control Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        UpdPnllblMessage.Update()
        uiMessageHelper.resetMessage(lblmessageAddFault, pnlMessageAddFault)
        UpdPnllblMessageAddFault.Update()
        uiMessageHelper.resetMessage(lblMessageAmendFault, pnlMessageAmendFault)
        UpdPnllblMessageAmendFault.Update()

        'Set/Save PageSortBO to View State with Initial Values on first page Load
        If Not Page.IsPostBack Then
            setPageSortBoViewState(objPageSortBo)
        End If
    End Sub

#End Region

#Region "Utility Methods"

#Region "Get Lookup Values"

#Region "General Get Lookup Values Method"

#Region "Populate Lookup"

    ''' <summary>
    ''' PopulateLookup method populates the given dropdown list with values from given lookuplist.
    ''' </summary>
    ''' <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
    ''' <param name="lstLookup">LookUpList: LookUpList to Bind with dropdown list</param>
    ''' <remarks>this is a utility function that is used to populated a dropdown with lookup values in lookup list.</remarks>
    ''' 
    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByRef lstLookup As List(Of LookUpBO))
        ddlLookup.Items.Clear()
        If (Not (lstLookup Is Nothing) AndAlso (lstLookup.Count > 0)) Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If
    End Sub

    'End Region "Populate Lookup"
#End Region

#Region "Populate Duration"

    Private Sub PopulateDuration(ByRef ddlLookup As DropDownList)

        Dim lstLookup = New List(Of Duration)
        ddlLookup.Items.Clear()
        Dim startingLimitFaultDuration As Double = 0.5
        Dim endLimitFaultDuration As Double = 8.0
        Dim incrementStepFaultDuration As Double = 0.5
        For I As Double = startingLimitFaultDuration To endLimitFaultDuration Step incrementStepFaultDuration
            Dim DurationObj As Duration = New Duration()
            DurationObj.LookUpValue = I
            DurationObj.LookUpName = I.ToString() + vbTab + vbTab + "hour"
            lstLookup.Add(DurationObj)
        Next

        If Not lstLookup Is Nothing Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If

    End Sub

    'End Region "Populate Duration"
#End Region

    'End Region '"General Get Lookup Values Method"
#End Region

#Region "Look UP Values Functions"

    ''Area Lookup Values
    Private Sub getAreaLookUpValues(ByRef ddlname As DropDownList)
        Dim FaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        FaultManagementBL.getAreaLookUpValuesAll(lstLookUp)
        PopulateLookup(ddlname, lstLookUp)
    End Sub

    ''Priority Lookup Values
    Private Sub getPriorityLookUpValues(ByRef ddlname As DropDownList)
        Dim FaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        FaultManagementBL.getPriorityLookUpValuesAll(lstLookUp)
        PopulateLookup(ddlname, lstLookUp)
    End Sub

    ''Duration Lookup Values
    Private Sub getDurationLookUpValues(ByRef ddlname As DropDownList)
        PopulateDuration(ddlname)
    End Sub

    ''Trades Lookup Values
    Private Sub getTradeLookUpValues(ByRef ddlname As DropDownList)
        Dim FaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        FaultManagementBL.getTradeLookUpValuesAll(lstLookUp)
        PopulateLookup(ddlname, lstLookUp)
    End Sub

    ''Vat Rate Lookup Values
    Private Sub getVatRateLookUpValues(ByRef ddlname As DropDownList)
        Dim FaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        FaultManagementBL.getVatRateLookUpValuesAll(lstLookUp)
        PopulateLookup(ddlname, lstLookUp)
    End Sub

    'End Region Look UP Values Functions
#End Region

#Region "Add Fault Look Up Values"

    Private Sub GetAddFaultLookUpValues()
        getAreaLookUpValues(ddlLocationFault)
        getPriorityLookUpValues(ddlPriorityFault)
        getDurationLookUpValues(ddlDurationFault)
        getTradeLookUpValues(ddlTradeFault)
        getVatRateLookUpValues(ddlVatAddFault)
    End Sub

    'End Region "Add Fault Look Up Values"
#End Region

#Region "Amend Fault Look Up Values"

    Private Sub GetAmendFaultLookUpValues()
        getAreaLookUpValues(ddlLocationAmendFault)
        getPriorityLookUpValues(ddlPriorityAmendFault)
        getDurationLookUpValues(ddlDurationAmendFault)
        getTradeLookUpValues(ddlTradeAmendFault)
        getVatRateLookUpValues(ddlVatAmendFault)
    End Sub

    'End Region "Amend Fault Look Up Values"
#End Region

    'End Region "Get Lookup Values"
#End Region

#Region "GetSearchRecords"

    Protected Sub GetSearchResults()
        objPageSortBo = getPageSortBOViewState()
        Dim search As String = getSearchViewState()
        Dim faultDS As DataSet = New DataSet()
        Dim objFaultManagementBL As New FaultManagementBL

        objFaultManagementBL.GetFaultSearchData(faultDS, search, objPageSortBo)

        grdFault.DataSource = faultDS
        grdFault.DataBind()
        objPageSortBo.PageNumber += 1
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        updPnl_LoadGrid.Update()

        If grdFault.PageCount > 1 AndAlso objPageSortBo.PageNumber + 1 = grdFault.PageCount Then
            setResultDataSetViewState(faultDS)
        End If

    End Sub

    Protected Friend Sub PopulateSearchResults(ByVal searchTerm As String, Optional ByRef PageNumber As Integer = 0)
        'save these new search options entered by user unless new search term is being set.

        setSearchViewState(searchTerm)


        'setting new starting page index of for this new search
        grdFault.PageIndex = PageNumber

        objPageSortBo = getPageSortBOViewState()
        objPageSortBo.PageNumber = PageNumber


        Dim totalCount As Integer = GetSearchRowCount()

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        setVirtualItemCountViewState(totalCount)

        'getting and setting row count of new search results
        grdFault.VirtualItemCount = totalCount
        'getting search results
        GetSearchResults()
        updPnl_LoadGrid.Update()

    End Sub

    Private Function GetSearchRowCount() As Integer
        Dim search As String = getSearchViewState()
        Dim objFaultManagementBL As New FaultManagementBL
        Return (objFaultManagementBL.GetFaultSearchRowCount(search))
    End Function

#End Region

#Region "Method to Reset Controls"

    Public Sub GetControlsResetAddFault()

        uiMessageHelper.resetMessage(lblmessageAddFault, pnlMessageAddFault)

        If (ddlLocationFault.Items.Count > 0) Then
            ddlLocationFault.SelectedIndex = 0
        End If
        txtDescription.Text = ""
        If (ddlPriorityFault.Items.Count > 0) Then
            ddlPriorityFault.SelectedIndex = 0
        End If

        If (ddlDurationFault.Items.Count > 0) Then
            ddlDurationFault.SelectedIndex = 0
        End If

        If (ddlTradeFault.Items.Count > 0) Then
            ddlTradeFault.SelectedIndex = 0
        End If

        btnAddTradeFault.Enabled = False

        gvTrades.DataSource = Nothing
        gvTrades.DataBind()

        ViewState.Remove(ViewStateConstants.TradesGridViewState)

        txtNet.Text = "0.0"
        If (ddlVatAddFault.Items.Count > 0) Then
            ddlVatAddFault.SelectedIndex = 0
        End If
        txtVat.Text = "0.0"
        txtGross.Text = "0.0"

        chkBoxRecharge.Checked = False
        'txtNet.Enabled = chkBoxRecharge.Checked
        'ddlVatAddFault.Enabled = chkBoxRecharge.Checked
        'txtVat.Enabled = chkBoxRecharge.Checked
        'txtGross.Enabled = chkBoxRecharge.Checked

        chkBoxSubContractor.Checked = False
        chkBoxFaultActive.Checked = False
        chkBoxGasSafe.Checked = False
        chkBoxOFTEC.Checked = False

        removeTradesDatatableViewState()

    End Sub

    Private Sub GetControlsResetAmendFault()
        uiMessageHelper.resetMessage(lblMessageAmendFault, pnlMessageAmendFault)

        If (ddlLocationAmendFault.Items.Count > 0) Then
            ddlLocationAmendFault.SelectedValue = ""
        End If
        txtDescriptionAmendFault.Text = ""
        If (ddlPriorityAmendFault.Items.Count > 0) Then
            ddlPriorityAmendFault.SelectedValue = ""
        End If
        If (ddlDurationAmendFault.Items.Count > 0) Then
            ddlDurationAmendFault.SelectedValue = ""
        End If

        If (ddlTradeAmendFault.Items.Count > 0) Then
            ddlTradeAmendFault.SelectedValue = ""
        End If

        btnAddTradeAmendFault.Enabled = False

        gvTradesAmendFault.DataSource = Nothing
        gvTradesAmendFault.DataBind()

        txtNetAmendFault.Text = 0.0
        txtVatAmendFault.Text = 0.0
        txtGrossAmendFault.Text = 0.0

        If (ddlVatAmendFault.Items.Count > 0) Then
            ddlVatAmendFault.SelectedValue = ""
        End If

        chkBoxRechargeAmendFault.Checked = False
        'txtNetAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'ddlVatAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'txtVatAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'txtGrossAmendFault.Enabled = chkBoxRechargeAmendFault.Checked

        chkBoxSubContractor.Checked = False
        chkBoxFaultActiveAmendFault.Checked = False
        chkBoxGasSafeAmendFault.Checked = False
        chkBoxOFTECAmendFault.Checked = False

        'Remove any Amend Fault View State Values
        removeAmendFaultStates()

    End Sub

#End Region

#Region "Show Amend Fault Popup"

    Private Sub showFaultDetail(ByVal faultID As Int32)
        'GetAmendFaultLookUpValues()
        'GetControlsResetAmendFault()
        'Populate Amend Fault Fields with data fetched from database.
        'PopulateAmendFaultFields(faultBO)
        'updPnlAmendFault.Update()
        'mdlPopup_AmendFault.Show()
        'reBindGridForLastPage()

        Dim objfaultBO As FaultBO = New FaultBO(faultID, Nothing)
        Dim objFaultManagementBL As New FaultManagementBL
        objFaultManagementBL.GetFaultValues(objfaultBO)
        SessionManager.removeFaultBo()
        SessionManager.setFaultBo(objfaultBO)
        navigateToFaultDetail()

    End Sub

#End Region

#Region "Navigate To Fault Detail"

    Private Sub navigateToFaultDetail()
        Response.Redirect(PathConstants.FaultDetailPath)
    End Sub

#End Region

#Region "Populate Amend Faults Fields"

    Private Sub PopulateAmendFaultFields(ByRef faultBO As FaultBO) '(ByVal faultDS As DataSet)

        If faultBO.FaultID > 0 Then
            lblFaultID.Text = faultBO.FaultID.ToString()
            ddlLocationAmendFault.SelectedValue = faultBO.AreaID
            txtDescriptionAmendFault.Text = faultBO.Description
            ddlPriorityAmendFault.SelectedValue = faultBO.PriorityID

            If (Not IsNothing(faultBO.Duration)) Then
                ddlDurationAmendFault.SelectedValue = faultBO.Duration
            Else
                ddlDurationAmendFault.SelectedValue = ""
            End If

            ddlTradeAmendFault.SelectedValue = ""
            btnAddTradeAmendFault.Enabled = False

            If Not IsNothing(faultBO.Trades) Then
                setTradesDataTabelViewState(faultBO.Trades)
                gvTradesAmendFault.DataSource = faultBO.Trades
                gvTradesAmendFault.DataBind()
                'Get Existing Trades in a list of integers and save in view state
                getExistingTradesFromTradesTable(faultBO.Trades)
            End If

            chkBoxRechargeAmendFault.Checked = faultBO.Recharge
            'txtNetAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
            'ddlVatAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
            'txtVatAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
            'txtGrossAmendFault.Enabled = chkBoxRechargeAmendFault.Checked

            If Not IsNothing(faultBO.NetCost) Then
                txtNetAmendFault.Text = faultBO.NetCost
            Else
                txtNetAmendFault.Text = String.Empty
            End If

            If Not IsNothing(faultBO.Vat) Then
                txtVatAmendFault.Text = faultBO.Vat
            Else
                txtVatAmendFault.Text = String.Empty
            End If

            If Not IsNothing(faultBO.Gross) Then
                txtGrossAmendFault.Text = faultBO.Gross
            Else
                txtGrossAmendFault.Text = String.Empty
            End If

            If Not IsNothing(faultBO.VatRateID) Then
                ddlVatAmendFault.SelectedValue = faultBO.VatRateID
            Else
                ddlVatAmendFault.SelectedValue = ""
            End If

            chkBoxSubContractorAmendFault.Checked = faultBO.IsContractor
            chkBoxFaultActiveAmendFault.Checked = faultBO.FaultActive
            chkBoxGasSafeAmendFault.Checked = faultBO.isGasSafe
            chkBoxOFTECAmendFault.Checked = faultBO.isOFTEC

        End If
    End Sub

#End Region

#Region "Method to get Vat Values"

    Private Sub PopulateVatValues(ByVal ddlName As String)
        Dim objFault As New FaultManagementBL
        Dim vatBO As New VatBO()
        vatBO.VatRate = 0
        Try
            If (ddlName = "Vat" AndAlso ddlVatAddFault.SelectedValue <> "" AndAlso ddlVatAddFault.SelectedIndex <> -1) Then
                vatBO.VatID = CType(ddlVatAddFault.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
                calculatevat(vatBO, ddlName)
            ElseIf (ddlName = "AmendVat" AndAlso ddlVatAmendFault.SelectedValue <> "" AndAlso ddlVatAmendFault.SelectedIndex <> -1) Then
                vatBO.VatID = CType(ddlVatAmendFault.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
                calculatevat(vatBO, ddlName)
            End If
        Catch ex As Exception
            vatBO.IsExceptionGenerated = True
            vatBO.ExceptionMsg = ex.Message

            If (ddlName = "Vat") Then
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, ex.Message, True)
                UpdPnllblMessageAddFault.Update()
            Else
                uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, ex.Message, True)
                UpdPnllblMessageAmendFault.Update()
            End If

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        End Try

    End Sub

#End Region

#Region "Method to Calculate Vat"

    Private Sub calculatevat(ByRef vatbo As VatBO, ByVal ddlname As String)

        Dim vatamount As Double

        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        If (ddlname = "Vat") Then
            If txtNet.Text <> "" And re.IsMatch(txtNet.Text) Then
                vatamount = vatbo.VatRate
                txtVat.Text = Math.Round(((CType(txtNet.Text, Double) * vatamount) / 100), 2).ToString()
                updatePanel_Vat.Update()
                txtGross.Text = Math.Round(Double.Parse(txtNet.Text) + ((Double.Parse(txtNet.Text) * vatamount) / 100), 2)
                updatePanel_Gross.Update()
            Else
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, "Please enter correct net cost", True)
                txtNet.Focus()
                UpdPnllblMessageAddFault.Update()
            End If
        Else
            If txtNetAmendFault.Text <> "" And re.IsMatch(txtNetAmendFault.Text) Then
                vatamount = vatbo.VatRate
                txtVatAmendFault.Text = Math.Round(((Double.Parse(txtNetAmendFault.Text) * vatamount) / 100), 2).ToString()
                updatePanel_VatAmendFault.Update()
                txtGrossAmendFault.Text = Math.Round(Double.Parse(txtNetAmendFault.Text) + ((Double.Parse(txtNetAmendFault.Text) * vatamount) / 100), 2)
                updatePanel_GrossAmendFault.Update()

            Else
                uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, "Please enter correct net cost", True)
                txtNetAmendFault.Focus()
                UpdPnllblMessageAddFault.Update()
            End If

        End If

    End Sub

#End Region

#Region "Method to Add New Fault"

    Private Sub AddNewFault()
        uiMessageHelper.resetMessage(lblMessageAmendFault, pnlMessageAmendFault)
        UpdPnllblMessageAddFault.Update()
        Dim errorMessage As String = String.Empty

        Try
            'Check fault for valid values.
            Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
            If ddlLocationFault.SelectedValue = "" OrElse ddlLocationFault.SelectedValue = "0" OrElse ddlLocationFault.SelectedValue = "-1" Then
                errorMessage = "Please select a Location"
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, errorMessage, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
            ElseIf txtDescription.Text = "" Then
                errorMessage = "Please enter description for the fault"
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, errorMessage, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
            ElseIf ddlPriorityFault.SelectedValue = "" OrElse ddlPriorityFault.SelectedValue = "0" OrElse ddlPriorityFault.SelectedValue = "-1" Then
                errorMessage = "Please select a priority from the list"
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, errorMessage, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
            ElseIf ddlDurationFault.SelectedValue = "" OrElse ddlDurationFault.SelectedValue = "0" OrElse ddlDurationFault.SelectedValue = "-1" Then
                errorMessage = "Please select a Duration from the list."
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, errorMessage, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
                'ElseIf chkBoxRecharge.Checked AndAlso Not (re.IsMatch(txtNet.Text)) Then
            ElseIf Not (re.IsMatch(txtNet.Text)) Then
                errorMessage = "Please enter correct net amount or atleast 0"
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, errorMessage, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
                'ElseIf chkBoxRecharge.Checked AndAlso (ddlVatAddFault.SelectedValue = "" OrElse ddlVatAddFault.SelectedValue = "-1") Then
            ElseIf ddlVatAddFault.Items.Count > 0 AndAlso (ddlVatAddFault.SelectedValue = "" OrElse ddlVatAddFault.SelectedValue = "-1") Then

                errorMessage = "Please select a Vat Rate."
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, errorMessage, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
            Else 'If all the values are valid then this else Block will be executed.
                uiMessageHelper.IsError = False
                uiMessageHelper.message = String.Empty
                SaveAddNewFault()
                If uiMessageHelper.IsError = False Then
                    mdlPopup_AddFault.Hide()
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)

                    Dim searchTerm As String = String.Empty
                    If getPageSortBOViewState() IsNot Nothing Then
                        searchTerm = IIf(getSearchViewState() IsNot Nothing, getSearchViewState, String.Empty)

                        objPageSortBo = getPageSortBOViewState()
                        objPageSortBo.SortExpression = ApplicationConstants.faultManagementSortBy
                        objPageSortBo.SmallSortDirection = ApplicationConstants.Descending
                        objPageSortBo.PageNumber = ApplicationConstants.faultManagementInitialPageIndex
                        grdFault.PageIndex = ApplicationConstants.faultManagementInitialPageIndex
                        setPageSortBoViewState(objPageSortBo)
                    End If
                    removeTradesDatatableViewState()
                    PopulateSearchResults(searchTerm)
                    updPnl_LoadGrid.Update()
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, uiMessageHelper.message, True)
                UpdPnllblMessageAddFault.Update()
                mdlPopup_AddFault.Show()
            End If
        End Try

    End Sub

    Private Sub SaveAddNewFault()
        Dim objFault As FaultManagementBL = New FaultManagementBL()
        Dim faultBO As FaultBO = New FaultBO()
        Try
            ''Get Values
            faultBO.AreaID = CType(ddlLocationFault.SelectedValue, Int32)
            faultBO.Description = txtDescription.Text.Trim()
            faultBO.PriorityID = CType(ddlPriorityFault.SelectedValue, Int32)
            faultBO.Duration = CType(ddlDurationFault.SelectedValue, Double)

            'Add list of Trades
            faultBO.Trades = CType(ViewState(ViewStateConstants.TradesGridViewState), DataTable)

            faultBO.Recharge = chkBoxRecharge.Checked

            'If chkBoxRecharge.Checked Then
            faultBO.NetCost = CType(txtNet.Text, Double)
            If (ddlVatAddFault.Items.Count > 0 And (ddlVatAddFault.SelectedValue = "" OrElse ddlVatAddFault.SelectedIndex = -1)) Then
                faultBO.VatRateID = Nothing
            Else
                faultBO.VatRateID = CType(ddlVatAddFault.SelectedValue, Int32)
            End If
            faultBO.Vat = CType(txtVat.Text, Double)
            faultBO.Gross = CType(txtGross.Text, Double)
            'Else
            ''faultBO.NetCost = Nothing
            ''faultBO.VatRateID = Nothing
            ''faultBO.Vat = Nothing
            ''faultBO.Gross = Nothing
            'End If

            faultBO.IsContractor = chkBoxSubContractor.Checked
            faultBO.FaultActive = chkBoxFaultActive.Checked
            faultBO.isGasSafe = chkBoxGasSafe.Checked
            faultBO.isOFTEC = chkBoxOFTEC.Checked

            faultBO.SubmitDate = CalendarUtilities.FormatDate(System.DateTime.Now.ToShortDateString())
            faultBO.UserID = SessionManager.getUserEmployeeId()

            'Start Setting unused values to Nothing/Null To Avoid Garbage Data to be added in FL_FAULT (DB_TABLE)
            faultBO.ElementID = Nothing
            faultBO.EffectFrom = Nothing
            faultBO.PreInspection = Nothing
            faultBO.PostInspection = Nothing
            faultBO.StockConditionItem = Nothing
            faultBO.FaultAction = Nothing
            faultBO.IsInflative = Nothing
            faultBO.Accepted = Nothing
            'End Setting unused values to Nothing/Null To Avoid Garbage Data to be added in FL_FAULT (DB_TABLE)

            objFault.AddNewFault(faultBO)
            If faultBO.IsFlagStatus = True Then
                mdlPopup_AddFault.Hide()
                uiMessageHelper.IsError = False
                uiMessageHelper.message = "Fault Added Successfully."
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, uiMessageHelper.message, True)
                UpdPnllblMessageAddFault.Update()
            End If
        End Try
    End Sub

#End Region

#Region "Method to Amend a Fault"

    Private Sub AmendFault()
        Dim errorMessage As String = String.Empty
        uiMessageHelper.resetMessage(lblMessageAmendFault, pnlMessageAmendFault)
        UpdPnllblMessageAmendFault.Update()

        'Check fault to Amend for valid values.
        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        If ddlLocationAmendFault.SelectedValue = "" OrElse ddlLocationAmendFault.SelectedValue = "0" OrElse ddlLocationAmendFault.SelectedValue = "-1" Then
            errorMessage = "Please select a Location"
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, errorMessage, True)
            UpdPnllblMessageAmendFault.Update()
            mdlPopup_AmendFault.Show()
        ElseIf txtDescriptionAmendFault.Text = "" Then
            errorMessage = "Please enter description for the fault"
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, errorMessage, True)
            UpdPnllblMessageAmendFault.Update()
            mdlPopup_AmendFault.Show()
        ElseIf ddlPriorityAmendFault.SelectedValue = "" OrElse ddlPriorityAmendFault.SelectedValue = "0" OrElse ddlPriorityAmendFault.SelectedValue = "-1" Then
            errorMessage = "Please select a priority from the list"
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, errorMessage, True)
            UpdPnllblMessageAmendFault.Update()
            mdlPopup_AmendFault.Show()
        ElseIf ddlDurationAmendFault.SelectedValue = "" OrElse ddlDurationAmendFault.SelectedValue = "0" OrElse ddlDurationAmendFault.SelectedValue = "-1" Then
            errorMessage = "Please select a Duration from the list."
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, errorMessage, True)
            UpdPnllblMessageAmendFault.Update()
            mdlPopup_AmendFault.Show()
            'ElseIf chkBoxRechargeAmendFault.Checked AndAlso Not (re.IsMatch(txtNetAmendFault.Text)) Then
        ElseIf Not (re.IsMatch(txtNetAmendFault.Text)) Then
            errorMessage = "Please enter correct net amount or Uncheck Recharge"
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, errorMessage, True)
            UpdPnllblMessageAmendFault.Update()
            mdlPopup_AmendFault.Show()
        ElseIf ddlVatAmendFault.Items.Count > 0 AndAlso (ddlVatAmendFault.SelectedValue = "" OrElse ddlVatAmendFault.SelectedValue = "-1") Then
            errorMessage = "Please select a Vat Rate."
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, errorMessage, True)
            UpdPnllblMessageAmendFault.Update()
            mdlPopup_AmendFault.Show()

            'If all the values are valid then this else Block will be executed.
        Else
            uiMessageHelper.resetMessage(lblMessageAmendFault, pnlMessageAmendFault)
            UpdPnllblMessageAmendFault.Update()

            SaveAmendFault()

            If uiMessageHelper.IsError = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
                mdlPopup_AmendFault.Hide()
                UpdPnllblMessage.Update()
                removeTradesDatatableViewState()

                Dim searchTerm As String = String.Empty
                If getSearchViewState() IsNot Nothing Then
                    searchTerm = getSearchViewState()
                End If

                objPageSortBo = getPageSortBOViewState()

                PopulateSearchResults(searchTerm, objPageSortBo.PageNumber)
                updPnl_LoadGrid.Update()
                'Fault has been changed sucessfully and Results are updated also with a sucess message.
            End If
        End If

    End Sub

    Protected Sub SaveAmendFault()
        Dim objFault As FaultManagementBL = New FaultManagementBL()
        Dim faultBO As FaultBO = New FaultBO()
        Try
            ''Get Values
            faultBO.FaultID = CType(lblFaultID.Text, Int32)
            faultBO.AreaID = CType(ddlLocationAmendFault.SelectedValue, Int32)
            faultBO.Description = txtDescriptionAmendFault.Text.Trim()
            faultBO.PriorityID = CType(ddlPriorityAmendFault.SelectedValue, Int32)
            faultBO.Duration = CType(ddlDurationAmendFault.SelectedValue, Double)

            faultBO.Recharge = chkBoxRechargeAmendFault.Checked

            'If chkBoxRechargeAmendFault.Checked Then
            faultBO.NetCost = CType(txtNetAmendFault.Text, Double)
            If (ddlVatAmendFault.Items.Count > 0) Then
                faultBO.VatRateID = CType(ddlVatAmendFault.SelectedValue, Int32)
            Else
                faultBO.VatRateID = Nothing
            End If
            faultBO.Vat = CType(txtVatAmendFault.Text, Double)
            faultBO.Gross = CType(txtGrossAmendFault.Text, Double)
            'Else
            ''faultBO.NetCost = Nothing
            ''faultBO.VatRateID = Nothing
            ''faultBO.Vat = Nothing
            ''faultBO.Gross = Nothing
            'End If

            faultBO.IsContractor = chkBoxSubContractorAmendFault.Checked
            faultBO.FaultActive = chkBoxFaultActiveAmendFault.Checked
            faultBO.isGasSafe = chkBoxGasSafeAmendFault.Checked
            faultBO.isOFTEC = chkBoxOFTECAmendFault.Checked

            faultBO.Trades = CType(ViewState(ViewStateConstants.TradesGridViewState), DataTable)

            faultBO.SubmitDate = CalendarUtilities.FormatDate(System.DateTime.Now.ToShortDateString())

            'faultBO.UserID = 65
            faultBO.UserID = SessionManager.getUserEmployeeId()

            If getTradesToDeleteListViewState() IsNot Nothing Then
                faultBO.TradesToDelete = getTradesToDeleteListViewState()
            End If

            If getTradesToInsertListViewState() IsNot Nothing Then
                faultBO.TradesToInsert = getTradesToInsertListViewState()
            End If

            'Start Setting unused values to Nothing/Null To Avoid Garbage Data to be added in FL_FAULT (DB_TABLE)
            faultBO.ElementID = Nothing
            faultBO.EffectFrom = Nothing
            faultBO.PreInspection = Nothing
            faultBO.PostInspection = Nothing
            faultBO.StockConditionItem = Nothing
            faultBO.FaultAction = Nothing
            faultBO.IsInflative = Nothing
            faultBO.Accepted = Nothing
            'End Setting unused values to Nothing/Null To Avoid Garbage Data to be added in FL_FAULT (DB_TABLE)

            objFault.AmendFault(faultBO)
            If faultBO.IsFlagStatus = True Then
                uiMessageHelper.IsError = False
                uiMessageHelper.message = "Fault Changed Successfully."

                mdlPopup_AmendFault.Hide()
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = faultBO.ExceptionMsg
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            faultBO.IsExceptionGenerated = True
            faultBO.ExceptionMsg = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, uiMessageHelper.message, True)
                UpdPnllblMessageAmendFault.Update()
                mdlPopup_AmendFault.Show()
            End If
        End Try
    End Sub

#End Region

#Region "Show Add Fault Detail"

    Private Sub showAddFaultDetail()
        Try
            'GetAddFaultLookUpValues()
            'GetControlsResetAddFault()
            'updPnlAddFault.Update()
            'mdlPopup_AddFault.Show()
            'reBindGridForLastPage()

            Dim objfaultBO As FaultBO = New FaultBO(ApplicationConstants.NewfaultId, Nothing)
            objfaultBO.IsNewFault = True
            SessionManager.removeFaultBo()
            SessionManager.setFaultBo(objfaultBO)
            navigateToFaultDetail()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Add Trade to Trade Grid - Add Fault(New Fault)"

    Private Sub addTradeFault()
        Dim dt As DataTable = New DataTable
        Dim faultBO As New FaultBO

        dt = faultBO.Trades.Clone()

        If ViewState(ViewStateConstants.TradesGridViewState) IsNot Nothing Then
            dt = CType(ViewState(ViewStateConstants.TradesGridViewState), DataTable)
        End If

        'Create a new Trades row
        Dim dr = dt.NewRow

        If ddlTradeFault.SelectedValue <> "" And ddlTradeFault.SelectedValue <> -1 Then

            dr(ApplicationConstants.tradeNameColumn) = ddlTradeFault.SelectedItem.Text
            dr(ApplicationConstants.tradeIdColumn) = ddlTradeFault.SelectedValue
            dr(ApplicationConstants.faultTradeIdColumn) = 0

        End If

        'Check if Trade is Already in the Table/Grid If yes Show Message else Add
        If dt.Rows.Contains(ddlTradeFault.SelectedValue) Then
            Dim ErrorMesg = UserMessageConstants.TradeAlreadyAdded '"The trade has already been added."
            uiMessageHelper.setMessage(lblmessageAddFault, pnlMessageAddFault, ErrorMesg, True)
            btnAddTradeFault.Enabled = False
        Else
            uiMessageHelper.resetMessage(lblmessageAddFault, pnlMessageAddFault)
            dt.Rows.Add(dr)

            setTradesDataTabelViewState(dt)

            gvTrades.DataSource = dt
            gvTrades.DataBind()

            btnAddTradeFault.Enabled = False
        End If

        updPnlAddFault.Update()
        mdlPopup_AddFault.Show()
    End Sub

#End Region

#Region "Add Trade to Trade Grid - Amend Fault"

    Private Sub addTradeAmendFault()
        Dim dt As DataTable = New DataTable
        Dim faultBO As New FaultBO

        dt = faultBO.Trades.Clone()

        If ViewState(ViewStateConstants.TradesGridViewState) IsNot Nothing Then
            dt = CType(ViewState(ViewStateConstants.TradesGridViewState), DataTable)
        End If

        'Create a new row
        Dim dr = dt.NewRow

        If ddlTradeAmendFault.SelectedValue <> "" And ddlTradeAmendFault.SelectedValue <> -1 Then

            dr(ApplicationConstants.tradeNameColumn) = ddlTradeAmendFault.SelectedItem.Text
            dr(ApplicationConstants.tradeIdColumn) = ddlTradeAmendFault.SelectedValue
            dr(ApplicationConstants.faultTradeIdColumn) = 0

        End If

        'Start - Check if Trade is Already in the Trades Table/Grid If yes Show Message else Add
        If dt.Rows.Contains(ddlTradeAmendFault.SelectedValue) Then
            Dim ErrorMesg = UserMessageConstants.TradeAlreadyAdded '"The Trade has already been added."
            uiMessageHelper.setMessage(lblMessageAmendFault, pnlMessageAmendFault, ErrorMesg, True)
            UpdPnllblMessageAmendFault.Update()
            btnAddTradeFault.Enabled = False
        Else
            uiMessageHelper.resetMessage(lblmessageAddFault, pnlMessageAddFault)
            dt.Rows.Add(dr)

            setTradesDataTabelViewState(dt)

            gvTradesAmendFault.DataSource = dt
            gvTradesAmendFault.DataBind()

            btnAddTradeAmendFault.Enabled = False

            'Start - Check if Trade is already in Trades from database.
            'If no then Insert in List to add trades else do nothing
            'And also if trade is already in tradesToDelete list, delete it from there

            Dim existingTrades As List(Of Integer) = getExistingTradesListViewState()

            If ((existingTrades IsNot Nothing) AndAlso (Not existingTrades.Contains(ddlTradeAmendFault.SelectedValue))) Then
                Dim tradesToInsert As List(Of Integer) = New List(Of Integer)

                If getTradesToInsertListViewState() IsNot Nothing Then
                    tradesToInsert = getTradesToInsertListViewState()
                End If
                tradesToInsert.Add(ddlTradeAmendFault.SelectedValue)
                setTradesToInsertListViewState(tradesToInsert)
            End If

            Dim tradesToDelete As List(Of Integer) = Nothing
            If getTradesToDeleteListViewState() IsNot Nothing Then
                tradesToDelete = getTradesToDeleteListViewState()
            End If

            If tradesToDelete IsNot Nothing AndAlso tradesToDelete.Count > 0 _
                AndAlso tradesToDelete.Contains(ddlTradeAmendFault.SelectedValue) Then
                tradesToDelete.Remove(ddlTradeAmendFault.SelectedValue)
                setTradesToDeleteListViewState(tradesToDelete)
            End If

            'End - Check if Trade is already in Trades from database.

        End If
        'End - Check if Trade is Already in the Trades Table/Grid If yes Show Message else Add


        updPnlAmendFault.Update()
        mdlPopup_AmendFault.Show()
    End Sub

#End Region

#Region "Get Trade From Trade Table to existing trades list"

    Private Sub getExistingTradesFromTradesTable(ByRef dataTable As DataTable)
        Dim existingTrades = New List(Of Integer)
        If ((dataTable IsNot Nothing) AndAlso dataTable.Rows.Count > 0) Then
            For i = 0 To dataTable.Rows.Count - 1
                existingTrades.Add(dataTable.Rows(i).Item(ApplicationConstants.tradeIdColumn))
            Next
        End If
        setExistingTradesListViewState(existingTrades)
    End Sub

#End Region

#Region "Remove the states (viewState) used for Amend Fault popup"

    Private Sub removeAmendFaultStates()
        removeTradesDatatableViewState()
        removeExistingTradesListViewState()
        removeTradesToDeleteListViewState()
        removeTradesToInsertListViewState()
    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        'Rebind the Grid to prevent show empty lines for last page only

        objPageSortBo = getPageSortBOViewState()

        If grdFault.PageCount > 1 AndAlso objPageSortBo.PageNumber + 1 = grdFault.PageCount Then
            grdFault.VirtualItemCount = getVirtualItemCountViewState()
            grdFault.PageIndex = objPageSortBo.PageNumber
            grdFault.DataSource = getResultDataSetViewState()
            grdFault.DataBind()
            objPageSortBo.PageNumber += 1
            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        End If

    End Sub

#End Region

#End Region

#Region "Events"

#Region "ddl Index Changed"

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlVatAddFault.SelectedIndexChanged
        PopulateVatValues("Vat")
    End Sub

    Protected Sub ddlVatAmendFault_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlVatAmendFault.SelectedIndexChanged
        PopulateVatValues("AmendVat")
    End Sub

    Protected Sub ddlTradeFault_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTradeFault.SelectedIndexChanged
        If ddlTradeFault.SelectedIndex = 0 OrElse ddlTradeFault.SelectedIndex = -1 Then
            btnAddTradeFault.Enabled = False
        Else
            btnAddTradeFault.Enabled = True
        End If

        updPnlAddFault.Update()
        mdlPopup_AddFault.Show()

    End Sub

    Protected Sub ddlTradeAmendFault_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTradeAmendFault.SelectedIndexChanged
        If ddlTradeAmendFault.SelectedIndex = 0 OrElse ddlTradeAmendFault.SelectedIndex = -1 Then
            btnAddTradeAmendFault.Enabled = False
        Else
            btnAddTradeAmendFault.Enabled = True
        End If

        updPnlAmendFault.Update()
        mdlPopup_AmendFault.Show()
    End Sub

#End Region

#Region "Grid Events"

    Protected Sub GVFault_PageIndexChanging(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        'assiging new page index to the gridview
        grdFault.PageIndex = e.NewPageIndex

        'Get PageSortBO from View State
        objPageSortBo = getPageSortBOViewState()

        objPageSortBo.PageNumber = e.NewPageIndex

        'Set/Save PageSortBO in View State with new page number.
        setPageSortBoViewState(objPageSortBo)

        'calling functions for populating gridview again after page indexing
        GetSearchResults()
    End Sub

    Protected Sub GVFault_Sorting(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)

        'Get PageSortBO from View State
        objPageSortBo = getPageSortBOViewState()

        'setting pageindex of gridview equal to zero for new search
        grdFault.PageIndex = ApplicationConstants.faultManagementInitialPageIndex

        'setting new sorting expression and order
        objPageSortBo.PageNumber = ApplicationConstants.faultManagementInitialPageIndex
        objPageSortBo.SortExpression = e.SortExpression
        objPageSortBo.setSortDirection()

        setPageSortBoViewState(objPageSortBo)

        'populating gridview with new results
        GetSearchResults()

    End Sub

    Protected Sub gvTrades_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTrades.RowDeleting
        Dim dt As DataTable = New DataTable()
        dt = getTradesDataTableViewState()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim dr = dt.Rows(e.RowIndex)
            Dim TradeID = dr(ApplicationConstants.tradeIdColumn)

            If (ddlTradeFault.Items.Count > 0 AndAlso ddlTradeFault.SelectedValue = TradeID) Then
                btnAddTradeFault.Enabled = True
            End If

            dt.Rows.RemoveAt(e.RowIndex)

            setTradesDataTabelViewState(dt)

            Me.gvTrades.DataSource = dt
            Me.gvTrades.DataBind()
        End If

        updPnlAddFault.Update()
        mdlPopup_AddFault.Show()
    End Sub

    Protected Sub gvTradesAmendFault_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvTradesAmendFault.RowDeleting
        Dim dt As DataTable = New DataTable()
        dt = getTradesDataTableViewState()

        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then

            Dim dr = dt.Rows(e.RowIndex)
            Dim tradeID = dr(ApplicationConstants.tradeIdColumn)

            If (ddlTradeAmendFault.Items.Count > 0 AndAlso ddlTradeAmendFault.SelectedValue = tradeID) Then
                btnAddTradeAmendFault.Enabled = True
            End If

            dt.Rows.RemoveAt(e.RowIndex)

            setTradesDataTabelViewState(dt)

            gvTradesAmendFault.DataSource = dt
            gvTradesAmendFault.DataBind()

            'Start - Check if Trade is already in Trades from database. 
            'If yes then Insert in List to delete trades else do nothing
            'And also if a trade is added to Trades to Insert List, Delete it from there

            Dim existingTrades As List(Of Integer) = getExistingTradesListViewState()

            If ((Not IsNothing(existingTrades)) AndAlso (existingTrades.Contains(tradeID))) Then
                Dim tradesToDelete As List(Of Integer) = New List(Of Integer)

                If getTradesToDeleteListViewState() IsNot Nothing Then
                    tradesToDelete = getTradesToDeleteListViewState()
                End If
                tradesToDelete.Add(tradeID)
                setTradesToDeleteListViewState(tradesToDelete)
            End If

            Dim tradesToInsert As List(Of Integer) = Nothing
            If getTradesToInsertListViewState() IsNot Nothing Then
                tradesToInsert = getTradesToInsertListViewState()
            End If

            If tradesToInsert IsNot Nothing AndAlso tradesToInsert.Count > 0 _
                AndAlso tradesToInsert.Contains(tradeID) Then
                tradesToInsert.Remove(ddlTradeAmendFault.SelectedValue)
                setTradesToInsertListViewState(tradesToInsert)
            End If

            'End - Check if Trade is already in Trades from database.

        End If

        updPnlAmendFault.Update()
        mdlPopup_AmendFault.Show()
    End Sub

    'End Region "Grid Events"
#End Region

    Protected Sub grdFault_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBOViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Button Click"

    Protected Sub btnAddFault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFault.Click
        showAddFaultDetail()
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        removeTradesDatatableViewState()
        mdlPopup_AddFault.Hide()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        AddNewFault()
    End Sub

    Protected Sub btnAmendClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAmendClose.Click
        removeAmendFaultStates()
        mdlPopup_AmendFault.Hide()
    End Sub

    Protected Sub btnAmendSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAmendSave.Click
        AmendFault()
    End Sub

    Protected Sub btnAddTradeFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTradeFault.Click
        addTradeFault()
    End Sub

    Protected Sub btnAddTradeAmendFault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddTradeAmendFault.Click
        addTradeAmendFault()
    End Sub

#End Region

#Region "Image Button"

#Region "Image Button Inside Faults Management Grid"

    Protected Sub imgBtnEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim imgBtn As ImageButton = DirectCast(sender, ImageButton)
            Dim faultId As Integer = CType(imgBtn.CommandArgument, Integer)
            showFaultDetail(CType(faultId, Int32))

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                updPnl_LoadGrid.Update()
            End If

        End Try
    End Sub

#End Region

    'End Region "Link Button"
#End Region

#Region "Text Changed"

    Protected Sub txtNet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNet.TextChanged
        PopulateVatValues("Vat")
    End Sub

    Protected Sub txtNetAmendFault_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtNetAmendFault.TextChanged
        PopulateVatValues("AmendVat")
    End Sub

#End Region

#Region "Check Box Checked Change"

    Protected Sub chkBoxRecharge_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        'txtNet.Enabled = chkBoxRecharge.Checked
        'ddlVatAddFault.Enabled = chkBoxRecharge.Checked
        'txtVat.Enabled = chkBoxRecharge.Checked
        'txtGross.Enabled = chkBoxRecharge.Checked
        'updPnlAddFault.Update()
        'mdlPopup_AddFault.Show()
    End Sub

    Protected Sub chkBoxRechargeAmendFault_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        'txtNetAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'ddlVatAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'txtVatAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'txtGrossAmendFault.Enabled = chkBoxRechargeAmendFault.Checked
        'updPnlAmendFault.Update()
        'mdlPopup_AmendFault.Show()
    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBOViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            objPageSortBo.PageNumber -= 1
            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            GetSearchResults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBOViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBOViewState()

                objPageSortBo.PageNumber = pageNumber

                objPageSortBo.PageNumber -= 1 'Just Added for Fault Management Report
                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                GetSearchResults()
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

    'End Region "Events"
#End Region

#Region "View State Get and Set Method/Function(s)"

#Region "Trades Datatable/GridView View State Get, Set and Remove Method/Function(s)"

    Protected Sub setTradesDataTabelViewState(ByRef Tradesdt As DataTable)
        ViewState(ViewStateConstants.TradesGridViewState) = Tradesdt
    End Sub

    Protected Function getTradesDataTableViewState() As DataTable
        If ViewState(ViewStateConstants.TradesGridViewState) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.TradesGridViewState), DataTable)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub removeTradesDatatableViewState()
        ViewState.Remove(ViewStateConstants.TradesGridViewState)
    End Sub

#End Region

#Region "PageSortBO View State Get, Set and Remove Method/Function(s)"

    Protected Sub setPageSortBoViewState(ByRef PageSortBo As PageSortBO)
        ViewState(ViewStateConstants.PageSortBo) = PageSortBo
    End Sub

    Protected Function getPageSortBOViewState() As PageSortBO
        If ViewState(ViewStateConstants.PageSortBo) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.PageSortBo), PageSortBO)
        Else
            Return New PageSortBO(ApplicationConstants.Descending, _
                                    ApplicationConstants.faultManagementSortBy, _
                                    ApplicationConstants.faultManagementInitialPageIndex, _
                                    ApplicationConstants.faultManagementResultPerPage)
        End If
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Search View State Get, Set and Remove Method/Function(s)"

    Protected Sub setSearchViewState(ByVal Search As String)
        ViewState(ViewStateConstants.Search) = Search
    End Sub

    Protected Function getSearchViewState() As String
        If ViewState(ViewStateConstants.Search) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.Search), String)
        Else
            Return Nothing
        End If
    End Function

    Protected Sub removeSearchViewState()
        ViewState.Remove(ViewStateConstants.Search)
    End Sub

#End Region

#Region "Existing Trades List View State Get, Set and Remove Method/Function(s)"

    Protected Sub setExistingTradesListViewState(ByVal list As List(Of Integer))
        ViewState(ViewStateConstants.ExistingTradesViewState) = list
    End Sub

    Protected Function getExistingTradesListViewState()
        If ViewState(ViewStateConstants.ExistingTradesViewState) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.ExistingTradesViewState), List(Of Integer))
        Else
            Return New List(Of Integer)
        End If
    End Function

    Protected Sub removeExistingTradesListViewState()
        ViewState.Remove(ViewStateConstants.ExistingTradesViewState)
    End Sub

#End Region

#Region "Trades to delete List View State Get, Set and Remove Method/Function(s)"

    Protected Sub setTradesToDeleteListViewState(ByVal list As List(Of Integer))
        ViewState(ViewStateConstants.TradesToDeleteViewState) = list
    End Sub

    Protected Function getTradesToDeleteListViewState()
        If ViewState(ViewStateConstants.TradesToDeleteViewState) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.TradesToDeleteViewState), List(Of Integer))
        Else
            Return Nothing
        End If
    End Function

    Protected Sub removeTradesToDeleteListViewState()
        ViewState.Remove(ViewStateConstants.TradesToDeleteViewState)
    End Sub

#End Region

#Region "Trades to Insert List View State Get, Set and Remove Method/Function(s)"

    Protected Sub setTradesToInsertListViewState(ByVal list As List(Of Integer))
        ViewState(ViewStateConstants.TradesToInsertViewState) = list
    End Sub

    Protected Function getTradesToInsertListViewState()
        If ViewState(ViewStateConstants.TradesToInsertViewState) IsNot Nothing Then
            Return CType(ViewState(ViewStateConstants.TradesToInsertViewState), List(Of Integer))
        Else
            Return Nothing
        End If
    End Function

    Protected Sub removeTradesToInsertListViewState()
        ViewState.Remove(ViewStateConstants.TradesToInsertViewState)
    End Sub

#End Region

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#End Region

End Class