﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RecallListControl.ascx.vb"
    Inherits="FaultScheduling.RecallListControl" %>
<%@ Register Src="~/Controls/Reports/schemeBlockDropdown.ascx" TagName="SchemeBlockDropdown"
    TagPrefix="ucSchemeBlockddl" %>
<%@ Register Src="~/Controls/Reports/FinancialYear.ascx" TagName="FinancialYear"
    TagPrefix="uc" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .recallDetailOriginalFault
    {
        width: 45%;
        float: left;
        margin-top: 0;
        margin-bottom: 0;
        margin-left: 1%;
        margin-right: 1%;
        padding: 10px;
    }
    .recallDetailRecalls
    {
        width: 45%;
        float: left;
        margin-bottom: 0;
        margin-left: 1%;
        margin-right: 1%;
        padding: 10px;
        overflow: auto;
        max-height: 300px;
    }
</style>
<asp:Panel ID="pnlMessage" runat="server" Visible="False">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:Panel runat="server" ID="pnlSchemBlockddl">
    <div class="">
        <ucSchemeBlockddl:SchemeBlockDropdown ID="SchemeBlockDDL" runat="server"></ucSchemeBlockddl:SchemeBlockDropdown>
        <uc:FinancialYear ID="ucFinancialYear" runat="server" style="width: 95px;">
        </uc:FinancialYear>
    </div>
</asp:Panel>
<div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
    <cc1:PagingGridView ID="grdRecallList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
        OnRowCreated="grdRecallList_RowCreated" 
        Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
        <Columns>
            <asp:TemplateField HeaderText="JSN" ItemStyle-CssClass="dashboard" SortExpression="JSN">
                <ItemTemplate>
                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle BorderStyle="None" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recorded" SortExpression="Recorded">
                <ItemTemplate>
                    <asp:Label ID="lblRecorded" runat="server" Text='<%# Bind("Recorded") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                <ItemTemplate>
                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Block" SortExpression="Block">
                <ItemTemplate>
                    <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="145px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location" SortExpression="Location">
                <ItemTemplate>
                    <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="115px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="190px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Completed" SortExpression="Completed">
                <ItemTemplate>
                    <asp:Label ID="lblCompleted" runat="server" Text='<%# Bind("Completed") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="By" SortExpression="Operative">
                <ItemTemplate>
                    <asp:Label ID="lblBy" runat="server" Text='<%# Bind("Operative") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="110px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recalls:" SortExpression="number">
                <ItemTemplate>
                    <asp:Label ID="lblRecalls" runat="server" Text='<%# Bind("number") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="40px" />
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnView" runat="server" Text="View" EnableTheming="True" UseSubmitBehavior="False"
                        OnClick="btnView_Click" CommandArgument='<%# Bind("OriginalFaultLogId") %>'
                        CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc1:PagingGridView>
</div>
<%--Pager Template Start--%>
<asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
    <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
        <div class="paging-left">
            <span style="padding-right:10px;">
                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                    &lt;&lt;First
                </asp:LinkButton>
                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                    &lt;Prev
                </asp:LinkButton>
            </span>
            <span style="padding-right:10px;">
                <b>Page:</b>
                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                of
                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
            </span>
            <span style="padding-right:20px;">
                <b>Result:</b>
                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                to
                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                of
                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
            </span>
            <span style="padding-right:10px;">
                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                </asp:LinkButton>
                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                </asp:LinkButton>
            </span>
        </div>
        <div style="float: right;">
            <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
            <div class="field" style="margin-right: 10px;">
                <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
            </div>
            <span>
                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                    class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
            </span>
        </div>
    </div>
</asp:Panel>
<%--Pager Template End--%>
<asp:Panel ID="pnlRecallDetail" runat="server" CssClass="modalPopup" Width="670px">
    <div id="header-Popup">
        <p id="header_label">
            BHG Fault Locator
        </p>
        <div id="header_box">
        </div>
    </div>
    <br />
    <br />
    <br />
    <asp:Panel ID="pnlMessageRecallDetail" runat="server" Visible="False">
        <asp:Label ID="lblMessageRecallDetail" runat="server"></asp:Label>
    </asp:Panel>
    <div style="padding: 10px; padding-left: 20px; clear: both;">
        <asp:Label ID="lblRecallDetailsFor" runat="server" Font-Bold="true" Text="Recall Detail For: "></asp:Label>
        <%--<asp:Label ID="lblClientNameHeader" runat="server" Font-Bold="true" Text=""></asp:Label>--%>
        &nbsp;<asp:Label ID="lblPropertyAddress" runat="server" Font-Bold="True"></asp:Label>
        ,&nbsp;<asp:Label ID="lblPropertyCityHeader" runat="server" Font-Bold="True"></asp:Label>
        &nbsp;<asp:Label ID="lblPropertyTel" runat="server" Font-Bold="true" Text=" Tel :"> </asp:Label>
        <asp:Label ID="lblPropertyTelHeader" runat="server" Font-Bold="True"></asp:Label>
        <hr />
    </div>
    <div class="recallDetailOriginalFault">
        <table width="100%">
            <tr>
                <td colspan="2">
                    <span style="font-weight: bold">Original Fault/Repair Details:</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblJSNOriginalFault" runat="server" Text="" Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Contractor:
                </td>
                <td>
                    <asp:Label ID="lblContractor" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    Operative:
                </td>
                <td>
                    <asp:Label ID="lblOperative" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    Priority:
                </td>
                <td>
                    <asp:Label ID="lblPriority" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    Order date:
                </td>
                <td>
                    <asp:Label ID="lblOrderDate" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    Completion:
                </td>
                <td>
                    <asp:Label ID="lblCompletion" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <asp:Label ID="lblLocation" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td>
                    Description:
                </td>
                <td>
                    <asp:Label ID="lblDescription" runat="server" Text="" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Notes:
                    <br />
                    <asp:Label ID="lblNotes" runat="server" Text="These are notes." BorderColor="Gray"
                        BorderStyle="Solid" BorderWidth="1px" Width="100%" Height="50px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span style="font-weight: bold">Repair Details:</span><br />
                    <asp:Label ID="lblDateRepairDetailsOriginalFault" runat="server" Text=""></asp:Label>:
                    <asp:Label ID="lblDescriptionRepairDetailOriginalFault" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div class="recallDetailRecalls">
        <span style="font-weight: bold">Recalls</span>
        <asp:Repeater ID="rptRecalls" runat="server">
            <ItemTemplate>
                <table width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblJSNRecallFault" runat="server" Text="<%# Bind('JSN') %>" Font-Bold="true" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            Contractor:
                        </td>
                        <td>
                            <asp:Label ID="lblContractorRecallFault" runat="server" Text="" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            Operative:
                        </td>
                        <td>
                            <asp:Label ID="lblOperativeRecallFault" runat="server" Text="<%# BIND('Operative') %>" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td>
                            Priority:
                        </td>
                        <td>
                            <asp:Label ID="lblPriorityRecallFault" runat="server" Text="" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            Order date:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderDateRecallFault" runat="server" Text="<%# BIND('OrderDate') %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Completion:
                        </td>
                        <td>
                            <asp:Label ID="lblCompletionRecallFault" runat="server" Text="<%# BIND('CompletionDate') %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Location:
                        </td>
                        <td>
                            <asp:Label ID="lblLocationRecallFault" runat="server" Text="<%# BIND('Location') %>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description:
                        </td>
                        <td>
                            <asp:Label ID="lblDescriptionRecallFault" runat="server" Text="<%# BIND('Description') %>" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Notes:
                            <br />
                            <asp:Label ID="lblNotesRecallFault" runat="server" Text="<%# BIND('Notes') %>" BorderColor="Gray"
                                BorderStyle="Solid" BorderWidth="1px" Width="100%" Height="50px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="font-weight: bold">Repair Details:</span><br />
                            <asp:Label ID="lblDateRepairDetailsRecallFault" runat="server" Text="<%# BIND('RepairDate') %>"></asp:Label>:
                            <asp:Label ID="lblDescriptionRepairDetailRecallFault" runat="server" Text="<%# BIND('RepairNotes') %>"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SeparatorTemplate>
                <hr />
            </SeparatorTemplate>
        </asp:Repeater>
    </div>
    <div style="text-align: right; clear: both;">
        <asp:Button ID="btnBack" runat="server" Text="< Back" UseSubmitBehavior="False" />
        <ajaxToolkit:ModalPopupExtender ID="mdlPopupRecallDetail" runat="server" DynamicServicePath=""
            Enabled="True" TargetControlID="lblRecallDetailsFor" PopupControlID="pnlRecallDetail"
            BackgroundCssClass="modalBackground">
        </ajaxToolkit:ModalPopupExtender>
    </div>
</asp:Panel>
<%--End - Changes By Aamir Waheed on April 24 2013 --%>