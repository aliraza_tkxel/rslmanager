﻿Imports FLS_BusinessLogic
Imports FLS_Utilities
Imports FLS_BusinessObject

Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Reflection
Imports System.ComponentModel
Imports System.IO

Public Class RepairDatabase
    Inherits UserControlBase
#Region "Properties"

    Dim objReportsBl As ReportsBL = New ReportsBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "RepairDescription", 1, 30)

    Dim objPageSortBoAssociatedFaults As PageSortBO = New PageSortBO("DESC", "RepairDescription", 1, 30)
    Dim totalCount As Integer = 0
    Dim totalCountAssociatedFaultd As Integer = 0
#End Region

#Region "Events"
#Region "Page Pre Render Event"
    'Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    Try
    '        Me.reBindGridForLastPage()
    '        ' Me.reBindGridForLastPageAssociatedFaults()
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
    '        End If
    '    End Try
    'End Sub
#End Region

#Region "PageLoad"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        '    mdlAddNewRepairPopup.Show()
        'End If

        uiMessageHelper.resetMessage(lblMessage, pnlMessage)

    End Sub
#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateRepairDatabaseList(resultDataSet, search, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateRepairDatabaseList(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Grid grdRepairList Sorting Event"

    Protected Sub grdRepairList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdRepairList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdRepairList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()

            populateRepairDatabaseList(resultDataSet, search, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Export Associated Faults to Excel Click"
    ''' <summary>
    ''' Button Export Associated Faults to Excel Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub btnAssociatedFaultsExportXls_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssociatedFaultsExportXls.Click
        Try
            exportAssociatedFaults()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Button Export Repair list to Excel Click"
    ''' <summary>
    ''' Button Export Repair list to Excel Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub btnExportToXls_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToXls.Click
        Try
            exportRepairsList()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Function"

#Region "Bind To Grid"

    Sub bindToGrid()
        ViewState.Item(ViewStateConstants.Search) = ""
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.RepairsResultDataSet)





        'grdRepairList.VirtualItemCount = totalCount
        grdRepairList.DataSource = resultDataSet
        grdRepairList.DataBind()

        objPageSortBo = getPageSortBoViewState()

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdRepairList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdRepairList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Populate Repair List"

    Sub populateRepairDatabaseList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)
        Try
            If setSession = True Then
                removePageSortBoViewState()
                getPageSortBoViewState()
            Else
                objPageSortBo = getPageSortBoViewState()
            End If

            ViewState.Add(ViewStateConstants.Search, search)
            totalCount = objReportsBl.getReactiveRepairList(resultDataSet, search, objPageSortBo)

            objPageSortBo.TotalRecords = totalCount
            objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)


            If setSession Then
                ViewState.Add(ViewStateConstants.RepairsResultDataSet, resultDataSet)
                'ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
                setPageSortBoViewState(objPageSortBo)
                ViewState.Add(ViewStateConstants.TotalCount, totalCount)
            End If

            If totalCount > 0 Then           
                btnExportToXls.Enabled = True
            Else
                btnExportToXls.Enabled = False
            End If

            'grdRepairList.VirtualItemCount = totalCount
            grdRepairList.DataSource = resultDataSet
            grdRepairList.DataBind()

            setPageSortBoViewState(objPageSortBo)

            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

            ' If grdRepairList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdRepairList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
            '  End If

            'End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
    Protected Sub grdRepairList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdRepairList.RowDataBound
        Dim drview As DataRowView = TryCast(e.Row.DataItem, DataRowView)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lnkAssociatedFault As LinkButton = TryCast(e.Row.FindControl("lnkAssociatedFault"), LinkButton)
            If lnkAssociatedFault.Text = 0 Then
                lnkAssociatedFault.Enabled = False
            End If

        End If
    End Sub

    Sub populateRepairDatabaseList(ByRef resultDataSet As DataSet, ByVal search As String)
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "RepairDescription", 1, 30)
        totalCount = objReportsBl.getReactiveRepairList(resultDataSet, search, objPageSortBo)
        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        ViewState.Add(ViewStateConstants.RepairsResultDataSet, resultDataSet)
        ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        ViewState.Add(ViewStateConstants.Search, search)

        If totalCount > 0 Then
            btnExportToXls.Enabled = True
        Else
            btnExportToXls.Enabled = False
        End If

        ' grdRepairList.VirtualItemCount = totalCount
        grdRepairList.DataSource = resultDataSet
        grdRepairList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdRepairList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdRepairList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If
    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        'Rebind the Grid to prevent show empty lines for last page only

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdRepairList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdRepairList.PageCount Then
            grdRepairList.VirtualItemCount = getVirtualItemCountViewState()
            grdRepairList.PageIndex = objPageSortBo.PageNumber - 1
            grdRepairList.DataSource = getResultDataSetViewState()
            grdRepairList.DataBind()
            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        End If

    End Sub

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.RepairsResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.RepairsResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.RepairsResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.RepairsResultDataSet)
    End Sub

#End Region

#Region "Fault log ID Set/Get/Remove"

#Region "Fault log ID Set"

    Protected Sub setFaultLogIdViewState(ByVal faultLogId As Integer)
        ViewState.Add(ViewStateConstants.FaultLogId, faultLogId)
    End Sub

#End Region

#Region "Fault log ID Get"

    Protected Function getFaultLogIdViewState()
        Dim faultLogId As Integer = 0

        If Not IsNothing(ViewState(ViewStateConstants.FaultLogId)) Then
            faultLogId = ViewState(ViewStateConstants.FaultLogId)
        End If

        Return faultLogId
    End Function

#End Region

#Region "Fault log ID Remove"

    Protected Sub removeFaultLogIdViewState()
        ViewState.Remove(ViewStateConstants.FaultLogId)
    End Sub

#End Region

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        Else
            pageSortBO = New PageSortBO("DESC", "RepairDescription", 1, 30)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

#Region "Page Sort BO for associated faults"

    Protected Sub setPageSortBoViewStateAssociatedFaults(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBoAssociatedFaults) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewStateAssociatedFaults()
        Dim pageSortBO = objPageSortBoAssociatedFaults

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBoAssociatedFaults)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBoAssociatedFaults)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewStateAssociatedFaults()
        ViewState.Remove(ViewStateConstants.PageSortBoAssociatedFaults)
    End Sub

#End Region

#Region "Method to Calculate Vat"

    Private Sub calculatevat(ByRef vatbo As VatBO, ByVal ddlname As String)

        Dim vatamount As Double
        'vatbo.VatRate = 0
        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")
        uiMessageHelper.resetMessage(lblmessageAmendRepair, pnlMessageAmendRepair)
        If (ddlname = "AmendVat") Then
            If txtAmendNet.Text <> "" And re.IsMatch(txtAmendNet.Text) Then
                vatamount = vatbo.VatRate
                txtAmendVat.Text = Math.Round(((CType(txtAmendNet.Text, Double) * vatamount) / 100), 2).ToString()
                updatePanel_Vat.Update()
                txtAmendGross.Text = Math.Round(Double.Parse(txtAmendNet.Text) + ((Double.Parse(txtAmendNet.Text) * vatamount) / 100), 2)
                updatePanel_Gross.Update()
            Else
                uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, "Please enter correct net cost", True)
                txtAmendNet.Focus()
                UpdPnllblMessageAmendRepair.Update()
            End If
        Else
            If txtAddNet.Text <> "" And re.IsMatch(txtAddNet.Text) Then
                vatamount = vatbo.VatRate
                txtAddVat.Text = Math.Round(((CType(txtAddNet.Text, Double) * vatamount) / 100), 2).ToString()
                updatePanelAdd_Vat.Update()
                txtAddGross.Text = Math.Round(Double.Parse(txtAddNet.Text) + ((Double.Parse(txtAddNet.Text) * vatamount) / 100), 2)
                updatePanelAdd_Gross.Update()
            Else
                uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, "Please enter correct net cost", True)
                txtAddNet.Focus()
                UpdPnllblMessageAddRepair.Update()
            End If

        End If

    End Sub

#End Region

#Region "Report buttons action"

#Region "Image Button Inside Repair List Grid"

    Protected Sub imgBtnEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim imgBtn As ImageButton = DirectCast(sender, ImageButton)
            Dim repairId As Integer = CType(imgBtn.CommandArgument, Integer)
            Dim dataSet As DataSet
            dataSet = CType(ViewState(ViewStateConstants.RepairsResultDataSet), DataSet)
            setAmendPopupValues(dataSet, repairId)
            mdlAmendRepairPopup.Show()
        Catch ex As Exception
        Finally

            mdlAmendRepairPopup.Show()
        End Try
    End Sub


#End Region

#Region "Add a repair popup button"
    Protected Sub AddRepairBtnOnRepairList_Click(sender As Object, e As EventArgs) Handles AddRepairBtnOnRepairList.Click
        setAddPopupValues()
        mdlAddRepairPopup.Show()
    End Sub
#End Region

#End Region

#Region "Populate Lookup"


    Private Sub PopulateLookup(ByRef ddlLookup As DropDownList, ByRef lstLookup As List(Of LookUpBO))
        ddlLookup.Items.Clear()
        If (Not (lstLookup Is Nothing) AndAlso (lstLookup.Count > 0)) Then
            ddlLookup.DataSource = lstLookup
            ddlLookup.DataValueField = "LookUpValue"
            ddlLookup.DataTextField = "LookUpName"
            'ddlLookup.Items.Insert(0, "Please select")
            ' ddlLookup.Items.Add(New ListItem("Please select", ""))
            ddlLookup.DataBind()
        End If
    End Sub
#End Region

#Region "Look UP Values Functions"
    Private Sub getVatRateLookUpValues(ByRef ddlname As DropDownList)
        Dim FaultManagementBL As New FaultManagementBL()
        Dim lstLookUp As New List(Of LookUpBO)
        FaultManagementBL.getVatRateLookUpValuesAll(lstLookUp)
        PopulateLookup(ddlname, lstLookUp)
    End Sub
#End Region

#Region "set Amend Repair popup Values"
    Sub setAmendPopupValues(ByRef dataSet As DataSet, ByVal repairId As Integer)
        getVatRateLookUpValues(ddlAmendVatRate)
        Dim dataTable As DataTable
        SessionManager.setFaultRepairListId(repairId)
        dataTable = dataSet.Tables("Table1").AsEnumerable().CopyToDataTable()
        Dim dataRow = (From row In dataTable _
                     Where row("FaultRepairListID") = repairId _
               Select row)

        txtAmendDescription.Text = CType(dataRow.Last.Item("RepairDescription"), String)
        txtAmendNet.Text = CType(dataRow.Last.Item("NetCost"), String)
        txtAmendVat.Text = CType(dataRow.Last.Item("Vat"), String)
        txtAmendGross.Text = CType(dataRow.Last.Item("Gross"), String)
        ddlAmendVatRate.SelectedItem.Value = CType(dataRow.Last.Item("VatRateId"), Integer)
        ddlAmendVatRate.SelectedIndex = CType(dataRow.Last.Item("VatRateId"), Integer)


        If Not IsDBNull(CType(dataRow.Last.Item("IsActive"), String)) Then
            If CType(dataRow.Last.Item("IsActive"), String) = "Yes" Then
                chkBoxAmendIsActive.Checked = True
            Else
                chkBoxAmendIsActive.Checked = False
            End If
        End If

        If Not IsDBNull(CType(dataRow.Last.Item("PostInspection"), Boolean)) Then
            chkBoxAmendPostInspection.Checked = CType(dataRow.Last.Item("PostInspection"), Boolean)
        Else
            chkBoxAmendPostInspection.Checked = False
        End If

        If Not IsDBNull(CType(dataRow.Last.Item("StockConditionItem"), Boolean)) Then
            chkBoxAmendStockCondition.Checked = CType(dataRow.Last.Item("StockConditionItem"), Boolean)
        Else
            chkBoxAmendStockCondition.Checked = False
        End If

    End Sub
#End Region

#Region "set Add Repair popup Values"
    Sub setAddPopupValues()
        getVatRateLookUpValues(ddlAddVatRate)
    End Sub
#End Region

#Region "ddl Index Changed"
    'Amend repair ddl action
    Protected Sub ddlAmendVatRate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAmendVatRate.SelectedIndexChanged
        uiMessageHelper.resetMessage(lblmessageAmendRepair, pnlMessageAmendRepair)
        PopulateVatValues("AmendVat")
        mdlAmendRepairPopup.Show()
    End Sub
    'Add repair ddl action
    Protected Sub ddlAddVatRate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAddVatRate.SelectedIndexChanged
        uiMessageHelper.resetMessage(lblmessageAddRepair, pnlMessageAddRepair)
        PopulateVatValues("AddVat")
        mdlAddRepairPopup.Show()



    End Sub
#End Region

#Region "Method to get Vat Values"

    Private Sub PopulateVatValues(ByVal ddlName As String)
        Dim objFault As New FaultManagementBL
        Dim vatBO As New VatBO()
        Try
            If (ddlName = "AddVat" AndAlso ddlAddVatRate.SelectedValue <> "" AndAlso ddlAddVatRate.SelectedIndex <> -1) Then
                vatBO.VatID = CType(ddlAddVatRate.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
                calculatevat(vatBO, ddlName)
            ElseIf (ddlName = "AmendVat" AndAlso ddlAmendVatRate.SelectedValue <> "" AndAlso ddlAmendVatRate.SelectedIndex <> -1) Then
                vatBO.VatID = CType(ddlAmendVatRate.SelectedValue, Int32)
                objFault.GetVatValues(vatBO)
                calculatevat(vatBO, ddlName)
            End If
        Catch ex As Exception
            vatBO.IsExceptionGenerated = True
            vatBO.ExceptionMsg = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Amend repair popup buttons"

    Protected Sub btnAmendBack_Click(sender As Object, e As EventArgs) Handles btnAmendBack.Click
        uiMessageHelper.resetMessage(lblmessageAmendRepair, pnlMessageAmendRepair)
        UpdPnllblMessageAmendRepair.Update()
        uiMessageHelper.IsError = False
        mdlAmendRepairPopup.Hide()
    End Sub

    Protected Sub btnAmendRepair_Click(sender As Object, e As EventArgs) Handles btnAmendRepair.Click
        SaveAmendRepair()
    End Sub
#End Region

#Region "Add repair popup buttons"
    Protected Sub btnAddRepair_Click(sender As Object, e As EventArgs) Handles btnAddRepair.Click
        SaveNewRepair()
    End Sub

    Protected Sub btnAddBack_Click(sender As Object, e As EventArgs) Handles btnAddBack.Click
        uiMessageHelper.resetMessage(lblmessageAddRepair, pnlMessageAddRepair)
        UpdPnllblMessageAddRepair.Update()
        uiMessageHelper.IsError = False
        mdlAddRepairPopup.Hide()
    End Sub
#End Region

#Region "Methods for Amending a Repair"

    Protected Sub SaveAmendRepair()
        Dim repairBO As RepairsBO = New RepairsBO()
        Try

            If Not AmendRepair() = True Then
                repairBO.FaultRepairListId = SessionManager.getFaultRepairListId()
                repairBO.RepairDescription = CType(txtAmendDescription.Text, String)
                repairBO.NetCost = CType(txtAmendNet.Text, Double)
                repairBO.Vat = CType(txtAmendVat.Text, Double)
                repairBO.GrossCost = CType(txtAmendGross.Text, Double)
                repairBO.PostInspection = CType(chkBoxAmendPostInspection.Checked, Boolean)
                repairBO.StockConditionItem = CType(chkBoxAmendStockCondition.Checked, Boolean)
                repairBO.IsActive = CType(chkBoxAmendIsActive.Checked, Boolean)
                If (ddlAmendVatRate.Items.Count > 0) Then
                    repairBO.VatRateId = CType(ddlAmendVatRate.SelectedItem.Value, Integer)
                Else
                    repairBO.VatRateId = Nothing
                End If
                objReportsBl.AmendRepair(repairBO)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateRepairDatabaseList(resultDataSet, search, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.message = UserMessageConstants.RepairAmendedSuccess


            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.RepairAmendingException
            uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, uiMessageHelper.message, True)
            UpdPnllblMessageAmendRepair.Update()
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, uiMessageHelper.message, True)
                UpdPnllblMessageAmendRepair.Update()
            Else
                uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, uiMessageHelper.message, False)
                UpdPnllblMessageAmendRepair.Update()
            End If
            mdlAmendRepairPopup.Show()
        End Try
    End Sub



    Private Function AmendRepair() As Boolean
        Dim errorMessage As String = String.Empty
        uiMessageHelper.resetMessage(lblmessageAmendRepair, pnlMessageAmendRepair)
        UpdPnllblMessageAmendRepair.Update()

        'Check repair to Amend for valid values.
        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")

        If txtAmendDescription.Text = "" Then
            errorMessage = UserMessageConstants.RepairDescriptionError
            uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, errorMessage, True)
            UpdPnllblMessageAmendRepair.Update()
            mdlAmendRepairPopup.Show()
            uiMessageHelper.IsError = True
        ElseIf txtAmendNet.Text = "" Or Not re.IsMatch(txtAmendNet.Text) Then
            errorMessage = UserMessageConstants.RepairCostError
            uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, errorMessage, True)
            UpdPnllblMessageAmendRepair.Update()
            mdlAmendRepairPopup.Show()
            uiMessageHelper.IsError = True
        ElseIf ddlAmendVatRate.Items.Count > 0 AndAlso (ddlAmendVatRate.SelectedValue = "" OrElse ddlAmendVatRate.SelectedValue = "-1") Then
            errorMessage = UserMessageConstants.RepairVatRateSelectionError
            uiMessageHelper.setMessage(lblmessageAmendRepair, pnlMessageAmendRepair, errorMessage, True)
            UpdPnllblMessageAmendRepair.Update()
            mdlAmendRepairPopup.Show()
            uiMessageHelper.IsError = True
        Else
            uiMessageHelper.resetMessage(lblmessageAmendRepair, pnlMessageAmendRepair)
            UpdPnllblMessageAmendRepair.Update()
            uiMessageHelper.IsError = False

        End If
        Return uiMessageHelper.IsError
    End Function

#End Region

#Region "Methods for adding a new Repair"
    Protected Sub SaveNewRepair()
        Dim repairBO As RepairsBO = New RepairsBO()
        Try
            If Not AddNewRepair() = True Then
                repairBO.RepairDescription = CType(txtAddDescription.Text, String)
                repairBO.NetCost = CType(txtAddNet.Text, Double)
                repairBO.Vat = CType(txtAddVat.Text, Double)
                repairBO.GrossCost = CType(txtAddGross.Text, Double)
                repairBO.PostInspection = CType(chkBoxAddPostInspection.Checked, Boolean)
                repairBO.StockConditionItem = CType(chkBoxAddStockCondition.Checked, Boolean)
                repairBO.IsActive = CType(chkBoxAddIsActive.Checked, Boolean)
                If (ddlAddVatRate.Items.Count > 0) Then
                    repairBO.VatRateId = CType(ddlAddVatRate.SelectedItem.Value, Integer)
                Else
                    repairBO.VatRateId = Nothing
                End If
                objReportsBl.AddNewRepair(repairBO)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateRepairDatabaseList(resultDataSet, search, True)
                uiMessageHelper.IsError = False
                uiMessageHelper.message = UserMessageConstants.RepairAddingSuccess


            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.RepairAddingException
            uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, uiMessageHelper.message, True)
            UpdPnllblMessageAddRepair.Update()
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, uiMessageHelper.message, True)
                UpdPnllblMessageAddRepair.Update()
            Else
                uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, uiMessageHelper.message, False)
                UpdPnllblMessageAddRepair.Update()
            End If
            mdlAddRepairPopup.Show()
        End Try
    End Sub



    Private Function AddNewRepair() As Boolean
        Dim errorMessage As String = String.Empty
        uiMessageHelper.resetMessage(lblmessageAddRepair, pnlMessageAddRepair)
        UpdPnllblMessageAddRepair.Update()

        'Check fault to Add for valid values.
        Dim re As Regex = New Regex("^[0-9]*[\.]?[0-9]+$")

        If txtAddDescription.Text = "" Then
            errorMessage = UserMessageConstants.RepairDescriptionError
            uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, errorMessage, True)
            UpdPnllblMessageAddRepair.Update()
            mdlAddRepairPopup.Show()
            uiMessageHelper.IsError = True
        ElseIf txtAddNet.Text = "" Or Not re.IsMatch(txtAddNet.Text) Then
            errorMessage = UserMessageConstants.RepairCostError
            uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, errorMessage, True)
            UpdPnllblMessageAddRepair.Update()
            mdlAddRepairPopup.Show()
            uiMessageHelper.IsError = True
        ElseIf ddlAddVatRate.Items.Count > 0 AndAlso (ddlAddVatRate.SelectedValue = "" OrElse ddlAddVatRate.SelectedValue = "-1") Then
            errorMessage = UserMessageConstants.RepairVatRateSelectionError
            uiMessageHelper.setMessage(lblmessageAddRepair, pnlMessageAddRepair, errorMessage, True)
            UpdPnllblMessageAddRepair.Update()
            mdlAddRepairPopup.Show()
            uiMessageHelper.IsError = True
        Else
            uiMessageHelper.resetMessage(lblmessageAddRepair, pnlMessageAddRepair)
            UpdPnllblMessageAddRepair.Update()
            uiMessageHelper.IsError = False


        End If
        Return uiMessageHelper.IsError
    End Function
#End Region

#Region "Populate associated faults list"
    Sub populateAssociatedFaultList(ByRef resultDataSet As DataSet, ByVal repairId As Integer)
        Try
            Dim faultRepairList As New List(Of FaultRepairBO)
            totalCountAssociatedFaultd = objReportsBl.getAssociatedFaultList(resultDataSet, repairId, objPageSortBoAssociatedFaults)
            lblRepair.Text = resultDataSet.Tables(0).Rows(0).Item("RepairDescription").ToString()

            ViewState.Add(ViewStateConstants.repairId, repairId)
            For Each dr As DataRow In resultDataSet.Tables(0).Rows
                Dim objFaultRepairBO As New FaultRepairBO
                Dim tempFaultRepairList As New List(Of FaultRepairBO)
                If faultRepairList.Count = 0 Then

                    objFaultRepairBO.FaultID = Integer.Parse(dr.Item("FaultID").ToString())
                    objFaultRepairBO.RepairId = Integer.Parse(dr.Item("RepairId").ToString())
                    objFaultRepairBO.FaultDescription = dr.Item("FaultDescription").ToString()
                    objFaultRepairBO.RepairDescription = dr.Item("RepairDescription").ToString()
                    faultRepairList.Add(objFaultRepairBO)
                Else
                    Dim faultId As Integer = Integer.Parse(dr.Item("FaultID").ToString())
                    Dim indexOfItem As Integer = -1
                    For Each objfaultRepairList In faultRepairList
                        If objfaultRepairList.FaultID = faultId Then
                            indexOfItem = faultRepairList.IndexOf(objfaultRepairList)
                        End If
                    Next

                    objFaultRepairBO.FaultID = Integer.Parse(dr.Item("FaultID").ToString())
                    objFaultRepairBO.RepairId = Integer.Parse(dr.Item("RepairId").ToString())
                    objFaultRepairBO.FaultDescription = dr.Item("FaultDescription").ToString()
                    objFaultRepairBO.RepairDescription = dr.Item("RepairDescription").ToString()
                    faultRepairList.Add(objFaultRepairBO)

                End If

            Next

            totalCountAssociatedFaultd = faultRepairList.Count
            objPageSortBoAssociatedFaults.TotalRecords = totalCountAssociatedFaultd
            objPageSortBoAssociatedFaults.TotalPages = Math.Ceiling(Convert.ToDouble(totalCountAssociatedFaultd / objPageSortBoAssociatedFaults.PageSize))

            Session("faultRepairList") = faultRepairList
            GridAssociatedFaultsList.DataSource = GetPage(faultRepairList, objPageSortBoAssociatedFaults.PageNumber - 1, objPageSortBoAssociatedFaults.PageSize)

            GridAssociatedFaultsList.DataBind()
            GridAssociatedFaultsList.VirtualItemCount = totalCountAssociatedFaultd

            GeneralHelper.setGridViewPagerAssociatedFaults(PnlPaginationAssociatedFaults, objPageSortBoAssociatedFaults)
            setPageSortBoViewStateAssociatedFaults(objPageSortBoAssociatedFaults)
            If GridAssociatedFaultsList.PageCount > 1 AndAlso objPageSortBoAssociatedFaults.PageNumber = GridAssociatedFaultsList.PageCount Then
                setVirtualItemCountViewState(faultRepairList.Count)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessageAssociatedFaults, pnlMsgAssociatedFaults, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

    Private Function GetPage(list As IList(Of FaultRepairBO), page As Integer, pageSize As Integer) As List(Of FaultRepairBO)
        Return list.Skip(page * pageSize).Take(pageSize).ToList()
    End Function

#Region "Associated fault button click"
    Protected Sub ViewAssociatedFault(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim resultDataSet As DataSet = New DataSet()

            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim repairId As Integer = CType(linkBtn.CommandArgument, Integer)
            populateAssociatedFaultList(resultDataSet, repairId)
            mdlAssociatedFaultsPopup.Show()
        Catch ex As Exception
        Finally

        End Try
    End Sub
#End Region

#Region "Associated fault popup button actions"
    Protected Sub btnAssociatedFaultsBack_Click(sender As Object, e As EventArgs) Handles btnAssociatedFaultsBack.Click
        mdlAssociatedFaultsPopup.Hide()
    End Sub
#End Region

#Region "Associated faults popup pagination"

    Protected Sub lnkbtnPagerAssociatedFaults_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerAssociatedFaultsFirst.Click _
        , lnkbtnPagerAssociatedFaultsLast.Click, lnkbtnPagerAssociatedFaultsPrev.Click, lnkbtnPagerAssociatedFaultsNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBoAssociatedFaults = getPageSortBoViewStateAssociatedFaults()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBoAssociatedFaults.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBoAssociatedFaults.PageNumber = objPageSortBoAssociatedFaults.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBoAssociatedFaults.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBoAssociatedFaults.PageNumber -= 1
            End If
            Dim resultDataSet As DataSet = New DataSet()
            'populateAssociatedFaultList(resultDataSet, CType(ViewState(ViewStateConstants.repairId), Integer))

            Dim faultRepairList As New List(Of FaultRepairBO)
            faultRepairList = TryCast(Session("faultRepairList"), List(Of FaultRepairBO))

            objPageSortBoAssociatedFaults.TotalRecords = faultRepairList.Count
            objPageSortBoAssociatedFaults.TotalPages = Math.Ceiling(faultRepairList.Count / objPageSortBoAssociatedFaults.PageSize)
            Dim index As Integer = objPageSortBoAssociatedFaults.PageNumber
            If index > 0 Then
                index = index - 1
            End If
            GridAssociatedFaultsList.DataSource = GetPage(faultRepairList, index, objPageSortBoAssociatedFaults.PageSize)

            GridAssociatedFaultsList.DataBind()
            GeneralHelper.setGridViewPagerAssociatedFaults(PnlPaginationAssociatedFaults, objPageSortBoAssociatedFaults)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub


#End Region

#Region "Export to Excel"

#Region "Export Repairs List"
    ''' <summary>
    ''' Export Repairs List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportRepairsList()

        Dim grdRepairsList As New GridView()

        Dim associatedFaults As New Dictionary(Of String, String)
        associatedFaults.Add("RepairDescription", "Repair Description")
        associatedFaults.Add("Fault", "Associated Fault Description")
        associatedFaults.Add("Location", "Location(s)")
        associatedFaults.Add("NetCost", "Net £")
        associatedFaults.Add("VATRate", "VAT Rate")
        associatedFaults.Add("VAT", "VAT £")
        associatedFaults.Add("Gross", "Gross £")
        associatedFaults.Add("PostInspection", "Post Inspection")
        associatedFaults.Add("SCI", "SCI")
        associatedFaults.Add("IsActive", "Repair Active")

        generateGridForExcel(grdRepairsList, associatedFaults)

        objPageSortBo = getPageSortBoViewState()
        Dim search As String = ViewState(ViewStateConstants.Search)
        Dim isFullList As Boolean = True
        Dim resultDataSet As New DataSet
        objReportsBl.getReactiveRepairListExportToExcel(resultDataSet, search)
        grdRepairsList.DataSource = resultDataSet
        grdRepairsList.DataBind()

        Dim fileNamePrefix As String = "Repairs Database"
        ExportGridToExcel(grdRepairsList, fileNamePrefix)

    End Sub
#End Region

#Region "Export Associated faults List"
    ''' <summary>
    ''' Export Associated faults List
    ''' </summary>
    ''' <remarks></remarks>
    Sub exportAssociatedFaults()

        Dim grdAssociatedFaults As New GridView()

        Dim associatedFaults As New Dictionary(Of String, String)
        associatedFaults.Add("FaultDescription", "Fault Description:")        
        generateGridForExcel(grdAssociatedFaults, associatedFaults)

        Dim faultRepairList As New List(Of FaultRepairBO)
        faultRepairList = TryCast(Session("faultRepairList"), List(Of FaultRepairBO))
        grdAssociatedFaults.DataSource = faultRepairList
        grdAssociatedFaults.DataBind()

        Dim fileNamePrefix As String = faultRepairList.First().RepairDescription
        ExportGridToExcel(grdAssociatedFaults, fileNamePrefix)

    End Sub
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView, ByVal fileNamePrefix As String)

        Dim fileName As String = String.Format("{0}_{1}_{2}_{3}", fileNamePrefix, Convert.ToString(DateTime.Now.Day), Convert.ToString(DateTime.Now.Month), Convert.ToString(DateTime.Now.Year))

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = System.Text.Encoding.Default

        Response.ContentType = "application/vnd.ms-excel"
        Dim swExcel As New StringWriter()
        Dim hwExcel As New HtmlTextWriter(swExcel)

        grdViewObject.RenderControl(hwExcel)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(swExcel.ToString())
        Response.Flush()
        Response.End()

    End Sub

#End Region

#Region "Generate Grid For Excel"

    Sub generateGridForExcel(ByRef gridView As GridView, ByVal gridInfoDictionary As Dictionary(Of String, String))

        gridView.AllowPaging = False
        gridView.AutoGenerateColumns = False
        gridView.ShowFooter = True

        For Each kvp As KeyValuePair(Of String, String) In gridInfoDictionary
            createBoundField(gridView, kvp.Value, kvp.Key)
        Next

    End Sub

#End Region

#Region "Create Bound field"

    Sub createBoundField(ByRef gridView As GridView, ByVal headerText As String, ByVal dataField As String)

        Dim boundField As BoundField = New BoundField()
        boundField.HeaderText = headerText
        boundField.DataField = dataField
        gridView.Columns.Add(boundField)

    End Sub

#End Region

#End Region

End Class