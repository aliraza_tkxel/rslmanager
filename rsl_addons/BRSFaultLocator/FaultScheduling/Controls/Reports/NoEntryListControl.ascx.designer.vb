﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class NoEntryListControl

    '''<summary>
    '''pnlMessage1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlSchemBlockddl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSchemBlockddl As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''SchemeBlockDDL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SchemeBlockDDL As Global.FaultScheduling.schemeBlockDropdown

    '''<summary>
    '''ucFinancialYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucFinancialYear As Global.FaultScheduling.FinancialYear

    '''<summary>
    '''hdnAppointmentType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAppointmentType As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''grdNoEntryList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdNoEntryList As Global.Fadrian.Web.Control.PagingGridView

    '''<summary>
    '''pnlPagination control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPagination As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lnkbtnPagerFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerFirst As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerPrev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerPrev As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblPagerCurrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerCurrentPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerTotalPages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerTotalPages As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordStart As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordEnd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkbtnPagerNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerNext As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerLast As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''rangevalidatorPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rangevalidatorPageNumber As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''txtPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageNumber As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnGoPageNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGoPageNumber As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnHiddenEntry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHiddenEntry As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''popupNoEntry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents popupNoEntry As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''pnlNoEntry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlNoEntry As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRecallDetailsFor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecallDetailsFor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlPropertyData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPropertyData As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblClientNameHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientNameHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientStreetAddressHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientStreetAddressHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientCityHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientCityHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientTelHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientTelHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientTelPhoneNumberHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientTelPhoneNumberHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlSchemeBlock control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSchemeBlock As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblSchemeLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSchemeLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSchemeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSchemeName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBlockLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBlockLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBlockName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBlockName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultId As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlPropertyDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPropertyDetail As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblClientName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientStreetAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientStreetAddress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientCity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientPostCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientPostCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientRegion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientRegion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientTelPhoneNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientTelPhoneNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientMobileNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientMobileNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblClientEmailId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClientEmailId As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContractor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContractor As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOperativeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOperativeName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultPriority As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultCompletionDateTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultCompletionDateTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOrderDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOrderDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultLocation As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultDescription As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTrade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFaultOperatorNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFaultOperatorNote As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNoEntryNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNoEntryNote As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnCloseNoEntryPopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseNoEntryPopup As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnScheduleAppointment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnScheduleAppointment As Global.System.Web.UI.WebControls.Button
End Class
