﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SubcontractorListControl.ascx.vb"
    Inherits="FaultScheduling.SubcontractorListControl" %>
<%@ Register Src="~/Controls/Reports/schemeBlockDropdown.ascx" TagName="SchemeBlockDropdown"
    TagPrefix="ucSchemeBlockddl" %>
<%@ Register Src="~/Controls/Reports/FinancialYear.ascx" TagName="FinancialYear"
    TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<asp:UpdatePanel ID="updPnlSubContractorList" runat="server">
    <ContentTemplate>
        <style>
            .dashboard, .dashboard td {
                padding: 5px 0px 5px 10px !important;
            }
        </style>
        <asp:Panel ID="pnlMessage1" runat="server" Visible="False">
            <asp:Label ID="lblMessage1" runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlSchemBlockddl">
             <div class="">
                <ucSchemeBlockddl:SchemeBlockDropdown ID="SchemeBlockDDL" runat="server"></ucSchemeBlockddl:SchemeBlockDropdown>
                <uc:FinancialYear ID="ucFinancialYear" runat="server" style="width: 95px;">
                </uc:FinancialYear>
                 <div class="form-control">
                    <div class="select_div">
                        <div class="">
                            <asp:DropDownList runat="server" ID="ddlStatusFilter" AutoPostBack="true" Style="width: 170px;"
                                OnSelectedIndexChanged="ddlStatusFilter_SelectedIndexChanged">
                                <asp:ListItem Text="All" Selected="True" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
            <cc2:PagingGridView ID="grdSubcontractorList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                OnRowCreated="grdSubcontractorList_RowCreated" 
                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
                <Columns>
                    <asp:TemplateField HeaderText="JSN" ItemStyle-CssClass="dashboard" SortExpression="FaultLogID">
                        <ItemTemplate>
                            <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Reported" SortExpression="ReportedSort">
                        <ItemTemplate>
                            <asp:Label ID="lblReported" runat="server" Text='<%# Bind("Reported") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                        <ItemTemplate>
                            <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Block" SortExpression="Block">
                        <ItemTemplate>
                            <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location" SortExpression="Location">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" SortExpression="Description">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contractor" SortExpression="Subcontractor">
                        <ItemTemplate>
                            <asp:Label ID="lblSubcontractor" runat="server" Text='<%# Bind("Subcontractor") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Assigned" SortExpression="AssignedSort">
                        <ItemTemplate>
                            <asp:Label ID="lblAssigned" runat="server" Text='<%# Bind("Assigned") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="By" SortExpression="UserInitials">
                        <ItemTemplate>
                            <asp:Label ID="lblBy" runat="server" Text='<%# Bind("UserInitials") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Completion Due" SortExpression="CompletionDueSort">
                        <ItemTemplate>
                            <asp:Label ID="lblCompletionDue" runat="server" Text='<%# Bind("CompletionDue") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnViewSubcontractorJobsheet" runat="server" CommandArgument='<%#Eval("JSN").ToString() + ";" + Eval("PROPERTYID").ToString() %>'
                                Text="View" OnClick="grdSubcontractorList_SelectedIndexChanged" UseSubmitBehavior="False"
                                CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
            </cc2:PagingGridView>
        </div>

        <%--Pager Template Start--%>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
            <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                <div class="paging-left">
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                            &lt;&lt;First
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                            &lt;Prev
                        </asp:LinkButton>
                    </span>
                    <span style="padding-right:10px;">
                        <b>Page:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                        of
                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                    </span>
                    <span style="padding-right:20px;">
                        <b>Result:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                        to
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                        of
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                    </span>
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                        </asp:LinkButton>
                    </span>
                </div>
                <div style="float: right;">
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                    <div class="field" style="margin-right: 10px;">
                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    </div>
                    <span>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                    </span>
                </div>
            </div>
        </asp:Panel>
        <%--Pager Template End--%>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="updPnlJobSheetSummary" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlJobSheetSummary" runat="server" CssClass="modalPopup">
            <div id="header-Popup">
                <p id="header_label">
                    BHG Fault Locator
                </p>
                <div id="header_box">
                </div>
            </div>
            <br style="border-style: none; border-color: #FFFFFF">
            <div style="height: auto; overflow: auto; clear: both;">
                <table style="width: 100%; margin-top: 10px;">
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : "
                                Font-Bold="true"></asp:Label>
                            <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                            &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                            &nbsp;,
                            <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                            &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel :" Font-Bold="true"> </asp:Label><asp:Label
                                ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            <asp:Label ID="lblFaultLogID" runat="server" Visible="false" />
                            <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static" Font-Bold="True"></asp:Label>
                        </td>
                        <td width="40%">
                            <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Font-Bold="True">Status : </asp:Label>
                            <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                            <asp:DropDownList ID="ddlStatus" runat="server" Visible="False" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td rowspan="10" width="44%" valign="top">
                            <div style='width: 90%;'>
                                <asp:Panel ID="pnlPropertyData" runat="server">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tel : &nbsp;<asp:Label ID="lblClientTelPhoneNumber" runat="server" Text=" " Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Mobile: &nbsp;
                                                <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Email :
                                                <asp:Label ID="lblClientEmail" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <%-- Completed Fault Panel--%>
                                <asp:Panel ID="pnlRepairDetail" runat="server" Visible="false">
                                    <div class="repairDetailContainer">
                                        <table width="95%" style='margin: 10px;'>
                                            <tr>
                                                <td>
                                                    Complete Fault:
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" Style="float: right;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Completed:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCompleted" runat="server" Style="float: left;"></asp:TextBox>
                                                    <asp:Image ID="imgCalendar" runat="server" Height="23px" Width="25px" ImageUrl="~/Images/calendar.png"
                                                        Style="float: left;" />
                                                    <cc1:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtCompleted"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtCompleted"
                                                        WatermarkText="DD/MM/YYYY">
                                                    </cc1:TextBoxWatermarkExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Time:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlHours" runat="server" Width="50px">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlMin" runat="server" Width="50px">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlFormate" runat="server" Width="50px">
                                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                                        <asp:ListItem Value="PM">PM</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Repair:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSearch" runat="server" Width="92%" AutoPostBack="false" AutoCompleteType="Search"></asp:TextBox>
                                                    <cc1:AutoCompleteExtender ID="autoComplete1" runat="server" EnableCaching="true"
                                                        BehaviorID="AutoCompleteEx" TargetControlID="txtSearch" ServicePath="../../Views/Reports/ReportsArea.aspx"
                                                        ServiceMethod="getRepairSearchResult" CompletionInterval="1000" CompletionSetCount="20"
                                                        CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="complitionList"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="ClientItemSelected">
                                                    </cc1:AutoCompleteExtender>
                                                    <div id="complitionList">
                                                    </div>
                                                    <br />
                                                    <div style='float: right; margin-right: 5%; margin-top: 2px;'>
                                                        <asp:Button ID="btnAdd" runat="server" Text="Add" align="right" /></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <div class="repairsGridContainer" id="repairsGridContainer" runat="server" visible="false">
                                                        <asp:GridView ID="grdRepairs" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                            BorderStyle="None" GridLines="None" Width="98%">
                                                            <Columns>
                                                                <asp:BoundField DataField="Description">
                                                                    <ItemStyle Width="80%" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete"><img src="../../Images/cross2.png" alt="Delete Fault"  style="border:none; width:16px; " />  </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Repair Notes:
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtBoxRepairNotes" TextMode="MultiLine" Style="width: 230px;
                                                        height: 40px;"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Follow on works required?:
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlFollowOnWorks">
                                                        <asp:ListItem Value="1" Text="Yes">Yes</asp:ListItem>
                                                        <asp:ListItem Value="0" Text="No">No</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    Follow on notes:
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtBoxFollowOnNotes" TextMode="MultiLine" Style="width: 230px;
                                                        height: 40px;"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <div style='float: right; margin-right: 5%;'>
                                                        <%-- <asp:Button ID="btnCancel" runat="server" Text="Cancel" />&nbsp;&nbsp;&nbsp;
                                                        <asp:Button ID="btnSave" runat="server" Text="Save" /></div>--%>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <%-- No Entry Panel--%>
                                <asp:Panel ID="pnlNoEntryDetail" runat="server" Visible="false">
                                    <div class="repairDetailContainer">
                                        <table width="95%" style='margin: 10px;'>
                                            <tr>
                                                <td>
                                                    No Entry:
                                                </td>
                                                <td>
                                                    <asp:Button runat="server" ID="btnCancelNoEntry" Text="Cancel" Style="float: right;" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Completed:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtNoEntry" runat="server" Style="float: left;"></asp:TextBox>
                                                    <asp:Image ID="imgNoEntryCalendar" runat="server" Height="23px" Width="25px" ImageUrl="~/Images/calendar.png"
                                                        Style="float: left;" />
                                                    <cc1:CalendarExtender ID="calExtNoEntry" runat="server" TargetControlID="txtNoEntry"
                                                        Format="dd/MM/yyyy" PopupButtonID="imgNoEntryCalendar" />
                                                    <cc1:TextBoxWatermarkExtender ID="txtWater" runat="server" TargetControlID="txtNoEntry"
                                                        WatermarkText="DD/MM/YYYY">
                                                    </cc1:TextBoxWatermarkExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Time:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlNoEntryHours" runat="server" Width="50px">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlNoEntryMin" runat="server" Width="50px">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlNoEntryFormate" runat="server" Width="50px">
                                                        <asp:ListItem Value="AM">AM</asp:ListItem>
                                                        <asp:ListItem Value="PM">PM</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contractor :
                        </td>
                        <td>
                            <asp:Label ID="lblContractor" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td>
                            Operative :
                        </td>
                        <td>
                            <asp:Label ID="lblOperativeName" runat="server">-</asp:Label>
                        </td>
                    </tr>--%>
                    <tr>
                        <td>
                            Priority :
                        </td>
                        <td>
                            <asp:Label ID="lblPriority" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Completion due :
                        </td>
                        <td>
                            <asp:Label ID="lblCompletionDateTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Order date :
                        </td>
                        <td>
                            <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Location :
                        </td>
                        <td>
                            <asp:Label ID="lblLocation" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Description:
                        </td>
                        <td>
                            <asp:Label ID="lblDescription" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Trade :
                        </td>
                        <td>
                            <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Note :
                        </td>
                        <td>
                            <asp:TextBox ID="lblNotes" CssClass="roundcornerby5" runat="server" Enabled="false"
                                TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <b>Appointment:</b>
                        </td>
                        <td valign="top">
                            <asp:Label ID="lblTime" runat="server" Text="N/A"></asp:Label><br />
                            <br />
                        </td>
                        <td>
                            <b>Asbestos:</b>
                            <div style="height: 20px; overflow: auto;">
                            </div>
                        </td>
                    </tr>
                    <tr >
                    <td></td>
                    <td></td>
                        <td colspan="3">
                            <ajaxToolkit:ModalPopupExtender ID="mdlPopupJobSheetSummarySubContractorUpdate" runat="server"
                                PopupControlID="pnlJobSheetSummary" TargetControlID="lblJSN" BackgroundCssClass="modalBackground"
                                CancelControlID="btnBack">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" 
                                BorderStyle="None" GridLines="None" ShowHeader="False">
                                <Columns>
                                    <asp:BoundField DataField="AsbRiskID" />
                                    <asp:BoundField DataField="Description">
                                    <ItemStyle Wrap="False" />
                                    </asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="width: 100%; text-align: left; clear: both;">
                                <div class="text_a_r_padd__r_20px">
                                    <asp:Button ID="btnBack" runat="server" Text=" &lt; Back" CssClass="margin_right20"
                                        Width="100px" />
                                    <asp:Button ID="btnUpdate" runat="server" CssClass="margin_right20" Text="Update"
                                        Width="100px" />
                                    <input id="btnPrintJobSheetSummary" type="button" value="Print Job Sheet" class="margin_right20"
                                        onclick="PrintJobSheet()" />
                                    <asp:Button ID="btnReschedule" runat="server" Text="Reschedule" Visible="false" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel runat="server" ID="updPnlPopupFaultNotes" UpdateMode="Conditional">
    <ContentTemplate>
        <table class="modalPopupNotes">
            <tr>
                <td>
                    <asp:Label ID="lblPopupHeaderSaveNotes" runat="server" Font-Bold="true" Visible="true" />
                    <asp:ImageButton ID="imgBtnClosePopup" runat="server" Style="position: absolute;
                        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlMessagePopupFaultNotes" runat="server" Visible="false">
                        <asp:Label ID="lblMessagePopupFaultNotes" runat="server">
                        </asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPopupNotesHeading" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlPopupSaveMsg" runat="server" Font-Bold="true" Visible="false">
                        <asp:Label ID="lblPopupSaveMsg" runat="server" Text="The fault has been canceled" />
                        <br />
                        <asp:LinkButton ID="lnkBtnCustomerRecord" runat="server" Text="Click here" />&nbsp;
                        to return to the customers record</asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mdlPopupSaveNotes" runat="server" TargetControlID="lblPopupNotesHeading"
                        PopupControlID="updPnlPopupFaultNotes" BackgroundCssClass="modalBackground" CancelControlID="imgBtnClosePopup">
                    </ajaxToolkit:ModalPopupExtender>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnPopupSave" runat="server" Text="Yes" />
                    <asp:Button ID="btnNo" runat="server" Text="No" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<%-- MESSAGE POPUP --%>
<asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
<ajaxToolkit:ModalPopupExtender ID="mdlPopupMessage" runat="server" TargetControlID="btnHidden3"
    PopupControlID="pnlUserMessage" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
    CancelControlID="btnClose">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlUserMessage" CssClass="left" runat="server" BackColor="White" Width="350px"
    Style="border: 1px solid black; font-weight: normal; font-size: 13px;">
    <br />
    <div>
        <table style="width: 100%; margin-top: -16px; margin-bottom: 20px;">
            <tr style="background-color: Gray; height: 40px;">
                <td align="center">
                    <asp:Label ID="lblMessageStatus" Font-Bold="true" ForeColor="White" runat="server"
                        Text="Assign To Contractor JobSheet"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblUserMessage" runat="server" Text="Assign To Contractor JobSheet Summary not implemented."></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    &nbsp
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnClose" CssClass="btn" Width="70px" runat="server" Text="Ok" Style="margin: 2px;" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
