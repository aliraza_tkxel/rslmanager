﻿Imports FLS_BusinessLogic
Imports FLS_Utilities
Imports FLS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Globalization
Imports System.IO
Imports System.Net.Mail
Imports System.Net

Public Class SubcontractorListControl
    Inherits UserControlBase
    Private customerId As Integer = 0
    Private propertyID As String


#Region "Delegates"

    Public Delegate Sub Filters_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

#End Region

#Region "Events"

    Public Event FiltersChanged As Filters_Changed

#End Region

#Region "Properties"

    Dim objReportsBl As ReportsBL = New ReportsBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO(ApplicationConstants.Descending, "FaultLogID", 1, 30)
    Dim totalCount As Integer = 0
    Dim FaultBL As New FaultBL()
    Dim jsnClass As String = String.Empty
    
    Public ReadOnly Property SchemeBlock As schemeBlockDropdown
        Get
            Return SchemeBlockDDL
        End Get
    End Property

#End Region

#Region "Events Handling"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString(PathConstants.RejectedByContractor) = LCase("true") Then
            ddlStatusFilter.SelectedItem.Value = 21 'redirect user to rejected by contractor report
        End If

        uiMessageHelper.resetMessage(lblMessage1, pnlMessage1)
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        uiMessageHelper.resetMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes)

    End Sub

#End Region

#Region "Grid Subcontractor List Page Index Changing Event"

    Protected Sub grdSubcontractorList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdSubcontractorList.PageIndexChanging

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdSubcontractorList.PageIndex = e.NewPageIndex
            grdSubcontractorList.DataBind()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateSubcontractorList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Subcontractor List Sorting Event"

    Protected Sub grdSubcontractorList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdSubcontractorList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdSubcontractorList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()

            populateSubcontractorList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdSubcontractorList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Drop Down List Events"

#Region "ddlStatus_SelectedIndexChanged"
    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        changeUpadteSaveButtonEnable()
    End Sub
#End Region

#Region "ddlStatusFilter_SelectedIndexChanged"
    Protected Sub ddlStatusFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStatusFilter.SelectedIndexChanged
        Dim search As String = ViewState.Item(ViewStateConstants.Search)
        Dim resultDataSet As DataSet = New DataSet()
        populateSubcontractorList(resultDataSet, search, False)

        If resultDataSet.Tables(0).Rows.Count = 0 Then
            uiMessageHelper.setMessage(DirectCast(Parent.FindControl("lblMessage"), Label), DirectCast(Parent.FindControl("pnlMessage"), Panel), UserMessageConstants.FilterReportNotFound, True)
            Dim updPanelReportArea As UpdatePanel = DirectCast(Parent.FindControl("updPanelReportArea"), UpdatePanel)
            'updPanelReportArea.Update()
        Else
            uiMessageHelper.resetMessage(DirectCast(Parent.FindControl("lblMessage"), Label), DirectCast(Parent.FindControl("pnlMessage"), Panel))
            Dim updPanelReportArea As UpdatePanel = DirectCast(Parent.FindControl("updPanelReportArea"), UpdatePanel)
            'updPanelReportArea.Update()
        End If
    End Sub
#End Region
#End Region

#Region "Drop Down List Events"



#End Region

#Region "Button Click Events"

#Region "Button Click View Button Grid View"

    Protected Sub grdSubcontractorList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles grdSubcontractorList.SelectedIndexChanged
        Try
            Dim btnView As Button = CType(sender, Button)
            Dim commandArgument As String = btnView.CommandArgument.ToString
            Dim argument As String() = commandArgument.Split(";")
            viewSubcontractorJobsheet(argument(0).ToString)
            'If argument(1).ToString() = "NA" Then
            '    mdlPopupMessage.Show()
            'Else
            '    viewSubcontractorJobsheet(argument(0).ToString)
            'End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#Region "Save Popup Button Click Event"

    Protected Sub btnPopupSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupSave.Click
        Try
            saveUpdatedFaultStatus()
            mdlPopupJobSheetSummarySubContractorUpdate.Hide()
            mdlPopupSaveNotes.Hide()
            uiMessageHelper.message = "Fault is completed successfuly"
            uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try

        reBindGridForLastPage()
    End Sub

#End Region

#Region "Update Fault with selected status and status Notes"

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
        updateFault()
        reBindGridForLastPage()
    End Sub

#End Region

#Region "Job Sheet Summary Back Button Click / Close the Pop up Window"

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        uiMessageHelper.resetMessage(lblMessage1, pnlMessage1)
        reBindGridForLastPage()
    End Sub

#End Region

    'End Region Button Click Events
#End Region

#Region "Link Button Click"

    Protected Sub lnkBtnCustomerRecord_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnCustomerRecord.Click
        Try
            mdlPopupSaveNotes.Hide()
            'mdlPopUpSaveAlert.Hide()
            mdlPopupJobSheetSummarySubContractorUpdate.Hide()
            Dim resultDataSet As New DataSet
            populateSubcontractorList(resultDataSet, ViewState(ViewStateConstants.Search), False)
            uiMessageHelper.setMessage(lblMessage1, pnlMessage1, UserMessageConstants.FaultStatusUpdateSucessful, False)
            'uiMessageHelper.setMessage(lblMessage1, pnlMessage1, "Fault Status Updated Sucessfully.", False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#Region "Image Button Click"

    Protected Sub imgBtnClosePopup_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgBtnClosePopup.Click
        Try
            mdlPopupJobSheetSummarySubContractorUpdate.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
        reBindGridForLastPage()
    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateSubcontractorList(resultDataSet, search, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateSubcontractorList(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "btnAdd Add repair click event"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim hdnSelectedRepairId As HiddenField = CType(Me.Parent.FindControl("hdnSelectedRepairId"), HiddenField)
        Dim faultRepairListID As String = hdnSelectedRepairId.Value
        Dim dtRepair As DataTable = New DataTable()
        dtRepair = SessionManager.getRepairsDataTable()
        If txtSearch.Text <> String.Empty And faultRepairListID <> String.Empty Then

            If dtRepair.Columns.Count = 0 Then
                dtRepair.Columns.Add(New DataColumn(ApplicationConstants.FaultRepairListID))
                dtRepair.Columns.Add(New DataColumn(ApplicationConstants.FaultRepairDescription))
            End If
            Dim repairResult = (From ps In dtRepair Where ps.Item(ApplicationConstants.FaultRepairListID).ToString() = faultRepairListID Select ps)
            If repairResult.Count > 0 Then
                Dim ErrorMesg = UserMessageConstants.RepairAlreadyAdded '"The Trade has already been added."
                uiMessageHelper.setMessage(lblMessage, pnlMessage, ErrorMesg, True)
            Else
                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                'Create a new row
                Dim dr = dtRepair.NewRow()
                dr(ApplicationConstants.FaultRepairListID) = faultRepairListID
                dr(ApplicationConstants.FaultRepairDescription) = txtSearch.Text
                dtRepair.Rows.Add(dr)
                SessionManager.setRepairsDataTable(dtRepair)
            End If
            repairsGridContainer.Visible = True
        End If
        grdRepairs.DataSource = dtRepair
        grdRepairs.DataBind()
        txtSearch.Text = String.Empty
        mdlPopupJobSheetSummarySubContractorUpdate.Show()
    End Sub
#End Region

#Region "grdRepairs Row deleting event"
    ''' <summary>
    ''' grdRepairs Row deleting event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdRepairs_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles grdRepairs.RowDeleting
        Try
            Dim dtRepairs As DataTable = New DataTable()
            dtRepairs = SessionManager.getRepairsDataTable()

            If dtRepairs IsNot Nothing AndAlso dtRepairs.Rows.Count > 0 Then
                dtRepairs.Rows.RemoveAt(e.RowIndex)
                dtRepairs.AcceptChanges()
                SessionManager.setRepairsDataTable(dtRepairs)
                grdRepairs.DataSource = dtRepairs
                grdRepairs.DataBind()
            End If
            mdlPopupJobSheetSummarySubContractorUpdate.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Cancel click event"
    ''' <summary>
    ''' btn Cancel click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            resetAssignToContractor()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Cancel No Entry click event"
    ''' <summary>
    ''' btn Cancel No Entry click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancelNoEntry_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelNoEntry.Click
        Try
            resetAssignToContractor()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Save Repair click event"
    ' ''' <summary>
    ' ''' btn save repair click event
    ' ''' </summary>
    ' ''' <param name="sender"></param>
    ' ''' <param name="e"></param>
    ' ''' <remarks></remarks>
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
    '    Try
    '        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    '        If ValidateRepairData() Then
    '            Dim objRepairListBo As RepairListBO = New RepairListBO()
    '            Dim objReportBl As ReportsBL = New ReportsBL()
    '            objRepairListBo.FaultLogId = CType(lblFaultLogID.Text, Integer)
    '            objRepairListBo.InspectionDate = CType(txtCompleted.Text, DateTime)
    '            objRepairListBo.Time = ddlHours.SelectedValue + ":" + ddlMin.SelectedValue + " " + ddlFormate.SelectedValue
    '            Dim sb As StringBuilder = New StringBuilder()
    '            Dim dtRepair As DataTable = SessionManager.getRepairsDataTable()
    '            For Each row As DataRow In dtRepair.Rows
    '                sb.Append(row(ApplicationConstants.FaultRepairListID))
    '                sb.Append(",")
    '            Next
    '            objRepairListBo.FaultRepairIdList = sb.ToString().Substring(0, sb.ToString().Length - 1)
    '            'Dim str As String = sb.ToString()
    '            'str.Substring(0, str.Length - 1)
    '            objRepairListBo.UserId = 615
    '            objReportBl.InsertRepairListData(objRepairListBo)
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.RepairSavedSuccessfuly, False)
    '        End If

    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
    '        End If
    '        mdlPopupJobSheetSummarySubContractorUpdate.Show()
    '    End Try
    'End Sub
#End Region

#Region "Button No Click"

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNo.Click
        Try
            txtBoxRepairNotes.Enabled = True
            txtCompleted.Enabled = True
            txtBoxFollowOnNotes.Enabled = True
            ddlFollowOnWorks.Enabled = True
            cntrlCalendarExtender.Enabled = True
            txtSearch.Enabled = True
            ddlHours.Enabled = True
            ddlMin.Enabled = True
            ddlFormate.Enabled = True
            btnAdd.Enabled = True
            grdRepairs.Enabled = True
            btnCancel.Enabled = True
            mdlPopupJobSheetSummarySubContractorUpdate.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Scheme or Block Or Financial Year Changed"
    Private Sub Filters_FilterChanged(sender As Object, e As System.EventArgs) Handles SchemeBlockDDL.blockChanged, SchemeBlockDDL.schemeChanged _
        , ucFinancialYear.financialYearChanged
        RaiseEvent FiltersChanged(sender, e)
    End Sub
#End Region

    'End Region Events
#End Region

#Region "Functions"

#Region "Reset Assign to Contractor Jobsheet"
    ''' <summary>
    ''' Reset Assign to Contractor Jobsheet
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetAssignToContractor()

        txtSearch.Text = String.Empty
        SessionManager.removeRepairsDataTable()
        grdRepairs.DataSource = Nothing
        grdRepairs.DataBind()
        'Reset all Controls
        txtBoxRepairNotes.Enabled = True
        txtCompleted.Enabled = True
        txtBoxFollowOnNotes.Enabled = True
        ddlFollowOnWorks.Enabled = True
        cntrlCalendarExtender.Enabled = True
        txtSearch.Enabled = True
        ddlHours.Enabled = True
        ddlMin.Enabled = True
        ddlFormate.Enabled = True
        btnAdd.Enabled = True
        grdRepairs.Enabled = True
        btnCancel.Enabled = True
        txtBoxRepairNotes.Text = String.Empty
        txtCompleted.Text = String.Empty
        txtNoEntry.Text = String.Empty
        txtBoxFollowOnNotes.Text = String.Empty

        repairsGridContainer.Visible = False
        pnlRepairDetail.Visible = False
        pnlNoEntryDetail.Visible = False
        pnlPropertyData.Visible = True
        mdlPopupJobSheetSummarySubContractorUpdate.Show()
        ddlStatus.SelectedValue = 2
        btnUpdate.Enabled = False

    End Sub

#End Region

#Region "Bind To Grid"

    Sub bindToGrid()
        ViewState.Item(ViewStateConstants.Search) = ""
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)

        grdSubcontractorList.VirtualItemCount = totalCount
        grdSubcontractorList.DataSource = resultDataSet
        grdSubcontractorList.DataBind()

        objPageSortBo = getPageSortBoViewState()

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdSubcontractorList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdSubcontractorList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Populate Subcontractor List"

    Sub populateSubcontractorList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)
        If Not IsPostBack Then
            PopulateStatusLookup(ddlStatusFilter)
            ddlStatusFilter.SelectedItem.Text.Contains("Contractor")
        End If

        If setSession = True Then
            removePageSortBoViewState()
            getPageSortBoViewState()
        Else
            objPageSortBo = getPageSortBoViewState()
        End If

        ViewState.Add(ViewStateConstants.Search, search)

        totalCount = objReportsBl.getSubcontractorList(resultDataSet, search, objPageSortBo, ddlStatusFilter.SelectedValue, SchemeBlockDDL.SchemeId, SchemeBlockDDL.BlockId, ucFinancialYear.FinancialYear)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        If Not resultDataSet.Tables(0).Rows.Count = 0 Then

            If setSession Then
                ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
                setPageSortBoViewState(objPageSortBo)
                ViewState.Add(ViewStateConstants.TotalCount, totalCount)
            End If

        End If

        grdSubcontractorList.VirtualItemCount = totalCount
        grdSubcontractorList.DataSource = resultDataSet
        grdSubcontractorList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdSubcontractorList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdSubcontractorList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "View Subcontractor JobSheet"

    Protected Sub viewSubcontractorJobsheet(ByVal jSN As String)
        Try
            showPopupJobSheetSummary(jSN)
            pnlPropertyData.Visible = True
            pnlRepairDetail.Visible = False
            pnlNoEntryDetail.Visible = False
            resetRepairControls()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try

    End Sub

#End Region

#Region "Get Job Sheed Summary Detail By Jsn(Job Sheed Number)"

    Private Sub populateJobSheetSummary(ByRef jsn As String)

        Dim custObj As New CustomerBO()

        Dim dsJobSheetSummary As New DataSet()

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)

        FaultBL.getJobSheetSummaryByJsn(dsJobSheetSummary, jsn)

        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0)

                lblFaultLogID.Text = .Item("FaultLogID")
                lblJSN.Text = jsn
                lblStatus.Text = .Item("FaultStatus") '"In Progress"
                If Not String.Compare(lblStatus.Text, "Rejected by Contractor") Then
                    btnReschedule.Visible = True 'enable schedule button

                    ' set customer data
                    custObj.Name = .Item("ClientName")
                    custObj.Street = .Item("ClientStreetAddress")
                    custObj.City = .Item("ClientCity")
                    custObj.PostCode = .Item("ClientPostCode")
                    custObj.Telephone = .Item("ClientTel")

                    ' save customer data in session
                    SessionManager.setCustomerData(custObj)

                    'set propertyid and customerid in session
                    Me.propertyID = .Item("PropertyID")
                    Me.customerId = .Item("CustomerId")
                    SessionManager.setCustomerId(Me.customerId)
                    SessionManager.setPropertyId(Me.propertyID)
                    SessionManager.setFaultLogId(Integer.Parse(lblFaultLogID.Text))

                End If
                lblContractor.Text = .Item("Contractor")

                lblPriority.Text = .Item("priority")
                lblCompletionDateTime.Text = .Item("completionDue")
                lblOrderDate.Text = .Item("orderDate")
                lblLocation.Text = .Item("Location")
                lblDescription.Text = .Item("Description")
                lblTrade.Text = .Item("Trade")
                lblNotes.Text = .Item("Notes")

                lblClientName.Text = .Item("ClientName")
                lblClientNameHeader.Text = lblClientName.Text

                lblClientStreetAddress.Text = .Item("ClientStreetAddress")
                lblClientStreetAddressHeader.Text = lblClientStreetAddress.Text

                lblClientCity.Text = .Item("ClientCity")
                lblClientCityHeader.Text = lblClientCity.Text

                lblClientPostCode.Text = .Item("ClientPostCode")
                lblClientRegion.Text = .Item("ClientRegion")

                lblClientTelPhoneNumber.Text = .Item("ClientTel")
                lblClientTelPhoneNumberHeader.Text = lblClientTelPhoneNumber.Text

                lblClientMobileNumber.Text = .Item("ClientMobile")
                lblClientEmail.Text = .Item("ClientEmail")
                ViewState(ViewStateConstants.CustomerId) = .Item("CustomerId")

                'for rescheduling rejected faults





            End With
            If lblStatus.Text.ToUpper() = "complete".ToUpper() Then
                btnUpdate.Enabled = False
            Else
                btnUpdate.Enabled = True
            End If
        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

#End Region

#Region "Update Fault Status"

    Private Sub updateFault()
        Try
            txtBoxRepairNotes.Enabled = True
            txtCompleted.Enabled = True
            txtBoxFollowOnNotes.Enabled = True
            ddlFollowOnWorks.Enabled = True
            cntrlCalendarExtender.Enabled = True
            txtSearch.Enabled = True
            ddlHours.Enabled = True
            ddlMin.Enabled = True
            ddlFormate.Enabled = True
            btnAdd.Enabled = True
            grdRepairs.Enabled = True
            btnCancel.Enabled = True
            If btnUpdate.Text = "Update" Then
                btnUpdate.Text = "Save"


                lblStatus.Visible = False
                ddlStatus.Visible = Not (lblStatus.Visible)
                ' PopulateStatusLookup(ddlStatus)
                PopulateStatusLookupOnJobSheetPopup(ddlStatus)

                changeUpadteSaveButtonEnable()

            ElseIf btnUpdate.Text = "Save" Then
                'btnUpdate.Text = "Update"
                If ddlStatus.SelectedItem.Value = 17 Then
                    Me.SaveCompleteFaultData()
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
                        mdlPopupJobSheetSummarySubContractorUpdate.Show()
                        Exit Sub
                    End If
                ElseIf ddlStatus.SelectedItem.Value = 14 Then
                    Me.ValidateNoEntryData()
                    If uiMessageHelper.IsError = True Then
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
                        mdlPopupJobSheetSummarySubContractorUpdate.Show()
                        Exit Sub
                    End If
                End If

                ' changeUpadteSaveButtonEnable()
                lblPopupHeaderSaveNotes.Text = String.Empty
                lblPopupNotesHeading.Text = String.Empty

                lblPopupHeaderSaveNotes.Visible = True
                lblPopupNotesHeading.Visible = True
                imgBtnClosePopup.Visible = True
                pnlPopupSaveMsg.Visible = False
                'txtPopupNotesFault.Text = String.Empty
                btnPopupSave.Visible = True
                mdlPopupJobSheetSummarySubContractorUpdate.Show()
                If (ddlStatus.Items.Count > 0 AndAlso ddlStatus.SelectedValue = 13) Then
                    lblPopupHeaderSaveNotes.Text = UserMessageConstants.PopupHeaderSaveNotesCancelFault '"Cancel Fault"
                    lblPopupNotesHeading.Text = UserMessageConstants.PopupSaveCancelFault '"Enter the reason for cancellation below:"
                ElseIf (ddlStatus.Items.Count > 0 AndAlso ddlStatus.SelectedValue = 14) Then
                    lblPopupHeaderSaveNotes.Text = UserMessageConstants.PopupHeaderSaveNotesNoEntry '"No Entry"
                    lblPopupNotesHeading.Text = UserMessageConstants.PopupSaveNoEntryFault ' "Enter the reason for no entry below:"
                ElseIf (ddlStatus.Items.Count > 0 AndAlso ddlStatus.SelectedValue = 17 AndAlso uiMessageHelper.IsError = False) Then
                    txtBoxRepairNotes.Enabled = False
                    txtCompleted.Enabled = False
                    txtBoxFollowOnNotes.Enabled = False
                    ddlFollowOnWorks.Enabled = False
                    cntrlCalendarExtender.Enabled = False
                    txtSearch.Enabled = False
                    ddlHours.Enabled = False
                    ddlMin.Enabled = False
                    ddlFormate.Enabled = False
                    btnAdd.Enabled = False
                    grdRepairs.Enabled = False
                    btnCancel.Enabled = False
                    lblPopupHeaderSaveNotes.Text = UserMessageConstants.PopupHeaderSaveNotesCompleteFault '"Complete Fault"
                    lblPopupNotesHeading.Text = UserMessageConstants.PopupHeadingSaveNotesCompleteFault '"Enter the notes for completion below:"                    
                    lblPopupNotesHeading.Text = UserMessageConstants.PopupSaveCompleteFault

                End If
                mdlPopupSaveNotes.Show()
                updPnlPopupFaultNotes.Update()
            End If
            'mdlPopupJobSheetSummarySubContractorUpdate.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try


    End Sub

    Private Sub saveUpdatedFaultStatus()

        'If Trim(txtPopupNotesFault.Text) <> String.Empty Then
        uiMessageHelper.IsError = False
        setFaultStatusSubContractorUpdate()
        If uiMessageHelper.IsError Then
            mdlPopupJobSheetSummarySubContractorUpdate.Show()
            'mdlPopupSaveNotes.Show()
            uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, uiMessageHelper.message, uiMessageHelper.IsError)
            Exit Sub
        End If
        pnlPopupSaveMsg.Visible = True
        imgBtnClosePopup.Visible = False
        btnPopupSave.Visible = False
        Me.setLinkUrlForCustRec()
        'Else
        '    uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, UserMessageConstants.PopupNotesErrorSaveNotes, True)
        '    'uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, "Please provide notes.", True)
        'End If
        'mdlPopupJobSheetSummarySubContractorUpdate.Show()
        'mdlPopupSaveNotes.Show()
        'mdlPopUpSaveAlert.Show()
    End Sub

#End Region

#Region "Show Job Sheet Summary Sub Contractor Update"

    Private Sub showPopupJobSheetSummary(ByRef jsn As String)
        Me.resetControls()
        Me.populateJobSheetSummary(jsn)
        Me.reBindGridForLastPage()
        Me.mdlPopupJobSheetSummarySubContractorUpdate.Show()

    End Sub

#End Region

#Region "Save/Set Fault Stauts in Database with Notes"

    Private Sub setFaultStatusSubContractorUpdate()
        Try
            Dim faultBO As New FaultBO
            faultBO.FaultID = CType(lblFaultLogID.Text, Integer)
            faultBO.StatusID = CType(ddlStatus.SelectedValue, Integer)
            'Dim notes As String = txtPopupNotesFault.Text
            ''Truncate the notes if the length is more than 1000 (notes limit)
            'If notes.Length > 1000 Then
            '    notes = notes.Substring(0, 1000)
            'End If
            faultBO.IsFollowOn = CType(ddlFollowOnWorks.SelectedItem.Value, Boolean)
            If faultBO.IsFollowOn Then
                faultBO.FollowOnNotes = txtBoxFollowOnNotes.Text
            End If

            faultBO.Notes = txtBoxRepairNotes.Text
            'faultBO.UserID = 65
            faultBO.UserID = SessionManager.getUserEmployeeId()
            faultBO.SubmitDate = DateTime.Now
            If ddlStatus.SelectedItem.Text.Equals("No Entry") Then
                faultBO.NoEntryDate = Convert.ToDateTime(txtNoEntry.Text + " " + ddlNoEntryHours.SelectedValue + ":" + ddlNoEntryMin.SelectedValue + " " + ddlNoEntryFormate.SelectedValue)
                faultBO.CompletedDate = faultBO.NoEntryDate
            ElseIf ddlStatus.SelectedItem.Text.Equals("Complete") Then
                faultBO.CompletedDate = Convert.ToDateTime(txtCompleted.Text + " " + ddlHours.SelectedValue + ":" + ddlMin.SelectedValue + " " + ddlFormate.SelectedValue)
            End If
            FaultBL.setFaultStatusSubContractorUpdate(faultBO)

            If ddlStatus.SelectedItem.Text.Equals("Cancelled") Then
                Dim sendEmail As Integer = FaultBL.cancelFaultPurchaseOrder(faultBO)
                If (sendEmail > 0) Then
                    sendCancelledEmailtoContractor(faultBO)
                End If
            End If


            txtBoxFollowOnNotes.Text = String.Empty
            txtBoxRepairNotes.Text = String.Empty
            txtCompleted.Text = String.Empty

            If faultBO.IsFlagStatus Then
                uiMessageHelper.IsError = False
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = faultBO.UserMsg
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, uiMessageHelper.message, uiMessageHelper.IsError)
            Else
                uiMessageHelper.resetMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes)
                pnlPopupSaveMsg.Visible = True
            End If
        End Try
    End Sub

#End Region

#Region "Reset All Controls to default values"

    Private Sub resetControls()
        lblStatus.Visible = True
        ddlStatus.Visible = Not (lblStatus.Visible)

        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        lblFaultLogID.Text = String.Empty
        lblJSN.Text = String.Empty
        lblStatus.Text = String.Empty
        lblContractor.Text = String.Empty
        lblPriority.Text = String.Empty
        lblCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblLocation.Text = String.Empty
        lblDescription.Text = String.Empty
        lblTrade.Text = String.Empty
        lblNotes.Text = String.Empty
        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmail.Text = String.Empty

        btnUpdate.Enabled = True
        btnUpdate.Text = "Update"

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()

    End Sub

#End Region

#Region "Change Update/Save Button Enable"

    Private Sub changeUpadteSaveButtonEnable()
        Try
            If (ddlStatus.Items.Count > 0 AndAlso (ddlStatus.SelectedValue = 2 OrElse ddlStatus.SelectedValue = 15)) Then
                btnUpdate.Enabled = False
            Else
                btnUpdate.Enabled = True
            End If
            If ddlStatus.SelectedValue = 17 Then
                pnlRepairDetail.Visible = True
                pnlPropertyData.Visible = False
                pnlNoEntryDetail.Visible = False
                populateHours(ddlHours)
                populateMin(ddlMin)
                ddlFollowOnWorks.SelectedIndex = 0
            ElseIf ddlStatus.SelectedValue = 14 Then
                pnlRepairDetail.Visible = False
                pnlPropertyData.Visible = False
                pnlNoEntryDetail.Visible = True
                resetNoEntryControls()
                populateHours(ddlNoEntryHours)
                populateMin(ddlNoEntryMin)

            Else
                pnlRepairDetail.Visible = False
                pnlNoEntryDetail.Visible = False
                pnlPropertyData.Visible = True
            End If

            mdlPopupJobSheetSummarySubContractorUpdate.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        'Rebind the Grid to prevent show empty lines for last page only
        Try
            uiMessageHelper.IsError = False
            objPageSortBo = getPageSortBoViewState()
            Dim resultDataSet As DataSet = New DataSet()
            Dim search As String = String.Empty
            populateSubcontractorList(resultDataSet, search, True)




            'Comment this code for refershing grid on setting the fault.
            'If grdSubcontractorList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdSubcontractorList.PageCount Then
            '    grdSubcontractorList.VirtualItemCount = getVirtualItemCountViewState()
            '    grdSubcontractorList.PageIndex = objPageSortBo.PageNumber - 1
            '    grdSubcontractorList.DataSource = getResultDataSetViewState()
            '    grdSubcontractorList.DataBind()

            '    GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
            'End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, uiMessageHelper.IsError)
            End If
        End Try
    End Sub

#End Region

#Region "Set Link Url For Customer Record"
    Private Sub setLinkUrlForCustRec()
        Me.lnkBtnCustomerRecord.PostBackUrl = PathConstants.CustomerRecords + CType(ViewState(ViewStateConstants.CustomerId), String)
    End Sub
#End Region

#Region "populate ddlHour and ddlMin"

#Region "populate Hours"

    Private Sub populateHours(ByRef ddlHour As DropDownList)

        ddlHour.Items.Clear()
        For i As Integer = 1 To 12
            ddlHour.Items.Add(New ListItem((i).ToString("D2"), (i).ToString("D2")))
        Next i
    End Sub
#End Region

#Region "populate Min"

    Private Sub populateMin(ByRef ddlMins As DropDownList)

        ddlMins.Items.Clear()
        For i As Integer = 0 To 59
            ddlMins.Items.Add(New ListItem((i).ToString("D2"), (i).ToString("D2")))
        Next i
    End Sub
#End Region

#End Region

#Region "Reset Repair controls"
    ''' <summary>
    ''' Reset Repair controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub resetRepairControls()

        txtSearch.Text = String.Empty
        txtCompleted.Text = String.Empty
        txtBoxRepairNotes.Text = String.Empty
        txtBoxFollowOnNotes.Text = String.Empty

        ddlFormate.SelectedIndex = 0

        SessionManager.removeRepairsDataTable()
        grdRepairs.DataSource = Nothing
        grdRepairs.DataBind()

        repairsGridContainer.Visible = False
        'ddlStatus.SelectedIndex = 0
        btnUpdate.Enabled = True
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub
#End Region

#Region "Reset No Entry controls"
    ''' <summary>
    ''' Reset No Entry  controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub resetNoEntryControls()

        txtSearch.Text = String.Empty
        txtNoEntry.Text = String.Empty

        ddlNoEntryFormate.SelectedIndex = 0
        repairsGridContainer.Visible = False
        btnUpdate.Enabled = True
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
    End Sub
#End Region

#Region "Validate repair data"
    ''' <summary>
    ''' validate repair data
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateRepairData() As Boolean
        Dim result As Boolean = True
        If txtCompleted.Text = "DD/MM/YYY" Or txtCompleted.Text = String.Empty Then
            Dim dateTimeText As Date
            Dim isValidDate As Boolean = Date.TryParseExact(txtCompleted.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)
            If isValidDate = False Then
                result = False
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidCompleteddate, True)
            End If
        ElseIf CType(txtCompleted.Text, DateTime) > DateTime.Today Or Convert.ToDateTime(txtCompleted.Text + " " + ddlHours.SelectedValue + ":" + ddlMin.SelectedValue + " " + ddlFormate.SelectedValue) < Convert.ToDateTime(lblOrderDate.Text) Then
            result = False
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidCompleteddatoeday, True)
        End If
        If uiMessageHelper.IsError = False Then
            Dim dtRepair As DataTable = SessionManager.getRepairsDataTable()
            If dtRepair.Rows.Count = 0 Then
                result = False
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AddAtLeastRepair, True)
            End If
        End If
        Return result
    End Function
#End Region

#Region "Validate No Entry data"
    ''' <summary>
    ''' Validate No Entry
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidateNoEntryData() As Boolean
        Dim result As Boolean = True
        Dim selectedDate As DateTime = Convert.ToDateTime(txtNoEntry.Text + " " + ddlNoEntryHours.SelectedValue + ":" + ddlNoEntryMin.SelectedValue + " " + ddlNoEntryFormate.SelectedValue)
        Dim orderDate As DateTime = Convert.ToDateTime(lblOrderDate.Text)
        If txtNoEntry.Text = "DD/MM/YYY" Or txtNoEntry.Text = String.Empty Then
            Dim dateTimeText As Date
            Dim isValidDate As Boolean = Date.TryParseExact(txtNoEntry.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, dateTimeText)
            If isValidDate = False Then
                result = False
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidNoEntrydate, True)
            End If
        ElseIf CType(txtNoEntry.Text, DateTime) > DateTime.Today Then
            result = False
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidNoEntryDateday, True)

        ElseIf selectedDate < orderDate Then
            result = False
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidCompletedDateForNoEntry, True)
        End If

        Return result
    End Function
#End Region

#Region " Save Complete Fault Data"
    ''' <summary>
    ''' Save Complete Fault Data
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SaveCompleteFaultData()
        Dim selectTime As DateTime = New DateTime()
        Dim currTime As DateTime = New DateTime()
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If ValidateRepairData() Then
                Dim objRepairListBo As RepairListBO = New RepairListBO()
                Dim objReportBl As ReportsBL = New ReportsBL()
                objRepairListBo.FaultLogId = CType(lblFaultLogID.Text, Integer)
                objRepairListBo.InspectionDate = CType(txtCompleted.Text, DateTime)
                objRepairListBo.Time = ddlHours.SelectedValue + ":" + ddlMin.SelectedValue + " " + ddlFormate.SelectedValue
                selectTime = CType(objRepairListBo.Time, DateTime)
                Dim selectTimeinInt As Integer
                currTime = CType(TimeString, DateTime)
                selectTimeinInt = selectTime.CompareTo(currTime)

                If objRepairListBo.InspectionDate.Date = DateTime.Now.Date Then
                    If selectTimeinInt > 0 Then
                        uiMessageHelper.IsError = True
                        uiMessageHelper.message = UserMessageConstants.TimenotgreaterthancurrTime
                        uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, uiMessageHelper.IsError)
                        Exit Sub
                    End If
                End If
                Dim sb As StringBuilder = New StringBuilder()
                Dim dtRepair As DataTable = SessionManager.getRepairsDataTable()
                For Each row As DataRow In dtRepair.Rows
                    sb.Append(row(ApplicationConstants.FaultRepairListID))
                    sb.Append(",")
                Next
                objRepairListBo.FaultRepairIdList = sb.ToString().Substring(0, sb.ToString().Length - 1)
                objRepairListBo.UserId = SessionManager.getFaultSchedulingUserId()
                objReportBl.InsertRepairListData(objRepairListBo)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
            mdlPopupJobSheetSummarySubContractorUpdate.Show()
        End Try
    End Sub
#End Region

#Region "Populate Status Lookup on main page"
    Private Sub PopulateStatusLookup(ByRef ddlStatus As DropDownList)
        Dim LookUpList As New List(Of LookUpBO)

        FaultBL.getFaultStatusLookUpSubContractor(LookUpList)

        ddlStatus.Items.Clear()
        ddlStatus.DataSource = LookUpList
        ddlStatus.DataValueField = "LookUpValue"
        ddlStatus.DataTextField = "LookUpName"
        ddlStatus.DataBind()
        If Request.QueryString.Get("rbc") = "true" Then
            ddlStatus.SelectedValue = 21
        Else
            ddlStatus.SelectedValue = 2
        End If
    End Sub
#End Region


#Region "Populate Status Lookup on job sheet popup"
    Private Sub PopulateStatusLookupOnJobSheetPopup(ByRef ddlStatus As DropDownList)
        Dim LookUpList As New List(Of LookUpBO)

        FaultBL.getFaultStatusLookUpSubContractor(LookUpList)
        ddlStatus.Items.Clear()
        ddlStatus.DataSource = LookUpList.Take(4)
        ddlStatus.DataValueField = "LookUpValue"
        ddlStatus.DataTextField = "LookUpName"
        ddlStatus.DataBind()
        If (ddlStatus.Items.Contains(ddlStatus.Items.FindByValue("2"))) Then
            ddlStatus.SelectedValue = 2
        End If
    End Sub
#End Region



#Region "Load Financial Years in Financial Years Drop down"
    Public Sub loadFinancialYears()
        ucFinancialYear.loadFinancialYears()
    End Sub
#End Region

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        Else
            pageSortBO = New PageSortBO(ApplicationConstants.Descending, "FaultLogID", 1, 30)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
    '    Try
    '        txtSearch.Text = String.Empty
    '        SessionManager.removeRepairsDataTable()
    '        grdRepairs.DataSource = Nothing
    '        grdRepairs.DataBind()
    '        repairsGridContainer.Visible = False
    '        mdlPopupJobSheetSummarySubContractorUpdate.Show()
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If

    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
    '        End If
    '    End Try
    'End Sub


#Region "reschedule button click action"

    Protected Sub btnReschedule_Click(sender As Object, e As EventArgs) Handles btnReschedule.Click





        Response.Redirect("~/Views/Faults/FaultBasket.aspx?src=atc")

    End Sub
#End Region

#Region "Populate Body and Send Cancelled Email to Contractor"

    Private Sub sendCancelledEmailtoContractor(ByRef faultBO As FaultBO)
        Dim detailsForEmailDS As New DataSet()
        FaultBL.getEmailDetail(faultBO, detailsForEmailDS)
        Dim socket
        If Request.ServerVariables("HTTPS") = "on" Then
            socket = "https"
        Else
            socket = "http"
        End If
        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then

                If (DBNull.Value.Equals(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email"))) Then
                    Throw New Exception("email is not sent as email id of contractor is not available.")
                End If


                If String.IsNullOrEmpty(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) _
                    OrElse Not isEmail(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email")) Then

                    Throw New Exception("Unable to send email, invalid email address.")
                Else

                    Dim body As New StringBuilder
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/CancelPurchaseOrder.html"))
                    body.Append(reader.ReadToEnd())

                    ' Set contractor detail(s) '
                    '==========================================='
                    'Populate Contractor Contact Name
                    body.Replace("{ContractorName}", detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName"))
                    '==========================================='
                    ' Populate work, estimate and cost details
                    '==========================================='

                    'Set Order Id 
                    body.Replace("{JSNumber}", faultBO.FaultID)

                    ' Set JSN (Reactive Repair work reference)
                    body.Replace("{PONumber}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("POID"))


                    body.Replace("{Address}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("FullAddress"))

                    body.Replace("{Block}", detailsForEmailDS.Tables(ApplicationConstants.BlockDetailsdt)(0)("BlockName"))

                    body.Replace("{TownCity}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("TOWNCITY"))

                    body.Replace("{Postcode}", detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)("POSTCODE"))

                    body.Replace("{WorkRequired}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("WorkRequired"))

                    'Get sum/total of net cost from works required data table.
                    body.Replace("{NetCost}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("NetCost").ToString)

                    'Get sum/total of Vat from works required data table.
                    body.Replace("{VAT}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("VAT").ToString())

                    'Get sum/total of Gross/Total from works required data table.
                    body.Replace("{Total}", detailsForEmailDS.Tables(ApplicationConstants.FaultInfoDt)(0)("Gross").ToString())

                    '==========================================='
                    ' There is an asbestos present at the below property. Please contact Broad land Housing for details.
                    ' Attach logo images with email
                    '==========================================='


                    Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                    logo50Years.ContentId = "logo50Years_Id"

                    body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    alternatevw.LinkedResources.Add(logo50Years)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)



                    'Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/PropertyModule/broadland.png"))
                    'logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    'body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    'Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")
                    'Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    'alternatevw.LinkedResources.Add(logoBroadLandRepairs)

                    '==========================================='

                    Dim mailMessage As New MailMessage

                    mailMessage.Subject = ApplicationConstants.EmailSubject
                    mailMessage.To.Add(New MailAddress(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("Email").ToString, detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)(0)("ContractorContactName").ToString))

                    ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                    mailMessage.Body = body.ToString
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)

                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub
#End Region

#Region "Is Email"
    Public Shared Function isEmail(ByVal value As String) As Boolean
        Dim rgx As New Regex(RegularExpConstants.emailExp)
        If rgx.IsMatch(value) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region


End Class