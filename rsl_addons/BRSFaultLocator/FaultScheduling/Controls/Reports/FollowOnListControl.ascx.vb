﻿Imports FLS_BusinessLogic
Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class FollowOnListControl
    Inherits UserControlBase

#Region "Delegates"

    Public Delegate Sub Filters_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

#End Region

#Region "Events"

    Public Event FiltersChanged As Filters_Changed

#End Region

#Region "Properties"

    Dim objReportsBl As ReportsBL = New ReportsBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 30)
    Dim totalCount As Integer = 0

    Public ReadOnly Property SchemeBlock As schemeBlockDropdown
        Get
            Return SchemeBlockDDL
        End Get
    End Property

#End Region

#Region "Events Handling"

#Region "Page Pre Render Event"
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.reBindGridForLastPage()
    End Sub
#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Grid Follow On List Page Index Changing Event"

    Protected Sub grdFollowOnList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdFollowOnList.PageIndexChanging

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdFollowOnList.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateFollowOnList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Follow On List Sorting Event"

    Protected Sub grdFollowOnList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdFollowOnList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdFollowOnList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()

            populateFollowOnList(resultDataSet, search, False)

            'Dim dt As DataTable = New DataTable()
            'dt = resultDataSet.Tables(0)

            'If IsNothing(dt) = False Then
            '    Dim dvSortedView As DataView = New DataView(dt)
            '    dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            '    grdFollowOnList.DataSource = dvSortedView
            '    grdFollowOnList.DataBind()
            'End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdFollowOnList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateFollowOnList(resultDataSet, search, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateFollowOnList(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Scheme or Block Or Financial Year Changed"
    Private Sub Filters_FilterChanged(sender As Object, e As System.EventArgs) Handles SchemeBlockDDL.blockChanged, SchemeBlockDDL.schemeChanged _
        , ucFinancialYear.financialYearChanged, ddlFollowOnStatus.SelectedIndexChanged
        RaiseEvent FiltersChanged(sender, e)
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"

    Sub bindToGrid()
        ViewState.Item(ViewStateConstants.Search) = String.Empty
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdFollowOnList.VirtualItemCount = totalCount
        grdFollowOnList.DataSource = resultDataSet
        grdFollowOnList.DataBind()

        objPageSortBo = getPageSortBoViewState()

        If grdFollowOnList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdFollowOnList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
    End Sub

#End Region

#Region "Get Follow On Work Detail"

    Protected Sub getFollowOnWorkDetail(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            ' Reset Job sheet controls
            resetJobsheetControls()

            Dim commandArgument As String = CType(CType(sender, Button).CommandArgument, String)
            Dim commandArg = commandArgument.Split(";")

            Dim jsn As String = commandArg(0)
            Dim propertyId As String = commandArg(1)
            If Not IsNothing(commandArg(2)) AndAlso commandArg(2) <> "" Then
                Dim customerId As Integer = Convert.ToInt32(commandArg(2))
                ViewState.Add(ViewStateConstants.CustomerId, customerId)
            End If
            If Not IsNothing(commandArg(5)) AndAlso commandArg(5) <> "" Then
                Dim shcemeId As Integer = Convert.ToInt32(commandArg(5))
                ViewState.Add(ViewStateConstants.SchemeId, shcemeId)
            End If
            If Not IsNothing(commandArg(6)) AndAlso commandArg(6) <> "" Then
                Dim blockId As Integer = Convert.ToInt32(commandArg(6))
                ViewState.Add(ViewStateConstants.BlockId, blockId)
            End If

            Dim faultLogId As Integer = Convert.ToInt32(commandArg(3))
            hdnAppointmentType.Value = commandArg(4)
            ViewState.Add(ViewStateConstants.PropertyId, propertyId)

            setFaultLogIdViewState(faultLogId)

            Dim resultDataSet As DataSet = New DataSet()

            objReportsBl.getFollowOnWorkDetail(resultDataSet, jsn)

            ' Job Sheet Detail
            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else

                lblFaultId.Text = resultDataSet.Tables(0).Rows(0)(0).ToString
                lblContractor.Text = resultDataSet.Tables(0).Rows(0)(1).ToString
                lblOperativeName.Text = resultDataSet.Tables(0).Rows(0)(2).ToString
                lblFaultPriority.Text = resultDataSet.Tables(0).Rows(0)(3).ToString
                lblFaultCompletionDateTime.Text = resultDataSet.Tables(0).Rows(0)(4).ToString
                lblOrderDate.Text = resultDataSet.Tables(0).Rows(0)(5).ToString
                lblFaultLocation.Text = resultDataSet.Tables(0).Rows(0)(6).ToString
                lblFaultDescription.Text = resultDataSet.Tables(0).Rows(0)(7).ToString
                lblFaultOperatorNote.Text = resultDataSet.Tables(0).Rows(0)(8).ToString
                lblTrade.Text = resultDataSet.Tables(0).Rows(0)(12).ToString
                txtBoxRepairNotes.Text = resultDataSet.Tables(0).Rows(0)("RepairNotes").ToString

            End If

            ' Customer Info
            If (resultDataSet.Tables(1).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else
                If hdnAppointmentType.Value = "Fault" Then
                    pnlPropertyData.Visible = True
                    pnlPropertyDetail.Visible = True
                    pnlSchemeBlock.Visible = False
                    ' Header Customer Labels
                    lblClientNameHeader.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                    lblClientStreetAddressHeader.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                    lblClientCityHeader.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                    lblClientTelPhoneNumberHeader.Text = resultDataSet.Tables(1).Rows(0)(5).ToString

                    ' Body customer Labels

                    lblClientName.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                    lblClientStreetAddress.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                    lblClientCity.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                    lblClientPostCode.Text = resultDataSet.Tables(1).Rows(0)(3).ToString
                    lblClientRegion.Text = resultDataSet.Tables(1).Rows(0)(4).ToString
                    lblClientTelPhoneNumber.Text = resultDataSet.Tables(1).Rows(0)(5).ToString
                    lblClientMobileNumber.Text = resultDataSet.Tables(1).Rows(0)(6).ToString
                    lblClientEmailId.Text = resultDataSet.Tables(1).Rows(0)(7).ToString
                Else
                    pnlPropertyData.Visible = False
                    pnlPropertyDetail.Visible = False
                    pnlSchemeBlock.Visible = True
                    lblSchemeName.Text = resultDataSet.Tables(1).Rows(0)("SchemeName").ToString()
                    lblBlockName.Text = resultDataSet.Tables(1).Rows(0)("BlockName").ToString()
                End If


            End If

            ' Repair Detail
            If (Not resultDataSet.Tables(2).Rows.Count = 0) Then
                grdRepairDetail.DataSource = resultDataSet.Tables(2)
                grdRepairDetail.DataBind()

            End If

            ' Follow On Work
            If (Not resultDataSet.Tables(3).Rows.Count = 0) Then
                lblFollowOnWork.Text = resultDataSet.Tables(3).Rows(0)(0).ToString
            End If

            reBindGridForLastPage()

            popupFollowOn.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Schedule Follow On Work"

    Protected Sub scheduleFollowOnWork(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            Dim propertyId As String = ViewState.Item(ViewStateConstants.PropertyId)
            Dim customerId As Integer = ViewState.Item(ViewStateConstants.CustomerId)
            Dim schemeId As Integer = ViewState.Item(ViewStateConstants.SchemeId)
            Dim blockId As Integer = ViewState.Item(ViewStateConstants.BlockId)
            Dim faultLogId As Integer = getFaultLogIdViewState()
            SessionManager.setPropertyId(propertyId)
            SessionManager.setCustomerId(customerId)
            SessionManager.setSchemeId(schemeId)
            SessionManager.setBlockId(blockId)

            'SessionManager.setIsFollowOn(True)
            SessionManager.setFollowOnFaultLogId(faultLogId)
            SessionManager.setSbFollowOnFaultLogIdlockFollowOnFaultLogId(faultLogId)
            If hdnAppointmentType.Value = "Fault" Then
                Response.Redirect(PathConstants.SearchFault + GeneralHelper.getSrc(PathConstants.ReportAreaSrcVal) + "&" + PathConstants.Jsn + "=" + lblFaultId.Text + "&" + PathConstants.isFollowOn + "=" + PathConstants.Yes)

            Else
                Response.Redirect(PathConstants.SearchSbFault + GeneralHelper.getSrc(PathConstants.ReportAreaSrcVal) + "&" + PathConstants.Jsn + "=" + lblFaultId.Text + "&" + PathConstants.isFollowOn + "=" + PathConstants.Yes)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try


    End Sub

#End Region

#Region "Reset Job sheet control "

    Protected Sub resetJobsheetControls()
        reBindGridForLastPage()

        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ClearPanelMessage, True)
        popupFollowOn.Hide()

        ' Job Sheet Detail
        lblFaultId.Text = String.Empty
        lblContractor.Text = String.Empty
        lblOperativeName.Text = String.Empty
        lblFaultPriority.Text = String.Empty
        lblFaultCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblFaultLocation.Text = String.Empty
        lblFaultDescription.Text = String.Empty
        lblFaultOperatorNote.Text = String.Empty
        lblTrade.Text = String.Empty

        ' Header Customer Labels
        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        ' Body customer Labels

        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmailId.Text = String.Empty


        grdRepairDetail.DataSource = Nothing

        lblFollowOnWork.Text = String.Empty

    End Sub

#End Region

#Region "Populate Follow On List"

    Sub populateFollowOnList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)

        If setSession = True Then
            removePageSortBoViewState()
            getPageSortBoViewState()
        Else
            objPageSortBo = getPageSortBoViewState()
        End If

        ViewState.Add(ViewStateConstants.Search, search)
        Dim followOnStatusId As Integer = Integer.Parse(ddlFollowOnStatus.SelectedValue)

        totalCount = objReportsBl.getFollowOnList(resultDataSet, search, objPageSortBo, SchemeBlockDDL.SchemeId, SchemeBlockDDL.BlockId, ucFinancialYear.FinancialYear, followOnStatusId)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        If setSession Then
            ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
            setPageSortBoViewState(objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        End If

        grdFollowOnList.VirtualItemCount = totalCount
        grdFollowOnList.DataSource = resultDataSet
        grdFollowOnList.DataBind()

        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdFollowOnList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdFollowOnList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        'Rebind the Grid to prevent show empty lines for last page only
        'Start - Changes By Aamir Waheed on April 11 2013

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdFollowOnList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdFollowOnList.PageCount Then
            grdFollowOnList.VirtualItemCount = getVirtualItemCountViewState()
            grdFollowOnList.PageIndex = objPageSortBo.PageNumber - 1
            grdFollowOnList.DataSource = getResultDataSetViewState()
            grdFollowOnList.DataBind()
        End If

        'End - Changes By Aamir Waheed on April 11 2013

    End Sub

#End Region

#Region "Load Financial Years in Financial Years Drop down"
    Public Sub loadFinancialYears()
        ucFinancialYear.loadFinancialYears()
    End Sub
#End Region


#Region "Delete FollowOn Work Detail"

    Protected Sub DeleteFollowOnWorkDetail(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim commandArgument As String = CType(CType(sender, Button).CommandArgument, String)
            Dim commandArg = commandArgument.Split(";")
            Dim followOnId As Integer = Integer.Parse(commandArg(0))
            Dim faultLogId As Integer = Integer.Parse(commandArg(1))

            SessionManager.setFollowOnWorkId(followOnId)
            SessionManager.setFaultLogId(faultLogId)
            popupDeleteFolloOnWork.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        End Try
    End Sub

    Protected Sub BtnYes_Click(sender As Object, e As EventArgs) Handles BtnYes.Click
        DeleteFollowOnWork(SessionManager.getFollowOnWorkId(), SessionManager.getFaultLogId())
        Response.Redirect("ReportsArea.aspx?rd=fow")
    End Sub
    Public Sub DeleteFollowOnWork(followOnId As Integer, faultLogId As Integer)
        objReportsBl.DeleteFollowOnWork(followOnId, faultLogId)
    End Sub
    Protected Sub BtnNo_Click(sender As Object, e As EventArgs) Handles BtnNo.Click
        popupDeleteFolloOnWork.Hide()
    End Sub
#End Region


#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Fault log ID Set/Get/Remove"

#Region "Fault log ID Set"

    Protected Sub setFaultLogIdViewState(ByVal faultLogId As Integer)
        ViewState.Add(ViewStateConstants.FaultLogId, faultLogId)
    End Sub

#End Region

#Region "Fault log ID Get"

    Protected Function getFaultLogIdViewState()
        Dim faultLogId As Integer = 0

        If Not IsNothing(ViewState(ViewStateConstants.FaultLogId)) Then
            faultLogId = ViewState(ViewStateConstants.FaultLogId)
        End If

        Return faultLogId
    End Function

#End Region

#Region "Fault log ID Remove"

    Protected Sub removeFaultLogIdViewState()
        ViewState.Remove(ViewStateConstants.FaultLogId)
    End Sub

#End Region

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        Else
            pageSortBO = New PageSortBO("DESC", "Recorded", 1, 30)
        End If


        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

End Class