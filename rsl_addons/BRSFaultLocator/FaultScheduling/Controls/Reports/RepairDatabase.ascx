﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RepairDatabase.ascx.vb"
    Inherits="FaultScheduling.RepairDatabase" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../../Styles/default.css" rel="stylesheet" media="screen" />
<asp:Panel ID="pnlMessage" runat="server" Visible="False">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:UpdatePanel ID="UpdPnlgrdRepairList" runat="server" >
    <ContentTemplate>
        <cc2:PagingGridView ID="grdRepairList" runat="server" AllowPaging="false" AllowSorting="True"
            AutoGenerateColumns="False" BackColor="White" BorderColor="#ADADAD" BorderStyle="Solid"
            BorderWidth="2px" CellPadding="4" ForeColor="Black" GridLines="None"
            PageSize="30" Width="100%" PagerSettings-Visible="false" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:TemplateField HeaderText="Description" SortExpression="RepairDescription">
                    <ItemTemplate>
                        <asp:Label ID="lblRepairDescription" runat="server" Text='<%# Bind("RepairDescription") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Width="60%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Associated Faults" SortExpression="AssiciatedFault">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkAssociatedFault" OnClick="ViewAssociatedFault" runat="server"
                            Text='<%#Bind("AssiciatedFault") %>' CommandArgument='<%# Eval("FaultRepairListId") %>'></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Active" SortExpression="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("IsActive") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="imgBtnEdit" Width="25" Height="25" CommandArgument='<%# Eval("FaultRepairListId") %>'
                            BorderStyle="None" runat="server" ImageUrl="~/Images/edit.png" OnClick="imgBtnEdit_Click" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#E6E6E6"></RowStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle BackColor="#C00000" ForeColor="White" Font-Bold="True" HorizontalAlign="Left">
            </HeaderStyle>
            <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        </cc2:PagingGridView>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" CssClass="gridPagerContainer">
            <table class="gridPager">
                <tbody>
                    <tr>
                        <td align="center">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="First " runat="server" CommandName="Page"
                                                CommandArgument="First" />
                                           &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev " runat="server" CommandName="Page"
                                                CommandArgument="Prev" />
                                           &nbsp;
                                        </td>
                                        <td>
                                            Page:
                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                            of
                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                            to
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                            of
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                        </td>
                                        <td>&nbsp;
                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                CommandArgument="Next" />
                                          &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                CommandArgument="Last" />
                                             
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td align="right">
                            <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                            <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                            <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td>
                </td>
                <td style="text-align: right; padding-right: 10px">
                    <asp:Button ID="btnExportToXls" runat="server" Text="Export to XLS" UseSubmitBehavior="false"    />
                </td>
                <td style="text-align: right; width: 30px">
                    <asp:Button ID="AddRepairBtnOnRepairList" runat="server" Text="Add a New Repair" UseSubmitBehavior="false"    />
                </td>
            </tr>
        </table>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToXls" />
    </Triggers>
</asp:UpdatePanel>

<!--Model Amend popup-->
<cc1:ModalPopupExtender ID="mdlAmendRepairPopup" runat="server" TargetControlID="lblAmendDismiss"
    Drag="true" PopupControlID="pnlAmendRepair" BackgroundCssClass="modalBackground"
    CancelControlID="btnAmendBack">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlAmendRepair" runat="server" Visible="true" CssClass="modalPopupRepair"
    align="center">
    <asp:UpdatePanel runat="server" ID="updPanelAmendRepair" style="height: 210px" UpdateMode="Conditional" RenderMode ="Inline">
        <ContentTemplate>
            <div style="width: 100%; background-color: #fff; padding-bottom:10px;">
                <div style="background-color: Black; color: white; font-size=12px; font-weight=bold;
                    text-align: left; padding-left: 5px;">
                    <b>Amend a repair</b>
                    <br />
                    <br />
                </div>
                <br />
                <div style="margin-left: 20px">
                    <table style="width: 500px; padding: 5px;">
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel ID="UpdPnllblMessageAmendRepair" runat="server" RenderMode="Inline"
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlMessageAmendRepair" runat="server" Visible="False">
                                            <asp:Label ID="lblmessageAmendRepair" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr style="height: 50px">
                            <td>
                                Description:
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtAmendDescription" runat="server" Width="269px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cost:
                            </td>
                            <td>
                                Net £:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAmendNet" runat="server" ></asp:TextBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxAmendPostInspection" runat="server" />
                                Post Inspection
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                VAT Rate:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel_VatRate" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlAmendVatRate" runat="server" AutoPostBack="true" class="short-select"
                                            OnSelectedIndexChanged="ddlAmendVatRate_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxAmendStockCondition" runat="server" />
                                Stock Condition
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                VAT £:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel_Vat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAmendVat" runat="server" ReadOnly="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxAmendIsActive" runat="server" />
                                Active
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Gross £:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel_Gross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAmendGross" runat="server" ReadOnly="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr style="height: 20px">
                            <td>
                                <asp:Button ID="btnAmendBack" runat="server" Text="< Back" UseSubmitBehavior="false"   />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnAmendRepair" runat="server" Text="Amend Repair" UseSubmitBehavior="false"    />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Label ID="lblAmendDismiss" runat="server"></asp:Label>
<!--Model popup Add Repair-->
<cc1:ModalPopupExtender ID="mdlAddRepairPopup" runat="server" TargetControlID="lblAddDismiss"
    Drag="true" PopupControlID="pnlAddRepair" BackgroundCssClass="modalBackground"
    CancelControlID="btnAddBack">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlAddRepair" runat="server" Visible="true" CssClass="modalPopupRepair"
    align="center">
    <asp:UpdatePanel runat="server" ID="updPanelAddRepair" style="height: 210px">
        <ContentTemplate>
            <div style="width: 100%; background-color: #fff;">
                <div style="background-color: Black; color: white; font-size=12px; font-weight=bold;
                    text-align: left; padding-left: 5px;">
                    <b>Add New repair</b>
                    <br />
                    <br />
                </div>
                <br />
                <div style="margin-left: 20px">
                    <table style="width: 500px; padding: 5px;">
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel ID="UpdPnllblMessageAddRepair" runat="server" RenderMode="Inline"
                                    UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlMessageAddRepair" runat="server" Visible="False">
                                            <asp:Label ID="lblmessageAddRepair" runat="server"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr style="height: 40px">
                            <td>
                                Description:
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="txtAddDescription" runat="server" Width="269px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cost:
                            </td>
                            <td>
                                Net £:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddNet" runat="server" ></asp:TextBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxAddPostInspection" runat="server" />
                                Post Inspection
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                VAT Rate:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanelAdd_VatRate" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlAddVatRate" runat="server" AutoPostBack="true" class="short-select"
                                            OnSelectedIndexChanged="ddlAddVatRate_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxAddStockCondition" runat="server" />
                                Stock Condition
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                VAT £:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanelAdd_Vat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAddVat" runat="server" ReadOnly="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkBoxAddIsActive" runat="server" />
                                Active
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Gross £:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanelAdd_Gross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAddGross" runat="server" ReadOnly="True"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr style="height: 20px">
                            <td>
                                <asp:Button ID="btnAddBack" runat="server" Text="< Back" UseSubmitBehavior="false"    />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                <asp:Button ID="btnAddRepair" runat="server" Text="Add Repair" UseSubmitBehavior="false"    />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Label ID="lblAddDismiss" runat="server"></asp:Label>
<!--Model popup Associated faults-->
<cc1:ModalPopupExtender ID="mdlAssociatedFaultsPopup" runat="server" TargetControlID="lblAssociatedFaultsDismiss"
    Drag="true" PopupControlID="pnlAssociatedFaults" BackgroundCssClass="modalBackground"
    CancelControlID="btnAssociatedFaultsBack">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlMsgAssociatedFaults" runat="server" Visible="False">
    <asp:Label ID="lblMessageAssociatedFaults" runat="server"></asp:Label>
</asp:Panel>
<asp:Panel ID="pnlAssociatedFaults" runat="server" Visible="true" CssClass="modalPopupAssociatedFaults"
    align="center">
    <div style="background-color: Black; color: white; font-size=12px; font-weight: bold;
        text-align: left; padding-left: 5px; padding-top: 10px; padding-bottom: 10px;">
        <b>Associated Faults </b>
    </div>
    <br />
    <asp:UpdatePanel ID="UpdpnlAssociatedFaults" runat="server">
        <ContentTemplate>
            <div style="text-align: left; padding-left: 5px">
                <table width="50%">
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Repair:"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblRepair" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div id="associatedFaults" style="height: 370px; margin-left: 10px;
                margin-right: 10px; border: 1px solid gray;">
                <div style="height: 350px; overflow: auto;">
                    <cc2:PagingGridView ID="GridAssociatedFaultsList" runat="server" AllowPaging="false"
                        AllowSorting="False" AutoGenerateColumns="False" BackColor="White" CellPadding="4"
                        ForeColor="Black" GridLines="none" orderby="" PageSize="30" Width="100%" PagerSettings-Visible="true"
                        ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField HeaderText="Fault Description:" SortExpression="FaultDescription">
                                <ItemTemplate>
                                    <asp:Label ID="lblFaultDescription" runat="server" Text='<%# Bind("FaultDescription") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="100%" />
                            </asp:TemplateField>                      
                        </Columns>
                        <RowStyle BackColor="White"></RowStyle>
                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                        <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
                        <HeaderStyle BackColor="White" ForeColor="Black" Font-Bold="True" HorizontalAlign="Left"
                            CssClass="gridHeader"></HeaderStyle>
                        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                        <FooterStyle BackColor="Black" ForeColor="Black"></FooterStyle>
                    </cc2:PagingGridView>
                </div>
                <asp:Panel runat="server" ID="PnlPaginationAssociatedFaults" Visible="false" CssClass="gridPagerContainerAssociatedFaults" Width ="100%">
                    <table class="gridPagerAssociatedFaults">
                        <tbody>
                            <tr>
                                <td align="center">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <asp:LinkButton ID="lnkbtnPagerAssociatedFaultsFirst" Text="First" runat="server"
                                                        CommandName="Page" CommandArgument="First" />
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkbtnPagerAssociatedFaultsPrev" Text="Prev" runat="server" CommandName="Page"
                                                        CommandArgument="Prev" />
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    Page:
                                                    <asp:Label Text="1" runat="server" ID="lblPagerAssociatedFaultsCurrentPage" ForeColor="Red" />
                                                    of
                                                    <asp:Label Text="1" runat="server" ID="lblPagerAssociatedFaultsTotalPages" />. Records:
                                                    <asp:Label Text="1" runat="server" ID="lblPagerAssociatedFaultsRecordStart" />
                                                    to
                                                    <asp:Label Text="30" runat="server" ID="lblPagerAssociatedFaultsRecordEnd" />
                                                    of
                                                    <asp:Label Text="30" runat="server" ID="lblPagerAssociatedFaultsRecordTotal" />
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkbtnPagerAssociatedFaultsNext" Text="Next" runat="server" CommandName="Page"
                                                        CommandArgument="Next" />
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkbtnPagerAssociatedFaultsLast" Text="Last" runat="server" CommandName="Page"
                                                        CommandArgument="Last" />
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
            <br />

            <table width="100%">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 15px;">
                        <asp:Button ID="btnAssociatedFaultsBack" runat="server" Text=" < Back " UseSubmitBehavior="false"    />
                    </td>
                    <td style="text-align: right; padding-right: 15px;">
                        <asp:Button ID="btnAssociatedFaultsExportXls" runat="server" Text=" Export to XLS " UseSubmitBehavior="false"    />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAssociatedFaultsExportXls" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Label ID="lblAssociatedFaultsDismiss" runat="server"></asp:Label>
