﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FaultManagementListControl.ascx.vb"
    Inherits="FaultScheduling.FaultManagementListControl" %>
<%--Start Custom Controls Registration to use in page design.--%>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<%@ Register Src="~/Controls/Reports/schemeBlockDropdown.ascx" TagName="SchemeBlockDropdown"
    TagPrefix="ucSchemeBlockddl" %>
<%--End Custom Controls Registration to use in page design.--%>
<script type="text/javascript">

    $(document).ready(function () {
        $("#ContentPlaceHolder1_btnGo").hide()
    });
        
</script>
<asp:UpdatePanel ID="updPnl_LoadGrid" runat="server" RenderMode="Inline" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Button ID="btnAddFault" runat="server" Text="Add New Fault" UseSubmitBehavior="False"
            class="btn btn-xs btn-blue" style="padding:3px 10px !important; min-width:0px;" />
        <asp:UpdatePanel ID="UpdPnllblMessage" runat="server" RenderMode="Inline" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
            <cc1:PagingGridView ID="grdFault" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                OnRowCreated="grdFault_RowCreated" OnPageIndexChanging="GVFault_PageIndexChanging" OnSorting="GVFault_Sorting"
                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
                <Columns>
                    <asp:TemplateField HeaderText="Description" ItemStyle-CssClass="Description" SortExpression="FaultLogID">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Trade" InsertVisible="False" SortExpression="Trade"
                        ShowHeader="False">
                        <ItemTemplate>
                            <asp:Label ID="GVLblTrade" runat="server" Text='<%# Bind("Trade") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle HorizontalAlign="Left" Width="25%" />
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="PriorityName" HeaderText="Priority" InsertVisible="False">
                        <ItemTemplate>
                            <asp:Label ID="GVLblResponse" runat="server" Text='<%# Bind("PriorityName") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" Wrap="False" />
                        <ItemStyle HorizontalAlign="Left" Width="10%" Wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="duration" HeaderText="Duration" InsertVisible="False">
                        <ItemTemplate>
                            <asp:Label ID="GVLblPriorityDays" runat="server" Text='<%# Eval("duration") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Width="10%" Wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Net(£)" InsertVisible="False" SortExpression="NetCost">
                        <ItemTemplate>
                            <asp:Label ID="GVLblNet" runat="server" Text='<%# Eval("NetCost") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Width="10%" Wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" SortExpression="FaultActive">
                        <ItemTemplate>
                            <asp:Label ID="GVLblStatus" runat="server" Text='<%# Bind("FaultActive") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Width="10%" Wrap="False" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Contractor" SortExpression="isContractor">
                        <ItemTemplate>
                            <asp:Label ID="GVLblContractor" runat="server" Text='<%# Eval("isContractor") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Width="5%" Wrap="False" />
                    </asp:TemplateField>
                        <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnEdit" Width="25" Height="25" CommandArgument='<%# Eval("FaultId") %>' BorderStyle="None" runat="server" ImageUrl="~/Images/edit.png" OnClick="imgBtnEdit_Click" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Center" Wrap="False" />
                        <ItemStyle HorizontalAlign="Center" Width="5%" Wrap="False" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
            </cc1:PagingGridView>
        </div>
        <%--Pager Template Start--%>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
            <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                <div class="paging-left">
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                            &lt;&lt;First
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                            &lt;Prev
                        </asp:LinkButton>
                    </span>
                    <span style="padding-right:10px;">
                        <b>Page:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                        of
                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                    </span>
                    <span style="padding-right:20px;">
                        <b>Result:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                        to
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                        of
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                    </span>
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                        </asp:LinkButton>
                    </span>
                </div>
                <div style="float: right;">
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                    <div class="field" style="margin-right: 10px;">
                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    </div>
                    <span>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                    </span>
                </div>
            </div>
        </asp:Panel>
        <%--Pager Template End--%>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="updPnlAddFault" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <table border="1" style="background-color: #ffffff">
            <tr>
                <td>
                    <table width="400px" border="0">
                        <tbody>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:Label ID="lblViewEnquiry" runat="server" Font-Bold="True" Font-Names="Arial"
                                        Font-Size="Small" Height="19px" Text="Add Fault" Width="35%"></asp:Label>
                                    <asp:ImageButton ID="imgBtnClosePopup" runat="server" Style="position: absolute;
                                        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:UpdatePanel ID="UpdPnllblMessageAddFault" runat="server" RenderMode="Inline"
                                        UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlMessageAddFault" runat="server" Visible="False">
                                                <asp:Label ID="lblmessageAddFault" runat="server"></asp:Label>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px; height: 21px" class="caption">
                                    <asp:Label ID="lblArea" runat="server" CssClass="caption" Text="Location:"></asp:Label>
                                </td>
                                <td align="right" style="height: 21px" colspan="3">
                                    <asp:DropDownList ID="ddlLocationFault" runat="server" AppendDataBoundItems="True"
                                        Font-Names="Arial" Font-Size="Small" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="lblDescription" runat="server" CssClass="caption" Text="Description:"></asp:Label>
                                </td>
                                <td align="right" colspan="3">
                                    <asp:TextBox ID="txtDescription" runat="server" Width="98.4%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px; height: 21px" class="caption">
                                    <asp:Label ID="lblPriority" runat="server" CssClass="caption" Text="Priority:"></asp:Label>
                                </td>
                                <td style="height: 21px" colspan="3">
                                    <asp:DropDownList ID="ddlPriorityFault" runat="server" AppendDataBoundItems="True"
                                        Font-Names="Arial" Font-Size="Small" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="lblDuration" runat="server" CssClass="caption" Text="Duration:"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="ddlDurationFault" runat="server" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="lblTrade" runat="server" CssClass="caption" Text="Trade:"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="ddlTradeFault" runat="server" AutoPostBack="True" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnAddTradeFault" runat="server" Text="Add" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                </td>
                                <td align="left" style="width: 191px" colspan="3">
                                    <asp:GridView ID="gvTrades" runat="server" AutoGenerateColumns="False" Width="100%"
                                        GridLines="None" ShowHeader="False" BorderStyle="Solid" BorderWidth="1px">
                                        <Columns>
                                            <asp:TemplateField HeaderText="FaultTradeID" ShowHeader="False" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FaultTradeID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trade Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("TradeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowDeleteButton="True" DeleteText="X" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 2px;" colspan="4" valign="top">
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="lblRecharge" runat="server" CssClass="caption" Text="Recharge:"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkBoxRecharge" runat="server" />
                                </td>
                                <td style="width: 191px">
                                    <asp:Label ID="lblNet" runat="server" CssClass="caption" Height="21px" Text="Net (£) :"
                                        Width="96px"></asp:Label>
                                </td>
                                <td style="width: 444px">
                                    <asp:UpdatePanel ID="updatePanel_NetCost" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtNet" runat="server" OnTextChanged="txtNet_TextChanged" AutoPostBack="True"
                                                CausesValidation="True" Width="98.4%"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 191px">
                                    <asp:Label ID="lblVateRate" runat="server" CssClass="caption" Text="Vat Rate:"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="updatePanel_VatRate" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlVatAddFault" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                Font-Names="Arial" Font-Size="Small" OnSelectedIndexChanged="ddlVat_SelectedIndexChanged"
                                                Width="100%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 191px">
                                    <asp:Label ID="lblVat" runat="server" CssClass="caption" Text="Vat (£) :"></asp:Label>
                                </td>
                                <td style="width: 444px">
                                    <asp:UpdatePanel ID="updatePanel_Vat" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtVat" runat="server" ReadOnly="True" Width="98.4%">0.0</asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 191px">
                                    <asp:Label ID="lblGross" runat="server" CssClass="caption" Text="Gross (£) :"></asp:Label>
                                </td>
                                <td style="width: 444px">
                                    <asp:UpdatePanel ID="updatePanel_Gross" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtGross" runat="server" ReadOnly="True" Width="98.4%">0.0</asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 2px;" colspan="4" valign="top">
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px;">
                                    <asp:Label ID="lblSubContractor" runat="server" CssClass="caption" Text="Sub-Contractor:"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkBoxSubContractor" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="lblFaultActive" runat="server" CssClass="caption" Text="Active:"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkBoxFaultActive" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="lblGasSafe" runat="server" CssClass="caption" Text="Gas Safe:"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkBoxGasSafe" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="lblOFTEC" runat="server" CssClass="caption" Text="OFTEC:"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkBoxOFTEC" runat="server" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <cc2:ModalPopupExtender ID="mdlPopup_AddFault" runat="server" BackgroundCssClass="modalBackground"
                                        CancelControlID="btnClose" Drag="True" PopupControlID="updPnlAddFault" PopupDragHandleControlID="lblLoction"
                                        TargetControlID="lblArea">
                                    </cc2:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 2px;" colspan="4" valign="top">
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4">
                                    &nbsp;
                                    <asp:Button ID="btnClose" runat="server" OnClick="btnClose_Click" Text="Cancel" UseSubmitBehavior="true"
                                        Width="56px" />
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save Fault" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel ID="updPnlAmendFault" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <table border="1" style="background-color: #ffffff">
            <tr>
                <td>
                    <table width="400px" border="0">
                        <tbody>
                            <tr>
                                <td align="left" colspan="4">
                                    <asp:Label ID="lblAmendFault" runat="server" Font-Bold="True" Font-Names="Arial"
                                        Font-Size="Small" Height="19px" Text="Amend Fault" Width="35%"></asp:Label>
                                    <asp:Label ID="lblFaultID" runat="server" Visible="False"></asp:Label>
                                    <asp:ImageButton ID="ImageButton1" runat="server" Style="position: absolute; top: -12px;
                                        right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:UpdatePanel ID="UpdPnllblMessageAmendFault" runat="server" RenderMode="Inline"
                                        UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlMessageAmendFault" runat="server" Visible="False">
                                                <asp:Label ID="lblMessageAmendFault" runat="server"></asp:Label>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px; height: 21px">
                                    <asp:Label ID="Label10" runat="server" CssClass="caption" Text="Location:"></asp:Label>
                                </td>
                                <td align="right" style="height: 21px" colspan="3">
                                    <asp:DropDownList ID="ddlLocationAmendFault" runat="server" AppendDataBoundItems="True"
                                        Font-Names="Arial" Font-Size="Small" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="Label11" runat="server" CssClass="caption" Text="Description:"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="txtDescriptionAmendFault" runat="server" Width="98.6%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px; height: 21px">
                                    <asp:Label ID="Label12" runat="server" CssClass="caption" Text="Priority:"></asp:Label>
                                </td>
                                <td style="height: 21px" colspan="3">
                                    <asp:DropDownList ID="ddlPriorityAmendFault" runat="server" AppendDataBoundItems="True"
                                        Font-Names="Arial" Font-Size="Small" Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="Label13" runat="server" CssClass="caption" Text="Duration:"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="ddlDurationAmendFault" runat="server" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                    <asp:Label ID="Label14" runat="server" CssClass="caption" Text="Trade:"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="ddlTradeAmendFault" runat="server" AutoPostBack="True" AppendDataBoundItems="True">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnAddTradeAmendFault" runat="server" Text="Add" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px">
                                </td>
                                <td align="left" colspan="3">
                                    <asp:GridView ID="gvTradesAmendFault" runat="server" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="100%" BorderStyle="Solid" BorderWidth="1px"
                                        GridLines="None" ShowHeader="False">
                                        <Columns>
                                            <asp:TemplateField HeaderText="FaultTradeID" ShowHeader="False" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FaultTradeID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trade Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("TradeName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:CommandField ShowDeleteButton="True" DeleteText="X" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 2px;" colspan="4" valign="top">
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" style="width: 191px; text-align: left;">
                                    <asp:Label ID="Label5" runat="server" CssClass="caption" Text="Recharge:"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:CheckBox ID="chkBoxRechargeAmendFault" runat="server" />
                                </td>
                                <td align="right" style="width: 191px; text-align: left;">
                                    <asp:Label ID="Label6" runat="server" CssClass="caption" Height="21px" Text="Net £ :"
                                        Width="96px"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="updatePanel1" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtNetAmendFault" runat="server" AutoPostBack="True" CausesValidation="True"></asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td align="right" style="width: 191px; text-align: left;">
                                    <asp:Label ID="Label7" runat="server" CssClass="caption" Text="Vat Rate:"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="updatePanel_VatRateAmendFault" runat="server" RenderMode="Inline"
                                        UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlVatAmendFault" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                                Font-Names="Arial" Font-Size="Small" Width="152px">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="Label8" runat="server" CssClass="caption" Text="Vat          £ :"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="updatePanel_VatAmendFault" runat="server" RenderMode="Inline"
                                        UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtVatAmendFault" runat="server" ReadOnly="True">0.0</asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="Label9" runat="server" CssClass="caption" Text="Gross £  :"></asp:Label>
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="updatePanel_GrossAmendFault" runat="server" RenderMode="Inline"
                                        UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtGrossAmendFault" runat="server" ReadOnly="True">0.0</asp:TextBox>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 2px;" colspan="4" valign="top">
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="Label1" runat="server" CssClass="caption" Text="Sub-Contractor:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkBoxSubContractorAmendFault" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="Label2" runat="server" CssClass="caption" Text="Active:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkBoxFaultActiveAmendFault" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="Label3" runat="server" CssClass="caption" Text="Gas Safe:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkBoxGasSafeAmendFault" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 191px" class="caption">
                                    <asp:Label ID="Label4" runat="server" CssClass="caption" Text="OFTEC:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:CheckBox ID="chkBoxOFTECAmendFault" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <cc2:ModalPopupExtender ID="mdlPopup_AmendFault" runat="server" BackgroundCssClass="modalBackground"
                                        CancelControlID="btnAmendClose" Drag="True" PopupControlID="updPnlAmendFault"
                                        PopupDragHandleControlID="lblAmendLoction" TargetControlID="ddlLocationAmendFault">
                                    </cc2:ModalPopupExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 2px;" colspan="4" valign="top">
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4">
                                    <asp:Button ID="btnAmendClose" runat="server" OnClick="btnAmendClose_Click" Text="Close"
                                        Width="56px" />
                                    <asp:Button ID="btnAmendSave" runat="server" OnClick="btnAmendSave_Click" Text="Save Amends" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
