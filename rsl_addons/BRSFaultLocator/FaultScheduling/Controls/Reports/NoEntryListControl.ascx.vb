﻿Imports FLS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports FLS_BusinessObject

Public Class NoEntryListControl
    Inherits UserControlBase

#Region "Delegates"

    Public Delegate Sub Filters_Changed(ByVal sender As Object, ByVal e As System.EventArgs)

#End Region

#Region "Events"

    Public Event FiltersChanged As Filters_Changed

#End Region

#Region "Properties"

    Dim objReportsBl As ReportsBL = New ReportsBL()
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Recorded", 1, 30)
    Dim totalCount As Integer = 0

#End Region

#Region "Events Handling"

#Region "Page Pre Render Event"
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.reBindGridForLastPage()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Grid No Entry List Page Index Changing Event"

    Protected Sub grdNoEntryList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdNoEntryList.PageIndexChanging

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
            objPageSortBo.PageNumber = e.NewPageIndex + 1
            grdNoEntryList.PageIndex = e.NewPageIndex

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As New DataSet
            populateNoEntryList(resultDataSet, search, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Grid No Entry List Sorting Event"

    Protected Sub grdNoEntryList_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdNoEntryList.Sorting

        Try
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdNoEntryList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()

            populateNoEntryList(resultDataSet, search, False)

            'Dim dt As DataTable = New DataTable()
            'dt = resultDataSet.Tables(0)

            'If IsNothing(dt) = False Then
            '    Dim dvSortedView As DataView = New DataView(dt)
            '    dvSortedView.Sort = objPageSortBo.SortExpression + " " + objPageSortBo.SmallSortDirection
            '    grdFollowOnList.DataSource = dvSortedView
            '    grdFollowOnList.DataBind()
            'End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

    Protected Sub grdNoEntryList_RowCreated(sender As Object, e As GridViewRowEventArgs)
        'check if it is a header row
        'since allowsorting is set to true, column names are added as command arguments to
        'the linkbuttons by DOTNET API
        If e.Row.RowType = DataControlRowType.Header Then
            Dim btnSort As LinkButton
            Dim image As Image
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            'iterate through all the header cells
            For Each cell As TableCell In e.Row.Cells
                'check if the header cell has any child controls
                If cell.HasControls() Then
                    'get reference to the button column
                    btnSort = DirectCast(cell.Controls(0), LinkButton)
                    image = New Image()
                    If objPageSortBo IsNot Nothing Then
                        If btnSort.CommandArgument = objPageSortBo.SortExpression Then
                            'following snippet figure out whether to add the up or down arrow
                            'based on the sortdirection
                            If objPageSortBo.SortDirection = SortDirection.Ascending.ToString() Then
                                image.ImageUrl = "~/Images/Grid/sort_asc.png"
                            Else
                                image.ImageUrl = "~/Images/Grid/sort_desc.png"
                            End If
                        Else
                            image.ImageUrl = "~/Images/Grid/sort_both.png"
                        End If
                        cell.Controls.Add(image)
                    End If
                End If
            Next
        End If
    End Sub

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)

            Dim search As String = ViewState.Item(ViewStateConstants.Search)
            Dim resultDataSet As DataSet = New DataSet()
            populateNoEntryList(resultDataSet, search, False)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"

    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()

                objPageSortBo.PageNumber = pageNumber

                setPageSortBoViewState(objPageSortBo)
                Dim search As String = ViewState.Item(ViewStateConstants.Search)
                Dim resultDataSet As DataSet = New DataSet()
                populateNoEntryList(resultDataSet, search, False)
            Else
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidPageNumber, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Scheme or Block Or Financial Year Changed"
    Private Sub Filters_FilterChanged(sender As Object, e As System.EventArgs) Handles SchemeBlockDDL.blockChanged, SchemeBlockDDL.schemeChanged _
        , ucFinancialYear.financialYearChanged
        RaiseEvent FiltersChanged(sender, e)
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Bind To Grid"

    Sub bindToGrid()
        ViewState.Item(ViewStateConstants.Search) = ""
        Dim totalCount As Integer = ViewState.Item(ViewStateConstants.TotalCount)
        Dim resultDataSet As DataSet = New DataSet()
        resultDataSet = ViewState.Item(ViewStateConstants.ResultDataSet)
        grdNoEntryList.VirtualItemCount = totalCount
        grdNoEntryList.DataSource = resultDataSet
        grdNoEntryList.DataBind()

        objPageSortBo = getPageSortBoViewState()

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdNoEntryList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdNoEntryList.PageCount Then
            setVirtualItemCountViewState(totalCount)
            setResultDataSetViewState(resultDataSet)
        End If

    End Sub

#End Region

#Region " Get No Entry Detail"

    Protected Sub getNoEntryDetail(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            ' Reset Job sheet controls
            resetJobsheetControls()

            '0 = JSN
            '1 = FaultLogId

            Dim commandArguments = CType(CType(sender, Button).CommandArgument, String).Split(";")

            Dim jsn As String = commandArguments(0)
            setFaultLogIdViewState(commandArguments(1))
            hdnAppointmentType.Value = commandArguments(2)
            Dim resultDataSet As DataSet = New DataSet()

            objReportsBl.getNoEntryDetail(resultDataSet, jsn)

            ' Job Sheet Detail
            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else

                lblFaultId.Text = resultDataSet.Tables(0).Rows(0)(0).ToString
                lblContractor.Text = resultDataSet.Tables(0).Rows(0)(1).ToString
                lblOperativeName.Text = resultDataSet.Tables(0).Rows(0)(2).ToString
                lblFaultPriority.Text = resultDataSet.Tables(0).Rows(0)(3).ToString
                lblFaultCompletionDateTime.Text = resultDataSet.Tables(0).Rows(0)(4).ToString
                lblOrderDate.Text = resultDataSet.Tables(0).Rows(0)(5).ToString
                lblFaultLocation.Text = resultDataSet.Tables(0).Rows(0)(6).ToString
                lblFaultDescription.Text = resultDataSet.Tables(0).Rows(0)(7).ToString
                lblFaultOperatorNote.Text = resultDataSet.Tables(0).Rows(0)(8).ToString
                lblTrade.Text = resultDataSet.Tables(0).Rows(0)(12).ToString


            End If

            ' Customer Info
            If (resultDataSet.Tables(1).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else
                If hdnAppointmentType.Value = "Fault" Then
                    pnlPropertyData.Visible = True
                    pnlPropertyDetail.Visible = True
                    pnlSchemeBlock.Visible = False
                    ' Header Customer Labels
                    lblClientNameHeader.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                    lblClientStreetAddressHeader.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                    lblClientCityHeader.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                    lblClientTelPhoneNumberHeader.Text = resultDataSet.Tables(1).Rows(0)(5).ToString

                    ' Body customer Labels

                    lblClientName.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                    lblClientStreetAddress.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                    lblClientCity.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                    lblClientPostCode.Text = resultDataSet.Tables(1).Rows(0)(3).ToString
                    lblClientRegion.Text = resultDataSet.Tables(1).Rows(0)(4).ToString
                    lblClientTelPhoneNumber.Text = resultDataSet.Tables(1).Rows(0)(5).ToString
                    lblClientMobileNumber.Text = resultDataSet.Tables(1).Rows(0)(6).ToString
                    lblClientEmailId.Text = resultDataSet.Tables(1).Rows(0)(7).ToString

                    populateCustomerObject(resultDataSet)
                Else
                    pnlPropertyData.Visible = False
                    pnlPropertyDetail.Visible = False
                    pnlSchemeBlock.Visible = True
                    lblSchemeName.Text = resultDataSet.Tables(1).Rows(0)("SchemeName").ToString()
                    lblBlockName.Text = resultDataSet.Tables(1).Rows(0)("BlockName").ToString()
                End If


            End If

            ' No Entry Detail
            If (Not resultDataSet.Tables(2).Rows.Count = 0) Then
                lblTime.Text = resultDataSet.Tables(2).Rows(0)(0).ToString
                lblNoEntryNote.Text = resultDataSet.Tables(2).Rows(0)(1).ToString
            End If

            reBindGridForLastPage()

            popupNoEntry.Show()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region " Schedule Appointment"

    Protected Sub scheduleAppointment(ByVal sender As Object, ByVal e As System.EventArgs)

        Try
            SessionManager.setIsNoEntry(True)
            SessionManager.setNoEntryFaultLogId(getFaultLogIdViewState())
            If hdnAppointmentType.Value = "Fault" Then
                Response.Redirect(PathConstants.RearrangCurrentFault + GeneralHelper.getSrc(PathConstants.ReportAreaSrcVal) + "&" + PathConstants.Create + "=yes&" + PathConstants.Jsn + "=" + lblFaultId.Text + "&" + PathConstants.RepairsDashboard + "=ne" + "&" + PathConstants.isNoEnty + "=" + PathConstants.Yes)
            Else
                Response.Redirect(PathConstants.ReArrangingSbFault + GeneralHelper.getSrc(PathConstants.ReportAreaSrcVal) + "&" + PathConstants.Create + "=yes&" + PathConstants.Jsn + "=" + lblFaultId.Text + "&" + PathConstants.RepairsDashboard + "=ne" + "&" + PathConstants.isNoEnty + "=" + PathConstants.Yes)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Reset Job sheet control "

    Protected Sub resetJobsheetControls()

        reBindGridForLastPage()
        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ClearPanelMessage, True)
        popupNoEntry.Hide()

        ' Job Sheet Detail
        lblFaultId.Text = String.Empty
        lblContractor.Text = String.Empty
        lblOperativeName.Text = String.Empty
        lblFaultPriority.Text = String.Empty
        lblFaultCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblFaultLocation.Text = String.Empty
        lblFaultDescription.Text = String.Empty
        lblFaultOperatorNote.Text = String.Empty
        lblTrade.Text = String.Empty

        ' Header Customer Labels
        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        ' Body customer Labels

        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmailId.Text = String.Empty


        lblTime.Text = String.Empty
        lblNoEntryNote.Text = String.Empty
    End Sub

#End Region

#Region "Populate No Entry List"

    Sub populateNoEntryList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal setSession As Boolean)
        Try
            If setSession = True Then
                removePageSortBoViewState()
                getPageSortBoViewState()
            Else
                objPageSortBo = getPageSortBoViewState()
            End If

            ViewState.Add(ViewStateConstants.Search, search)

            totalCount = objReportsBl.getNoEntryList(resultDataSet, search, objPageSortBo, SchemeBlockDDL.SchemeId, SchemeBlockDDL.BlockId, ucFinancialYear.FinancialYear)

            objPageSortBo.TotalRecords = totalCount
            objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

            'If Not resultDataSet.Tables(0).Rows.Count = 0 Then

            If setSession Then
                ViewState.Add(ViewStateConstants.ResultDataSet, resultDataSet)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
                ViewState.Add(ViewStateConstants.TotalCount, totalCount)
            End If

            grdNoEntryList.VirtualItemCount = totalCount
            grdNoEntryList.DataSource = resultDataSet
            grdNoEntryList.DataBind()

            setPageSortBoViewState(objPageSortBo)

            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

            If grdNoEntryList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdNoEntryList.PageCount Then
                setVirtualItemCountViewState(totalCount)
                setResultDataSetViewState(resultDataSet)
            End If

            'End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "populate Customer Object"
    Private Sub populateCustomerObject(ByRef resultDataSet As DataSet)
        Dim customerID As Integer
        Dim propertyID As String

        Dim customerObj As CustomerBO = New CustomerBO()

        Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()

        customerObj.Name = resultDataSet.Tables(1).Rows(0)(0).ToString
        customerObj.Street = resultDataSet.Tables(1).Rows(0)(1).ToString
        customerObj.Address = String.Empty
        customerObj.City = resultDataSet.Tables(1).Rows(0)(2).ToString
        customerObj.PostCode = resultDataSet.Tables(1).Rows(0)(3).ToString
        customerObj.Area = resultDataSet.Tables(1).Rows(0)(4).ToString
        customerObj.Telephone = resultDataSet.Tables(1).Rows(0)(5).ToString
        customerObj.Mobile = resultDataSet.Tables(1).Rows(0)(6).ToString
        customerObj.Email = resultDataSet.Tables(1).Rows(0)(7).ToString
        customerObj.CustomerId = resultDataSet.Tables(1).Rows(0)(8).ToString

        SessionManager.setCustomerData(customerObj)

        customerID = Convert.ToInt32(resultDataSet.Tables(1).Rows(0)(8).ToString)
        propertyID = resultDataSet.Tables(1).Rows(0)(9).ToString

        SessionManager.setCustomerId(customerID)
        SessionManager.setPropertyId(propertyID)

        'fill object for common item of property,scheme and Block
        commonAddressBo.Street = resultDataSet.Tables(3).Rows(0).Item("Address").ToString()
        commonAddressBo.Address = resultDataSet.Tables(3).Rows(0).Item("Address").ToString()
        commonAddressBo.Area = resultDataSet.Tables(3).Rows(0).Item("County").ToString()
        commonAddressBo.City = resultDataSet.Tables(3).Rows(0).Item("TownCity").ToString()
        commonAddressBo.PostCode = resultDataSet.Tables(3).Rows(0).Item("PostCode").ToString()
        commonAddressBo.PatchId = resultDataSet.Tables(3).Rows(0).Item("PatchId").ToString()
        commonAddressBo.PatchName = resultDataSet.Tables(3).Rows(0).Item("PatchName").ToString()
        'store dataset in session for further use in intelligent scheduling
        SessionManager.setCommonAddressBO(commonAddressBo)



    End Sub
#End Region

#Region "Rebind the Grid to prevent show empty lines for last page only"

    Private Sub reBindGridForLastPage()
        'Rebind the Grid to prevent show empty lines for last page only

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        If grdNoEntryList.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdNoEntryList.PageCount Then
            grdNoEntryList.VirtualItemCount = getVirtualItemCountViewState()
            grdNoEntryList.PageIndex = objPageSortBo.PageNumber - 1
            grdNoEntryList.DataSource = getResultDataSetViewState()
            grdNoEntryList.DataBind()
            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        End If

    End Sub

#End Region

#Region "Load Financial Years in Financial Years Drop down"
    Public Sub loadFinancialYears()
        ucFinancialYear.loadFinancialYears()
    End Sub
#End Region

#End Region

#Region "View State Functions"

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

#Region "Result DataSet Set/Get/Remove"

    Protected Sub setResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.ResultDataSet) = resultDataSet
    End Sub

    Protected Function getResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.ResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.ResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.ResultDataSet)
    End Sub

#End Region

#Region "Fault log ID Set/Get/Remove"

#Region "Fault log ID Set"

    Protected Sub setFaultLogIdViewState(ByVal faultLogId As Integer)
        ViewState.Add(ViewStateConstants.FaultLogId, faultLogId)
    End Sub

#End Region

#Region "Fault log ID Get"

    Protected Function getFaultLogIdViewState()
        Dim faultLogId As Integer = 0

        If Not IsNothing(ViewState(ViewStateConstants.FaultLogId)) Then
            faultLogId = ViewState(ViewStateConstants.FaultLogId)
        End If

        Return faultLogId
    End Function

#End Region

#Region "Fault log ID Remove"

    Protected Sub removeFaultLogIdViewState()
        ViewState.Remove(ViewStateConstants.FaultLogId)
    End Sub

#End Region

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        Else
            pageSortBO = New PageSortBO("DESC", "Recorded", 1, 30)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#End Region

End Class