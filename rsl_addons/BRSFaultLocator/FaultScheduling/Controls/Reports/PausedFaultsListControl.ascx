﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PausedFaultsListControl.ascx.vb"
    Inherits="FaultScheduling.PausedFaultsListControl" %>
<%@ Register Src="~/Controls/Reports/schemeBlockDropdown.ascx" TagName="SchemeBlockDropdown"
    TagPrefix="ucSchemeBlockddl" %>
<%@ Register Src="~/Controls/Reports/FinancialYear.ascx" TagName="FinancialYear"
    TagPrefix="uc" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .textsize
    {
        resize: none;
    }
    .modalPopup
    {
        padding:0;
        border:none;
    }
    .modal-content
    {
        / Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it /
        width: inherit;
        height: inherit; / To center horizontally /
        margin: 0 auto;
        pointer-events: all;
    }
    
    
    .modal-header, h4, .close
    {
        background-color: black;
        color: white !important;
        text-align: left;
        padding-top:1px;
    }
    
    .modal-footer
    {
        /*background-color: #f9f9f9;*/
    }
    
    .modal-header
    {
        min-height: 16.428571429px;
        padding-left: 20px !important;
        border-bottom: 1px solid #e5e5e5;
        font-weight: bold;
    }
    
    .modal-dialog
    {
        width: 100%;
        padding-top: -10px;
        padding-bottom: 10px;
    }
    
    .modal-dialog-950
    {
        width: 950px;
    }
    
    .modal-footer
    {
        padding: 5px 20px 5px 20px !important;
        text-align: right !important;
        border-top: 1px solid #e5e5e5 !important;
    }
</style>
<asp:Panel ID="pnlMessage" runat="server" Visible="False">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Panel>
<asp:Panel runat="server" ID="pnlSchemBlockddl">
    <div class="">
        <ucSchemeBlockddl:SchemeBlockDropdown ID="SchemeBlockDDL" runat="server"></ucSchemeBlockddl:SchemeBlockDropdown>
        <uc:FinancialYear ID="ucFinancialYear" runat="server" style="width: 95px;"></uc:FinancialYear>
    </div>
</asp:Panel>
<div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
    <cc1:PagingGridView ID="grdPausedList" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
        OnRowCreated="grdPausedList_RowCreated" 
        Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
        GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="30">
        <Columns>
            <asp:TemplateField HeaderText="JSN" ItemStyle-CssClass="dashboard" SortExpression="JSN">
                <ItemTemplate>
                    <asp:Label ID="lblJSN" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle BorderStyle="None" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Reported" SortExpression="Recorded">
                <ItemTemplate>
                    <asp:Label ID="lblRecorded" runat="server" Text='<%# Bind("Recorded") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Address" SortExpression="Address">
                <ItemTemplate>
                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="200px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                <ItemTemplate>
                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Block" SortExpression="Block">
                <ItemTemplate>
                    <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="70px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Location" SortExpression="Location">
                <ItemTemplate>
                    <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="115px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Description" SortExpression="Description">
                <ItemTemplate>
                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="190px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Started" SortExpression="Started">
                <ItemTemplate>
                    <asp:Label ID="lblStarted" runat="server" Text='<%# Bind("Started") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="By" SortExpression="UserInitials">
                <ItemTemplate>
                    <asp:Label ID="lblBy" runat="server" Text='<%# Bind("UserInitials") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="40px" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Paused" SortExpression="PausedOn">
                <ItemTemplate>
                    <asp:Label ID="lblPausedOn" runat="server" Text='<%# Bind("PausedOn") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="100px" />
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnView" runat="server" Text="View" EnableTheming="True" UseSubmitBehavior="False"
                        OnClick="btnView_Click" CommandArgument='<%# Eval("Reason") + ";" + Eval("Notes") %>'
                        CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
    </cc1:PagingGridView>
</div>
<%--Pager Template Start--%>
<asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
    <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
        <div class="paging-left">
            <span style="padding-right:10px;">
                <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn">
                    &lt;&lt;First
                </asp:LinkButton>
                <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn">
                    &lt;Prev
                </asp:LinkButton>
            </span>
            <span style="padding-right:10px;">
                <b>Page:</b>
                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                of
                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
            </span>
            <span style="padding-right:20px;">
                <b>Result:</b>
                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                to
                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                of
                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
            </span>
            <span style="padding-right:10px;">
                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn">
                                    
                </asp:LinkButton>
                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn">
                                        
                </asp:LinkButton>
            </span>
        </div>
        <div style="float: right;">
            <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
            <div class="field" style="margin-right: 10px;">
                <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
            </div>
            <span>
                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                    class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
            </span>
        </div>
    </div>
</asp:Panel>
<%--Pager Template End--%>
<!-- POPUP Confirm Appointment Detail (START) -->
<asp:Button ID="btnHidden1" runat="server" Text="" Style="display: none;" />
<ajaxToolkit:ModalPopupExtender ID="mdlPausedFaultDetail" runat="server" PopupControlID="pnlPausedFaultDetail"
    TargetControlID="btnHidden1" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="pnlPausedFaultDetail" runat="server" CssClass="modalPopup" Style="height: 265px;
    width: 400px;">
    <asp:Panel ID="pnlMessagePausedDetail" runat="server" Visible="False">
        <asp:Label ID="lblMessagePausedDetail" runat="server"></asp:Label>
    </asp:Panel>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Paused Fault: <asp:Label ID="lblPopupJsn" runat="server" Text="" Font-Bold="true"></asp:Label></b></h4>                    
                    </div>
                    <div class="modal-body">
                        <table id="tblPausedFault" width="100%" style='margin: 5px;'>                
                            <tr style="padding:0 0 5px 0">
                                <td align="left" valign="top">
                                    <span style="width: 50px; margin-left: 15px;">Paused:</span>
                                </td>
                                <td align="left" valign="top" >
                                    <asp:Label ID="lblPopPausedOn" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr style="padding:0 0 5px 0">
                                <td align="left" valign="top">
                                    <span style="width: 50px; margin-left: 15px;">Reason:</span>
                                </td>
                                <td align="left" valign="top">
                                    <asp:Label ID="lblPopupReason" runat="server" Text="" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <span style="width: 50px; margin-left: 15px;">Note:</span>
                                </td>
                                <td align="left" valign="top">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="margin-left: 15px; margin-right: 30px;">
                                        <div style="border: 1px solid #000000; height: 100px; overflow: auto;">
                                            <asp:Label ID="lblPopupNotes" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer" style="text-align: right; height:20px;">
                        <div style='margin-right: 25px;'>
                            <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="btn btn-xs btn-blue right" style="padding: 3px 23px !important; margin: -3px 3px 0 0;" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- POPUP Confirm Appointment Detail (END) -->
<!-- ModalPopupExtender -->
