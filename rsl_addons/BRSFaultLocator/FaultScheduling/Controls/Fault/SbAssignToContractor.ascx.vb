﻿Imports FLS_Utilities
Imports FLS_BusinessObject
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessLogic
Imports System.Net.Mail
Imports System.Net
Imports System.IO

Public Class SbAssignToContractor
    Inherits UserControlBase
#Region "Properties"

#Region "Is Saved Property to use in consumer page to determine either work is assign to contractor or not"

    Public Property IsSaved() As Boolean
        Get
            Dim isSavedret As Boolean = False
            If Not IsNothing(ViewState(ViewStateConstants.IsSaved)) Then
                isSavedret = CType(ViewState(ViewStateConstants.IsSaved), Boolean)
            End If
            Return isSavedret
        End Get
        Set(ByVal value As Boolean)
            ViewState(ViewStateConstants.IsSaved) = value
        End Set
    End Property

#End Region



#End Region

#Region "Events"
#Region "Page Load Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'PopulateControl()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Drop down Events"

#Region "ddl Cost Centre Selected Index Change"

    Protected Sub ddlCostCentre_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCostCentre.SelectedIndexChanged
        Try
            Dim costCentreId As Integer = CType(ddlCostCentre.SelectedValue, Integer)
            GetBudgetHeadDropDownValuesByCostCentreId(costCentreId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Budget Head Selected Index Change"

    Protected Sub ddlBudgetHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlBudgetHead.SelectedIndexChanged
        Try
            Dim budgetHeadId As Integer = CType(ddlBudgetHead.SelectedValue, Integer)
            GetExpenditureDropDownValuesByBudgetHeadId(budgetHeadId, SessionManager.getUserEmployeeId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Vat Selected Index Change"

    Protected Sub ddlVat_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlVat.SelectedIndexChanged
        Try
            Page.Validate("addWorkRequired")
            CalculateVatAndTotal()
            ddlVat.Focus()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "ddl Contractor Selected Index Change"

    Private Sub ddlContractor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContractor.SelectedIndexChanged
        GetContactDropDownValuesbyContractorId(ddlContractor.SelectedValue)
    End Sub

#End Region

#Region "ddl Contact Selected Index Change"

    Private Sub ddlContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContact.SelectedIndexChanged

        If (Not ddlContact.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue)) Then
            Dim objAssignToContractorBL As AssignToContractorBL = New AssignToContractorBL
            Dim detailsForEmailDS As New DataSet()
            objAssignToContractorBL.getContactEmailDetail(ddlContact.SelectedValue, detailsForEmailDS)

            If IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt)) _
                   Or detailsForEmailDS.Tables(ApplicationConstants.ContactEmailDetailsDt).Rows.Count = 0 Then
                mdlPopupContactAlert.Show()
            End If
        End If


    End Sub

#End Region

    Protected Sub ddlArea_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlArea.SelectedIndexChanged

        populateAttributeList()


    End Sub
#End Region

#Region "Button Events"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            Page.Validate("addWorkRequired")
            If Page.IsValid Then
                AddPurchaseIteminWorkRequiredDT()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub



    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssignToContractor.Click
        Try
            assignWorkToContractor()
            If ddlContact.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValue) Then
                mdlPopupContactAlert.Show()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Grid View Events"

#Region "Grid Works Detail Row data bound"

    Protected Sub grdWorksDeatil_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdWorksDeatil.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Footer Then
                Dim workRequiredDt As DataTable = getWorkRequiredDTViewState()

                If workRequiredDt.Rows.Count > 0 Then
                    e.Row.Cells(1).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.NetCostCol & ")", "").ToString
                    e.Row.Cells(2).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.VatCol & ")", "").ToString
                    e.Row.Cells(3).Text = workRequiredDt.Compute("Sum(" & ApplicationConstants.GrossCol & ")", "").ToString
                End If
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#End Region
#End Region

#Region "Functions"

#Region "populate Block DropDownList"
    Public Sub populateBlockDropDownList()
        Dim schemeId As Integer = SessionManager.getSchemeId()
        Dim resultDataSet As New DataSet()
        Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
        resultDataSet = objAssignToContractorBL.getBlockListBySchemeId(schemeId)

        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlBlock.DataSource = resultDataSet
            ddlBlock.DataTextField = "BLOCKNAME"
            ddlBlock.DataValueField = "BLOCKID"
            ddlBlock.DataBind()
            ddlBlock.Items.Insert(0, New ListItem("Please Select", "0"))
        End If
    End Sub
#End Region

#Region "populate Area List"
    Public Sub populateAreaList()

        Dim resultDataSet As New DataSet()
        Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
        resultDataSet = objAssignToContractorBL.getAreaList()
        ddlArea.Items.Clear()
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlArea.DataSource = resultDataSet
            ddlArea.DataTextField = "AreaName"
            ddlArea.DataValueField = "AreaID"
            ddlArea.DataBind()
        End If
        ddlArea.Items.Insert(0, New ListItem("Please Select", "-1"))
    End Sub
#End Region
#Region "populate Attribute List"
    Public Sub populateAttributeList()

        Dim resultDataSet As New DataSet()
        Dim areaId As Integer = Convert.ToInt32(ddlArea.SelectedValue)
        Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
        resultDataSet = objAssignToContractorBL.getAttributeByAreaId(areaId)
        ddlAttribute.Items.Clear()
        If resultDataSet.Tables(0).Rows.Count > 0 Then
            ddlAttribute.DataSource = resultDataSet
            ddlAttribute.DataTextField = "ItemName"
            ddlAttribute.DataValueField = "ItemID"
            ddlAttribute.DataBind()
        End If
        ddlAttribute.Items.Insert(0, New ListItem("Please Select", "-1"))
    End Sub
#End Region


#Region "Populate User Control By Passing Values from Page"

    Public Sub PopulateControl()


        'Parallel.Invoke(AddressOf GetContractorsList,
        '                AddressOf GetCostCentreDropDownVales)
        GetContractorsList()
        GetCostCentreDropDownVales()
        populateBlockDropDownList()
        GetVatDropDownValues()
        populateAreaList()
        populateAttributeList()
        ResetControls()
        Dim schemeBlockBo As SchemeBlockBO = SessionManager.getSbData()
        lblScheme.Text = schemeBlockBo.SchemeName
        lblReplacementDate.Text = "N/A"
    End Sub

#End Region

#Region "Add Purchase Items in Work Required Data Table"

    Private Sub AddPurchaseIteminWorkRequiredDT()

        Dim objWorkRequiredBo As New WorkRequiredBo
        objWorkRequiredBo.WorkRequired = txtWorksRequired.Text.Trim
        objWorkRequiredBo.NetCost = If(IIf(String.IsNullOrEmpty(txtNetCost.Text.Trim), Nothing, txtNetCost.Text.Trim), 0.0)
        objWorkRequiredBo.VatIdDDLValue = ddlVat.SelectedValue
        objWorkRequiredBo.Vat = If(IIf(String.IsNullOrEmpty(txtVat.Text.Trim), Nothing, txtVat.Text.Trim), 0.0)
        objWorkRequiredBo.Total = If(IIf(String.IsNullOrEmpty(txtTotal.Text.Trim), Nothing, txtTotal.Text.Trim), 0.0)
        objWorkRequiredBo.ExpenditureId = ddlExpenditure.SelectedValue

        'Dim result = Validation.Validate(Of WorkRequiredBo)(objWorkRequiredBo)

        'If result.IsValid Then

        Dim workRequiredDt = getWorkRequiredDTViewState()
        Dim workRequiredRow = workRequiredDt.NewRow

        workRequiredRow(ApplicationConstants.ExpenditureIdCol) = objWorkRequiredBo.ExpenditureId
        workRequiredRow(ApplicationConstants.WorksRequiredCol) = objWorkRequiredBo.WorkRequired
        workRequiredRow(ApplicationConstants.NetCostCol) = objWorkRequiredBo.NetCost
        workRequiredRow(ApplicationConstants.VatTypeCol) = objWorkRequiredBo.VatIdDDLValue
        workRequiredRow(ApplicationConstants.VatCol) = objWorkRequiredBo.Vat
        workRequiredRow(ApplicationConstants.GrossCol) = objWorkRequiredBo.Total
        workRequiredRow(ApplicationConstants.PIStatusCol) = getPIStatus(objWorkRequiredBo.ExpenditureId, objWorkRequiredBo.Total)

        workRequiredDt.Rows.Add(workRequiredRow)

        resetWorksRequired()

        setWorkRequiredDTViewState(workRequiredDt)

        bindWorkRequiredGrid()

        'Else
        'Dim errorMessages As New StringBuilder
        'For Each item In result
        '    errorMessages.AppendLine(item.Message.ToString() + "<br />")
        'Next

        'If errorMessages.Length > 0 Then
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, errorMessages.ToString(), True)
        'End If

        'End If
    End Sub

#End Region

#Region "Get Cost Centre Drop Down Values."

    Private Sub GetCostCentreDropDownVales()

        Dim dropDownList = New List(Of DropDownBO)
        Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
        objAssignToContractorBL.GetCostCentreDropDownVales(dropDownList)

        BindDropDownList(ddlCostCentre, dropDownList)

    End Sub

#End Region

#Region "Get Budget Head Drop Down Values by Cost Centre Id"

    Private Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef CostCentreId As Integer)

        If CostCentreId = ApplicationConstants.DropDownDefaultValue Then
            resetBudgetHead()
            resetExpenditureddl()
        Else
            Dim dropDownList As New List(Of DropDownBO)
            Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
            objAssignToContractorBL.GetBudgetHeadDropDownValuesByCostCentreId(dropDownList, CostCentreId)
            BindDropDownList(ddlBudgetHead, dropDownList)
        End If

    End Sub

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id"

    Private Sub GetExpenditureDropDownValuesByBudgetHeadId(ByVal BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        If BudgetHeadId = ApplicationConstants.DropDownDefaultValue Then
            resetExpenditureddl()
        Else
            Dim expenditureBOList As New List(Of ExpenditureBO)
            Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
            objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)

            'Save expenditure BO list in Session to get Employee LIMIT and remaining at the stage of adding an item
            ' in work required list and to mark it as pending/in queue for authorization if needed.
            SessionManager.setExpenditureBOList(expenditureBOList)

            ddlExpenditure.Items.Clear()
            ddlExpenditure.DataSource = expenditureBOList
            ddlExpenditure.DataTextField = ApplicationConstants.ddlDefaultDataTextField
            ddlExpenditure.DataValueField = ApplicationConstants.ddlDefaultDataValueField
            ddlExpenditure.DataBind()

            If expenditureBOList.Count = 0 Then
                ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.NoExpenditureSetup, ApplicationConstants.DropDownDefaultValue))
            Else
                ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
            End If
        End If
    End Sub

#End Region

#Region "Get Contractor Having Planned Contract"

    Private Sub GetContractorsList()
        Dim dropDownList As New List(Of DropDownBO)
        Dim currentFaultBOObj As FaultBO = DirectCast(SessionManager.getSbSubcontractorCurrentFaultBO, FaultBO)
        Dim objFaultBL As FaultBL = New FaultBL()
        Dim resultDataSet As DataSet = New DataSet()
        objFaultBL.getContractorsByTrade(SessionManager.getTradeType, resultDataSet)

        If (resultDataSet.Tables(0).Rows.Count > 0) Then
            ddlContractor.DataSource = resultDataSet.Tables(0).DefaultView
            ddlContractor.DataValueField = "orgid"
            ddlContractor.DataTextField = "name"
            ddlContractor.DataBind()
        End If

        Dim item As ListItem = New ListItem("Please Select", "-1")
        ddlContractor.Items.Insert(0, item)
        If currentFaultBOObj.OrgID > 0 AndAlso Not IsNothing(ddlContractor.Items.FindByValue(currentFaultBOObj.OrgID)) Then
            ddlContractor.SelectedValue = currentFaultBOObj.OrgID
            GetContactDropDownValuesbyContractorId(ddlContractor.SelectedValue)
        End If

    End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

    Sub GetContactDropDownValuesbyContractorId(ByVal ContractorId As Integer)
        ddlContact.Items.Clear()
        Dim dropDownBoList As New List(Of DropDownBO)

        Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL
        objAssignToContractorBL.GetContactDropDownValuesbyContractorId(dropDownBoList, ContractorId)

        If dropDownBoList.Count = 0 Then
            ddlContact.Items.Insert(0, New ListItem(ApplicationConstants.noContactFound, ApplicationConstants.DropDownDefaultValue))
        Else
            BindDropDownList(ddlContact, dropDownBoList)
        End If
    End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Id as Value Field and Vat Name as text field."

    Sub GetVatDropDownValues()
        Dim vatBoList As New List(Of ContractorVatBO)

        Dim objAssignToContractorBL As SbAssignToContractorBL = New SbAssignToContractorBL

        objAssignToContractorBL.getVatDropDownValues(vatBoList)

        ddlVat.Items.Clear()
        ddlVat.DataSource = vatBoList
        ddlVat.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlVat.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlVat.DataBind()

        ddlVat.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))

        ' Save Vat Bo List in session, to get vat rate while adding a work required.
        SessionManager.setVatBOList(vatBoList)

    End Sub

#End Region

#Region "Bind Drop Down List"

    Private Sub BindDropDownList(ByRef ddlToBind As DropDownList, ByRef dropDownItemsList As List(Of DropDownBO), Optional ByVal insertDefault As Boolean = True)
        ddlToBind.Items.Clear()
        ddlToBind.DataSource = dropDownItemsList
        ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField
        ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField
        ddlToBind.DataBind()

        If insertDefault Then
            ddlToBind.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Calculate Vat and Total"

    Private Sub CalculateVatAndTotal()
        Dim vatRateId As Integer = ddlVat.SelectedValue
        Dim VatRate As Decimal = 0.0

        If vatRateId >= 0 Then
            VatRate = getVatRateByVatId(vatRateId)
        End If

        Dim netCost As Decimal

        If Decimal.TryParse(txtNetCost.Text.Trim, 0.0) Then
            netCost = If(IIf(String.IsNullOrEmpty(txtNetCost.Text.Trim), Nothing, txtNetCost.Text.Trim), 0.0)
            Dim vat As Decimal = netCost * VatRate / 100
            Dim total As Decimal = netCost + vat

            txtNetCost.Text = String.Format("{0:0.00}", netCost)
            txtVat.Text = String.Format("{0:0.00}", vat)
            txtTotal.Text = String.Format("{0:0.00}", total)
        Else
            txtVat.Text = String.Empty
            txtTotal.Text = String.Empty
        End If

    End Sub

#End Region

#Region "Get Vat Rate by VatId"

    Private Function getVatRateByVatId(ByVal vatRateId As Integer) As Decimal
        Dim vatRate As Decimal = 0.0

        Dim vatBoList As List(Of ContractorVatBO) = SessionManager.getVatBOList

        Dim vatBo As ContractorVatBO = vatBoList.Find(Function(i) i.ID = vatRateId)

        If Not IsNothing(vatBo) Then
            vatRate = vatBo.VatRate
        End If

        Return vatRate
    End Function

#End Region

#Region "Reset Controls (on first populate)"

    Private Sub ResetControls()
        'Set is Saved property to false on populate.
        IsSaved = False
        btnAssignToContractor.Enabled = True
        btnAdd.Enabled = True

        resetBudgetHead()
        resetExpenditureddl()

        txtEstimate.Text = String.Empty
        txtEstimateRef.Text = String.Empty

        resetWorksRequired()

        'uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        removeWorkRequiredDTViewState()
        bindWorkRequiredGrid()

    End Sub

#End Region

#Region "Bind Work Required Grid"

    Private Sub bindWorkRequiredGrid()
        Dim dtWorksRequired As DataTable = getWorkRequiredDTViewState()
        Dim addExtraSpaces As Boolean = False

        If dtWorksRequired.Rows.Count = 0 Then
            For Each col As DataColumn In dtWorksRequired.Columns
                col.AllowDBNull = True
            Next
            Dim newRow As DataRow = dtWorksRequired.NewRow
            dtWorksRequired.Rows.Add(newRow)
            addExtraSpaces = True
        End If

        grdWorksDeatil.DataSource = dtWorksRequired
        grdWorksDeatil.DataBind()
        If addExtraSpaces Then
            grdWorksDeatil.Rows(0).Cells(0).Text = "<br /><br /><br />"
        End If
    End Sub

#End Region


#Region "Get Pi Status (Pending Status)"

    Private Function getPIStatus(ByVal expenditureId As Integer, ByVal gross As Decimal) As Object
        Dim pIStatus As Integer = 3 ' 3 = "Work Ordered" in table F_POSTATUS
        Dim expenditureBoList As List(Of ExpenditureBO) = SessionManager.getExpenditureBOList

        'Check if an item is costing 0 (zero) or less then it should not go to queued list.
        If gross > 0 Then
            Dim ExpenditureBo As ExpenditureBO = expenditureBoList.Find(Function(i) i.ID = expenditureId)

            If Not IsNothing(ExpenditureBo) Then
                If (gross > ExpenditureBo.Limit OrElse gross > ExpenditureBo.Remaining) Then
                    pIStatus = 0 ' 0 = "Queued" in table F_POSTATUS
                End If
            End If
        End If

        Return pIStatus
    End Function

#End Region

#Region "Set PO Status"

    Private Function getPOStatusFromWorkItemsDt(ByVal workRequiredDT As DataTable) As Integer
        Dim pOStatus As Integer = 3 ' 3 = "Work Ordered" in table F_POSTATUS

        If (workRequiredDT.Select(ApplicationConstants.PIStatusCol & " = 0").Count > 0) Then
            pOStatus = 0 ' 0 = "Queued" in table F_POSTATUS
        End If

        Return pOStatus
    End Function

#End Region


#Region "Assign Work To Contractor"

    Private Sub assignWorkToContractor()

        Dim workRequiredDT As DataTable = getWorkRequiredDTViewState()

        If workRequiredDT.Rows.Count = 0 Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.workRequiredCount, True)
        Else
            Page.Validate("assignToContractor")
            If Page.IsValid Then
                Dim assignToContractorBo As New AssignToContractorBO
                assignToContractorBo.WorksRequired = workRequiredDT
                assignToContractorBo.ContractorId = ddlContractor.SelectedValue
                assignToContractorBo.ContactId = ddlContact.SelectedValue
                assignToContractorBo.AreaId = ddlArea.SelectedValue
                assignToContractorBo.AttributeId = ddlAttribute.SelectedValue
                assignToContractorBo.Estimate = If(IIf(String.IsNullOrEmpty(txtEstimate.Text.Trim), Nothing, txtEstimate.Text.Trim), 0.0)
                assignToContractorBo.EstimateRef = txtEstimateRef.Text
                assignToContractorBo.SchemeId = SessionManager.getSchemeId
                assignToContractorBo.BlockId = ddlBlock.SelectedValue
                assignToContractorBo.POStatus = getPOStatusFromWorkItemsDt(workRequiredDT)
                'SessionManager.getExpenditureId = ddlExpenditure.s

                assignToContractorBo.UserId = SessionManager.getFaultSchedulingUserId
                Dim jobSheetNumber As Integer = ConfirmSelectedFault()
                assignToContractorBo.JournalId = jobSheetNumber
                Dim objAssignToContractorBl As New SbAssignToContractorBL
                Dim isSavedStatus As Boolean = objAssignToContractorBl.assignToContractor(assignToContractorBo)

                If isSavedStatus Then
                    'Set Property Is Saved to True.
                    Me.IsSaved = True
                    'Disable btnAssignToContractor to avoid assigning the same work more than once. 
                    btnAssignToContractor.Enabled = False
                    btnAdd.Enabled = False

                    Dim message As String = UserMessageConstants.AssignedToContractor
                    Try
                        If (assignToContractorBo.POStatus = ApplicationConstants.WorkOrderedPoStatusId And assignToContractorBo.POCurrentStatus <> ApplicationConstants.QueuedPoStatusId) Then
                            Dim detailsForEmailDS As New DataSet()
                            Dim objAssignToContractor As New AssignToContractorBL
                            objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
                            If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                                                       AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then
                                sendEmailtoContractor(assignToContractorBo)
                            Else
                                message = message + UserMessageConstants.EmailToContractor
                            End If

                        ElseIf (assignToContractorBo.POCurrentStatus = ApplicationConstants.QueuedPoStatusId AndAlso assignToContractorBo.OrderId > 0) Then

                            sendEmailtoBudgetHolder(assignToContractorBo)

                        End If
                    Catch ex As Exception
                        message += "<br />but " + ex.Message
                    End Try
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, message, False)
                Else
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AssignedToContractorFailed, True)
                End If

            End If
        End If

    End Sub

#End Region

#Region "Confirm selected fault and send email to subcontractor"
    Private Function ConfirmSelectedFault()


        uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        Dim objFaultsBl As FaultBL = New FaultBL()
        Dim arrTempFaultBO As ArrayList = DirectCast(SessionManager.getSbTemporaryFaultBo, ArrayList)
        Dim tempFaultBO As TempFaultBO = arrTempFaultBO.Item(0)

        Dim tempFaultId As Integer = tempFaultBO.TempFaultId
        Dim currentFaultBOObj As FaultBO = DirectCast(SessionManager.getSbSubcontractorCurrentFaultBO, FaultBO)
        Dim currentUserId As Integer = SessionManager.getUserEmployeeId()
        Dim jobSheetNumber As Integer


        'Save Fault Info in database
        jobSheetNumber = objFaultsBl.saveSbSubcontractorFaultInfo(currentFaultBOObj, currentUserId, tempFaultId)

        ' Remove fault from session move to next one OR redirect to Basket screen
        arrTempFaultBO.RemoveAt(ViewState(ViewStateConstants.CurrentIndex))

        If Not uiMessageHelper.IsError Then

            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.AssignedToSubcontractor, False)
        End If
        Return jobSheetNumber


    End Function
#End Region

#Region "Populate Body and Send Email to Contractor"

    Private Sub sendEmailtoContractor(ByRef assignToContractorBo As AssignToContractorBO)
        Dim detailsForEmailDS As New DataSet()
        Dim objAssignToContractor As New SbAssignToContractorBL
        objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)

        If Not IsNothing(detailsForEmailDS) Then
            If Not IsNothing(detailsForEmailDS.Tables(0)) _
                AndAlso detailsForEmailDS.Tables(0).Rows.Count > 0 Then

                If String.IsNullOrEmpty(detailsForEmailDS.Tables(0)(0)("Email")) _
                    OrElse Not Validation.isEmail(detailsForEmailDS.Tables(0)(0)("Email")) Then

                    Throw New Exception("Unable to send email, invalid email address.")
                Else

                    Dim body As New StringBuilder
                    Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/AssignWorkToContractor.html"))
                    body.Append(reader.ReadToEnd())

                    ' Set contractor detail(s) '
                    '==========================================='
                    'Populate Contractor Contact Name
                    body.Replace("{ContractorContactName}", detailsForEmailDS.Tables(0)(0)("ContractorContactName"))
                    '==========================================='


                    ' Populate work, estimate and cost details
                    '==========================================='
                    ' Set PMO (planned work reference)
                    body.Replace("{PMO}", "JS" + assignToContractorBo.JournalId.ToString)

                    ' Populate Estimate and Estimate Reference
                    body.Replace("{Estimate}", assignToContractorBo.Estimate)
                    body.Replace("{EstimateRef}", If(assignToContractorBo.EstimateRef, "N/A"))

                    'Get sum/total of net cost from works required data table.
                    body.Replace("{NetCost}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString)
                    'Get sum/total of Vat from works required data table.
                    body.Replace("{VAT}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString)
                    'Get sum/total of Gross/Total from works required data table.
                    body.Replace("{TOTAL}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString)

                    'Get all works detail from works required data table in form of ordered list.
                    body.Replace("{WorksRequired}", getWorksRequired(assignToContractorBo.WorksRequired))
                    '==========================================='

                    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
                    commonAddressBo = SessionManager.getCommonAddressBO()
                    ' Property detail(s) (address)
                    '==========================================='

                    'Get Property Address details from property details data set
                    If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)) _
                        AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then
                        With detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)
                            body.Replace("{Address}", commonAddressBo.Address)
                            body.Replace("{TownCity}", commonAddressBo.City)
                            body.Replace("{County}", commonAddressBo.Area)
                            body.Replace("{PostCode}", commonAddressBo.PostCode)
                        End With
                    Else
                        ' This case may not occur but it is done for completeness
                        body.Replace("{Address}", "N/A")
                        body.Replace("{TownCity}", "")
                        body.Replace("{County}", "")
                        body.Replace("{PostCode}", "")
                    End If
                    '==========================================='

                    ' Attach logo images with email
                    '==========================================='
                    Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                    logo50Years.ContentId = "logo50Years_Id"

                    body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                    Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                    body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                    Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                    Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                    alternatevw.LinkedResources.Add(logo50Years)
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                    '==========================================='

                    Dim mailMessage As New MailMessage

                    mailMessage.Subject = ApplicationConstants.EmailSubject
                    mailMessage.To.Add(New MailAddress(detailsForEmailDS.Tables(0)(0)("Email").ToString, detailsForEmailDS.Tables(0)(0)("ContractorContactName").ToString))
                    ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                    mailMessage.Body = body.ToString
                    mailMessage.AlternateViews.Add(alternatevw)
                    mailMessage.IsBodyHtml = True

                    EmailHelper.sendEmail(mailMessage)

                End If
            Else
                Throw New Exception("Unable to send email, contractor details not available")
            End If
        End If
    End Sub

#End Region

#Region "Get Works Required - Concatenated in form of ordered list to add in email"

    Private Function getWorksRequired(ByVal dataTable As DataTable) As String
        Dim worksRequired As New StringBuilder
        If dataTable.Rows.Count = 1 Then
            worksRequired.Append(dataTable(0)(ApplicationConstants.WorksRequiredCol).ToString)
        Else
            worksRequired.Append("<ol>")
            For Each row As DataRow In dataTable.Rows
                worksRequired.Append("<li>" + row(ApplicationConstants.WorksRequiredCol).ToString + "</li>")
            Next
            worksRequired.Append("</ol>")
        End If
        Return worksRequired.ToString
    End Function

#End Region
#End Region
#Region "Controls Reset Functions"

#Region "Reset Expenditure Dropdown List"

    Private Sub resetExpenditureddl()
        ddlExpenditure.Items.Clear()
        If ddlExpenditure.Items.Count = 0 Then
            ddlExpenditure.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Reset Budget Head"

    Private Sub resetBudgetHead()
        ddlBudgetHead.Items.Clear()
        If ddlBudgetHead.Items.Count = 0 Then
            ddlBudgetHead.Items.Insert(0, New ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue))
        End If
    End Sub

#End Region

#Region "Reset Works Required"

    Private Sub resetWorksRequired()

        If ddlVat.Items.Count > 0 Then
            ddlVat.SelectedIndex = 0
        End If

        txtWorksRequired.Text = String.Empty
        txtNetCost.Text = String.Empty
        txtVat.Text = String.Empty
        txtTotal.Text = String.Empty
    End Sub

#End Region

#End Region
#Region "View State Functions"

#Region "Work Required Data Table"

    ''' <summary>
    ''' Get the Work Required Data Table from the view state if a Data Table is not there
    ''' Create a new data table and add required column.
    ''' This is to be used to get data table of this type for the first time.
    ''' </summary>
    ''' <returns>A data table containing the required columns of work required grid.</returns>
    ''' <remarks></remarks>
    Private Function getWorkRequiredDTViewState() As DataTable
        Dim workRequiredDt As DataTable
        workRequiredDt = TryCast(ViewState(ViewStateConstants.WorkRequiredDT), DataTable)

        If IsNothing(workRequiredDt) Then
            workRequiredDt = New DataTable

            '1- Add Work Required Column with data type string
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.WorksRequiredCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.WorksRequiredCol, GetType(String))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = String.Empty
                'Set Maximum length for Work Required Column.
                insertedColumn.MaxLength = ApplicationConstants.MaxStringLegthWordRequired
            End If

            '2- Add Net Cost Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.NetCostCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.NetCostCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '3- Add Vat type Column with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.VatTypeCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.VatTypeCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            '4- Add Vat Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.VatCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.VatCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '5- Add Gross/Total Column with data type Decimal
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.GrossCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.GrossCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '6- Add PI Status Column with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.PIStatusCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.PIStatusCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
                insertedColumn.DefaultValue = False
            End If

            '7- Add Expenditure ID Col with data type Integer
            If Not workRequiredDt.Columns.Contains(ApplicationConstants.ExpenditureIdCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = workRequiredDt.Columns.Add(ApplicationConstants.ExpenditureIdCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If



        End If

        Return workRequiredDt
    End Function

    Private Sub removeWorkRequiredDTViewState()
        ViewState.Remove(ViewStateConstants.WorkRequiredDT)
    End Sub

    Private Sub setWorkRequiredDTViewState(ByVal dt As DataTable)
        ViewState(ViewStateConstants.WorkRequiredDT) = dt
    End Sub

#End Region







#End Region


    Protected Sub ddlAttributeSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAttribute.SelectedIndexChanged
        Try
            Dim resultDataSet As DataSet = New DataSet()
            Dim objAssignToContractorBl As New SbAssignToContractorBL
            resultDataSet = objAssignToContractorBl.getReplacementDueForAssignToContractor(ddlAttribute.SelectedValue, Convert.ToInt32(SessionManager.getSchemeId), ddlBlock.SelectedValue)
            If Not IsNothing(resultDataSet.Tables(0)) AndAlso resultDataSet.Tables(0).Rows.Count > 0 Then
                lblReplacementDate.Text = If(Not IsDBNull(resultDataSet.Tables(0).Rows(0).Item("DueDate")), resultDataSet.Tables(0).Rows(0).Item("DueDate"), "N/A")
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClose.Click

        DirectCast(Me.Page, SbFaultBasket).loadParent()

    End Sub

    Private Sub sendEmailtoBudgetHolder(ByRef assignToContractorBo As AssignToContractorBO)
        Dim workRequiredDT As DataTable = getWorkRequiredDTViewState()
        Dim detailsForEmailDS As New DataSet()
        Dim budgetHolderDs As New DataSet()
        Dim objAssignToContractor As New AssignToContractorBL
        objAssignToContractor.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)

        For Each item As DataRow In workRequiredDT.Rows

            objAssignToContractor.getBudgetHolderByOrderId(budgetHolderDs, assignToContractorBo.OrderId, item("ExpenditureId"))
            If Not IsNothing(detailsForEmailDS) Then
                If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt)) _
                    AndAlso detailsForEmailDS.Tables(ApplicationConstants.ContractorDetailsDt).Rows.Count > 0 Then


                    If (DBNull.Value.Equals(budgetHolderDs.Tables(0)(0)("Email"))) Then
                        Throw New Exception("email is not sent as email id of budget holder is not available.")
                    End If

                    If String.IsNullOrEmpty(budgetHolderDs.Tables(0)(0)("Email")) _
                        OrElse Not isEmail(budgetHolderDs.Tables(0)(0)("Email")) Then

                        Throw New Exception("Unable to send email, invalid email address.")
                    Else

                        Dim body As New StringBuilder
                        Dim reader As StreamReader = New StreamReader(Server.MapPath("~/Email/AssignWorkToContractor.html"))
                        body.Append(reader.ReadToEnd())

                        ' Set contractor detail(s) '
                        '==========================================='
                        'Populate Contractor Contact Name
                        body.Replace("{ContractorContactName}", budgetHolderDs.Tables(0)(0)("OperativeName"))
                        '==========================================='


                        ' Populate work, estimate and cost details
                        '==========================================='
                        ' Set PMO (planned work reference)
                        body.Replace("{PMO}", "JS" + assignToContractorBo.JournalId.ToString)

                        ' Populate Estimate and Estimate Reference
                        body.Replace("{Estimate}", assignToContractorBo.Estimate)
                        body.Replace("{EstimateRef}", If(assignToContractorBo.EstimateRef, "N/A"))

                        'Get sum/total of net cost from works required data table.
                        body.Replace("{NetCost}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString)
                        'Get sum/total of Vat from works required data table.
                        body.Replace("{VAT}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString)
                        'Get sum/total of Gross/Total from works required data table.
                        body.Replace("{TOTAL}", assignToContractorBo.WorksRequired.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString)

                        'Get all works detail from works required data table in form of ordered list.
                        'body.Replace("{WorksRequired}", getWorksRequired(assignToContractorBo.WorksRequired))
                        body.Replace("{WorksRequired}", item(ApplicationConstants.WorksRequiredCol))

                        '==========================================='

                        Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
                        commonAddressBo = SessionManager.getCommonAddressBO()
                        ' Property detail(s) (address)
                        '==========================================='

                        'Get Property Address details from property details data set
                        If Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)) _
                            AndAlso detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT).Rows.Count > 0 Then
                            With detailsForEmailDS.Tables(ApplicationConstants.PropertyDetailsDT)(0)
                                body.Replace("{Address}", commonAddressBo.Address)
                                body.Replace("{TownCity}", commonAddressBo.City)
                                body.Replace("{County}", commonAddressBo.Area)
                                body.Replace("{PostCode}", commonAddressBo.PostCode)
                            End With
                        Else
                            ' This case may not occur but it is done for completeness
                            body.Replace("{Address}", "N/A")
                            body.Replace("{TownCity}", "")
                            body.Replace("{County}", "")
                            body.Replace("{PostCode}", "")
                        End If
                        '==========================================='

                        ' Attach logo images with email
                        '==========================================='
                        Dim logo50Years As LinkedResource = New LinkedResource(Server.MapPath("~/Images/50_Years.gif"))
                        logo50Years.ContentId = "logo50Years_Id"

                        body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

                        Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"))
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

                        body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

                        Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

                        Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body.ToString, mimeType)
                        alternatevw.LinkedResources.Add(logo50Years)
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs)
                        '==========================================='

                        Dim mailMessage As New MailMessage

                        mailMessage.Subject = ApplicationConstants.EmailSubject
                        mailMessage.To.Add(New MailAddress(budgetHolderDs.Tables(0)(0)("Email").ToString, budgetHolderDs.Tables(0)(0)("OperativeName").ToString))
                        ' For a graphical view with logos, alternative view will be visible, body is attached for text view.
                        mailMessage.Body = body.ToString
                        mailMessage.AlternateViews.Add(alternatevw)
                        mailMessage.IsBodyHtml = True

                        EmailHelper.sendEmail(mailMessage)

                    End If
                Else
                    Throw New Exception("Unable to send email, contractor details not available")
                End If
            End If
        Next
    End Sub

#Region "Is Email"
    Public Shared Function isEmail(ByVal value As String) As Boolean
        Dim rgx As New Regex(RegularExpConstants.emailExp)
        If rgx.IsMatch(value) Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region


#Region "Get Risk Details - As concatenated string split on separate row."

    Private Function getRiskDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim RiskDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows.Count > 0 Then

            RiskDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantRiskDetailsDt).Rows
                RiskDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
            Next
        End If

        Return RiskDetails.ToString
    End Function

#End Region

#Region "Get Vulnerability Details - As concatenated string split on separate row."

    Private Function getVulnerabilityDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim vulnerabilityDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows.Count > 0 Then

            vulnerabilityDetails.Clear()

            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.TenantVulnerabilityDetailsDt).Rows
                vulnerabilityDetails.Append(row("CATDESC") + If(String.IsNullOrEmpty(row("SUBCATDESC")), "", ": " + row("SUBCATDESC")) + "<br />")
            Next

        End If

        Return vulnerabilityDetails.ToString
    End Function

#End Region

#Region "Get Asbestos detail"

    Private Function getAsbestosDetails(ByVal detailsForEmailDS As DataSet) As String
        Dim asbestosDetails As New StringBuilder("N/A")

        If Not IsNothing(detailsForEmailDS) _
            AndAlso Not IsNothing(detailsForEmailDS.Tables(ApplicationConstants.AsbestosDetailsDt)) _
            AndAlso detailsForEmailDS.Tables(ApplicationConstants.AsbestosDetailsDt).Rows.Count > 0 Then

            asbestosDetails.Clear()
            For Each row As DataRow In detailsForEmailDS.Tables(ApplicationConstants.AsbestosDetailsDt).Rows
                asbestosDetails.Append(row("ASBRISKLEVELDESCRIPTION") + If(String.IsNullOrEmpty(row("RISKDESCRIPTION")), "", ": " + row("RISKDESCRIPTION")) + "<br />")
            Next

        End If
        Return asbestosDetails.ToString

    End Function

#End Region

End Class