﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="JobSheetSummarySubcontractorUpdate.ascx.vb"
    Inherits="FaultScheduling.JobSheetSummarySubcontractorUpdate" %>
<link href="~/Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function PrintJobSheet() {
        javascript: window.open('PrintSubcontractorJobSheetSummary.aspx');
    }    

</script>
<asp:UpdatePanel ID="updPnlJobSheetSummary" runat="server">
    <ContentTemplate>
        <table class="modalPopup" style="width: 100%; min-width: 100%;">
            <tr>
                <td>
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : "
                        Font-Bold="true"></asp:Label>
                    <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp;,
                    <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                    &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel :" Font-Bold="true"> </asp:Label><asp:Label
                        ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFaultLogID" runat="server" Visible="false" />
                    <asp:Label ID="lblJSN" runat="server" ClientIDMode="Static" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Font-Bold="True">Status : </asp:Label>
                    <asp:Label ID="lblStatus" runat="server" Font-Bold="True"></asp:Label>
                    <asp:DropDownList ID="ddlStatus" runat="server" Visible="False">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Contractor :
                </td>
                <td>
                    <asp:Label ID="lblContractor" runat="server" Font-Bold="true"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Operative :
                </td>
                <td>
                    <asp:Label ID="lblOperativeName" runat="server">-</asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Priority :
                </td>
                <td>
                    <asp:Label ID="lblPriority" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Completion Due :
                </td>
                <td>
                    <asp:Label ID="lblCompletionDateTime" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Order Date :
                </td>
                <td>
                    <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                </td>
                <td>
                    Tel : &nbsp;<asp:Label ID="lblClientTelPhoneNumber" runat="server" Text=" " Font-Bold="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Location :
                </td>
                <td>
                    <asp:Label ID="lblLocation" runat="server"></asp:Label>
                </td>
                <td>
                    Mobile: &nbsp;
                    <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Description:
                </td>
                <td>
                    <asp:Label ID="lblDescription" runat="server" Font-Bold="True"></asp:Label>
                </td>
                <td>
                    Email :
                    <asp:Label ID="lblClientEmail" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Trade :
                </td>
                <td>
                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Note :
                </td>
                <td>
                    <asp:TextBox ID="lblNotes" CssClass="roundcornerby5" runat="server" Enabled="false"
                        TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Repair Notes :
                </td>
                <td>
                    <asp:TextBox ID="txtBoxRepairNotes" CssClass="roundcornerby5" runat="server" Enabled="false"
                        TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <hr />
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <b>Appointment</b>
                </td>
                <td valign="top">
                    <asp:Label ID="lblTime" runat="server" Text="N/A"></asp:Label><br />
                    <br />
                </td>
                <td>
                    <b>Asbestos</b>
                    <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                        BorderStyle="None">
                        <Columns>
                            <asp:BoundField DataField="AsbRiskID" />
                            <asp:BoundField DataField="Description" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <ajaxToolkit:ModalPopupExtender ID="mdlPopupJobSheetSummarySubContractorUpdate" runat="server"
                        PopupControlID="updPnlJobSheetSummary" TargetControlID="lblJSN" BackgroundCssClass="modalBackground"
                        CancelControlID="btnBack">
                    </ajaxToolkit:ModalPopupExtender>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div style="width: 100%; text-align: left; clear: both;">
                        <div class="text_a_r_padd__r_20px">
                            <asp:Button ID="btnBack" runat="server" Text=" &lt; Back" CssClass="margin_right20"
                                Width="100px" />
                            <asp:Button ID="btnUpdate" runat="server" CssClass="margin_right20" Text="Update"
                                Width="100px" />
                            <asp:Button ID="btnPrintJobSheet" runat="server" Text="Print Job Sheet" CssClass="margin_right20"
                                OnClientClick="PrintJobSheet()" Width="100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdatePanel runat="server" ID="updPnlPopupFaultNotes">
    <ContentTemplate>
        <table class="modalPopup">
            <tr>
                <td>
                    <asp:Label ID="lblPopupHeaderSaveNotes" runat="server" Font-Bold="true" Text="Cancel Fault" />
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlMessagePopupFaultNotes" runat="server" Visible="false">
                        <asp:Label ID="lblMessagePopupFaultNotes" runat="server">
                        </asp:Label>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPopupNotesHeading" runat="server" Text="Enter The reason for Cancellation Below:" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtPopupNotesFault" runat="server" Rows="4" TextMode="MultiLine" />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Panel ID="pnlPopupSaveMsg" runat="server" Font-Bold="true" Visible="false">
                        <asp:Label ID="lblPopupSaveMsg" runat="server" Text="The Fault Has Been Canceled." />
                        <br />
                        <asp:LinkButton ID="lnkBtnCustomerRecord" runat="server" Text="Click here " />to
                        Return to the customer's record.</asp:Panel>
                    <ajaxToolkit:ModalPopupExtender ID="mdlPopupSaveNotes" runat="server" TargetControlID="txtPopupNotesFault"
                        PopupControlID="updPnlPopupFaultNotes" BackgroundCssClass="modalBackground">
                    </ajaxToolkit:ModalPopupExtender>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnPopupSave" runat="server" Text="Save" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
