﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FaultDuration

    '''<summary>
    '''pnlDuration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDuration As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''imgBtnEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnEdit As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''lblDuration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDuration As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlDuration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDuration As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''imgBtnDone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgBtnDone As Global.System.Web.UI.WebControls.ImageButton
End Class
