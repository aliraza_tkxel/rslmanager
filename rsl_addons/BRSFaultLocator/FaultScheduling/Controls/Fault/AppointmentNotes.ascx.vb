﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports FLS_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class AppointmentNotes
    Inherits UserControlBase

#Region "Control Events"

    Public Event CloseButtonClicked As EventHandler

#End Region

#Region "Events"

#Region "Page Load "
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblPopupMessage, pnlPopupMessage)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.Message, True)
            End If
        End Try


    End Sub
#End Region


#End Region

#Region "Functions"

#Region "Load appointment data"
    ''' <summary>
    ''' load appointment data
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub loadAppointmentNotes(ByVal appointmentId As Integer, ByVal appointmentType As String)
        Dim objCalendarBL As CalendarBL = New CalendarBL()
        Dim dsAppointmentNotes As DataSet = New DataSet()

        objCalendarBL.getAppointmentNotes(dsAppointmentNotes, appointmentId, appointmentType)
        Dim dtAppointmentDetail As DataTable = dsAppointmentNotes.Tables(0)
        If dtAppointmentDetail.Rows.Count > 0 Then
            txtAppointmentNotes.Text = dtAppointmentDetail.Rows(0)("notes")
        Else
            uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.ProblemLoadingData, True)
        End If

    End Sub
#End Region


#End Region

End Class