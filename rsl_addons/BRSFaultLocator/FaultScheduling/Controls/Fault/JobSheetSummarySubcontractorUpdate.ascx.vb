﻿Imports FLS_Utilities
Imports FLS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports FLS_BusinessObject

Public Class JobSheetSummarySubcontractorUpdate
    Inherits UserControlBase

#Region "Data Members"
    Dim FaultBL As New FaultBL()
    Dim jSNClass As String = String.Empty
#End Region

#Region "Events"

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        uiMessageHelper.resetMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes)

        ResetControls()
    End Sub

#End Region

    'End Region Page Events
#End Region

#Region "Button Click Events"

#Region "Save Popup Button Click Event"

    Protected Sub btnPopupSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPopupSave.Click
        SaveUpdatedFaultStatus()
    End Sub

#End Region

#Region "Update Fault with selected status and status Notes"

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click

        If btnUpdate.Text = "Update" Then
            btnUpdate.Text = "Save"

            lblStatus.Visible = False
            ddlStatus.Visible = Not (lblStatus.Visible)

            Dim LookUpList As New List(Of LookUpBO)

            FaultBL.getFaultStatusLookUpSubContractor(LookUpList)

            ddlStatus.Items.Clear()
            ddlStatus.DataSource = LookUpList
            ddlStatus.DataValueField = "LookUpValue"
            ddlStatus.DataTextField = "LookUpName"
            ddlStatus.DataBind()

            ddlStatus.SelectedValue = 15

        ElseIf btnUpdate.Text = "Save" Then
            btnUpdate.Text = "Update"

            pnlPopupSaveMsg.Visible = False
            txtPopupNotesFault.Text = String.Empty
            If (ddlStatus.Items.Count > 0 AndAlso ddlStatus.SelectedItem.Text = "Cancelled") Then
                lblPopupHeaderSaveNotes.Text = "Cancel Fault"
                lblPopupNotesHeading.Text = "Enter the reason for Cancellation below:"
                lblPopupSaveMsg.Text = "The Fault has been canceled."
                mdlPopupSaveNotes.Show()
            ElseIf (ddlStatus.Items.Count > 0 AndAlso ddlStatus.SelectedItem.Text = "No Entry") Then
                lblPopupHeaderSaveNotes.Text = "No Entry"
                lblPopupNotesHeading.Text = "Enter the reason for No Entry below:"
                lblPopupSaveMsg.Text = "The Fault Has been set to No Entry."
                mdlPopupSaveNotes.Show()
            ElseIf (ddlStatus.Items.Count > 0 AndAlso ddlStatus.SelectedItem.Text = "Complete") Then
                lblPopupHeaderSaveNotes.Text = "Complete Fault"
                lblPopupNotesHeading.Text = "Enter the notes for Completion below:"
                lblPopupSaveMsg.Text = "The Fault has been completed."
                mdlPopupSaveNotes.Show()
            End If

        End If
        mdlPopupJobSheetSummarySubContractorUpdate.Show()

    End Sub

#End Region

#Region "Job Sheet Summary Back Button Click / Close the Pop up Window"

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click

    End Sub

#End Region

    'End Region Button Click Events
#End Region

#Region "Link Button Click"

    Protected Sub lnkBtnCustomerRecord_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkBtnCustomerRecord.Click
        mdlPopupSaveNotes.Hide()
        mdlPopupJobSheetSummarySubContractorUpdate.Hide()
    End Sub

#End Region

    'End Region Events
#End Region

#Region "Functions "

#Region "Get Job Sheed Summary Detail By JSN(Job Sheed Number)"

    Private Sub populateJobSheetSummary(ByRef jsn As String)

        Dim dsJobSheetSummary As New DataSet()

        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryDetailTable)
        dsJobSheetSummary.Tables.Add(ApplicationConstants.jobSheetSummaryAsbestosTable)

        FaultBL.getJobSheetSummaryByJSN(dsJobSheetSummary, jsn)

        If (dsJobSheetSummary.Tables.Count > 0 AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows.Count > 0) Then

            With dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryDetailTable).Rows(0)

                lblFaultLogID.Text = .Item("FaultLogID")
                lblJSN.Text = jsn
                lblStatus.Text = .Item("FaultStatus") '"In Progress"
                lblContractor.Text = .Item("Contractor")
                lblPriority.Text = .Item("priority")
                lblCompletionDateTime.Text = .Item("completionDue")
                lblOrderDate.Text = .Item("orderDate")
                lblLocation.Text = .Item("Location")
                lblDescription.Text = .Item("Description")
                lblTrade.Text = .Item("Trade")
                lblNotes.Text = .Item("Notes")
                txtBoxRepairNotes.Text = .Item("RepairNotes")

                lblClientName.Text = .Item("ClientName")
                lblClientNameHeader.Text = lblClientName.Text

                lblClientStreetAddress.Text = .Item("ClientStreetAddress")
                lblClientStreetAddressHeader.Text = lblClientStreetAddress.Text

                lblClientCity.Text = .Item("ClientCity")
                lblClientCityHeader.Text = lblClientCity.Text

                lblClientPostCode.Text = .Item("ClientPostCode")
                lblClientRegion.Text = .Item("ClientRegion")

                lblClientTelPhoneNumber.Text = .Item("ClientTel")
                lblClientTelPhoneNumberHeader.Text = lblClientTelPhoneNumber.Text

                lblClientMobileNumber.Text = .Item("ClientMobile")
                lblClientEmail.Text = .Item("ClientEmail")

            End With
        End If

        'Bind Asbestos Grid with data from Database for JSN
        If (dsJobSheetSummary.Tables.Count > 1 _
            AndAlso dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable).Rows.Count > 0) Then
            grdAsbestos.DataSource = dsJobSheetSummary.Tables(ApplicationConstants.jobSheetSummaryAsbestosTable)
            grdAsbestos.DataBind()
        End If


    End Sub

#End Region

#Region "Update Fault Status"

    Private Sub SaveUpdatedFaultStatus()

        If Trim(txtPopupNotesFault.Text) <> String.Empty Then

            setFaultStatusSubContractorUpdate()
            pnlPopupSaveMsg.Visible = True

        Else
            uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, "Please provide notes.", True)

        End If
        mdlPopupJobSheetSummarySubContractorUpdate.Show()
        mdlPopupSaveNotes.Show()
    End Sub

#End Region

#Region "Show Job Sheet Summary Sub Contractor Update"

    Protected Friend Sub showPopupJobSheetSummary(ByRef jSN As String)
        ResetControls()
        populateJobSheetSummary(jSN)

        mdlPopupJobSheetSummarySubContractorUpdate.Show()

    End Sub

#End Region

#Region "Save/Set Fault Stauts in Database with Notes"

    Private Sub setFaultStatusSubContractorUpdate()
        Try
            Dim faultBO As New FaultBO
            faultBO.FaultID = CType(lblFaultLogID.Text, Integer)
            faultBO.StatusID = CType(ddlStatus.SelectedValue, Integer)
            faultBO.Notes = txtPopupNotesFault.Text
            faultBO.UserID = 65 'SessionManager.getUserEmployeeId
            faultBO.SubmitDate = DateTime.Now

            FaultBL.setFaultStatusSubContractorUpdate(faultBO)
            If faultBO.IsFlagStatus Then
                uiMessageHelper.IsError = False
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.message = faultBO.UserMsg
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
        Finally
            If uiMessageHelper.IsError Then
                uiMessageHelper.setMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes, uiMessageHelper.message, uiMessageHelper.IsError)
            Else
                uiMessageHelper.resetMessage(lblMessagePopupFaultNotes, pnlMessagePopupFaultNotes)
                pnlPopupSaveMsg.Visible = True
            End If
        End Try
    End Sub

#End Region

#Region "Reset All Controls to default values"

    Private Sub ResetControls()
        lblStatus.Visible = True
        ddlStatus.Visible = Not (lblStatus.Visible)

        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        lblFaultLogID.Text = String.Empty
        lblJSN.Text = String.Empty
        lblStatus.Text = String.Empty
        lblContractor.Text = String.Empty
        lblPriority.Text = String.Empty
        lblCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblLocation.Text = String.Empty
        lblDescription.Text = String.Empty
        lblTrade.Text = String.Empty
        lblNotes.Text = String.Empty
        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmail.Text = String.Empty

        grdAsbestos.DataSource = Nothing
        grdAsbestos.DataBind()


    End Sub

#End Region

#End Region



End Class