﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Warranties.ascx.vb" Inherits="FaultScheduling.Warranties" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%--Add appropriate style sheet in web form or master page.--%>
<style type="text/css" media="all">
    .warranties
    {
        background-color: White;
    }
    
         .modalBack
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
            border: 1px solid black;
        }
        
    .warranties .popupHeader
    {
        font-weight: bold;
        padding-left: 20px;
        padding-top: 10px;
    }
    .warranties .popupHeader .warrantiesText{
        font-weight: normal;
    }
    .warranties .worksRequiredGrid
    {
        padding-left: 20px;
        padding-right: 20px;
        max-height: 200px;
        max-width: 800px;
        overflow: auto;
    }
    .warranties .input_row
    {
        margin-bottom: 10px;
        padding-left: 20px;
    }
    .warranties .input_label
    {
        width: 150px;
        float: left;
    }
    .warranties .input
    {
        width: auto;
    }
    .warranties .input *
    {
        width: 250px;
    }
    .warranties .input_small
    {
        width: auto;
    }
    .warranties .input_small *
    {
        width: 140px;
    }
    .warranties .input_small .addButton
    {
        width: 60px;
        margin-left: 40px;
    }
    
    .warranties .bottomButtonsContainer
    {
        width: 100%;
        height: 100%;
    }
    
    .warranties .bottomButtonsContainer .buttonFloatLeft
    {
        float: left;
        text-align: left;
        padding-left: 20px;
    }
    
    .warranties .bottomButtonsContainer .buttonFloatRight
    {
        float: right;
        text-align: right;
        padding-right: 20px;
    }
    
    .warranties .worksRequired
    {
        word-wrap: break-word;
    }
    .warranties .WorksRequiredCount
    {
        display: none;
    }
    
    .clear
    {
        clear: both;
    }

    .warrantiesGrid 
    {
        
    }

    .warrantiesGridRow 
    {
        margin-bottom:4px;
        padding-bottom:4px !important;
        padding-top:4px !important;
        border-bottom:2px solid black;
        
    }
    .warrantiesRow 
    {
        padding-bottom:4px !important;
        padding-top:8px !important;
    }
</style>
<asp:UpdatePanel ID="updpnlwarranties" runat="server">
    <ContentTemplate>
        <div class="warranties" style="margin-left:0px;">
            <div class="popupHeader">
                <strong>Warranties</strong>
                <br />
                <br />
                <asp:Panel ID="pnlMessage" runat="server" Width="100%" Visible="true">
                    <asp:Label ID="lblMessage" runat="server" class="warrantiesText">
                        The property currently has the following warranties
                    </asp:Label>
                </asp:Panel>
            </div>
            <br />
            <br />
            <div class="worksRequiredGrid">
                <asp:GridView runat="server" ID="grdWarrantiesList" GridLines="None"
                    AutoGenerateColumns="False" PageSize="100" ShowHeaderWhenEmpty="true" Width="100%"
                    CssClass="warrantiesGrid">
                    <HeaderStyle Font-Bold="false" HorizontalAlign="Center"/>
                    <Columns>
                        <asp:BoundField HeaderText="Warranty-type:"
                            DataField="WARRANTYTYPE" 
                            HeaderStyle-CssClass="warrantiesGridRow" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                            ItemStyle-CssClass="warrantiesRow"/>
                        <asp:BoundField HeaderText="Category:"
                            DataField="LOCATIONNAME"
                            HeaderStyle-CssClass="warrantiesGridRow" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                            ItemStyle-CssClass="warrantiesRow"/>
                        <asp:BoundField HeaderText="Area:" 
                            DataField="AREANAME" 
                            HeaderStyle-CssClass="warrantiesGridRow" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                            ItemStyle-CssClass="warrantiesRow"/>
                        <asp:BoundField HeaderText="Item:" 
                            DataField="ITEMNAME" 
                            HeaderStyle-CssClass="warrantiesGridRow" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                            ItemStyle-CssClass="warrantiesRow"/>
                        <asp:BoundField HeaderText="Contractor:" 
                            DataField="CONTRACTOR" 
                            HeaderStyle-CssClass="warrantiesGridRow" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                            ItemStyle-CssClass="warrantiesRow"/>
                        <asp:BoundField HeaderText="Expiry:"
                            DataField="EXPIRYDATE"
                            HeaderStyle-CssClass="warrantiesGridRow" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="Small"
                            ItemStyle-CssClass="warrantiesRow"/>
                    </Columns>
                </asp:GridView>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div class="bottomButtonsContainer">
                <div class="buttonFloatRight">
                    <asp:Button ID="btnCloseWindow" runat="server" Style="text-align: right" Text="Close Window"/>
                </div>
                <div class="clear" />
            </div>
            <div class="clear" />
            <br />
            <br />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
