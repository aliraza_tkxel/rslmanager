﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LocationOperative.ascx.vb" Inherits="FaultScheduling.LocationOperative" %>
<div class="border_height40pc">
    <asp:UpdatePanel  runat="server">
        <ContentTemplate>
            <div class="fl_width_100_text_a_l">
                <asp:GridView ID="grdCurrentFaults" runat="server" AutoGenerateColumns="False" 
                    CssClass="gridfaults" DataKeyNames="FaultID" GridLines="None" Height="200px" 
                    Width="100%">
                    <Columns>
                        <asp:BoundField DataField="JSN" HeaderText="JSN" HtmlEncode="false" />
                        <asp:TemplateField HeaderText="Location">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("ElementName") %>'></asp:Label>
                                &gt;
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" 
                                    Text='<%# Bind("FaultDetail") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Duration" HeaderText="Duration" HtmlEncode="false" />
                        <asp:BoundField DataField="Trade" HeaderText="Trade" />
                        <asp:BoundField DataField="Due" HeaderText="Due" />
                    </Columns>
                </asp:GridView>
            </div>
            <div class="fl_width_100_text_a_l">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CssClass="gridfaults" DataKeyNames="FaultID" GridLines="None" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="Operative" HeaderText="Operative">
                        <ItemStyle Width="20%" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Time">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Bind("ElementName") %>'></asp:Label>
                                &gt;
                                <asp:Label ID="Label4" runat="server" Font-Bold="true" 
                                    Text='<%# Bind("FaultDetail") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="30%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="AppointmentDate" 
                            DataFormatString="{0:dddd dd MMMM yyyy}" HeaderText="Date" HtmlEncode="false">
                        <ItemStyle Width="40%" />
                        </asp:BoundField>
                        <asp:ButtonField ButtonType="Button" CommandName="view" Text="Select">
                        <ItemStyle Width="10%" />
                        </asp:ButtonField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="text_a_r_padd__r_20px">
                <asp:Button ID="btnBackToBasket" runat="server" Text="&lt; Back To Basket" />
                <asp:Button ID="btnRefreshList" runat="server" Text="Refresh List" />
                <asp:Button ID="btnViewCalandar" runat="server" Text="View Calandar" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>

