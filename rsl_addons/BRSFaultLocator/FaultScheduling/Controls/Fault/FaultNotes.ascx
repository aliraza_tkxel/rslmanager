﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FaultNotes.ascx.vb"
    Inherits="FaultScheduling.FaultNotes" %>
    <script type="text/javascript" language="javascript">
        function showMessageBox() {
            alert("Fault Notes Updated Successfully !!");
        }
    </script> 
<style type='text/css'>
    .apt-notes-popup
    {
        width: 446px;
        background-color: white;
        margin: 0 0 0 0px;
        border: 1px solid black;
        padding-bottom: 10px;
    }
    
    .popup-sub-container
    {
        padding: 5px 10px 5px 10px;
    }
    
    .right-align
    {
        float: right;
    }
    
    .left-align
    {
        float: left;
    }
    
    td
    {
        padding: 4px;
    }
    
    input[type="submit"]
    {
        background-color: White;
        border-radius: 5px;
        padding: 4px;
    }
    .style1
    {
        width: 68px;
    }
</style>
<asp:UpdatePanel ID="updPanelAppointmentNotes" runat="server">
    <ContentTemplate>
        <div><!--class="apt-notes-popup"-->
            <div class="popup-sub-container">
                <asp:Image ID="Image1" runat="server" Width="20px" Height="20px" ImageUrl="~/Images/editCustomerNotes.png" style="margin-left:4px"  />
                &nbsp;
               
               <asp:Label ID="Label1" runat="server" Text="Fault Notes" Font-Bold="true" style="font-weight:bold;position:absolute;top:9px"></asp:Label>
                    
                
            </div>
            <hr />
            <div class="popup-sub-container">
                <label>
                    &nbsp;&nbsp;Location</label>:&nbsp;
                <asp:Label ID="lblLocation" runat="server" style="margin-left:12px"></asp:Label>
            </div>
            <hr />
            <div class="popup-sub-container">
                <table style="width: 100%;">
                    <tr>
                        <td></td>
                        <td><asp:Label ID="lblMessage" Text=" " runat="server"></asp:Label></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td valign="top" style="width:60px" >
                            Notes:
                        </td>
                        <td>
                            <asp:TextBox ID="txtFaultNotes" TextMode="MultiLine" runat="server" ReadOnly="false"
                                Height="100px" Width="90%" style="overflow:auto"></asp:TextBox>
                        </td>
                        <td>
                            <asp:HiddenField runat="server" Value="" ID="hfTempFaultID" />
                        </td>
                    </tr>
                </table>
                
            </div>
            <hr /><br />
            <span style="width: 95%; float: left;">
                    <asp:Button runat="server" ID="btnClosePopup" Text="Close" style="border:1px solid grey;margin-left:20px;"/>
                    <asp:Button runat="server" ID="btnSaveChanges" Text="Save Changes" style="float: right;border:1px solid grey;"/>
            </span>
            <br /><br /><br />
        </div>

    </ContentTemplate>
</asp:UpdatePanel>
