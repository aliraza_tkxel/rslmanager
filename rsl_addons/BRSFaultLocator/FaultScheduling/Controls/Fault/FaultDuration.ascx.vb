﻿Imports System.ComponentModel
Imports FLS_Utilities

Public Class FaultDuration
    Inherits System.Web.UI.UserControl

#Region "Properties"

    Public ReadOnly Property DoneButton As ImageButton
        Get
            Return imgBtnDone
        End Get
    End Property

    <Browsable(True)> _
    Public Property Duration As Decimal
        Get
            Return ddlDuration.SelectedValue
        End Get
        Set(value As Decimal)
            populateDurationControls(value)
        End Set
    End Property

    <Browsable(True)> _
    Public Property DoneButtonCommandArguments() As String
        Get
            Return imgBtnDone.CommandArgument
        End Get
        Set(ByVal value As String)
            imgBtnDone.CommandArgument = value
        End Set
    End Property

#End Region

#Region "Delegates"

    Public Delegate Sub doneButton_Clicked(ByVal sender As Object, ByVal e As EventArgs)

#End Region

#Region "Events"

    Public Event DoneButtonClick As doneButton_Clicked

#End Region

#Region "Events Handling"

#Region "Image btn Edit Click"
    Protected Sub imgBtnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgBtnEdit.Click
        lblDuration.Visible = False
        imgBtnEdit.Visible = lblDuration.Visible
        ddlDuration.Visible = Not lblDuration.Visible
        imgBtnDone.Visible = Not lblDuration.Visible
    End Sub
#End Region

#Region "Image btn Done Click"
    Protected Sub imgBtnDone_Click(ByVal sender As Object, ByVal e As EventArgs) Handles imgBtnDone.Click
        lblDuration.Visible = True
        imgBtnEdit.Visible = lblDuration.Visible
        ddlDuration.Visible = Not lblDuration.Visible
        imgBtnDone.Visible = Not lblDuration.Visible

        RaiseEvent DoneButtonClick(sender, e)
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate Duration Controls"
    ''' <summary>
    ''' This function 'll create the duration control on the top of trade grid
    ''' </summary>
    ''' <param name="durationString">Duration string to show in duration label</param>
    ''' <param name="duration">Duration to select drop down value.</param>
    ''' <param name="doneButtonCommandArgument">Optional command arguments for doneButton.</param>
    ''' <remarks></remarks>
    Public Sub populateDurationControls(ByVal duration As Decimal, Optional ByVal doneButtonCommandArgument As String = "", Optional ByVal durationString As String = Nothing, Optional ByVal durationPostFix As String = "Hour")

        'Duration Label
        setDurationLabel(duration, durationString, durationPostFix)

        'Duration Dropdown        
        populateDurationDropdownList(ddlDuration, durationPostFix)
        ddlDuration.SelectedValue = duration

        'Done ImageButton
        imgBtnDone.CommandArgument = doneButtonCommandArgument

    End Sub
#End Region

#Region "Populate duration dropdown"

    ''' <summary>
    ''' Populate duration dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateDurationDropdownList(ByRef ddl As DropDownList, Optional ByVal durationPostFix As String = "hr")
        Dim newListItem As ListItem
        ddl.Items.Clear()
        For i = 0.5D To GeneralHelper.getMaxFaultDuration Step 0.5
            If (i <= 1) Then
                newListItem = New ListItem(i & " " & durationPostFix, i)
            Else
                newListItem = New ListItem(i & " " & durationPostFix & "s", i)
            End If
            ddl.Items.Add(newListItem)
        Next
    End Sub

#End Region

#Region "Set fault Duration Label"

    Private Sub setDurationLabel(ByVal duration As Decimal, Optional ByVal durationString As String = Nothing, Optional ByVal durationPostFix As String = "Hour")
        If Not IsNothing(durationString) Then
            lblDuration.Text = durationString
        Else
            lblDuration.Text = duration & If(duration <= 1, " " & durationPostFix, " " & durationPostFix & "s")
        End If
    End Sub

#End Region

#End Region

End Class