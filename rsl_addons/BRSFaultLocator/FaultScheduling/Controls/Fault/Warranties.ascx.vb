﻿Imports FLS_BusinessLogic
Imports FLS_BusinessObject
Imports FLS_Utilities
Imports System.Collections.Generic
Imports System.Threading.Tasks
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Web.HttpContext
Imports System.IO
Imports System.Net.Mail
Imports System.Net

Public Class Warranties
    Inherits UserControlBase

    Dim objFaultbl As FaultBL = New FaultBL()
#Region "Control Events"

    Public Event CloseButtonClicked As EventHandler

#End Region

#Region "Page Events"

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateControl()
    End Sub

#End Region

#End Region

#Region "Control Events"

#Region "Button Events"

    Protected Sub btnAssignToContractor_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCloseWindow.Click
        RaiseEvent CloseButtonClicked(Me, New EventArgs)
    End Sub

#End Region


#End Region

#Region "Functions"

#Region "Populate User Control By Passing Values from Page"

    ''' <summary>
    ''' Supply Required PropertyId from page in which you need to refer this user control.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub PopulateControl()

        Dim dtWWarrantiesList As DataTable = getWWarrantiesList()

        If dtWWarrantiesList.Rows.Count = 0 Then
            For Each col As DataColumn In dtWWarrantiesList.Columns
                col.AllowDBNull = True
            Next
            Dim newRow As DataRow = dtWWarrantiesList.NewRow
        End If

        grdWarrantiesList.DataSource = dtWWarrantiesList
        grdWarrantiesList.DataBind()
    End Sub

#End Region

#End Region

#Region "View State Functions"

#Region "Work Required Data Table"

    ''' <summary>
    ''' Get the Work Required Data Table from the view state if a Data Table is not there
    ''' Create a new data table and add required column.
    ''' This is to be used to get data table of this type for the first time.
    ''' </summary>
    ''' <returns>A data table containing the required columns of work required grid.</returns>
    ''' <remarks></remarks>
    Private Function getWWarrantiesList() As DataTable
        Dim warrantiesDt As DataTable
        'Dim propertyId As String = "P10040000A"
        Dim propertyId As String = SessionManager.getPropertyId()
        warrantiesDt = objFaultbl.GetWarratiesList(propertyId).Tables(0)

        If IsNothing(warrantiesDt) Then
            warrantiesDt = New DataTable

            '2- Add Net Cost Column with data type Decimal
            If Not warrantiesDt.Columns.Contains(ApplicationConstants.WarrantyTypeCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = warrantiesDt.Columns.Add(ApplicationConstants.WarrantyTypeCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '3- Add Vat type Column with data type Integer
            If Not warrantiesDt.Columns.Contains(ApplicationConstants.CategoryCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = warrantiesDt.Columns.Add(ApplicationConstants.CategoryCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            '4- Add Vat Column with data type Decimal
            If Not warrantiesDt.Columns.Contains(ApplicationConstants.AreaCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = warrantiesDt.Columns.Add(ApplicationConstants.AreaCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '5- Add Gross/Total Column with data type Decimal
            If Not warrantiesDt.Columns.Contains(ApplicationConstants.ItemCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = warrantiesDt.Columns.Add(ApplicationConstants.ItemCol, GetType(Decimal))
                insertedColumn.AllowDBNull = False
            End If

            '6- Add PI Status Column with data type Integer
            If Not warrantiesDt.Columns.Contains(ApplicationConstants.ContractorCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = warrantiesDt.Columns.Add(ApplicationConstants.ContractorCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

            '7- Add Expenditure ID Col with data type Integer
            If Not warrantiesDt.Columns.Contains(ApplicationConstants.ExpiryCol) Then
                Dim insertedColumn As DataColumn
                insertedColumn = warrantiesDt.Columns.Add(ApplicationConstants.ExpiryCol, GetType(Integer))
                insertedColumn.AllowDBNull = False
            End If

        End If

        Return warrantiesDt
    End Function

#End Region

#Region "PropertyId"

    Private Function getPropertyIdViewState() As String
        Return TryCast(ViewState(ViewStateConstants.PropertyId), String)
    End Function

    Private Sub setPropertyIdViewState(ByVal propertyId As String)
        ViewState(ViewStateConstants.PropertyId) = propertyId
    End Sub

    Private Sub removePropertyIdViewState()
        ViewState.Remove(ViewStateConstants.PropertyId)
    End Sub

#End Region




#End Region


End Class