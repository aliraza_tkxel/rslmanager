﻿Public Class FaultNotes
    Inherits System.Web.UI.UserControl
#Region "LocalVariables"

    Dim faultBL As FLS_BusinessLogic.FaultBL = New FLS_BusinessLogic.FaultBL()

#End Region

#Region "Control Events"

    Public Event CloseButtonClicked As EventHandler

#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Sub PopulateControl(ByVal location As String, ByVal notes As String, ByVal tempFaultID As Integer)

        lblLocation.Text = location
        txtFaultNotes.Text = notes
        hfTempFaultID.Value = tempFaultID

    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClosePopup.Click

        lblMessage.Text = ""
        Response.Redirect(Request.RawUrl) 'Refresh page
        RaiseEvent CloseButtonClicked(Me, New EventArgs)


    End Sub

    Protected Sub btnSaveChanges_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveChanges.Click
        Dim tempFaultID As Integer = Convert.ToInt32(hfTempFaultID.Value)
        Dim faultNotes As String = txtFaultNotes.Text.ToString()
        Try
            faultBL.updateFaultNotes(tempFaultID, faultNotes)
            lblMessage.Text = "Fault Notes Updated Successfully !"
            lblMessage.ForeColor = Drawing.Color.Green

        Catch ex As Exception

        End Try


    End Sub
End Class