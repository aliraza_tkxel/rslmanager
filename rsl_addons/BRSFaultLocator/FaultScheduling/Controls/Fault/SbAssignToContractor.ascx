﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SbAssignToContractor.ascx.vb"
    Inherits="FaultScheduling.SbAssignToContractor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--Add appropriate style sheet in web form or master page.--%>
<style type="text/css" media="all">
    .assignToContractor
    {
        background-color: White;
    }
    
           .modalBack
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
            border: 1px solid black;
        }
    .assignToContractor .popupHeader
    {
        font-weight: bold;
        padding-left: 20px;
        padding-top: 10px;
    }
    .assignToContractor .worksRequiredGrid
    {
        padding-left: 20px;
        padding-right: 20px;
        max-height: 200px;
        max-width: 800px;
        overflow: auto;
    }
    .assignToContractor .input_row
    {
        margin-bottom: 10px;
        padding-left: 20px;
    }
    .assignToContractor .input_label
    {
        width: 150px;
        float: left;
    }
    .assignToContractor .input
    {
        width: auto;
    }
    .assignToContractor .input *
    {
        width: 250px;
    }
    .assignToContractor .input_small
    {
        width: auto;
    }
    .assignToContractor .input_small *
    {
        width: 140px;
    }
    .assignToContractor .input_small .addButton
    {
        width: 60px;
        margin-left: 40px;
    }
    
    .assignToContractor .bottomButtonsContainer
    {
        width: 100%;
        height: 100%;
    }
    
    .assignToContractor .bottomButtonsContainer .buttonFloatLeft
    {
        float: left;
        text-align: left;
        padding-left: 20px;
    }
    
    .assignToContractor .bottomButtonsContainer .buttonFloatRight
    {
        float: right;
        text-align: right;
        padding-right: 20px;
    }
    
    .assignToContractor .worksRequired
    {
        word-wrap: break-word;
    }
    .assignToContractor .WorksRequiredCount
    {
        display: none;
    }
    
    .clear
    {
        clear: both;
    }
    .WatermarkColor
    {
        color:Gray;
        font-style:italic;   
    }
</style>
<asp:UpdatePanel ID="updpnlAssignToContractor" runat="server" UpdateMode="Always"   >
    <ContentTemplate>
        <div class="assignToContractor" >
            <div class="popupHeader">
                <strong>Assign to Contractor</strong>
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <hr />
            <br />
            <div class="input_row">
                <div class="input_label">
                    Cost Centre:</div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlCostCentre" AutoPostBack="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvCostCentre" ControlToValidate="ddlCostCentre"
                        InitialValue="-1" ErrorMessage="<br/>Please select a Cost Centre" Display="Dynamic"
                        ValidationGroup="addWorkRequired" ForeColor="Red" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Budget Head:</div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlBudgetHead" AutoPostBack="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvBudgetHead" ControlToValidate="ddlBudgetHead"
                        InitialValue="-1" ErrorMessage="<br/>Please select a Budget Head" Display="Dynamic"
                        ValidationGroup="addWorkRequired" ForeColor="Red" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Expenditure:</div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlExpenditure" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvExpenditure" ControlToValidate="ddlExpenditure"
                        InitialValue="-1" ErrorMessage="<br/>Please select an Expenditure" Display="Dynamic"
                        ValidationGroup="addWorkRequired" ForeColor="Red" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Contractor:</div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlContractor" AutoPostBack="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvContractor" InitialValue="-1" ErrorMessage="<br/>Please select a Contractor"
                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlContractor"
                        ForeColor="Red" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Contact:</div>
                <div class="input">
                    <asp:DropDownList runat="server" AutoPostBack="true"   ID="ddlContact">
                        <asp:ListItem Selected="True" Text="-- Please select contractor --" Value="-1" />
                    </asp:DropDownList>
            <%--        <asp:RequiredFieldValidator runat="server" ID="rfvContact" InitialValue="-1" ErrorMessage="<br/>Please select a Contact"
                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlContact"
                        ForeColor="Red" ToolTip="If no contact is found, you may add a new contact (name/email) for this contractor in supplier module " />--%>
                </div>
            </div>
            <hr />
            <br />
            <div class="input_row">
                <div class="input_label">
                    <b>Scheme:</b> </div>
                <div class="input">
                    <asp:Label Text="" runat="server" ID="lblScheme" Font-Bold="true"   />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                   <b> Block:</b> </div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlBlock">
                    <asp:ListItem Selected="True" Text="Please Select" Value="0"></asp:ListItem>   
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" InitialValue="-1" ErrorMessage="<br/>Please select an Block"
                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlBlock"
                        ForeColor="Red" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    <b>Area:</b> </div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlArea" AutoPostBack="true"  >
                    </asp:DropDownList>
                <%--    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" InitialValue="-1" ErrorMessage="<br/>Please select an Area"
                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlArea"
                        ForeColor="Red" />--%>
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                   <b> Attribute:</b> </div>
                <div class="input">
                    <asp:DropDownList runat="server" ID="ddlAttribute" AutoPostBack="true" >
                    </asp:DropDownList>
                 <%--   <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" InitialValue="-1" ErrorMessage="<br/> Please select an Attribute"
                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlAttribute"
                        ForeColor="Red" />--%>
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Replacement Due:</div>
                <div class="input">
                    <asp:Label Text="" ID="lblReplacementDate" runat="server" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    <b>Works Required:</b><asp:Label Text="" runat="server" ID="lblWorksRequiredCount" CssClass="WorksRequiredCount" /></div>
                <div class="input">
                    <asp:TextBox runat="server" ID="txtWorksRequired" TextMode="MultiLine" Rows="5" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                        TargetControlID="txtWorksRequired" WatermarkText="Enter details here" WatermarkCssClass="searchTextDefault">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator runat="server" ID="rfvWorksRequired" ErrorMessage="<br/>Please enter works required."
                        Display="Dynamic" ValidationGroup="addWorkRequired" ControlToValidate="txtWorksRequired"
                        ForeColor="Red" Style="vertical-align: top;" />
                    <asp:CustomValidator ID="cvWorksRequired" runat="server" ControlToValidate="txtWorksRequired"
                        ErrorMessage="<br/>Works Required must not exceed 4000 characters." Display="Dynamic"
                        ForeColor="Red" ValidationGroup="addWorkRequired" EnableClientScript="False" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Estimate (£):</div>
                <div class="input">
                    <asp:TextBox runat="server" ID="txtEstimate" MaxLength="11" TextMode="SingleLine" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                        TargetControlID="txtEstimate" WatermarkText="Required"  WatermarkCssClass="WatermarkColor">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ErrorMessage="<br/>Please enter estimate, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        ControlToValidate="txtEstimate" ID="rfvEstimate" ValidationGroup="assignToContractor"
                        runat="server" ForeColor="Red" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="rxvEstimate" runat="server" ErrorMessage="<br/>Please enter estimate, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        Display="Dynamic" ValidationGroup="assignToContractor" ForeColor="Red" ControlToValidate="txtEstimate"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                    <asp:RangeValidator ID="rvEstimate" ErrorMessage='<br/>Please enter a value between "0" and "214748.3647".'
                        ControlToValidate="txtEstimate" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                        Display="Dynamic" ForeColor="Red" ValidationGroup="assignToContractor" Type="Double" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Estimate Ref:</div>
                <div class="input">
                    <asp:TextBox runat="server" ID="txtEstimateRef" MaxLength="200" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                        TargetControlID="txtEstimateRef" WatermarkText="Optional" WatermarkCssClass="WatermarkColor">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <asp:RegularExpressionValidator runat="server" ID="rxvEstimateRef" ErrorMessage="<br/>Estimate Ref must not exceed 200 characters"
                        Display="Dynamic" ForeColor="Red" ControlToValidate="txtEstimateRef" ValidationGroup="assignToContractor"
                        ValidationExpression="^[\s\S]{0,200}$" Style="vertical-align: top;" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Net Cost (£):</div>
                <div class="input_small">
                    <asp:TextBox runat="server" ID="txtNetCost" AutoPostBack="True" CausesValidation="true"
                        ValidationGroup="addWorkRequired" MaxLength="11"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="<br/>Please enter net cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        ControlToValidate="txtNetCost" ID="rfvNetCost" ValidationGroup="addWorkRequired"
                        runat="server" ForeColor="Red" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="rxvNetCost" runat="server" ErrorMessage="<br/>Please enter net Cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        Display="Dynamic" ValidationGroup="addWorkRequired" ForeColor="Red" ControlToValidate="txtNetCost"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                    <asp:RangeValidator ID="rvNetCost" runat="server" ControlToValidate="txtNetCost"
                        MinimumValue="0" MaximumValue="178956.9704" ErrorMessage="<br/>Please enter a value between &quot;0&quot; and &quot;178956.9704&quot;."
                        ForeColor="Red" Type="Double" Display="Dynamic" ValidationGroup="addWorkRequired" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Vat:</div>
                <div class="input_small">
                    <asp:DropDownList runat="server" ID="ddlVat" AutoPostBack="True" CausesValidation="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvddlVat" InitialValue="-1" ErrorMessage="Please select a Vat(Vat Rate)"
                        Display="Dynamic" ValidationGroup="addWorkRequired" ControlToValidate="ddlVat"
                        ForeColor="Red" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Vat (£):</div>
                <div class="input_small">
                    <asp:TextBox runat="server" ID="txtVat" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    <strong>Total (£):</strong></div>
                <div class="input_small">
                    <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true"></asp:TextBox>
                    <asp:Button Text="Add" runat="server" ID="btnAdd" CssClass="addButton" ValidationGroup="addWorkRequired" />
                </div>
            </div>
            <hr />
            <br />
            <div class="worksRequiredGrid">
                <asp:GridView runat="server" ID="grdWorksDeatil" ShowFooter="True" GridLines="Horizontal"
                    AutoGenerateColumns="False" PageSize="100" ShowHeaderWhenEmpty="True" Width="100%"
                    BorderStyle="None">
                    <HeaderStyle Font-Bold="false" />
                    <FooterStyle BorderColor="Black" Font-Bold="true" HorizontalAlign="Right" Wrap="False" />
                    <HeaderStyle HorizontalAlign="Center" /> 
                    <RowStyle BorderColor="Gray" BorderWidth="0px" BorderStyle="None" />
                    <Columns>
                        <asp:BoundField HeaderText="Works Required:" FooterText="Total:" DataField="WorksRequired"
                            HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="worksRequired" />
                        <asp:BoundField HeaderText="Net:" FooterText="0.00" DataFormatString="{0:0.####}"
                            DataField="NetCost" FooterStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="100px" />
                        <asp:BoundField HeaderText="Vat:" FooterText="0.00" DataFormatString="{0:0.####}"
                            DataField="Vat" FooterStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="100px" />
                        <asp:BoundField HeaderText="Gross:" FooterText="0.00" DataFormatString="{0:0.####}"
                            DataField="Gross" FooterStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="100px" />
                    </Columns>
                </asp:GridView>
            </div>
            <hr />
            <br />
            <div class="bottomButtonsContainer">
                <div class="buttonFloatLeft">
                    <asp:Button Text="Close" runat="server" ID="btnClose" />
                </div>
                <div class="buttonFloatRight">
                    <asp:Button ID="btnAssignToContractor" runat="server" Style="text-align: right" Text="Assign to selected contractor"
                        ValidationGroup="assignToContractor" />
                </div>
                <div class="clear" />
            </div>
            <div class="clear" />
        </div>


         <div style="border:2 solid black">
    <asp:Panel ID="pnlContactAlert" runat="server" BackColor="White" Width="350px"
        Style="font-weight: normal; font-size: 13px; padding: 10px;">
        <b> Warning
        </b>
        </br>
        </br>
        <hr>
        <p>No contact details exist for this Supplier, and therefore they have not received an email notification that the Purchase Order has been approved.
				Please contact the supplier directly with the Purchase Order details. 
				To update the Supplier records with contact details please contact a member of the Finance Team. </p>
                  <asp:Button ID="btnOk" runat="server" Text="OK" />
         </asp:Panel>
         <asp:Button ID="btnHidde" runat="server" UseSubmitBehavior="false" Text="" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="mdlPopupContactAlert" runat="server" TargetControlID="btnHidde"
        PopupControlID="pnlContactAlert" CancelControlID="btnOk" Enabled="true" DropShadow="true" BackgroundCssClass="modalBack" >
    </ajaxToolkit:ModalPopupExtender>
</div>
    </ContentTemplate>
    <Triggers>
      <asp:AsyncPostbackTrigger ControlID="ddlAttribute" EventName="SelectedIndexChanged" />
   </Triggers>
</asp:UpdatePanel>
