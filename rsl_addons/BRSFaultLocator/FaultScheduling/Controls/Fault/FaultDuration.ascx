﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FaultDuration.ascx.vb"
    Inherits="FaultScheduling.FaultDuration" %>
<asp:Panel runat="server" ID="pnlDuration" CssClass="temp-trade-content">
    <asp:ImageButton runat="server" ID="imgBtnEdit" ImageUrl="~/Images/edit.png" CssClass="temp-trade-content-left" />
    <asp:Label runat="server" ID="lblDuration" />
    <asp:DropDownList runat="server" ID="ddlDuration" Visible="false" />
    <asp:ImageButton runat="server" ID="imgBtnDone" ImageUrl="~/Images/done.png" Visible="false" />
</asp:Panel>
