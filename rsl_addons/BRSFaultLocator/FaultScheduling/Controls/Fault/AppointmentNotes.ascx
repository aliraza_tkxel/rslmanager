﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AppointmentNotes.ascx.vb"
    Inherits="FaultScheduling.AppointmentNotes" %>
<style type='text/css'>
    .apt-notes-popup
    {
        width: 446px;
        background-color: white;
        margin: 0 0 0 0px;
        border: 1px solid black;
        padding-bottom: 10px;
    }
    
    .popup-sub-container
    {
        padding: 5px 10px 5px 10px;
    }
    
    .right-align
    {
        float: right;
    }
    
    .left-align
    {
        float: left;
    }
    
    td
    {
        padding: 4px;
    }
    
    input[type="submit"]
    {
        background-color: White;
        border-radius: 5px;
        padding: 4px;
    }
</style>
<asp:UpdatePanel ID="updPanelAppointmentNotes" runat="server" >
    <ContentTemplate>
        <div class="apt-notes-popup">
            <div class="popup-sub-container">
                <asp:Image ID="Image1" runat="server" Width="20px" Height="20px" ImageUrl="~/Images/editCustomerNotes.png" />
                &nbsp;
                <asp:Label ID="Label1" runat="server" Text="Appointment Notes" Font-Bold="true"></asp:Label>
            </div>
            <hr />
            <div class="popup-sub-container">
                <asp:Panel ID="pnlPopupMessage" runat="server" Visible="false">
                    <asp:Label ID="lblPopupMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
            </div>
            <div class="popup-sub-container">
                <table style="width: 100%;">
                    <tr>
                        <td valign="top">
                            Notes:
                        </td>
                        <td>
                            <asp:TextBox ID="txtAppointmentNotes" TextMode="MultiLine" runat="server" ReadOnly="true"
                                Height="92px" Width="90%"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
