﻿Imports FLS_Utilities
Imports FLS_BusinessObject

Public Class SchemeBlockHeader
    Inherits UserControlBase

#Region "Page Events"

    ''' <summary>
    ''' This function handles the load event of the control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Get Scheme Block object from session
        'Check for null and populate the control
        'Show error where required
        'Set the start string        
        If Not IsPostBack Then
            If (Not (schemeBlockBo Is Nothing)) Then
                Me.setSchemeBlockLabels()
            Else
                'Show error
                lblStartText.Text = UserMessageConstants.SchemeBlockHeaderControlErrorMsg
                lblSchemeName.Visible = False
                lblBlockName.Visible = False

                lblStartText.ForeColor = System.Drawing.Color.Red
            End If
        End If

    End Sub

#End Region

#Region "Data Members"
    Private _StartString As String
#End Region

#Region "Property"

    ''' <summary>
    ''' This property gets and sets the start string of customer details
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StartString() As String
        Get
            Return _StartString
        End Get
        Set(ByVal value As String)
            _StartString = value
        End Set
    End Property

    ''' <summary>
    ''' This is to get the Client Street Address Label from this user control
    ''' </summary>
    ''' <value></value>
    ''' <returns>Type Label: Client Street Address</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property SchemeName As Label
        Get
            Return lblSchemeName
        End Get
    End Property

    ''' <summary>
    ''' This is to get the Client City Name Label from this user control
    ''' </summary>
    ''' <value></value>
    ''' <returns>Type Label: Client City Label</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property BlockName As Label
        Get
            Return lblBlockName
        End Get
    End Property
    Public ReadOnly Property ClientStreetAddressLabel As String
        Get
            Return commonAddressBo.Address
        End Get
    End Property

    Dim schemeBlockBo As SchemeBlockBO = New SchemeBlockBO()
    Dim commonAddressBo As CommonAddressBO = New CommonAddressBO()
#End Region

#Region "Function "

#Region "set Scheme Block Labels"
    Public Sub setSchemeBlockLabels()
        Me.getSessionValue()

        lblSchemeName.Text = Me.schemeBlockBo.SchemeName
        If Me.schemeBlockBo.BlockName = String.Empty Then
            lblBlockName.Text = "Non Selected"
        Else
            lblBlockName.Text = Me.schemeBlockBo.BlockName
        End If
        lblStartText.Text = StartString

    End Sub
#End Region

#Region "get Session Value"
    Public Sub getSessionValue()
        schemeBlockBo = SessionManager.getSbData()
        commonAddressBo = SessionManager.getCommonAddressBO()
    End Sub
#End Region

#End Region

End Class