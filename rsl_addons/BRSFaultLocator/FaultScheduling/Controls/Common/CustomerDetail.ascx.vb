﻿Imports FLS_BusinessLogic
Imports FLS_BusinessObject
Imports FLS_Utilities

Public Class CustomerDetail
    Inherits UserControlBase

#Region "Page Events"

    ''' <summary>
    ''' This function handles the load event of the control
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Get Customer object from session
        'Check for null and populate the control
        'Show error where required
        'Set the start string        

        If Not IsPostBack Then
            If (Not (custObj Is Nothing)) Then
                Me.setCustomerLabels()
            Else
                'Show error
                lblStartText.Text = UserMessageConstants.CustomControlErrorMsg
                lblClientTel.Visible = False
                lblComma.Visible = False

                lblStartText.ForeColor = System.Drawing.Color.Red
            End If
        End If

    End Sub

#End Region

#Region "Data Members"
    Private _StartString As String
#End Region

#Region "Property"

    ''' <summary>
    ''' This property gets and sets the start string of customer details
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property StartString() As String
        Get
            Return _StartString
        End Get
        Set(ByVal value As String)
            _StartString = value
        End Set
    End Property

    ''' <summary>
    ''' This is to get the Client Street Address Label from this user control
    ''' </summary>
    ''' <value></value>
    ''' <returns>Type Label: Client Street Address</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ClientStreetAddressLabel As Label
        Get
            Return lblClientStreetAddress
        End Get
    End Property

    ''' <summary>
    ''' This is to get the Client City Name Label from this user control
    ''' </summary>
    ''' <value></value>
    ''' <returns>Type Label: Client City Label</returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ClientClientCityLabel As Label
        Get
            Return lblClientCity
        End Get
    End Property

    Dim custObj As CustomerBO = New CustomerBO()
#End Region

#Region "Function "

#Region "set Customer Labels"
    Public Sub setCustomerLabels()
        Me.getSessionValue()

        lblClientName.Text = Me.custObj.Name
        lblClientStreetAddress.Text = custObj.Street
        lblClientCity.Text = custObj.City
        lblClientPostcode.Text = custObj.PostCode
        lblClientTelPhoneNumber.Text = custObj.Telephone
        lblStartText.Text = StartString

    End Sub
#End Region

#Region "get Session Value"
    Public Sub getSessionValue()
        custObj = SessionManager.getCustomerData()
    End Sub
#End Region

#End Region

End Class