﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CustomerDetail.ascx.vb"
    Inherits="FaultScheduling.CustomerDetail" %>
<div id="CustDetailFont">
    <asp:Label ID="lblStartText" runat="server" Text="" Font-Bold="true"></asp:Label>
    <asp:Label ID="lblClientName" runat="server" Text="" Font-Bold="true"></asp:Label>
    &nbsp;<asp:Label ID="lblClientStreetAddress" runat="server" Text="" Font-Bold="true"></asp:Label>
    <asp:Label ID="lblComma" runat="server" Text="," Font-Bold="true"> </asp:Label>
    <asp:Label ID="lblClientCity" runat="server" Text="" Font-Bold="true"></asp:Label>
    &nbsp;<asp:Label ID="lblClientPostcode" runat="server" Text="" Font-Bold="true"></asp:Label>
    &nbsp;<asp:Label ID="lblClientTel" runat="server" Text=" Tel :" Font-Bold="true"> 
    </asp:Label><asp:Label ID="lblClientTelPhoneNumber" runat="server" Text="" Font-Bold="true"></asp:Label>
</div>
