﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SchemeBlockHeader.ascx.vb" Inherits="FaultScheduling.SchemeBlockHeader" %>
<div id="CustDetailFont">
    <asp:Label ID="lblStartText" runat="server" Text="" Font-Bold="true"></asp:Label>
    <asp:Label ID="lblSchemeLabel" runat="server" Text="Scheme:" Font-Bold="true" ForeColor="LightGray"></asp:Label>
    &nbsp;<asp:Label ID="lblSchemeName" runat="server" Text="" Font-Bold="true"></asp:Label>    
    <asp:Label ID="lblBlockLabel" runat="server" Text="; Block:" Font-Bold="true" ForeColor="LightGray"></asp:Label>
    &nbsp;<asp:Label ID="lblBlockName" runat="server" Text="" Font-Bold="true"></asp:Label>    
</div>
