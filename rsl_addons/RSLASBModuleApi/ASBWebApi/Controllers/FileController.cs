﻿using ASB_BL;
using ASB_DAL;
using ASB_BO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ASBWebApi
{
    public class FileController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FileController));
        #region Add a file against a case
        [HttpPost]
        public HttpResponseMessage AddFile([FromBody]AddFileRequestBO objFileRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                FileResponseBO objFileResponseBO = new FileResponseBO();
                FileBL objFileBL = new FileBL(new FileDAL());
                objFileResponseBO = objFileBL.AddFile(objFileRequestBO);
                log.Debug("Exit:" + objFileResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objFileResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region get all files against a case
        [HttpPost]
        public HttpResponseMessage GetAllFiles([FromBody]GetAllFilesRequestBO objGetAllFilesRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                FileResponseBO objFileResponseBO = new FileResponseBO();
                FileBL objFileBL = new FileBL(new FileDAL());
                objFileResponseBO = objFileBL.SearchAllFiles(objGetAllFilesRequestBO);
                log.Debug("Exit:" + objFileResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objFileResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Remove selected file from a case
        [HttpPost]
        public HttpResponseMessage RemoveFile([FromBody]RemoveFileRequestBO objRemoveFileRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                FileResponseBO objFileResponseBO = new FileResponseBO();
                FileBL objFileBL = new FileBL(new FileDAL());
                objFileResponseBO = objFileBL.RemoveFile(objRemoveFileRequestBO);
                log.Debug("Exit:" + objFileResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objFileResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
        //Test method for checking response type
        [HttpGet]
        public HttpResponseMessage Test()
        {
            //RemoveFileRequestBO bo = new RemoveFileRequestBO();

            //bo.caseId = 2;
            //bo.fileType = "Document";
            //bo.fileId = 1;
            //bo.RemovedBy = 760;

            AddFileRequestBO bo = new AddFileRequestBO();
            bo.CaseId = 10;
            bo.DateUploaded = DateTime.Now;
            bo.FileName = "test";
            bo.FilePath = "c";
            bo.FileType = "Photograph";
            bo.IsActive = true;
            bo.UploadedBy = 113;

            bo.objPaginationBO = new PaginationBO();
            bo.objPaginationBO.PageNumber = 1;
            bo.objPaginationBO.PageSize = 10;


            var responseFormate = bo.ToString();
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "test");
        }

    }
}
