﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ASB_BO;
using ASB_BL;
using ASB_DAL;
namespace ASBWebApi.Controllers
{
    public class MenusController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(MenusController));
        MenusBL objMenusBL = new MenusBL(new MenusDAL());
        #region Displaying Menu on top bar
        [HttpPost]
        public HttpResponseMessage GetMenus([FromBody] MenuRequestBO objMenuRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                MenusResponseBO menuLst = new MenusResponseBO();
                menuLst = objMenusBL.GetMenus(objMenuRequestBO);
                log.Debug("Exit:" + menuLst);
                return Request.CreateResponse(HttpStatusCode.OK, menuLst);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
      

        [HttpGet]
        public HttpResponseMessage Test()
        {
            MenuRequestBO bo = new MenuRequestBO();
            bo.UserId = 113;
            bo.ModuleName = "Customer";
            var responseFormate = bo.ToString();//.ToString();
            return Request.CreateResponse(HttpStatusCode.OK, "test");


            
        }
    }
}
