﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using ASB_BL;
using ASB_BO;
using ASB_DAL;
using System.Web.Script.Serialization;

namespace ASBWebApi.Controllers
{
    public class ReportsController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ReportsController));

        #region Get dashboard info
        [HttpPost]
        public HttpResponseMessage GetDashboardData([FromBody]DashboardRequestBO objDashboardRequestBO)
        {

            log.Debug("Enter:");
            try
            {          
                DashboardResponseBO objDashboardResponseBO = new DashboardResponseBO();
                ReportsBL objDashboardBL = new ReportsBL(new ReportsDAL());
                objDashboardResponseBO = objDashboardBL.GetCaseData(objDashboardRequestBO.objPaginationBO, objDashboardRequestBO.objDashboardCaseFilterBO);//getting data on dashboard.including alerts and pagination info
                log.Debug("Exit:" + objDashboardResponseBO);
                HttpResponseMessage message = Request.CreateResponse(HttpStatusCode.OK, objDashboardResponseBO);         
                return Request.CreateResponse(HttpStatusCode.OK, objDashboardResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Get case management info
        [HttpPost]
        public HttpResponseMessage GetCaseManagementReportData([FromBody]CaseManagementReportRequestBO objCaseManagementReportRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                CaseManagementReportResponseBO objCaseManagementReportResponseBO = new CaseManagementReportResponseBO();
                ReportsBL objDashboardBL = new ReportsBL(new ReportsDAL());
                objCaseManagementReportResponseBO = objDashboardBL.GetCaseManagementReportData(objCaseManagementReportRequestBO);//get case management report info
                log.Debug("Exit:" + objCaseManagementReportResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objCaseManagementReportResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        //Test method for checking response type
        [HttpGet]
        public HttpResponseMessage Test()
        {
            CaseManagementReportRequestBO bo = new CaseManagementReportRequestBO();
            bo.CaseStatus = 1;
            bo.CategoryId = 0;
            bo.RiskLevelId = 0;
            bo.Search = "";
            bo.objPaginationBO = new PaginationBO();
            bo.objPaginationBO.PageNumber = 1;
            bo.objPaginationBO.PageSize = 10;
            //DashboardRequestBO bo = new DashboardRequestBO();
            //bo.objDashboardCaseFilterBO = new DashboardCaseFilterBO();
            //bo.objDashboardCaseFilterBO.caseOfficerId = 0;
            //bo.objDashboardCaseFilterBO.categoryId = 0;
            //bo.objDashboardCaseFilterBO.riskLevelId = 0;
            //bo.objDashboardCaseFilterBO.IsOverDueCases = false;
            //bo.objPaginationBO = new PaginationBO();
            //bo.objPaginationBO.PageNumber = 1;
            // bo.objPaginationBO.PageSize = 10;

            var responseFormate = bo.ToString();
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "test");
        }
    }
}
