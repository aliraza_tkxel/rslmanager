﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using ASB_BL;
using ASB_BO;
using ASB_DAL;
namespace ASBWebApi.Controllers
{
    public class CaseController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CaseController));
        CaseBL objNewCaseBL = new CaseBL(new CaseDAL());
        #region Add a new Case
        [HttpPost]
        public HttpResponseMessage AddCase([FromBody]AddCaseRequestBO objAddCaseRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                int caseId;              
                caseId = objNewCaseBL.AddCase(objAddCaseRequestBO);
                log.Debug("Exit:" + caseId);
                return Request.CreateResponse(HttpStatusCode.OK, caseId);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
        #region Update a case details
        [HttpPost]
        public HttpResponseMessage UpdateCaseDetails([FromBody]UpdateCaseDetailsRequestBO objAddCaseRequestBO)
        {
            log.Debug("Enter:");

            try
            {
                int caseId=2;
                caseId = objNewCaseBL.UpdateCaseDetails(objAddCaseRequestBO);
                //log.Debug("Exit:" + caseId);
                return Request.CreateResponse(HttpStatusCode.OK, caseId);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Associated Cases List       
        [HttpPost]
        public HttpResponseMessage GetAssociatedCasesList([FromBody]AssociatedCasesRequestBO objAssociatedCasesRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                AssociatedCasesResponseBO list = new AssociatedCasesResponseBO();
                list = objNewCaseBL.GetAssociatedCasesList(objAssociatedCasesRequestBO);
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Get persons and case details against a caseID
        [HttpGet]
        public HttpResponseMessage PopulateUpdateCaseData(int caseId)
        {
            log.Debug("Enter:");
            try
            {
                UpdateCaseResponseBO response = new UpdateCaseResponseBO();
                response = objNewCaseBL.PopulateDetailsForCaseUpdateByCaseId(caseId);
                log.Debug("Exit:" + response);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get persons and case details against a caseID for case amnedment.
        [HttpGet]
        public HttpResponseMessage PopulateUpdateAmendCaseData(int caseId)
        {
            log.Debug("Enter:");
            try
            {
                AmendCaseResponseBO response = new AmendCaseResponseBO();
                response = objNewCaseBL.PopulateDetailsForCaseAmendByCaseId(caseId);
                log.Debug("Exit:" + response);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Add Persons Against a Case
        [HttpPost]
        public HttpResponseMessage AddPersonsAgainstCase([FromBody]UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                bool isUpdated = false;
                isUpdated = objNewCaseBL.AddPersonsAgainstCase(objUpdatePersonAgainstCaseRequestBO);
                log.Debug("Exit:" + isUpdated);
                return Request.CreateResponse(HttpStatusCode.OK, isUpdated);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Remove Persons From a Case
        [HttpPost]
        public HttpResponseMessage RemovePersonsFromCase([FromBody]UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                bool isUpdated = false;
                isUpdated = objNewCaseBL.DeletePersonsFromCase(objUpdatePersonAgainstCaseRequestBO);
                log.Debug("Exit:" + isUpdated);
                return Request.CreateResponse(HttpStatusCode.OK, isUpdated);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Get Associated Cases List with joint tenancy      
        [HttpGet]
        public HttpResponseMessage GetJointTenancyCasesList(int customerId)
        {
            log.Debug("Enter:");
            try
            {
                AssociatedCasesResponseBO list = new AssociatedCasesResponseBO();
                list = objNewCaseBL.GetJointTenancyCasesList(customerId);
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion





        // Test method for checking response type
        [HttpGet]
        public HttpResponseMessage Test()
        {
                AddCaseRequestBO bo = new AddCaseRequestBO();
                bo.objCaseBO = new CaseBO();
                bo.objCaseBO.IncidentDate = DateTime.Now;

            //    List<PersonBO> objPersonList = new List<PersonBO>();
            //    PersonBO p1 = new PersonBO();

            //    p1.CaseId = 0;
            //    p1.PersonId = 113;
            //    p1.PersonRole = "Perpetrator";
            //    p1.PersonType = "Customer";

            //    PersonBO p2 = new PersonBO();

            //    p2.CaseId = 0;
            //    p2.PersonId = 760;
            //    p2.PersonRole = "Complainant";
            //    p2.PersonType = "Employee";


            //    bo.objPersonBO = new List<PersonBO>();
            //    objPersonList.Add(p1);
            //    objPersonList.Add(p2);

            //UpdatePersonAgainstCaseRequestBO bo = new UpdatePersonAgainstCaseRequestBO();
            //bo.objPerson = new PersonBO();
            //bo.objPerson.CaseId = 5;
            //bo.objPerson.PersonId = 10;
            //bo.objPerson.PersonRole = "Complainant";
            //bo.objPerson.PersonType = "Customer";

            //bo.RecordedBy = 113;

            //JointTenancyCasesRequestBO bo = new JointTenancyCasesRequestBO();
            //bo.CustomerId = 10025;
            //bo.objPaginationBO = new PaginationBO();
            //bo.objPaginationBO.PageSize = 25;
            //bo.objPaginationBO.PageNumber = 1;
            //bo.objPaginationBO.TotalPages = 0;
            //bo.objPaginationBO.TotalRows = 0;



        var responseFormate = bo.ToString();//.ToString();
            return Request.CreateResponse(HttpStatusCode.OK, "test");
        }





    }
}
