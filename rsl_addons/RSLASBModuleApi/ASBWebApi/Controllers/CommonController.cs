﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using ASB_BL;
using ASB_BO;
using ASB_DAL;

namespace ASBWebApi.Controllers
{
    public class CommonController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CommonController));

        #region Get risk level values 
        [HttpGet]
        public HttpResponseMessage GetRiskLevel()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new RiskLevelDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message + "\r" + ex.InnerException);
            }
        }
        #endregion

        #region Get category values
        [HttpGet]
        public HttpResponseMessage GetCategory()
        {
            log.Debug("Enter:");
            try
            {               
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new CategoryDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get case officer values
        [HttpGet]
        public HttpResponseMessage GetCaseOfficer()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new CaseOfficerDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region IsManager
        [HttpGet]
        public HttpResponseMessage IsManager(int employeeId)
        {
            log.Debug("Enter:");
            try
            {
                int ismanager = 1;
                List<DropDownBO> list = new List<DropDownBO>();
                var manager = new ManagerDAL() { employeeId = employeeId };
                DropDownsBL objDropDownsBL = new DropDownsBL(manager);
                list = objDropDownsBL.GetDropDownData();
                if(list==null || list.Count==0)
                {
                    ismanager = 0;
                }
                else if(list[0].key == 1)
                {
                    ismanager = 1;
                }
                else
                {
                    ismanager = 0;
                }
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, ismanager);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get incident type values
        [HttpGet]
        public HttpResponseMessage GetIncidentType()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new IncidentTypeDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Stages values
        [HttpGet]
        public HttpResponseMessage GetStages()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                var stagelokup = new StagesLookupDAL();
                DropDownsBL objDropDownsBL = new DropDownsBL(stagelokup);
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Closing Types
        [HttpGet]
        public HttpResponseMessage GetClosingTypes()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                var closingtypes = new ClosingTypesDAL();
                DropDownsBL objDropDownsBL = new DropDownsBL(closingtypes);
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get person type values
        [HttpGet]
        public HttpResponseMessage GetPersonType()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new PersonTypeDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get person title values
        [HttpGet]
        public HttpResponseMessage GetPersonTitles()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new PersonTitlesDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion



        #region Get Sub Categories values
        [HttpGet]
        public HttpResponseMessage GetSubCategories(int stageId)
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                var subcategories = new SubCategoryLookupDAL() { stageId = stageId };
                DropDownsBL objDropDownsBL = new DropDownsBL(subcategories);
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Types values
        [HttpGet]
        public HttpResponseMessage GetTypes(int subCategoryId)
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                var types = new TypesLookupDAL() { subCategoryId = subCategoryId };
                DropDownsBL objDropDownsBL = new DropDownsBL(types);
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get SubTypes values
        [HttpGet]
        public HttpResponseMessage GetSubTypes(int typeId)
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                var subtypes = new SubTypeLookupDAL() { typeId = typeId };
                DropDownsBL objDropDownsBL = new DropDownsBL(subtypes);
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        #region Get action types
        [HttpGet]
        public HttpResponseMessage GetActionType()
        {
            log.Debug("Enter:");
            try
            {
                List<DropDownBO> list = new List<DropDownBO>();
                DropDownsBL objDropDownsBL = new DropDownsBL(new ActionTypeDAL());
                list = objDropDownsBL.GetDropDownData();
                log.Debug("Exit:" + list);
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

    }
}
