﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using ASB_BL;
using ASB_BO;
using ASB_DAL;
using System.Web.Http.Cors;

namespace ASBWebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //[EnableCors(origins: "http://10.0.2.247:8085", headers: "*", methods: "*")]
    //[EnableCors(origins: "http://10.0.2.19:8085", headers: "*", methods: "*")]
    public class PersonController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PersonController));
        PersonBL objPersonBL = new PersonBL(new PersonDAL());

        #region Search a person
        [HttpPost]
        public HttpResponseMessage SearchPerson([FromBody] PersonSearchRequestBO objPersonSearchRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                List<PersonSearchResponseBO> personResponseList = new List<PersonSearchResponseBO>();
                personResponseList = objPersonBL.SearchPerson(objPersonSearchRequestBO);
                log.Debug("Exit:" + personResponseList);
                return Request.CreateResponse(HttpStatusCode.OK, personResponseList);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Search a Complainent/Preperator
        [HttpPost]
        public HttpResponseMessage SearchCompPrep([FromBody] PersonSearchRequestBO objPersonSearchRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                List<PersonSearchResponseBO> personResponseList = new List<PersonSearchResponseBO>();
                personResponseList = objPersonBL.SearchCompPrep(objPersonSearchRequestBO);
                log.Debug("Exit:" + personResponseList);
                return Request.CreateResponse(HttpStatusCode.OK, personResponseList);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Populate details of a person based on keyword
        [HttpPost]
        public HttpResponseMessage PopulatePersonDetails([FromBody]PersonSearchResponseBO objPersonSearchResponseBO)
        {
            log.Debug("Enter:");
            try
            {
                PersonDetailsBO objPersonBO = new PersonDetailsBO();
                objPersonBO = objPersonBL.PopulatePersonDetails(objPersonSearchResponseBO);
                log.Debug("Exit:" + objPersonBO);
                return Request.CreateResponse(HttpStatusCode.OK, objPersonBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Add a new person    
        [HttpPost]
        public HttpResponseMessage AddPerson([FromBody] PersonDetailsBO objPersonBO)
        {
            log.Debug("Enter:");
            try
            {
                int customerId ;
                customerId = objPersonBL.AddPersonDetails(objPersonBO);
                log.Debug("Exit:" + customerId);
                return Request.CreateResponse(HttpStatusCode.OK, customerId);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region update person details
        [HttpPost]
        public HttpResponseMessage UpdatePerson([FromBody] UpdatePersonRequestBO objUpdatePersonRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                PersonDetailsBO PersonDetailsBO = new PersonDetailsBO();
                PersonDetailsBO = objPersonBL.UpdatePerson(objUpdatePersonRequestBO);
                log.Debug("Exit:" + PersonDetailsBO);
                return Request.CreateResponse(HttpStatusCode.OK, PersonDetailsBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
        // Test method for checking response type
        [HttpGet]
        public HttpResponseMessage Test()
        {
            UpdatePersonRequestBO bo = new UpdatePersonRequestBO();
            bo.AlternateContact = "";
            bo.Email = "";
            bo.PersonId =760;
            bo.PersonType = "Employee";
            bo.TelephoneHome = "";
            bo.TelephoneWork = "";
              
            //PersonBO bo = new PersonBO();
            //bo.Dob = "";
            //bo.PersonId = 760;
            //bo.PersonName = "FName760";
            //bo.personType = "Employee";
            // PersonSearchRequestBO bo = new PersonSearchRequestBO();
            // bo.input = "76";
            // bo.filter = "Tanent"; //NonTanent, BHG
            //PersonDetailsBO bo = new PersonDetailsBO();
            //{
            //    bo.FirstName = "FName414";
            //    bo.LastName = "LName414";
            //    bo.TenancyRef = "N/A";
            //    bo.Organization = "Erinaceous Property Maintenance Ltd";
            //    bo.Address1 = "N/A";
            //    bo.Address2 = "N/A";
            //    bo.City = "N/A";
            //    bo.PostCode = "N/A";
            //    bo.TelephoneHome = "N/A";
            //    bo.TelephoneWork = "01603750507";
            //    bo.Mobile1 = "";
            //    bo.Mobile2 = "N/A ";
            //    bo.Email = "enterprisedev@broadlandgroup.org";
            //    bo.AlternateContact = "N/A";
            //    bo.TelephoneContact = "N/A";
            //    bo.PersonType = 0;
            //}

             var responseFormate = bo.ToString();
            return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "test");
        }


    }
}
