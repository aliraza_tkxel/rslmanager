﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ASB_BO;
using ASB_BL;
using ASB_DAL;
namespace ASBWebApi.Controllers
{
    public class SessionController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SessionController));
        #region Create user Session
        [HttpGet]
        public HttpResponseMessage CreateSession(int userId)
        {
            log.Debug("Enter:");
            try
            {
                SessionBO objSessionBO = new SessionBO();
                SessionBL objSessionBL = new SessionBL(new SessionDAL());
                objSessionBO = objSessionBL.CreateSession(userId);
                log.Debug("Exit:" + objSessionBO);
                return Request.CreateResponse(HttpStatusCode.OK, objSessionBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

    }
}
