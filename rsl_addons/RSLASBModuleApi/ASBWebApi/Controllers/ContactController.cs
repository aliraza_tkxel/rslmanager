﻿using ASB_BL;
using ASB_DAL;
using ASB_BO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ASBWebApi
{
    public class ContactController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ContactController));

        #region Add a Contact against a case
        [HttpPost]
        public HttpResponseMessage AddContact([FromBody]AddContactRequestBO objAddContactRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                ContactResponseBO objContactResponseBO = new ContactResponseBO();
                ContactBL objContactBL = new ContactBL(new ContactDAL());
                objContactResponseBO = objContactBL.AddContact(objAddContactRequestBO);
                log.Debug("Exit:" + objContactResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objContactResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region search all contact against a case
        [HttpPost]
        public HttpResponseMessage GetAllContact([FromBody]GetAllContactRequestBO objGetAllContactRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                ContactResponseBO objContactResponseBO = new ContactResponseBO();
                ContactBL objContactBL = new ContactBL(new ContactDAL());
                objContactResponseBO = objContactBL.SearchAllContacts(objGetAllContactRequestBO);
                log.Debug("Exit:" + objContactResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objContactResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Remove selected contact from a case
        [HttpPost]
        public HttpResponseMessage RemoveContact([FromBody]RemoveContactRequestBO objRemoveContactRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                ContactResponseBO objContactResponseBO = new ContactResponseBO();
                ContactBL objContactBL = new ContactBL(new ContactDAL());
                objContactResponseBO = objContactBL.RemoveContact(objRemoveContactRequestBO);
                log.Debug("Exit:" + objContactResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objContactResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


        //Test method for checking response type
        [HttpGet]
        public HttpResponseMessage Test()
        {
            //RemoveContactRequestBO bo = new RemoveContactRequestBO();

            //bo.CaseId = 2;
            //bo.ContactId = 1;

            //bo.objPaginationBO = new PaginationBO();
            //bo.objPaginationBO.PageNumber = 1;
            //bo.objPaginationBO.PageSize = 10;


            AddContactRequestBO bo = new AddContactRequestBO();

            bo.RecordedBy = 113;
            bo.objContactBO = new ContactBO();
            bo.objContactBO.CaseId = 97;
            bo.objContactBO.ContactName="AAA";
            bo.objContactBO.IsActive = true;
            bo.objContactBO.Organization = "bbbb";
            bo.objContactBO.Telephone = "6886886";

            bo.objPaginationBO = new PaginationBO();
            bo.objPaginationBO.PageNumber = 1;
            bo.objPaginationBO.PageSize = 10;
            var responseFormate = bo.ToString();
            return Request.CreateResponse(HttpStatusCode.OK, "test");
        }

    }
}
