﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using ASB_BL;
using ASB_BO;
using ASB_DAL;
namespace ASBWebApi.Controllers
{
    public class JournalController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(JournalController));
        JournalBL objJournalBL = new JournalBL(new JournalDAL());

        #region get journal data
        [HttpPost]
        public HttpResponseMessage GetJournalData([FromBody]JournalRequestBO objJournalRequestBO)
        {
            log.Debug("Enter:");
            try
            {
                JournalResponseBO objJournalResponseBO = new JournalResponseBO();
                objJournalResponseBO = objJournalBL.GetJournalDataByCaseId(objJournalRequestBO);
                log.Debug("Exit:" + objJournalResponseBO);
                return Request.CreateResponse(HttpStatusCode.OK, objJournalResponseBO);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Recording action agaisnt a case
        [HttpPost]
        public HttpResponseMessage AddActionInJournal([FromBody]ActionsBO objActionsBO)
        {
            log.Debug("Enter:");
            try
            {
                bool isAdded = false;
                isAdded = objJournalBL.AddInJournal(objActionsBO);
                log.Debug("Exit:" + isAdded);
                return Request.CreateResponse(HttpStatusCode.OK, isAdded);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message + ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

            //Test method for checking response type
        [HttpGet]
        public HttpResponseMessage Test()
        {
            ActionsBO bo = new ActionsBO();

            bo.Action = ApplicationConstants.Visit;
            bo.ActionNotes = "Scheduled visit";
            bo.CaseId = 44;
            bo.FileName = null;
            bo.FilePath = null;
            bo.RecordedBy = 100;
            bo.RecordedDate = DateTime.Now;
           // bo.FollowUpDate = DateTime.Now.AddDays(1).ToString();
           // bo.ClosedDate = DateTime.Now.AddDays(2).ToString();


            //JournalRequestBO bo = new JournalRequestBO();
            //bo.CaseId = 101;
            //bo.objPaginationBO = new PaginationBO();
            //bo.objPaginationBO.PageNumber = 1;
            //bo.objPaginationBO.PageSize = 5;
            //bo.objPaginationBO.TotalPages = 10;
            //bo.objPaginationBO.TotalRows = 10;


            var responseFormate = bo.ToString();
            return Request.CreateResponse(HttpStatusCode.OK, "test");
        }

    }
}
