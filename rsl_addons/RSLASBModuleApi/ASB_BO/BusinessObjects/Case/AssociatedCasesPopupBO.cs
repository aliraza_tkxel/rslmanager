﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
  public class AssociatedCasesPopupBO
    {
        public int Ref { get; set; }//caseId
        public string PersonRole { get; set; }//comp/prep
        public string PersonType { get; set; }//Cust/Emp
        public string IncidentDate { get; set; }
        public string Category { get; set; }
        public string IncidentType { get; set; }
        public string Status { get; set; }
    }
}
