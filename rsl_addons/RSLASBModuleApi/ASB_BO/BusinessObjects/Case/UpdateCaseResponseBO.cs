﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class UpdateCaseResponseBO
    {
        public UpdateCaseBO objUpdateCaseBO { get; set; }
        public List<PersonBO> objPersonList { get; set; }
    }
}
