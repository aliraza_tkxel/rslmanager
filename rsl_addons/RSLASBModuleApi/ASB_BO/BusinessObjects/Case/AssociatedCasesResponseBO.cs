﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
  public class AssociatedCasesResponseBO
    {
        public List<AssociatedCasesPopupBO> objAssociatedCasesPopupBO { get; set; }
       public PaginationBO objPaginationBO { get; set; }

    }
}
