﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
   public class AssociatedCasesRequestBO
    {
        public int PersonId { get; set; }//customer or employee id
        public string PersonType { get; set; }//customer or employee
        public string PersonRole { get; set; }//comp or prep
        public PaginationBO objPaginationBO { get; set; }
        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }

}
