﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
    public class UpdateCaseBO
    {
        public int caseId { get; set; }
        public string DateRecorded { get; set; }
        public string DateReported { get; set; }
        public string IncidentDate { get; set; }
        public string IncidentTime { get; set; }
        public string CaseOfficer { get; set; }
        public string CaseOfficerTwo { get; set; }
        public string Category { get; set; }
        public string incidentType { get; set; }
        public string RiskLevel { get; set; }
        public string IncidentDescription { get; set; }
        public string PoliceNotified { get; set; }
        public int? CrimeCaseNumber { get; set; }
        public string NextFollowupDate { get; set; }
        public string FollowupDescription { get; set; }
        public string ClosedDate { get; set; }
        public string ClosedDescription { get; set; }
        public string CaseStatus { get; set; }
        public string ReviewDate { get; set; }
        public string Stage { get; set; }
        public int? StageId { get; set; }
    }
}
