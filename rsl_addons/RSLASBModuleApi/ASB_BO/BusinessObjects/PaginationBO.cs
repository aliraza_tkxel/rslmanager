﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class PaginationBO
    {
        public int PageSize { get; set; }   // total no.of record per page    
        public int PageNumber { get; set; }// current page no.
        public int TotalRows { get; set; }// total no.of records
        public int TotalPages { get; set; }// total no.of pages in record set
    }
}
