﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
   public class UpdatePersonRequestBO
    {
        public int PersonId { get; set; }
        public string PersonType { get; set; }
        public string TelephoneHome { get; set; }
        public string TelephoneWork { get; set; }
        public string Email { get; set; }
        public string AlternateContact { get; set; }
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
