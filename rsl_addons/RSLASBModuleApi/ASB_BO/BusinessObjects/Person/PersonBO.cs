﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
  public  class PersonBO
    {
        public string PersonType { get; set; }//customer or employee. if selected radio button is non tenant or tenant then customer else employee 
        public string PersonRole { get; set; }//comp or prep
        public int PersonId { get; set; }
        public int CaseId { get; set; }
        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
