﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
   public class PersonSearchResponseBO
    {
        public string PersonName { get; set; } //set person name
        public string Dob { get; set; }//set person DOB
        public string personType { get; set; } // set person type. eg customer or employee
        public int PersonId { get; set; }//set person Id eg.if peron
        public string Address1 { get; set; }

        //test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
