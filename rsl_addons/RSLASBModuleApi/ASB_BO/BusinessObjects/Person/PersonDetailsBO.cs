﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
    public class PersonDetailsBO
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string TenancyRef { get; set; }
        public string Organization { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string TelephoneHome { get; set; }
        public string TelephoneWork { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string Email { get; set; }
        public string AlternateContact { get; set; }
        public string TelephoneContact { get; set; }
        public int PersonType { get; set; }
        public int PersonId { get; set; }
        public string PersonTitle { get; set; }
        public int? TitleId { get; set; }
        //test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }

    }
}
