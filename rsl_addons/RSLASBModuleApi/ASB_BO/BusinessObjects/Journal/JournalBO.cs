﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
    public class JournalBO
    {
        public int JournalId { get; set; }
        public DateTime RecordedDate { get; set; }
        public string Action { get; set; }
        public string ActionNotes { get; set; }
        public string RecordedBy { get; set; }
        public int CaseId { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string stage { get; set; }
        public int? StageId { get; set; }
        public string SubCategory { get; set; }      
        public int? SubCategoryId { get; set; }
        public DateTime? ReviewDate { get; set; }

    }
}
