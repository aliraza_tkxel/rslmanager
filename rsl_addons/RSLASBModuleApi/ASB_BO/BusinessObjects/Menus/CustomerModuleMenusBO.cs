﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class CustomerModuleMenusBO
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
        public string Url { get; set; }
    }
}
