﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class MenusBO
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public List<SubMenusBO> objLstSubMenus { get; set; }
    }
}
