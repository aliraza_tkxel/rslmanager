﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class MenusResponseBO
    {
        public List<MenusBO> objMenusBO { get; set; }
        public List<CustomerModuleMenusBO> objLstCustomerModuleMenus { get; set; }
       
    }
}
