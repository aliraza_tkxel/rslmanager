﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
  public class SubMenusBO
    {
        public int MenuId { get; set; }
        public string SubMenuName { get; set; }
        public string Path { get; set; }

    }
}
