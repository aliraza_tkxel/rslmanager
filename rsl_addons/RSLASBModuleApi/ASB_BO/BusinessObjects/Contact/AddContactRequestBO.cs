﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
    public class AddContactRequestBO
    {
       public ContactBO objContactBO { get; set; }
       public PaginationBO objPaginationBO { get; set; }
       public int RecordedBy { get; set; }
        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
