﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
    public class ContactBO
    {     
        public int ContactId { get; set; } 
        public string ContactName { get; set; }
        public string Organization { get; set; }
        public string Telephone { get; set; }
        public bool IsActive { get; set; }
        public int CaseId { get; set; } 
    }
}
