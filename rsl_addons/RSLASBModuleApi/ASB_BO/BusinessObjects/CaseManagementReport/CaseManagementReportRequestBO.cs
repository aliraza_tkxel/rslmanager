﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
   public class CaseManagementReportRequestBO
    {
        public int CategoryId { get; set; }
        public int RiskLevelId { get; set; }
        public string Search { get; set; }
        public int CaseStatus { get; set; }
        public bool IsOverdue { get; set; }
        public int CaseOfficerId { get; set; }
        public DateTime? ReportedFrom { get; set; }
        public DateTime? ReportedTo { get; set; }
        public string sortOrder { get; set; }
        public string sortBy { get; set; }
        public PaginationBO objPaginationBO { get; set; }

        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
