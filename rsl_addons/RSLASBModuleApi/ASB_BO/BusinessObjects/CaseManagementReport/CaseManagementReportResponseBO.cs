﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class CaseManagementReportResponseBO
    {
        public List<CaseDetailsBO> objCaseBO { get; set; }
        public PaginationBO objPaginationBO { get; set; }

    }
}
