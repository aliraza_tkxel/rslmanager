﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
  public class CaseDetailsBO
    {
        public int CaseId { get; set; }
        public DateTime ReportedDate { get; set; }
        public string CaseOfficer { get; set; }
        public string IncidentType { get; set; }
        public string AsbCategory { get; set; }
        public string Risk { get; set; }
        public string CaseStatus { get; set; }
        public int AsbCategoryId { get; set; }
        public int? RiskId { get; set; }
        public int? CaseStatusId { get; set; }
        public DateTime? FollowUpDate { get; set; }
        public int CaseOfficerId { get; set; }
        
        public int? StageId { get; set; }
        public string Stage { get; set; }
        public DateTime? ClosedDate { get; set; }
        public List<PersonsInfoForCaseMgmtReportBO> objComplainantList { get; set; }
        public List<PersonsInfoForCaseMgmtReportBO> objPerpetratorList { get; set; }

    }
}
