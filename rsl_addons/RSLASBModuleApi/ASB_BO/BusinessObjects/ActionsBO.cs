﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
    public class ActionsBO
    {
        public DateTime RecordedDate { get; set; }
        public string Action { get; set; }
        public string ActionNotes { get; set; }
        public int RecordedBy { get; set; }
        public int CaseId { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public DateTime FollowUpDate { get; set; }
        public DateTime ClosedDate { get; set; }
        public int? Stage { get; set; }
        public int? SubCategory { get; set; }
        public int? Types { get; set; }
        public int? SubType { get; set; }
        public DateTime? ReviewDate { get; set; }
        public int? ClosingType { get; set; }
        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
