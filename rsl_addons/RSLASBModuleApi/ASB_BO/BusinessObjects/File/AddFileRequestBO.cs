﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
    public class AddFileRequestBO
    {
        //public FileBO objFileBO { get; set; }
        public string FilePath { get; set; }
        public int? UploadedBy { get; set; }
        public DateTime? DateUploaded { get; set; }
        public string FileType { get; set; }
        public bool IsActive { get; set; }
        public int CaseId { get; set; }
        public string FileName { get; set; }
        public string FileNotes { get; set; }
        public PaginationBO objPaginationBO { get; set; }
        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
