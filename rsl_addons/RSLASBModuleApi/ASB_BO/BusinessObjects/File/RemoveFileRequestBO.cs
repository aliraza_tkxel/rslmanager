﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ASB_BO
{
   public class RemoveFileRequestBO
    {
        public int caseId { get; set; }
        public int fileId { get; set; }
        public string fileType { get; set; }
        public int RemovedBy { get; set; }
        public string ActionNotes { get; set; }
        public PaginationBO objPaginationBO { get; set; }
        // test method for checking response type
        public override string ToString()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(this);
        }
    }
}
