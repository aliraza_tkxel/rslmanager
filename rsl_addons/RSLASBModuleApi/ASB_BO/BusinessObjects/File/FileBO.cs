﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
    public class FileBO
    {
        public int fileId { get; set; }
        public string FilePath { get; set; }
        public string UploadedBy { get; set; }
        public string DateUploaded { get; set; }
        public string FileType { get; set; }
        public bool IsActive { get; set; }
        public int CaseId { get; set; } 
        public string FileName { get; set; }
        public string FileNotes { get; set; }


    }
}
