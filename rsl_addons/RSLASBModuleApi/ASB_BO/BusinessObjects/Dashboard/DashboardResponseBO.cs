﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class DashboardResponseBO
    {
        public PaginationBO objPaginationBO { get; set; }
        public List<DashboardCaseInfoBO> objDashboardCaseInfoBO { get; set; }
        public DashboardAlertsBO objDashboardAlertsBO { get; set; }
    }
}
