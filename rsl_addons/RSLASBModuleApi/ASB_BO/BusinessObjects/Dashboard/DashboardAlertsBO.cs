﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class DashboardAlertsBO
    {
        public int openCaseCount { get; set; }
        public int highRiskCasesCount { get; set; }
        public int overdueCasesCount { get; set; }
    }
}
