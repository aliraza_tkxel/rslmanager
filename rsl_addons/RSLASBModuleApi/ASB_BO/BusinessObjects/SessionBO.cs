﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_BO
{
   public class SessionBO
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int? IsActive { get; set; }
        public string userType { get; set; }
    }
}
