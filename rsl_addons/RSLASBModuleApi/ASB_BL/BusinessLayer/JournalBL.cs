﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_BO;
namespace ASB_BL
{
    public class JournalBL
    {
        IJournalRepoDAL objJournalRepoDAL;
        public JournalBL(IJournalRepoDAL repoDAL)
        {
            objJournalRepoDAL = repoDAL;
        }

        #region Add In Journal      
        public bool AddInJournal(ActionsBO objActionsBO)
        {
          return  objJournalRepoDAL.AddInJournal(objActionsBO);
        }
        #endregion

        #region Get Journal Data By CaseId
        public JournalResponseBO GetJournalDataByCaseId(JournalRequestBO objJournalRequestBO)
        {
          return  objJournalRepoDAL.GetJournalDataByCaseId(objJournalRequestBO);
        }
        #endregion


    }

}
