﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;
using ASB_BO;
namespace ASB_BL
{
   public class ContactBL
    {
        IContactRepoDAL objContactRepo;
        public ContactBL(IContactRepoDAL ContactRepo)
        {
            this.objContactRepo = ContactRepo;
        }
        public ContactResponseBO AddContact(AddContactRequestBO objAddContactRequestBO)
        {
            return this.objContactRepo.AddContact(objAddContactRequestBO);
        }
        public ContactResponseBO SearchAllContacts(GetAllContactRequestBO objGetAllContactRequestBO)
        {
            return objContactRepo.SearchAllContact(objGetAllContactRequestBO);
        }
        public ContactResponseBO RemoveContact(RemoveContactRequestBO objRemoveContactRequestBO)
        {
            return objContactRepo.RemoveContact(objRemoveContactRequestBO);
        }
    }
}
