﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;
using ASB_BO;
namespace ASB_BL
{
    public class PersonBL
    {
        IPersonDAL objPersonDAL;
        public PersonBL(IPersonDAL personDAL)
        {
            this.objPersonDAL = personDAL;
        }
        public List<PersonSearchResponseBO> SearchPerson(PersonSearchRequestBO objPersonSearchRequestBO)
        {
            List<PersonSearchResponseBO> objPersonSearchResponseBO = this.objPersonDAL.SearchPerson(objPersonSearchRequestBO);
            return objPersonSearchResponseBO;
        }

        public List<PersonSearchResponseBO> SearchCompPrep(PersonSearchRequestBO objPersonSearchRequestBO)
        {
            List<PersonSearchResponseBO> objPersonSearchResponseBO = this.objPersonDAL.SearchCompPrep(objPersonSearchRequestBO);
            return objPersonSearchResponseBO;
        }

        public PersonDetailsBO PopulatePersonDetails(PersonSearchResponseBO objPersonSearchResponseBO)
        {
            PersonDetailsBO objPersonBO = this.objPersonDAL.PopulatePersonDetails(objPersonSearchResponseBO);
            return objPersonBO;
        }
        public int AddPersonDetails(PersonDetailsBO objPersonBO)
        {
            return this.objPersonDAL.AddPerson(objPersonBO);
        }
        public PersonDetailsBO UpdatePerson(UpdatePersonRequestBO objUpdatePersonRequestBO)
        {
            return this.objPersonDAL.UpdatePerson(objUpdatePersonRequestBO);
        }
     }
}
