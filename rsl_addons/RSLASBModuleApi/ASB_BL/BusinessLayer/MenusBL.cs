﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_BO;
namespace ASB_BL
{
   public class MenusBL
    {
        IMenusRepoDAL objIMenusRepoDAL;
        public MenusBL(IMenusRepoDAL repoDAL)
        {
            objIMenusRepoDAL = repoDAL;
        }
        public MenusResponseBO GetMenus(MenuRequestBO objMenuRequestBO)
        {
            return objIMenusRepoDAL.GetMenus(objMenuRequestBO);
        }
       
    }
}
