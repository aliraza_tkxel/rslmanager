﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;
using ASB_BO;
namespace ASB_BL
{
    public class FileBL
    {
        IFileRepoDAL objFileRepo;
        public FileBL(IFileRepoDAL FileRepo)
        {
            this.objFileRepo = FileRepo;
        }

        public FileResponseBO AddFile(AddFileRequestBO objFileRequestBO)
        {
            return this.objFileRepo.AddFile(objFileRequestBO);
        }
        public FileResponseBO SearchAllFiles(GetAllFilesRequestBO objGetAllFilesRequestBO)
        {
            return objFileRepo.SearchAllFiles(objGetAllFilesRequestBO);
        }
        public FileResponseBO RemoveFile(RemoveFileRequestBO objRemoveFileRequestBO)
        {
            return objFileRepo.RemoveFile(objRemoveFileRequestBO);
        }

    }
}
