﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_BO;
namespace ASB_BL
{
   public class SessionBL
    {
        ISessionDAL objISessionDAL;
        public SessionBL(ISessionDAL repoDAL)
        {
            objISessionDAL = repoDAL;
        }
        public SessionBO CreateSession(int userId)
        {
            return objISessionDAL.CreateSession(userId);
        }
    }
}
