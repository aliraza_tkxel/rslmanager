﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_BO;
namespace ASB_BL
{
    #region business layer for all drop downs
    public class DropDownsBL
    {
        ILookupRepoDAL objLookupRepoDAL;
        public DropDownsBL(ILookupRepoDAL lookUpDAL)
        {
            this.objLookupRepoDAL = lookUpDAL;
        }
        public List<DropDownBO> GetDropDownData()
        {
            List<DropDownBO> list = this.objLookupRepoDAL.GetData();
            return list;
        }
        
    }
    #endregion

}
