﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;
using ASB_BO;
namespace ASB_BL
{
    public class ReportsBL 
    {
        IReportsRepoDAL objReportsRepoDAL;
        public ReportsBL(IReportsRepoDAL ReportsRepoDAL)
        {
            this.objReportsRepoDAL = ReportsRepoDAL;
        }
        public DashboardResponseBO GetCaseData(PaginationBO objPaginationBO, DashboardCaseFilterBO objDashboardCaseFilterBO)
        {
            DashboardResponseBO objDashboardResponseBO = this.objReportsRepoDAL.GetDashboardData(objPaginationBO, objDashboardCaseFilterBO);
            return objDashboardResponseBO;
        }


        public CaseManagementReportResponseBO GetCaseManagementReportData(CaseManagementReportRequestBO objCaseManagementReportRequestBO)
        {
            CaseManagementReportResponseBO objCaseManagementReportResponseBO = this.objReportsRepoDAL.GetCaseManagementReportData(objCaseManagementReportRequestBO);
            return objCaseManagementReportResponseBO;
        }
    }
}
