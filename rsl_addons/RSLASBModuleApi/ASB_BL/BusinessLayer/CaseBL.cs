﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL;
using ASB_BO; 

namespace ASB_BL
{
    public class CaseBL
    {
        ICaseRepoDAL objICaseRepoDAL;
        public CaseBL(ICaseRepoDAL repoDAL)
        {
            objICaseRepoDAL = repoDAL;
        }
        public int AddCase(AddCaseRequestBO objAddCaseRequestBO)
        {
            return objICaseRepoDAL.AddCase(objAddCaseRequestBO);
        }
        public int UpdateCaseDetails(UpdateCaseDetailsRequestBO objAddCaseRequestBO)
        {
            return objICaseRepoDAL.UpdateCaseDetails(objAddCaseRequestBO);
        }
        public AssociatedCasesResponseBO GetAssociatedCasesList(AssociatedCasesRequestBO objAssociatedCasesRequestBO)
        {
            return objICaseRepoDAL.GetAssociatedCasesList(objAssociatedCasesRequestBO);
        }
        public UpdateCaseResponseBO PopulateDetailsForCaseUpdateByCaseId(int caseId)
        {
            return objICaseRepoDAL.PopulateDetailsForCaseUpdateByCaseId(caseId);
        }
        public AmendCaseResponseBO PopulateDetailsForCaseAmendByCaseId(int caseId)
        {
            return objICaseRepoDAL.PopulateDetailsForCaseAmendByCaseId(caseId);
        }
        public bool AddPersonsAgainstCase(UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO)
        {
            return objICaseRepoDAL.AddPersonsAgainstCase(objUpdatePersonAgainstCaseRequestBO);
        }

        public bool DeletePersonsFromCase(UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO)
        {
            return objICaseRepoDAL.DeletePersonsFromCase(objUpdatePersonAgainstCaseRequestBO);
        }
        public AssociatedCasesResponseBO GetJointTenancyCasesList(int customerId)
        {
            return objICaseRepoDAL.GetJointTenancyCasesList(customerId);
        }


        }
}
