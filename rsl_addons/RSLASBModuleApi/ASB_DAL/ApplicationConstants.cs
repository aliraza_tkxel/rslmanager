﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASB_DAL
{
    public class ApplicationConstants
    {
        public const string HousingTeamType = "Housing Team";
        public const string Neighbourhoodofficer = "Neighbourhood Officer";
        public const string HousingTeamLeader = "Housing Team Leader";
        public const string PersonRolePerpetrator = "Perpetrator";
        public const string PersonRoleComplainant = "Complainant";
        public const string PersonTypeCustomer = "Customer";
        public const string PersonTypeEmployee = "Employee";
        public const string RiskLevelHigh = "High";
        public const string CaseStatusOpen = "Open";
        public const string CaseStatusClosedResolved = "Closed Resolved";
        public const string CaseStatusClosedUnresolved = "Closed Unresolved";
        public const string CaseStatusClosedNoneASB = "Closed None ASB";
        public const string TenantFilter = "Tenant";
        public const string NonTenantFilter = "NonTenant";
        public const string BhgFilter = "BHG";
        public const string StakeholderFilter = "Stakeholder";
        public const string GeneralFilter = "General";
        public const string ProspectiveTenantFilter = "Prospective Tenant";
        public const string Closed = "Closed";
        public const string PropertyModule = "Property";

        public const int NO_PAGINATION = -1;
        public const int STAGE = 1;
        public const int SUBCATEGORY = 2;
        public const int TYPES = 3;
        public const int SUBTYPE = 4;
        public const int CaseStatusOpenId = 1;
        public const int CaseStatusClosedResolvedId = 2;
        public const int CaseStatusClosedUnresolvedId = 3;
        public const int CaseStatusClosenoneasbId = 4;

        public const string PersonTypeTenant = "Tenant";


        #region Journal Actions
        public const string CloseCase = "Close Case";
        public const string Conversation = "Conversation";
        public const string Email = "Email";
        public const string FollowUp = "Follow Up";
        public const string Letter = "Letter";
        public const string Meeting = "Meeting";
        public const string Telephone = "Telephone";
        public const string TXT = "TXT";
        public const string Visit = "Visit";
        public const string ComplainantAdded = "Complainant Added";
        public const string ComplainantDeleted = "Complainant Deleted";
        public const string PerpetratorAdded = "Perpetrator Added";
        public const string PerpetratorDeleted = "Perpetrator Deleted";
        public const string DocumentAdded = "Document Added";
        public const string DocumentDeleted = "Document Deleted";
        public const string PhotographAdded = "Photograph Added";
        public const string PhotographDeleted = "Photograph Deleted";
        public const string AssociatedContactAdded = "Associated Contact Added";
        public const string AssociatedContactDeleted = "Associated Contact Deleted";
        public const string Stages = "Stages";
        public const string StagesChanged = "Stage Changed";
        #endregion

        #region ReportSorting
        public static readonly IList<string> reportSort = new ReadOnlyCollection<string>
    (new List<string> {
         "Ref", "Reported", "Complainant(s)", "Address", "Postcode", "Local Authority", "Perpetrator(s)", "Stage", "Case Officer", "Type", "Risk", "Status", "Closed" });
        #endregion


        #region Person Constants
        public const int AddressTypeCurrent = 3;
        #endregion








    }
}
