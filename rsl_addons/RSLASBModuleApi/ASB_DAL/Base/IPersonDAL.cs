﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
namespace ASB_DAL
{
   public interface IPersonDAL
    {
        List<PersonSearchResponseBO> SearchPerson(PersonSearchRequestBO objPersonSearchRequestBO);
        List<PersonSearchResponseBO> SearchCompPrep(PersonSearchRequestBO objPersonSearchRequestBO);
        int AddPerson(PersonDetailsBO objPersonBO);
        PersonDetailsBO PopulatePersonDetails(PersonSearchResponseBO objPersonSearchResponseBO);
        PersonDetailsBO UpdatePerson(UpdatePersonRequestBO objUpdatePersonRequestBO);


    }
}
