﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
namespace ASB_DAL
{
   public interface ISessionDAL
    {
        SessionBO CreateSession(int userId);
    }
}
