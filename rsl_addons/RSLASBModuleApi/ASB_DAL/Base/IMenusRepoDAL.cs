﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
namespace ASB_DAL
{
   public interface IMenusRepoDAL
    {
       MenusResponseBO GetMenus(MenuRequestBO objMenuRequestBO);

    }
}
