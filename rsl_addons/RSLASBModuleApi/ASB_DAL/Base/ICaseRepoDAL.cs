﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
namespace ASB_DAL
{
    public interface ICaseRepoDAL 
    {
        UpdateCaseResponseBO PopulateDetailsForCaseUpdateByCaseId(int caseId);
        AmendCaseResponseBO PopulateDetailsForCaseAmendByCaseId(int caseId);
        List<PersonBO> GetPersonDetailsByCaseId(int caseId);
        int AddCase(AddCaseRequestBO objAddCaseRequestBO);
        int UpdateCaseDetails(UpdateCaseDetailsRequestBO objAddCaseRequestBO);
        bool DeletePersonsFromCase(UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO);
        bool AddPersonsAgainstCase(UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO);
       AssociatedCasesResponseBO GetAssociatedCasesList(AssociatedCasesRequestBO objAssociatedCasesRequestBO);
        AssociatedCasesResponseBO GetJointTenancyCasesList(int customerId);
    }
}
