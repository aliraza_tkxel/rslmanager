﻿using ASB_DAL.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;

namespace ASB_DAL
{
    public class IncidentTypeDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get ASB incident type 
        List<DropDownBO> ILookupRepoDAL.GetData()
        {
            var data = (from inc in context.ASB_IncidentType
                        select new DropDownBO
                        {
                            key = inc.IncidentTypeId,
                            value = inc.Description
                        }
                        );

            return data.ToList();
        }
        #endregion

    }
}
