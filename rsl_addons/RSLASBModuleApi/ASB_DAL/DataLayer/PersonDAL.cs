﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL.DatabaseEntities;

namespace ASB_DAL
{
    public class PersonDAL : ABaseDAL, IPersonDAL
    {

        #region Add a new person     
        public int AddPerson(PersonDetailsBO objPersonBO)
        {
          C__CUSTOMER objCustomer = new C__CUSTOMER();
          C_ADDRESS objAddress = new C_ADDRESS();
         
          objCustomer.FIRSTNAME = objPersonBO.FirstName;
          objCustomer.MIDDLENAME = objPersonBO.MiddleName;
          objCustomer.LASTNAME = objPersonBO.LastName;
          objCustomer.CUSTOMERTYPE = objPersonBO.PersonType;//type of person from drop down
          objCustomer.TENANCY = objPersonBO.TenancyRef;
          objCustomer.CUSTOMERTYPE = objPersonBO.PersonType;
          objCustomer.TENANCY = objPersonBO.TenancyRef;
          objCustomer.TITLE = objPersonBO.TitleId;
          context.C__CUSTOMER.Add(objCustomer);
          context.SaveChanges();
              
          objAddress.ADDRESS1 = objPersonBO.Address1;
          objAddress.ADDRESS2 = objPersonBO.Address2;
          objAddress.CUSTOMERID = objCustomer.CUSTOMERID;//inserting customerId.As PersonBO don't have this field.so inserting it directly
          objAddress.TOWNCITY = objPersonBO.City;
          objAddress.POSTCODE = objPersonBO.PostCode;
          objAddress.TEL = objPersonBO.TelephoneHome;
          objAddress.TELWORK = objPersonBO.TelephoneWork;
          objAddress.MOBILE = objPersonBO.Mobile1;
          objAddress.MOBILE2 = objPersonBO.Mobile2;
          objAddress.EMAIL = objPersonBO.Email;
          objAddress.CONTACTFIRSTNAME = objPersonBO.AlternateContact;
          objAddress.TELRELATIVE = objPersonBO.TelephoneContact;
          context.C_ADDRESS.Add(objAddress);
          context.SaveChanges();
          return objCustomer.CUSTOMERID;
        }
        #endregion

        #region Search a person based on name. eg search a comp or prep

        public List<PersonSearchResponseBO> SearchPerson(PersonSearchRequestBO objPersonSearchRequestBO)
        {
            List<PersonSearchResponseBO> objPersonBO = new List<PersonSearchResponseBO>();
            int resultingRowsCount = 300;
            if (objPersonSearchRequestBO.filter == ApplicationConstants.TenantFilter)
            {
                var tanentData = (from cust in context.C__CUSTOMER         
                                  join custType in context.C_CUSTOMERTYPE on cust.CUSTOMERTYPE equals custType.CUSTOMERTYPEID
                                  join cTen in context.C_CUSTOMERTENANCY.Where(x=>x.ENDDATE == null) on cust.CUSTOMERID equals cTen.CUSTOMERID
                                  join ten in context.C_TENANCY on cTen.TENANCYID equals ten.TENANCYID
                                  join prop in context.P__PROPERTY on ten.PROPERTYID equals prop.PROPERTYID
                                  where custType.DESCRIPTION == ApplicationConstants.TenantFilter && (cust.FIRSTNAME.ToString() + " " + cust.LASTNAME.ToString()).Contains(objPersonSearchRequestBO.input.Trim())
                                  select new PersonSearchResponseBO
                                  {
                                      PersonName = string.Concat(cust.FIRSTNAME, " ", cust.LASTNAME),
                                      Dob = cust.DOB.Value.ToString(),
                                      //Dob = string.Format("{0:d}", Convert.ToDateTime(cust.DOB.Value.ToString())),
                                      Address1 = string.Concat(prop.HOUSENUMBER == null ? String.Empty : prop.HOUSENUMBER, " ", prop.ADDRESS1 == null ? String.Empty : prop.ADDRESS1, " ", prop.ADDRESS2 == null ? String.Empty : prop.ADDRESS2),
                                      PersonId = cust.CUSTOMERID,
                                      personType = ApplicationConstants.PersonTypeCustomer
                                  }
                       ).Take(resultingRowsCount);

                objPersonBO = tanentData.ToList();
            }
            else if (objPersonSearchRequestBO.filter == ApplicationConstants.NonTenantFilter)
            {
                var nonTanentData = (from cust in context.C__CUSTOMER
                                     join custType in context.C_CUSTOMERTYPE on cust.CUSTOMERTYPE equals custType.CUSTOMERTYPEID
                                     join adrs in context.C_ADDRESS
                                    on cust.CUSTOMERID equals adrs.CUSTOMERID
                                     where (custType.DESCRIPTION == ApplicationConstants.GeneralFilter || custType.DESCRIPTION == ApplicationConstants.StakeholderFilter || custType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter)
                                     && (cust.FIRSTNAME.ToString() + " " + cust.LASTNAME.ToString()).Contains(objPersonSearchRequestBO.input.Trim())
                                     select new PersonSearchResponseBO
                                     {
                                         PersonName = string.Concat(cust.FIRSTNAME, " ", cust.LASTNAME),
                                         Dob = cust.DOB.Value.ToString(),
                                         Address1 = string.Concat(adrs.HOUSENUMBER == null ? String.Empty : adrs.HOUSENUMBER, " ", adrs.ADDRESS1 == null ? String.Empty : adrs.ADDRESS1, " ", adrs.ADDRESS2 == null ? String.Empty : adrs.ADDRESS2),
                                         PersonId = cust.CUSTOMERID,
                                         personType = ApplicationConstants.PersonTypeCustomer
                                     }
                       ).Take(resultingRowsCount);
                objPersonBO = nonTanentData.ToList();
            }
            else if (objPersonSearchRequestBO.filter == ApplicationConstants.BhgFilter)
            {
                var bhgData = (from emp in context.E__EMPLOYEE
                               join adrs in context.E_CONTACT on emp.EMPLOYEEID equals adrs.EMPLOYEEID
                                    into group1
                               from cnt in group1.DefaultIfEmpty()
                               where (emp.FIRSTNAME.ToString() + " " + emp.LASTNAME.ToString()).Contains(objPersonSearchRequestBO.input.Trim())
                               select new PersonSearchResponseBO
                               {
                                   PersonName = emp.FIRSTNAME + " " + emp.LASTNAME,
                                   Dob = emp.DOB.Value.ToString(),
                                   Address1 = "",//string.Concat(cnt.ADDRESS1 == null ? "N/A" : cnt.ADDRESS1, " ", cnt.ADDRESS2 == null ? "N/A" : cnt.ADDRESS2),
                                   PersonId = emp.EMPLOYEEID,
                                   personType = ApplicationConstants.PersonTypeEmployee
                               }
                             ).Take(resultingRowsCount);
                objPersonBO = bhgData.ToList();
            }


            foreach (var person in objPersonBO)
            {
                if(!string.IsNullOrEmpty(person.Dob))
                {
                    person.Dob = string.Format("{0:d}", Convert.ToDateTime(person.Dob));//removing time part from incident date
                }
            }

            return objPersonBO;
        }
        #endregion

        #region Search a Complainant/Preperator based on search string

        public List<PersonSearchResponseBO> SearchCompPrep(PersonSearchRequestBO objPersonSearchRequestBO)
        {
            List<PersonSearchResponseBO> objPersonBO = new List<PersonSearchResponseBO>();
            

            var CompPrepDataCustomer = (from cust in context.C__CUSTOMER
                                join custType in context.ASB_CaseCustomerBridge on cust.CUSTOMERID equals custType.PersonId
                                join adrs in context.C_ADDRESS
                                on cust.CUSTOMERID equals adrs.CUSTOMERID
                                where custType.PersonType == ApplicationConstants.PersonTypeCustomer && (cust.FIRSTNAME.ToString() + " " + cust.LASTNAME.ToString()).Contains(objPersonSearchRequestBO.input)
                                select new PersonSearchResponseBO
                                {
                                    PersonName = string.Concat(cust.FIRSTNAME, " ", cust.LASTNAME),
                                    Dob = cust.DOB.Value.ToString(),
                                    //Dob = string.Format("{0:d}", Convert.ToDateTime(cust.DOB.Value.ToString())),
                                    Address1 = string.Concat(adrs.HOUSENUMBER == null ? "N/A" : adrs.HOUSENUMBER, " ", adrs.ADDRESS1 == null ? "N/A" : adrs.ADDRESS1, " ", adrs.ADDRESS2 == null ? "N/A" : adrs.ADDRESS2),
                                    PersonId = cust.CUSTOMERID,
                                    personType = ApplicationConstants.PersonTypeCustomer
                                }
                    );

            var CompPrepDataEmployee = (from emp in context.E__EMPLOYEE
                                        join custType in context.ASB_CaseCustomerBridge on emp.EMPLOYEEID equals custType.PersonId
                                        join adrs in context.E_CONTACT on emp.EMPLOYEEID equals adrs.EMPLOYEEID
                                             into group1
                                        from cnt in group1.DefaultIfEmpty()
                                        where custType.PersonType == ApplicationConstants.PersonTypeEmployee && (emp.FIRSTNAME.ToString() + " " + emp.LASTNAME.ToString()).Contains(objPersonSearchRequestBO.input)
                                        select new PersonSearchResponseBO
                                        {
                                            PersonName = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                                            Dob = emp.DOB.Value.ToString(),
                                            Address1 = string.Concat(cnt.ADDRESS1 == null ? "N/A" : cnt.ADDRESS1, " ", cnt.ADDRESS2 == null ? "N/A" : cnt.ADDRESS2),
                                            PersonId = emp.EMPLOYEEID,
                                            personType = ApplicationConstants.PersonTypeEmployee
                                        }
                             );
                    

            objPersonBO = CompPrepDataCustomer.GroupBy(x=>x.PersonId).Select(y=>y.FirstOrDefault()).ToList();
            objPersonBO = objPersonBO.Concat(CompPrepDataEmployee.GroupBy(x => x.PersonId).Select(y => y.FirstOrDefault()).ToList()).ToList();
            objPersonBO = objPersonBO.OrderBy(x => x.PersonName).ToList();
            foreach (var person in objPersonBO)
            {
                if (!string.IsNullOrEmpty(person.Dob))
                {
                    person.Dob = string.Format("{0:d}", Convert.ToDateTime(person.Dob));//removing time part from incident date
                }
            }

            return objPersonBO;
        }
        #endregion

        #region populate details of person 

        public PersonDetailsBO PopulatePersonDetails(PersonSearchResponseBO objPersonSearchResponseBO)
        {
            PersonDetailsBO objPersonBO = new PersonDetailsBO();
            if (objPersonSearchResponseBO.personType == ApplicationConstants.PersonTypeCustomer)
            {
                var customerType = (from cust in context.C__CUSTOMER
                                    join cType in context.C_CUSTOMERTYPE on cust.CUSTOMERTYPE equals cType.CUSTOMERTYPEID
                                    where cust.CUSTOMERID == objPersonSearchResponseBO.PersonId
                                    select cType.DESCRIPTION
                                    ).FirstOrDefault();
                if(customerType == ApplicationConstants.TenantFilter)
                {
                    var tenantData = (from cust in context.C__CUSTOMER 
                                      join cA in context.C_ADDRESS.Where(x=>x.ADDRESSTYPE == ApplicationConstants.AddressTypeCurrent && x.ISDEFAULT ==1) on cust.CUSTOMERID equals cA.CUSTOMERID
                                      into group1
                                      from adrs in group1.DefaultIfEmpty()
                                      join titles in context.G_TITLE on cust.TITLE equals titles.TITLEID
                                      into group2
                                      from title in group2.DefaultIfEmpty()
                                      join c in context.C_CUSTOMERTENANCY.Where(x=>x.ENDDATE == null) on cust.CUSTOMERID equals c.CUSTOMERID
                                      into group3
                                      from cTen in group3.DefaultIfEmpty()
                                      join ten in context.C_TENANCY on cTen.TENANCYID equals ten.TENANCYID
                                      into group4
                                      from ten in group4.DefaultIfEmpty()
                                      join prop in context.P__PROPERTY on ten.PROPERTYID equals prop.PROPERTYID
                                      into group5
                                      from prop in group5.DefaultIfEmpty()
                                      where cust.CUSTOMERID == objPersonSearchResponseBO.PersonId
                                      select new PersonDetailsBO
                                      {
                                          PersonId = cust.CUSTOMERID,
                                          PersonTitle = title.DESCRIPTION,
                                          FirstName = cust.FIRSTNAME == null ? "N/A" : cust.FIRSTNAME,
                                          MiddleName = cust.MIDDLENAME == null ? String.Empty : cust.MIDDLENAME,
                                          LastName = cust.LASTNAME == null ? String.Empty : cust.LASTNAME,
                                          TenancyRef = ten.TENANCYID.ToString(),
                                          Organization = "N/A",//as customer does not belong to any org 
                                          Address1 = string.Concat(prop.HOUSENUMBER == null ? "N/A" : prop.HOUSENUMBER, " ", prop.ADDRESS1 == null ? "N/A" : prop.ADDRESS1),
                                          Address2 = prop.ADDRESS2 == null ? "N/A" : prop.ADDRESS2,
                                          City = prop.TOWNCITY == null ? "N/A" : prop.TOWNCITY,
                                          PostCode = prop.POSTCODE == null ? "N/A" : prop.POSTCODE,
                                          TelephoneHome = adrs.TEL == null ? "N/A" : adrs.TEL,
                                          TelephoneWork = adrs.TELWORK == null ? "N/A" : adrs.TELWORK,
                                          Mobile1 = adrs.MOBILE == null ? "N/A" : adrs.MOBILE,
                                          Mobile2 = adrs.MOBILE2 == null ? "N/A" : adrs.MOBILE2,
                                          Email = adrs.EMAIL == null ? "N/A" : adrs.EMAIL,
                                          AlternateContact = (string.IsNullOrEmpty(adrs.CONTACTFIRSTNAME) && string.IsNullOrEmpty(adrs.CONTACTSURNAME)) ? "N/A" : string.Concat(adrs.CONTACTFIRSTNAME, " ", adrs.CONTACTSURNAME),
                                          TelephoneContact = adrs.TELRELATIVE == null ? "N/A" : adrs.TELRELATIVE,
                                          PersonType = 1
                                      }

                                      );
                    objPersonBO = tenantData.FirstOrDefault();
                }
                else
                {
                    var nonTenantData = (from cust in context.C__CUSTOMER
                                        join adrs in context.C_ADDRESS on cust.CUSTOMERID equals adrs.CUSTOMERID

                                        join titles in context.G_TITLE on cust.TITLE equals titles.TITLEID
                                        into group1
                                        from title in group1.DefaultIfEmpty()

                                        where cust.CUSTOMERID == objPersonSearchResponseBO.PersonId
                                        select new PersonDetailsBO
                                        {
                                            PersonId = cust.CUSTOMERID,
                                            PersonTitle = title.DESCRIPTION,
                                            FirstName = cust.FIRSTNAME == null ? "N/A" : cust.FIRSTNAME,
                                            MiddleName = cust.MIDDLENAME == null ? String.Empty : cust.MIDDLENAME,
                                            LastName = cust.LASTNAME == null ? String.Empty : cust.LASTNAME,
                                            TenancyRef =  cust.TENANCY ,
                                            Organization = "N/A",//as customer does not belong to any org 
                                            Address1 = string.Concat(adrs.HOUSENUMBER == null ? String.Empty : adrs.HOUSENUMBER, " ", adrs.ADDRESS1 == null ? "N/A" : adrs.ADDRESS1),
                                            Address2 = adrs.ADDRESS2 == null ? String.Empty : adrs.ADDRESS2,
                                            City = adrs.TOWNCITY == null ? String.Empty : adrs.TOWNCITY,
                                            PostCode = adrs.POSTCODE == null ? "N/A" : adrs.POSTCODE,
                                            TelephoneHome = adrs.TEL == null ? "N/A" : adrs.TEL,
                                            TelephoneWork = adrs.TELWORK == null ? "N/A" : adrs.TELWORK,
                                            Mobile1 = adrs.MOBILE == null ? "N/A" : adrs.MOBILE,
                                            Mobile2 = adrs.MOBILE2 == null ? "N/A" : adrs.MOBILE2,
                                            Email = adrs.EMAIL == null ? "N/A" : adrs.EMAIL,
                                            AlternateContact = (string.IsNullOrEmpty(adrs.CONTACTFIRSTNAME) && string.IsNullOrEmpty(adrs.CONTACTSURNAME)) ? "N/A" : string.Concat(adrs.CONTACTFIRSTNAME, " ", adrs.CONTACTSURNAME),
                                            TelephoneContact = adrs.TELRELATIVE == null ? "N/A" : adrs.TELRELATIVE,
                                            PersonType = 1
                                        }
              );
                    objPersonBO = nonTenantData.FirstOrDefault();
                }
                
               
            }
            else if (objPersonSearchResponseBO.personType == ApplicationConstants.PersonTypeEmployee)
            {
                var employeeData = (from emp in context.E__EMPLOYEE join adrs in context.E_CONTACT on emp.EMPLOYEEID equals adrs.EMPLOYEEID into group1
                                    from cnt in group1.DefaultIfEmpty()
                                    join org in context.S_ORGANISATION on emp.ORGID equals org.ORGID
                                    into group2 from or in group2.DefaultIfEmpty()
                                    join titles in context.G_TITLE on emp.TITLE equals titles.TITLEID
                                    into group3
                                    from title in group3.DefaultIfEmpty()

                                    where emp.EMPLOYEEID == objPersonSearchResponseBO.PersonId
                                    select new PersonDetailsBO
                                    {
                                        PersonId = emp.EMPLOYEEID,
                                        
                                        FirstName = emp.FIRSTNAME == null ? "N/A" : emp.FIRSTNAME,
                                        PersonTitle = title.DESCRIPTION,
                                        MiddleName = emp.MIDDLENAME == null ? String.Empty : emp.MIDDLENAME,
                                        LastName = emp.LASTNAME == null ? String.Empty : emp.LASTNAME,
                                        TenancyRef = "N/A",//as employee is not a tanent,so it has no tenancy ref
                                        Organization = or.NAME == null ? "N/A" : or.NAME,
                                        Address1 = "N/A",//cnt.ADDRESS1 == null ? "N/A" : cnt.ADDRESS1,
                                        Address2 = "",//cnt.ADDRESS2 == null ? "N/A" : cnt.ADDRESS2,
                                        City = "",//cnt.POSTALTOWN == null ? "N/A" : cnt.POSTALTOWN,
                                        PostCode = "",//cnt.POSTCODE == null ? "N/A" : cnt.POSTCODE,
                                        TelephoneHome = "N/A",//cnt.HOMETEL == null ? "N/A" : cnt.HOMETEL,
                                        TelephoneWork = "N/A",//cnt.WORKDD == null ? "N/A" : cnt.WORKDD,
                                        Mobile1 = "N/A",//cnt.MOBILE == null ? "N/A" : cnt.MOBILE,
                                        Mobile2 =  "N/A" ,// cnt.WORKMOBILE == null ? "N/A" : cnt.WORKMOBILE,
                                        Email = "N/A",//cnt.WORKEMAIL == null ? "N/A" : cnt.WORKEMAIL,
                                        AlternateContact = "N/A",//cnt.EMERGENCYCONTACTNAME == null ? "N/A" : cnt.EMERGENCYCONTACTNAME,
                                        TelephoneContact = "N/A",//cnt.EMERGENCYCONTACTTEL == null ? "N/A" : cnt.EMERGENCYCONTACTTEL,
                                        PersonType = 2
                                    }
             );
                objPersonBO = employeeData.FirstOrDefault();
            }

            return objPersonBO;
        }
        #endregion

        #region Update a person details

        public PersonDetailsBO UpdatePerson(UpdatePersonRequestBO objUpdatePersonRequestBO)
        {
            string personType = objUpdatePersonRequestBO.PersonType;
            int personId = objUpdatePersonRequestBO.PersonId;
            if (string.Compare(personType, ApplicationConstants.PersonTypeCustomer, true) == 0)//if person is customer then update data in customer table
            {
                var data = (from cAddress in context.C_ADDRESS
                            where cAddress.CUSTOMERID == personId
                            select cAddress).FirstOrDefault();
                data.TEL = objUpdatePersonRequestBO.TelephoneHome;
                data.TELWORK = objUpdatePersonRequestBO.TelephoneWork;
                data.EMAIL = objUpdatePersonRequestBO.Email;
                data.CONTACTFIRSTNAME = objUpdatePersonRequestBO.AlternateContact;
                context.SaveChanges();
            }
            else if (string.Compare(personType, ApplicationConstants.PersonTypeEmployee, true) == 0)//if person is employee then update data in employee table
            {
                var data = (from eAddress in context.E_CONTACT
                            where eAddress.EMPLOYEEID == personId
                            select eAddress).FirstOrDefault();
                data.HOMETEL = objUpdatePersonRequestBO.TelephoneHome;
                data.WORKDD = objUpdatePersonRequestBO.TelephoneWork;
                data.WORKEMAIL = objUpdatePersonRequestBO.Email;
                data.EMERGENCYCONTACTNAME = objUpdatePersonRequestBO.AlternateContact;
                context.SaveChanges();
            }
            //start creating response
            PersonSearchResponseBO objPersonSearchResponseBO = new PersonSearchResponseBO();
            PersonDetailsBO objPersonDetailsBO = new PersonDetailsBO();
            objPersonSearchResponseBO.personType = personType;
            objPersonSearchResponseBO.PersonId = personId;
            objPersonDetailsBO = PopulatePersonDetails(objPersonSearchResponseBO);
            return objPersonDetailsBO;
            //end creating response
        }

        #endregion



    }
}
