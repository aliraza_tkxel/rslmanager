﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;

namespace ASB_DAL
{
    public class ActionTypeDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get Action type        
        public List<DropDownBO> GetData()
        {
            var data = (from actnType in context.ASB_CaseActionType
                        where actnType.Description !=ApplicationConstants.ComplainantAdded 
                        && actnType.Description != ApplicationConstants.ComplainantDeleted
                        && actnType.Description != ApplicationConstants.PerpetratorAdded
                        && actnType.Description != ApplicationConstants.PerpetratorDeleted
                        && actnType.Description != ApplicationConstants.DocumentAdded
                        && actnType.Description != ApplicationConstants.DocumentDeleted
                        && actnType.Description != ApplicationConstants.PhotographAdded
                        && actnType.Description != ApplicationConstants.PhotographDeleted
                        && actnType.Description != ApplicationConstants.AssociatedContactAdded
                        && actnType.Description != ApplicationConstants.AssociatedContactDeleted
                        select new DropDownBO
                        {
                            key = actnType.ActionTypeId,
                            value = actnType.Description
                        });
            return data.ToList();
        }
        #endregion
    }
}
