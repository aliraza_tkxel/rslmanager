﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;

namespace ASB_DAL
{
    public class ClosingTypesDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get ASB Closing types from Status Table
        public List<DropDownBO> GetData()
        {
            var data = (from closetype in context.ASB_CaseStatus
                        where closetype.StatusId != ApplicationConstants.CaseStatusOpenId
                        select new DropDownBO
                        {
                            key = closetype.StatusId,
                            value = closetype.Description
                        }
                         );

            return data.ToList();
        }
        #endregion
    }
}
