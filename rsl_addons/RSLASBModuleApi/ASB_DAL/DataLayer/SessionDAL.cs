﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL.DatabaseEntities;
using ASB_BO;
namespace ASB_DAL
{
  public  class SessionDAL : ABaseDAL, ISessionDAL
    {
        public SessionBO CreateSession(int userId)
        {
            var data = (from emp in context.FL_GetEmployeeById(userId)
                        select new SessionBO
                        {
                            EmployeeId = emp.EmployeeId,
                            IsActive = emp.IsActive,
                            Name = emp.FullName,
                            userType = emp.UserType
                        }).FirstOrDefault();
            return data;
        }
    }
}
