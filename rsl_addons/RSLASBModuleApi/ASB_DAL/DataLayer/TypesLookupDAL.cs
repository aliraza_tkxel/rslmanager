﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;

namespace ASB_DAL
{
    public class TypesLookupDAL : ABaseDAL, ILookupRepoDAL
    {
        public int subCategoryId { get; set; }
        #region Get ASB Stages from lookup
        public List<DropDownBO> GetData()
        {
            var data = (from stage in context.ASB_StagesLookup
                        where stage.StageTypeId == ApplicationConstants.TYPES && stage.StageParentId == subCategoryId
                        select new DropDownBO
                        {
                            key = stage.StagesLookupId,
                            value = stage.Headline
                        }
                         );

            return data.ToList();
        }
        #endregion
    }
}
