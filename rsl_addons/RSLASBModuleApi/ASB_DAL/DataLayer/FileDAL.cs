﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL.DatabaseEntities;
namespace ASB_DAL
{
    public class FileDAL : ABaseDAL, IFileRepoDAL
    {
        JournalDAL objJournalDAL = new JournalDAL();
        ActionsBO objActionsBO = new ActionsBO();

        #region Add a file against a case
        public FileResponseBO AddFile(AddFileRequestBO objAddFileRequestBO)
        {
            FileResponseBO objFileResponseBO = new FileResponseBO();
            PaginationBO objPaginationBO = new PaginationBO();
            ASB_File objFile = new ASB_File();
            objFile.FilePath = objAddFileRequestBO.FilePath;
            objFile.UploadedBy = objAddFileRequestBO.UploadedBy;
            objFile.UploadedDate = DateTime.Now;
            objFile.CaseId = objAddFileRequestBO.CaseId;
            objFile.FileType = objAddFileRequestBO.FileType;
            objFile.IsActive = objAddFileRequestBO.IsActive;
            objFile.FileName = objAddFileRequestBO.FileName;
            objFile.FileNotes = objAddFileRequestBO.FileNotes;
            context.ASB_File.Add(objFile);
            context.SaveChanges();
      
            //start Recording action for case journal  
            
            objActionsBO.CaseId = objFile.CaseId;
            objActionsBO.FileName = objFile.FileName;
            objActionsBO.FilePath = objFile.FilePath;
            objActionsBO.RecordedBy = objFile.UploadedBy.GetValueOrDefault();
            objActionsBO.RecordedDate = DateTime.Now;
            objActionsBO.ReviewDate = DateTime.Now ;
            objActionsBO.ReviewDate = DateTime.Now;
            if (string.Compare(objAddFileRequestBO.FileType, "Document", true) == 0)
            {
                objActionsBO.Action = ApplicationConstants.DocumentAdded;
                objActionsBO.ActionNotes = ApplicationConstants.DocumentAdded;
            }
            else
            {
                objActionsBO.Action = ApplicationConstants.PhotographAdded;
                objActionsBO.ActionNotes = ApplicationConstants.PhotographAdded;
            }
            objJournalDAL.AddInJournal(objActionsBO);
            //End Recording action for case journal          

            //start creating response
            var data = (from file in context.ASB_File
                        join emp in context.E__EMPLOYEE on file.UploadedBy equals emp.EMPLOYEEID
                        into group1
                        from e in group1.DefaultIfEmpty()
                        where file.IsActive == true
                        && file.CaseId == objAddFileRequestBO.CaseId
                        && file.FileType == objAddFileRequestBO.FileType
                        orderby file.FileId descending
                        select new FileBO
                        {
                            fileId = file.FileId,
                            FileName = file.FileName,
                            UploadedBy =string.Concat(e.FIRSTNAME," ",e.LASTNAME),
                            DateUploaded = file.UploadedDate.ToString(),
                            FilePath = file.FilePath,     
                            FileType = file.FileType ,
                            IsActive = file.IsActive,
                            CaseId = file.CaseId,
                            FileNotes=file.FileNotes                                                                                                                        
                        }
                        );


            //start of pagination
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objAddFileRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objAddFileRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objAddFileRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objAddFileRequestBO.objPaginationBO.PageNumber - 1) * objAddFileRequestBO.objPaginationBO.PageSize).Take(objAddFileRequestBO.objPaginationBO.PageSize);
            //End of pagination


            //start removing time part from Uploaded Date and adding in response
            objFileResponseBO.objFileBO = new List<FileBO>();
            foreach (var dt in data)
            {
                dt.DateUploaded = string.Format("{0:d}", Convert.ToDateTime(dt.DateUploaded));
                objFileResponseBO.objFileBO.Add(dt);
            }
            objFileResponseBO.objPaginationBO = objPaginationBO;
            //end removing time part from Uploaded Date and adding in response

            return objFileResponseBO;
        }
        #endregion

        #region Get all files against a caseId
        public FileResponseBO SearchAllFiles(GetAllFilesRequestBO objGetAllFilesRequestBO)
        {
            PaginationBO objPaginationBO = new PaginationBO();
            FileResponseBO objFileResponseBO = new FileResponseBO();
            int caseId = objGetAllFilesRequestBO.caseId;
            string fileType = objGetAllFilesRequestBO.fileType;
            var data = (from file in context.ASB_File
                        join emp in context.E__EMPLOYEE on file.UploadedBy equals emp.EMPLOYEEID
                        into group1
                        from e in group1.DefaultIfEmpty()
                        where file.IsActive == true
                        && file.CaseId == caseId 
                        && file.FileType == fileType
                        orderby file.FileId descending
                        select new FileBO
                        {
                            fileId = file.FileId,
                            FileName = file.FileName,
                            UploadedBy = string.Concat(e.FIRSTNAME, " ", e.LASTNAME),
                            DateUploaded = file.UploadedDate.ToString(),
                            FilePath = file.FilePath,
                            FileType = file.FileType,
                            IsActive = file.IsActive,
                            CaseId = file.CaseId,
                            FileNotes=file.FileNotes
                        }
                     );

            //start of pagination
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objGetAllFilesRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objGetAllFilesRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objGetAllFilesRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objGetAllFilesRequestBO.objPaginationBO.PageNumber - 1) * objGetAllFilesRequestBO.objPaginationBO.PageSize).Take(objGetAllFilesRequestBO.objPaginationBO.PageSize);
            //End of pagination

            //start removing time part from Uploaded Date and adding in response
            objFileResponseBO.objFileBO = new List<FileBO>();
            foreach (var dt in data)
            {
                dt.DateUploaded = string.Format("{0:d}", Convert.ToDateTime(dt.DateUploaded));
                objFileResponseBO.objFileBO.Add(dt);
            }
            objFileResponseBO.objPaginationBO = objPaginationBO;
            //end removing time part from Uploaded Date and adding in response
         
             return objFileResponseBO;
        }
        #endregion

        #region Remove a file against from a caseId
       
        public FileResponseBO RemoveFile(RemoveFileRequestBO objRemoveFileRequestBO)
        {
            int caseId = objRemoveFileRequestBO.caseId;
            int fileId = objRemoveFileRequestBO.fileId;
            string fileType = objRemoveFileRequestBO.fileType;
            int? removedBy = objRemoveFileRequestBO.RemovedBy;
            PaginationBO objPaginationBO = new PaginationBO();
            FileResponseBO objFileResponseBO = new FileResponseBO();
            ASB_File objFile = context.ASB_File.FirstOrDefault(f =>f.IsActive==true &&  f.CaseId.Equals(caseId) && f.FileId.Equals(fileId) && f.FileType.Equals(fileType));
            objFile.IsActive = false;
            context.SaveChanges();
       
            //start Recording action for case journal  
            objActionsBO.CaseId = objFile.CaseId;
            objActionsBO.FileName = objFile.FileName;
            objActionsBO.FilePath = objFile.FilePath;
            objActionsBO.RecordedBy = objFile.UploadedBy.GetValueOrDefault();
            objActionsBO.RecordedDate = DateTime.Now;
            objActionsBO.ReviewDate = DateTime.Now;
            if (string.Compare(objRemoveFileRequestBO.fileType, "Document", true) == 0)
            {
                objActionsBO.Action = ApplicationConstants.DocumentDeleted;
                objActionsBO.ActionNotes = ApplicationConstants.DocumentDeleted;
            }
            else
            {
                objActionsBO.Action = ApplicationConstants.PhotographDeleted;
                objActionsBO.ActionNotes = ApplicationConstants.PhotographDeleted;
            }
            objJournalDAL.AddInJournal(objActionsBO);
            //End Recording action for case journal          

            //start creating response
            var data = (from file in context.ASB_File
                        join emp in context.E__EMPLOYEE on file.UploadedBy equals emp.EMPLOYEEID
                        into group1
                        from e in group1.DefaultIfEmpty()
                        where file.IsActive == true
                        && file.CaseId == caseId
                        && file.FileType == fileType
                        orderby file.FileId descending
                        select new FileBO
                        {
                            fileId = file.FileId,
                            FileName = file.FileName,
                            UploadedBy = string.Concat(e.FIRSTNAME, " ", e.LASTNAME),
                            DateUploaded = file.UploadedDate.ToString(),
                            FilePath = file.FilePath,
                            FileType = file.FileType,
                            IsActive = file.IsActive,
                            CaseId = file.CaseId,
                            FileNotes=file.FileNotes
                        }
                     );

            //start of pagination
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objRemoveFileRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objRemoveFileRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objRemoveFileRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objRemoveFileRequestBO.objPaginationBO.PageNumber - 1) * objRemoveFileRequestBO.objPaginationBO.PageSize).Take(objRemoveFileRequestBO.objPaginationBO.PageSize);
            //End of pagination

            //start removing time part from Uploaded Date and adding in response
            objFileResponseBO.objFileBO = new List<FileBO>();
            foreach (var dt in data)
            {
                dt.DateUploaded = string.Format("{0:d}", Convert.ToDateTime(dt.DateUploaded));
                objFileResponseBO.objFileBO.Add(dt);
            }
            objFileResponseBO.objPaginationBO = objPaginationBO;
            //end removing time part from Uploaded Date and adding in response
            return objFileResponseBO;

        }

        #endregion

        #region Download file
        //to be implemented soon
        #endregion


    }
}
