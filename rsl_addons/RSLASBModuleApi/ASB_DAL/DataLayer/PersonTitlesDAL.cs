﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
namespace ASB_DAL
{
   public class PersonTitlesDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get Person type DropDown while adding a person
        List<DropDownBO> ILookupRepoDAL.GetData()
        {
            var data = (from title in context.G_TITLE
                        select new DropDownBO
                        {
                            key = title.TITLEID,
                            value = title.DESCRIPTION
                        }
                        );

            return data.ToList();
        }
        #endregion
    }
}
