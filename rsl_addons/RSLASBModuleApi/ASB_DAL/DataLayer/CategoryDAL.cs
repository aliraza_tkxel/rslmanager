﻿using ASB_DAL.DatabaseEntities;
using ASB_DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;

namespace ASB_DAL
{

    public class CategoryDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get ASB category 
        public List<DropDownBO> GetData()
        {
            var data = (from cat in context.ASB_Category
                        select new DropDownBO
                        {
                            key = cat.CategoryId,
                            value = cat.Description
                        }
                        );

            return data.ToList();
        }
        #endregion

    }
}
