﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;
namespace ASB_DAL
{
    public class ManagerDAL : ABaseDAL, ILookupRepoDAL
    {
        public int employeeId { get; set; }
        #region Get ASB case officers 
        public List<DropDownBO> GetData()
        {
            var data = (from user in context.E_JOBDETAILS
                        where user.EMPLOYEEID==employeeId
                        select new DropDownBO
                        {
                            key = (int)user.ISMANAGER,
                            value = ""
                        }
                         );

            return data.ToList();
        }
        #endregion
    }
}
