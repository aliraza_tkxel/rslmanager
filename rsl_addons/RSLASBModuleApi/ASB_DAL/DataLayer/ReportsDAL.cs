﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL.DatabaseEntities;
namespace ASB_DAL
{
    public class ReportsDAL : ABaseDAL, IReportsRepoDAL
    {
        #region function for displaying data on Dashboard 
        public DashboardResponseBO GetDashboardData(PaginationBO objPaginationBO, DashboardCaseFilterBO objDashboardCaseFilterBO)
        {
            IQueryable<DashboardCaseInfoBO> data=null;
            DashboardResponseBO objDashboardResponseBO = new DashboardResponseBO();
            if(objDashboardCaseFilterBO.personId!=null)
            {
                 data = (
                    from asb in context.ASB_Case
                    from ccb in context.ASB_CaseCustomerBridge.Where(x => x.CaseId == asb.CaseId).OrderByDescending(x => x.CaseCustBridgeId)

                    join cust in context.C__CUSTOMER on ccb.PersonId equals cust.CUSTOMERID
                    into ccbGroup1
                    from customer in ccbGroup1.DefaultIfEmpty()


                    join custType in context.C_CUSTOMERTYPE on customer.CUSTOMERTYPE equals custType.CUSTOMERTYPEID
                    into ccbGroup2
                    from cType in ccbGroup2.DefaultIfEmpty()


                    join addrs in context.C_ADDRESS on ccb.PersonId equals addrs.CUSTOMERID
                    into ccbGroup3
                    from c in ccbGroup3.DefaultIfEmpty()

                    join custTenancy in context.C_CUSTOMERTENANCY.Where(x=>x.ENDDATE == null) on ccb.PersonId equals custTenancy.CUSTOMERID
                    into ccbGroup4
                    from ct in ccbGroup4.DefaultIfEmpty()

                    join cTenancy in context.C_TENANCY on ct.TENANCYID equals cTenancy.TENANCYID
                    into ccbGroup5
                    from ctenancy in ccbGroup5.DefaultIfEmpty()



                    join prop in context.P__PROPERTY on ctenancy.PROPERTYID equals prop.PROPERTYID
                    into ccbGroup6
                    from p in ccbGroup6.DefaultIfEmpty()


                    join emp in context.E__EMPLOYEE on ccb.PersonId equals emp.EMPLOYEEID
                    into ccbGroup7
                    from emp in ccbGroup7.DefaultIfEmpty()

                    join ec in context.E_CONTACT on ccb.PersonId equals ec.EMPLOYEEID
                    into ccbGroup8
                    from e in ccbGroup8.DefaultIfEmpty()

                    join caseOfficer in context.E__EMPLOYEE on asb.CaseOfficer equals caseOfficer.EMPLOYEEID
                    into ccbGroup9
                    from officer in ccbGroup9.DefaultIfEmpty()


                    join risk in context.ASB_RiskLevel on asb.RiskLevel equals risk.RiskLevelId
                    into ccbGroup10
                    from riskLevel in ccbGroup10.DefaultIfEmpty()
                    join cat in context.ASB_Category on asb.Category equals cat.CategoryId
                    join inc in context.ASB_IncidentType on asb.IncidentType equals inc.IncidentTypeId
                    join caseStatus in context.ASB_CaseStatus on asb.CaseStatus equals caseStatus.StatusId
                    join casestage in context.ASB_StagesLookup on asb.StageId equals casestage.StagesLookupId

                    where (caseStatus.Description == (objDashboardCaseFilterBO.openOrClose ? ApplicationConstants.CaseStatusOpen : ApplicationConstants.CaseStatusClosedResolved) || caseStatus.Description == (objDashboardCaseFilterBO.openOrClose ? ApplicationConstants.CaseStatusOpen : ApplicationConstants.CaseStatusClosedNoneASB) || caseStatus.Description == (objDashboardCaseFilterBO.openOrClose ? ApplicationConstants.CaseStatusOpen : ApplicationConstants.CaseStatusClosedUnresolved))
                    //&& ct.ENDDATE == null
                    && ccb.PersonId==objDashboardCaseFilterBO.personId
                    && ccb.PersonType==objDashboardCaseFilterBO.personType
                    orderby emp.EMPLOYEEID descending
                    select new DashboardCaseInfoBO
                    {

                        address = cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter ? string.Concat(c.HOUSENUMBER == null ? "" : c.HOUSENUMBER.Trim() + " ", c.ADDRESS1 == null ? "" : c.ADDRESS1.Trim() + " ", c.ADDRESS2 == null ? "" : c.ADDRESS2.Trim() + " ", c.TOWNCITY == null ? "" : c.TOWNCITY.Trim()) :
                                  cType.DESCRIPTION == ApplicationConstants.TenantFilter ? string.Concat(p.HOUSENUMBER == null ? "" : p.HOUSENUMBER.Trim() + " ", p.ADDRESS1 == null ? "" : p.ADDRESS1.Trim() + " ", p.ADDRESS2 == null ? "" : p.ADDRESS2.Trim() + " ", p.TOWNCITY == null ? "" : p.TOWNCITY.Trim()) :
                                  ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? string.Concat(e.ADDRESS1 == null ? "" : e.ADDRESS1.Trim() + " ", e.ADDRESS2 == null ? "" : e.ADDRESS2.Trim() + " ", e.ADDRESS3 == null ? "" : e.ADDRESS3.Trim() + " ", e.POSTALTOWN == null ? "" : e.POSTALTOWN.Trim()) : "-",

                        postCode = cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter ? c.POSTCODE :
                                   cType.DESCRIPTION == ApplicationConstants.TenantFilter ? p.POSTCODE :
                                   ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? e.POSTCODE : "-",
                        asbCategory = cat.Description,
                        asbIncidentType = inc.Description,
                        asbRiskLevel = riskLevel.Description == null ? "N/A" : riskLevel.Description,
                        asbCaseOfficer = officer.FIRSTNAME + " " + officer.LASTNAME,
                        caseStatus = caseStatus.Description,
                        followupDate = asb.NextFollowupDate,
                        asbCategoryId = asb.Category,
                        asbRiskLevelId = asb.RiskLevel,
                        asbCaseOfficerId = asb.CaseOfficer,
                        caseId = asb.CaseId,
                        closedDate = asb.ClosedDate,
                        Stage = casestage.Headline,
                        StageId = asb.StageId,
                        DateLogged = (DateTime)asb.DateRecorded
                    }
                  );
                //data = data.GroupBy(x => x.caseId).Select(y => y.FirstOrDefault());
            }
            else
            {
                 data = (
                    from asb in context.ASB_Case
                    from ccb in context.ASB_CaseCustomerBridge.Where(x => x.CaseId == asb.CaseId && string.Compare(x.PersonRole, ApplicationConstants.PersonRolePerpetrator, true) == 0).OrderByDescending(x => x.CaseCustBridgeId)

                    join cust in context.C__CUSTOMER on ccb.PersonId equals cust.CUSTOMERID
                    into ccbGroup1
                    from customer in ccbGroup1.DefaultIfEmpty()


                    join custType in context.C_CUSTOMERTYPE on customer.CUSTOMERTYPE equals custType.CUSTOMERTYPEID
                    into ccbGroup2
                    from cType in ccbGroup2.DefaultIfEmpty()


                    join addrs in context.C_ADDRESS on ccb.PersonId equals addrs.CUSTOMERID
                    into ccbGroup3
                    from c in ccbGroup3.DefaultIfEmpty()



                    join custTenancy in context.C_CUSTOMERTENANCY.Where(x => x.ENDDATE == null) on ccb.PersonId equals custTenancy.CUSTOMERID
                    into ccbGroup4
                    from ct in ccbGroup4.DefaultIfEmpty()

                    join cTenancy in context.C_TENANCY on ct.TENANCYID equals cTenancy.TENANCYID
                    into ccbGroup5
                    from ctenancy in ccbGroup5.DefaultIfEmpty()

                    join prop in context.P__PROPERTY on ctenancy.PROPERTYID equals prop.PROPERTYID
                    into ccbGroup6
                    from p in ccbGroup6.DefaultIfEmpty()


                    join emp in context.E__EMPLOYEE on ccb.PersonId equals emp.EMPLOYEEID
                    into ccbGroup7
                    from emp in ccbGroup7.DefaultIfEmpty()

                    join ec in context.E_CONTACT on ccb.PersonId equals ec.EMPLOYEEID
                    into ccbGroup8
                    from e in ccbGroup8.DefaultIfEmpty()

                    join caseOfficer in context.E__EMPLOYEE on asb.CaseOfficer equals caseOfficer.EMPLOYEEID
                    into ccbGroup9
                    from officer in ccbGroup9.DefaultIfEmpty()


                    join risk in context.ASB_RiskLevel on asb.RiskLevel equals risk.RiskLevelId
                    into ccbGroup10
                    from riskLevel in ccbGroup10.DefaultIfEmpty()
                    join cat in context.ASB_Category on asb.Category equals cat.CategoryId
                    join inc in context.ASB_IncidentType on asb.IncidentType equals inc.IncidentTypeId
                    join caseStatus in context.ASB_CaseStatus on asb.CaseStatus equals caseStatus.StatusId
                    join casestage in context.ASB_StagesLookup on asb.StageId equals casestage.StagesLookupId

                    where ccb.PersonRole == ApplicationConstants.PersonRolePerpetrator
                    && (caseStatus.Description == (objDashboardCaseFilterBO.openOrClose ? ApplicationConstants.CaseStatusOpen : ApplicationConstants.CaseStatusClosedResolved) || caseStatus.Description == (objDashboardCaseFilterBO.openOrClose ? ApplicationConstants.CaseStatusOpen : ApplicationConstants.CaseStatusClosedNoneASB) || caseStatus.Description == (objDashboardCaseFilterBO.openOrClose ? ApplicationConstants.CaseStatusOpen : ApplicationConstants.CaseStatusClosedUnresolved))
                    //&& ct.ENDDATE == null
                    orderby emp.EMPLOYEEID descending
                    select new DashboardCaseInfoBO
                    {

                        address = cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter ? string.Concat(c.HOUSENUMBER == null ? "" : c.HOUSENUMBER.Trim() + " ", c.ADDRESS1 == null ? "" : c.ADDRESS1.Trim() + " ", c.ADDRESS2 == null ? "" : c.ADDRESS2.Trim() + " ", c.TOWNCITY == null ? "" : c.TOWNCITY.Trim()) :
                                  cType.DESCRIPTION == ApplicationConstants.TenantFilter ? string.Concat(p.HOUSENUMBER == null ? "" : p.HOUSENUMBER.Trim() + " ", p.ADDRESS1 == null ? "" : p.ADDRESS1.Trim() + " ", p.ADDRESS2 == null ? "" : p.ADDRESS2.Trim() + " ", p.TOWNCITY == null ? "" : p.TOWNCITY.Trim()) :
                                  ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? string.Concat(e.ADDRESS1 == null ? "" : e.ADDRESS1.Trim() + " ", e.ADDRESS2 == null ? "" : e.ADDRESS2.Trim() + " ", e.ADDRESS3 == null ? "" : e.ADDRESS3.Trim() + " ", e.POSTALTOWN == null? "" : e.POSTALTOWN.Trim()) : "-",

                        postCode = cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter ? c.POSTCODE :
                                   cType.DESCRIPTION == ApplicationConstants.TenantFilter ? p.POSTCODE :
                                   ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? e.POSTCODE : "-",
                        asbCategory = cat.Description,
                        asbIncidentType = inc.Description,
                        asbRiskLevel = riskLevel.Description == null ? "N/A" : riskLevel.Description,
                        asbCaseOfficer = officer.FIRSTNAME + " " + officer.LASTNAME,
                        caseStatus = caseStatus.Description,
                        followupDate = asb.NextFollowupDate,
                        asbCategoryId = asb.Category,
                        asbRiskLevelId = asb.RiskLevel,
                        asbCaseOfficerId = asb.CaseOfficer,
                        caseId = asb.CaseId,
                        closedDate = asb.ClosedDate,
                        Stage = casestage.Headline,
                        StageId = asb.StageId,
                        DateLogged = (DateTime)asb.DateRecorded
                    }
                  );
            }

            if (!objDashboardCaseFilterBO.openOrClose)
            {
                data = data.GroupBy(x => x.caseId).Select(x => x.FirstOrDefault());
                data = data.Distinct().OrderByDescending(x => x.closedDate);//sort by recent closed case
            }
            else
            {
                data = data.GroupBy(x => x.caseId).Select(x => x.FirstOrDefault());
                data = data.Distinct().OrderByDescending(x => x.DateLogged);//sorting data based on DateLogged
            }

            //data = data.Distinct().OrderBy(x => x.caseId);//sorting data based on caseID
            //objDashboardResponseBO.objDashboardAlertsBO = GetAlertsData(data);//setting alerts values

            //start applying filters on data
            if (objDashboardCaseFilterBO.categoryId > 0)
            {
                data = data.Where(x => x.asbCategoryId == objDashboardCaseFilterBO.categoryId);
            }
            if (objDashboardCaseFilterBO.riskLevelId > 0)
            {
                data = data.Where(x => x.asbRiskLevelId == objDashboardCaseFilterBO.riskLevelId);
            }
            if (objDashboardCaseFilterBO.caseOfficerId > 0)
            {
                data = data.Where(x => x.asbCaseOfficerId == objDashboardCaseFilterBO.caseOfficerId);
            }
            // We calculates alerts here because second level filters do not require recalculation of alerts.
            objDashboardResponseBO.objDashboardAlertsBO = GetAlertsData(data);//setting alerts values

            if (objDashboardCaseFilterBO.IsOverDueCases == true)
            {
                data = data.Where(x => x.followupDate < DateTime.Today);
            }
            if (objDashboardCaseFilterBO.IsHighRisk == true)
            {
                data = data.Where(x => x.asbRiskLevelId == 3);
            }
            if (objDashboardCaseFilterBO.IsOpenCases == true)
            {
                //Do nothing
            }
            //End applying filters on data
            if (objPaginationBO.PageSize == ApplicationConstants.NO_PAGINATION || 
                objPaginationBO.PageNumber == ApplicationConstants.NO_PAGINATION)
            {
                // Pagination skiped.
            }
            else
            {
                // Start of pagination
                int totalPages = data.Count();
                objPaginationBO.TotalRows = totalPages;
                objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objPaginationBO.PageSize);
                data = data.Skip((objPaginationBO.PageNumber - 1) * objPaginationBO.PageSize).Take(objPaginationBO.PageSize);
                // End of pagination
            }
            //objDashboardResponseBO.objDashboardAlertsBO = GetAlertsData(data);//setting alerts values
            objDashboardResponseBO.objDashboardCaseInfoBO = data.ToList();//setting case info on dashboard

            objDashboardResponseBO.objPaginationBO = objPaginationBO;//setting pagination info
            return objDashboardResponseBO;
        }
        #endregion

        #region Dashboard Alerts caluculation
       
        private DashboardAlertsBO GetAlertsData(IQueryable<DashboardCaseInfoBO> objDashboardCaseInfoBO)
        {
            //highRiskCount calculation
            DashboardAlertsBO objDashboardAlertsBO = new DashboardAlertsBO();
            objDashboardAlertsBO.highRiskCasesCount = (from highRiskCount in objDashboardCaseInfoBO
                                                       where highRiskCount.asbRiskLevel == ApplicationConstants.RiskLevelHigh
                                                       select highRiskCount.caseId
                                                       ).Count();
            //openCaseCount calculation
            objDashboardAlertsBO.openCaseCount = (from openCaseCount in objDashboardCaseInfoBO
                                                       where openCaseCount.caseStatus == ApplicationConstants.CaseStatusOpen
                                                       select openCaseCount.caseId
                                                       ).Count();

            //overdueCasesCount calculation
            objDashboardAlertsBO.overdueCasesCount = (from overdueCasesCount in objDashboardCaseInfoBO
                                                  where overdueCasesCount.followupDate < DateTime.Today
                                                  select overdueCasesCount.caseId
                                                       ).Count();
            return objDashboardAlertsBO;
        }
        #endregion

        #region Case Management Report
       
        public CaseManagementReportResponseBO GetCaseManagementReportData(CaseManagementReportRequestBO objCaseManagementReportRequestBO)
        {
            CaseManagementReportResponseBO objCaseManagementReportResponseBO = new CaseManagementReportResponseBO();
            // Getting data of Cases
            var caseInfo = (from asb in context.ASB_Case 
                            join risk in context.ASB_RiskLevel on asb.RiskLevel equals risk.RiskLevelId
                            into group1
                            from risklevel in group1.DefaultIfEmpty()
                            join emp in context.E__EMPLOYEE on asb.CaseOfficer equals emp.EMPLOYEEID
                            join inc in context.ASB_IncidentType on asb.IncidentType equals inc.IncidentTypeId
                            join status in context.ASB_CaseStatus on asb.CaseStatus equals status.StatusId
                            join casestage in context.ASB_StagesLookup on asb.StageId equals casestage.StagesLookupId
                            select new CaseDetailsBO
                            {
                                CaseId = asb.CaseId,
                                ReportedDate = asb.DateReported,//get only date part
                                CaseOfficer =string.Concat(emp.FIRSTNAME," ", emp.LASTNAME),
                                IncidentType = inc.Description,
                                Risk = risklevel.Description==null?"N/A": risklevel.Description,
                                CaseStatus = status.Description,
                                AsbCategoryId = asb.Category,
                                RiskId = asb.RiskLevel,
                                CaseStatusId = asb.CaseStatus,
                                FollowUpDate = asb.NextFollowupDate,
                                CaseOfficerId = emp.EMPLOYEEID,
                                StageId=asb.StageId,
                                ClosedDate=asb.ClosedDate,
                                Stage=casestage.Headline                            }
                );
          
            //start applying filters on data
            if (objCaseManagementReportRequestBO.RiskLevelId > 0)
            {
                caseInfo = caseInfo.Where(x => x.RiskId == objCaseManagementReportRequestBO.RiskLevelId);
            }
            if (objCaseManagementReportRequestBO.CategoryId > 0)
            {
                caseInfo = caseInfo.Where(x => x.AsbCategoryId == objCaseManagementReportRequestBO.CategoryId);
            }
            if (objCaseManagementReportRequestBO.CaseStatus > 0)
            {
                if(objCaseManagementReportRequestBO.CaseStatus==1)
                    caseInfo = caseInfo.Where(x => x.CaseStatusId == objCaseManagementReportRequestBO.CaseStatus);
                else
                    caseInfo = caseInfo.Where(x => x.CaseStatus != ApplicationConstants.CaseStatusOpen);
            }
            if(objCaseManagementReportRequestBO.IsOverdue)
            {
                caseInfo = caseInfo.Where(x => x.FollowUpDate < DateTime.Today);
            }
            if(objCaseManagementReportRequestBO.CaseOfficerId > 0)
            {
                caseInfo = caseInfo.Where(x => x.CaseOfficerId == objCaseManagementReportRequestBO.CaseOfficerId);
            }
            if(objCaseManagementReportRequestBO.ReportedFrom!=null)
            {
                caseInfo = caseInfo.Where(x => x.ReportedDate >= objCaseManagementReportRequestBO.ReportedFrom);
            }
            if (objCaseManagementReportRequestBO.ReportedTo != null)
            {
                caseInfo = caseInfo.Where(x => x.ReportedDate <= objCaseManagementReportRequestBO.ReportedTo);
            }
            //End applying filters on data   

            //start applying search filter on data
            //if (objCaseManagementReportRequestBO.Search != "" || objCaseManagementReportRequestBO.Search != null)
            //{
            //    caseInfo = caseInfo.Where(x => x.CaseId.ToString().Contains(objCaseManagementReportRequestBO.Search.Trim()) || x.CaseOfficer.Contains(objCaseManagementReportRequestBO.Search.Trim()));
            //}
            //end applying search filter on data



            //start Getting data of Complainant and Perpetrator
            var caseList = caseInfo.ToList();
            foreach (var data in caseList)
            {
                data.objComplainantList = GetPersonList(ApplicationConstants.PersonRoleComplainant, data.CaseId, objCaseManagementReportRequestBO.Search);
                data.objPerpetratorList = GetPersonList(ApplicationConstants.PersonRolePerpetrator, data.CaseId, objCaseManagementReportRequestBO.Search);
            }
            //end Getting data of Complainant and Perpetrator
            if (objCaseManagementReportRequestBO.sortBy == "ASC")
            {
                if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[0])
                    caseList = caseList.OrderBy(x => x.CaseId).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[1])
                    caseList = caseList.OrderBy(x => x.ReportedDate).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[2])
                    caseList = caseList.OrderBy(x => x.objComplainantList[0].FirstName).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[3])
                    caseList = caseList.OrderBy(x => x.objComplainantList[0].Address1).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[4])
                    caseList = caseList.OrderBy(x => x.objComplainantList[0].PostCode).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[5])
                    caseList = caseList.OrderBy(x => x.objComplainantList[0].LocalAuthority).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[6])
                    caseList = caseList.OrderBy(x => x.objPerpetratorList[0].FirstName).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[7])
                    caseList = caseList.OrderBy(x => x.Stage).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[8])
                    caseList = caseList.OrderBy(x => x.CaseOfficer).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[9])
                    caseList = caseList.OrderBy(x => x.IncidentType).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[10])
                    caseList = caseList.OrderBy(x => x.Risk).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[11])
                    caseList = caseList.OrderBy(x => x.CaseStatus).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[12])
                    caseList = caseList.OrderBy(x => x.ClosedDate).ToList();
            }
            else
            {
                if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[0])
                    caseList = caseList.OrderByDescending(x => x.CaseId).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[1])
                    caseList = caseList.OrderByDescending(x => x.ReportedDate).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[2])
                    caseList = caseList.OrderByDescending(x => x.objComplainantList[0].FirstName).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[3])
                    caseList = caseList.OrderByDescending(x => x.objComplainantList[0].Address1).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[4])
                    caseList = caseList.OrderByDescending(x => x.objComplainantList[0].PostCode).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[5])
                    caseList = caseList.OrderByDescending(x => x.objComplainantList[0].LocalAuthority).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[6])
                    caseList = caseList.OrderByDescending(x => x.objPerpetratorList[0].FirstName).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[7])
                    caseList = caseList.OrderByDescending(x => x.Stage).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[8])
                    caseList = caseList.OrderByDescending(x => x.CaseOfficer).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[9])
                    caseList = caseList.OrderByDescending(x => x.IncidentType).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[10])
                    caseList = caseList.OrderByDescending(x => x.Risk).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[11])
                    caseList = caseList.OrderByDescending(x => x.CaseStatus).ToList();
                else if (objCaseManagementReportRequestBO.sortOrder == ApplicationConstants.reportSort[12])
                    caseList = caseList.OrderByDescending(x => x.ClosedDate).ToList();
            }
            if (objCaseManagementReportRequestBO.Search != "" || objCaseManagementReportRequestBO.Search != null)
            {
                var search = objCaseManagementReportRequestBO.Search.Trim();
                caseList = caseList.Where(x => x.CaseId.ToString().Contains(search) || x.CaseOfficer.ToUpper().Contains(search.ToUpper()) || x.objComplainantList.Any(y => (y.FirstName + " " + y.LastName).ToUpper().Contains((search.Trim().ToUpper()))) || x.objPerpetratorList.Any(y => (y.FirstName + " " + y.LastName).ToUpper().Contains((search.ToUpper())))).ToList();
            }
                //start of pagination
            int totalPages = caseList.Count();
            objCaseManagementReportResponseBO.objPaginationBO = new PaginationBO();
            objCaseManagementReportResponseBO.objPaginationBO.TotalRows = totalPages;
            objCaseManagementReportResponseBO.objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objCaseManagementReportRequestBO.objPaginationBO.PageSize);
            objCaseManagementReportResponseBO.objPaginationBO.PageNumber = objCaseManagementReportRequestBO.objPaginationBO.PageNumber;
            objCaseManagementReportResponseBO.objPaginationBO.PageSize = objCaseManagementReportRequestBO.objPaginationBO.PageSize;
            caseList = caseList.Skip((objCaseManagementReportRequestBO.objPaginationBO.PageNumber - 1) * objCaseManagementReportRequestBO.objPaginationBO.PageSize).Take(objCaseManagementReportRequestBO.objPaginationBO.PageSize).ToList();
            //end of pagination

            objCaseManagementReportResponseBO.objCaseBO = caseList;

            return objCaseManagementReportResponseBO;
        }
        #endregion

        #region  Get Complainant and Perpetrator data against caseid     
        private List<PersonsInfoForCaseMgmtReportBO> GetPersonList(string PersonRole,int caseId,string searchFilter)
        {
            var personData = (from ccb in context.ASB_CaseCustomerBridge
                              join emp in context.E__EMPLOYEE on ccb.PersonId equals emp.EMPLOYEEID
                              into ccbGroup1
                              from e in ccbGroup1.DefaultIfEmpty()

                              join cust in context.C__CUSTOMER on ccb.PersonId equals cust.CUSTOMERID
                              into ccbGroup2
                              from c in ccbGroup2.DefaultIfEmpty()

                              join lau in context.G_LOCALAUTHORITY on c.LOCALAUTHORITY equals lau.LOCALAUTHORITYID
                              into ccbGroup5
                              from la in ccbGroup5.DefaultIfEmpty()

                              join cAddress in context.C_ADDRESS on c.CUSTOMERID equals cAddress.CUSTOMERID
                              into ccbGroup3
                              from ca in ccbGroup3.DefaultIfEmpty()

                              join eAddress in context.E_CONTACT on e.EMPLOYEEID equals eAddress.EMPLOYEEID
                              into ccbGroup4
                              from ea in ccbGroup4.DefaultIfEmpty()

                              join eAddresstype in context.C_ADDRESSTYPE on ca.ADDRESSTYPE equals eAddresstype.ADDRESSTYPEID
                              into ccGroup5
                              from at in ccGroup5.DefaultIfEmpty()

                              join custType in context.C_CUSTOMERTYPE on c.CUSTOMERTYPE equals custType.CUSTOMERTYPEID
                              into ccbGroup6
                              from cType in ccbGroup6.DefaultIfEmpty()

                              join custTenancy in context.C_CUSTOMERTENANCY.Where(x => x.ENDDATE == null) on ccb.PersonId equals custTenancy.CUSTOMERID
                              into ccbGroup7
                              from ct in ccbGroup7.DefaultIfEmpty()

                              join cTenancy in context.C_TENANCY on ct.TENANCYID equals cTenancy.TENANCYID
                              into ccbGroup8
                              from ctenancy in ccbGroup8.DefaultIfEmpty()

                              join prop in context.P__PROPERTY on ctenancy.PROPERTYID equals prop.PROPERTYID
                              into ccbGroup9
                              from p in ccbGroup9.DefaultIfEmpty()

                              where ccb.PersonRole == PersonRole
                          && ccb.CaseId == caseId && at.DESCRIPTION == "Current"
                              select new PersonsInfoForCaseMgmtReportBO
                          {   
                             CaseId=ccb.CaseId,                               
                             FirstName = ccb.PersonType == ApplicationConstants.PersonTypeCustomer ? c.FIRSTNAME : e.FIRSTNAME,
                             LastName = ccb.PersonType == ApplicationConstants.PersonTypeCustomer ? c.LASTNAME : e.LASTNAME,
                             Address1 = ccb.PersonType == (ApplicationConstants.PersonTypeCustomer) && (cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter) ? ca.HOUSENUMBER:
                                        ccb.PersonType == (ApplicationConstants.PersonTypeCustomer) &&  cType.DESCRIPTION == ApplicationConstants.TenantFilter ? p.HOUSENUMBER :
                                        ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? "" : "-",

                         
                             Address2 = ccb.PersonType == (ApplicationConstants.PersonTypeCustomer) && (cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter) ? ca.ADDRESS1 :
                                        ccb.PersonType == (ApplicationConstants.PersonTypeCustomer) && cType.DESCRIPTION == ApplicationConstants.TenantFilter ? p.ADDRESS1 :
                                        ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? "": "-",



                             PostCode = ccb.PersonType == (ApplicationConstants.PersonTypeCustomer) && (cType.DESCRIPTION == ApplicationConstants.GeneralFilter || cType.DESCRIPTION == ApplicationConstants.StakeholderFilter || cType.DESCRIPTION == ApplicationConstants.ProspectiveTenantFilter) ? ca.POSTCODE :
                                        ccb.PersonType == (ApplicationConstants.PersonTypeCustomer) && cType.DESCRIPTION == ApplicationConstants.TenantFilter ? p.POSTCODE :
                                        ccb.PersonType == ApplicationConstants.PersonTypeEmployee ? "" : "-",                            
                             LocalAuthority=la.DESCRIPTION==null?"N/A":la.DESCRIPTION                                              
                            }
                          );

            personData = personData.Distinct();
            //if (searchFilter != "" || searchFilter != null)
            //{
            //    personData = personData.Where(x => x.FirstName.ToString().Contains(searchFilter) || x.LastName.ToString().Contains(searchFilter));               
            //}

            return personData.ToList();
            
        }

        #endregion
        

    }
}
