﻿using System;
using System.Collections.Generic;
using System.Linq;
using ASB_BO;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;

namespace ASB_DAL
{
    public class RiskLevelDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get ASB risk level
        public List<DropDownBO> GetData()
        {
            var data = (from lvl in context.ASB_RiskLevel
                        select new DropDownBO
                        {
                            key = lvl.RiskLevelId,
                            value = lvl.Description
                        }
                         );

            return data.ToList();
        }
        #endregion

    }

}
