﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL;
using ASB_DAL.DatabaseEntities;
namespace ASB_DAL
{
   public class CaseOfficerDAL : ABaseDAL, ILookupRepoDAL
    {
        #region Get ASB case officers 
        public List<DropDownBO> GetData()
        {
            var data = (
                from emp in context.E__EMPLOYEE
                join ejrt in context.E_JOBROLETEAM on emp.JobRoleTeamId equals ejrt.JobRoleTeamId
                join jd in context.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                join et in context.E_TEAM on ejrt.TeamId equals et.TEAMID
                join ejr in context.E_JOBROLE on ejrt.JobRoleId equals ejr.JobRoleId
                where  (jd.ACTIVE == 1) && et.TEAMNAME == ApplicationConstants.HousingTeamType && (et.ACTIVE==1) &&
                    (ejr.JobeRoleDescription == ApplicationConstants.Neighbourhoodofficer || ejr.JobeRoleDescription == ApplicationConstants.HousingTeamLeader)
                orderby emp.FIRSTNAME, emp.LASTNAME
                select new DropDownBO
                {
                    key = emp.EMPLOYEEID,
                    value = emp.FIRSTNAME + " " + emp.LASTNAME
                }
                        );

//            select* from E__EMPLOYEE emp
//inner join E_JOBROLETEAM ejrt on emp.JobRoleTeamId = ejrt.JobRoleTeamId
//inner join E_JOBDETAILS jd on emp.EMPLOYEEID = jd.employeeid
//inner join E_TEAM et on ejrt.TeamId = et.teamid
//inner join E_JOBROLE ejr on ejrt.JobRoleId = ejr.JobRoleId
//where(jd.ACTIVE = 1) and et.TEAMNAME = 'Frontline' and(et.ACTIVE = 1) and
//                    ejr.JobeRoleDescription = 'Neighbourhood Officer'

            return data.ToList();
        }
        #endregion

    }
}
