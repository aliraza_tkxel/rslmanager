﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL.DatabaseEntities;
using ASB_DAL;
using System.Data.Common;

namespace ASB_DAL
{
    public class CaseDAL : ABaseDAL, ICaseRepoDAL
    {
        JournalDAL objJournalDAL = new JournalDAL();
        ActionsBO objActionsBO = new ActionsBO();

        #region new record insertion
        public int AddCase(AddCaseRequestBO objAddCaseRequestBO)
        {       
            //by default EFW placed it in transaction        
            //start adding data of case
            ASB_Case objCase = new ASB_Case();
            objCase.DateRecorded = objAddCaseRequestBO.objCaseBO.DateRecorded;
            objCase.DateReported = objAddCaseRequestBO.objCaseBO.DateReported;
            objCase.IncidentDate = objAddCaseRequestBO.objCaseBO.IncidentDate;
            objCase.IncidentTime = objAddCaseRequestBO.objCaseBO.IncidentTime;
            objCase.CaseOfficer = objAddCaseRequestBO.objCaseBO.CaseOfficer;
            objCase.CaseOfficerTwo = objAddCaseRequestBO.objCaseBO.CaseOfficerTwo;
            objCase.Category = objAddCaseRequestBO.objCaseBO.Category;
            objCase.IncidentType = objAddCaseRequestBO.objCaseBO.incidentType;
            objCase.RiskLevel = objAddCaseRequestBO.objCaseBO.RiskLevel;
            if(objCase.RiskLevel==0)
            {
                objCase.RiskLevel = null;
            }
            objCase.IncidentDescription = objAddCaseRequestBO.objCaseBO.IncidentDescription;
            objCase.PoliceNotified = objAddCaseRequestBO.objCaseBO.PoliceNotified;
            objCase.CrimeCaseNumber = objAddCaseRequestBO.objCaseBO.CrimeCaseNumber;
            objCase.NextFollowupDate = objAddCaseRequestBO.objCaseBO.NextFollowupDate;
            objCase.FollowupDescription = objAddCaseRequestBO.objCaseBO.FollowupDescription;
            objCase.ClosedDate = objAddCaseRequestBO.objCaseBO.ClosedDate;
            objCase.ClosedDescription = objAddCaseRequestBO.objCaseBO.ClosedDescription;
            objCase.CaseStatus = objAddCaseRequestBO.objCaseBO.CaseStatus;
            objCase.StageId = objAddCaseRequestBO.objCaseBO.Stage;
            objCase.SubCategoryId = objAddCaseRequestBO.objCaseBO.SubCategory;
            objCase.TypeId = objAddCaseRequestBO.objCaseBO.Types;
            objCase.SubTypeId = objAddCaseRequestBO.objCaseBO.SubType;
            objCase.ReviewDate = objAddCaseRequestBO.objCaseBO.ReviewDate;
            context.ASB_Case.Add(objCase);
            context.SaveChanges();
            //end adding data of case

            int  caseId = objCase.CaseId;//get newly inserted value of caseID      
            //start adding data of person. eg comp and prep
            ASB_CaseCustomerBridge objCaseCustomerBridge = new ASB_CaseCustomerBridge();
            foreach (PersonBO obj in objAddCaseRequestBO.objPersonBO)
            {
                objCaseCustomerBridge.CaseId = caseId;
                objCaseCustomerBridge.PersonId = obj.PersonId;
                objCaseCustomerBridge.PersonRole = obj.PersonRole;
                objCaseCustomerBridge.PersonType = obj.PersonType;
                context.ASB_CaseCustomerBridge.Add(objCaseCustomerBridge);
                context.SaveChanges();
                //end adding data of person. eg comp and prep

                //start Recording action for case journal  

                objActionsBO.CaseId = objCaseCustomerBridge.CaseId;
                objActionsBO.RecordedBy = objAddCaseRequestBO.RecordedBy;
                objActionsBO.RecordedDate = DateTime.Now;
                if (string.Compare(objCaseCustomerBridge.PersonRole, ApplicationConstants.PersonRoleComplainant, true) == 0)
                {
                    objActionsBO.Action = ApplicationConstants.ComplainantAdded;
                    objActionsBO.ActionNotes = ApplicationConstants.ComplainantAdded; 
                }
               else if (string.Compare(objCaseCustomerBridge.PersonRole, ApplicationConstants.PersonRolePerpetrator, true) == 0)
                {
                    objActionsBO.Action = ApplicationConstants.PerpetratorAdded;
                    objActionsBO.ActionNotes = ApplicationConstants.PerpetratorAdded;
                }
                objActionsBO.ReviewDate = DateTime.Now;
                objJournalDAL.AddInJournal(objActionsBO);
                //End Recording action for case journal  
            }
           

            return caseId;
        }
        #endregion

        #region Update case details
        public int UpdateCaseDetails(UpdateCaseDetailsRequestBO objupdateCaseRequestBO)
        {
            //start Updating data of case
            var stageChanged = false;
            int caseId = objupdateCaseRequestBO.objCaseBO.CaseId;//get updated case ID  
            var updateCase = (from asb in context.ASB_Case
                               where asb.CaseId == caseId
                              select asb).FirstOrDefault();

            if((objupdateCaseRequestBO.objCaseBO.Stage!=updateCase.StageId) || (objupdateCaseRequestBO.objCaseBO.SubCategory != updateCase.SubCategoryId))
            {
                stageChanged = true;
            }
            updateCase.CaseOfficer = objupdateCaseRequestBO.objCaseBO.CaseOfficer;
            updateCase.CaseOfficerTwo = objupdateCaseRequestBO.objCaseBO.CaseOfficerTwo;
            updateCase.Category = objupdateCaseRequestBO.objCaseBO.Category;
            updateCase.IncidentType = objupdateCaseRequestBO.objCaseBO.incidentType;
            updateCase.RiskLevel = objupdateCaseRequestBO.objCaseBO.RiskLevel;
            if (updateCase.RiskLevel == 0)
            {
                updateCase.RiskLevel = null;
            }
            updateCase.IncidentDescription = objupdateCaseRequestBO.objCaseBO.IncidentDescription;
            updateCase.PoliceNotified = objupdateCaseRequestBO.objCaseBO.PoliceNotified;
            updateCase.FollowupDescription = objupdateCaseRequestBO.objCaseBO.FollowupDescription;
            updateCase.StageId = objupdateCaseRequestBO.objCaseBO.Stage;
            updateCase.SubCategoryId = objupdateCaseRequestBO.objCaseBO.SubCategory;
            updateCase.TypeId = objupdateCaseRequestBO.objCaseBO.Types;
            updateCase.SubTypeId = objupdateCaseRequestBO.objCaseBO.SubType;
            context.SaveChanges();
            if(stageChanged)
            {
                objActionsBO.CaseId = objupdateCaseRequestBO.objCaseBO.CaseId;
                objActionsBO.RecordedBy = objupdateCaseRequestBO.RecordedBy;
                objActionsBO.RecordedDate = DateTime.Now;
                objActionsBO.Action = ApplicationConstants.Stages;
                objActionsBO.ActionNotes = ApplicationConstants.StagesChanged;
                objActionsBO.ReviewDate = DateTime.Now;
                objActionsBO.Stage= objupdateCaseRequestBO.objCaseBO.Stage;
                objActionsBO.SubCategory = objupdateCaseRequestBO.objCaseBO.SubCategory;
                objActionsBO.Types = objupdateCaseRequestBO.objCaseBO.Types;
                objActionsBO.SubType = objupdateCaseRequestBO.objCaseBO.SubType;
                objJournalDAL.AddInJournal(objActionsBO);
            }


            //end adding data of case

                
            return caseId;
        }
        #endregion

        #region Get a case details by caseID
        public UpdateCaseResponseBO PopulateDetailsForCaseUpdateByCaseId(int caseId)
        {
            var data = (from caseInfo in context.ASB_Case
                        join emp in context.E__EMPLOYEE on caseInfo.CaseOfficer equals emp.EMPLOYEEID
                        join emptwo in context.E__EMPLOYEE on caseInfo.CaseOfficerTwo equals emptwo.EMPLOYEEID
                        into ccbGroup1
                        from c in ccbGroup1.DefaultIfEmpty()
                        join cat in context.ASB_Category on caseInfo.Category equals cat.CategoryId
                        join inc in context.ASB_IncidentType on caseInfo.IncidentType equals inc.IncidentTypeId
                        join risk in context.ASB_RiskLevel on caseInfo.RiskLevel equals risk.RiskLevelId
                        into group1
                        from cnt in group1.DefaultIfEmpty()
                        join status in context.ASB_CaseStatus on caseInfo.CaseStatus equals status.StatusId
                        join stage in context.ASB_StagesLookup on caseInfo.StageId equals stage.StagesLookupId
                        where caseInfo.CaseId == caseId
                        select new UpdateCaseBO
                        {
                            caseId = caseInfo.CaseId,
                            DateRecorded = caseInfo.DateRecorded.ToString(),
                            DateReported = caseInfo.DateReported.ToString(),
                            IncidentDate = caseInfo.IncidentDate.ToString(),
                            IncidentTime = caseInfo.IncidentTime.ToString(),
                            CaseOfficer = string.Concat(emp.FIRSTNAME," ",emp.LASTNAME),
                            CaseOfficerTwo=caseInfo.CaseOfficerTwo==null?"N/A": string.Concat(c.FIRSTNAME, " ", c.LASTNAME),
                            Category = cat.Description,
                            incidentType = inc.Description,
                            RiskLevel = cnt.Description==null?"N/A": cnt.Description,
                            IncidentDescription = caseInfo.IncidentDescription,
                            PoliceNotified =  (caseInfo.PoliceNotified == true ?"Yes" :"No"),
                            CrimeCaseNumber = caseInfo.CrimeCaseNumber,
                            NextFollowupDate = caseInfo.NextFollowupDate.ToString(),
                            FollowupDescription = caseInfo.FollowupDescription,
                            ClosedDate = caseInfo.ClosedDate.ToString(),
                            ClosedDescription = caseInfo.ClosedDescription,
                            CaseStatus = status.Description,
                            ReviewDate=caseInfo.ReviewDate.ToString(),
                            StageId=caseInfo.StageId,
                            Stage=stage.Headline
                        }
                        );
            UpdateCaseResponseBO response = new UpdateCaseResponseBO();
            response.objUpdateCaseBO = new UpdateCaseBO();
            foreach (var dt in data)
            {     
                //start getting date part         
                response.objUpdateCaseBO.DateRecorded = string.Format("{0:d}", Convert.ToDateTime(dt.DateRecorded));
                response.objUpdateCaseBO.DateReported = string.Format("{0:d}", Convert.ToDateTime(dt.DateReported));
                response.objUpdateCaseBO.IncidentDate = string.Format("{0:d}", Convert.ToDateTime(dt.IncidentDate));
                response.objUpdateCaseBO.IncidentTime = string.Format("{0:t}", Convert.ToDateTime(dt.IncidentTime));//get time part
                //response.objUpdateCaseBO.ReviewDate = string.Format("{0:d}", Convert.ToDateTime(dt.ReviewDate));
                response.objUpdateCaseBO.NextFollowupDate = string.Format("{0:d}", Convert.ToDateTime(dt.NextFollowupDate));
                //end getting date part  
                if (dt.ClosedDate != null && dt.ClosedDate != "")
                {
                    response.objUpdateCaseBO.ClosedDate = string.Format("{0:d}", Convert.ToDateTime(dt.ClosedDate));
                }
                else
                {
                    response.objUpdateCaseBO.ClosedDate = dt.ClosedDate.ToString();
                }
                if (dt.ReviewDate != null && dt.ReviewDate != "")
                {
                    response.objUpdateCaseBO.ReviewDate = string.Format("{0:d}", Convert.ToDateTime(dt.ReviewDate));
                }
                else
                {
                    response.objUpdateCaseBO.ReviewDate = dt.ReviewDate.ToString();
                }
                response.objUpdateCaseBO.caseId = dt.caseId;
                response.objUpdateCaseBO.CaseOfficer = dt.CaseOfficer;
                response.objUpdateCaseBO.CaseOfficerTwo = dt.CaseOfficerTwo;
                response.objUpdateCaseBO.Category = dt.Category;
                response.objUpdateCaseBO.incidentType = dt.incidentType;
                response.objUpdateCaseBO.RiskLevel = dt.RiskLevel;
                response.objUpdateCaseBO.IncidentDescription = dt.IncidentDescription;
                response.objUpdateCaseBO.PoliceNotified = dt.PoliceNotified;
                response.objUpdateCaseBO.CrimeCaseNumber = dt.CrimeCaseNumber;
                response.objUpdateCaseBO.FollowupDescription = dt.FollowupDescription;
                response.objUpdateCaseBO.ClosedDescription = dt.ClosedDescription;
                response.objUpdateCaseBO.CaseStatus = dt.CaseStatus;
                response.objUpdateCaseBO.Stage=dt.Stage;
                response.objUpdateCaseBO.StageId=dt.StageId;
                //response.objUpdateCaseBO.ReviewDate = dt.ReviewDate;
            }
            response.objPersonList = GetPersonDetailsByCaseId(caseId);
            return response;      
        }
        #endregion

        #region Get a case details by caseID for Details Amendment
        public AmendCaseResponseBO PopulateDetailsForCaseAmendByCaseId(int caseId)
        {
            var data = (from caseInfo in context.ASB_Case
                        join emp in context.E__EMPLOYEE on caseInfo.CaseOfficer equals emp.EMPLOYEEID
                        join emptwo in context.E__EMPLOYEE on caseInfo.CaseOfficerTwo equals emptwo.EMPLOYEEID
                        into ccbGroup1
                        from c in ccbGroup1.DefaultIfEmpty()
                        join cat in context.ASB_Category on caseInfo.Category equals cat.CategoryId
                        join inc in context.ASB_IncidentType on caseInfo.IncidentType equals inc.IncidentTypeId
                        join risk in context.ASB_RiskLevel on caseInfo.RiskLevel equals risk.RiskLevelId
                        into group1
                        from cnt in group1.DefaultIfEmpty()
                        join status in context.ASB_CaseStatus on caseInfo.CaseStatus equals status.StatusId
                        join stage in context.ASB_StagesLookup on caseInfo.StageId equals stage.StagesLookupId
                        where caseInfo.CaseId == caseId
                        select new AmendCaseBO
                        {
                            caseId = caseInfo.CaseId,
                            DateRecorded = caseInfo.DateRecorded.ToString(),
                            DateReported = caseInfo.DateReported.ToString(),
                            IncidentDate = caseInfo.IncidentDate.ToString(),
                            IncidentTime = caseInfo.IncidentTime.ToString(),
                            CaseOfficer = caseInfo.CaseOfficer,
                            CaseOfficerTwo = caseInfo.CaseOfficerTwo,
                            Category = caseInfo.Category,
                            incidentType = caseInfo.IncidentType,
                            RiskLevel = caseInfo.RiskLevel,
                            IncidentDescription = caseInfo.IncidentDescription,
                            PoliceNotified = (caseInfo.PoliceNotified == true ? 1 : 0),
                            CrimeCaseNumber = (int)caseInfo.CrimeCaseNumber,
                            NextFollowupDate = caseInfo.NextFollowupDate.ToString(),
                            FollowupDescription = caseInfo.FollowupDescription,
                            ClosedDate = caseInfo.ClosedDate.ToString(),
                            ClosedDescription = caseInfo.ClosedDescription,
                            CaseStatus = status.Description,
                            ReviewDate = caseInfo.ReviewDate.ToString(),
                            StageId = (int)caseInfo.StageId,
                            SelectedSubCategoryId = caseInfo.SubCategoryId,
                            SelectedSubTypeId = (int)caseInfo.SubTypeId,
                            SelectedTypeId = (int)caseInfo.TypeId
                        }
                        );
            AmendCaseResponseBO response = new AmendCaseResponseBO();
            response.objUpdateCaseBO = new AmendCaseBO();
            foreach (var dt in data)
            {
                //start getting date part         
                response.objUpdateCaseBO.DateRecorded = string.Format("{0:d}", Convert.ToDateTime(dt.DateRecorded));
                response.objUpdateCaseBO.DateReported = string.Format("{0:d}", Convert.ToDateTime(dt.DateReported));
                response.objUpdateCaseBO.IncidentDate = string.Format("{0:d}", Convert.ToDateTime(dt.IncidentDate));
                response.objUpdateCaseBO.IncidentTime = string.Format("{0:t}", Convert.ToDateTime(dt.IncidentTime));//get time part
                //response.objUpdateCaseBO.ReviewDate = string.Format("{0:d}", Convert.ToDateTime(dt.ReviewDate));
                response.objUpdateCaseBO.NextFollowupDate = string.Format("{0:d}", Convert.ToDateTime(dt.NextFollowupDate));
                //end getting date part  
                if (dt.ClosedDate != null && dt.ClosedDate != "")
                {
                    response.objUpdateCaseBO.ClosedDate = string.Format("{0:d}", Convert.ToDateTime(dt.ClosedDate));
                }
                else
                {
                    response.objUpdateCaseBO.ClosedDate = dt.ClosedDate.ToString();
                }
                if (dt.ReviewDate != null && dt.ReviewDate != "")
                {
                    response.objUpdateCaseBO.ReviewDate = string.Format("{0:d}", Convert.ToDateTime(dt.ReviewDate));
                }
                else
                {
                    response.objUpdateCaseBO.ReviewDate = dt.ReviewDate.ToString();
                }
                response.objUpdateCaseBO.caseId = dt.caseId;
                response.objUpdateCaseBO.CaseOfficer = dt.CaseOfficer;
                response.objUpdateCaseBO.CaseOfficerTwo = dt.CaseOfficerTwo;
                response.objUpdateCaseBO.Category = dt.Category;
                response.objUpdateCaseBO.incidentType = dt.incidentType;
                response.objUpdateCaseBO.RiskLevel = dt.RiskLevel;
                response.objUpdateCaseBO.IncidentDescription = dt.IncidentDescription;
                response.objUpdateCaseBO.PoliceNotified = dt.PoliceNotified;
                response.objUpdateCaseBO.CrimeCaseNumber = dt.CrimeCaseNumber;
                response.objUpdateCaseBO.FollowupDescription = dt.FollowupDescription;
                response.objUpdateCaseBO.ClosedDescription = dt.ClosedDescription;
                response.objUpdateCaseBO.CaseStatus = dt.CaseStatus;
                response.objUpdateCaseBO.StageId = dt.StageId;
                response.objUpdateCaseBO.SelectedSubCategoryId = dt.SelectedSubCategoryId;
                response.objUpdateCaseBO.SelectedTypeId = dt.SelectedTypeId;
                response.objUpdateCaseBO.SelectedSubTypeId = dt.SelectedSubTypeId;
                //response.objUpdateCaseBO.ReviewDate = dt.ReviewDate;
            }
            response.objPersonList = GetPersonDetailsByCaseId(caseId);
            return response;
        }
        #endregion

        #region Get person details against a caseId      
        public List<PersonBO> GetPersonDetailsByCaseId(int caseId)
        {
            var personData = (from ccb in context.ASB_CaseCustomerBridge
                              where ccb.CaseId == caseId
                              select new PersonBO
                              {
                                  PersonId = ccb.PersonId,
                                  PersonRole = ccb.PersonRole,
                                  PersonType = ccb.PersonType,
                                  CaseId = ccb.CaseId
                              }
                            );
            return personData.ToList();
        }
        #endregion
       
        #region Associated cases list
        public AssociatedCasesResponseBO GetAssociatedCasesList(AssociatedCasesRequestBO objAssociatedCasesRequestBO)
        {
            int personId = objAssociatedCasesRequestBO.PersonId;
            string personType = objAssociatedCasesRequestBO.PersonType;
            string personRole = objAssociatedCasesRequestBO.PersonRole;
            PaginationBO objPaginationBO = new PaginationBO();
            AssociatedCasesResponseBO objAssociatedCasesResponseBO = new AssociatedCasesResponseBO();
            var data = (from ccb in context.ASB_CaseCustomerBridge
                        join asb in context.ASB_Case on ccb.CaseId equals asb.CaseId

                        into ccbGroup1
                        from c in ccbGroup1.DefaultIfEmpty()
                        join cat in context.ASB_Category on c.Category equals cat.CategoryId
                        join inc in context.ASB_IncidentType on c.IncidentType equals inc.IncidentTypeId
                        join status in context.ASB_CaseStatus on c.CaseStatus equals status.StatusId
                        where ccb.PersonId == personId
                        && ccb.PersonType == personType // if tenant or non tenant then customer else employee. set this value from front end
                        orderby c.CaseId descending
                        select new AssociatedCasesPopupBO
                        {
                            Ref = c.CaseId,
                            PersonType = ccb.PersonType,
                            PersonRole = ccb.PersonRole,
                            IncidentDate = c.IncidentDate.ToString(),
                            Category = cat.Description,
                            IncidentType = inc.Description,
                            Status = status.Description
                        }
                );           
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objAssociatedCasesRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objAssociatedCasesRequestBO.objPaginationBO.PageNumber;
            objPaginationBO.PageSize = objAssociatedCasesRequestBO.objPaginationBO.PageSize;
            data = data.Skip((objAssociatedCasesRequestBO.objPaginationBO.PageNumber - 1) * objAssociatedCasesRequestBO.objPaginationBO.PageSize).Take(objAssociatedCasesRequestBO.objPaginationBO.PageSize);
            //end of pagination


           List< AssociatedCasesPopupBO> lst = new List<AssociatedCasesPopupBO>();
            foreach (var dt in data)
            {
                dt.IncidentDate = string.Format("{0:d}", Convert.ToDateTime(dt.IncidentDate));    //removing time part from incident date           
                lst.Add(dt);
            }
            objAssociatedCasesResponseBO.objAssociatedCasesPopupBO = lst;//assigning case info
            objAssociatedCasesResponseBO.objPaginationBO = objPaginationBO;//assigning pagination info
            return objAssociatedCasesResponseBO;
        }


        #endregion

        #region Add a Persons Against a Case

        public bool AddPersonsAgainstCase(UpdatePersonAgainstCaseRequestBO objAddPersonRequest)
        {
            ASB_CaseCustomerBridge objCaseCustomerBridge = new ASB_CaseCustomerBridge();
            bool isUpdated = false;
            try
            {              
               objCaseCustomerBridge.CaseId = objAddPersonRequest.objPerson.CaseId;
               objCaseCustomerBridge.PersonId = objAddPersonRequest.objPerson.PersonId;
               objCaseCustomerBridge.PersonRole = objAddPersonRequest.objPerson.PersonRole;
               objCaseCustomerBridge.PersonType = objAddPersonRequest.objPerson.PersonType;
               context.ASB_CaseCustomerBridge.Add(objCaseCustomerBridge);
               context.SaveChanges();
               isUpdated = true;
                //start Recording action for case journal  

                
                objActionsBO.CaseId = objCaseCustomerBridge.CaseId;
                objActionsBO.RecordedBy = objAddPersonRequest.RecordedBy;
                objActionsBO.RecordedDate = DateTime.Now;
                if (string.Compare(objCaseCustomerBridge.PersonRole, ApplicationConstants.PersonRoleComplainant, true) == 0)
                {
                    objActionsBO.Action = ApplicationConstants.ComplainantAdded;
                    objActionsBO.ActionNotes = ApplicationConstants.ComplainantAdded;
                }
                else if (string.Compare(objCaseCustomerBridge.PersonRole, ApplicationConstants.PersonRolePerpetrator, true) == 0)
                {
                    objActionsBO.Action = ApplicationConstants.PerpetratorAdded;
                    objActionsBO.ActionNotes = ApplicationConstants.PerpetratorAdded;
                }
                objActionsBO.ReviewDate = DateTime.Now;
                objJournalDAL.AddInJournal(objActionsBO);
                //End Recording action for case journal  
            }
            catch (Exception)
            {               
                isUpdated = false;
            }
            return isUpdated;          
        }
        #endregion

        #region remove a Persons from a Case    
        public bool DeletePersonsFromCase(UpdatePersonAgainstCaseRequestBO objUpdatePersonAgainstCaseRequestBO)
        {
            ASB_CaseCustomerBridge objCaseCustomerBridge = new ASB_CaseCustomerBridge();
            int caseId = objUpdatePersonAgainstCaseRequestBO.objPerson.CaseId;
            int personId= objUpdatePersonAgainstCaseRequestBO.objPerson.PersonId;
            string personRole = objUpdatePersonAgainstCaseRequestBO.objPerson.PersonRole;
            string personType = objUpdatePersonAgainstCaseRequestBO.objPerson.PersonType;
            bool isUpdated = false;
            try
            {
                var data = (from ccb in context.ASB_CaseCustomerBridge
                            where ccb.CaseId == caseId 
                            && ccb.PersonId == personId
                            && ccb.PersonRole == personRole
                            && ccb.PersonType == personType
                            select ccb).FirstOrDefault();
                context.ASB_CaseCustomerBridge.Remove(data);
                context.SaveChanges();
                isUpdated = true;
                //start Recording action for case journal  


                objActionsBO.CaseId = caseId;
                objActionsBO.RecordedBy = objUpdatePersonAgainstCaseRequestBO.RecordedBy;
                objActionsBO.RecordedDate = DateTime.Now;
                if (string.Compare(personRole, ApplicationConstants.PersonRoleComplainant, true) == 0)
                {
                    objActionsBO.Action = ApplicationConstants.ComplainantDeleted;
                    objActionsBO.ActionNotes = ApplicationConstants.ComplainantDeleted;
                }
                else if (string.Compare(personRole, ApplicationConstants.PersonRolePerpetrator, true) == 0)
                {
                    objActionsBO.Action = ApplicationConstants.PerpetratorDeleted;
                    objActionsBO.ActionNotes = ApplicationConstants.PerpetratorDeleted;
                }
                objActionsBO.ReviewDate = DateTime.Now;
                objJournalDAL.AddInJournal(objActionsBO);
                //End Recording action for case journal  
            }
            catch (Exception)
            {
                isUpdated = false;
            }
            return isUpdated;
            
        }


        #endregion

        #region Associated cases list with joint tenancy
        public AssociatedCasesResponseBO GetJointTenancyCasesList(int customerId)
        {           
            List<int> personIds = new List<int>();
            personIds = (from tList in context.C_GetJointTenancyList(customerId)
                         select tList.Value
                            ).ToList();
            AssociatedCasesResponseBO objAssociatedCasesResponseBO = new AssociatedCasesResponseBO();
            objAssociatedCasesResponseBO.objAssociatedCasesPopupBO = new List<AssociatedCasesPopupBO>();
            //iterate through list and find cases of all customers
            if (personIds != null && personIds.Count > 0)
            {
                foreach (var id in personIds)
                {
                    var data = (from ccb in context.ASB_CaseCustomerBridge
                                join asb in context.ASB_Case on ccb.CaseId equals asb.CaseId

                                into ccbGroup1
                                from c in ccbGroup1.DefaultIfEmpty()
                                join cat in context.ASB_Category on c.Category equals cat.CategoryId
                                join inc in context.ASB_IncidentType on c.IncidentType equals inc.IncidentTypeId
                                join status in context.ASB_CaseStatus on c.CaseStatus equals status.StatusId
                                where ccb.PersonId == id
                                && string.Compare(ccb.PersonType, ApplicationConstants.PersonTypeCustomer, true) == 0
                                && string.Compare(status.Description, ApplicationConstants.CaseStatusOpen, true) == 0

                                orderby c.CaseId descending
                                select new AssociatedCasesPopupBO
                                {
                                    Ref = c.CaseId,
                                    PersonType = ccb.PersonType,
                                    PersonRole = ccb.PersonRole,
                                    IncidentDate = c.IncidentDate.ToString(),
                                    Category = cat.Description,
                                    IncidentType = inc.Description,
                                    Status = status.Description
                                }
                   );
                    foreach (var dt in data)
                    {
                        dt.IncidentDate = string.Format("{0:d}", Convert.ToDateTime(dt.IncidentDate));  //removing time part from incident date                                                                                                               
                        objAssociatedCasesResponseBO.objAssociatedCasesPopupBO.Add(dt);
                    }
                }
            }
            else
            {
                var data = (from ccb in context.ASB_CaseCustomerBridge
                            join asb in context.ASB_Case on ccb.CaseId equals asb.CaseId

                            into ccbGroup1
                            from c in ccbGroup1.DefaultIfEmpty()
                            join cat in context.ASB_Category on c.Category equals cat.CategoryId
                            join inc in context.ASB_IncidentType on c.IncidentType equals inc.IncidentTypeId
                            join status in context.ASB_CaseStatus on c.CaseStatus equals status.StatusId
                            where ccb.PersonId == customerId
                            && string.Compare(ccb.PersonType, ApplicationConstants.PersonTypeCustomer, true) == 0
                            && string.Compare(status.Description, ApplicationConstants.CaseStatusOpen, true) == 0

                            orderby c.CaseId descending
                            select new AssociatedCasesPopupBO
                            {
                                Ref = c.CaseId,
                                PersonType = ccb.PersonType,
                                PersonRole = ccb.PersonRole,
                                IncidentDate = c.IncidentDate.ToString(),
                                Category = cat.Description,
                                IncidentType = inc.Description,
                                Status = status.Description
                            }
                   );
                foreach (var dt in data)
                {
                    dt.IncidentDate = string.Format("{0:d}", Convert.ToDateTime(dt.IncidentDate));  //removing time part from incident date                                                                                                               
                    objAssociatedCasesResponseBO.objAssociatedCasesPopupBO.Add(dt);
                }
            }           

            return objAssociatedCasesResponseBO;
        }


        #endregion








    }

}
