﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_DAL.DatabaseEntities;
using ASB_BO;
using System.Data.Entity;
using System.Globalization;
using System.Threading;
using System.Configuration;

namespace ASB_DAL
{
    public class JournalDAL : ABaseDAL, IJournalRepoDAL
    {
        #region Inserting data in journal
        public bool AddInJournal(ActionsBO objActionsBO)
        {
            bool isAdded = false;
            try
            {
                var data = context.ASB_CaseActionType.Where(x => x.Description == objActionsBO.Action).Select(y => y.ActionTypeId);
                int actionId = int.Parse(data.FirstOrDefault().ToString());
                ASB_CaseAction objCaseAction = new ASB_CaseAction();
                objCaseAction.ActionType = actionId;
                objCaseAction.DateRecorded = DateTime.Now;
                objCaseAction.RecoredBy = objActionsBO.RecordedBy;
                objCaseAction.CaseId = objActionsBO.CaseId;
                objCaseAction.Notes = objActionsBO.ActionNotes == null ? string.Empty : objActionsBO.ActionNotes;
                objCaseAction.FileName = objActionsBO.FileName;
                objCaseAction.FilePath = objActionsBO.FilePath;
                objCaseAction.StageId = objActionsBO.Stage;
                objCaseAction.SubCategoryId = objActionsBO.SubCategory;
                objCaseAction.TypeId = objActionsBO.Types;
                objCaseAction.SubTypeId = objActionsBO.Types;
                objCaseAction.ReviewDate = objActionsBO.ReviewDate;
                context.ASB_CaseAction.Add(objCaseAction);
                context.SaveChanges();
                //start follow up and closed date action handling
                if (string.Compare(objActionsBO.Action, ApplicationConstants.FollowUp, true) == 0)
                {
                    var followUpAction = (from followUp in context.ASB_Case
                                          where followUp.CaseId == objActionsBO.CaseId
                                          select followUp).FirstOrDefault();

                    followUpAction.NextFollowupDate = objActionsBO.FollowUpDate;
                    followUpAction.FollowupDescription = objActionsBO.ActionNotes;
                    followUpAction.ReviewDate = objActionsBO.ReviewDate;
                    context.SaveChanges();
                }
                else if (string.Compare(objActionsBO.Action, ApplicationConstants.CloseCase, true) == 0)
                {
                    var closeAction = (from close in context.ASB_Case
                                       where close.CaseId == objActionsBO.CaseId
                                       select close).FirstOrDefault();
                   
                    closeAction.ClosedDate = objActionsBO.ClosedDate;
                    closeAction.ClosedDescription = objActionsBO.ActionNotes;
                    closeAction.ReviewDate = objActionsBO.ReviewDate;

                    var closeStatus = (from status in context.ASB_CaseStatus
                                       where status.StatusId==objActionsBO.ClosingType
                                       select status).FirstOrDefault();
                    closeAction.CaseStatus = closeStatus.StatusId;

                    context.SaveChanges();
                }
                else if (string.Compare(objActionsBO.Action, ApplicationConstants.Stages, true) == 0)
                {
                    var stagesAction = (from close in context.ASB_Case
                                       where close.CaseId == objActionsBO.CaseId
                                       select close).FirstOrDefault();

                    stagesAction.StageId = objActionsBO.Stage;
                    stagesAction.SubCategoryId = objActionsBO.SubCategory;
                    stagesAction.TypeId = objActionsBO.Types;
                    stagesAction.SubTypeId = objActionsBO.SubType;
                    stagesAction.ReviewDate = objActionsBO.ReviewDate;
                    context.SaveChanges();
                }
                else
                {
                    var otherAction = (from close in context.ASB_Case
                                        where close.CaseId == objActionsBO.CaseId
                                        select close).FirstOrDefault();
                    otherAction.ReviewDate = objActionsBO.ReviewDate;
                    context.SaveChanges();
                }
                isAdded = true;

            }
            catch (Exception e)
            {
                isAdded = false;
            }
            return isAdded;

        }
        #endregion

        #region Search data from journal
        public JournalResponseBO GetJournalDataByCaseId(JournalRequestBO objJournalRequestBO)
        {
            int caseId = objJournalRequestBO.CaseId;
            JournalResponseBO objJournalResponseBO = new JournalResponseBO();
            var data = (from actn in context.ASB_CaseAction
                        join emp in context.E__EMPLOYEE on actn.RecoredBy equals emp.EMPLOYEEID
                        into group1
                        from e in group1.DefaultIfEmpty()
                        join type in context.ASB_CaseActionType on actn.ActionType equals type.ActionTypeId
                        where actn.CaseId == caseId
                        select new JournalBO
                        {
                            RecordedDate = actn.DateRecorded,
                            RecordedBy = string.Concat(e.FIRSTNAME, " ", e.LASTNAME),
                            Action = type.Description,
                            ActionNotes = actn.Notes,
                            CaseId = actn.CaseId,
                            FilePath = actn.FilePath,
                            FileName = actn.FileName,
                            JournalId = actn.ActionId,
                            StageId = actn.StageId,
                            stage = (from lookup in context.ASB_StagesLookup
                                     where lookup.StagesLookupId==actn.StageId
                                     select lookup.Headline).FirstOrDefault(),
                            SubCategoryId=actn.SubCategoryId,
                            SubCategory= (from lookup in context.ASB_StagesLookup
                                          where lookup.StagesLookupId == actn.SubCategoryId
                                          select lookup.Headline).FirstOrDefault(),
                            ReviewDate=actn.ReviewDate

                        }
                        );

            data = data.OrderBy(x => x.CaseId);
            //Sort Journals Based on DateTime
            data = data.OrderByDescending(x => x.JournalId);
            //start of pagination
            PaginationBO objPaginationBO = new PaginationBO();
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objJournalRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objJournalRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objJournalRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objJournalRequestBO.objPaginationBO.PageNumber - 1) * objJournalRequestBO.objPaginationBO.PageSize).Take(objJournalRequestBO.objPaginationBO.PageSize);
            //End of pagination


            //start removing time part from incident date   
            List<JournalBO> responseLst = new List<JournalBO>();
            foreach (var dt in data)
            {
                dt.RecordedDate = dt.RecordedDate.Date;
                responseLst.Add(dt);
            }
            //end removing time part from incident date

            //start creating response
            objJournalResponseBO.objJournalBO = data.ToList();
            objJournalResponseBO.objPaginationBO = objPaginationBO;
            //end creating response

            return objJournalResponseBO;
        }
        #endregion

    }
}
