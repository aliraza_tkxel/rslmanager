﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL.DatabaseEntities;
namespace ASB_DAL
{
    public class ContactDAL : ABaseDAL, IContactRepoDAL
    {
        JournalDAL objJournalDAL = new JournalDAL();
        ActionsBO objActionsBO = new ActionsBO();
        #region Add a contact against a case      
        public ContactResponseBO AddContact(AddContactRequestBO objAddContactRequestBO)
        {
            ContactResponseBO objContactResponseBO = new ContactResponseBO();
            PaginationBO objPaginationBO = new PaginationBO();
            ASB_AssociatedContacts objContact = new ASB_AssociatedContacts();
            objContact.Name = objAddContactRequestBO.objContactBO.ContactName;
            objContact.Telephone = objAddContactRequestBO.objContactBO.Telephone;
            objContact.Organization = objAddContactRequestBO.objContactBO.Organization;
            objContact.CaseId = objAddContactRequestBO.objContactBO.CaseId;
            objContact.IsActive = objAddContactRequestBO.objContactBO.IsActive;
            context.ASB_AssociatedContacts.Add(objContact);
            context.SaveChanges();

            //start Recording action for case journal   
            objActionsBO.Action = ApplicationConstants.AssociatedContactAdded;
            objActionsBO.ActionNotes = ApplicationConstants.AssociatedContactAdded;
            objActionsBO.CaseId = objContact.CaseId;
            objActionsBO.RecordedBy = objAddContactRequestBO.RecordedBy;
            objActionsBO.RecordedDate = DateTime.Now;
            objActionsBO.ReviewDate = DateTime.Now;

            objJournalDAL.AddInJournal(objActionsBO);
          
            //End Recording action for case journal 


            //start creating response
            var data = (from contact in context.ASB_AssociatedContacts
                        where contact.IsActive == true
                        && contact.CaseId == objAddContactRequestBO.objContactBO.CaseId
                        orderby contact.ContactId descending
                        select new ContactBO
                        {
                            ContactId = contact.ContactId,
                            ContactName = contact.Name,
                            Organization = contact.Organization,
                            Telephone = contact.Telephone,
                            IsActive = contact.IsActive,
                            CaseId = contact.CaseId                           
                        }
                        );


            //start of pagination
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objAddContactRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objAddContactRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objAddContactRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objAddContactRequestBO.objPaginationBO.PageNumber - 1) * objAddContactRequestBO.objPaginationBO.PageSize).Take(objAddContactRequestBO.objPaginationBO.PageSize);
            //End of pagination

            //start of creating file list and pagination obj
            objContactResponseBO.objContactBO = data.ToList();
            objContactResponseBO.objPaginationBO = objPaginationBO;
            //end of creating file list and pagination obj


            //end creating response
            return objContactResponseBO;
        }
        #endregion

        #region Remove a contact from a case
        public ContactResponseBO RemoveContact(RemoveContactRequestBO objRemoveContactRequestBO)
        {
            int caseId = objRemoveContactRequestBO.CaseId;
            int contactId = objRemoveContactRequestBO.ContactId;
            PaginationBO objPaginationBO = new PaginationBO();
            ContactResponseBO objContactResponseBO = new ContactResponseBO();
            ASB_AssociatedContacts objContact = context.ASB_AssociatedContacts.FirstOrDefault(cnt => cnt.IsActive == true && cnt.CaseId.Equals(caseId) && cnt.ContactId.Equals(contactId));
            objContact.IsActive = false;
            context.SaveChanges();

            var data = (from cont in context.ASB_AssociatedContacts
                        where cont.IsActive == true
                        && cont.CaseId == caseId
                        orderby cont.ContactId descending
                        select new ContactBO
                        {
                            ContactId = cont.ContactId,
                            ContactName = cont.Name,
                            Organization = cont.Organization,
                            Telephone = cont.Telephone,
                            IsActive = cont.IsActive,
                            CaseId = cont.CaseId
                        }
                     );


            //start Recording action for case journal   
            objActionsBO.Action = ApplicationConstants.AssociatedContactDeleted;
            objActionsBO.ActionNotes = ApplicationConstants.AssociatedContactDeleted;
            objActionsBO.CaseId = objContact.CaseId;
            objActionsBO.RecordedBy = objRemoveContactRequestBO.RemovedBy;
            objActionsBO.RecordedDate = DateTime.Now;

            objJournalDAL.AddInJournal(objActionsBO);
            //End Recording action for case journal

            //start of pagination
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objRemoveContactRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objRemoveContactRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objRemoveContactRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objRemoveContactRequestBO.objPaginationBO.PageNumber - 1) * objRemoveContactRequestBO.objPaginationBO.PageSize).Take(objRemoveContactRequestBO.objPaginationBO.PageSize);
            //End of pagination

            //start of creating file list and pagination obj
            objContactResponseBO.objContactBO = data.ToList();
            objContactResponseBO.objPaginationBO = objPaginationBO;
            //end of creating file list and pagination obj

            //   end creating response
            return objContactResponseBO;
        }
        #endregion

        #region Search all contact against a case        
        public ContactResponseBO SearchAllContact(GetAllContactRequestBO objGetAllContactRequestBO)
        {
            PaginationBO objPaginationBO = new PaginationBO();
            ContactResponseBO objContactResponseBO = new ContactResponseBO();
            int caseId = objGetAllContactRequestBO.caseId;
            var data = (from contact in context.ASB_AssociatedContacts
                        where contact.IsActive == true
                        && contact.CaseId == caseId                   
                        orderby contact.ContactId descending
                        select new ContactBO
                        {
                            ContactId = contact.ContactId,
                            ContactName = contact.Name,
                            Organization = contact.Organization,
                            Telephone = contact.Telephone,
                            IsActive = contact.IsActive,
                            CaseId = contact.CaseId
                        }
                     );

            //start of pagination
            int totalPages = data.Count();
            objPaginationBO.TotalRows = totalPages;
            objPaginationBO.TotalPages = (int)Math.Ceiling((double)totalPages / objGetAllContactRequestBO.objPaginationBO.PageSize);
            objPaginationBO.PageNumber = objGetAllContactRequestBO.objPaginationBO.PageNumber;//set page no to requested page no.
            objPaginationBO.PageSize = objGetAllContactRequestBO.objPaginationBO.PageSize;//set page size to requested page size.
            data = data.Skip((objGetAllContactRequestBO.objPaginationBO.PageNumber - 1) * objGetAllContactRequestBO.objPaginationBO.PageSize).Take(objGetAllContactRequestBO.objPaginationBO.PageSize);
            //End of pagination

            //start of creating file list and pagination obj
            objContactResponseBO.objContactBO = data.ToList();
            objContactResponseBO.objPaginationBO = objPaginationBO;
            //  end of creating file list and pagination obj

            //   end creating response
            return objContactResponseBO;
        }
        #endregion


    }
}
