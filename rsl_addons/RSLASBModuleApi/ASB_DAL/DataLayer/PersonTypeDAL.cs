﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
namespace ASB_DAL
{
   public class PersonTypeDAL: ABaseDAL, ILookupRepoDAL
    {
        #region Get Person type DropDown while adding a person
        List<DropDownBO> ILookupRepoDAL.GetData()
        {
            var data = (from type in context.C_CUSTOMERTYPE
                        where type.DESCRIPTION != ApplicationConstants.PersonTypeTenant
                        select new DropDownBO
                        {
                            key = type.CUSTOMERTYPEID,
                            value = type.DESCRIPTION
                        }
                        );

            return data.ToList();
        }
        #endregion
    }
}
