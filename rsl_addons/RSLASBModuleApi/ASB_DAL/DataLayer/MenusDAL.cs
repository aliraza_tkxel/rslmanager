﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASB_BO;
using ASB_DAL.DatabaseEntities;
namespace ASB_DAL
{
    public class MenusDAL : ABaseDAL, IMenusRepoDAL
    {
        #region Displaying Menu on top bar
        public MenusResponseBO GetMenus(MenuRequestBO objMenuRequestBO)
        {
            //Althought a store procedure exist which did same,but we need to skip customer from menu as per requirement document. so for that purpose, this function is created
           
            //start getting moduleId from module name
            int moduleId = int.Parse((from menu in context.AC_MENUS where string.Compare(menu.DESCRIPTION, objMenuRequestBO.ModuleName, true)==0
                                      select menu.MODULEID).FirstOrDefault().ToString());
            //end getting moduleId from module name
      

            //start filling menus list
            var menus = (from emp in context.E__EMPLOYEE 
                        join access in context.AC_MENUS_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                        join menu in context.AC_MENUS on access.MenuId equals menu.MENUID
                        join module in context.AC_MODULES on menu.MODULEID equals module.MODULEID
                        where menu.ACTIVE == 1
                        && emp.EMPLOYEEID == objMenuRequestBO.UserId
                        && module.MODULEID == moduleId
                        && string.Compare(menu.DESCRIPTION, objMenuRequestBO.ModuleName, true)==1
                        select new MenusBO
                        {
                            MenuId = menu.MENUID,
                            MenuName = menu.DESCRIPTION
                           
                        }
                        );
            //end filling menus list


            //start filling sub menus list
            var MenusAndSubMenuList = menus.ToList();
            foreach (var item in MenusAndSubMenuList)
            {
                item.objLstSubMenus = GetSubMenus(item.MenuId, objMenuRequestBO.UserId);
            }
            //end filling sub menus list

            //start creating response, cosisting of menu,subMenus and Customer module menus
            MenusResponseBO objMenusResponseBO = new MenusResponseBO();
            objMenusResponseBO.objMenusBO = MenusAndSubMenuList.ToList();
            objMenusResponseBO.objLstCustomerModuleMenus = GetSideMenus(objMenuRequestBO.UserId);//get customer module menus
            return objMenusResponseBO;
        }
        #endregion

        #region Get Sub Menus on top bar
        public List<SubMenusBO> GetSubMenus(int menuId,int userId)
        {
            var data = (from emp in context.E__EMPLOYEE
                        join access in context.AC_PAGES_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                        join page in context.AC_PAGES on access.PageId equals page.PAGEID
                        join menu in context.AC_MENUS on page.MENUID equals menu.MENUID
                        join module in context.AC_MODULES on menu.MODULEID equals module.MODULEID
                        where page.ACTIVE ==1
                        && page.LINK ==1
                        && emp.EMPLOYEEID == userId
                        && menu.MENUID == menuId
                        select new SubMenusBO
                        {
                            SubMenuName =page.DESCRIPTION,
                            MenuId = menu.MENUID,
                            Path = string.Concat(module.DIRECTORY,"/",page.PAGE)
                        }
                        );
            var sortedList = data.OrderBy(x => x.SubMenuName).ToList();
            return sortedList;
            //return data.ToList();
        }
        #endregion

        #region Get Customer module Menus
        public List<CustomerModuleMenusBO> GetSideMenus(int userId)
        {
            var data = (from sideMenus in context.P_GetRSLModulesList(userId,1)
                        select new CustomerModuleMenusBO
                        {
                            ModuleId = sideMenus.MODULEID,
                            ModuleName = sideMenus.DESCRIPTION,
                            Url = sideMenus.THEPATH
                        }
                );         
            return data.ToList();         
        }
        #endregion

    }
}

