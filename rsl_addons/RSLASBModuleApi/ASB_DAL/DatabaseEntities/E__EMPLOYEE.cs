//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASB_DAL.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class E__EMPLOYEE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public E__EMPLOYEE()
        {
            this.ASB_Case = new HashSet<ASB_Case>();
            this.E_CONTACT = new HashSet<E_CONTACT>();
            this.ASB_CaseAction = new HashSet<ASB_CaseAction>();
            this.ASB_Case1 = new HashSet<ASB_Case>();
        }
    
        public int EMPLOYEEID { get; set; }
        public Nullable<System.DateTime> CREATIONDATE { get; set; }
        public Nullable<int> TITLE { get; set; }
        public string FIRSTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string LASTNAME { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string GENDER { get; set; }
        public Nullable<int> MARITALSTATUS { get; set; }
        public Nullable<int> ETHNICITY { get; set; }
        public string ETHNICITYOTHER { get; set; }
        public Nullable<int> RELIGION { get; set; }
        public string RELIGIONOTHER { get; set; }
        public Nullable<int> SEXUALORIENTATION { get; set; }
        public string SEXUALORIENTATIONOTHER { get; set; }
        public string DISABILITY { get; set; }
        public string DISABILITYOTHER { get; set; }
        public string BANK { get; set; }
        public string SORTCODE { get; set; }
        public string ACCOUNTNUMBER { get; set; }
        public string ACCOUNTNAME { get; set; }
        public string ROLLNUMBER { get; set; }
        public string BANK2 { get; set; }
        public string SORTCODE2ND { get; set; }
        public string ACCOUNTNUMBER2 { get; set; }
        public string ACCOUNTNAME2 { get; set; }
        public string ROLLNUMBER2 { get; set; }
        public Nullable<int> ORGID { get; set; }
        public string IMAGEPATH { get; set; }
        public string ROLEPATH { get; set; }
        public string PROFILE { get; set; }
        public Nullable<System.DateTime> LASTLOGGEDIN { get; set; }
        public string SIGNATUREPATH { get; set; }
        public Nullable<int> JobRoleTeamId { get; set; }
        public Nullable<System.DateTime> LASTACTIONTIME { get; set; }
        public Nullable<int> LASTACTIONUSER { get; set; }
        public string EmploymentContracts { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASB_Case> ASB_Case { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<E_CONTACT> E_CONTACT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASB_CaseAction> ASB_CaseAction { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ASB_Case> ASB_Case1 { get; set; }
    }
}
