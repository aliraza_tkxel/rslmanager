﻿Imports System.IO

Public Class PropertyDal : Inherits BaseDAL
    Public Function getCp12Data(ByVal heatingId As Integer, ByVal journalId As Integer)
        Dim appliancesDt As DataTable = New DataTable()
        Dim inspectionDetailsDt As DataTable = New DataTable()
        Dim detectorDt As DataTable = New DataTable()
        Dim defectsDt As DataTable = New DataTable()
        Dim pipeWorkDt As DataTable = New DataTable()
        Dim employeeSignatureDt As DataTable = New DataTable()
        Dim cp12Ds As DataSet = New DataSet()
        Dim customerDt As DataTable = New DataTable()
        Dim plgsrDt As DataTable = New DataTable()
        Dim generalCommentDt As DataTable = New DataTable()
        Dim applianceTestedDt As DataTable = New DataTable()
        Dim jsgDt As DataTable = New DataTable()

        cp12Ds.Tables.Add(appliancesDt)
        cp12Ds.Tables.Add(inspectionDetailsDt)
        cp12Ds.Tables.Add(detectorDt)
        cp12Ds.Tables.Add(defectsDt)
        cp12Ds.Tables.Add(pipeWorkDt)
        cp12Ds.Tables.Add(employeeSignatureDt)
        cp12Ds.Tables.Add(customerDt)
        cp12Ds.Tables.Add(plgsrDt)
        cp12Ds.Tables.Add(generalCommentDt)
        cp12Ds.Tables.Add(applianceTestedDt)
        cp12Ds.Tables.Add(jsgDt)

        Dim parameterList2 As ParameterList = New ParameterList()

        Dim heatingIdParam As ParameterBO = New ParameterBO("heatingId", heatingId, DbType.Int32)
        parameterList2.Add(heatingIdParam)

        Dim journalIdParam As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
        parameterList2.Add(journalIdParam)


        Dim lResultDataReader2 As IDataReader = MyBase.SelectRecord(parameterList2, "AS_CP12DocumentData")
        cp12Ds.Load(lResultDataReader2, LoadOption.OverwriteChanges, appliancesDt, inspectionDetailsDt, detectorDt, defectsDt, pipeWorkDt, employeeSignatureDt, customerDt, plgsrDt, generalCommentDt, applianceTestedDt, jsgDt)
        Return cp12Ds
    End Function

#Region "Get Property Search Result"
    ''' <summary>
    ''' Get Property Search Result
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <param name="searchText"></param>
    ''' <remarks></remarks>
    Public Sub getPropertySearchResult(ByRef resultDataSet As DataSet, ByRef searchText As String)

        Dim parametersList As ParameterList = New ParameterList()

        Dim dtPropertyResult As DataTable = New DataTable()
        dtPropertyResult.TableName = "PropertySearchResult"
        resultDataSet.Tables.Add(dtPropertyResult)

        Dim dtSchemeResult As DataTable = New DataTable()
        dtSchemeResult.TableName = "SchemeSearchResult"
        resultDataSet.Tables.Add(dtSchemeResult)

        Dim dtBlockResult As DataTable = New DataTable()
        dtBlockResult.TableName = "BlockSearchResult"
        resultDataSet.Tables.Add(dtBlockResult)

        Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
        parametersList.Add(searchTextParam)

        Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, "PLANNED_GetSearchProperty")
        resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult, dtSchemeResult, dtBlockResult)

    End Sub

#End Region

    Public Sub getHeatingIds(ByVal type As String, ByVal selectedId As String, ByRef cp12JournalIdsDataSet As DataSet)
        Dim parametersList As ParameterList = New ParameterList()

        Dim heatingIdsDt As DataTable = New DataTable()
        cp12JournalIdsDataSet.Tables.Add(heatingIdsDt)

        Dim propertyIdParam As ParameterBO
        Dim schemeIdParam As ParameterBO
        Dim blockIdParam As ParameterBO

        If (type = "Property") Then
            propertyIdParam = New ParameterBO("propertyId", selectedId, DbType.String)
            parametersList.Add(propertyIdParam)

            schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
            parametersList.Add(schemeIdParam)

            blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
            parametersList.Add(blockIdParam)
        ElseIf (type = "Scheme") Then
            propertyIdParam = New ParameterBO("propertyId", DBNull.Value, DbType.String)
            parametersList.Add(propertyIdParam)

            schemeIdParam = New ParameterBO("schemeId", Convert.ToInt32(selectedId), DbType.Int32)
            parametersList.Add(schemeIdParam)

            blockIdParam = New ParameterBO("blockId", DBNull.Value, DbType.Int32)
            parametersList.Add(blockIdParam)
        Else
            propertyIdParam = New ParameterBO("propertyId", DBNull.Value, DbType.String)
            parametersList.Add(propertyIdParam)

            schemeIdParam = New ParameterBO("schemeId", DBNull.Value, DbType.Int32)
            parametersList.Add(schemeIdParam)

            blockIdParam = New ParameterBO("blockId", Convert.ToInt32(selectedId), DbType.Int32)
            parametersList.Add(blockIdParam)
        End If


        'get the journal ids against the property having cp12issue status
        Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, "AS_CP12HeatingIds")
        cp12JournalIdsDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, heatingIdsDt)
    End Sub

    Public Function saveCp12Document(ByVal requestBo As GenerateCP12RequestBO, ByVal cp12InByteForm As Byte(), ByVal heatingId As Int32)
        Dim parametersList As ParameterList = New ParameterList()
        Dim outParametersList As ParameterList = New ParameterList()

        Dim heatingIdParam As ParameterBO = New ParameterBO("heatingId", heatingId, DbType.Int32)
        parametersList.Add(heatingIdParam)

        Dim journalIdParam As ParameterBO = New ParameterBO("journalId", Convert.ToInt32(requestBo.JournalId), DbType.Int32)
        parametersList.Add(journalIdParam)

        Dim cp12DocParam As ParameterBO = New ParameterBO("cp12Doc", cp12InByteForm, DbType.Binary)
        parametersList.Add(cp12DocParam)

        Dim documentNamedParam As ParameterBO = New ParameterBO("documentName", requestBo.Cp12DocumentName, DbType.String)
        parametersList.Add(documentNamedParam)

        Dim updatedByParam As ParameterBO = New ParameterBO("updatedBy", Convert.ToInt32(requestBo.UpdatedBy), DbType.Int32)
        parametersList.Add(updatedByParam)

        outParametersList = MyBase.SelectRecord(parametersList, outParametersList, "AS_Cp12SaveDocument")

        Return True
    End Function


End Class
