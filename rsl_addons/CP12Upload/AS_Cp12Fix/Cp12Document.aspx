﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cp12Document.aspx.vb"
    Inherits="AS_Cp12Fix.Cp12Document" %>

<%@ Import Namespace="System.Data" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style>
        body
        {
            font-family: Arial;
        }
        
        .table_grid
        {
            width: 98.79%;
            border: 1px solid #bec0bf;
            border-collapse: collapse;
        }
        .table_grid td
        {
            border: 1px solid #bec0bf;
        }
        
        .table_grid th
        {
            text-align: left;
            height: 30px;
            background-color: #bec0bf;
        }
        .table_grid th p
        {
            font-weight: bold;
            font-size: 16px;
            margin: 0 0 0 5px;
        }
        .table_grid_container
        {
            width: 1040px;
            margin-bottom: 15px;
            margin-top: 15px;
        }
        .style1
        {
            width: 52px;
        }
        .style2
        {
            width: 53px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style='margin-top: 20px; margin-right: 60px; margin-left: 60px; text-align: center;
        font-size: 14px; width: 1040px;'>
        <div style='float: left; width: 1040px; margin-bottom: 10px;'>
            <div style='float: left; height: 150px; width: 120px;'>
                <%--<img src='Images/leftImg.png' height='150' width='120' alt='header Left Image' />--%>
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/ReportLeftImage.png" />
            </div>
            <div style='float: left; width: 729px; margin: 0 0 0 15px;'>
                <p style="font-size: 26px; font-weight: bold;">
                    Landlords Gas Safety Records</p>
                <p style="margin-top: -25px;">
                    This inspection is for gas safety purposes only to comply with the Gas Safety (Installation
                    and Use) Regulations. Flues have been inspected visually and checked for satisfactory
                    evacuation of products of combustion. A detailed internal inspection of the flue
                    integrity, construction and lining has not been carried out.
                    <br />
                    To confirm the validity of the gas operative please contact Gas Safe on 0800 408
                    5500 or
                    <label style="color: #0000FF; text-decoration: underline;">
                        www.gassaferegister.co.uk</label>
                </p>
            </div>
            <div style='float: left; height: 150px; width: 120px;'>
                <%--<img src='Images/rightImg.png' height='150' width='120' alt='header Right Image' />--%>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ReportRightImage.png" />
            </div>
        </div>
        <div style='clear: both;'>
        </div>
        <div style='text-align: left; width: 1038px; margin-bottom: 15px;'>
            <div style='float: left; height: 220px; width: 320px; border: 1px solid #999999;'>
                <h3 style="margin-left: 15px;">
                    <label id="lblInfoType" runat="server">Tenant</label></h3>
                <p style="margin-left: 15px; margin-top: -15px;">
                    <label id="lblTenantName" runat="server">
                        Mr.Craig</label>
                    <br />
                    <label id="lblHouseNumber" runat="server">
                        57 The Cedars</label>
                    <br />
                    <label id="lblAddress2" runat="server">
                        Albemarle Road</label>
                    <br />
                    <label id="lblCounty" runat="server">
                        Norfolk</label>
                    <br />
                    <label id="lblPostCode" runat="server">
                        NR2 2EF</label></p>
            </div>
            <div style='float: left; height: 220px; width: 320px; border: 1px solid #999999;
                margin-left: 30px;'>
                <h3 style="margin-left: 15px;">
                    Registered Business Details Gas Safe Reg No: 531012</h3>
                <p style="margin-left: 15px; margin-top: -15px;">
                    Broadland Housing Group
                    <br />
                    Norwich City FC<br />
                    Jarrold Stand
                    <br />
                    Carrow Road
                    <br />
                    Norwich
                    <br />
                    NR1 1HU
                    <br />
                    Tel No: 01603 750200</p>
            </div>
            <div style='float: left; height: 220px; width: 320px; border: 1px solid #999999;
                margin-left: 30px;'>
                <h3 style="margin-left: 15px;">
                    Landlord
                </h3>
                <p style="margin-left: 15px; margin-top: -15px;">
                    Broadland Housing Group
                    <br />
                    Norwich City FC
                    <br />
                    Jarrold Stand
                    <br />
                    Carrow Road
                    <br />
                    Norwich
                    <br />
                    NR1 1HU
                    <br />
                    Tel No: 01603 750200</p>
            </div>
        </div>
        <div style='clear: both;'>
        </div>
        <%--Appliance Detail Table -- Start--%>
        <div class="table_grid_container">
            <table class="table_grid" id="tblApplianceDetail">
                <tr>
                    <th colspan="7">
                        <p>
                            Appliance Detail</p>
                    </th>
                </tr>
                <tr>
                    <td>
                        Ref.
                    </td>
                    <td>
                        Location
                    </td>
                    <td>
                        Appliance Type
                    </td>
                    <td>
                        Make
                    </td>
                    <td>
                        Model
                    </td>
                    <td>
                        Flue Type OF/RS/FL
                    </td>
                    <td>
                        Landlord&#39;s Appliance
                    </td>
                </tr>
                <%
                If cp12Ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In cp12Ds.Tables(0).Rows()
                %>
                <tr>
                    <td>
                        <%=dr.Item("PropertyApplianceId")%>
                    </td>
                    <td>
                        <%=dr.Item("Location") %>
                    </td>
                    <td>
                        <%=dr.Item("ApplianceType")%>
                    </td>
                    <td>
                        <%=dr.Item("Manufacturer")%>
                    </td>
                    <td>
                        <%=dr.Item("Model")%>
                    </td>
                    <td>
                        <%=dr.Item("FlueType")%>
                    </td>
                    <td>
                        <%=dr.Item("isLandlordAppliance")%>
                    </td>
                </tr>
                <% Next%>
                <% 
                Else
                %>
                <tr>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                </tr>
                <% 
                End If
                %>
            </table>
        </div>
        <%--Appliance Detail Table -- End--%>
        <div style='clear: both;'>
        </div>
        <%--Inspection Detail Table -- Start --%>
        <div class="table_grid_container">
            <table class="table_grid" id="Table1">
                <tr>
                    <th colspan="12">
                        <p>
                            Inspection Detail</p>
                    </th>
                </tr>
                <tr>
                    <td class="style1">
                        Ref.
                    </td>
                    <td>
                        Appliance Inspected
                    </td>
                    <td>
                        Combustion analyser reading
                    </td>
                    <td>
                        Operating pressure in MBAR or Heat Input kW/H or Btu/h
                    </td>
                    <td>
                        Safety device correct operation
                    </td>
                    <td>
                        Ventilation provision satisfactory
                    </td>
                    <td>
                        Visual Condition of flue
                    </td>
                    <td>
                        Termination satisfactory
                    </td>
                    <td>
                        Flue performance check
                    </td>
                    <td>
                        Appliance serviced
                    </td>
                    <td>
                        Appliance safe to use
                    </td>
                     <td>
                        Spillage Test
                    </td>
                </tr>
                <%
                    If cp12Ds.Tables(1).Rows.Count > 0 Then
                        Dim inspectionCount As Integer = 0
                        Dim spilageTest As String = "N/A"
                        For Each dr As DataRow In cp12Ds.Tables(1).Rows()
                            inspectionCount = inspectionCount + 1
                            If IsDBNull(dr.Item("SpillageTest")) Then
                                spilageTest = "N/A"
                            Else
                                spilageTest = dr.Item("SpillageTest")
                            End If
                           
                %>
                <tr>
                    <td class="style1">
                        <%=dr.Item("PropertyApplianceId")%>
                    </td>
                    <td>
                        <%=If(dr.Item("Inspected")=True,"Yes", "No") %>
                    </td>
                    <td>
                        <%=dr.Item("CombustionReading")%>
                    </td>
                    <td>
                        <%=dr.Item("OperatingPressure")%>
                    </td>
                    <td>
                        <%=dr.Item("SafteyDeviceOprational")%>
                    </td>
                    <td>
                        <%=dr.Item("AdequateVentilation")%>
                    </td>
                    <td>
                        <%=dr.Item("FlueVisualCondition")%>
                    </td>
                    <td>
                        <%=dr.Item("SatisfactoryTermination")%>
                    </td>
                    <td>
                        <%=dr.Item("FluePerformanceChecks")%>
                    </td>
                    <td>
                        <%=dr.Item("ApplianceServiced")%>
                    </td>
                    <td>
                        <%=dr.Item("ApplianceSafeToUse")%>
                    </td>
                    <td>
                        <%=spilageTest%>
                    </td>
                </tr>
                <% Next%>
                <% 
                Else
                %>
                <tr>
                    <td class="style1">
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                </tr>
                <% 
                End If
                %>
            </table>
        </div>
        <%--Inspection Detail Table -- End --%>
        <div style='clear: both;'>
        </div>
        <%--Detector Detail Table -- Start --%>
        <div class="table_grid_container">
            <table class="table_grid" id="Table2">
                <tr>
                    <th colspan="10">
                        <p>
                            Detector Detail</p>
                    </th>
                </tr>
                <tr>
                    <td>
                        Ref.
                    </td>
                    <td>
                        Location
                    </td>
                    <td>
                        Type
                    </td>
                    <td>
                        Tested
                    </td>
                    <td>
                        Landlord&#39;s Detector
                    </td>
                </tr>
                <%
                    If cp12Ds.Tables(2).Rows.Count > 0 Then
                        For Each dr As DataRow In cp12Ds.Tables(2).Rows()
                %>
                <tr>
                    <td class="style1">
                        <%=dr.Item("detectorId")%>
                    </td>
                    <td>
                        <%=dr.Item("location")%>
                    </td>
                    <td>
                        <%=dr.Item("detectorType")%>
                    </td>
                    <td>
                        <%=dr.Item("isTested")%>
                    </td>
                    <td>
                        <%=dr.Item("isLandlordsDetector")%>
                    </td>
                </tr>
                <% Next%>
                <% 
                Else
                %>
                <tr>
                    <td class="style1">
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                </tr>
                <% 
                End If
                %>
            </table>
        </div>
        <%--Detector Detail Table -- End --%>
        <div style='clear: both;'>
        </div>
        <div class="table_grid_container">
            <table class="table_grid" id="tblDefects">
                <tr>
                    <th align="center" class="style2">
                        <p style="text-align: center;">
                            Ref.</p>
                    </th>
                    <th align="center" style="width: 20%">
                        <p style="text-align: center;">
                            Defect(s) Identified</p>
                    </th>
                    <th align="center">
                        <p style="text-align: center;">
                            Remedial Action Taken</p>
                    </th>
                    <th align="center">
                        <p style="text-align: center;">
                            Warning/Advice Note issued (if YES* Serial Number)</p>
                    </th>
                    <th>
                        <p>
                        </p>
                    </th>
                </tr>
                <%
                    If cp12Ds.Tables(3).Rows.Count > 0 Then
                        Dim faultCount As Integer = 0
                        For Each dr As DataRow In cp12Ds.Tables(3).Rows()
                            faultCount = faultCount + 1
                %>
                <tr>
                    <td class="style2">
                        <%=dr.Item("ApplianceId")%>
                    </td>
                    <td>
                        <% If (dr.Item("IsDefectIdentified") = 1) Then%>
                        <%=dr.Item("DefectNotes")%>
                        <%Else%>
                        --
                        <%End If%>
                    </td>
                    <td>
                        <% If (dr.Item("IsActionTaken") = 1) Then%>
                        <%=dr.Item("ActionNotes")%>
                        <%Else%>
                        --
                        <%End If%>
                    </td>
                    <td>
                        <% If (dr.Item("IsWarningIssued") = 1) Then%>
                        <%=dr.Item("SerialNumber")%>
                        <%Else%>
                        --
                        <%End If%>
                    </td>
                    <% If (faultCount = 1) Then%>
                    <td rowspan="<%=cp12Ds.Tables(3).Rows().Count() %>">
                        NEXT SAFETY CHECK WITHIN 12 MONTH
                    </td>
                    <%End If%>
                </tr>
                <% Next%>
                <% 
                Else
                %>
                <tr>
                    <td class="style2">
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td rowspan="3" style="width: 160px;">
                        <p>
                            NEXT SAFETY CHECK WITHIN 12 MONTH</p>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                    <td>
                        --
                    </td>
                </tr>
                <% 
                End If
                %>
            </table>
        </div>
        <div style='clear: both;'>
        </div>
        <div style='width: 1026px; margin-bottom: 15px; border: 2px solid #999999; border-radius: 5px;'>
            <div style='text-align: left; margin-left: 10px;'>
                <h3>
                    Gas Installation Pipework</h3>
            </div>
            <div>
                <div style='text-align: left; margin-left: 10px; float: left; width: 500px; border-top: 2px solid #999999;'>
                    <p>
                        Satisfactory visual Inspection?; <strong style='padding-left: 20px;'>
                            <label id='lblSatisfactoryVisualInspection' runat="server">
                            </label>
                        </strong>
                    </p>
                </div>
                <div style='border-top: 2px solid #999999; float: left; padding-right: 74px; text-align: right;
                    width: 440px;'>
                    <p>
                        Satisfactory gas tightness test?; <strong style='padding-left: 20px;'>
                            <label id='lblStatisfactoryGasTightness' runat="server">
                            </label>
                        </strong>
                    </p>
                </div>
            </div>
            <div style='clear: both;'>
            </div>
            <div>
                <div style='text-align: left; margin-left: 10px; float: left; width: 500px; border-top: 2px solid #999999;'>
                    <p>
                        Emergency Control accessible?; <strong style='padding-left: 20px;'>
                            <label id='lblEmergencyControl' runat="server">
                            </label>
                        </strong>
                    </p>
                </div>
                <div style='border-top: 2px solid #999999; float: left; padding-right: 74px; text-align: right;
                    width: 440px;'>
                    <p>
                        Equipotential bonding evident?; <strong style='padding-left: 20px;'>
                            <label id='lblEquipotentialBoundingEvent' runat="server">
                            </label>
                        </strong>
                    </p>
                </div>
            </div>
            <div style='clear: both;'>
            </div>
            <div style='border-top: 2px solid #999999; text-align: left; margin: 10px; width: 1015px;
                padding-top: 8px;'>
                General Comments:
                <br />
                <label id='lblGeneralCommnents' runat="server">
                </label>
            </div>
        </div>
        <div style='width: 1026px; margin-bottom: 15px; border: 2px solid #999999; border-radius: 5px;'>
            <div>
                <div style='text-align: left; margin-left: 10px; float: left; width: 500px;'>
                    <p>
                        This safety record issued by: <span style='text-align: left; margin: 10px;'>Signed:
                        </span>
                        <asp:ImageButton ID="imgSign" runat="server" Width="50" Height="50" />
                    </p>
                </div>
                <div style='float: left; padding-right: 74px; text-align: right; width: 440px;'>
                    <p>
                        Name: &nbsp;&nbsp;&nbsp;<strong><label id='lblSurveyourName' runat="server"></label></strong>
                    </p>
                </div>
            </div>
            <div style='clear: both;'>
            </div>
            <div>
                <div style='text-align: left; margin-left: 10px; float: left; width: 500px;'>
                    <p>
                        Received by: <span style='padding-left: 105px'>Signed</span>
                    </p>
                </div>
                <div style='float: left; padding-right: 74px; text-align: right; width: 440px;'>
                    <p>
                        Tenant</p>
                </div>
            </div>
            <div style='clear: both;'>
            </div>
            <div>
                <div style='text-align: left; margin-left: 10px; float: left; width: 500px;'>
                    <p>
                        Date:
                        <label id="lblIssueDate" runat="server">
                        </label>
                    </p>
                </div>
                <div style='float: left; padding-right: 74px; text-align: right; width: 440px;'>
                    <p>
                        Appliances Tested:<strong style='padding-left: 20px;'><label id='lblApplianceTested'
                            runat="server"></label></strong>
                    </p>
                </div>
            </div>
            <div style='clear: both;'>
            </div>
            <div style='text-align: left; margin: 10px;'>
                <strong>Inspection Ref:<label id='lblJsgNumber' runat="server"></label></strong>
            </div>
        </div>
        <div style='clear: both;'>
        </div>
        <div style='text-align: left; margin-left: 10px;'>
            <p>
                *Refer to seperate Warning/Advice Notice</p>
        </div>
    </div>
    </form>
</body>
</html>
