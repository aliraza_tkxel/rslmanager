﻿Public Class Cp12Document
    Inherits System.Web.UI.Page

    Public Shared cp12Ds As DataSet = New DataSet()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsNothing(Request.QueryString("hid")) And Not IsNothing(Request.QueryString("jid"))) Then
            Dim heatingId As Integer = CType(Request.QueryString("hid"), Integer)
            Dim journalId As Integer = CType(Request.QueryString("jid"), Integer)
            Me.getCp12Info(heatingId, journalId)
        End If

    End Sub

#Region "Get Cp12 Info"
    Private Sub getCp12Info(ByVal heatingId As Integer, ByVal journalId As Integer)
        Dim propDal As PropertyDal = New PropertyDal()
        cp12Ds = propDal.getCp12Data(heatingId, journalId)

        Me.setCustomerInfo(cp12Ds)
        Me.fillGasInstallationDetail(cp12Ds)
        Me.fillSurveyourInfo(cp12Ds)
    End Sub
#End Region

#Region "Set Customer Info"
    Private Sub setCustomerInfo(ByVal cp12Ds As DataSet)
        Dim customerDt As DataTable = New DataTable()
        customerDt = cp12Ds.Tables(6)
        If (customerDt.Rows.Count > 0) Then
            Dim infoType As String = customerDt.Rows.Item(0).Item("InfoType")
            lblInfoType.InnerText = If(infoType.Equals("Property"), "Tenant", infoType)
            lblTenantName.InnerText = customerDt.Rows.Item(0).Item("Title") + " " + customerDt.Rows.Item(0).Item("FirstName") + " " + customerDt.Rows.Item(0).Item("LastName")
            lblHouseNumber.InnerText = customerDt.Rows.Item(0).Item("HouseNumber") + " " + customerDt.Rows.Item(0).Item("Address1")
            lblAddress2.InnerText = customerDt.Rows.Item(0).Item("Address2")
            lblCounty.InnerText = customerDt.Rows.Item(0).Item("County")
            lblPostCode.InnerText = customerDt.Rows.Item(0).Item("PostCode")
        End If
    End Sub
#End Region

#Region "fill Gas Installation Detail"
    Private Sub fillGasInstallationDetail(ByVal cp12Ds As DataSet)

        Dim cp12Dt As DataTable = cp12Ds.Tables(4)

        If cp12Dt.Rows.Count() > 0 Then
            lblSatisfactoryVisualInspection.InnerText = cp12Dt.Rows.Item(0).Item("VisualInspection")
            lblStatisfactoryGasTightness.InnerText = cp12Dt.Rows.Item(0).Item("GasTightnessTest")
            lblEmergencyControl.InnerText = cp12Dt.Rows.Item(0).Item("EmergencyControl")
            lblEquipotentialBoundingEvent.InnerText = cp12Dt.Rows.Item(0).Item("EquipotentialBonding")
        Else
            lblSatisfactoryVisualInspection.InnerText = "--"
            lblStatisfactoryGasTightness.InnerText = "--"
            lblEmergencyControl.InnerText = "--"
            lblEquipotentialBoundingEvent.InnerText = "--"

        End If

        If cp12Ds.Tables(8).Rows.Count() > 0 Then
            lblGeneralCommnents.InnerText = cp12Ds.Tables(8).Rows.Item(0).Item("DefectNotes")
        Else
            lblGeneralCommnents.InnerText = "--"
        End If
    End Sub
#End Region

#Region "fill Surveyour Info "
    Private Sub fillSurveyourInfo(ByVal cp12Ds As DataSet)

        Dim signatureDt As DataTable = cp12Ds.Tables(5)
        If signatureDt.Rows.Count() > 0 Then
            lblSurveyourName.InnerText = signatureDt.Rows.Item(0).Item("Name")
            If IsDBNull(signatureDt.Rows.Item(0).Item("signaturepath")) = False Then
                Dim signaturePath As String = String.Empty
                Dim fullImagePath As String = String.Empty
                Dim imageName As String = String.Empty
                signaturePath = Me.getSginaturePath()
                imageName = signatureDt.Rows.Item(0).Item("signaturepath")
                fullImagePath = signaturePath + "\" + imageName
                imgSign.ImageUrl = fullImagePath
            Else
                imgSign.Visible = False
            End If

        End If

        If cp12Ds.Tables(7).Rows.Count > 0 AndAlso IsDBNull(cp12Ds.Tables(7).Rows.Item(0).Item("issueDate")) = False Then
            lblIssueDate.InnerText = cp12Ds.Tables(7).Rows.Item(0).Item("issueDate").ToString()
        End If

        If cp12Ds.Tables(9).Rows.Count() > 0 Then
            lblApplianceTested.InnerText = cp12Ds.Tables(9).Rows.Item(0).Item("ApplianceTested").ToString()
        Else
            lblApplianceTested.InnerText = "--"
        End If

        If cp12Ds.Tables(10).Rows.Count() > 0 Then
            lblJsgNumber.InnerText = cp12Ds.Tables(10).Rows.Item(0).Item("JSGNUMBER").ToString()
        Else
            lblJsgNumber.InnerText = ""
        End If

    End Sub
#End Region

#Region "get Sginature Path"
    Private Function getSginaturePath()
        Dim signatureImgPath As String = String.Empty
        signatureImgPath = System.Configuration.ConfigurationManager.AppSettings("SignatureImagePath").ToString()
        signatureImgPath = "../../../EmployeeSignature/"
        Return signatureImgPath
        'HttpContext.Current.Server.MapPath(signatureImgPath)
    End Function
#End Region

End Class