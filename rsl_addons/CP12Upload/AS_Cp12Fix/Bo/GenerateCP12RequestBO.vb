﻿Public Class GenerateCP12RequestBO

    Private _heatingIds As String
    Public Property HeatingIds() As String
        Get
            Return _heatingIds
        End Get
        Set(ByVal value As String)
            _heatingIds = value
        End Set
    End Property


    Private _propertyId As String
    Public Property PropertyId() As String
        Get
            Return _propertyId
        End Get
        Set(ByVal value As String)
            _propertyId = value
        End Set
    End Property


    Private _schemeId As String
    Public Property SchemeId() As String
        Get
            Return _schemeId
        End Get
        Set(ByVal value As String)
            _schemeId = value
        End Set
    End Property


    Private _blockId As String
    Public Property BlockId() As String
        Get
            Return _blockId
        End Get
        Set(ByVal value As String)
            _blockId = value
        End Set
    End Property


    Private _journalId As String
    Public Property JournalId() As String
        Get
            Return _journalId
        End Get
        Set(ByVal value As String)
            _journalId = value
        End Set
    End Property

    Private _updatedBy As String
    Public Property UpdatedBy() As String
        Get
            Return _updatedBy
        End Get
        Set(ByVal value As String)
            _updatedBy = value
        End Set
    End Property

    Private _cp12DocumentPath As String
    Public Property Cp12DocumentPath() As String
        Get
            Return _cp12DocumentPath
        End Get
        Set(ByVal value As String)
            _cp12DocumentPath = value
        End Set
    End Property

    Private _cp12DocumentName As String
    Public Property Cp12DocumentName() As String
        Get
            Return _cp12DocumentName
        End Get
        Set(ByVal value As String)
            _cp12DocumentName = value
        End Set
    End Property

    Private _heatingId As String
    Public Property HeatingId() As String
        Get
            Return _heatingId
        End Get
        Set(ByVal value As String)
            _heatingId = value
        End Set
    End Property

End Class
