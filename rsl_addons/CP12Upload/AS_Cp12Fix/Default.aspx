﻿<%@ Page Title="Home Page" Language="vb" MasterPageFile="~/Blank.Master" AutoEventWireup="false"
    CodeBehind="Default.aspx.vb" Inherits="AS_Cp12Fix._Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="head">

    <script src="Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <link href="Styles/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            InitAutoCompl();
        });


        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoCompl();
        }

        function InitAutoCompl() {
            var autocompleter = $('#<%= txtSearch.ClientID %>').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "Default.aspx/getPropertySearchResult",
                        data: "{'searchText':'" + $('#<%= txtSearch.ClientID %>').val() + "'}",
                        dataType: "json",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                var imgpath = "";
                                if (item.split('/')[2] === "Property") {
                                    imgpath = "Images/icon_property.png"
                                }
                                else if (item.split('/')[2] === "Scheme") {
                                    imgpath = "Images/icon_scheme.png"
                                }
                                else {
                                    imgpath = "Images/icon_block.png"
                                }
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1] + '/' + item.split('/')[2],
                                    type: item.split('/')[2],
                                    imageName: imgpath,
                                }
                            }));

                        },
                        error: function (result) {
                        }
                    });
                },
                select: function (event, ui) {

                    document.getElementById('<%= hdnSelectedPropertyId.ClientID %>').value = ui.item.val.split('/')[0]
                    document.getElementById('<%= hdnSelectedPropertyAddress.ClientID %>').value = ui.item.label
                    document.getElementById('<%= hdnSelectedType.ClientID %>').value = ui.item.val.split('/')[1]
                    document.getElementById('<%= txtSearch.ClientID %>').value = ui.item.label
                    __doPostBack('<%=btnHidden.UniqueID %>', '');
                }
            });
            if (autocompleter.data("autocomplete")) {
                autocompleter.data("autocomplete")._renderItem = function (ul, item) {
                    return $("<li>").data("item.autocomplete", item)
                                    .append("<a><img style='vertical-align:middle; padding-right: 7px;' src='" + item.imageName + "' height='30px' width='30px'/>" + item.label + "</a>")
                                    .appendTo(ul);
                };
            }
            autocompleter.unbind("blur.autocomplete") // Prevent to close autocomplete when scrolling
            // bind a listener that will hide the menu when clicking on the body of the page.
            $("body:not(.ui-autocomplete)").live('click', function () {
                autocompleter.autocomplete("close");
            });
        }

        function getQuerystring(name) {
            var hash = document.location.hash;
            var match = "";
            if (hash != undefined && hash != null && hash != '') {
                match = RegExp('[?&]' + name + '=([^&]*)').exec(hash);
            }
            else {
                match = RegExp('[?&]' + name + '=([^&]*)').exec(document.location.search);
            }

            return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
        }

    </script>

</asp:Content>

<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updPnlMessage" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
    <p>
        &nbsp;
    </p>

    <div style="width: 100%;">
        <b>Find a Property/Scheme/Block:</b>
        <asp:TextBox ID="txtSearch" runat="server" Width="200" CssClass="searchbox" AutoPostBack="false"
            AutoCompleteType="Search"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
            TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
        </asp:TextBoxWatermarkExtender>
        <div style="display: none;">
            <asp:HiddenField ID="hdnSelectedPropertyId" runat="server" />
            <asp:HiddenField ID="hdnSelectedPropertyAddress" runat="server" />
            <asp:HiddenField ID="hdnSelectedType" runat="server" />
            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
        </div>
    </div>

    <p>
        <asp:GridView ID="grdJournal" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="Heating/Boiler Id">
                    <ItemTemplate>
                        <asp:Label ID="lblHeatingId" runat="server" Text='<%# Eval("HeatingId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Journal Id">
                    <ItemTemplate>
                        <asp:Label ID="lblJournalId" runat="server" Text='<%# Eval("JournalId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cp12 Issue Date">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Eval("IssueDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnGeneratePdf" runat="server"
                            CommandArgument='<%# Eval("HeatingId").ToString() %>'
                            OnClick="btnGeneratePdf_Click" Text="Generate New Pdf" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </p>



</asp:Content>
