﻿Imports System.IO
Imports System.Drawing
Imports System.Net
Imports EO.Pdf

Public Class _Default
    Inherits System.Web.UI.Page



#Region "Web Page Events and Methods"

#Region "Log4Net setup"


    Public Shared log As log4net.ILog

    Public Sub CheckLogger()
        Try

            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
            Dim logFile As String = ""
            Dim strconfig As String = "\log"

            If strconfig <> String.Empty Then
                logFile = HttpContext.Current.Server.MapPath("../Logger.Config")
                strconfig = String.Empty
            End If

            log4net.Config.XmlConfigurator.ConfigureAndWatch(New System.IO.FileInfo(logFile))
        Catch ex As Exception
            log.[Error]("****************** Exception Block While Create log file ******************")
            log.[Error](String.Format("Exception Message: {0}", ex.Message))
            log.[Error](String.Format("Inner Exception: {0}", ex.InnerException))
            log.[Error](String.Format("Exception Object: {0}", ex))
        End Try
    End Sub


#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Hidden button event to populate dropdown list"
    ''' <summary>
    ''' Hidden button event to populate dropdown list
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Try
            If txtSearch.Text.Equals(String.Empty) = False Then
                Dim selectedId As String = hdnSelectedPropertyId.Value
                Dim type As String = hdnSelectedType.Value

                Me.displayPlgsrEntries(type, selectedId)
            Else
                lblMessage.Text = "Enter the property/scheme/block"
            End If
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try

    End Sub
#End Region

#Region "Btn Generate Pdf Click"
    ''' <summary>
    ''' Btn Generate Pdf Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btnGeneratePdf_Click(sender As Object, e As EventArgs)
        Try

            Dim btnGeneratePdf As Button = DirectCast(sender, Button)
            Dim heatingId As String = CType(btnGeneratePdf.CommandArgument, Int32)
            Dim row As GridViewRow = DirectCast(btnGeneratePdf.NamingContainer, GridViewRow)
            Dim lblJournalId As Label = DirectCast(row.FindControl("lblJournalId"), Label)
            Dim journalId As String = lblJournalId.Text

            Dim selectedId As String = hdnSelectedPropertyId.Value
            Dim type As String = hdnSelectedType.Value

            Dim request As GenerateCP12RequestBO = New GenerateCP12RequestBO()
            request.HeatingId = heatingId
            request.JournalId = journalId

            If (type = "Property") Then
                request.SchemeId = String.Empty
                request.BlockId = String.Empty
                request.PropertyId = selectedId
            ElseIf (type = "Scheme") Then
                request.SchemeId = selectedId
                request.BlockId = String.Empty
                request.PropertyId = String.Empty
            Else
                request.SchemeId = String.Empty
                request.BlockId = selectedId
                request.PropertyId = String.Empty
            End If

            Me.processCp12Document(request)
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try

    End Sub
#End Region

#Region "Display lgsr Entries"
    ''' <summary>
    ''' Display lgsr Entries
    ''' </summary>
    Public Sub displayPlgsrEntries(type As String, selectedId As String)
        Dim propDal As PropertyDal = New PropertyDal()
        Dim cp12HeatingIdsDataSet As DataSet = New DataSet()
        propDal.getHeatingIds(type, selectedId, cp12HeatingIdsDataSet)
        Me.grdJournal.DataSource = cp12HeatingIdsDataSet.Tables(0)
        Me.grdJournal.DataBind()
    End Sub

#End Region

#Region "Process Cp12 Document"
    ''' <summary>
    ''' Process Cp12 Document
    ''' </summary>
    Public Sub processCp12Document(ByVal requestBo As GenerateCP12RequestBO)

        Dim documentTemplateUrl As String = String.Empty
        documentTemplateUrl = Me.ToAbsoluteUrl("~/Cp12Document.aspx?hid=" + requestBo.HeatingId + "&jid=" + requestBo.JournalId)

        Dim request As GenerateCP12RequestBO = setFilePath(requestBo)
        makeEoPdf(request.Cp12DocumentPath, documentTemplateUrl)
        Dim cp12InByteForm As Byte() = readCp12Document(request.Cp12DocumentPath)

        Dim propDal As PropertyDal = New PropertyDal()
        propDal.saveCp12Document(request, cp12InByteForm, Convert.ToInt32(request.HeatingId))

        downloadCp12Document(request.Cp12DocumentName, request.Cp12DocumentPath)

    End Sub

#End Region

#Region "Get Url Content"
    ''' <summary>
    ''' Get Url Content
    ''' </summary>
    ''' <returns></returns>
    Public Function getUrlContent()
        Dim req As WebRequest = WebRequest.Create("http://localhost:6980/Cp12Document.aspx")
        Dim res As WebResponse = req.GetResponse()
        Dim sr As New StreamReader(res.GetResponseStream())
        Dim html As String = sr.ReadToEnd()
        Return html
    End Function
#End Region

#Region "Download Cp12 Document"
    ''' <summary>
    ''' Download Cp12 Document
    ''' </summary>
    ''' <param name="fileName"></param>
    ''' <param name="completeFilePath"></param>
    Public Sub downloadCp12Document(ByVal fileName As String, ByVal completeFilePath As String)
        Response.AppendHeader("content-disposition", "attachment; filename=" + fileName)
        Response.ContentType = "application/pdf"
        Response.WriteFile(completeFilePath)
        Response.Flush()
        Response.End()
    End Sub

#End Region

#End Region

#Region "Web Service Methods"

#Region "Populate Cp12Document"
    ''' <summary>
    ''' Populate CP12 document info
    ''' </summary>
    ''' <param name="hids"></param>
    ''' <param name="sid"></param>
    ''' <param name="bid"></param>
    ''' <param name="pid"></param>
    ''' <param name="jid"></param>
    ''' <param name="updatedBy"></param>
    ''' <returns></returns>
    <System.Web.Services.WebMethod()>
    Public Shared Function populateCp12Document(ByVal hids As String, ByVal sid As String, ByVal bid As String, ByVal pid As String, ByVal jid As String, ByVal updatedBy As String) As String

        Dim request As GenerateCP12RequestBO = New GenerateCP12RequestBO()
        request.HeatingIds = hids
        request.SchemeId = sid
        request.BlockId = bid
        request.PropertyId = pid
        request.JournalId = jid
        request.UpdatedBy = updatedBy

        Dim defaul As _Default = New _Default()
        defaul.processCp12DocumentForOfflineApp(request)
        Return "success"
    End Function
#End Region

#Region "Process Cp12 Document For Offline App"
    ''' <summary>
    ''' Process Cp12 Document For Offline App
    ''' </summary>
    ''' <param name="requestBo"></param>
    Public Sub processCp12DocumentForOfflineApp(requestBo As GenerateCP12RequestBO)

        Dim requestURL = HttpContext.Current.Request.Url
        Dim propDal As PropertyDal = New PropertyDal()
        Dim heatingIds = requestBo.HeatingIds.Split(","c).[Select](Function(n) Integer.Parse(n)).ToList()

        For Each hid In heatingIds

            Dim documentTemplateUrl As String = Convert.ToString((Convert.ToString(requestURL.GetLeftPart(UriPartial.Authority) + "/CP12Upload/Cp12Document.aspx?hid=" + hid.ToString() + "&jid=" + requestBo.JournalId.ToString())))
            requestBo.HeatingId = hid.ToString()
            Dim cp12DocumentPath As GenerateCP12RequestBO = setFilePath(requestBo)
            makeEoPdf(cp12DocumentPath.Cp12DocumentPath, documentTemplateUrl)
            Dim cp12InByteForm As Byte() = readCp12Document(cp12DocumentPath.Cp12DocumentPath)

            propDal.saveCp12Document(requestBo, cp12InByteForm, hid)

        Next

    End Sub

#End Region

#Region "Search Property"
    ''' <summary>
    ''' Returns properties 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <System.Web.Services.WebMethod()>
    Public Shared Function getPropertySearchResult(ByVal searchText As String) As List(Of String)

        Dim resultDataset As DataSet = New DataSet()

        Dim propDal As PropertyDal = New PropertyDal()
        propDal.getPropertySearchResult(resultDataset, searchText)

        Dim result As New List(Of String)()

        For Each row As DataRow In resultDataset.Tables(0).Rows
            result.Add(String.Format("{0}/{1}/{2}", row("value"), row("id"), row("Type")))
        Next row

        For Each row As DataRow In resultDataset.Tables(1).Rows
            result.Add(String.Format("{0}/{1}/{2}", row("value"), row("id"), row("Type")))
        Next row

        For Each row As DataRow In resultDataset.Tables(2).Rows
            result.Add(String.Format("{0}/{1}/{2}", row("value"), row("id"), row("Type")))
        Next row

        Return result

    End Function

#End Region

#End Region

#Region "Shared Methods"

#Region "Set File Path"
    Public Function setFilePath(requestBo As GenerateCP12RequestBO)

        Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")
        Dim cp12RootPath As String = String.Empty
        Dim cp12DocumentName As String = String.Empty
        Dim cp12DocumentPath As String = String.Empty

        If (Not String.IsNullOrEmpty(requestBo.PropertyId)) Then
            cp12RootPath = Server.MapPath("~/../PropertyImages/" + requestBo.PropertyId + "/Documents/")
            cp12DocumentName = String.Format("CP12_Property_{0}_{1}_{2}.pdf", requestBo.PropertyId, requestBo.JournalId.ToString(), requestBo.HeatingId.ToString())
        ElseIf (Not String.IsNullOrEmpty(requestBo.SchemeId)) Then
            cp12RootPath = Server.MapPath("~/../PDRDocuments/Schemes/" + requestBo.SchemeId + "/Documents/")
            cp12DocumentName = String.Format("CP12_Scheme_{0}_{1}_{2}.pdf", requestBo.SchemeId, requestBo.JournalId.ToString(), requestBo.HeatingId.ToString())
        ElseIf (Not String.IsNullOrEmpty(requestBo.BlockId)) Then
            cp12RootPath = Server.MapPath("~/../PDRDocuments/Blocks/" + requestBo.BlockId + "/Documents/")
            cp12DocumentName = String.Format("CP12_Block_{0}_{1}_{2}.pdf", requestBo.BlockId, requestBo.JournalId.ToString(), requestBo.HeatingId.ToString())
        End If


        cp12DocumentPath = cp12RootPath + cp12DocumentName

        requestBo.Cp12DocumentName = cp12DocumentName
        requestBo.Cp12DocumentPath = cp12DocumentPath

        If (Directory.Exists(cp12RootPath) = False) Then
            Directory.CreateDirectory(cp12RootPath)
        End If

        Return requestBo

    End Function
#End Region

#Region "Read Cp12 Document"
    ''' <summary>
    ''' Read Cp12 Document
    ''' </summary>
    ''' <param name="cp12DocumentPath"></param>
    Private Function readCp12Document(ByVal cp12DocumentPath As String)
        Dim cp12InByteForm As Byte()

        Using stream = New FileStream(cp12DocumentPath, FileMode.Open, FileAccess.Read)
            Using reader = New BinaryReader(stream)
                cp12InByteForm = reader.ReadBytes(CInt(stream.Length))
            End Using
        End Using

        Return cp12InByteForm
    End Function
#End Region

#Region "To Absolute Url"
    Public Function ToAbsoluteUrl(relativeUrl As String) As String
        If String.IsNullOrEmpty(relativeUrl) Then
            Return relativeUrl
        End If

        If HttpContext.Current Is Nothing Then
            Return relativeUrl
        End If

        If relativeUrl.StartsWith("/") Then
            relativeUrl = relativeUrl.Insert(0, "~")
        End If
        If Not relativeUrl.StartsWith("~/") Then
            relativeUrl = relativeUrl.Insert(0, "~/")
        End If

        Dim url = HttpContext.Current.Request.Url
        Dim port = If(url.Port <> 80, (":" + url.Port.ToString()), [String].Empty)

        Return [String].Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl))
    End Function

#End Region

#Region "Make Eo Pdf"
    Public Sub makeEoPdf(ByVal cp12DocumentPath As String, ByVal documentTemplateUrl As String)

        'Convert the Url to PDF
        EO.Pdf.Runtime.AddLicense(
    "S8+4iVmXpLHn4KXj8wjpjEOXpLHLn1mXpM0M452X+Aob5HaZyeDZz53dprEh" +
    "5Kvq7QAZvFuovL/boVmmwp61n1mXpM3a4KXj8wjpjEOXpLHLu6jp6PYdyKfd" +
    "87EP4K3cwbPhrm+mtsHct1uX9wYNxLHn7QMQ8nrrwbPhrm+mtsHcuFuX+vYd" +
    "8qLm8s7NsHGZpMDpjEOXpLHLu6zg6/8M867p6c8SsKjb3Ovy1KCwuN4Q0qno" +
    "u9sk97Huwc7nrqzg6/8M867p6c+4iXWm8PoO5Kfq6c+4iXXj7fQQ7azcwp61" +
    "n1mXpM0X6Jzc8gQQyJ21uMHis3CotMjgtW+zs/0U4p7l9/b043eEjrHLn1mz" +
    "8PoO5Kfq6fbpu2jj7fQQ7azc6Q==")

        HtmlToPdf.Options.OutputArea = New RectangleF(0.0F, 0.5F, 8.0F, 20.0F)
        HtmlToPdf.Options.PageSize = PdfPageSizes.A4
        'HtmlToPdf.Options.AutoFitY = HtmlToPdfAutoFitMode.ScaleToFit
        HtmlToPdf.ConvertUrl(documentTemplateUrl, cp12DocumentPath)

    End Sub

#End Region

#End Region

End Class