USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_CP12JournalIds]    Script Date: 05/31/2013 16:29:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC	[dbo].[AS_CP12JournalIds] @propertyId = N'A010060001'
-- =============================================
-- Author:		<Author,,NoorMuhammad>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AS_CP12JournalIds]
	@propertyId as varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    
    
    Select AS_JOURNAL.JournalId, AS_JOURNAL.PropertyId,issuedate from AS_JOURNAL 
	inner join as_status on as_journal.statusid = as_status.statusid
	inner join (select issuedate, journalid, propertyid from p_lgsr where propertyid=@propertyId) as p_lgsr ON as_journal.journalid = p_lgsr.journalid
	where AS_JOURNAL.propertyid=@propertyId
	and as_status.title= 'CP12 issued'
	
	
END
