#@del /F /Q *.user
@PUSHD AS_Cp12Fix
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PAUSE