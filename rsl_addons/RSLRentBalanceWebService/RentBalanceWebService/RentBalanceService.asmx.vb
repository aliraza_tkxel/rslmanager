﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="https://crm.broadlandhousing.org/RSLRentBalanceWebService/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function getRentBalance(ByVal mobile As String) As String
        Try
            Dim returnMsg As String
            ' Create object of RentBalance Class
            Dim objRentBal As New RentBalanceClass.RentBalance
            objRentBal.Mobile = mobile
            returnMsg = objRentBal.GetBalance(objRentBal.Mobile)
            Return returnMsg
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class