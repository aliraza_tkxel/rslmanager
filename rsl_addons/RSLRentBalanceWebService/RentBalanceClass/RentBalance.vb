﻿Public Class RentBalance

    Private _mobile As String
    Private _tenancyId As String

    Private ds As New dsRentBalance
    Private ta As New dsRentBalanceTableAdapters.C_GET_BALANCETableAdapter
    Private dt As New dsRentBalance.C_GET_BALANCEDataTable
    Public Property Mobile() As String
        Get
            Return _mobile
        End Get

        Set(value As String)
            _mobile = value
        End Set
    End Property


    Public Function GetBalance(ByVal _mobile As String) As String

        dt = ds.C_GET_BALANCE
        ' use the connection from the web.config. Otherwise system will use BHG-DEV2 connection hardcoded in dataset connection string xds
        ta.Connection.ConnectionString = Configuration.ConfigurationManager.ConnectionStrings("RSL Manager BroadlandsConnectionString").ConnectionString
        ta.FillTenancyID(dt, _mobile)

        If dt.Rows(0)(0).ToString() = "" Then
            Return "<ErrorMsg>The records do not match with the Broadland system</ErrorMsg>"
        Else
            Return ds.GetXml()
        End If


    End Function


End Class
