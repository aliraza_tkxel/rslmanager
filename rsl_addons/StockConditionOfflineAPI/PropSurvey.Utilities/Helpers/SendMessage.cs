﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Authentication;
using System.IO;

namespace PropSurvey.Utilities.Helpers
{
    public static class SendMessage
    {
        //The below mentioned settings needs to be changed on deployment
        const bool enableMessaging = false;  //change to true to enable messaging
        const int port = 2195; //default port
        const String hostname = "gateway.sandbox.push.apple.com"; //apple sandbox url needs to be replaced when publishing
        //const String hostname = "gateway.push.apple.com";
        const string certificatePath = @"D:\ck.pem"; //certificate path
        const string certificatePassword = "asdfasdf"; //certificate password
        /////////////////////////////////////////
        
        
        public static void sendPushMessage(string deviceToken, string message)
        {
            if (enableMessaging)
            {
                X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, certificatePassword);
                X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

                TcpClient client = new TcpClient(hostname, port);
                SslStream sslStream = new SslStream(
                                client.GetStream(),
                                false,
                                new RemoteCertificateValidationCallback(ValidateServerCertificate),
                                null
                );

                try
                {
                    sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
                }
                catch (AuthenticationException)
                {
                    client.Close();
                    return;
                }


                MemoryStream memoryStream = new MemoryStream();
                BinaryWriter writer = new BinaryWriter(memoryStream);

                writer.Write((byte)0);  //The command
                writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
                writer.Write((byte)32); //The deviceId length (big-endian second byte)


                writer.Write(StrToByteArray(deviceToken.ToUpper()));

                String payload = "{\"aps\":{\"alert\":\"" + message + "\",\"sound\":\"default\"}}";

                writer.Write((byte)0); //First byte of payload length; (big-endian first byte)
                writer.Write((byte)payload.Length);     //payload length (big-endian second byte)

                byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
                writer.Write(b1);
                writer.Flush();

                byte[] array = memoryStream.ToArray();
                sslStream.Write(array);
                sslStream.Flush();

                // Close the client connection.
                client.Close();
            }

        }

        // The following method is invoked by the RemoteCertificateValidationDelegate.
        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }

        public static byte[] StrToByteArray(string str)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            return encoding.GetBytes(str);
        }
    }
}
