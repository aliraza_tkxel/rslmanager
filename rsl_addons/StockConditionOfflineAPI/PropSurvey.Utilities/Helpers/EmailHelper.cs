﻿// -----------------------------------------------------------------------
// <copyright file="EmailHelper.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.IO;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Constants;
using System.Drawing;
using System.Web;
using PropSurvey.Contracts.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace PropSurvey.Utilities.Helpers
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class EmailHelper
    {

        public static void sendHtmlFormattedEmail(String recepientName, String recepientEmail, String subject, String body)
        {
            try
            {

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);

            }
            catch (IOException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.ErrorSendingEmailMsg, true, MessageCodesConstants.ErrorSendingEmailCode );
                throw new ArgumentException(MessageConstants.ErrorSendingEmailMsg, "Invalid Email");
            }
        }

    }
}
