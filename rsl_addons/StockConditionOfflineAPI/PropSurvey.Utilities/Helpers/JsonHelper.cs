﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;

namespace PropSurvey.Utilities.Helpers
{
    public static class JsonHelper
    {
        /// <summary>
        /// Serialize a data object to stream
        /// </summary>
        public static Stream SerializerToStream<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);   
            byte[] resultBytes = ms.ToArray();
            ms.Close();
            return new MemoryStream(resultBytes);          
        }

        /// <summary>
        /// Serialize a data object to string
        /// </summary>
        public static string SerializerToString<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            ms.Close();
            return obj;
        }
        /// <summary>
        /// Merge two JSON strings into one valid JSON string
        /// </summary>
        public static string MergeJSONStrings(string JSON1, string JSON2)
        {
            if (JSON1.Trim() == "" || JSON1.Trim() == "[]" )
                return JSON2;

            if (JSON2.Trim() == "" || JSON2.Trim() == "[]")
                return JSON1;

            StringBuilder finalJSON = new StringBuilder();
            if (JSON1.Length > 0 && JSON1.EndsWith("]"))
            {
                JSON1 = JSON1.Trim();
                int index = JSON1.LastIndexOf(']');
                JSON1 = JSON1.Remove(index);
                JSON1 += ",";
            }

            finalJSON.Append(JSON1);

            if (JSON2.Length > 0 && JSON2.StartsWith("["))
            {
                JSON2 = JSON2.Trim();
                JSON2 = JSON2.Remove(0, 1);
            }

            finalJSON.Append(JSON2);
            return finalJSON.ToString();
        }
    }
}
    //{
    //    /// <summary>
    //    /// This function serialize the data object into JSON string 
    //    /// </summary>
    //    /// <param name="objData">object to serialize</param>
    //    /// <param name="type">type of object to serialize</param>
    //    public static string SerializeToString( object objData, Type type )
    //    {
    //        DataContractJsonSerializer serializer = new DataContractJsonSerializer(type);
    //        MemoryStream memoryStream = new MemoryStream();

    //        serializer.WriteObject(memoryStream, objData);

    //        string json = Encoding.Default.GetString(memoryStream.ToArray());
    //        memoryStream.Dispose();
    //        serializer = null;
    //        return json;
    //    }
    //}
//}
