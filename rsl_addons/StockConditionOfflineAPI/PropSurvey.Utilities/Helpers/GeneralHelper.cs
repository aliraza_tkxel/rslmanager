﻿namespace PropSurvey.Utilities.Helpers
{
    public class GeneralHelper
    {

        #region Get Job Sheet Defect Number by Defect Id

        /// <summary>
        /// Pad Zeros to the left of defectId and append it to string JSD.        
        /// The purpose of this function is to make "Defect Job Sheet number JSD" consistant in whole system.
        /// </summary>
        /// <param name="defectId">Defect Id to get JSD i.e 123</param>
        /// <returns>Defect Job Sheet number i.e JSD00123</returns>
        public static string getJobSheetDefectNumber(int defectId)
        {
            return string.Format("JSD{0}", defectId.ToString().PadLeft(5, '0'));
        }

        #endregion
    
    }
}
