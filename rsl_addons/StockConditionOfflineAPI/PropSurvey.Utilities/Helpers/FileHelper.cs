﻿using System;
using System.IO;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Constants;
using System.Drawing;
using System.Web;
using PropSurvey.Contracts.Data;
using System.Data;
using System.Collections.Generic;
using System.Reflection;

namespace PropSurvey.Utilities.Helpers
{
    public static class FileHelper
    {
        #region get Property Image Upload Path

        /// <summary>
        /// this function returns the image upload path that is mentioned in web.config
        /// </summary>
        /// <returns>string path</returns>
        public static string getPropertyImageUploadPath()
        {
            string imageUploadPath = System.Configuration.ConfigurationManager.AppSettings["ImageUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        /// <summary>
        /// this function returns the thumbnail upload path that is mentioned in web.config
        /// </summary>
        /// <returns>string path</returns>
        public static string getPropertyThumbnailUploadPath()
        {
            string imageUploadPath = System.Configuration.ConfigurationManager.AppSettings["ThumbnailUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        /// <summary>
        /// Get Physical Property Image Upload Path 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public static string getPhysicalPropertyImageUploadPath(string propertyId)
        {
            string imageUploadPath = getPropertyImageUploadPath().ToString() + propertyId + "\\Images\\";
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }
        /// <summary>
        /// Get Physical Property Thumbnail Upload Path
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public static string getPhysicalPropertyThumbnailUploadPath(string propertyId)
        {
            string imageUploadPath = getPropertyThumbnailUploadPath().ToString() + propertyId + "\\Thumbs\\";
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        public static string getLogicalPropertyImagePath(string propertyId, string imageName)
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/PropertyImages/" + propertyId + "/Images/" + imageName;
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imagePath;
        }
        #endregion
        /// <summary>
        /// This function creates the path on the specific drive 
        /// </summary>
        /// <param name="path">writeable path</param>

        public static void createDirectory(string path)
        {
            try
            {
                Directory.CreateDirectory(@path);
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        #region get Property Image Type
        /// <summary>
        /// This function returns the image type which is mentioned in web.config
        /// </summary>
        /// <returns>returns the string which has image type</returns>
        public static string getPropertyImageType()
        {
            string imageType = System.Configuration.ConfigurationManager.AppSettings["ImageType"].ToString();
            return imageType;
        }

        #endregion

        #region make Property Image Full Path

        /// <summary>
        /// This function takes the upload path , property id and image name as an argument and returns the full path
        /// </summary>
        /// <param name="imageUploadPath">the path wehere image would be uploaded</param>
        /// <param name="propertyId">property id of the house</param>
        /// <param name="imageName">image name of the property</param>
        /// <returns></returns>
        public static string makePropertyImageFullPath(string imageUploadPath, string propertyId, string imageName)
        {
            string fullPath = @imageUploadPath + propertyId + @"\Images\" + imageName;

            return fullPath;
        }
        #endregion

        #region get File Extension

        /// <summary>
        /// returns the file extension 
        /// </summary>
        /// <param name="fileName">file name </param>
        /// <returns>returns the extension</returns>

        public static string getFileExtension(string fileName)
        {
            string ext = fileName.Substring(fileName.LastIndexOf("."));

            return ext;
        }
        #endregion

        #region generate Unique File Name

        /// <summary>
        /// this function returns the unique string name 
        /// </summary>
        /// <returns>unique string </returns>

        public static string generateUniqueString()
        {
            DateTime date = DateTime.Now;
            string uniqueName = date.ToString("dd_MM_yyyy_HH_mm_ss_ffffff");
            return uniqueName;
        }
        #endregion

        #region "getUniqueFileName"
        
        /// <summary>
        /// This function returns the unique filename
        /// </summary>
        /// <param name="pfileName"></param>
        /// <returns></returns>
        public static string getUniqueFileName(string pfileName)
        {
            string fileName = Path.GetFileNameWithoutExtension(pfileName);
            string ext = Path.GetExtension(pfileName);
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileName + uniqueString;

            return fileName + ext;
        }

        /// <summary>
        /// This function save the image of property on disk
        /// </summary>
        /// <param name="stream">stream of image</param>
        /// <param name="imageFullPath">full path of image, where image should be saved</param>
        public static void saveImageOnDisk(Stream stream, string imageFullPath, long fileSize)
        {
            try
            {
                FileStream outstream = File.Open(imageFullPath, FileMode.Create, FileAccess.Write);
                //read from the input stream in 4K chunks and save to output stream

                int bufferLen = (int)fileSize;
                byte[] buffer = new byte[bufferLen];
                int count = 0;
                while ((count = stream.Read(buffer, 0, bufferLen)) > 0)
                {
                    outstream.Write(buffer, 0, count);
                }                
                outstream.Close();
                stream.Close();
            }
            catch (IOException ioException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.ImageSavingErrorMsg, true, MessageCodesConstants.ImageSavingErrorCode);
                throw;
            }
        }


        #endregion

        #region "Generate Thumbnail"
        //Change#53 - Behroz - 17/01/2013 - Start
        /// <summary>
        /// This function generates thumbnails
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="thumbnailFolderPath"></param>
        public static void generateThumbnail(string fileName, string thumbnailFolderPath, string fullImagePath)
        {
            //Create a new Bitmap Image loading from location of origional file            
            Bitmap bm = (Bitmap)Image.FromFile(fullImagePath);

            String strFileName = fileName;

            //Declare Thumbnails Height and Width
            int newWidth = 100;
            int newHeight = 100;
            //Create the new image as a blank bitmap
            Bitmap resized = new Bitmap(newWidth, newHeight);
            //Create a new graphics object with the contents of the origional image
            Graphics g = Graphics.FromImage(resized);
            //Resize graphics object to fit onto the resized image
            g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
            //Get rid of the evidence
            g.Dispose();

            if (Directory.Exists(thumbnailFolderPath) == false)
            {
                Directory.CreateDirectory(thumbnailFolderPath);
            }
            //Create new path and filename for the resized image
            String newStrFileName = thumbnailFolderPath + strFileName;
            //Save the new image to the same folder as the origional
            resized.Save(newStrFileName);
            resized.Dispose();
            bm.Dispose();
        }
        //Change#53 - Behroz - 17/01/2013 - End
        #endregion

        #region Convert Date

        public static DateTime convertDate(SurveyParamItemFieldsData survItemsFieldsData)
        {
            string dateString = survItemsFieldsData.surveyParamItemFieldValue.Replace("/Date(", "").Replace(")/", "");
            int plusPosition = 0;
            if (dateString.Contains("+"))
            {
                plusPosition = dateString.IndexOf("+");
            }
            else if (dateString.Contains("-"))
            {
                plusPosition = dateString.IndexOf("-");
            }
            double seconds = double.Parse(dateString.Substring(0, plusPosition)) / 1000;
            TimeZone zone = TimeZone.CurrentTimeZone;
            // Get offset.
            double offset = double.Parse(zone.GetUtcOffset(DateTime.Now).ToString().Substring(0, 2));
            DateTime surveyParamItemFieldValue = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(seconds).AddHours(offset);

            return surveyParamItemFieldValue;
        }
        #endregion
                
        #region get Signature Image Path
        /// <summary>
        /// this function returns the signature image path that is mentioned in web.config
        /// </summary>
        /// <returns>string path</returns>
        public static string getSignatureImagePath()
        {
            string signatureImagePath = System.Configuration.ConfigurationManager.AppSettings["SignatureImagePath"].ToString();
            return HttpContext.Current.Server.MapPath(signatureImagePath);
            //return imageUploadPath;
        }
        #endregion

        #region make Signature Image Full Path

        /// <summary>
        /// This function takes the signature path and image name as an argument and returns the full path
        /// </summary>
        /// <param name="signatureImagePath">the path where image is available</param>
        /// <param name="imageName">image name of the operative signature</param>
        /// <returns></returns>
        public static string makeSignatureImageFullPath(string signatureImagePath, string imageName)
        {
            string fullPath = @signatureImagePath + @"\" + imageName;

            return fullPath;
        }
        #endregion
                
        #region Convert Date to Server Time Zone
        
        public static DateTime convertDate(string dateInputString)
        {
            string dateString = dateInputString.Replace("/Date(", "").Replace(")/", "");
            dateString = dateString.Replace("/Date(", "").Replace(")/", "");
            int plusPosition = 0;
            if (dateString.Contains("+"))
            {
                plusPosition = dateString.IndexOf("+");
            }
            else if (dateString.Contains("-"))
            {
                plusPosition = dateString.IndexOf("-");
            }
            double seconds = double.Parse(dateString.Substring(0, plusPosition)) / 1000;
            TimeZone zone = TimeZone.CurrentTimeZone;
            // Get offset.
            double offset = double.Parse(zone.GetUtcOffset(DateTime.Now).ToString().Substring(0, 2));
            DateTime replaceDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(seconds).AddHours(offset);

            return replaceDate;
        }

        // Region added  05/06/2013 - END

        #endregion
        #region Convert list to data table
        /// <summary>
        /// This function is being used to append asteriks with message so that it 'll be easy for us to identify the msg later in log file
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static string messageForLogger(string msg)
        {
            return MessageConstants.LineIdentifierForLogger + MessageConstants.PushNotificationSuccess + msg + MessageConstants.LineIdentifierForLogger;
        }
        #endregion
        #region Convert list to data table
        /// <summary>
        /// Convert Generic list to data table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DataTable ConvertToDataTable<T>(List<T> list)
        {
            DataTable dt = new DataTable();
            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                dt.Columns.Add(new DataColumn(info.Name, info.PropertyType));
            }
            if (list != null)
            {
                foreach (T tt in list)
                {
                    DataRow row = dt.NewRow();
                    foreach (PropertyInfo info in typeof(T).GetProperties())
                    {
                        row[info.Name] = info.GetValue(tt, null);
                    }
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        #endregion


        #region get Scheme Block Image Upload Path

        /// <summary>
        /// this function returns the image upload path that is mentioned in web.config
        /// </summary>
        /// <returns>string path</returns>
        public static string getSchemeBlockImageUploadPath()
        {
            string imageUploadPath = System.Configuration.ConfigurationManager.AppSettings["SbImageUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        /// <summary>
        /// this function returns the thumbnail upload path that is mentioned in web.config
        /// </summary>
        /// <returns>string path</returns>
        public static string getSchemeBlockThumbnailUploadPath()
        {
            string imageUploadPath = System.Configuration.ConfigurationManager.AppSettings["SbThumbnailUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        /// <summary>
        /// Get Physical Property Image Upload Path 
        /// </summary>
        /// <param name="propertyId"></param>
        /// 
        /// <returns></returns>
        public static string getPhysicalSchemeBlockImageUploadPath(int schemeId, int blockId)
        {
            string imageUploadPath = string.Empty;
            if (blockId > 0)
            {
                imageUploadPath = getSchemeBlockImageUploadPath().ToString() + "Blocks\\" + blockId.ToString() + "\\Images\\";
            }
            else
            {
                imageUploadPath = getSchemeBlockImageUploadPath().ToString() + "Schemes\\" + schemeId.ToString() + "\\Images\\";
            }
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }


        public static string getLogicalSchemeBlockImagePath(int schemeId, int blockId, string imageName)
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath  =string.Empty;
            if (blockId > 0)
            {
                imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/PDRDocuments/Blocks/" + blockId.ToString() + "/Images/" + imageName;
            }
            else
            {
                imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/PDRDocuments/Schemes/" + schemeId.ToString() + "/Images/" + imageName;
            }
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imagePath;
        }
        #endregion
    }
}
