﻿
namespace PropSurvey.Utilities.Constants
{
    public static class MessageConstants
    {
        public const string DbConErrorMsg = "Error occured while connecting with data store.";
        public const string DbConEntityErrorMsg = "Error occured while connecting with data store entity.";

        //Property agent to appointment Messages
        public const string PropertyAgentInvalid = "Property Agent does not exists.";

        //Login Messages
        public const string LoginErrorMsg = "Authenticated";
        public const string LoginSuccessMsg = "Invalid username or password!";
        public const string InvalidEmailMsg = "Your email address does not match any of our records, please try again.";
        public const string ErrorSendingEmailMsg = "An error occurred while sending email, please try again.";
        public const string AccessLockdownWarningMsg = "You have one more login attempt before your access is locked down!";
        public const string AccessLockdownMsg = "Your account is now locked, please contact the Systems Team for assistance.";
        public const string IncorrectUsernamePasswordMsg = "Your username or password do not appear to be correct.";
        public const string AlreadyLoggedInMsg = "User is already already logged in!";
        public const string SessionExpiredMsg = "User session has expired!";
        public const string LoginEmailSubjectMsg = "RSL Manager Login Information";
        public const string AbortAppointmentSubject = "Appointment Aborted";
        public const string PasswordUpdatedSuccessfullyMsg = "The password has been changed successfully. Please use your new password to log in.";
        public const string PasswordExistedInHistoryMsg = "Unable to update the password. The value provided for the new password does not meet the length, complexity, or history requirements of the RSLManager.";
        public const string PasswordComplexityMsg = "Your new password must be at least 7 characters , contain at least one lower case letter, one upper case letter and one digit";

        public const string LoginEmailSuccessMsg = "Your Login details have been emailed to you. Once you have received your login details try to login again.";
        public const string success = "success";
      
        //Faults Messages
        public const string InvalidFaultCategory = "Fault Category: {0} is invalid. Category should be GENERAL COMMENT / RIDDOR / IMMEDIATELY DANGEROUS / AT RISK / NOT TO CURRENT STANDARDS / OTHER";
     
        public const string InvalidFaultId = "Fault is invalid.";
       
        //Installation Pipework Messages
        public const string InvalidReceivedOnBehalfOf = "wrong Receiver provided. Value should be  TENANT/LANDLORD/AGENT/HOMEOWNER.";

        //Installation Pipework Messages
        public const string InstallationPipeWorkOptionInvalid = "wrong code provided. Value should be NA/Yes/No.";
        public const string AppointmentIDInvalid = "Appointment Id is not valid. This ID does not exist in Database.";
        public const string AppointmentIDInvalidGas = "The Appointment ID does not exist in Database.";

      
        public const string LGSRIdInvalid = "LGSR Id is not valid. This ID does not exist in Database.";
     

        //Customer Service Messages
        public const string CustomerIdInvalidFromtMsg = "Customer id is wrong. Customer id should be a positive integer(number)";
       
        public const string EmployeeIdInvalidFromtMsg = "Employee id is wrong. Employee id should be a positive integer(number)";
        public const string SignatureImageNotFoundMsg = "System is unable to locate the signature image file against this employee.";
       

        //Org Inspection Service Messages
        public const string OrgPaymentTypeInvalid = "Org PaymentType, or PaymentTerm or companyType is wrong.";
        public const string OrgInspectionInvalidInput = "Gas Engineer ID or Org ID or Appointment ID is wrong.";

        //Appliance Inspection Service Messages
        public const string AppInspectionValueInvalid = "Wrong code provided. Value should be NA/Yes/No/Pass/Fail.";

        //property service message
        public const string SkipTopInvalidFromtMsg = "Kkip OR top should be a positive integer(number)";
        public const string PropertyImageNotFoundMsg = "System is unable to locate the image file against this property.";
        public const string ImageExtensionInvalidMsg = "Image extension is invalid. It should be *.jpg or *.gif";
        public const string ImageSavingErrorMsg = "Error has been occured while saving the image on disk";
        public const string PropertyIdDoesNotExistMsg = "There is no appointment against this propertyId: {0}";
        public const string InvalidImageType = "Invalid Type parameter";
       
        public const string PropertyIdDoesNotExistMsgGas = "There is no appointment against this appointmentId: {0}";
        
        public const string PropertyPictureIdInvalidMsg = "Property Picture id: \"{0}\" is wrong.  Property picture id should be a positive integer(number)";
        public const string PropertyPictureIdDoesNotExistMsg = "There is no picture against this propertyPictureId: {0}";
        public const string DefectPictureIdDoesNotExistMsg = "There is no picture against this PropertyDefectImageId: {0}";
        //appointment service message
        public const string ProblemUpdatingAppointmentStatusMsg = "Problem while updating appointment status";
        public const string AppointmentRescheduledErrorMsg = "Appointment has been rescheduled, please perform pull to refresh";
        public const string AppointmentIdInvalidFromtMsg = "Appointment id is wrong. Appointment id should be a positive integer(number)";
        public const string AppointmentIdInvalidMsg = "Appointment does not exist.";
        public const string AppointProgStatusInvalidValueMsg = "Value: \"{0}\"of appointment status is invalid. Appointment status should be \"NotStarted\", \"Pending\", \"Finished\"";
        public const string AppointFaultProgStatusInvalidValueMsg = "Value: \"{0}\"of appointment status is invalid. Appointment status should be \"NotStarted\", \"InProgress\", \"Complete\"";
        public const string FaultJobStatusInvalidValueMsg = "Value: \"{0}\"of appointment status is invalid. Appointment status should be \"NotStarted\", \"InProgress\", \"Started\", \"NoEntry\", \"Paused\", \"Complete\"";
        public const string SurveyourUsernameInvalidUserMsg = "Surveyor username \"{0}\" does not exist";
        public const string AppointTypeInvalidValueMsg = "Value: \"{0}\"of appointment type is invalid. Appointment type should be \"Stock\", \"Pre\", \"Post\", \"Void\"";
        public const string SurveyourStatusInvalidValueMsg = "Value: \"{0}\"of surveyor availability status is invalid. Surveyor availability status should be \"Busy\", \"Available\"";
        public const string SurveyourTypeInvalidValueMsg = "Value: \"{0}\"of survey type is invalid. Survey type status should be \"Appointments\", \"ConditionSurveys\",\"WorkOrders\"";
        public const string PropertyIdOrCustomerIdDoesNotExistMsg = "Property id: \"{0}\"or customer id: \"{1}\" is wrong. There is no record against these in data store.";
        public const string PropertyDoesNotExistMsg = "Property Id: \"{0}\" is incorrect. There is no record against it in data store.";
        public const string AppointmentAlreadyExistMsg = "Appointment already exist for this Surveyor \"{0}\" .";


        //survey
        public const string AppointmentIdDoesNotExistForPropertyMsg = "Property id: \"{0}\"or customer id: \"{1}\" or appointment id: \"{2} \"is wrong. There is no record against these in data store.";
        public const string SurveyFormAlreadySavedForPropertyMsg = "This survey form is already saved in data store against this, appointment id: \"{0}\", propertyPortionName:\"{1}\"";
        public const string InspectionDataMissing = "Inspection data missing";

        public const string CertificateIssuedNameInAS_Status = "Certificate issued";
        public const string CertificateIssuedNameInAS_Action = "Certificate issued";
        public const string AppointmentArrangedInAS_Status = "Arranged";
        public const string NoEntryInAS_Status = "No Entry";
        public const string NoEntryInAS_Action = "No Entry";
        public const string AbortedInAs_Status = "Aborted";
        public const string AbortedInAs_Action = "Aborted";
        public const string AppointmentToBeArrangedStatus = "Appointment to be arranged";
        public const int GeneralCommentCategoryId = 1;

        public const string GasItemName = "Appliances";
        public const string GasAppointmentType = "Gas";
        public const string FaultAppointmentType = "Fault";
        public const string PlannedAppointmentType = "Planned";
        public const string StockItemName = "stock";
        public const string SmokeDetectorName = "Smoke Detector";
        public const string CODetectorName = "CO Detector";
        public const string MovieExtension = ".mov";
        public const string DefaultImagePath = "~/vidthumb.jpg";
        public const string PropertyItemName = "Property";
		public const string LineIdentifierForLogger = "***********";
        public const string PushNotificationSuccess = "Push notification has been sent.";
        public const string PushNotificationRejectByApple = "Push notification: Apple's server has rejected the push notification";
        public const string PushNotificationAppointmentIdDoesNotExistInDb = "Push notification: Appointment id does not exist in database";
        public const string PushNotificationDeviceTokenIsEmpty = "Push notification: Device token is empty";
        public const string PushNotificationOperatorIdIsEmpty = "Push notification: Operator id is not correct or doesn't exist in database";
        public const string PushNotificationMessageIsEmpty = "Push notification: Notification message is not valid";
        public const string InvalidData = "Invalid data provided, unable to parse.";

        //Overdue appointments push notification message
        public const string OverDuePushNotificationMessage = "Please ensure you complete all previous appointments on your app.";
        public const string DocumentExpiaryPushNotificationMessage = "The document \"{0}\" of {1} will get expired after 14 days.";
    }

}
