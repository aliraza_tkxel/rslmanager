﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropSurvey.Utilities.Constants
{
    public class ApplicationConstants
    {
        /*************************************
         * Logging Categories
         */
        public const string OverDueFailure = "OverdueFailure";
        public const string PushService = "PushService";
        /**************************************/
        public static string BHG_Dev_Name = "devcrm";
        public static string BHG_Test_Name = "testcrm";
        public static string BHG_Live_Name = "crm";
        public static string BHG_UAT_Name = "uatcrm";
        public static int  plannedComponentId = 0;
        public static int previousComponentId = 0;
        public static int nextComponentId = 0;


        public const string ApplianceInspectionType = "Appliance Servicing";
        public const string SaniClean = "Sani Clean";
        public const string StandardVoidWorks = "Standard Void Works";
        public const string HeatingItemName = "Heating";
        public const string BoilerRoomItemName = "Boiler Room";
        public const string DropdownParameterKey = "Dropdown";
        public const string TextBoxParameterKey = "TextBox";
        public const string CheckboxesParameterKey = "Checkboxes";
        public const string BoilerPrefix = "Boiler";
        public const string HeatingTypePrefix = "Heating Type";
        
        public const string HeatingTypeParameterKey = "Heating Type";
        public const string BoilerTypeParameterKey = "Boiler Type";
        public const string HeatingManufacturerParameterKey = "Manufacturer";
        public const string HeatingModelParameterKey = "Model";
        public const string SerialNoParameterKey = "Serial No";
        public const string GcNumberParameterKey = "GC Number";        
        public const string BoilerLocationParameterKey = "Location";
        public const string GasketSetReplacementParameterKey = "Gasket Set Replacement";
        public const string OriginalInstallDateParameterKey = "Original Install Date";
        public const string LandlordApplianceParameterKey = "Landlord Appliance";        
        public const string FlueTypeParameterKey = "Flue Type";
        public const string FuelTypeParameterKey = "Fuel Type";
        public const string TankTypeParameterKey = "Tank Type";
        public const string BurnerTypeParameterKey = "Burner Type";
        public const string SolarTypeParameterKey = "Solar Type";
        public const string LastReplacedParameterKey = "Last Replaced";
        public const string ReplacementDueParameterKey = "Replacement Due";
        public const string ApplianceMakeParameterKey = "Appliance Make";
        public const string ApplianceModelParameterKey = "Appliance Model";
        public const string ApplianceSerialNumberParameterKey = "Appliance Serial Number";
        public const string BurnerMakeParameterKey = "Burner Make";
        public const string BurnerModelParameterKey = "Burner Model";        

        public const string MainsGasParameterValue = "Mains Gas";
        public const string OilParameterValue = "Oil";
        public const string SolarParameterValue = "Solar";

        public const string PropertyGasAppointment = "property";
        public const string SchemeGasAppointment = "scheme";
        public const string BlockGasAppointment = "block";

        public const int DefaultFaultLogId = -1;
        public const int DefaultRepairDuration = 1;

        //Appoitnemnt and defect Status
        public const string AcceptedAppointmentStatus = "Accepted";
        public const string AbortedAppointmentStatus = "Aborted";
        public const string NoEntryAppointmentStatus = "No Entry";
        public const string ToBeApprovedDefectStatus = "To Be Approved";

        // Appointment Types         
        public const string GasAppointmentType = "Gas";
        public const string VoidGasAppointmentType = "Void Gas";
        public const string AlternativeAppointmentType = "Alternative Servicing";
        public const string OilAppointmentType = "Oil";
        public const string FaultAppointmentType = "Fault";
        public const string PlannedAppointmentType = "Planned";
        public const string MiscellaneousAppointmentType = "Miscellaneous";
        public const string AdaptationAppointmentType = "Adaptation";
        public const string ConditionAppointmentType = "Condition";
        public const string VoidInspectionAppointmentType = "Void Inspection";
        public const string PostVoidInspectionAppointmentType = "Post Void Inspection";
        public const string VoidWorksAppointmentType = "Void Works";
        public const string VoidGasCheckAppointmentType = "Void Gas Check";
        public const string VoidElectricCheckAppointmentType = "Void Electric Check";
        public const string ApplianceDefectAppointmentType = "Appliance Defect";

    }
}
