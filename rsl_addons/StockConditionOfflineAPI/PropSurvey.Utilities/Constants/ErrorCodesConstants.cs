﻿
namespace PropSurvey.Utilities.Constants
{
    public static class MessageCodesConstants
    {
        public const string EntitiyExceptionCode = "001";
        public const string GeneralExceptionCode = "002";
        public const string IntegerToStringConversionExceptionCode = "003";
        public const string FileNotFoundExceptionCode = "004";
        public const string DatabaseEntryNotFoundCode = "005";
        public const string AppointmentProgStatusInvalidSelectionCode = "006";
        public const string SurveyourUserNameInvalidCode = "007";
        public const string ImageExtensionInvalidCode = "008";
        public const string ImageSavingErrorCode = "009";
        public const string PropertyIdDoesNotExistCode = "010";
        public const string AppointmentTypeInvalidSelectionCode = "011";
        public const string SurveyourStatusInvalidSelectionCode = "012";
        public const string SurveyTypeInvalidSelectionCode = "013";
        public const string PropertyOrCustomerDoesNotExistCode = "014";
        public const string PropertyOrCustomerOrAppointmentDoesNotExistCode = "015";
        public const string UserNamePasswordDoesNotExistCode = "016";
        public const string IncorrectUserNamePasswordCode = "032";
        public const string AccessLockdownWarningCode = "033";
        public const string AccessLockdownCode = "034";
        public const string InvalidEmailCode = "035";
        public const string ErrorSendingEmailCode = "037";
        public const string LoginEmailSuccessCode = "036";
        public const string PasswordUpdatedSuccessfullyCode = "038";
        public const string PasswordExistedInHistoryCode="039";
        public const string AppointmentAlreadyExist = "031";
        public const string PasswordComplexityCode = "040";
        
        public const string PropertyPictureIdDoesNotExistCode = "017";
        public const string SurveyFormAlreadySavedErrorCode = "018";
        public const string UserNameAlreadyLoggedIn = "099";
        public const string UserNameSessionExpired = "100";

        public const string OrgPaymentTypeInvalid = "019";
        public const string OrgInspectionInvalidInput = "020";

        public const string AppInspectionValueInvalid = "021";

        public const string AppointmentIDInvalid = "022";
        public const string InstallationPipeWorkOptionInvalid = "023";

        public const string InvalidReceivedOnBehalfOf = "024";
        public const string InvalidFaultCategory = "025";
        public const string PropertyAgentInvalid = "026";
        public const string LGSRIdInvalid = "027";
        public const string AppointmentIdDoesNotExistCode = "028";
        public const string InvalidFaultId = "029";
        public const string FaultJobProgStatusInvalidSelectionCode = "030";
        public const string successCode = "200";
    }
}
