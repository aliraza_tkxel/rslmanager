﻿
namespace PropSurvey.Utilities.Constants
{
    public static class ServiceDescConstants
    {

        //IssuedReceivedBy - embed issuedreceivedby/ before the below urls
        public const string GetIssuedReceivedBy = "Get an IssuedReceivedBy form Data. The requested URl is: get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string SaveIssuedReceivedBy = "Saves an IssuedReceivedBy form Data. The requested URl is: save?username={userName}&salt={salt}";
        public const string UpdateIssuedReceivedBy = "Updates an IssuedReceivedBy form Data. The requested URl is: update?username={userName}&salt={salt}";

        //Faults - embed Faults/ before the below urls
        public const string GetFaults = "Get Faults Data. The requested URl is: get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetFaultsGas = "Get Faults Data for gas. The requested URl is: FetchGasDefects";
        public const string GetDefectCategory = "Get all the defects categories. The requested URl is: get/defects?username={username}&salt={salt}";
        public const string GetFaultId = "Returns a new fault id. The requested URl is: get/id?propertyId={propertyId}&username={username}&salt={salt}";
        public const string DeleteFault = "This function deletes the fault. The requested URl is: deleteDefect";
        public const string SaveFaultImage = "Saves the image of fault. The requested URl is: save/image?faultId={faultId}&fileName={fileName}&fileSize={fileSize}&username={username}&salt={salt}";
        public const string SaveFaultRepairImage = "Saves the image of fault repair. The requested URl is: saveFaultRepairImage?PropertyId={PropertyId}&jsNumber={jsNumber}&fileExt={fileExt}&createdBy={createdBy}&isBeforeImage={isBeforeImage}&username={username}&salt={salt}";
        //save/image?faultId={faultId}&fileName={fileName}&username={username}&salt={salt}        
        public const string GetFaultsOnly = "Get faults data only. this service will not include the general comments. The requested URL is: faults/get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetFaultsOnlyGas = "Get faults data only for gas. this service will not include the general comments. The requested URL is: faults/get/gas?propertyid={propertyid}&username={username}&salt={salt}";
        public const string GetGeneralComments = "Get general comments. The requested URl is: comments/get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetGeneralCommentsGas = "Get general comments for gas section. The requested URl is: comments/get/gas?propertyid={propertyid}&username={username}&salt={salt}";
        public const string SaveFaults = "Saves Faults Data. The requested URl is: save?username={userName}&salt={salt}";
        public const string SaveFaultsGas = "Saves Faults Data for gas. The requested URl is: saveFaultsGas";
        public const string UpdateFaults = "Update Faults Data. The requested URl is: update?username={userName}&salt={salt}";
        public const string UpdateFaultsGas = "Update Faults Data fpr Gas. The requested URl is: update/gas?username={userName}&salt={salt}";


        //Installation pipework - embed installationpipework/ before the below urls
        public const string GetInstallationPipework = "Get an InstallationPipework form Data. The requested URl is: get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string SaveInstallationPipework = "Saves an InstallationPipework form Data. The requested URl is: save?username={userName}&salt={salt}";
        public const string GetInstallationPipeworkGas = "Get an InstallationPipework form Data. for gas The requested URl is: get/gas?propertyId={propertyId}&username={username}&salt={salt}";
        public const string SaveInstallationPipeworkGas = "Saves an InstallationPipework form Data for gas. The requested URl is: save/gas?username={userName}&salt={salt}";
        public const string GetIssuedReceivedByGas = "Get an issue received form Data. The requested URl is: get/gas?propertyId={propertyId}&username={username}&salt={salt}";
        public const string UpdateIssueReceivedByGas = "Updates an Issued Received form Data. The requested URl is: update/gas?username={userName}&salt={salt}";
        public const string UpdateCP12DocumentGas = "Updates an existing Issued Received form Data and adds the CP12 Document in database. The requested URl is: update/gas/doc?id={id}&username={userName}&salt={salt}&tenantHanded={tenantHanded}";
        public const string UpdateInstallationPipework = "Updates an InstallationPipework form Data. The requested URl is: update?username={userName}&salt={salt}";

        //Org Inspection
        public const string SaveOrgInspection = "Save an Org Inspection in database. The Requested URL is: save?username={userName}&salt={salt}";
        public const string UpdateOrgInspection = "Updates an existing Org Inspection in database. The Requested URL is: update?username={userName}&salt={salt}";
        public const string GetOrgInspection = "Get an Org Inspection from database. The Requested URL is: get?appointmentid={appointmentId}&username={userName}&salt={salt}";
        public const string GetGasEngineerList = "Gets the list of Gas Engineers who are working under a company. The Requested URL is: engineer/get?username={userName}&salt={salt}";
        public const string GetOrgList = "Get Org list from database. The Requested URL is: org/get?username={userName}&salt={salt}";
        public const string SaveAnOrg = "Save a new Org in database. The Requested URL is: org/save?username={userName}&salt={salt}";
        public const string GetCompanyTypes = "Get the list of all company types. The Requested URL is: companytype/get?username={userName}&salt={salt}";
        public const string GetPaymentTypes = "Gets Payment methods and payment Terms list. The Requested URL is: paymenttype/get?username={userName}&salt={salt}";

        //appliances        
        public const string SaveHeatingInspectionImage = "Saves the Heating Inspection image. The requested URl is: saveHeatingInspectionImage?journalId={journalId}&heatingId={heatingId}&fileExt={fileExt}&propertyId={propertyId}&username={username}&salt={salt}&imageIdentifier={imageIdentifier}&schemeId={schemeId}&blockId={blockId}";
        public const string GetAppliancesByPropertyIDGas = "Returns the detail of the appliances by Property ID for GAS. The requested url is: ";
        public const string GetAppliancesByPropertyID = "Returns the detail of the appliances by Property ID. The requested url is: get?propertyid={propertyid}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetAppliancesSurveyData = "Returns the detail of appliance survey data. The requested url is: get?propertyid={propertyid}&schemeId={schemeId}&blockId={blockId}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetAlternativeSurveyData = "Returns the detail of alternative heating survey data. The requested url is: get?propertyid={propertyid}&schemeId={schemeId}&blockId={blockId}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetOilSurveyData = "Returns the detail of oil heating survey data. The requested url is: get?propertyid={propertyid}&schemeId={schemeId}&blockId={blockId}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceTypeList = "Get all appliances type list. The requested url is: type/get?username={userName}&salt={salt}";
        public const string GetApplianceInspectionForm = "Get appliance inspection form. The requested url is: inspection/get?applianceid={applianceid}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceInspectionFormGas = "Get appliance inspection form for gas. The requested url is: inspection/get/gas?applianceid={applianceid}&propertyid={propertyid}&username={userName}&salt={salt}";
        public const string GetApplianceModelList = "Get all appliance models List. The requested url is: model/get?username={userName}&salt={salt}";
        public const string GetApplianceManufacturList = "Get all appliance manufacturers list. The requested url is: manufacturer/get?username={userName}&salt={salt}";
        public const string SaveAnAppliance = "Save new appliance. The requested url is: save?username={userName}&salt={salt}";
        public const string SaveAnApplianceInspection = "Save an appliance inspection. The requested url is: inspection/save?username={userName}&salt={salt}";
        public const string SaveAnApplianceInspectionGas = "Save an appliance inspection for gas. The requested url is: inspection/save/gas?username={userName}&salt={salt}";
        public const string UpdateAnAppliance = "Updates existing appliance by ApplianceID. The requested url is: update?username={userName}&salt={salt}";
        public const string UpdateAnApplianceInspection = "Updates existing appliance inspection. The requested url is: inspection/update?username={userName}&salt={salt}";
        public const string GetApplianceCount = "Gets the applicance count. The requested url is: appliance/get/count?propertyId={propertyId}&username={userName}&salt={salt}";
        public const string SaveAnApplianceType = "Save new appliance type in database. The requested url is: type/save?username={userName}&salt={salt}";
        public const string SaveAnApplianceModel = "Save new appliance model in database. The requested url is: model/save?username={userName}&salt={salt}";
        public const string SaveAnApplianceManufacturer = "Save new appliance manufacturer in database. The requested url is: manufacturer/save?username={userName}&salt={salt}";
        public const string GetAppliancesLocationList = "Get all appliances location list. The requested url is: location/get?username={userName}&salt={salt}";
        public const string SaveAnApplianceLocation = "Save new appliance manufacturer in database. The requested url is: location/save?username={userName}&salt={salt}";
        public const string GetApplianceInspecedByAppointmentID = "Get all appliances inspected total by appointment ID. The requested url is: inspection/getinspected?appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceInspecedByAppointmentIDGas = "Get all appliances inspected total by Property Id. The requested url is: inspection/getinspected/gas?propertyid={propertyid}&username={userName}&salt={salt}";
        public const string GetApplianceInspectionDetailsByAppointmentID = "Get all appliances inspection details by appointment ID. The requested url is: inspection/details?appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceInspectionDetailsByPropertyID = "Get all appliances inspection details by property ID. The requested url is: inspection/gas/details?propertyid={propertyid}&username={userName}&salt={salt}";
        public const string UpdateDetectorCount = "Updates the count for detectors against property. Request url should be like this: detector/updatecount?propertyid={propertyid}&username={userName}&salt={salt}";
        public const string AddUpdateDetectorInfo = "Add a new record or updates an existing record for detector. Request url should be like this: detector/addupdateinfo?detectortype={detectortype}&username={userName}&salt={salt}";


        //Property Agent or Landlord
        public const string GetAllPropertyAgents = "Returns the detail of the property agent,its name, address and phone no. The requested url is: propertyagent/all?username={username}&salt{salt}";
        public const string SavePropertyAgent = "Saves a property agent in database. The requested url is: propertyagent/save?username={username}&salt{salt}";
        public const string UpdatePropertyAgent = "Updates a property agent that is already in database. The requested url is: propertyagent/update?username={username}&salt{salt}";
        public const string GetAgentToAppointment = "Returns the detail of the property agent to appointment. The requested url is: agenttoappointment/get?appointmentid={appoinmentId}&username={username}&salt={salt}";
        public const string GetAgentDetails = "Returns the detail of the property agent details. The requested url is: agentdetails/get?appointmentid={appoinmentId}&username={username}&salt={salt}";
        public const string SaveAgentToAppointment = "Saves a property agent to appointment in database. The requested url is: agenttoappointment/save?username={userName}&salt={salt}";
        public const string UpdateAgentToAppointment = "Updates a property agent to appointment that is already in database. The requested url is: agenttoappointment/update?username={userName}&salt={salt}";

        //authenticate
        public const string AuthenticateUser = "Authenticates the user against username and password. The requested url is: authenticate/user?username={userName}&password={password}&deviceToken={deviceToken}";
        public const string LogoutUser = "Logouts the user against username and salt. The requested url is: authenticate/logout?username={userName}&salt={salt}";
        public const string EmailForgottenPassword = "Email forgotten password of the user. The requested url is: authenticate/emailForgottenPassword";
        public const string UpdatePassword = "Update password of the user. The requested url is: authenticate/updatePassword";

        //customer
        public const string GetCustomerInfo = "Returns the detail of the customer and its property address. The requested url is: customer/info/{customerid}";
        public const string GetCustomerVulData = "Returns the list of vulnerabilities of data (If any). The requested url is: customer/vul/{customerid}";
        public const string GetEmployeeSignatureImage = "Returns the signature image of the employee against the employee id.Request url should be like this: customer/images/employeesignature/{employeeId}";

        //appointment        
        public const string GetAllAppointmentsByAppType = "Returns all appointments againt particular user and application {stock,gas or fault locator} type . The requested url should be look like this: allByAppType?startdate={startdate}&enddate={enddate}&exlcudeOverdue={exlcudeOverdue}&applicationType={applicationType}&appUserAgent={userName}&salt={salt}";
        public const string PopulateStockInspection = "Populate Stock Inspection Documents.The requested url should be look like this: appointment/prepareStockInspectionDocument";
        public const string UpdateAppointmentsStatus = "Update the given appointments statuses. The requested URL should look like this: appointment/updateAppointmentStatus";
        
        public const string GetAllStockAppointments = "Returns all Stock appointments. The requested url should be look like this: allStock?username={userName}&salt={salt}";
        public const string GetAllAppointmentsByUser = "Returns all appointments for a user. The requested url should be look like this: allByUser?username={userName}&filter={filter}&salt={salt}";
        public const string UpdateFaultAppointmentProgressStatus = "Update the progress of appointment. The request url should look like this: updateFaultAppntStatus?username={username}&appointmentId={appointmentId}&progressStatus={progressStatus}&salt={salt}";
        public const string GetFaultJobStatus = "This function will get the status of fault appointment. The request url should look like this: faultStatus/Get?appointmentid={appointmentId}&username={userName}";
        public const string UpdateFaultJobStatus = "This function will set the status of fault appointment. The request url should look like this: faultStatus/Set?username={userName}&salt={salt}";
        public const string NoEntryAppointmentStatus = "This function will set the status of fault appointment to NoEntry. The request url should look like this: appointmentStatus/NoEntry?username={userName}&salt={salt}";
        public const string GetPauseReasonList = "This function return the list of reasons for pausing a job. The request url should look like this: fetchPauseReasonList";
        public const string GetFaultRepairList = "This function return the list of fault repairs. The request url should look like this: fetchFaultRepairList";
        public const string UpdateAppointmentProgressStatus = "Update the progress of appointment. There are 3 status for this \"Not Started\", \"Pending\", \"Finished\". The request json should be like this: {\"appointmentStatus\":\"Finished\",\"appointmentId\":\"9\"}";
        public const string GetAndSetStockAppointmentProgressStatus = "This function will check the status of appointment. If progress status of appointment is \"NotStarted\" then it will update the status to \"Pending\" & it will return \"true\".  If progress status of appointment is \"Pending\" then it will update the status to \"Finished\" & it will return \"true\". If progress status of appointment is \"Finished\" then it will return \"true\". The user who created the appointment can change the status of appointment.";
        public const string SaveAppointment = "This funciton saves the appointment in database. Client will have to post the data at this url: appointment/save?username={userName}&salt={salt}";
        public const string GetAndSetGasAppointmentProgressStatus = "This function will check the status of appointment for the Gas Tab. If progress status of appointment is \"NotStarted\" then it will update the status to \"Pending\" & it will return \"true\".  If progress status of appointment is \"Pending\" then it will update the status to \"Finished\" & it will return \"true\". If progress status of appointment is \"Finished\" then it will return \"true\". The user who created the appointment can change the status of appointment.";
        public const string SaveAppointmentGas = "This funciton saves the appointment related to Gas in database. Client will have to post the data at this url: appointment/save/gas?username={userName}&salt={salt}";
        public const string GetUserAppointmentsGas = "Returns the appointments of particular user for type GAS only. The requested url should be look like this: appointment/get/gas?username={userName}&filter={filter}&salt={salt}";
        //public const string GetUserAppointmentsAll = "Returns all the appointments and combine them in a single result set. The requested url should be look like this: appointment/get/all?username={userName}&filter={filter}&salt={salt}";
        public const string GetStatus = "Returns all the statuses saved in database. The requested url should be look like this: appointment/get/status?username={userName}&salt={salt}";
        public const string SaveNoEntryGas = "This funciton saves No entry against appointment when the button is clicked from the Gas tab. The requested url should be look like this: appointment/noentry/save/gas?username={userName}&salt={salt}";
        public const string GetStockAppointmentsByUser = "Returns stock appointments of particular user. The requested url should be look like this: get?username={userName}&filter={filter}&salt={salt}";
        public const string GetAllUsers = "Returns all the users of rsl manager database.The requested url should be look like this: appointment/users/all?username={userName}&salt={salt}";
        public const string GetAllUsersGas = "Returns all the users of rsl manager database.The requested url should be look like this: appointment/users/all/gas?username={userName}&salt={salt}";
        public const string UpdateAppointment = "This funciton updates the appointment in database. Client will have to post the data at this url: appointment/update?username={userName}&salt={salt}";
        public const string UpdateAppointmentGas = "This funciton updates the appointment in database that is related to gas. Client will have to post the data at this url: appointment/update/gas?username={userName}&salt={salt}";
        public const string DeleteAppointment = "This funciton deletes the appointment in database. Client will have to post the data at this url: appointment/delete?appointmentid={appointmentId}&username={userName}&salt={salt}";
        public const string GetAppointment = "This funciton returns. The requested url should be look like this: appointment/getappointment?appointmentid={appointmentId}";
        public const string GetGasTabRights = "This funciton returns the rights of a user on gas tab. The requested url should be look like this: get/gas/rights?userid={userid}&username={userName}&salt={salt}";
        public const string GetNoEntries = "This funciton returns total no of No Entries and total appointments. The requested url should be look like this: appointment/appointmentInfo/get?propertyId={propertyId}&username={userName}&salt={salt}";
        public const string SaveNoEntry = "This funciton saves No entry against appointment. The requested url should be look like this: appointment/noentry/save?username={userName}&salt={salt}";
        public const string GetIndexScreenInfo = "Get index screen information. The requested URl is: indexinfo/get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetIndexScreenInfoGas = "Get index screen information for gas. The requested URl is: indexinfo/get/gas?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string populatePropertyRecordInspection = "Poupulate Property Record Inspection document of the stock survey appointment.";

        //property
        public const string FindProperty = "Returns the list of property against the specified criteria. Request Format is: property/search?skip={skip}&top={top}&reference={reference}&housenumber={housenumber}&street={street}&postcode={postcode}&customername={customername}";
        public const string FindPropertyGas = "Returns the list of property that are related to GAS against the specified criteria. Request Format is: property/search?skip={skip}&top={top}&reference={reference}&housenumber={housenumber}&street={street}&postcode={postcode}&customername={customername}";
        public const string GetPropertyImageNames = "Returns the list of names of property images against property id";
        public const string GetPropertyImage = "Returns the image against the name of image.Request url should be like this: property/images/{propertyId}?imgname={imageName}";
        public const string GetDefaultPropertyImage = "Returns the default image of the property against the property id.Request url should be like this: property/images/default/{propertyId}";
        public const string UpdatePropertyImage = "Upload the Default Property Images.";
        public const string UpdatePropertySurveyImage = "Upload the  Property  survey Images.";
        public const string UpdatePropertyImageGas = "Update the property image on server for gas. Client application will have to post the image at this url: update/images/gas?propertyid={propertyId}&appointmentId={appointmentId}&filename={fileName}&fileSize={fileSize}&updatedby={updatedBy}&picturetag={pictureTag}&isDefault={isDefault}";
        public const string DeletePropertyImage = "This funciton deletes the property image from the server. Client will have to post the data at this url: property/delete/image/{propertyPictureId}. This request will return true or false upon success or failure respectively";
        public const string GetPropertyDimensions = "Returns the property dimensions for specific property against property id";
        public const string UpdatePropertyDimensions = "This funciton saves the property dimensions in database. Client will have to post the data at this url: property/dimension/save. The request format will be: {\"bathroomHeight\":3.2,\"bathroomWidth\":2,\"bedroom1Height\":2.1,\"bedroom1Width\":3.2,\"bedroom2Height\":2.1,\"bedroom2Width\":2.6,\"hallwayHeight\":3.2,\"hallwayWidth\":3.2,\"kitchenHeight\":2.2,\"kitchenWidth\":1.2,\"livingRoomHeight\":3.2,\"livingRoomWidth\":4,\"propertyDimId\":0,\"propertyId\":\"A010010005\",\"wcHeight\":1.2,\"wcWidth\":1.2}";
        public const string GetPropertyAsbestosRisk = "This funciton returns the list of abestos risk against the property.  The requested url is: property/get/asb/{propertyId}";

        //survey
        public const string SaveSurveyForm = "This funciton saves the survey form in database. Client will have to post the data at this url: survey/save?username={userName}&salt={salt}";
        public const string GetSavedSurveyForm = "This funciton returns the saved survey form against appointment id and property portion name. Client will have to post the data at this url: survey/get?appointmentid={appointmentId}&portionname={propertyPortionName}";
        public const string GetAllSavedSurveyForms = "This funciton returns all the saved survey forms against appointment id. Client will have to post the data at this url: survey/all?appointmentid={appointmentId}";
        public const string getSurveyorSurveyForms = "This funciton returns all the saved survey forms against surveyor username. Client will have to post the data at this url: survey/getsurveys?userName={userName}";

        //application screen data
        public const string GetApplicaitonScreens = "This funciton return the application screens. Request url should be like this: appscreen/get?username={userName}&salt={salt}";

        // Test Service
        public const string GetAllJobs = "Get all Jobs. The requested URl is: jobs/getAllJobs?appointmentID={appointmentID}&username={username}&salt={salt}";
        public const string GetJobsByStatusID = "Get Jobs. The requested URl is: jobs/getJobsByStatusID?appointmentID={appointmentID}&jobstatusID={jobstatusID}&username={username}&salt={salt}";
        public const string UpdateCustomerContactFaultAppointment = "Update customer contact detail form within Fault Appiontment. URL is: updateCusContact?username={userName}&salt={salt}";

        // Push Notification
        public const string SendPushNotificationFault = "This function sends push notification for Fault Appointment. The requested url is: notificationFault?appointmentId={appointmentId}&type={type}";
        public const string SendPushNotificationApplianceAndPlanned = "This function sends push notification for Appliance and Planned Appointment. The requested url is: notificationApplianceAndPlanned?appointmentMessage={appointmentMessage}&operatorId={operatorId}";
        public const string sendOverduePushNotification = "This function sends push notification to operative(s) having overdue appointment(s). i.e an incomplete appointment is considered as overdue after 30 minutes of suggested completion date time.";
        public const string sendDocumentExpiaryPushNotification = "This function sends push notification to operative(s) having development Document Expiry with in Fourteen Days. ";        
    }
}
