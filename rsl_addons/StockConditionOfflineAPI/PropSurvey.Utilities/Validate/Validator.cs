﻿using System;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using System.Text.RegularExpressions;

namespace PropSurvey.Utilities.Validate
{
    public static class Validator
    {
        public static bool validateImageFileExtension(string extension)
        {
            bool success = false;
            if (!extension.Equals(String.Empty))
            {
                //Added by Behroz - 28/01/2013
                //if (extension.Equals(".jpg") || extension.Equals(".gif") || extension.Equals(".png"))
                if (extension.Equals(".jpg") || extension.Equals(".gif") || extension.Equals(".png") || extension.Equals(Utilities.Constants.MessageConstants.MovieExtension))
                {
                    success = true;
                }                
            }

            return success;

        }

        public static bool isEmail(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.emailExp);
            if (rgx.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        } 

        public static void validateCustomerId(string customerId)
        {

            if (!String.IsNullOrEmpty(customerId.ToString()))
            {
                try
                {
                    int custId = 0;
                    custId = Int32.Parse(customerId.ToString());
                }
                catch (FormatException formatException)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.CustomerIdInvalidFromtMsg, true, MessageCodesConstants.IntegerToStringConversionExceptionCode);
                    throw formatException;
                }
            }
            else
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.CustomerIdInvalidFromtMsg, customerId), true, MessageCodesConstants.SurveyTypeInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.CustomerIdInvalidFromtMsg, customerId), "customerId");
            }
        }

    }
}
