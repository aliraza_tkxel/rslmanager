﻿using System;
using System.IO;
using System.Net;
using System.Configuration;

namespace OverDuePushNotificationTrigger
{
    class Program
    {
        static void Main(string[] args)
        {
            //------------------------------------------------------
            // Logging using simple file operations.

            StreamWriter file = new StreamWriter(@"Logger.log",true);

            file.WriteLine("TimeStamp: {0}", DateTime.Now.ToUniversalTime());

            //------------------------------------------------------

            string failureessage = "An error occoured while sending overdue push notification, please contact support.";

            try
            {
                //------------------------------------------------------
                // Get server name and other configurations from configuration file.
                var hostname = ConfigurationManager.AppSettings[ConfigurationKeys.hostUrl.ToString()];
                var webServiceAPI = ConfigurationManager.AppSettings[ConfigurationKeys.webServiceAPI.ToString()];
                var aPIfunction = ConfigurationManager.AppSettings[ConfigurationKeys.APIfunction.ToString()];
                var useSecureConnection = bool.Parse(ConfigurationManager.AppSettings[ConfigurationKeys.useSecureConnction.ToString()]);
                var protocol = useSecureConnection ? "https" : "http";
                var url = string.Format("{0}://{1}/{2}{3}", useSecureConnection ? "https" : "http", hostname, webServiceAPI != string.Empty ? webServiceAPI + "/" : string.Empty, aPIfunction);
                //------------------------------------------------------

                //------------------------------------------------------
                // Log sending message and server/url details
                string sendingMessage = "Sending notification to operative having overdue appointments";
                Console.WriteLine(sendingMessage);
                file.WriteLine(sendingMessage);
                file.WriteLine("Server: {0}", hostname);
                file.WriteLine("URL: {0}", url);
                //------------------------------------------------------

                //------------------------------------------------------
                // Createe a web request and get the response from server.
                var webRequest = WebRequest.Create(url);                
                webRequest.Method = "post";
                webRequest.ContentLength = 0;

                var responseObj = webRequest.GetResponse();
                var responseRedaer = new StreamReader(responseObj.GetResponseStream());

                var responseText = responseRedaer.ReadToEnd();

                //------------------------------------------------------
                // Parse response from server, and show/log the message for success or failure case.
                
                bool responseStatus =bool.Parse(responseText);
                string successMessage = "Push notifications queued for sending.";
                if (responseStatus) { file.WriteLine(successMessage); Console.WriteLine(successMessage); }
                else { file.WriteLine(failureessage); Console.WriteLine(failureessage); }
            }
            catch (Exception ex)
            {
                Console.WriteLine(failureessage);
                file.WriteLine("-----------------------------------------------------------");
                file.WriteLine(failureessage);
                file.WriteLine("Exception");
                file.WriteLine(ex.ToString());               
            }

            file.WriteLine("===================================================================");
            file.Close();
            file.Dispose();
        }
    }

    enum ConfigurationKeys
    {
        hostUrl,
        webServiceAPI,
        APIfunction,
        useSecureConnction
    }
}