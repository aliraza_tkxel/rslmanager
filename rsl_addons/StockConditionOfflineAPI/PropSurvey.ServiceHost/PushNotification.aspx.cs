﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropSurvey.BuisnessLayer;
using PropSurvey.BuisnessLayer.Push;

namespace PropSurvey.ServiceHost
{
    public partial class PushNotification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {

                PushNotificationBl objBL = new PushNotificationBl();

                string fileName = Server.MapPath(FileUpload1.FileName);

                lblmsg.Text = objBL.sendPushNotification(txtDeviceToken.Text, fileName, chkSandbox.Checked, txtMessage.Text, txtPassword.Text);
            }
            catch (Exception ex)
            {
                lblmsg.Text = "exception: " + ex.Message ; 
                //                    " - inner exception: " + ex.InnerException.Message;
            }

        }
    }
}