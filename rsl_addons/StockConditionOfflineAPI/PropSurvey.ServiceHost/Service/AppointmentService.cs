using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Appointment;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Contracts.Data.Appointment;
using System.Web.Script.Serialization;
using System.Text;
using PropSurvey.Contracts.Data.Void;
using System.Reflection;
using PropSurvey.Contracts.Data.Stock;
using System.Web;
using PropSurvey.Contracts.Data.StockAPIParameters;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AppointmentService : IAppointmentService
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private void CheckLogger()
        {
            try
            {

                string logFile = "";
                string strconfig = "\\log";
                if (strconfig != string.Empty)
                {
                    logFile = HttpContext.Current.Server.MapPath("../Logger.Config");
                    strconfig = string.Empty;
                }

                //if (System.IO.File.Exists(logFile))
                    log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(logFile));
            }
            catch (Exception ex)
            {
                log.Error("****************** Exception Block While Create log file ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

            }

        }

        #region get All Appointments By Application Type

        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accommodations','Surveyors List' and 'Customer List'.
        /// </summary>
        /// <returns>
        /// List of appointments,Property Info,Accommodations,Surveyors List and Customer List, data objects
        /// </returns>
        public ResponseTemplate<AppointmentResponse> getAllAppointmentsByAppType(AppointmentParam varAppointmentParam)
        {
            try
            {
                CheckLogger();
                ResponseTemplate<AppointmentResponse> objresponse = new ResponseTemplate<AppointmentResponse>();
                AppointmentBl objAppointmentBl = new AppointmentBl();
                AppointmentResponse aptResponse = new AppointmentResponse();
                CommonData commonData = new CommonData();
                List<DeleteAppointmentParam> deleteAppointment = new List<DeleteAppointmentParam>();
                List<IAppointment> iAppointmentList = objAppointmentBl.getAllAppointmentsByAppType(varAppointmentParam, ref commonData, varAppointmentParam.existingAppointments, ref deleteAppointment);
                aptResponse.appointmentList = iAppointmentList;
                aptResponse.commonData = commonData;
                aptResponse.deleteAppointments = deleteAppointment;
                objresponse.response = aptResponse;
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objresponse.status = objMessageData;
                return objresponse;

            }
            catch (Exception ex)
            {
                log.Error("******************  get All Appointments By Application Type ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                MessageData objMessageData = new MessageData();
                //EmptyResponse objEmptyResponse = new EmptyResponse();
                ResponseTemplate<AppointmentResponse> objResponseTemplate = new ResponseTemplate<AppointmentResponse>();
                //Assigning values {error code and message} to MessageData object
                objMessageData.message = ErrorFault.message;
                objMessageData.code = ErrorFault.errorCode;
                //Assigning Message object to Response Template object
                //objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
        }

        #endregion

        #region get All Stock Appointments
        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accommodations','Surveyors List' and 'Customer List'.
        /// </summary>
        /// <returns>
        /// List of stock appointments,Property Info,Accommodations,Surveyors List and Customer List, data objects
        /// </returns>

        public ResponseTemplate<List<AllAppointmentsList>> getAllStockAppointments(AppointmentParam varAppointmentParam)
        {
            try
            {
                CheckLogger();
                AppointmentBl objAppointmentBl = new AppointmentBl();
                return objAppointmentBl.getAllStockAppointments(varAppointmentParam);
            }
            catch (Exception ex)
            {
                log.Error("******************  get All Stock Appointments******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                MessageData objMessageData = new MessageData();
                //EmptyResponse objEmptyResponse = new EmptyResponse();
                ResponseTemplate<List<AllAppointmentsList>> objResponseTemplate = new ResponseTemplate<List<AllAppointmentsList>>();
                //Assigning values {error code and message} to MessageData object
                objMessageData.message = ErrorFault.message;
                objMessageData.code = ErrorFault.errorCode;
                //Assigning Message object to Response Template object
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
        }
        #endregion

        #region save Stock Appointment
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>
        public ResponseTemplate<List<AllAppointmentsList>> saveStockAppointment(AppointmentDataStock varAppointmentDataStock)
        {
            try
            {
                CheckLogger();
                AppointmentBl apptBl = new AppointmentBl();
                //AuthenticationBl authBl = new AuthenticationBl();
                //if (authBl.checkForValidSession(varAppointmentDataStock.username, varAppointmentDataStock.salt))
                //{
                //Validate appointment data
                this.validateStockAppointmentData(varAppointmentDataStock);
                //}
                return apptBl.saveStockAppointment(varAppointmentDataStock);

            }
            catch (Exception ex)
            {
                log.Error("******************  Save Stock Appointment  ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }

        #endregion

        #region validate Stock Appointment Data
        /// <summary>
        /// This function validates the appointment data such as appointment type, appointment status, surveyour availability
        /// </summary>
        /// <param name="apptData">the object of appointment data</param>
        private void validateStockAppointmentData(AppointmentDataStock varAppointmentDataStock)
        {
            //validate the appointment type
            if (!varAppointmentDataStock.appointmentType.Equals(null) && !varAppointmentDataStock.appointmentType.Equals(String.Empty))
            {
                if (varAppointmentDataStock.validateAppointmentTypes(varAppointmentDataStock.appointmentType) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointTypeInvalidValueMsg, varAppointmentDataStock.appointmentType), true, MessageCodesConstants.AppointmentTypeInvalidSelectionCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointTypeInvalidValueMsg, varAppointmentDataStock.appointmentType), "appointmentType");
                }
            }

            //validate the appointment progress status
            if (varAppointmentDataStock.validateAppointmentProgressStatus(varAppointmentDataStock.appointmentStatus) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, varAppointmentDataStock.appointmentStatus), true, MessageCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, varAppointmentDataStock.appointmentStatus), "appointmentStatus");
            }

            //validate the surveyour availability status
            if (varAppointmentDataStock.validateSurveyourAvailabilityStatus(varAppointmentDataStock.surveyourAvailability) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, varAppointmentDataStock.surveyourAvailability), true, MessageCodesConstants.SurveyourStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, varAppointmentDataStock.surveyourAvailability), "surveyourAvailability");
            }


        }

        /// <summary>
        /// This function validates the appointment data such as appointment type, appointment status, surveyour availability
        /// </summary>
        /// <param name="apptData">the object of appointment data</param>
        private void validateAppointmentDataGas(AppointmentDataGas apptData)
        {
            //confirm the validations and apply

            //validate the appointment type
            //if (!apptData.appointmentType.Equals(null) && !apptData.appointmentType.Equals(String.Empty))
            //{
            //    if (apptData.validateAppointmentTypes(apptData.appointmentType) == false)
            //    {
            //        ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointTypeInvalidValueMsg, apptData.appointmentType), true, MessageCodesConstants.AppointmentTypeInvalidSelectionCode);
            //        throw new ArgumentException(String.Format(MessageConstants.AppointTypeInvalidValueMsg, apptData.appointmentType), "appointmentType");
            //    }
            //}

            //validate the appointment progress status
            if (apptData.validateAppointmentProgressStatus(apptData.appointmentStatus) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), true, MessageCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), "appointmentStatus");
            }

            //validate the surveyor availability status
            //if (apptData.validateSurveyourAvailabilityStatus(apptData. surveyourAvailability) == false)
            //{
            //    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, apptData.surveyourAvailability), true, MessageCodesConstants.SurveyourStatusInvalidSelectionCode);
            //    throw new ArgumentException(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, apptData.surveyourAvailability), "surveyourAvailability");
            //}
        }
        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        public ResponseTemplate<List<SurveyorUserData>> getAllUsers(SurveyorParam varSurveyorParam)
        {
            try
            {

                ResponseTemplate<List<SurveyorUserData>> userData = new ResponseTemplate<List<SurveyorUserData>>();

                //AuthenticationBl authBl = new AuthenticationBl();
                //if (authBl.checkForValidSession(varSurveyorParam.username, varSurveyorParam.salt))
                //{
                AppointmentBl apptBl = new AppointmentBl();
                userData = apptBl.getAllUsers(varSurveyorParam);

                //}
                return userData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion

        #region delete Stock Appointment
        /// <summary>
        /// This function delete the appointment in the database
        /// </summary>
        /// <param name="appointmentId">Appointment Id</param>
        /// <returns>returns true based on success and false otherwise </returns>
        public ResponseTemplate<List<DelAppointmentData>> deleteStockAppointment(DelAppointmentParam varDelAppointmentParam)
        {
            try
            {
                CheckLogger();
                AuthenticationBl authBl = new AuthenticationBl();
                ResponseTemplate<List<DelAppointmentData>> objResponseTemplate = new ResponseTemplate<List<DelAppointmentData>>();
                //if (authBl.checkForValidSession(varDelAppointmentParam.username, varDelAppointmentParam.salt))
                //{
                AppointmentBl apptBl = new AppointmentBl();
                return apptBl.deleteStockAppointment(varDelAppointmentParam.appointmentid);
                //}

                //return objResponseTemplate;
            }
            catch (Exception ex)
            {
                log.Error("******************  Delete Stock Appointment  ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }
        #endregion

        #region get Invalid Appointment

        /// <summary>
        /// This function returns an Invalid appointment for security
        /// </summary>
        /// <param name="appointmentid"></param>
        /// <returns>appointment data </returns>

        public AppointmentListStock returnInValidAppointment()
        {
            try
            {
                AppointmentListStock apptData = new AppointmentListStock();

                apptData.appointmentId = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        public AppointmentListGas returnInValidAppointmentGas()
        {
            try
            {
                AppointmentListGas apptData = new AppointmentListGas();

                apptData.appointmentId = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }


        public StatusData returnInValidStatusData()
        {
            try
            {
                StatusData apptData = new StatusData();

                apptData.statusId = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion

        #region Update appointment status

        public SyncAppointmentStatusResponse updateAppointmentStatus(SyncAppointmentStatusRequest request)
        {
            try
            {
                CheckLogger();
                AppointmentBl apptBl = new AppointmentBl();
                return apptBl.updateAppointmentStatus(request);
            }
            catch (Exception ex)
            {
                log.Error("******************  Change appointment status  ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region  Complete Appointment

        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>
        public Stream completeAppointment(AppointmentContractData varSaveCompleteAppointmentData)
        {
            try
            {
                CheckLogger();
                AppointmentBl apptBl = new AppointmentBl();
                var serializer = new JavaScriptSerializer();
                string jsonClient;
                jsonClient = serializer.Serialize(apptBl.completeAppointmentV2(varSaveCompleteAppointmentData));

                WebOperationContext.Current.OutgoingResponse.ContentType ="application/json; charset=utf-8";
                return new MemoryStream(Encoding.UTF8.GetBytes(jsonClient));
            }

            catch (Exception ex)
            {
                log.Error("******************  Complete Appointment  ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region validate Complete Appointment Data
        /// <summary>
        /// This function validates the appointment data such as appointment type, appointment status, surveyour availability
        /// </summary>
        /// <param name="apptData">the object of appointment data</param>
        private void validateCompleteAppointmentData(AllAppointmentsList varSaveCompleteAppointmentData)
        {
            //validate the appointment type
            if (!varSaveCompleteAppointmentData.appointmentType.Equals(null) && !varSaveCompleteAppointmentData.appointmentType.Equals(String.Empty))
            {
                if (varSaveCompleteAppointmentData.validateAppointmentTypes(varSaveCompleteAppointmentData.appointmentType) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointTypeInvalidValueMsg, varSaveCompleteAppointmentData.appointmentType), true, MessageCodesConstants.AppointmentTypeInvalidSelectionCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointTypeInvalidValueMsg, varSaveCompleteAppointmentData.appointmentType), "appointmentType");
                }
            }

            //validate the appointment progress status
            if (varSaveCompleteAppointmentData.validateAppointmentProgressStatus(varSaveCompleteAppointmentData.appointmentStatus) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, varSaveCompleteAppointmentData.appointmentStatus), true, MessageCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, varSaveCompleteAppointmentData.appointmentStatus), "appointmentStatus");
            }

            //validate the surveyour availability status
            if (varSaveCompleteAppointmentData.validateSurveyourAvailabilityStatus(varSaveCompleteAppointmentData.surveyourAvailability) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, varSaveCompleteAppointmentData.surveyourAvailability), true, MessageCodesConstants.SurveyourStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, varSaveCompleteAppointmentData.surveyourAvailability), "surveyourAvailability");
            }


        }

        #endregion

        #region Populate Stock Inspection Documents.
        /// <summary>
        /// This function used to Populate Stock Inspection Documents.
        /// </summary>        
        /// <returns>string Message</returns>

        public void populateStockInspection(StockInspectionDocument stockInspectionDocList)
        {
            //MessageData objMessageData = new MessageData();
            //ResponseTemplate<string> objResponseTemplate = new ResponseTemplate<string>();
            AppointmentBl apptBl = new AppointmentBl();
           apptBl.populateStockInspection(stockInspectionDocList); 
            
            //<Status Information Start Here>
            //objMessageData.code = MessageCodesConstants.successCode;
            //objMessageData.message = MessageConstants.success;
            //objResponseTemplate.status = objMessageData;
            //return objResponseTemplate;
            
        }
        #endregion
    }
}
