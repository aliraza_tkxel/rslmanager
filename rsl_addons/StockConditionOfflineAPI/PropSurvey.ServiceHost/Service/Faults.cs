﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Faults;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.IO;
using System.Web;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class Faults : IFaults
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private void CheckLogger()
        {
            try
            {

                string logFile = "";
                string strconfig = "\\log";
                if (strconfig != string.Empty)
                {
                    logFile = HttpContext.Current.Server.MapPath("../Logger.Config");
                    strconfig = string.Empty;
                }

                //if (System.IO.File.Exists(logFile))
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(logFile));
            }
            catch (Exception ex)
            {
                log.Error("****************** Exception Block While Create log file ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

            }

        }

        #region get IFaults Data
        /// <summary>
        /// This function returns the IFaults data
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        public List<FaultsData> getFaultsData(int AppointmentID, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsData(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }


        /// <summary>
        /// This function returns the Faults data for gas
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        public ResponseTemplate<GasDefectsData> fetchGasDefects(RequestGasDefectsData requestData)
        {
            try
            {
                FaultsBl fltBl = new FaultsBl();
                //AuthenticationBl authBl = new AuthenticationBl();
                // authBl.checkForValidSession(requestData.username, requestData.salt);
                return fltBl.fetchGasDefects(requestData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }


        #endregion

        #region get general comments Data
        /// <summary>
        /// This function returns general comments
        /// </summary>
        /// <returns>List of general comments objects</returns>        
        public List<FaultsData> getGeneralComments(int AppointmentID, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getGeneralComments(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns general comments for gas
        /// </summary>
        /// <returns>List of general comments objects</returns>
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getGeneralCommentsGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#21 - Behroz - 19/12/2012 - End
        #endregion

        #region get faults data only
        /// <summary>
        /// This function returns faults data
        /// </summary>
        /// <returns>List of faults data objects</returns>
        public List<FaultsData> getFaultsOnly(int AppointmentID, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsOnly(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns faults data for gas
        /// </summary>
        /// <returns>List of faults data objects</returns>
        public List<FaultsDataGas> getFaultsOnlyGas(int journalId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsOnlyGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#22 - Behroz - 19/12/2012 - End

        #endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        public int saveFaultsData(FaultsData faultData, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateFaultsData(faultData);
                return apptBl.saveFaultsData(faultData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region save Faults Data Gas

        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public ResponseTemplate<FaultsDataGas> saveFaultsDataGas(RequestDefectDataGas faultData)
        {
            try
            {
                CheckLogger();

                FaultsBl apptBl = new FaultsBl();
                //AuthenticationBl authBl = new AuthenticationBl();
                //authBl.checkForValidSession(faultData.username, faultData.salt);
                apptBl.validateFaultsData(faultData.defect);
                return apptBl.saveFaultsDataGas(faultData.defect,faultData.username);
            }
            catch (Exception ex)
            {

                log.Error("******************  Complete Appointment  ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateFaultData(FaultsData faultData, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateFaultsData(faultData);
                return apptBl.updateFaultData(faultData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#20 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        //public bool updateFaultDataGas(FaultsDataGas faultData, string userName, string salt)
        //{
        //    try
        //    {
        //        FaultsBl apptBl = new FaultsBl();
        //        AuthenticationBl authBl = new AuthenticationBl();
        //        authBl.checkForValidSession(userName, salt);
        //        apptBl.validateFaultsData(faultData);
        //        return apptBl.updateFaultDataGas(faultData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
        //    }
        //}
        ////Change#20 - Behroz - 19/12/2012 - End
        #endregion

        #region validate Faults Data
        /// <summary>
        /// This function validate Faults Data
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>

        public void validateFaultsData(FaultsData insData)
        {
            FaultsData validate = new FaultsData();
            if (!(validate.validateFaultsCategories(insData.FaultCategory)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidFaultCategory, true, MessageCodesConstants.InvalidFaultCategory);
                throw new ArgumentException(string.Format(MessageConstants.InvalidFaultCategory, insData.FaultCategory));
            }
        }



        #endregion

        #region "Get All Defect Categories"

        //Change#15 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of defect categories</returns>
        //public List<DefectCategoryData> getDefectCategory(string userName, string salt)
        //{
        //    try
        //    {
        //        FaultsBl apptBl = new FaultsBl();
        //        AuthenticationBl authBl = new AuthenticationBl();
        //        authBl.checkForValidSession(userName, salt);
        //        return apptBl.getDefectCategories();
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
        //    }
        //}
        //Change#15 - Behroz - 12/17/2012 - End

        #endregion

        #region "Get Fault Id"
        //Change#51 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function return the fault id
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public int getFaultId(string propertyId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultId(propertyId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#51 - Behroz - 16/01/2013 - End
        #endregion

        #region "Delete Fault"

        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="faultId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public ResponseTemplate<bool> deleteFaultImage(RequestDeleteDefectImageData deleteDefectImage)
        {
            try
            {
                FaultsBl propBl = new FaultsBl();
                int propPicId = (int)deleteDefectImage.faultImageId;
                bool success = true;
                FaultImagesData flImageData = new FaultImagesData();

                flImageData = propBl.deleteFaultImage(propPicId);
                if (flImageData.ImageTitle != string.Empty && flImageData.ImageTitle != null)
                {
                    string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                    string imageFullPath = FileHelper.getPhysicalPropertyImageUploadPath(deleteDefectImage.propertyId) + flImageData.ImageTitle;
                    string thumbnilUploadPath = FileHelper.getPhysicalPropertyImageUploadPath(deleteDefectImage.propertyId) + flImageData.ImageTitle;

                    if (File.Exists(imageFullPath))
                    {
                        File.Delete(imageFullPath);
                    }

                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.DefectPictureIdDoesNotExistMsg, deleteDefectImage.faultImageId), true, MessageCodesConstants.PropertyPictureIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.DefectPictureIdDoesNotExistMsg, deleteDefectImage.faultImageId), "fileName");

                }
                //  return success;
                ResponseTemplate<bool> objResponseTemplate = new ResponseTemplate<bool>();
                objResponseTemplate.response = success;
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region Save defect Image

        /// <summary>
        ///  This function saves the image path in the database and the image on the local drive
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="faultId"></param>
        /// <param name="fileExt"></param>
        /// <param name="propertyId"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <param name="imageIdentifier"></param>
        /// <returns></returns>
        public ResponseTemplate<ResponseUploadDefectPictureData> saveDefectImage(Stream stream, int faultId, string fileExt, string propertyId,int schemeId ,int blockId,string userName, string salt, string imageIdentifier = "")
        {
            try
            {
                FaultsBl faultsBl = new FaultsBl();
                //AuthenticationBl authBl = new AuthenticationBl();
                //authBl.checkForValidSession(userName, salt);
                string imageName;
                int propertyDefectImageId;

                bool isPropertyDefectImageAlreadyExists = faultsBl.ispropertyDefectImageAlreadyExists(imageIdentifier, out imageName, out propertyDefectImageId);

                if (!isPropertyDefectImageAlreadyExists)
                {
                    long fileSize = long.Parse(HttpContext.Current.Request.Headers["Content-Length"]);
                    faultsBl.validateFaultsId(faultId);

                    //Generate Path               
                    imageName = "Defect_" + FileHelper.generateUniqueString() + fileExt;

                    string completePath = string.Empty;
                    if (string.IsNullOrEmpty(propertyId))
                    {
                        // for Scheme Block Image
                        completePath = FileHelper.getPhysicalSchemeBlockImageUploadPath(schemeId, blockId);
                    }
                    else
                    {
                        // for Property Image
                        completePath = FileHelper.getPhysicalPropertyImageUploadPath(propertyId);
                    }                    

                    if (!Directory.Exists(completePath))
                    {
                        Directory.CreateDirectory(completePath);
                    }
                    //Save Image
                    FileHelper.saveImageOnDisk(stream, completePath + imageName, fileSize);
                    propertyDefectImageId = faultsBl.saveFaultImage(imageName, faultId, imageIdentifier: imageIdentifier);
                }
                ResponseUploadDefectPictureData responseData = new ResponseUploadDefectPictureData();
                responseData.PropertyDefectImageId = propertyDefectImageId;
                responseData.imagePath = FileHelper.getLogicalPropertyImagePath(propertyId, imageName);
                ResponseTemplate<ResponseUploadDefectPictureData> objResponseTemplate = new ResponseTemplate<ResponseUploadDefectPictureData>();
                objResponseTemplate.response = responseData;
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair</returns>
        public ResponseTemplate<ResponseFaultRepairData> getFaultRepairList(RequestFaultRepair faultRepairData)
        {
            try
            {
                // AuthenticationBl authBl = new AuthenticationBl();
                // authBl.checkForValidSession(faultRepairData.username, faultRepairData.salt);

                FaultsBl apptBl = new FaultsBl();
                return apptBl.getFaultRepairList(faultRepairData.faultLogId);

                //WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(faultRepairlist.ToString()));

                //return ms;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        public ResponseTemplate<ResponsePauseReasonList> getPauseReasonList(RequestFault resuestFaultReason)
        {
            try
            {
                //AuthenticationBl authBl = new AuthenticationBl();
                //authBl.checkForValidSession(resuestFaultReason.username, resuestFaultReason.salt);

                FaultsBl apptBl = new FaultsBl();
                return apptBl.getPauseReasonList();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region Save Fault repair Image

        /// <summary>
        /// This function saves the image path in the database and the image on the local drive
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="faultId"></param>
        /// <param name="fileName"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public ResponseTemplate<ResponseSaveRepairPictureData> saveFaultRepairImage(Stream stream, string propertyId, int schemeId, int blockId, string jsNumber, string fileExt, int createdBy, bool isBeforeImage, string userName, string salt, string appointmentType, string imageIdentifier = "")
        {
            try
            {
                FaultsBl faultsBl = new FaultsBl();
                //AuthenticationBl authBl = new AuthenticationBl();
                //authBl.checkForValidSession(userName, salt);


                ResponseSaveRepairPictureData responseData = new ResponseSaveRepairPictureData();
                ResponseTemplate<ResponseSaveRepairPictureData> objResponseTemplate = new ResponseTemplate<ResponseSaveRepairPictureData>();
                long fileSize = long.Parse(HttpContext.Current.Request.Headers["Content-Length"]);
                string imageName = string.Empty;
                int faultRepairImageId;
                string completePath = string.Empty;
                bool isImageAlreadyExists = false;

                if (appointmentType == "Appliance Defect")
                    isImageAlreadyExists = faultsBl.ispropertyDefectImageAlreadyExists(imageIdentifier, out imageName, out faultRepairImageId);
                else isImageAlreadyExists = faultsBl.isFaultRepairImageAlreadyExists(imageIdentifier, out imageName, out faultRepairImageId);

                if (!isImageAlreadyExists)
                {
                    #region Get Unique Image Name
                    //Generate Path               
                    if (appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidWorks.ToString())
                    {
                        imageName = "VoidWorks_" + FileHelper.generateUniqueString() + fileExt;
                    }
                    else if (appointmentType == "Fault")
                    {
                        imageName = "Repair_" + FileHelper.generateUniqueString() + fileExt;
                    }
                    else if (appointmentType == "Appliance Defect")
                    {
                        imageName = "Defect_" + FileHelper.generateUniqueString() + fileExt;
                    }
                    #endregion

                    if (string.IsNullOrEmpty(propertyId))
                    {
                        // for Scheme Block Image
                        completePath = FileHelper.getPhysicalSchemeBlockImageUploadPath(schemeId, blockId);
                    }
                    else
                    {
                        // for Property Image
                        completePath = FileHelper.getPhysicalPropertyImageUploadPath(propertyId);
                    }

                    //Create Directory if Not Exists 

                    if (!Directory.Exists(completePath))
                    {
                        Directory.CreateDirectory(completePath);
                    }
                    //Save Image
                    FileHelper.saveImageOnDisk(stream, completePath + imageName, fileSize);

                    if (appointmentType == "Appliance Defect")
                    {
                        int defectId = int.Parse(jsNumber.Replace("JSD", ""));
                        faultRepairImageId = faultsBl.saveFaultImage(imageName, defectId, isBeforeImage, imageIdentifier);
                    }
                    else
                    {
                        //Save Property Repair Image
                        faultRepairImageId = faultsBl.saveFaultRepairImage(propertyId, schemeId, blockId, jsNumber, imageName, (bool)isBeforeImage, createdBy, appointmentType, imageIdentifier);
                    }

                }
                responseData.FaultRepairImageId = faultRepairImageId;

                if (string.IsNullOrEmpty(propertyId))
                {
                    responseData.imagePath = FileHelper.getLogicalSchemeBlockImagePath(schemeId, blockId, imageName);
                }
                else
                {
                    responseData.imagePath = FileHelper.getLogicalPropertyImagePath(propertyId, imageName);
                }

                objResponseTemplate.response = responseData;
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

    }
}