﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Property;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Validate;
using System.Web;

namespace PropSurvey.ServiceHost.Service
{
    // Start the service and browse to http://<machine_name>:<port>/AppointmentService/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	    
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public class PropertyService : IProperty
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private void CheckLogger()
        {
            try
            {

                string logFile = "";
                string strconfig = "\\log";
                if (strconfig != string.Empty)
                {
                    logFile = HttpContext.Current.Server.MapPath("../Logger.Config");
                    strconfig = string.Empty;
                }

                //if (System.IO.File.Exists(logFile))
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(logFile));
            }
            catch (Exception ex)
            {
                log.Error("****************** Exception Block While Create log file ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

            }

        }

        #region find Property
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sure name of customer</param>
        /// <returns>It returns the list of customer data's object</returns>

        public ResponseTemplate<PropertiesListData> findProperty(PropertySearchParam varPropertySearchParam)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                ResponseTemplate<PropertiesListData> propertiesListData = new ResponseTemplate<PropertiesListData>();

                propertiesListData = propBl.findProperty(varPropertySearchParam);

                return propertiesListData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findPropertyGas(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                List<CustomerData> custData = new List<CustomerData>();

                custData = propBl.findPropertyGas(skip, top, reference, houseNumber, street, postCode, surName);

                return custData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Property Image Names

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="itemId">item id</param>
        /// <returns>returns the list of image names against property</returns>

        public List<PropertyPictureData> getPropertyImageNames(string propertyId, int itemId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                List<PropertyPictureData> propList = new List<PropertyPictureData>();
                propList = propBl.getPropertyImages(propertyId, itemId);

                return propList;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Property Image

        /// <summary>
        /// This fucntion returns the property images against property name
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="imageName">image name </param>
        /// <returns></returns>
        public Stream getPropertyImage(string propertyId, string imageName)
        {
            try
            {

                string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                string imageType = FileHelper.getPropertyImageType();
                string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propertyId, imageName);

                try
                {

                    Stream imageStream = File.OpenRead(imageFullPath);
                    WebOperationContext.Current.OutgoingResponse.ContentType = imageType;
                    return imageStream;
                }
                catch (FileNotFoundException fileNotFoundException)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyImageNotFoundMsg, true, MessageCodesConstants.FileNotFoundExceptionCode);
                    throw fileNotFoundException;
                }

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Property Default Image

        /// <summary>
        /// This function returns the default image which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>it returns the stream of image (if found)</returns>

        public Stream getDefaultPropertyImage(String propertyId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();

                String imageName = propBl.getDefaultPropertyImageName(propertyId);

                if (!imageName.Equals(String.Empty))
                {
                    string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                    string imageType = FileHelper.getPropertyImageType();
                    string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propertyId, imageName);

                    try
                    {
                        Stream imageStream = File.OpenRead(imageFullPath);
                        WebOperationContext.Current.OutgoingResponse.ContentType = imageType;

                        return imageStream;
                    }
                    catch (FileNotFoundException fileNotFoundException)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyImageNotFoundMsg, true, MessageCodesConstants.FileNotFoundExceptionCode);
                        throw fileNotFoundException;
                    }
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyImageNotFoundMsg, true, MessageCodesConstants.DatabaseEntryNotFoundCode);
                    throw new FileNotFoundException();
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

       #region Save Heating Inspection Image

        public ResponseTemplate<ResponseUploadHeatingInspectionImage> saveHeatingInspectionImage(Stream stream)
        {
            try
            {
                CheckLogger();

                var request = System.Web.HttpContext.Current.Request;

                string propertyId = string.IsNullOrEmpty(request.QueryString["propertyId"]) ? null : request.QueryString["propertyId"];
                int  schemeId = string.IsNullOrEmpty(request.QueryString["schemeId"]) ? 0 : Convert.ToInt32(request.QueryString["schemeId"]);
                int blockId = string.IsNullOrEmpty(request.QueryString["blockId"]) ? 0 : Convert.ToInt32(request.QueryString["blockId"]);
                int journalId = string.IsNullOrEmpty(request.QueryString["journalId"]) ? 0 : Convert.ToInt32(request.QueryString["journalId"]);
                string fileExt = string.IsNullOrEmpty(request.QueryString["fileExt"]) ? null : request.QueryString["fileExt"];
                string username = string.IsNullOrEmpty(request.QueryString["username"]) ? null : request.QueryString["username"];
                string salt = string.IsNullOrEmpty(request.QueryString["salt"]) ? null : request.QueryString["salt"];
                string imageIdentifier = string.IsNullOrEmpty(request.QueryString["imageIdentifier"]) ? null : request.QueryString["imageIdentifier"];
                int heatingId = string.IsNullOrEmpty(request.QueryString["heatingId"]) ? 0 : Convert.ToInt32(request.QueryString["heatingId"]);
                int updatedby = string.IsNullOrEmpty(request.QueryString["updatedby"]) ? 0 : Convert.ToInt32(request.QueryString["updatedby"]);

                PropertyBl propBl = new PropertyBl();

                string imageName;
                int heatingInspectionImageId;

                bool isImageExists = propBl.isHeatingInspectionImageAlreadyExists(imageIdentifier, out imageName, out heatingInspectionImageId);

                if (!isImageExists)
                {
                    long fileSize = long.Parse(HttpContext.Current.Request.Headers["Content-Length"]);
                    propBl.validateHeatingId(heatingId);

                    //Generate Path               
                    imageName = "Insp_" + FileHelper.generateUniqueString() + fileExt;

                    string completePath = (string.IsNullOrEmpty(propertyId)) 
                                            ? FileHelper.getPhysicalSchemeBlockImageUploadPath(schemeId, blockId)
                                            : FileHelper.getPhysicalPropertyImageUploadPath(propertyId);


                    if (!Directory.Exists(completePath))
                    {
                        Directory.CreateDirectory(completePath);
                    }

                    string imagePath = (string.IsNullOrEmpty(propertyId))
                                            ? FileHelper.getSchemeBlockImageUploadPath()
                                            : FileHelper.getPropertyImageUploadPath();

                    //Save Image
                    FileHelper.saveImageOnDisk(stream, completePath + imageName, fileSize);

                    PropertyPictureData propPicData = new PropertyPictureData();
                    propPicData.propertyPictureName = imageName;
                    propPicData.imagePath = imagePath;
                    propPicData.propertyId = propertyId;
                    propPicData.schemeId = schemeId;
                    propPicData.blockId = blockId;
                    propPicData.journalId = journalId;
                    propPicData.createdBy = updatedby;
                    propPicData.ImageIdentifier = imageIdentifier;
                    propPicData.heatingMappingId = heatingId;

                    heatingInspectionImageId = propBl.saveHeatingInspectionImageData(propPicData);
                }
                ResponseUploadHeatingInspectionImage responseData = new ResponseUploadHeatingInspectionImage();
                responseData.HeatingInspectionImageId = heatingInspectionImageId;
                responseData.imagePath = (string.IsNullOrEmpty(propertyId))
                                            ? FileHelper.getLogicalSchemeBlockImagePath(schemeId,blockId,imageName)
                                            : FileHelper.getLogicalPropertyImagePath(propertyId, imageName);                

                ResponseTemplate<ResponseUploadHeatingInspectionImage> objResponseTemplate = new ResponseTemplate<ResponseUploadHeatingInspectionImage>();
                objResponseTemplate.response = responseData;

                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {

                log.Error("******************  Save Heating Inspection Image ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region update Property Image

        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="surveyId">property id</param>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="itemId">item id</param>
        /// <param name="fileName">file name image</param>
        /// <param name="pictureTag">picutre tag , this 'll be like this: Internal;Roof;Dwelling</param>
        /// <param name="isDefault">true or false</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public ResponseTemplate<ResponseUploadPictureData> uploadPropertyImage(Stream stream, string propertyId, int appointmentId, int itemId, int updatedBy, string fileExt, string type, string userName, string salt, int propertyPicId = 0, bool isDefault = false, string imageIdentifier = "", int schemeId = 0, int blockId = 0, int heatingId = 0)
        {
            try
            {
                CheckLogger();

                PropertyBl propBl = new PropertyBl();
                ResponseUploadPictureData responseData = new ResponseUploadPictureData();
                string imageName = string.Empty;

                bool isPropertyImageAlreadyExists = false;
                isPropertyImageAlreadyExists =  propBl.isPropertyImageAlreadyExists(imageIdentifier, out imageName, out propertyPicId);

                if (!isPropertyImageAlreadyExists)
                {
                    if (propertyPicId > 0 && isDefault == true)
                    {
                        imageName = propBl.GetPicNameByPropertyPicId(propertyPicId);

                        if (!string.IsNullOrEmpty(propertyId)) {
                            propBl.updatePropertyDefaultImage(propertyId, propertyPicId);                        
                        }                        
                    }
                    else
                    {
                        long dataLength = 0;
                        string imageUploadPath = string.Empty;

                        //check the existance of property 
                        if (type.ToUpper() == MessageConstants.StockItemName.ToUpper() && !string.IsNullOrEmpty(propertyId))
                        {
                            propBl.isPropertyAppointmentExist(propertyId);
                        }

                        dataLength = long.Parse(HttpContext.Current.Request.Headers["Content-Length"]);
                        //  string imageExt = FileHelper.getFileExtension(fileName);

                        if (isDefault || type.ToUpper() == MessageConstants.GasAppointmentType.ToUpper()
                                      || type.ToUpper() == ApplicationConstants.AlternativeAppointmentType.ToUpper()
                                      || type.ToUpper() == ApplicationConstants.OilAppointmentType.ToUpper() 
                                      || type.ToUpper() == MessageConstants.StockItemName.ToUpper() 
                                      || type.ToUpper() == MessageConstants.FaultAppointmentType.ToUpper() 
                                      || type.ToUpper() == MessageConstants.PlannedAppointmentType.ToUpper())
                        {
                            imageName = type.Replace(" ","") + "_" + FileHelper.generateUniqueString() + fileExt;
                        }
                        else
                        {
                            ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidImageType, true, MessageCodesConstants.ImageExtensionInvalidCode);
                            throw new ArgumentException(MessageConstants.InvalidImageType, "type");
                        }

                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            imageUploadPath = FileHelper.getPhysicalPropertyImageUploadPath(propertyId);
                        }
                        else {
                            imageUploadPath = FileHelper.getPhysicalSchemeBlockImageUploadPath(schemeId,blockId);
                        }
                        
                        if (!Directory.Exists(imageUploadPath))
                        {
                            Directory.CreateDirectory(imageUploadPath);
                        }

                        if (Validator.validateImageFileExtension(fileExt) == false)
                        {
                            ErrorFaultSetGet.setErrorFault(MessageConstants.ImageExtensionInvalidMsg, true, MessageCodesConstants.ImageExtensionInvalidCode);
                            throw new ArgumentException(MessageConstants.ImageExtensionInvalidMsg, "fileName");
                        }

                        //this function can throw io exception
                        //Save Image
                        FileHelper.saveImageOnDisk(stream, imageUploadPath + imageName, dataLength);

                        PropertyPictureData propPicData = new PropertyPictureData();
                        propPicData.propertyPictureName = imageName;
                        propPicData.imagePath = FileHelper.getPropertyImageUploadPath();
                        propPicData.isDefault = isDefault;
                        propPicData.itemId = itemId;

                        if (type.ToUpper() == MessageConstants.GasAppointmentType.ToUpper())
                            propPicData.itemId = propBl.getItemId();
                        propPicData.propertyId = propertyId;
                        propPicData.schemeId = schemeId;
                        propPicData.blockId = blockId;
                        propPicData.appointmentId = appointmentId;
                        propPicData.createdBy = updatedBy;
                        propPicData.ImageIdentifier = imageIdentifier;
                        propPicData.heatingMappingId = heatingId;
                        
                        //int propertyPicId = 0;

                        if (isDefault == true)
                        {
                            propertyPicId = propBl.savePropertyImageGas(propPicData);                            

                            if (!string.IsNullOrEmpty(propertyId))                            
                                propBl.updatePropertyDefaultImage(propertyId, propertyPicId);
                            
                        }
                        else if (type.ToUpper() == MessageConstants.StockItemName.ToUpper())
                        {
                            propertyPicId = propBl.savePropertyImage(propPicData);
                        }
                        else
                        {
                            propertyPicId = propBl.savePropertyImageGas(propPicData);
                        }
                    }
                }

                responseData.propertyPictureId = propertyPicId;
                responseData.imagePath = FileHelper.getLogicalPropertyImagePath(propertyId, imageName);
                ResponseTemplate<ResponseUploadPictureData> objResponseTemplate = new ResponseTemplate<ResponseUploadPictureData>();
                objResponseTemplate.response = responseData;

                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {

                log.Error("******************  Upload Property Image ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);


                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        
        #endregion

        #region delete Property Image
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>true if file deleted or not but return false in case of any error while connecting with database or property picture id is not valid integer</returns>

        public ResponseTemplate<bool> deletePropertyImage(DeletepropertyPictureData deletepropertyPictureData)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                PropertyPictureData propPicData = new PropertyPictureData();
                int propPicId = (int)deletepropertyPictureData.propertyPictureId;
                bool success = true;


                propPicData = propBl.deletePropertyImage(propPicId);

                if (propPicData.propertyPictureName != string.Empty && propPicData.propertyPictureName != null)
                {
                    string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                    string imageFullPath = FileHelper.getPhysicalPropertyImageUploadPath(propPicData.propertyId) + propPicData.propertyPictureName;
                    string thumbnilUploadPath = FileHelper.getPhysicalPropertyImageUploadPath(propPicData.propertyId) + propPicData.propertyPictureName;

                    if (File.Exists(imageFullPath))
                    {
                        File.Delete(imageFullPath);
                    }
                    if (File.Exists(thumbnilUploadPath))
                    {
                        File.Delete(thumbnilUploadPath);
                    }

                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyPictureIdDoesNotExistMsg, deletepropertyPictureData.propertyPictureId), true, MessageCodesConstants.PropertyPictureIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.PropertyPictureIdDoesNotExistMsg, deletepropertyPictureData.propertyPictureId), "fileName");
                }

                //  return success;
                ResponseTemplate<bool> objResponseTemplate = new ResponseTemplate<bool>();
                objResponseTemplate.response = success;
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>property dimension data object</returns>
        public List<PropertyDimData> getPropertyDimensions(string propertyId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                return propBl.getPropertyDimensions(propertyId);

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region update Property Dimensions

        ///// <summary>
        ///// This function updates the property dimensions
        ///// </summary>
        ///// <param name="propDimData">This function accepts the property dimension data object</param>
        ///// <returns>true on successful update and false on un successful update</returns>
        //public bool updatePropertyDimensions(List<PropertyDimData> propertyDimBo)
        //{
        //    try
        //    {
        //        PropertyBl propBl = new PropertyBl();

        //        //varifies property id exists in database against any appointment
        //        propBl.isPropertyAppointmentExist(propertyDimBo[0].propertyId);
        //        return propBl.updatePropertyDimensions(propertyDimBo);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
        //    }
        //}

        #endregion
                
        #region Update Property Default Image

        /// <summary>
        /// This funciton updates the property default image in the database
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="surveyId">property id</param>
        /// <param name="propertyPicId">property Picture Id</param>
        /// <param name="updatedBy">updated By</param>
        /// <param name="fileExt">file Extension</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public ResponseTemplate<ResponseUploadPictureData> updatePropertyDefaultImage(Stream stream, string propertyId, int propertyPicId, int updatedBy, string fileExt)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                ResponseUploadPictureData responseData = new ResponseUploadPictureData();
                string imageName = string.Empty;

                if (propertyPicId > 0)
                {
                    imageName = propBl.GetPicNameByPropertyPicId(propertyPicId);
                    propBl.updatePropertyDefaultImage(propertyId, propertyPicId);
                }
                else
                {
                    PropertyPictureData propPicData = new PropertyPictureData();
                    string imageUploadPath = string.Empty;
                    
                    long dataLength = long.Parse(HttpContext.Current.Request.Headers["Content-Length"]);
                    imageName = MessageConstants.PropertyItemName.ToUpper() + "_" + FileHelper.generateUniqueString() + fileExt;
                    imageUploadPath = FileHelper.getPhysicalPropertyImageUploadPath(propertyId);

                    if (!Directory.Exists(imageUploadPath))
                    {
                        Directory.CreateDirectory(imageUploadPath);
                    }

                    if (Validator.validateImageFileExtension(fileExt) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.ImageExtensionInvalidMsg, true, MessageCodesConstants.ImageExtensionInvalidCode);
                        throw new ArgumentException(MessageConstants.ImageExtensionInvalidMsg, "fileName");
                    }

                    //this function can throw io exception
                    //Save Image
                    FileHelper.saveImageOnDisk(stream, imageUploadPath + imageName, dataLength);

                    propPicData.propertyPictureName = imageName;
                    propPicData.imagePath = FileHelper.getPropertyImageUploadPath();
                     //propPicData.itemId = itemId;
                    //if (type.ToUpper() == MessageConstants.GasItemName.ToUpper())
                    //    propPicData.itemId = propBl.getItemId();
                    propPicData.propertyId = propertyId;
                    //propPicData.appointmentId = appointmentId;
                    propPicData.createdBy = updatedBy;
                   
                    propertyPicId = propBl.savePropertyImageGas(propPicData);
                    propBl.updatePropertyDefaultImage(propertyId, propertyPicId);
                }

                //Return information to the response
                responseData.propertyPictureId = propertyPicId;
                responseData.imagePath = FileHelper.getLogicalPropertyImagePath(propertyId, imageName);
                ResponseTemplate<ResponseUploadPictureData> objResponseTemplate = new ResponseTemplate<ResponseUploadPictureData>();
                objResponseTemplate.response = responseData;

                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        
        #endregion
    }
}

