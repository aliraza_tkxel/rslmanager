﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using PropSurvey.BuisnessLayer.PropertyAgent;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PropertyAgentServices:IPropertyAgent
    {
        #region get All Property Agents List
        /// <summary>
        /// This function returns all the property agents list
        /// </summary>
        /// <returns>List of property agent data objects</returns>
        /// 

        public List<PropertyAgentData> getAllAgents(string userName, string salt)
        {
            try
            {
                List<PropertyAgentData> proAgentData = new List<PropertyAgentData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    PropertyAgentBl proAgentBl = new PropertyAgentBl();
                    proAgentData = proAgentBl.getAllPropertyAgent();
                }
                else
                {
                    proAgentData.Add(returnInValidPropertyAgent());
                }

                return proAgentData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion    
  
        #region update Property Agent

        public bool updatePropertyAgent(PropertyAgentData agentData, string userName, string salt)
        {
            try
            {
                bool success = false;
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    PropertyAgentBl proAgentBl = new PropertyAgentBl();
                    success = proAgentBl.UpdatePropertyAgent(agentData);
                }
                else
                {
                    success = false;
                }
                return success;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
       
        #endregion 

        #region save Property Agent

        public bool savePropertyAgent(PropertyAgentData agentData, string userName, string salt)
        {
            try
            {
                bool success = false;
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    PropertyAgentBl proAgentBl = new PropertyAgentBl();
                    success = proAgentBl.SavePropertyAgent(agentData);
                }
                else
                {
                    success = false;
                }
                return success;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
       
        #endregion 

        #region get Property Agent to Appointment
        /// <summary>
        /// This function returns Property Agent to Appointment
        /// </summary>
        /// <returns>It retruns Property Agent to Appointment's object</returns>

        public PropertyAgentToAppointmentData getAgentToAppointment(int AppointmentID, string userName, string salt)
        {
            try
            {
                PropertyAgentToAppointmentData proAgentData = new PropertyAgentToAppointmentData();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    PropertyAgentBl proAgentBl = new PropertyAgentBl();
                    proAgentData = proAgentBl.getAgentToAppointment(AppointmentID);
                }
                return proAgentData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Property Agent Details
        /// <summary>
        /// This function returns Property Agent Details
        /// </summary>
        /// <returns>It retruns Property Agent Details</returns>

        public PropertyAgentData getAgentDetails(int appoinmentId, string userName, string salt)
        {
            try
            {
                PropertyAgentData proAgentData = new PropertyAgentData();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    PropertyAgentBl proAgentBl = new PropertyAgentBl();
                    proAgentData = proAgentBl.getAgentDetails(appoinmentId);
                }
                return proAgentData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Update a Property Agent to Appointment
        /// <summary>
        /// This function updates a Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdateAgentToAppointment(PropertyAgentToAppointmentData proAgentData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                PropertyAgentBl proAgentBl = new PropertyAgentBl();
                authBl.checkForValidSession(userName, salt);
                return proAgentBl.UpdateAgentToAppointment(proAgentData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region save a Property Agent to Appointment
        /// <summary>
        /// This function saves  Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public int SaveAgentToAppointment(PropertyAgentToAppointmentData proAgentData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                PropertyAgentBl proAgentBl = new PropertyAgentBl();
                authBl.checkForValidSession(userName, salt);
                return proAgentBl.SaveAgentToAppointment(proAgentData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion


        #region invalid Property Agent data
        /// <summary>
        /// This function returns an Invalid Property Agent for security
        /// </summary>
        /// <returns>appointment data </returns>

        public PropertyAgentData returnInValidPropertyAgent()
        {
            try
            {
                PropertyAgentData proAgentData = new PropertyAgentData();

                proAgentData.ID = -1;
                return proAgentData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion
    }
}