﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Faults;
using PropSurvey.BuisnessLayer.Appointment;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class JobsService : IJobsService
    {
        #region get All Jobs Data
        /// <summary>
        /// This function returns all Jobs data of an operative
        /// </summary>
        /// <param name="operativeID">operative ID</param>
        /// <param name="userName">User Name</param>
        /// <param name="salt">salt</param>
        /// <returns>List of JobsData data objects</returns>
        public List<JobsData> getAllJobsData(int operativeID, string userName, string salt)
        {
            try
            {
                JobsBl jobsBl = new JobsBl();
                // AuthenticationBl authBl = new AuthenticationBl();
                // authBl.checkForValidSession(userName, salt);
                List<JobsData> listJobsData;
                listJobsData = jobsBl.getAllJobsData(operativeID);
                return listJobsData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region get All Jobs Data String
        /// <summary>
        /// This function returns all Jobs data of an operative
        /// </summary>
        /// <param name="operativeID">operative ID</param>
        /// <param name="userName">User Name</param>
        /// <param name="salt">salt</param>
        /// <returns>List of JobsData data objects</returns>
        public string getAllJobsDataString(int operativeID, string userName, string salt)
        {
            try
            {
                JobsBl jobsBl = new JobsBl();
                
                // AuthenticationBl authBl = new AuthenticationBl();
                // authBl.checkForValidSession(userName, salt);
                List<JobsData> listJobsData;
                listJobsData = jobsBl.getAllJobsData(operativeID);
                // Serialize the results as JSON
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(listJobsData.GetType());
                MemoryStream memoryStream = new MemoryStream();
                serializer.WriteObject(memoryStream, listJobsData);
                string json = Encoding.Default.GetString(memoryStream.ToArray());

                List<AppointmentDataStock> apptData = new List<AppointmentDataStock>();
                AuthenticationBl authBl = new AuthenticationBl();
               
                AppointmentBl apptBl = new AppointmentBl();
                apptData = apptBl.getAllStockAppointments();
               
                memoryStream.Flush();
                serializer = new DataContractJsonSerializer(apptData.GetType());
                serializer.WriteObject(memoryStream, apptData);

                json = json + Encoding.Default.GetString(memoryStream.ToArray());


                // Return the results serialized as JSON
               
                return json;               
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion
        
        //#region get Jobs by Status Data
        ///// <summary>
        ///// This function returns Jobs data by Status ID of an Operative
        ///// </summary>
        ///// <param name="operativeID">Operative id</param>
        ///// <param name="jobstatusID">Job Status ID</param>
        ///// <param name="userName">User Name</param>
        ///// <param name="salt">salt</param>
        ///// <returns>List of Jobs data objects</returns>
        //public List<JobsData> getJobsDataByStatusID(int operativeID, int jobstatusID, string userName, string salt)
        //{
        //    try
        //    {
        //        JobsBl jobsBl = new JobsBl();
        //        // AuthenticationBl authBl = new AuthenticationBl();
        //        // authBl.checkForValidSession(userName, salt);
        //        return jobsBl.getJobsDataByStatusID(operativeID, jobstatusID);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
        //    }
        //}
        //#endregion

       
    }
}