﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.OrgInspection;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class OrgInspection:IOrgInspection
    {     
        
        #region get Org list

        /// <summary>
        /// This function returns the Org list
        /// </summary>
        /// <returns>List of Org data objects</returns>

        public List<OrgData> getAllOrg(string userName, string salt)
        {
            try
            {
                List<OrgData> apptData = new List<OrgData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    apptData = orgBl.getAllOrg(userName);
                }
                else
                {
                    apptData.Add(returnInValidOrgData());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region get Gas Engineer list

        /// <summary>
        /// This function returns all the Gas Engineers working in a Org
        /// </summary>
        /// <returns>List of Emp data objects</returns>

        public List<EmployeData> getAllGasEngineer(string userName, string salt)
        {
            try
            {
                List<EmployeData> apptData = new List<EmployeData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    apptData = orgBl.getAllGasEngineer(userName);
                }
                else
                {
                    apptData.Add(returnInValidEmpData());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region get payment Type list

        /// <summary>
        /// This function returns all payment Types
        /// </summary>
        /// <returns>List of payment Type data objects</returns>
        public List<PaymentTypeData> getPaymentType(string userName, string salt)
        {
            try
            {
                List<PaymentTypeData> apptData = new List<PaymentTypeData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    apptData = orgBl.getPaymentType();
                }
                else
                {
                    apptData.Add(returnInValidPaymentTypeData());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region get Company types

        /// <summary>
        /// This function returns Company types
        /// </summary>
        /// <returns>List of Company type data objects</returns>
        public List<CompanyTypeData> getCompanyType(string userName, string salt)
        {
            try
            {
                List<CompanyTypeData> apptData = new List<CompanyTypeData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    apptData = orgBl.getCompanyType();
                }
                else
                {
                    apptData.Add(returnInValidCompanyData());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region get Org Inspection Form

        /// <summary>
        /// This function returns Org Inspected form
        /// </summary>
        /// <returns>List of org inspection data objects</returns>

        public OrgInspectionData getOrgInspection(int appointmentId, string userName, string salt)
        {
            try
            {
                OrgInspectionData apptData = new OrgInspectionData();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    apptData = orgBl.getOrgInspection(appointmentId);
                }
                else
                {
                    apptData = returnInValidOrgInspectionData();
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region save Org Inspection
        /// <summary>
        /// This function saves the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        public int saveOrgInspection(OrgInspectionData orgData, string userName, string salt)
        {
            try
            {
                int status = 0;
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    status = orgBl.saveOrgInspection(orgData);
                }
                else
                {
                    status = -1;
                }

                return status;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region update Org Inspection
        /// <summary>
        /// This function updates the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateOrgInspection(OrgInspectionData orgData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    return  orgBl.updateOrgInspection(orgData);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region Save a new Org
        /// <summary>
        /// This function saves a new Org
        /// </summary>
        /// <param name="appData">Org data objects</param>
        /// <returns>ID in case of success</returns>

        public int saveOrg(OrgData orgData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    OrgInspectionBl orgBl = new OrgInspectionBl();
                    return orgBl.saveOrg(orgData);
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion


        #region inValid OrgData, OrgInspectionData, EmpData

        public OrgData returnInValidOrgData()
        {
            try
            {
                OrgData orgData = new OrgData();

                orgData.ORGID = -1;
                return orgData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        public OrgInspectionData returnInValidOrgInspectionData()
        {
            try
            {
                OrgInspectionData orgData = new OrgInspectionData();

                orgData.ID = -1;
                return orgData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        public EmployeData returnInValidEmpData()
        {
            try
            {
                EmployeData orgData = new EmployeData();

                orgData.EmpID = -1;
                return orgData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        public CompanyTypeData returnInValidCompanyData()
        {
            try
            {
                CompanyTypeData orgData = new CompanyTypeData();

                orgData.ID = -1;
                return orgData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        public PaymentTypeData returnInValidPaymentTypeData()
        {
            try
            {
                PaymentTypeData orgData = new PaymentTypeData();

                orgData.ID = -1;
                return orgData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

    }
}