﻿using System;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.AppScreen;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using System.Web;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AppScreenService : IAppScreen
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public Stream getApplicationScreens(StockSurveyParam varStockSurveyParam)
        {
            try
            {

                string appScreenJson = string.Empty;

                AuthenticationBl authBl = new AuthenticationBl();
                //authBl.checkForValidSession(varStockSurveyParam.username, varStockSurveyParam.salt);
                AppScreenBl appScreenBl = new AppScreenBl();
                appScreenJson = appScreenBl.getApplicationScreens(varStockSurveyParam.propertyId, varStockSurveyParam.appointmentid);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(appScreenJson));
                //HttpContext.Current.Response.Headers.Add("Content-Length", ms.Length.ToString());  
                HttpContext.Current.Response.AddHeader("Content-Length", ms.Length.ToString());
                
                
                return ms;
                
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }



    }
}
