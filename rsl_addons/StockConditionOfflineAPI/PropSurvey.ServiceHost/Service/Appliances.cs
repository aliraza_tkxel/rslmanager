﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Appliances;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;
using PropSurvey.BuisnessLayer;
using PropSurvey.BuisnessLayer.Faults;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class Appliances : IAppliances
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private void CheckLogger()
        {
            try
            {
                CheckLogger();

                string logFile = "";
                string strconfig = "\\log";
                if (strconfig != string.Empty)
                {
                    logFile = HttpContext.Current.Server.MapPath("../Logger.Config");
                    strconfig = string.Empty;
                }

                //if (System.IO.File.Exists(logFile))
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(logFile));
            }
            catch (Exception ex)
            {
                log.Error("****************** Exception Block While Create log file ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);

            }

        }

        #region get Appliances Data
        /// <summary>
        /// This function returns all the Appliances Data
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        /// 
        public Stream getAppliancesData(AppliancesDataParam varAppliancesDataParam)
        {
            try
            {
                ResponseTemplate<AppliancesAllData> apptData = new ResponseTemplate<AppliancesAllData>();
                //AuthenticationBl authBl = new AuthenticationBl();
                //if (authBl.checkForValidSession(varAppliancesDataParam.username, varAppliancesDataParam.salt))
                //{
                AppliancesBl apptBl = new AppliancesBl();
                apptData = apptBl.getAppliancesData();

                //===================================================================
                // To get content length
                long content_Length = 0;

                var serializer = new JavaScriptSerializer();
                var outputJSON = serializer.Serialize(apptData);
                var ms = new MemoryStream(Encoding.UTF8.GetBytes(outputJSON));

                content_Length = ms.Length;
                //===================================================================

                //===================================================================
                // Set content length in response header
                HttpContext.Current.Response.AddHeader("Content-Length", content_Length.ToString());
                //===================================================================

                //}
                //else
                //{
                //    apptData.appliancesList.Add(returnInValidAppliance());
                //}

                return ms;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region Get Appliance Survey Data
        /// <summary>
        /// Get Appliance Survey Data
        /// </summary>
        /// <param name="requestParam"></param>
        /// <returns></returns>
        public ResponseTemplate<ApplianceSurveyData> getApplianceSurveyData(ApplianceSurveyDataParam requestParam)
        {

            try
            {
                ApplianceSurveyData surveyData = new ApplianceSurveyData();

                #region "Appliances"

                AppliancesBl appliancesBL = new AppliancesBl();
                surveyData.appliancesList = appliancesBL.getAllGasAppliances(requestParam);
                
                #endregion

                #region "Boilers"

                surveyData.boilers = appliancesBL.getAllHeatingBoilers(requestParam);
                
                #endregion

                #region "Detectors"

                DetectorBL detectorBL = new DetectorBL();
                surveyData.propertyDetectors = detectorBL.GetAllDetectors(requestParam);
               // surveyData.detectorsList = appliancesBL.getDetectorList(requestParam.propertyId, requestParam.appointmentid);

                #endregion
                
                #region "Meters"

                MeterBL meterBL = new MeterBL();
                Meter gasMeter = meterBL.GetGasMeter(requestParam);                
                surveyData.Meters.Add(gasMeter);
                Meter electricMeter = meterBL.GetElectricMeter(requestParam);
                surveyData.Meters.Add(electricMeter);

                #endregion

                ResponseTemplate<ApplianceSurveyData> objResponseTemplate = new ResponseTemplate<ApplianceSurveyData>();                
                objResponseTemplate.response = surveyData;
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Get Alternative Heating Survey
        /// <summary>
        /// Get Alternative Heating Survey
        /// </summary>
        /// <param name="requestParam"></param>
        /// <returns></returns>
        public ResponseTemplate<AlternativeSurveyData> getAlternativeSurveyData(AlternativeSurveyDataParam requestParam)
        {

            try
            {
                AlternativeSurveyData surveyData = new AlternativeSurveyData();
                AppliancesBl appliancesBL = new AppliancesBl();

                #region "Heatings"

                surveyData.heatings = appliancesBL.getAlternativeHeatings(requestParam);

                #endregion

                ResponseTemplate<AlternativeSurveyData> objResponseTemplate = new ResponseTemplate<AlternativeSurveyData>();
                objResponseTemplate.response = surveyData;
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Get Oil Heating Survey
        /// <summary>
        /// Get Alternative Heating Survey
        /// </summary>
        /// <param name="requestParam"></param>
        /// <returns></returns>
        public ResponseTemplate<OilSurveyData> getOilSurveyData(OilSurveyDataParam requestParam)
        {

            try
            {
                OilSurveyData surveyData = new OilSurveyData();
                AppliancesBl appliancesBL = new AppliancesBl();

                #region "Heatings"

                surveyData.oilHeatings = appliancesBL.getOilHeatings(requestParam);

                #endregion

                ResponseTemplate<OilSurveyData> objResponseTemplate = new ResponseTemplate<OilSurveyData>();
                objResponseTemplate.response = surveyData;
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region save Appliance
        /// <summary>
        /// This function saves the Appliance in the database
        /// </summary>
        /// <param name="appData">The object of Appliance</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public ResponseTemplate<ApplianceData> saveAppliance(CreateAppliancesData appData)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                //AuthenticationBl authBl = new AuthenticationBl();
                //authBl.checkForValidSession(appData.username, appData.salt);
                return apptBl.saveAppliance(appData.ApplianceData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region update Appliance
        /// <summary>
        /// This function updates the Appliance in the database
        /// </summary>
        /// <param name="appData">The object of Appliance</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public bool updateAppliance(ApplianceData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.updateAppliance(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region save Appliance Type
        /// <summary>
        /// This function saves the Appliance Type in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Type</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceType(ApplianceTypeData typeData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceType(typeData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region save Appliance Model
        /// <summary>
        /// This function saves the Appliance Model in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Model</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceModel(ApplianceModelData modelData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceModel(modelData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region save Appliance Location
        /// <summary>
        /// This function saves the Appliance Location in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Location</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceLocation(AppliancesLocationData locationData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceLocation(locationData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region save Appliance Manufacturer
        /// <summary>
        /// This function saves the Appliance Manufacturer in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Manufacturer</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceManufacturer(ManufacturerData manufactureData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceManufacturer(manufactureData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getInspectedAppliances(appointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }



        #endregion

        #region get ApplianceInspection Details by appointmentId
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>

        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionDetails(appointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionDetailsGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#40 - Behroz - 26/12/2012 - End
        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionByApplianceID(ApplianceID, appointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionByApplianceIDGas(ApplianceID, journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#26 - Behroz - 20/12/2012 - End
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateApplianceInspection(appData);
                return apptBl.saveApplianceInspection(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#24 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function saves a new ApplianceInspection for gas
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspectionGas(ApplianceInspectionData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateApplianceInspection(appData);
                return apptBl.saveApplianceInspectionGas(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#24 - Behroz - 20/12/2012 - End
        #endregion

        #region Update a existing ApplianceInspection
        /// <summary>
        /// This function updates a existing ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        //public bool updateApplianceInspection(ApplianceInspectionData appData, string userName, string salt)
        //{
        //    try
        //    {
        //        AppliancesBl apptBl = new AppliancesBl();
        //        AuthenticationBl authBl = new AuthenticationBl();
        //        authBl.checkForValidSession(userName, salt);
        //        validateApplianceInspection(appData);
        //        return apptBl.updateApplianceInspection(appData);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
        //    }
        //}
        #endregion

        #region "Get Appliance Count"

        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function return the applicance count
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                return apptBl.getApplianceCount(propertyId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        #region Validate ApplianceInspection Data
        private void validateApplianceInspection(ApplianceInspectionData appData)
        {
            ApplianceInspectionData appInsData = new ApplianceInspectionData();
            if (!(appInsData.validateInspectionOptions(appData.adequateVentilation) && appInsData.validateInspectionOptions(appData.applianceSafeToUse) && appInsData.validateInspectionOptions(appData.applianceServiced)
                && appInsData.validateInspectionOptions(appData.fluePerformanceChecks) && appInsData.validateInspectionOptions(appData.flueVisualCondition)
                && appInsData.validateInspectionOptions(appData.safetyDeviceOperational) && appInsData.validateInspectionOptions(appData.satisfactoryTermination)
                && appInsData.validateInspectionOptions(appData.smokePellet) && appInsData.validateInspectionOptions(appData.spillageTest)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.AppInspectionValueInvalid, true, MessageCodesConstants.AppInspectionValueInvalid);
                throw new ArgumentException(MessageConstants.AppInspectionValueInvalid);
            }
        }
        #endregion

        #region Invalid Type, Appliance, Location, Model and Manufacturer

        public ApplianceData returnInValidAppliance()
        {
            try
            {
                ApplianceData apptData = new ApplianceData();

                apptData.ApplianceID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public AppliancesLocationData returnInValidLocation()
        {
            try
            {
                AppliancesLocationData apptData = new AppliancesLocationData();

                apptData.LocationID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public ApplianceModelData returnInValidModel()
        {
            try
            {
                ApplianceModelData apptData = new ApplianceModelData();

                apptData.ApplianceModelID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public ApplianceTypeData returnInValidType()
        {
            try
            {
                ApplianceTypeData apptData = new ApplianceTypeData();

                apptData.ApplianceTypeID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public ManufacturerData returnInValidManufacturer()
        {
            try
            {
                ManufacturerData apptData = new ManufacturerData();

                apptData.ManufacturerID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion

        #region Update Detector Count


        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>
        //public ResponseTemplate<ResultBoolData> updateDetectorCount(DetectorCountData detectorCountData)
        //{
        //    try
        //    {
        //        AppliancesBl apptBl = new AppliancesBl();
        //        //AuthenticationBl authBl = new AuthenticationBl();
        //        //authBl.checkForValidSession(detectorCountData.userName, detectorCountData.salt);                 
        //        ResponseTemplate<ResultBoolData> objResponseTemplate = new ResponseTemplate<ResultBoolData>();
        //        MessageData objMessageData = new MessageData();
        //        objResponseTemplate.response = apptBl.updateDetectorCount(detectorCountData);
        //        //<Status Information Start Here>
        //        objMessageData.code = MessageCodesConstants.successCode;
        //        objMessageData.message = MessageConstants.success;
        //        objResponseTemplate.status = objMessageData;

        //        return objResponseTemplate;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
        //    }
        //}

        #endregion        

    }
}