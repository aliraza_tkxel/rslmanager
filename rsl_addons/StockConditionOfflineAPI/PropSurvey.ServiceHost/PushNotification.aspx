﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PushNotification.aspx.cs" Inherits="PropSurvey.ServiceHost.PushNotification" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1 {
            margin-bottom: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
        <br />
        <br />
        <asp:Label Text="Device Token: " runat="server" />
        <asp:TextBox runat="server" ID="txtDeviceToken"
            MaxLength="72" Width="491px" /><br />
        <asp:Label Text="Message: " runat="server"  />
        <asp:TextBox runat="server" ID="txtMessage" TextMode="MultiLine" 
            MaxLength="255" Width="520px" CssClass="style1" Height="88px" /><br />
        <asp:Label Text="Certificate Path" runat="server" />
        <asp:FileUpload ID="FileUpload1" runat="server" Width="489px">
        </asp:FileUpload><br />
        <asp:Label Text="Password" runat="server" />
        <asp:TextBox runat="server" ID="txtPassword" Width="428px" 
            TextMode="Password" Text="tkxel" /><br />
        <asp:CheckBox Text="Sandbox" runat="server" ID="chkSandbox" /><br />
        <asp:Button Text="Send" runat="server" ID="btnSend" onclick="btnSend_Click"/>
    </div>
    </form>
</body>
</html>
