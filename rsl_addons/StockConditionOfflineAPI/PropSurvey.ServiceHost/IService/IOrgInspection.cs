﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file

    interface IOrgInspection
    {
        #region get Org list

        /// <summary>
        /// This function returns the Org list
        /// </summary>
        /// <returns>List of Org data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetOrgList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetOrgList)]
        List<OrgData> getAllOrg(string userName, string salt);

        #endregion

        #region get Gas Engineer list

        /// <summary>
        /// This function returns all the Gas Engineers working in a Org
        /// </summary>
        /// <returns>List of Emp data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetGasEngineerList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetGasEngineerList)]
        List<EmployeData> getAllGasEngineer(string userName, string salt);

        #endregion

        #region get payment Type list

        /// <summary>
        /// This function returns all payment Types
        /// </summary>
        /// <returns>List of payment Type data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetPaymentTypes)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetPaymentTypes)]
        List<PaymentTypeData> getPaymentType(string userName, string salt);

        #endregion

        #region get Company types

        /// <summary>
        /// This function returns Company types
        /// </summary>
        /// <returns>List of Company type data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetCompanyTypes)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetCompanyTypes)]
        List<CompanyTypeData> getCompanyType(string userName, string salt);

        #endregion

        #region get Org Inspection Form

        /// <summary>
        /// This function returns Org Inspected form
        /// </summary>
        /// <returns>List of org inspection data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetOrgInspection)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetOrgInspection)]
        OrgInspectionData getOrgInspection(int appointmentId, string userName, string salt);

        #endregion

        #region save Org Inspection
        /// <summary>
        /// This function saves the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveOrgInspection)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveOrgInspection)]
        int saveOrgInspection(OrgInspectionData orgData, string userName, string salt);

        #endregion

        #region update Org Inspection
        /// <summary>
        /// This function updates the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        
        [OperationContract]
        [Description(ServiceDescConstants.UpdateOrgInspection)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateOrgInspection)]
        bool updateOrgInspection(OrgInspectionData orgData, string userName, string salt);
        
        #endregion


        #region Save a new Org
        /// <summary>
        /// This function saves a new Org
        /// </summary>
        /// <param name="appData">Org data objects</param>
        /// <returns>ID in case of success</returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnOrg)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAnOrg)]
        int saveOrg(OrgData orgData, string userName, string salt);
        #endregion

    }
}
