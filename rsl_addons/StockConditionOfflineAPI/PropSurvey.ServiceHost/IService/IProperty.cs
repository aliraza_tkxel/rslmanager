﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/property/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface IProperty
    {
        #region find Property
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">surname of customer</param>
        /// <returns>It returns the list of customer data's object</returns>
        [OperationContract]
        [Description(ServiceDescConstants.FindProperty)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.searchProperty)]
        ResponseTemplate<PropertiesListData> findProperty(PropertySearchParam varPropertySearchParam);

        /// <summary>
        /// This function returns the property that are related to GAS against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">surname of customer</param>
        /// <returns>It returns the list of customer data's object</returns>
        [OperationContract]
        [Description(ServiceDescConstants.FindPropertyGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.FindPropertyGas)]
        List<CustomerData> findPropertyGas(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "");

        #endregion

        #region get Property Image Names
        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="item id">item Id</param>
        /// <returns>returns the list of image names against property</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetPropertyImageNames)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetPropertyImageNames)]
        List<PropertyPictureData> getPropertyImageNames(string propertyId, int itemId);

        #endregion

        #region get Property Image
        /// <summary>
        /// This fucntion returns the property images against property name
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="imageName">image name </param>
        /// <returns></returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetPropertyImage)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetPropertyImage)]
        Stream getPropertyImage(string propertyId, string imageName);

        #endregion

        #region get Property Default Image

        /// <summary>
        /// This function returns the default image which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>it returns the stream of image (if found)</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetDefaultPropertyImage)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetDefaultPropertyImage)]
        Stream getDefaultPropertyImage(String propertyId);

        #endregion

        #region update Property Image

        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="propertyId">property Id</param>
        /// <param name="surveyId">survey id</param>
        /// <param name="itemId">item Id</param>
        /// <param name="fileName">file name image</param>
        /// <param name="pictureTag">picutre tag , this 'll be like this: Internal;Roof;Dwelling</param>
        /// <param name="isDefault">true or false</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdatePropertySurveyImage)]       
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UploadPropertyImage)]
        ResponseTemplate<ResponseUploadPictureData> uploadPropertyImage(Stream stream, string propertyId, int appointmentId, int itemId, int updatedBy, string fileExt, string type, string userName, string salt, int propertyPicId = 0, bool isDefault = false, string imageIdentifier = "", int schemeId = 0, int blockId = 0, int heatingId = 0);



        [OperationContract]
        [Description(ServiceDescConstants.SaveHeatingInspectionImage)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = UriTemplateConstants.saveHeatingInspectionImage)]
        ResponseTemplate<ResponseUploadHeatingInspectionImage> saveHeatingInspectionImage(Stream stream);


        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="propertyId">property Id</param>
        /// <param name="surveyId">survey id</param>
        /// <param name="itemId">item Id</param>
        /// <param name="fileName">file name image</param>
        /// <param name="pictureTag">picutre tag , this 'll be like this: Internal;Roof;Dwelling</param>
        /// <param name="isDefault">true or false</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdatePropertyImage)]        
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdatePropertyDefaultImage)]
        ResponseTemplate<ResponseUploadPictureData> updatePropertyDefaultImage(Stream stream, string propertyId, int propertyPicId, int updatedBy, string fileExt);
        
        #endregion

        #region deletePropertyImage
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>true if file deleted or not but return false in case of any error while connecting with database or property picture id is not valid integer</returns>
        [OperationContract]
        [Description(ServiceDescConstants.DeletePropertyImage)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.DeletePropertyImage)]
        ResponseTemplate<bool> deletePropertyImage(DeletepropertyPictureData deletepropertyPictureData);
        #endregion

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>List of property dimension data object</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetPropertyDimensions)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetPropertyDimensions)]
        List<PropertyDimData> getPropertyDimensions(string propertyId);

        #endregion       

    }
}
