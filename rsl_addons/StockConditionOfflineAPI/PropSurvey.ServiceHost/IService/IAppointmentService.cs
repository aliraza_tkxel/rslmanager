﻿using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Data.Appointment;
using PropSurvey.Contracts.Data.Void;
using PropSurvey.Contracts.Data.Stock;
using System.Web;




namespace PropSurvey.ServiceHost.IService
{
    [ServiceContract]
    [ServiceKnownType(typeof(AllAppointmentsList))]
    //[ServiceKnownType(typeof(List<AllAppointmentsList>))]
    [ServiceKnownType(typeof(VoidGasElectricData))]
    [ServiceKnownType(typeof(VoidInspectionAppointmentData))]
    [ServiceKnownType(typeof(VoidWorksAppointmentData))]
    [ServiceKnownType(typeof(ApplianceDefectAppointmentData))]
    [ServiceKnownType(typeof(AppointmentInProgress))]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface IAppointmentService
    {
        #region get All Appointments By Application Type
        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accomodations','Surveyors List' and 'Customer List'.
        /// </summary>
        /// <returns>
        /// List of appointments,Property Info,Accomodations,Surveyors List and Customer List, data objects
        /// </returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAllAppointmentsByAppType)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.GetAllAppointmentsByAppType)]
        ResponseTemplate<AppointmentResponse> getAllAppointmentsByAppType(AppointmentParam varAppointmentParam);

        #endregion

        #region Update Appointments status
        /// <summary>
        /// This function updates the given appointments
        /// </summary>
        /// <returns>
        /// Returns the success or failure response
        /// </returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateAppointmentsStatus)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAppointmentStatus)]
        SyncAppointmentStatusResponse updateAppointmentStatus(SyncAppointmentStatusRequest request);

        #endregion

        #region get All Stock Appointments
        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accomodations','Surveyors List' and 'Customer List'.
        /// </summary>
        /// <returns>
        /// List of stock appointments,Property Info,Accomodations,Surveyors List and Customer List, data objects
        /// </returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAllStockAppointments)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.GetAllStockAppointments)]
        ResponseTemplate<List<AllAppointmentsList>> getAllStockAppointments(AppointmentParam varAppointmentParam);

        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAllUsers)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.GetAllUsers)]
        ResponseTemplate<List<SurveyorUserData>> getAllUsers(SurveyorParam varSurveyorParam);
        #endregion

        #region delete Stock Appointment
        /// <summary>
        /// This function deletes the appointment in the database
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true on success and false otherwise </returns>

        [OperationContract]
        [Description(ServiceDescConstants.DeleteAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.DeleteAppointment)]
        ResponseTemplate<List<DelAppointmentData>> deleteStockAppointment(DelAppointmentParam varDelAppointmentParam);

        #endregion

        #region save Stock Appointment
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAppointment)]
        ResponseTemplate<List<AllAppointmentsList>> saveStockAppointment(AppointmentDataStock varAppointmentDataStock);

        #endregion

        #region update Complete Appointment
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.CompleteAppointment)]
        Stream completeAppointment(AppointmentContractData varSaveCompleteAppointmentData);

        #endregion
        
        #region Populate Stock Inspection Documents.
        /// <summary>
        /// This function used to Populate Stock Inspection Documents.
        /// </summary>        
        /// <returns>string Message</returns>

        [OperationContract]
        [Description(ServiceDescConstants.PopulateStockInspection)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.PopulateStockInspection)]
        void populateStockInspection(StockInspectionDocument stockInspectionDocList);
        #endregion
        
    }
}