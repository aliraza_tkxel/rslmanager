﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ComponentModel;
using PropSurvey.Utilities.Constants;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    interface IPush
    {

        #region send Push Notification for Fault Appointment

        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SendPushNotificationFault)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.SendPushNotificationFault)]
        bool sendPushNotificationFault(int appointmentId, int type,bool isSchemeBlockAppointment=false);

        #endregion

        #region send Push Notification for Appliance and Planned Appointment(s)

        /// <summary>
        /// This function will send push notification for Appliance and Planned Appointment(s)
        /// </summary>
        /// <returns>Success or failure</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SendPushNotificationApplianceAndPlanned)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.sendPushNotificationApplianceAndPlanned)]
        bool sendPushNotificationApplianceAndPlanned(string appointmentMessage, int operatorId);

       #endregion

#region Send Push Notification to Operative having incomplete appointments

        /// <summary>
        /// Send push notification to operatives having incomplete appointment after due completion time.
        /// To filter the operative/appointments time is save as a key/value pair in database and may be changed any time.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Description(ServiceDescConstants.sendDocumentExpiaryPushNotification)]
        [WebInvoke(UriTemplate = UriTemplateConstants.sendPushNotificationDocumentExpiry, ResponseFormat = WebMessageFormat.Json)]
        bool sendPushNotificationDocumentExpiry();

        #endregion
        #region Send Push Notification to Operative having incomplete appointments

        /// <summary>
        /// Send push notification to operatives having incomplete appointment after due completion time.
        /// To filter the operative/appointments time is save as a key/value pair in database and may be changed any time.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [Description(ServiceDescConstants.sendOverduePushNotification)]
        [WebInvoke(UriTemplate = UriTemplateConstants.sendOverduePushNotifications, ResponseFormat = WebMessageFormat.Json)]
        bool sendOverduePushNotification();
        
        #endregion

    }
}
