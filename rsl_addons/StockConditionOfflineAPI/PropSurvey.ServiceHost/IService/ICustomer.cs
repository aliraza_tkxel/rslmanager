﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.ServiceModel;
using PropSurvey.Contracts.Data;
using System.ServiceModel.Web;
using PropSurvey.Utilities.Constants;
using System.IO;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/customer/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface ICustomer
    {
        /// <summary>
        /// This function returns the customer record 
        /// </summary>
        /// <param name="customerId">customer id </param>
        /// <returns>It returns the customer data in json</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetCustomerInfo)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetCustomerInfo)]
        CustomerData getCustomerInfo(string customerId);


        

        // Region added  30/04/2013 - START

        #region get Employee Signature Image

        /// <summary>
        /// This function returns the signature image which was saved against employee
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <returns>it returns the stream of image (if found)</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetEmployeeSignatureImage)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetEmployeeSignatureImage)]
        Stream getEmployeeSignatureImage(String employeeId);

        #endregion

        // Region added  30/04/2013 - END
    }

    
     

}
