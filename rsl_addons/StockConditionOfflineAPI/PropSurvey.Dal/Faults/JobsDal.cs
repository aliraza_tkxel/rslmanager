﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.IO;

namespace PropSurvey.Dal.Faults
{
    public class JobsDal : BaseDal
    {
        #region get All Jobs Data
        /// <summary>
        /// This function returns all Jobs data of an operative
        /// </summary>
        /// <param name="operativeID">operative id</param>
        /// <returns>List of Jobs data objects</returns>

        public List<JobsData> getAllJobsData(int operativeID)
        {
            //var _var = (from app in context.FL_CO_APPOINTMENT
            //            join flt in context.FL_FAULT_LOG on app.FAULTLOGID equals flt.FaultLogID
            //            join cus in context.C__CUSTOMER on flt.CustomerId equals cus.CUSTOMERID
            //            join gti in context.G_TITLE on cus.TITLE equals gti.TITLEID
            //            join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
            //            join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
            //            join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
            //            join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
            //            orderby app.AppointmentID
            //            where app.OperativeID == operativeID                                
            //                    && ten.ENDDATE == null
            //                    && cut.ENDDATE == null
            //                    && cad.ISDEFAULT == 1
            //            select new JobsData
            //            {
            //                FaultID = flt.FaultLogID,
            //                DueDateTime = flt.DueDate,
            //                EndTime = app.EndTime,
            //                Notes = flt.Notes,
            //                AppointmentID = app.AppointmentID,
            //                CustomerData = new CustomerData
            //                {
            //                    customerId = cus.CUSTOMERID,
            //                    title = gti.DESCRIPTION,
            //                    firstName = cus.FIRSTNAME,

            //                    middleName = cus.MIDDLENAME,
            //                    lastName = cus.LASTNAME,
            //                    telephone = cad.TEL,

            //                    mobile = cad.MOBILE,
            //                    fax = cad.FAX,
            //                    email = cad.EMAIL,
            //                    property = new PropertyData
            //                    {
            //                        houseNumber = pro.HOUSENUMBER,
            //                        flatNumber = pro.FLATNUMBER,
            //                        address1 = pro.ADDRESS1,
            //                        address2 = pro.ADDRESS2,
            //                        address3 = pro.ADDRESS3,
            //                        townCity = pro.TOWNCITY,
            //                        postCode = pro.POSTCODE,
            //                        county = pro.COUNTY
            //                    }
            //                },
            //            });

            //List<JobsData> jobs = new List<JobsData>();
            //if (_var.Count() > 0)
            //{
            //    jobs = _var.ToList();
            //}

            //return jobs;
            return null;
        }
        #endregion

        //#region get Jobs Data by StatusID
        ///// <summary>
        ///// This function returns Jobs Data by StatusID of an operative
        ///// </summary>
        ///// <param name="operativeID">operative ID</param>
        ///// <param name="faultStatusID">fault Status id</param>
        ///// <returns>List of Jobs data objects</returns>

        //public List<JobsData> getJobsDataByStatusID(int operativeID, int faultStatusID)
        //{
        //    var _var = (from app in context.FL_CO_APPOINTMENT
        //                join flt in context.FL_FAULT_LOG on app.FAULTLOGID equals flt.FaultLogID
        //                join cus in context.C__CUSTOMER on flt.CustomerId equals cus.CUSTOMERID
        //                join gti in context.G_TITLE on cus.TITLE equals gti.TITLEID
        //                join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
        //                join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
        //                join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
        //                join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
        //                orderby app.AppointmentID
        //                where app.OperativeID == operativeID
        //                        && flt.StatusID == faultStatusID
        //                        && ten.ENDDATE == null
        //                        && cut.ENDDATE == null
        //                        && cad.ISDEFAULT == 1
        //                select new JobsData
        //                {
        //                    FaultID = flt.FaultLogID,
        //                    DueDate = flt.DueDate,
        //                    Notes = flt.Notes,
        //                    AppointmentID = app.AppointmentID,
        //                    CustomerData = new CustomerData
        //                    {
        //                        customerId = cus.CUSTOMERID,
        //                        title = gti.DESCRIPTION,
        //                        firstName = cus.FIRSTNAME,

        //                        middleName = cus.MIDDLENAME,
        //                        lastName = cus.LASTNAME,
        //                        telephone = cad.TEL,

        //                        mobile = cad.MOBILE,
        //                        fax = cad.FAX,
        //                        email = cad.EMAIL,
        //                        property = new PropertyData
        //                        {
        //                            houseNumber = pro.HOUSENUMBER,
        //                            flatNumber = pro.FLATNUMBER,
        //                            address1 = pro.ADDRESS1,
        //                            address2 = pro.ADDRESS2,
        //                            address3 = pro.ADDRESS3,
        //                            townCity = pro.TOWNCITY,
        //                            postCode = pro.POSTCODE,
        //                            county = pro.COUNTY
        //                        }
        //                    },
        //                });
        //    List<JobsData> jobs = new List<JobsData>();
        //    if (_var.Count() > 0)
        //    {
        //        jobs = _var.ToList();
        //    }

        //    return jobs;
        //}
        //#endregion

        //#region "get JobSheetNumber List for all jobs of an AppointmentID"
        ///// <summary>
        ///// This function returns the list of JobSheetNumber of all jobs of an appointment.
        ///// </summary>
        ///// <param name="appointmentId">appointment id</param>
        ///// <returns>the list of JobSheetNumber Object</returns>

        //public List<JobSheetNumberListData> getJobSheetNumbersList(int appointmentID)
        //{
        //    var jobSheetNumbers = (from app in context.FL_CO_APPOINTMENT
        //                           join fltl in context.FL_FAULT_LOG on app.FAULTLOGID equals fltl.FaultLogID
        //                           join flt in context.FL_FAULT on fltl.FaultID equals flt.FaultID
        //                           join cus in context.C__CUSTOMER on fltl.CustomerId equals cus.CUSTOMERID
        //                           join gti in context.G_TITLE on cus.TITLE equals gti.TITLEID
        //                           join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
        //                           join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
        //                           join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
        //                           join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
        //                           orderby app.AppointmentID
        //                           where app.AppointmentID == appointmentID
        //                                   && ten.ENDDATE == null
        //                                   && cut.ENDDATE == null
        //                                   && cad.ISDEFAULT == 1

        //                           select new JobSheetNumberListData
        //                           {
        //                               JSNumber = fltl.JobSheetNumber,
        //                               JSNDescription = flt.Description
        //                           });

        //    List<JobSheetNumberListData> jobSheetNumberList = new List<JobSheetNumberListData>();
        //    if (jobSheetNumbers.Count() > 0)
        //    {
        //        jobSheetNumberList = jobSheetNumbers.ToList();
        //    }

        //    return jobSheetNumberList;
        //}
        //#endregion

        //#region "get JobSheetNumber List by StatusID for a AppointmentID"
        ///// <summary>
        ///// This function returns the list of JobSheetNumber by StatusID of an appointment.
        ///// </summary>
        ///// <param name="appointmentId">appointment id</param>
        ///// <param name="faultStatusID">fault Status id</param>
        ///// <returns>the list of JobSheetNumber Object</returns>

        //public List<JobSheetNumberListData> getJobSheetNumbersListByStatusID(int appointmentID, int faultStatusID)
        //{
        //    var jobSheetNumbers = (from app in context.FL_CO_APPOINTMENT
        //                join fltl in context.FL_FAULT_LOG on app.FAULTLOGID equals fltl.FaultLogID
        //                join flt in context.FL_FAULT on fltl.FaultID equals flt.FaultID
        //                join cus in context.C__CUSTOMER on fltl.CustomerId equals cus.CUSTOMERID
        //                join gti in context.G_TITLE on cus.TITLE equals gti.TITLEID
        //                join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
        //                join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
        //                join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
        //                join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
        //                orderby app.AppointmentID
        //                where app.AppointmentID == appointmentID
        //                        && fltl.StatusID == faultStatusID
        //                        && ten.ENDDATE == null
        //                        && cut.ENDDATE == null
        //                        && cad.ISDEFAULT == 1  

        //                       select new JobSheetNumberListData 
        //                       {
        //                           JSNumber = fltl.JobSheetNumber,
        //                           JSNDescription = flt.Description
        //                       });

        //    List<JobSheetNumberListData> jobSheetNumberList = new List<JobSheetNumberListData>();
        //    if (jobSheetNumbers.Count() > 0)
        //    {
        //        jobSheetNumberList = jobSheetNumbers.ToList();
        //    }

        //    return jobSheetNumberList;
        //}
        //#endregion
    }
}
