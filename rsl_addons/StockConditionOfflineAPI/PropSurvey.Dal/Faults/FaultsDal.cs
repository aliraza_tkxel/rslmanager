﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.IO;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.Dal.Faults
{
    public class FaultsDal : BaseDal
    {
        #region get Faults Data
        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>

        public List<FaultsData> getFaultsData(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID
                        select new FaultsData
                       {
                           ID = fault.ID,
                           AppointmentID = fault.AppointmentID,
                           DefectDesc = fault.DefectDesc,
                           FaultCategory = fault.FaultCategory,
                           isAdviceNoteIssued = fault.isAdviceNoteIssued,
                           RemedialAction = fault.RemedialAction,
                           SerialNo = fault.SerialNo,
                           WarningTagFixed = fault.WarningTagFixed

                       });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }
        #endregion

        #region get Faults Data Gas

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getFaultsDataGas(int applianceId, ApplianceSurveyDataParam requestParam)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.ApplianceId == applianceId
                       && (defects.PropertyId == requestParam.propertyId || defects.SchemeId == requestParam.schemeId || defects.BlockId == requestParam.blockId)
                       select new FaultsDataGas
                       {
                           remedialActionNotes = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           faultCategory = defects.CategoryId,
                           defectDate = defects.DefectDate,
                           defectNotes = defects.DefectNotes,
                           isRemedialActionTaken = defects.IsActionTaken,
                           isDefectIdentified = defects.IsDefectIdentified,
                           warningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           schemeId = defects.SchemeId,
                           blockId = defects.BlockId,
                           serialNumber = defects.SerialNumber
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region get Boiler Defects

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getBoilerDefects(int? heatingId, int? journalId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.HeatingMappingId == heatingId && defects.JournalId == journalId
                       select new FaultsDataGas
                       {
                           remedialActionNotes = defects.ActionNotes,
                           boilerTypeId = defects.BoilerTypeId,
                           heatingId = defects.HeatingMappingId,
                           faultCategory = defects.CategoryId,
                           defectDate = defects.DefectDate,
                           defectNotes = defects.DefectNotes,
                           isRemedialActionTaken = defects.IsActionTaken,
                           isDefectIdentified = defects.IsDefectIdentified,
                           warningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           schemeId = defects.SchemeId,
                           blockId = defects.BlockId,
                           serialNumber = defects.SerialNumber,
                           gasCouncilNumber = defects.GasCouncilNumber
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region fetch Faults Data Gas

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> fetchFaultsDataGas(int journalId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.JournalId == journalId
                       select new FaultsDataGas
                       {
                           remedialActionNotes = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           faultCategory = defects.CategoryId,
                           defectDate = defects.DefectDate,
                           defectNotes = defects.DefectNotes,
                           isRemedialActionTaken = defects.IsActionTaken,
                           isDefectIdentified = defects.IsDefectIdentified,
                           warningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           serialNumber = defects.SerialNumber
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region get detector fault list

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getDetectorDefectList(int DetectorTypeId, string propertyId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.DetectorTypeId == DetectorTypeId && defects.PropertyId == propertyId
                       select new FaultsDataGas
                       {
                           remedialActionNotes = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           faultCategory = defects.CategoryId,
                           defectDate = defects.DefectDate,
                           defectNotes = defects.DefectNotes,
                           isRemedialActionTaken = defects.IsActionTaken,
                           isDefectIdentified = defects.IsDefectIdentified,
                           warningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           serialNumber = defects.SerialNumber,
                           detectorTypeId = defects.DetectorTypeId
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region fetch detector fault list

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> fetchDetectorDefectList(int journalId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       join detector in context.AS_DetectorType on defects.DetectorTypeId equals detector.DetectorTypeId
                       where defects.JournalId == journalId
                       select new FaultsDataGas
                       {
                           remedialActionNotes = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           faultCategory = defects.CategoryId,
                           defectDate = defects.DefectDate,
                           defectNotes = defects.DefectNotes,
                           isRemedialActionTaken = defects.IsActionTaken,
                           isDefectIdentified = defects.IsDefectIdentified,
                           warningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           serialNumber = defects.SerialNumber,
                           detectorTypeId = defects.DetectorTypeId
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region save Faults Data

        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsData(FaultsData faultData)
        {
            bool success = false;

            GS_GasFault fault = new GS_GasFault();
            using (TransactionScope trans = new TransactionScope())
            {
                fault.AppointmentID = faultData.AppointmentID;
                fault.WarningTagFixed = faultData.WarningTagFixed;
                fault.SerialNo = faultData.SerialNo;
                fault.RemedialAction = faultData.RemedialAction;
                fault.isAdviceNoteIssued = faultData.isAdviceNoteIssued;
                fault.FaultCategory = faultData.FaultCategory;
                fault.DefectDesc = faultData.DefectDesc;

                context.AddToGS_GasFault(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.ID;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region save Faults Data Gas
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsDataGas(FaultsDataGas faultData)
        {
            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            using (TransactionScope trans = new TransactionScope())
            {
                fault.ActionNotes = faultData.remedialActionNotes;
                fault.ApplianceId = faultData.ApplianceID;
                fault.CategoryId = faultData.faultCategory;
                fault.DefectDate = faultData.defectDate;
                fault.DefectNotes = faultData.defectNotes;
                fault.IsActionTaken = faultData.isRemedialActionTaken;
                fault.IsDefectIdentified = faultData.isDefectIdentified;
                fault.IsWarningFixed = faultData.warningTagFixed;
                fault.IsWarningIssued = faultData.isAdviceNoteIssued;
                fault.JournalId = faultData.JournalId;
                fault.PropertyId = faultData.propertyId;
                fault.SerialNumber = faultData.serialNumber;
                fault.DetectorTypeId = faultData.detectorTypeId;
                fault.BoilerTypeId = faultData.boilerTypeId;
                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }


        #endregion

        #region update fault Data

        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateFaultData(FaultsData faultData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.GS_GasFault
                            where ins.ID == faultData.ID
                            select ins);
                if (_var.Count() > 0)
                {
                    GS_GasFault fault = _var.FirstOrDefault();
                    fault.FaultCategory = faultData.FaultCategory;
                    fault.DefectDesc = faultData.DefectDesc;
                    fault.AppointmentID = faultData.AppointmentID;
                    fault.isAdviceNoteIssued = faultData.isAdviceNoteIssued;
                    fault.RemedialAction = faultData.RemedialAction;
                    fault.SerialNo = faultData.SerialNo;
                    fault.WarningTagFixed = faultData.WarningTagFixed;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }

            }

            return success;
        }

        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public int updateFaultDataGas(FaultsDataGas faultData, PropertySurvey_Entities currentContext = null, String username = null)
        {
            if (currentContext != null) this.context = currentContext;

            P_PROPERTY_APPLIANCE_DEFECTS applianceDefect = new P_PROPERTY_APPLIANCE_DEFECTS();

            var employeeId = (from logins in context.AC_LOGINS
                                  where logins.LOGIN == username
                                  select logins.EMPLOYEEID);
            var _var = (from ins in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where ins.PropertyDefectId == faultData.ID
                        select ins);

            //To update property appliance defect
            bool addNewDefect = true, isSame = false;
            if (_var.Count() > 0)
            {
                applianceDefect = _var.FirstOrDefault();
                addNewDefect = false;
                isSame = compareDefectObjects(faultData, applianceDefect, employeeId.FirstOrDefault());
            }

            if (isSame)
            {
                return applianceDefect.PropertyDefectId;
            }

            applianceDefect.ActionNotes = faultData.remedialActionNotes;
            applianceDefect.CategoryId = faultData.faultCategory;
            applianceDefect.DefectNotes = faultData.defectNotes;
            applianceDefect.DefectJobSheetStatus = GetPdrStatusId(ApplicationConstants.ToBeApprovedDefectStatus);
            applianceDefect.IsActionTaken = faultData.isRemedialActionTaken;
            applianceDefect.IsDefectIdentified = faultData.isDefectIdentified;
            applianceDefect.IsWarningFixed = faultData.warningTagFixed;
            applianceDefect.IsWarningIssued = faultData.isAdviceNoteIssued;
            applianceDefect.WarningNoteSerialNo = faultData.warningNoteSerialNo;
            applianceDefect.JournalId = faultData.JournalId;
            applianceDefect.PropertyId = faultData.propertyId;
            applianceDefect.SchemeId = faultData.schemeId;
            applianceDefect.BlockId = faultData.blockId;
            applianceDefect.SerialNumber = faultData.serialNumber;
            applianceDefect.GasCouncilNumber = faultData.gasCouncilNumber;
            applianceDefect.IsDisconnected = faultData.isDisconnected;
            applianceDefect.IsPartsrequired = faultData.isPartsRequired;
            applianceDefect.IsPartsOrdered = faultData.isPartsOrdered;
            applianceDefect.PartsOrderedBy = faultData.partsOrderedBy;
            applianceDefect.PartsDue = faultData.partsDueDate;
            applianceDefect.PartsDescription = faultData.partsDescription;
            applianceDefect.PartsLocation = faultData.partsLocation;
            applianceDefect.IsTwoPersonsJob = faultData.isTwoPersonsJob;
            applianceDefect.ReasonFor2ndPerson = faultData.reasonForTwoPerson;
            applianceDefect.Duration = faultData.duration;
            applianceDefect.TradeId = faultData.tradeId;
            applianceDefect.IsCustomerHaveHeating = faultData.IsCustomerHaveHeating;
            applianceDefect.NoEntryNotes = faultData.NoEntryNotes;
            applianceDefect.Priority = faultData.priorityId;
            if (employeeId.Count() > 0)
            {
                applianceDefect.CreatedBy = employeeId.FirstOrDefault();
            }
            if (faultData.ApplianceID > 0)
            {
                applianceDefect.ApplianceId = faultData.ApplianceID;
            }

            // Fields for detector defects.
            if (faultData.detectorTypeId > 0)
            {
                applianceDefect.DetectorTypeId = faultData.detectorTypeId;
            }

            // Fields for boiler defects.
            if (faultData.heatingId > 0)
            {
                applianceDefect.BoilerTypeId = faultData.boilerTypeId > 0 ? faultData.boilerTypeId : null;
                applianceDefect.HeatingMappingId = faultData.heatingId;

                BoilerData boilerData = new BoilerData();
                boilerData.propertyId = faultData.propertyId;
                boilerData.schemeId = faultData.schemeId;
                boilerData.blockId = faultData.blockId;
                boilerData.heatingId = faultData.heatingId;

                var itemName = string.IsNullOrEmpty(faultData.propertyId) ? ApplicationConstants.BoilerRoomItemName : ApplicationConstants.HeatingItemName;

                updateHeatingBoilerInfo(boilerData, itemName, ApplicationConstants.GcNumberParameterKey, applianceDefect.GasCouncilNumber, applianceDefect.CreatedBy);
                updateHeatingBoilerInfo(boilerData, itemName, ApplicationConstants.SerialNoParameterKey, applianceDefect.SerialNumber, applianceDefect.CreatedBy);
            }

            //To Add new property appliance defect
            if (addNewDefect)
            {  
                applianceDefect.DefectDate = faultData.defectDate;
                context.P_PROPERTY_APPLIANCE_DEFECTS.AddObject(applianceDefect);
            }

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();

            return applianceDefect.PropertyDefectId;
        }

        #endregion

        #region "Save Oil Defect"
        /// <summary>
        /// Save Oil Defect
        /// </summary>
        /// <param name="faultData"></param>
        /// <param name="currentContext"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public int saveOilDefect(FaultsDataGas faultData, PropertySurvey_Entities currentContext = null, String username = null)
        {
            if (currentContext != null) this.context = currentContext;

            P_PROPERTY_APPLIANCE_DEFECTS defect = new P_PROPERTY_APPLIANCE_DEFECTS();

            var employeeId = (from logins in context.AC_LOGINS
                              where logins.LOGIN == username
                              select logins.EMPLOYEEID);
            var _var = (from ins in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where ins.PropertyDefectId == faultData.ID
                        select ins);

            //To update property appliance defect
            bool addNewDefect = true, isSame = false;
            if (_var.Count() > 0)
            {
                defect = _var.FirstOrDefault();
                addNewDefect = false;
                isSame = compareOilDefectObjects(faultData, defect, employeeId.FirstOrDefault());
            }

            if (isSame)
            {
                return defect.PropertyDefectId;
            }

            defect.ActionNotes = faultData.remedialActionNotes;
            defect.CategoryId = faultData.faultCategory;
            defect.DefectNotes = faultData.defectNotes;
            defect.DefectJobSheetStatus = GetPdrStatusId(ApplicationConstants.ToBeApprovedDefectStatus);
            defect.IsActionTaken = faultData.isRemedialActionTaken;
            defect.IsDefectIdentified = faultData.isDefectIdentified;
            defect.IsWarningFixed = faultData.warningTagFixed;
            defect.IsWarningIssued = faultData.isAdviceNoteIssued;
            defect.WarningNoteSerialNo = faultData.warningNoteSerialNo;
            defect.JournalId = faultData.JournalId;
            defect.PropertyId = faultData.propertyId;
            defect.SchemeId = faultData.schemeId;
            defect.BlockId = faultData.blockId;
            defect.SerialNumber = faultData.serialNumber;
            defect.GasCouncilNumber = faultData.gasCouncilNumber;
            defect.IsDisconnected = faultData.isDisconnected;
            defect.IsPartsrequired = faultData.isPartsRequired;
            defect.IsPartsOrdered = faultData.isPartsOrdered;
            defect.PartsOrderedBy = faultData.partsOrderedBy;
            defect.PartsDue = faultData.partsDueDate;
            defect.PartsDescription = faultData.partsDescription;
            defect.PartsLocation = faultData.partsLocation;
            defect.IsTwoPersonsJob = faultData.isTwoPersonsJob;
            defect.ReasonFor2ndPerson = faultData.reasonForTwoPerson;
            defect.Duration = faultData.duration;
            defect.TradeId = faultData.tradeId;
            defect.IsCustomerHaveHeating = faultData.IsCustomerHaveHeating;
            defect.NoEntryNotes = faultData.NoEntryNotes;
            defect.Priority = faultData.priorityId;

            if (employeeId.Count() > 0)
            {
                defect.CreatedBy = employeeId.FirstOrDefault();
            }

            // Fields for boiler defects.
            if (faultData.heatingId > 0)
            {
                defect.BoilerTypeId = faultData.boilerTypeId > 0 ? faultData.boilerTypeId : null;
                defect.HeatingMappingId = faultData.heatingId;

                BoilerData boilerData = new BoilerData();
                boilerData.propertyId = faultData.propertyId;
                boilerData.schemeId = faultData.schemeId;
                boilerData.blockId = faultData.blockId;
                boilerData.heatingId = faultData.heatingId;

                var itemName = string.IsNullOrEmpty(faultData.propertyId) ? ApplicationConstants.BoilerRoomItemName : ApplicationConstants.HeatingItemName;                
                updateHeatingBoilerInfo(boilerData, itemName, ApplicationConstants.ApplianceSerialNumberParameterKey, defect.SerialNumber, defect.CreatedBy);
            }

            //To Add new property appliance defect
            if (addNewDefect)
            {
                defect.DefectDate = faultData.defectDate;
                context.P_PROPERTY_APPLIANCE_DEFECTS.AddObject(defect);
            }

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();

            return defect.PropertyDefectId;
        }

         #endregion

        #region Compare history of defect
        private bool compareDefectObjects(FaultsDataGas faultData, P_PROPERTY_APPLIANCE_DEFECTS applianceDefect, int? employeeId)
        {
            bool isSame = false;
            if (
            applianceDefect.ActionNotes == faultData.remedialActionNotes &&
            applianceDefect.CategoryId == faultData.faultCategory &&
            applianceDefect.DefectNotes == faultData.defectNotes &&
            applianceDefect.DefectJobSheetStatus == GetPdrStatusId(ApplicationConstants.ToBeApprovedDefectStatus) &&
            applianceDefect.IsActionTaken == faultData.isRemedialActionTaken &&
            applianceDefect.IsDefectIdentified == faultData.isDefectIdentified &&
            applianceDefect.IsWarningFixed == faultData.warningTagFixed &&
            applianceDefect.IsWarningIssued == faultData.isAdviceNoteIssued &&
            applianceDefect.WarningNoteSerialNo == faultData.warningNoteSerialNo &&
            applianceDefect.JournalId == faultData.JournalId &&
            applianceDefect.PropertyId == faultData.propertyId &&
            applianceDefect.SchemeId == faultData.schemeId &&
            applianceDefect.BlockId == faultData.blockId &&
            applianceDefect.SerialNumber == faultData.serialNumber &&
            applianceDefect.GasCouncilNumber == faultData.gasCouncilNumber &&
            applianceDefect.IsDisconnected == faultData.isDisconnected &&
            applianceDefect.IsPartsrequired == faultData.isPartsRequired &&
            applianceDefect.IsPartsOrdered == faultData.isPartsOrdered &&
            applianceDefect.PartsOrderedBy == faultData.partsOrderedBy &&
            applianceDefect.PartsDue == faultData.partsDueDate &&
            applianceDefect.PartsDescription == faultData.partsDescription &&
            applianceDefect.PartsLocation == faultData.partsLocation &&
            applianceDefect.IsTwoPersonsJob == faultData.isTwoPersonsJob &&
            applianceDefect.ReasonFor2ndPerson == faultData.reasonForTwoPerson &&
            applianceDefect.Duration == faultData.duration &&
            applianceDefect.TradeId == faultData.tradeId &&
            applianceDefect.IsCustomerHaveHeating == faultData.IsCustomerHaveHeating &&
            applianceDefect.NoEntryNotes == faultData.NoEntryNotes &&
            applianceDefect.Priority == faultData.priorityId &&
            applianceDefect.CreatedBy == employeeId &&
            applianceDefect.ApplianceId == faultData.ApplianceID &&
            applianceDefect.DetectorTypeId == faultData.detectorTypeId &&
            applianceDefect.BoilerTypeId == faultData.boilerTypeId &&
            applianceDefect.HeatingMappingId == faultData.heatingId &&
            applianceDefect.DefectDate == faultData.defectDate)
            {
                isSame = true; 
            }

            return isSame;
        }
        #endregion

        #region Compare Oil Defect Objects
        private bool compareOilDefectObjects(FaultsDataGas faultData, P_PROPERTY_APPLIANCE_DEFECTS applianceDefect, int? employeeId)
        {
            bool isSame = false;
            if (
            applianceDefect.ActionNotes == faultData.remedialActionNotes &&
            applianceDefect.CategoryId == faultData.faultCategory &&
            applianceDefect.DefectNotes == faultData.defectNotes &&
            applianceDefect.DefectJobSheetStatus == GetPdrStatusId(ApplicationConstants.ToBeApprovedDefectStatus) &&
            applianceDefect.IsActionTaken == faultData.isRemedialActionTaken &&
            applianceDefect.IsDefectIdentified == faultData.isDefectIdentified &&
            applianceDefect.IsWarningFixed == faultData.warningTagFixed &&
            applianceDefect.IsWarningIssued == faultData.isAdviceNoteIssued &&
            applianceDefect.WarningNoteSerialNo == faultData.warningNoteSerialNo &&
            applianceDefect.JournalId == faultData.JournalId &&
            applianceDefect.PropertyId == faultData.propertyId &&
            applianceDefect.SchemeId == faultData.schemeId &&
            applianceDefect.BlockId == faultData.blockId &&
            applianceDefect.SerialNumber == faultData.serialNumber &&
            applianceDefect.IsDisconnected == faultData.isDisconnected &&
            applianceDefect.IsPartsrequired == faultData.isPartsRequired &&
            applianceDefect.IsPartsOrdered == faultData.isPartsOrdered &&
            applianceDefect.PartsOrderedBy == faultData.partsOrderedBy &&
            applianceDefect.PartsDue == faultData.partsDueDate &&
            applianceDefect.PartsDescription == faultData.partsDescription &&
            applianceDefect.PartsLocation == faultData.partsLocation &&
            applianceDefect.IsTwoPersonsJob == faultData.isTwoPersonsJob &&
            applianceDefect.ReasonFor2ndPerson == faultData.reasonForTwoPerson &&
            applianceDefect.Duration == faultData.duration &&
            applianceDefect.TradeId == faultData.tradeId &&
            applianceDefect.IsCustomerHaveHeating == faultData.IsCustomerHaveHeating &&
            applianceDefect.NoEntryNotes == faultData.NoEntryNotes &&
            applianceDefect.Priority == faultData.priorityId &&
            applianceDefect.CreatedBy == employeeId &&
            applianceDefect.HeatingMappingId == faultData.heatingId &&
            applianceDefect.DefectDate == faultData.defectDate)
            {
                isSame = true;
            }

            return isSame;
        }
        #endregion

        #region update Heating Boiler Info
        public bool updateHeatingBoilerInfo(BoilerData boilerData, string itemName, string parameterName, object parameterValue, int? updatedBy)
        {
            bool result = false;
            var itemParams = (from ip in context.PA_ITEM_PARAMETER
                              join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                              join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                              where p.ParameterName == parameterName && i.ItemName == itemName
                              select new { itemParamId = ip.ItemParamID, controlType = p.ControlType, heatingFuel = ip.ParameterValueId });

            // Currently multiple heating types exists against the property but in future when scheme/block will
            // contains multiple heating types same filter will be applied to scheme/block as well. 
            if (boilerData.propertyId != null)
            {
                var heatingType = context.PA_HeatingMapping.Where(x => x.HeatingMappingId == boilerData.heatingId).First();
                itemParams = itemParams.Where(x => x.heatingFuel == heatingType.HeatingType);
            }     

            if (itemParams.Count() > 0)
            {
                var itemParam = itemParams.First();

                #region "Get Attribute Value Detail"

                int? valueId = null;
                bool? isCheckBoxSelected = null;
                string paramValue = null;
                if (itemParam.controlType.Equals(ApplicationConstants.DropdownParameterKey) && parameterValue != null)
                {
                    valueId = (int?)parameterValue;
                    paramValue = context.PA_PARAMETER_VALUE.Where(ppv => ppv.ValueID == valueId).FirstOrDefault().ValueDetail;
                }
                else if (itemParam.controlType.Equals(ApplicationConstants.TextBoxParameterKey) && parameterValue != null)
                {
                    paramValue = (string)parameterValue;
                }
                else if (itemParam.controlType.Equals(ApplicationConstants.CheckboxesParameterKey) && parameterValue != null)
                {
                    valueId = getParameterValueIdValueOfProperty(boilerData, itemName, parameterName);
                    isCheckBoxSelected = parameterValue.Equals("1") ? true : false;
                }
                #endregion

                #region "Update PA_PROPERTY_ATTRIBUTES"
                var propertyAttribute = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.HeatingMappingId == boilerData.heatingId && patt.ITEMPARAMID == itemParam.itemParamId);
                if (propertyAttribute.Count() > 0)
                {
                    PA_PROPERTY_ATTRIBUTES attribute = propertyAttribute.First();
                    attribute.PARAMETERVALUE = paramValue;
                    attribute.VALUEID = valueId;
                    attribute.IsCheckBoxSelected = isCheckBoxSelected;
                    attribute.UPDATEDON = DateTime.Now;
                    attribute.UPDATEDBY = updatedBy;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }
                else
                {                    
                    PA_PROPERTY_ATTRIBUTES attributes = new PA_PROPERTY_ATTRIBUTES();
                    attributes.PROPERTYID = boilerData.propertyId;
                    attributes.SchemeId = boilerData.schemeId;
                    attributes.BlockId = boilerData.blockId;
                    attributes.HeatingMappingId = boilerData.heatingId;
                    attributes.ITEMPARAMID = itemParam.itemParamId;
                    attributes.PARAMETERVALUE = paramValue;
                    attributes.IsCheckBoxSelected = isCheckBoxSelected;
                    attributes.UPDATEDON = DateTime.Now;
                    attributes.UPDATEDBY = updatedBy;
                    context.AddToPA_PROPERTY_ATTRIBUTES(attributes);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }
                #endregion

            }
            return result;
        }
        #endregion

        #region Get Parameter Value Id of Property
        public int? getParameterValueIdValueOfProperty(BoilerData boilerData, string itemName, string parameterName)
        {
            int? result = null;
            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == parameterName && i.ItemName == itemName
                             select new { ip.ItemParamID, heatingFuel = ip.ParameterValueId });

            // Currently multiple heating types exists against the property but in future when scheme/block will
            // contains multiple heating types same filter will be applied to scheme/block as well. 
            if (boilerData.propertyId != null)
            {
                var heatingType = context.PA_HeatingMapping.Where(x => x.HeatingMappingId == boilerData.heatingId).First();
                itemParam = itemParam.Where(x => x.heatingFuel == heatingType.HeatingType);
            }   

            if (itemParam.Count() > 0)
            {
                int itemParamId = itemParam.First().ItemParamID;
                var attributeId = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.HeatingMappingId == boilerData.heatingId && patt.ITEMPARAMID == itemParamId);
                if (attributeId.Count() > 0)
                {
                    result = (int?)attributeId.First().VALUEID;
                }
            }
            return result;
        }
        #endregion

        #region get General Comments
        /// <summary>
        /// This function returns General Comments
        /// </summary>
        /// <returns>List of General Comments</returns>
        public List<FaultsData> getGeneralComments(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID && fault.FaultCategory.ToLower() == "general comment"
                        select new FaultsData
                        {
                            ID = fault.ID,
                            AppointmentID = fault.AppointmentID,
                            DefectDesc = fault.DefectDesc,
                            FaultCategory = fault.FaultCategory,
                            isAdviceNoteIssued = fault.isAdviceNoteIssued,
                            RemedialAction = fault.RemedialAction,
                            SerialNo = fault.SerialNo,
                            WarningTagFixed = fault.WarningTagFixed

                        });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns General Comments for gas
        /// </summary>
        /// <returns>List of General Comments</returns>
        //public List<FaultsDataGas> getGeneralCommentsGas(string PropertyID)
        //{
        //    var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
        //                where fault.PropertyId == PropertyID && fault.CategoryId == 1
        //                select new FaultsDataGas
        //                {
        //                    RemedialAction = fault.ActionNotes,
        //                    ApplianceID = fault.ApplianceId,
        //                    FaultCategory = fault.CategoryId,
        //                    DefectDate = fault.DefectDate,
        //                    DefectDesc = fault.DefectNotes,
        //                    IsActionTaken = fault.IsActionTaken,
        //                    IsDefectIdentified = fault.IsDefectIdentified,
        //                    WarningTagFixed = fault.IsWarningFixed,
        //                    isAdviceNoteIssued = fault.IsWarningIssued,
        //                    JournalId = fault.JournalId,
        //                    ID = fault.PropertyDefectId,
        //                    PropertyId = fault.PropertyId,
        //                    SerialNo = fault.SerialNumber                            
        //                });

        //    List<FaultsDataGas> faults = new List<FaultsDataGas>();
        //    if (_var.Count() > 0)
        //    {
        //        faults = _var.ToList();
        //    }

        //    return faults;
        //}
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId)
        {
            var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where fault.JournalId == journalId && fault.CategoryId == Utilities.Constants.MessageConstants.GeneralCommentCategoryId
                        select new FaultsDataGas
                        {
                            remedialActionNotes = fault.ActionNotes,
                            ApplianceID = fault.ApplianceId,
                            faultCategory = fault.CategoryId,
                            defectDate = fault.DefectDate,
                            defectNotes = fault.DefectNotes,
                            isRemedialActionTaken = fault.IsActionTaken,
                            isDefectIdentified = fault.IsDefectIdentified,
                            warningTagFixed = fault.IsWarningFixed,
                            isAdviceNoteIssued = fault.IsWarningIssued,
                            JournalId = fault.JournalId,
                            ID = fault.PropertyDefectId,
                            propertyId = fault.PropertyId,
                            serialNumber = fault.SerialNumber
                        });

            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }
        //Change#21 - Behroz - 19/12/2012 - End
        #endregion

        #region get Faults only.

        /// <summary>
        /// This function returns Faults only.
        /// </summary>
        /// <returns>List of Faults.</returns>
        public List<FaultsData> getFaultsOnly(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID && fault.FaultCategory.ToLower() != "general comment"
                        select new FaultsData
                        {
                            ID = fault.ID,
                            AppointmentID = fault.AppointmentID,
                            DefectDesc = fault.DefectDesc,
                            FaultCategory = fault.FaultCategory,
                            isAdviceNoteIssued = fault.isAdviceNoteIssued,
                            RemedialAction = fault.RemedialAction,
                            SerialNo = fault.SerialNo,
                            WarningTagFixed = fault.WarningTagFixed

                        });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        public List<FaultsDataGas> getFaultsOnlyGas(int journalId)
        {
            var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where fault.JournalId == journalId && fault.CategoryId != Utilities.Constants.MessageConstants.GeneralCommentCategoryId
                        select new FaultsDataGas
                        {
                            remedialActionNotes = fault.ActionNotes,
                            ApplianceID = fault.ApplianceId,
                            faultCategory = fault.CategoryId,
                            defectDate = fault.DefectDate,
                            defectNotes = fault.DefectNotes,
                            isRemedialActionTaken = fault.IsActionTaken,
                            isDefectIdentified = fault.IsDefectIdentified,
                            warningTagFixed = fault.IsWarningFixed,
                            isAdviceNoteIssued = fault.IsWarningIssued,
                            JournalId = fault.JournalId,
                            ID = fault.PropertyDefectId,
                            propertyId = fault.PropertyId,
                            serialNumber = fault.SerialNumber
                        });

            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        #endregion

        #region Get All Defect Categories

        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of DefectCategory data objects</returns>
        public List<DefectCategoryData> getAllDefectCategories()
        {
            var _var = (from cat in context.P_DEFECTS_CATEGORY
                        orderby cat.CategoryId ascending
                        select new DefectCategoryData
                        {
                            CatId = cat.CategoryId,
                            CatDescription = cat.Description

                        });

            List<DefectCategoryData> faults = new List<DefectCategoryData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
                faults.Add(new DefectCategoryData() { CatId = -1, CatDescription = "Remedial Action Taken" });
            }

            return faults;
        }

        #endregion

        #region "Get Fault id"

        /// <summary>
        /// This function returns the fault id
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getFaultId(string propertyId)
        {
            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            using (TransactionScope trans = new TransactionScope())
            {
                fault.CategoryId = 1;
                fault.PropertyId = propertyId;

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region "Delete Fault"

        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public bool deleteFault(int faultId)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                List<P_PROPERTY_APPLIANCE_DEFECTS_IMAGES> faultImages = context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.Where(img => img.PropertyDefectId == faultId).ToList();

                foreach (var item in faultImages)
                {
                    context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.DeleteObject(item);
                    context.SaveChanges();
                }


                P_PROPERTY_APPLIANCE_DEFECTS fault = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(fau => fau.PropertyDefectId == faultId).First();
                context.P_PROPERTY_APPLIANCE_DEFECTS.DeleteObject(fault);
                context.SaveChanges();

                trans.Complete();
                success = true;
            }

            return success;
        }

        #endregion

        #region Save Defect Image

        /// <summary>
        /// This function saves the fault image data in database
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="faultId"></param>
        /// <returns></returns>
        public int saveFaultImageData(string fileName, int faultId, bool? isBeforeImage, string imageIdentifier = "")
        {
            int success = 0;
            using (TransactionScope trans = new TransactionScope())
            {
                P_PROPERTY_APPLIANCE_DEFECTS_IMAGES appDefectImage = new P_PROPERTY_APPLIANCE_DEFECTS_IMAGES()
                {
                    PropertyDefectId = faultId,
                    ImageTitle = fileName,
                    ImagePath = Utilities.Helpers.FileHelper.getPropertyImageUploadPath(),
                    ImageIdentifier = imageIdentifier,
                    isBefore = isBeforeImage
                };

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS_IMAGES(appDefectImage);
                context.SaveChanges();
                trans.Complete();
                success = appDefectImage.PropertyDefectImageId;
            }

            return success;
        }

        #endregion

        #region "Delete Fault"

        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public FaultImagesData deleteFaultImage(int faultImageId)
        {

            P_PROPERTY_APPLIANCE_DEFECTS_IMAGES defectImage = new P_PROPERTY_APPLIANCE_DEFECTS_IMAGES();
            FaultImagesData flImageData = new FaultImagesData();
            var propPic = (from pric in context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
                           where pric.PropertyDefectImageId == faultImageId
                           select new FaultImagesData
                           {
                               PropertyDefectImageId = pric.PropertyDefectImageId,
                               PropertyDefectId = pric.PropertyDefectId,
                               ImageTitle = pric.ImageTitle,
                               ImagePath = pric.ImagePath
                           }
                     );
            if (propPic.Count() > 0)
            {
                flImageData = propPic.First();
                defectImage.PropertyDefectImageId = faultImageId;
                context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.Attach(defectImage);
                context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.DeleteObject(defectImage);
                context.SaveChanges();
            }

            return flImageData;
        }

        #endregion

        #region Check Appointment

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app => app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkAppointmentIDGas(int journalId)
        {
            var id = context.AS_APPOINTMENTS.Where(app => app.JournalId == journalId);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region "Validate Category Id"

        public string validateFaultsCategories(int? CatId)
        {
            var id = context.P_DEFECTS_CATEGORY.Where(cat => cat.CategoryId == CatId);

            if (id.Count() > 0)
                return id.First().Description;
            else
                return string.Empty;
        }

        /// <summary>
        /// This function validates the fault id
        /// </summary>
        /// <param name="CatId"></param>
        /// <returns></returns>
        public bool validateFaultsId(int faultId)
        {
            var id = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(fault => fault.PropertyDefectId == faultId);

            if (id.Count() > 0)
                return true;
            else
                return false;
        }

        #endregion

        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair </returns>
        public List<FaultRepairsData> getFaultRepairList(int faultLogId)
        {
            var faultRepairList = (from faultRepair in context.FL_FAULT_REPAIR_LIST
                                   join associatedFault in
                                       (from fr in context.FL_FAULT_ASSOCIATED_REPAIR
                                        join fl in context.FL_FAULT_LOG on fr.FaultId equals fl.FaultID
                                        where fl.FaultLogID == faultLogId && fr.RepairId != null
                                        group fr by new { fr.FaultId, fr.RepairId }
                                        into repairsgroup
                                        select repairsgroup.FirstOrDefault()) on faultRepair.FaultRepairListID equals associatedFault.RepairId
                                    into selectedAssociatedFault
                                   from saf in selectedAssociatedFault.DefaultIfEmpty()
                                   where faultRepair.RepairActive == true
                                   select new FaultRepairsData
                                   {
                                       faultRepairId = faultRepair.FaultRepairListID,
                                       description = faultRepair.Description,
                                       isAssociated = (saf.RepairId == null ? 0 : 1)
                                   }
            );
            List<FaultRepairsData> faultRepairDataList = new List<FaultRepairsData>();
            if (faultRepairList.Count() > 0)
            {
                faultRepairDataList = faultRepairList.ToList();
            }

            return faultRepairDataList;

        }

        #endregion

        #region Get Complete Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repairs </returns>
        public List<RepairsData> getCompleteRepairList()
        {

            int faultLogId = ApplicationConstants.DefaultFaultLogId;
            var repairList = (from repair in context.FL_FAULT_REPAIR_LIST
                                   join associatedFault in
                                       (from fr in context.FL_FAULT_ASSOCIATED_REPAIR
                                        join fl in context.FL_FAULT_LOG on fr.FaultId equals fl.FaultID
                                        where fl.FaultLogID == faultLogId && fr.RepairId != null
                                        group fr by new { fr.FaultId, fr.RepairId }
                                            into repairsgroup
                                            select repairsgroup.FirstOrDefault()) on repair.FaultRepairListID equals associatedFault.RepairId
                                    into selectedAssociatedFault
                                   from saf in selectedAssociatedFault.DefaultIfEmpty()
                                   where repair.RepairActive == true
                                   select new RepairsData
                                   {
                                       faultRepairId = repair.FaultRepairListID,
                                       description = repair.Description,
                                       isAssociated = (saf.RepairId == null ? 0 : 1),
                                       gross = repair.Gross
                                   }
            );
            List<RepairsData> repairDataList = new List<RepairsData>();
            if (repairList.Count() > 0)
            {
                repairDataList = repairList.ToList();
            }

            return repairDataList;

        }

        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        public List<string> getPauseReasonList()
        {
            List<string> pauseReasonList = new List<string>();

            var queryReasonList = (from pauseReason in context.FL_PAUSED_REASON
                                   select new { pauseReason.Reason }).ToList();
            if (queryReasonList.Count() > 0)
            {
                foreach (var item in queryReasonList)
                {
                    pauseReasonList.Add(item.Reason);
                }
            }

            return pauseReasonList;
        }

        public List<PausedReasonList> getPausedReasonList()
        {


            var queryReasonList = (from pauseReason in context.FL_PAUSED_REASON
                                   where 1 == 1
                                   select new PausedReasonList
                                   {
                                       pauseId = pauseReason.PauseId,
                                       reason = pauseReason.Reason
                                   });
            List<PausedReasonList> pauseReasonList = new List<PausedReasonList>();

            if (queryReasonList.Count() > 0)
            {
                pauseReasonList = queryReasonList.ToList();
            }

            return pauseReasonList;
        }

        #endregion

        #region get CO2 Detector is inspected

        /// <summary>
        /// This function returns CO2 detector data against propertyid and journalid
        /// </summary>
        /// <returns></returns>
        public bool isInspectedCO2Detector(string propertyID, int appointmentId)
        {
            var co2DetectorData = (from coIns in context.P_CO2_Inspection
                                   join jor in context.AS_JOURNAL on coIns.Journalid equals jor.JOURNALID
                                   join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                   where coIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId

                                   select coIns);

            if (co2DetectorData.Count() > 0)
            {
                return (bool)co2DetectorData.First().IsInspected;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region get CO2 Detector Inspection

        /// <summary>
        /// This function returns CO2 detector data against propertyid and journalid
        /// </summary>
        /// <returns></returns>
        public DetectorInspectionData CO2DetectorInspection(string propertyID, int appointmentId)
        {
            DetectorInspectionData detectorInspectionData = new DetectorInspectionData();

            var co2DetectorData = (from coIns in context.P_CO2_Inspection
                                   join jor in context.AS_JOURNAL on coIns.Journalid equals jor.JOURNALID
                                   join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                   where coIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId

                                   select new DetectorInspectionData
                                   {
                                       detectorTest = coIns.DetectorTest,
                                       inspectionDate = coIns.DateStamp,
                                       inspectionID = coIns.DectectorId

                                   });

            if (co2DetectorData.Count() > 0)
            {
                detectorInspectionData = co2DetectorData.First();
                return detectorInspectionData;
            }
            else
            {
                return detectorInspectionData;
            }
        }

        #endregion

        #region get Smoke Detector is inspected

        /// <summary>
        /// This function returns smoke detector data against propertyid and journalid
        /// </summary>
        /// <returns>Smoke Inspection Object</returns>
        public bool isInspectedSmokeDetector(string propertyID, int appointmentId)
        {
            var smokeDetectorData = (from smIns in context.P_Smoke_Inspection
                                     join jor in context.AS_JOURNAL on smIns.Journalid equals jor.JOURNALID
                                     join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                     where smIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId
                                     select smIns);

            if (smokeDetectorData.Count() > 0)
            {
                return (bool)smokeDetectorData.First().IsInspected;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region get Smoke Detector inspection

        /// <summary>
        /// This function returns smoke detector data against propertyid and journalid
        /// </summary>
        /// <returns>Smoke Inspection Object</returns>
        public DetectorInspectionData smokeDetectorInspection(string propertyID, int appointmentId)
        {
            DetectorInspectionData detectorInspectionData = new DetectorInspectionData();
            var smokeDetectorData = (from smIns in context.P_Smoke_Inspection
                                     join jor in context.AS_JOURNAL on smIns.Journalid equals jor.JOURNALID
                                     join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                     where smIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId
                                     select new DetectorInspectionData
                                     {
                                         detectorTest = smIns.DetectorTest,
                                         inspectionDate = smIns.DateStamp,
                                         inspectionID = smIns.DectectorId

                                     });

            if (smokeDetectorData.Count() > 0)
            {
                detectorInspectionData = smokeDetectorData.First();
                return detectorInspectionData;
            }
            else
            {
                return detectorInspectionData;
            }
        }

        #endregion

        #region Save Fault Repair Image

        /// <summary>
        /// This function saves the fault image data in database
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="faultId"></param>
        /// <returns></returns>
        public int saveFaultRepairImage(string PropertyId, int schemeId, int blockId, string jsNumber, string imageName, bool isBeforeImage, int createdBy, string ImageType, string imageIdentifier = "")
        {
            int faultRepairImageId = 0;
            DateTime createdOn = DateTime.Now;
            using (TransactionScope trans = new TransactionScope())
            {
                if (ImageType.Replace(" ", "") == VoidAppointmentTypes.VoidWorks.ToString())
                {
                    int rWorkId = Convert.ToInt32(jsNumber.Substring(3, jsNumber.Length - 3));
                    V_RequiredWorks voidRecordWorks = new V_RequiredWorks();
                    var voidRecordWorksData = context.V_RequiredWorks.Where(x => x.RequiredWorksId == rWorkId);
                    if (voidRecordWorksData.Count() > 0)
                    {
                        voidRecordWorks = voidRecordWorksData.First();
                        V_Images voidRepairImages = new V_Images()
                        {
                            PropertyId = PropertyId,
                            JournalId = voidRecordWorks.WorksJournalId,
                            ImageName = imageName,
                            IsAfterWorkImage = isBeforeImage,
                            CreatedBy = createdBy,
                            CreatedOn = createdOn,
                            SchemeId = null,
                            BlockId = null,
                            RequiredWorksId = rWorkId

                        };
                        context.AddToV_Images(voidRepairImages);
                        context.SaveChanges();

                        trans.Complete();
                        faultRepairImageId = voidRepairImages.ImageId;
                    }
                }
                else
                {
                    FL_FAULT_REPAIR_IMAGES repairImages = new FL_FAULT_REPAIR_IMAGES()
                    {
                        PropertyId = PropertyId,
                        JobSheetNumber = jsNumber,
                        ImageName = imageName,
                        IsBeforeImage = isBeforeImage,
                        CreatedBy = createdBy,
                        CreatedOn = createdOn,
                        SchemeId = schemeId,
                        BlockId = blockId,
                        ImageIdentifier = imageIdentifier
                    };

                    context.AddToFL_FAULT_REPAIR_IMAGES(repairImages);
                    context.SaveChanges();

                    trans.Complete();
                    faultRepairImageId = repairImages.FaultRepairImageId;
                }
            }

            return faultRepairImageId;
        }

        #endregion

        #region is Fault Repair Image already exists

        /// <summary>
        /// check if fault repair Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="faultRepairImageId"></param>
        public bool isFaultRepairImageAlreadyExists(string imageIdentifier, out string imageName, out int faultRepairImageId)
        {
            bool isFaultRepairImageAlreadyExists = false;
            imageName = string.Empty;
            faultRepairImageId = -1;

            if (imageIdentifier != string.Empty)
            {
                var faultRepairImagesList = from img in context.FL_FAULT_REPAIR_IMAGES
                                            where img.ImageIdentifier == imageIdentifier
                                            select img;
                if (faultRepairImagesList.Count() > 0)
                {
                    isFaultRepairImageAlreadyExists = true;
                    var imageData = faultRepairImagesList.First();
                    imageName = imageData.ImageName;
                    faultRepairImageId = imageData.FaultRepairImageId;
                }


            }
            return isFaultRepairImageAlreadyExists;
        }

        #endregion

        #region is Property Defect Image already exists

        /// <summary>
        /// check if property defect Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="faultRepairImageId"></param>
        public bool ispropertyDefectImageAlreadyExists(string imageIdentifier, out string imageName, out int propertyDefectImageId)
        {
            bool isPropertyDefectAlreadyExists = false;
            imageName = string.Empty;
            propertyDefectImageId = -1;

            if (imageIdentifier != string.Empty)
            {
                var faultRepairImagesList = from img in context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
                                            where img.ImageIdentifier == imageIdentifier
                                            select img;
                if (faultRepairImagesList.Count() > 0)
                {
                    isPropertyDefectAlreadyExists = true;
                    var imageData = faultRepairImagesList.First();
                    imageName = imageData.ImageTitle;
                    propertyDefectImageId = imageData.PropertyDefectImageId;
                }
            }
            return isPropertyDefectAlreadyExists;
        }

        #endregion

        #region Get All Defect Images By Defect Id

        public List<RepairImageData> getAllDefectImagesByDefectId(int defectId)
        {
            List<RepairImageData> objFaultImagesDataList = new List<RepairImageData>();

            var defectImages = from defImage in context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
                               join def in context.P_PROPERTY_APPLIANCE_DEFECTS on defImage.PropertyDefectId equals def.PropertyDefectId
                               where defImage.PropertyDefectId == defectId
                                && defImage.isBefore != null
                               select new
                               {
                                   image = defImage,
                                   defect = def
                               };

            if (defectImages.Count() > 0)
            {
                foreach (var defImage in defectImages)
                {
                    int schemeId = defImage.defect.SchemeId == null ? 0 : (int)defImage.defect.SchemeId;
                    int blockId = defImage.defect.BlockId == null ? 0 : (int)defImage.defect.BlockId;

                    RepairImageData objFaultImagesData = new RepairImageData()
                        {
                            faultRepairImageId = defImage.image.PropertyDefectImageId,
                            imageName = defImage.image.ImageTitle,
                            imagePath = string.IsNullOrEmpty(defImage.defect.PropertyId) 
                                        ? FileHelper.getLogicalPropertyImagePath(defImage.defect.PropertyId, defImage.image.ImageTitle)
                                        : FileHelper.getLogicalSchemeBlockImagePath(schemeId, blockId, defImage.image.ImageTitle),
                            isBeforeImage = defImage.image.isBefore,
                            jsNumber = GeneralHelper.getJobSheetDefectNumber(defImage.defect.PropertyDefectId),
                            propertyId = defImage.defect.PropertyId,
                            schemeId = defImage.defect.SchemeId,
                            blockId = defImage.defect.BlockId
                        };
                    objFaultImagesDataList.Add(objFaultImagesData);
                }
            }

            return objFaultImagesDataList;
        }

        #endregion

        #region Get PDR Status Id
        public int? GetPdrStatusId(string status)
        {
            int? statusId = null;
            var statusResult = context.PDR_STATUS
                                .Where(x => x.AppTitle == status)
                                .FirstOrDefault();
            if (statusResult != null)
            {
                statusId = statusResult.STATUSID;
            }

            return statusId;

        }
        #endregion

    }
}
