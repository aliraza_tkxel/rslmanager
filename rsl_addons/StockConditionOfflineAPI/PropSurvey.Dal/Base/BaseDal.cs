﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Entities;


namespace PropSurvey.Dal.Base
{
    public class BaseDal
    {
        protected PropertySurvey_Entities context;

        public BaseDal()
        {
            context = new PropertySurvey_Entities();
            context.CommandTimeout = 7200; // Increasing the Database Time Out to 5 mins by Abdul Wahhab - 15/07/2013
        }
    }
}
