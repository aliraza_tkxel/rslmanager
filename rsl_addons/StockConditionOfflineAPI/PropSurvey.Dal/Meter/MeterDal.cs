﻿using System;
using System.Linq;
using PropSurvey.Contracts.Data;
using PropSurvey.Entities;
using System.Collections.Generic;

namespace PropSurvey.Dal
{
    public class MeterDal : Base.BaseDal
    {
        #region "Gas Meter"

        #region Get Gas Meter By PropertyId

        public Meter GetGasMeterByPropertyId(string propertyId)
        {
            Meter gasMeter = new Meter();
            gasMeter.deviceType = "Gas Meter";
            gasMeter.propertyId = propertyId;

            IQueryable<PropertyAttribute> gasMeterQuery = GetItemParametersAndValuesByPropertyIdAndFilters(propertyId, "Internals", "Services", "Gas", "Meters");

            if (gasMeterQuery.Count() > 0)
                gasMeter.deviceTypeId = gasMeterQuery.First().itemID;

            populateMeterFromAttributes(gasMeter, gasMeterQuery);

            return gasMeter;
        }

        #endregion

        #region Get Gas Meter By SchemeId

        public Meter GetGasMeterBySchemeId(int? schemeId)
        {
            Meter gasMeter = new Meter();
            gasMeter.deviceType = "Gas Meter";
            gasMeter.schemeId = schemeId;

            IQueryable<PropertyAttribute> gasMeterQuery = GetItemParametersAndValuesBySchemeIdAndFilters(schemeId, null, "Meters", "Gas");

            if (gasMeterQuery.Count() > 0)
                gasMeter.deviceTypeId = gasMeterQuery.First().itemID;

            populateMeterFromAttributes(gasMeter, gasMeterQuery);

            return gasMeter;
        }

        #endregion

        #region Get Gas Meter By BlockId

        public Meter GetGasMeterByBlockId(int? blockId)
        {
            Meter gasMeter = new Meter();
            gasMeter.deviceType = "Gas Meter";
            gasMeter.blockId = blockId;

            IQueryable<PropertyAttribute> gasMeterQuery = GetItemParametersAndValuesByBlockIdAndFilters(blockId, null, "Meters", "Gas");

            if (gasMeterQuery.Count() > 0)
                gasMeter.deviceTypeId = gasMeterQuery.First().itemID;

            populateMeterFromAttributes(gasMeter, gasMeterQuery);

            return gasMeter;
        }

        #endregion

        #endregion

        #region "Electric Meter"

        #region Get Electic Meter By PropertyId

        public Meter GetElectricMeterByPropertyId(string propertyId)
        {
            Meter electricMeter = new Meter();
            electricMeter.deviceType = "Electric Meter";

            electricMeter.propertyId = propertyId;

            IQueryable<PropertyAttribute> electricMeterQuery = GetItemParametersAndValuesByPropertyIdAndFilters(propertyId, "Internals", "Services", "Electric", "Meters");

            if (electricMeterQuery.Count() > 0)
                electricMeter.deviceTypeId = electricMeterQuery.First().itemID;

            populateMeterFromAttributes(electricMeter, electricMeterQuery);

            return electricMeter;
        }

        #endregion

        #region Get Electic Meter By SchemeId

        public Meter GetElectricMeterBySchemeId(int? schemeId)
        {
            Meter electricMeter = new Meter();
            electricMeter.deviceType = "Electric Meter";
            electricMeter.schemeId = schemeId;

            IQueryable<PropertyAttribute> electricMeterQuery = GetItemParametersAndValuesBySchemeIdAndFilters(schemeId, null, "Meters", "Electric");

            if (electricMeterQuery.Count() > 0)
                electricMeter.deviceTypeId = electricMeterQuery.First().itemID;

            populateMeterFromAttributes(electricMeter, electricMeterQuery);

            return electricMeter;
        }

        #endregion

        #region Get Electic Meter By BlockId

        public Meter GetElectricMeterByBlockId(int? blockId)
        {
            Meter electricMeter = new Meter();
            electricMeter.deviceType = "Electric Meter";
            electricMeter.blockId = blockId;

            IQueryable<PropertyAttribute> electricMeterQuery = GetItemParametersAndValuesByBlockIdAndFilters(blockId, null, "Meters", "Electric");

            if (electricMeterQuery.Count() > 0)
                electricMeter.deviceTypeId = electricMeterQuery.First().itemID;

            populateMeterFromAttributes(electricMeter, electricMeterQuery);

            return electricMeter;
        }

        #endregion

        #endregion

        #region Populate Meter Object for Property Attributes

        private void populateMeterFromAttributes(Meter meter, IQueryable<PropertyAttribute> meterQueryResults)
        {
            if (meterQueryResults.Count() > 0)
            {
                foreach (PropertyAttribute item in meterQueryResults)
                {
                    switch (item.ParameterName.ToLower())
                    {
                        //case "gas meter type":
                        //case "electric meter type":
                        case "meter type":
                            meter.meterTypeId = item.ValueId;
                            //meter.meterType = item.ValueDetail;
                            break;

                        //case "gas meter location":                        
                        //case "electric meter location":
                        case "location":
                            meter.location = item.ValueDetail;
                            break;

                        case "manufacturer":
                            meter.manufacturer = item.ValueDetail;
                            break;

                        case "serial number":
                            meter.serialNumber = item.ValueDetail;
                            break;

                        case "installed":
                            if (item.isDate)
                            {
                                meter.installedDate = item.dateValue;
                            }
                            break;

                        //case "electric meter reading":
                        //case "gas meter reading":
                        //case "meter reading":
                        case "reading":

                            meter.lastReading = item.ValueDetail;

                            break;

                        //case "electric meter reading date":
                        //case "gas meter reading date":
                        //case "meter reading date":
                        case "reading date":
                            if (item.isDate)
                            {
                                meter.lastReadingDate = item.dateValue;
                            }
                            break;

                        case "passed":
                            if (item.ValueDetail != null)
                            {
                                if (item.ValueDetail.ToLower() == "yes" || item.ValueDetail == "1") meter.isPassed = true;
                                else if (item.ValueDetail.ToLower() == "no" || item.ValueDetail == "0") meter.isPassed = false;
                            }
                            break;

                        case "notes":
                            meter.notes = item.ValueDetail;
                            break;

                        case "capped":
                            if (item.ValueDetail != null)
                            {
                                if (item.ValueDetail.ToLower() == "yes" || item.ValueDetail == "1") meter.isCapped = true;
                                else if (item.ValueDetail.ToLower() == "no" || item.ValueDetail == "0") meter.isCapped = false;
                            }
                            break;

                        case "last inspected":
                            if (item.isDate)
                            {
                                meter.lastReadingDate = item.dateValue;
                            }
                            break;
                    }
                }
            }
        }

        #endregion

        #region Get Item Parameters And Parameter Values By Property Id, Item name

        IQueryable<PropertyAttribute> GetItemParametersAndValuesByPropertyIdAndFilters(string propertyId, string locationName, string areaName, string itemName, string parentItemName = null)
        {
            IQueryable<PropertyAttribute> query = from I in context.PA_ITEM
                                                  join PItem in context.PA_ITEM on I.ParentItemId  equals PItem.ItemID  into PItem_join
                                                  from PItem in PItem_join.DefaultIfEmpty()
                                                  join A in context.PA_AREA
                                                        on I.AreaID
                                                    equals A.AreaID
                                                  join L in context.PA_LOCATION
                                                  on A.LocationId equals L.LocationID
                                                 
                                                  join IP in context.PA_ITEM_PARAMETER
                                                        on new { I.ItemID, IsActive = (Boolean?)true }
                                                    equals new { ItemID = (Int32)IP.ItemId, IsActive = IP.IsActive }
                                                  join P in context.PA_PARAMETER
                                                        on new { ParameterId = (Int32)IP.ParameterId, IsActive = (Boolean?)true, ShowInApp = (Boolean?)true }
                                                    equals new { ParameterId = P.ParameterID, IsActive = P.IsActive, ShowInApp = P.ShowInApp }
                                                  join PA in context.PA_PROPERTY_ATTRIBUTES
                                                        on new { IP.ItemParamID, PROPERTYID = propertyId }
                                                    equals new { ItemParamID = (Int32)PA.ITEMPARAMID, PA.PROPERTYID } into PA_join
                                                  from PA in PA_join.DefaultIfEmpty()
                                                  join PV in context.PA_PARAMETER_VALUE on new { VALUEID = (Int64)PA.VALUEID } equals new { VALUEID = PV.ValueID } into PV_join
                                                  from PV in PV_join.DefaultIfEmpty()
                                                  join PID in context.PA_PROPERTY_ITEM_DATES on new { ItemId = IP.ItemId, ParameterId = IP.ParameterId, PropertyId = propertyId }
                                                    equals new { ItemId = PID.ItemId, ParameterId = PID.ParameterId, PropertyId = PID.PROPERTYID } into PID_join
                                                  from PID in PID_join.DefaultIfEmpty()
                                                  where
                                                    I.ItemName.Contains(itemName)
                                                    && PItem.ItemName == parentItemName
                                                    && A.AreaName == areaName && A.IsActive==true
                                                    && L.LocationName == locationName
                                                  select new PropertyAttribute()
                                                  {
                                                      itemID = I.ItemID,
                                                      ParameterName = P.ParameterName,
                                                      ValueDetail = (PA.PARAMETERVALUE ?? PV.ValueDetail),
                                                      ValueId = PA.VALUEID,
                                                      IsCheckBoxSelected = PA.IsCheckBoxSelected,
                                                      updatedBy = PA.UPDATEDBY,
                                                      isDate = P.IsDate,
                                                      dateValue = PID.DueDate
                                                  };

            return query;
        }

        #endregion

        #region Get Item Parameters And Parameter Values By Scheme Id, Item name

        IQueryable<PropertyAttribute> GetItemParametersAndValuesBySchemeIdAndFilters(int? schemeId, string locationName, string areaName, string itemName, string parentItemName = null)
        {
            IQueryable<PropertyAttribute> query = from I in context.PA_ITEM
                                                  join PItem in context.PA_ITEM on I.ParentItemId equals PItem.ItemID into PItem_join
                                                  from PItem in PItem_join.DefaultIfEmpty()
                                                  join A in context.PA_AREA
                                                        on I.AreaID
                                                    equals A.AreaID

                                                  join IP in context.PA_ITEM_PARAMETER
                                                        on new { I.ItemID, IsActive = (Boolean?)true }
                                                    equals new { ItemID = (Int32)IP.ItemId, IsActive = IP.IsActive }
                                                  join P in context.PA_PARAMETER
                                                        on new { ParameterId = (Int32)IP.ParameterId, IsActive = (Boolean?)true, ShowInApp = (Boolean?)true }
                                                    equals new { ParameterId = P.ParameterID, IsActive = P.IsActive, ShowInApp = P.ShowInApp }
                                                  join PA in context.PA_PROPERTY_ATTRIBUTES
                                                        on new { IP.ItemParamID, SchemeId = (int?)schemeId }
                                                    equals new { ItemParamID = (Int32)PA.ITEMPARAMID, PA.SchemeId } into PA_join
                                                  from PA in PA_join.DefaultIfEmpty()
                                                  join PV in context.PA_PARAMETER_VALUE on new { VALUEID = (Int64)PA.VALUEID } equals new { VALUEID = PV.ValueID } into PV_join
                                                  from PV in PV_join.DefaultIfEmpty()
                                                  join PID in context.PA_PROPERTY_ITEM_DATES on new { ItemId = IP.ItemId, ParameterId = IP.ParameterId, SchemeId = (int?)schemeId }
                                                    equals new { ItemId = PID.ItemId, ParameterId = PID.ParameterId, SchemeId = PID.SchemeId } into PID_join
                                                  from PID in PID_join.DefaultIfEmpty()
                                                  where
                                                    I.ItemName.Contains(itemName)
                                                    && PItem.ItemName == null
                                                    && A.AreaName == areaName && A.IsActive == true
                                                    && A.LocationId == null
                                                  select new PropertyAttribute()
                                                  {
                                                      itemID = I.ItemID,
                                                      ParameterName = P.ParameterName,
                                                      ValueDetail = (PA.PARAMETERVALUE ?? PV.ValueDetail),
                                                      ValueId = PA.VALUEID,
                                                      IsCheckBoxSelected = PA.IsCheckBoxSelected,
                                                      updatedBy = PA.UPDATEDBY,
                                                      isDate = P.IsDate,
                                                      dateValue = PID.DueDate
                                                  };

            return query;
        }

        #endregion

        #region Get Item Parameters And Parameter Values By BlockId

        IQueryable<PropertyAttribute> GetItemParametersAndValuesByBlockIdAndFilters(int? blockId, string locationName, string areaName, string itemName, string parentItemName = null)
        {
            IQueryable<PropertyAttribute> query = from I in context.PA_ITEM
                                                  join PItem in context.PA_ITEM on I.ParentItemId equals PItem.ItemID into PItem_join
                                                  from PItem in PItem_join.DefaultIfEmpty()
                                                  join A in context.PA_AREA
                                                        on I.AreaID
                                                    equals A.AreaID

                                                  join IP in context.PA_ITEM_PARAMETER
                                                        on new { I.ItemID, IsActive = (Boolean?)true }
                                                    equals new { ItemID = (Int32)IP.ItemId, IsActive = IP.IsActive }
                                                  join P in context.PA_PARAMETER
                                                        on new { ParameterId = (Int32)IP.ParameterId, IsActive = (Boolean?)true, ShowInApp = (Boolean?)true }
                                                    equals new { ParameterId = P.ParameterID, IsActive = P.IsActive, ShowInApp = P.ShowInApp }
                                                  join PA in context.PA_PROPERTY_ATTRIBUTES
                                                        on new { IP.ItemParamID, BlockId = (int?)blockId }
                                                    equals new { ItemParamID = (Int32)PA.ITEMPARAMID, PA.BlockId } into PA_join
                                                  from PA in PA_join.DefaultIfEmpty()
                                                  join PV in context.PA_PARAMETER_VALUE on new { VALUEID = (Int64)PA.VALUEID } equals new { VALUEID = PV.ValueID } into PV_join
                                                  from PV in PV_join.DefaultIfEmpty()
                                                  join PID in context.PA_PROPERTY_ITEM_DATES on new { ItemId = IP.ItemId, ParameterId = IP.ParameterId, BlockId = (int?)blockId }
                                                    equals new { ItemId = PID.ItemId, ParameterId = PID.ParameterId, BlockId = PID.BlockId } into PID_join
                                                  from PID in PID_join.DefaultIfEmpty()
                                                  where
                                                    I.ItemName.Contains(itemName)
                                                    && PItem.ItemName == null
                                                    && A.AreaName == areaName && A.IsActive == true
                                                    && A.LocationId == null
                                                  select new PropertyAttribute()
                                                  {
                                                      itemID = I.ItemID,
                                                      ParameterName = P.ParameterName,
                                                      ValueDetail = (PA.PARAMETERVALUE ?? PV.ValueDetail),
                                                      ValueId = PA.VALUEID,
                                                      IsCheckBoxSelected = PA.IsCheckBoxSelected,
                                                      updatedBy = PA.UPDATEDBY,
                                                      isDate = P.IsDate,
                                                      dateValue = PID.DueDate
                                                  };

            return query;
        }

        #endregion

        #region Save Meter

        public bool saveMeter(Meter meter, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;

            if (currentContext != null) this.context = currentContext;
           
            string cappedValue = (meter.isCapped ?? false) ? "Yes" : "No";
            string passedValue = (meter.isPassed ?? false) ? "Yes" : "No";

            PropertyAttributesDal atrbDal = new PropertyAttributesDal();
            // Save Attribues (Except Dates)
            atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Meter Type", null, meter.meterTypeId, meter.inspectionDate, meter.inspectedBy, false, currentContext: context, schemeId: meter.schemeId,blockId:meter.blockId);
            atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Location", meter.location, null, meter.inspectionDate, meter.inspectedBy, false, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId);
            atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Reading", meter.reading.ToString(), null, meter.inspectionDate, meter.inspectedBy, false, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId);
            atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Manufacturer", meter.manufacturer, null, meter.inspectionDate, meter.inspectedBy, false, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId);
            atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Serial Number", meter.serialNumber, null, meter.inspectionDate, meter.inspectedBy, false, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId);
            if (meter.isInspected == true)
            {
                atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Capped", cappedValue, null, meter.inspectionDate, meter.inspectedBy, false, checkValueId: true, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId); ;
                if (meter.isPassed != null) atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Passed", passedValue, null, meter.inspectionDate, meter.inspectedBy, false, checkValueId: true, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId);
                atrbDal.saveMeterInfo(meter.propertyId, meter.deviceTypeId, "Notes", meter.notes, null, meter.inspectionDate, meter.inspectedBy, false, currentContext: context, schemeId: meter.schemeId, blockId: meter.blockId);

                //Save dates
                atrbDal.savePropertyDateAttribueByItemIdAndParameterName(meter.propertyId, meter.deviceTypeId, "Reading Date", meter.readingDate, null, meter.inspectionDate, meter.inspectedBy, meter.schemeId, meter.blockId, currentContext: context);
            }
            atrbDal.savePropertyDateAttribueByItemIdAndParameterName(meter.propertyId, meter.deviceTypeId, "Installed", meter.readingDate, null, meter.installedDate, meter.inspectedBy, meter.schemeId, meter.blockId, currentContext: context);
            atrbDal.savePropertyDateAttribueByItemIdAndParameterName(meter.propertyId, meter.deviceTypeId, "Last Inspected", meter.inspectionDate, null, meter.installedDate, meter.inspectedBy, meter.schemeId, meter.blockId, currentContext: context);

            saveStatus = true;

            return saveStatus;
        }

        #endregion

        #region Unused Functions

        #region Get All Meters By Property Id

        //public List<Meter> GetAllMetersByPropertyId(string PropertyId)
        //{
        //    List<Meter> meters = new List<Meter>();

        //    //Getting meters detail from database, joined meter type table to get meter type
        //    var metersQueryResult = (from meter in context.P_METER
        //                             join meterType in context.P_MeterType on meter.MeterTypeId equals meterType.MeterTypeId
        //                             where meter.PropertyId == PropertyId
        //                             select new Meter
        //                             {
        //                                 deviceTypeId = meter.MeterId,
        //                                 propertyId = meter.PropertyId,
        //                                 meterTypeId = meter.MeterTypeId,
        //                                 deviceType = meterType.MeterType,
        //                                 location = meter.Location,
        //                                 manufacturer = meter.Manufacturer,
        //                                 serialNumber = meter.SerialNumber,
        //                                 installedDate = meter.Installed,
        //                                 reading = meter.Reading,
        //                                 isPassed = meter.Passed,
        //                                 notes = meter.Notes,
        //                                 isCapped = meter.IsDisconnected
        //                             }).ToList();

        //    if (metersQueryResult.Count > 0)
        //        meters = metersQueryResult;

        //    return meters;
        //}

        #endregion

        #endregion



    }
}
