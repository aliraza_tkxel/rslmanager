﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal.Base;
using System.IO;
using System.Web;
using PropSurvey.Utilities.Helpers;

using PropSurvey.Utilities.Constants;
using PushSharp.Apple;
using PushSharp;
using System.Dynamic;
using PropSurvey.Contracts.Data;
using PushSharp.Core;

namespace PropSurvey.Dal.Push
{
    public class PushNotificationDal : BaseDal
    {

        

        #region send Push Notification for fault appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns></returns>
        public bool sendPushNotificationFault(int appointmentId, int type, string serverinUse, bool isSchemeBlockAppointment = false)
        {
            if (type < 1 || type > 3)
            {
                return false;
            }

            string[] typeOptions = { "New Reactive Repair Scheduled", "Reactive Repair Cancelled", "Reactive Repair Rearranged" };

            var appDataList = (from appointment in context.FL_CO_APPOINTMENT
                               join login in context.AC_LOGINS on appointment.OperativeID equals login.EMPLOYEEID
                               where appointment.AppointmentID == appointmentId
                               select new { appointment.AppointmentDate, appointment.Time, appointment.EndTime, login.DEVICETOKEN });

            if (appDataList.Count() > 0)
            {
                var appData = appDataList.First();

                string deviceToken = appData.DEVICETOKEN;
                //string deviceToken = "f3cc8d81ff9020d5087175447d71ef743a6cda1bf80cf4d55ccf5b9e104c9ba6";

                //string deviceToken = "7cca35814bb2b2f18e1fb4d4ca038030ebe97ca252269a5ce3c70ed2435da928";
                //string deviceToken = "fb9bbb90ebd3d9f8b17e34c3bad7f24bab5956405ca511ba13b86bc1ff3b3077";
                //string deviceToken = "5aa48d8cfd6ee10e0cee080d0fed745cb05a3b7a6001fde4df00631d491c14ad";

                 

                if (deviceToken != null)
                {
                    if (deviceToken == "0" || deviceToken == "")
                    {
                        string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationDeviceTokenIsEmpty);
                        
                        return false;
                    }

                    //dynamic propDataList;
                    IEnumerable<AddressData> propDataList;
                    //IQueryable <dynamic> propDataList;
                    if (isSchemeBlockAppointment)
                    {
                        propDataList = (from app in context.FL_CO_APPOINTMENT
                                        join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                                        join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                                        join schemes in context.P_SCHEME on fltl.SchemeId equals schemes.SCHEMEID into tempScheme
                                        from scheme1 in tempScheme.DefaultIfEmpty()
                                        join blocks in context.P_BLOCK on fltl.BlockId equals blocks.BLOCKID into tempBlock
                                        from blocks1 in tempBlock.DefaultIfEmpty()
                                        where fltAppt.AppointmentId == appointmentId
                                        select new AddressData { HOUSENUMBER = blocks1.BLOCKNAME ?? scheme1.SCHEMENAME, ADDRESS1 = blocks1.ADDRESS1, TOWNCITY = blocks1.TOWNCITY });
                    }
                    else
                    {
                        propDataList = (from property in context.P__PROPERTY
                                        join fltl in context.FL_FAULT_LOG on property.PROPERTYID equals fltl.PROPERTYID
                                        join flta in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals flta.FaultLogId
                                        where flta.AppointmentId == appointmentId &&
                                        flta.FaultLogId == context.FL_FAULT_LOG.Where(ffl => ffl.FaultLogID == flta.FaultLogId).FirstOrDefault().FaultLogID
                                        select new AddressData { HOUSENUMBER = property.HOUSENUMBER, ADDRESS1 = property.ADDRESS1, TOWNCITY = property.TOWNCITY });

                    }
                    if (propDataList.Count() > 0)
                    {
                        string propertyString = string.Empty;

                        AddressData propData = propDataList.First();
                        propertyString = propData.HOUSENUMBER ?? "" + (" " + propData.ADDRESS1) ?? "" + (", " + propData.TOWNCITY) ?? "";

                        DateTime appDate = DateTime.Parse(appData.AppointmentDate.ToString());
                        string appointmentDate = appDate.ToString("d MMM yyyy");

                        DateTime sTime = DateTime.Parse(appData.Time);
                        string startTime = sTime.ToString("HH:mm");

                        DateTime eTime = DateTime.Parse(appData.EndTime);
                        string endTime = eTime.ToString("HH:mm");



                        string PNString = typeOptions[type - 1] + "\n\n" + appointmentDate + " " + startTime + " to " + endTime + "\n" + propertyString;

                        string serverPath = HttpContext.Current.Server.MapPath("");

                     

                        serverPath = serverPath.Remove(serverPath.LastIndexOf('\\') + 1);

                        //This is the dev certificate file name with path.
                        string filePath = serverPath + "APNS_DEV_PSAOfflineCertOnly.p12";

                        //In case the server in use is Test or Live or UAT change the file name accordingly
                        if (serverinUse.Equals(ApplicationConstants.BHG_Test_Name))
                            filePath = serverPath + "APNS_TEST_PSAOfflineCertOnly.p12";
                        else if (serverinUse.Equals(ApplicationConstants.BHG_Live_Name))
                            filePath = serverPath + "APNS_LIVE_PSAOfflineCertOnly.p12";
                        else if (serverinUse.Equals(ApplicationConstants.BHG_UAT_Name))
                            filePath = serverPath + "APNS_UAT_PSAOfflineCertOnly.p12";

                        

                        var push = new PushBroker();

                        bool isNotificationFailed = false;
                        push.OnNotificationFailed += delegate
                        {
                            isNotificationFailed = true;
                        };
                        //-------------------------
                        // APPLE NOTIFICATIONS
                        //-------------------------
                        //Configure and start Apple APNS
                        // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
                        //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
                        //   app with!

                        //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
                        //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
                        //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
                        //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')
                        push.RegisterAppleService(new ApplePushChannelSettings(filePath, "tkxel")); //Extension method
                        //Fluent construction of an iOS notification
                        //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
                        //  for registered for remote notifications is called, and the device token is passed back to you

                        var appleNotification = new AppleNotification()
                                                   .ForDeviceToken(deviceToken)
                                                   .WithAlert(PNString);
                        push.QueueNotification(appleNotification);


                        //Stop and wait for the queues to drains
                        push.StopAllServices();

                        string notificationInfo = String.Format("device token:{0}, notification message: {1}, certificate path:{2} ", deviceToken, PNString, filePath);

                        if (isNotificationFailed)
                        {
                            string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationRejectByApple + notificationInfo);
                           
                            return false;
                        }
                        else
                        {
                            string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationSuccess + notificationInfo);
                            
                            return true;
                        }
                    }
                    else
                    {
                        string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationAppointmentIdDoesNotExistInDb + string.Format("appointment id :{0}", appointmentId));
                        
                        return false;
                    }
                }
                else
                {
                    string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationDeviceTokenIsEmpty);
                    
                    return false;
                }
            }
            else
            {
                string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationAppointmentIdDoesNotExistInDb + string.Format("appointment id :{0}", appointmentId));
                 
                return false;
            }
        }
		class AddressData
        {
            public string HOUSENUMBER { get; set; }
            public string ADDRESS1 { get; set; }
            public string TOWNCITY { get; set; }

        }
        #endregion
#region Send push notification for Document Expiary
        public bool sendPushNotificationDocumentExpiary()
        {
            bool status = false;
            DateTime varStartTime = Convert.ToDateTime(DateTime.Now.AddDays(14).ToShortDateString());
            var appDataList = (from document in context.PDR_DOCUMENTS
                               join login in context.AC_LOGINS on document.CreatedBy equals login.EMPLOYEEID
                               join devDoc in context.P_DEVELOPMENTDOCUMENTS on document.DocumentID equals devDoc.DocumentId  
                               join dev in context.PDR_DEVELOPMENT on devDoc.DevelopmentId equals dev.DEVELOPMENTID  
                               where document.Expires  == varStartTime
                               select new DocumentDetail { Title = document.Title,DevelopmentName=dev.DEVELOPMENTNAME,  Expires = (DateTime)document.Expires, CreatedBy = document.CreatedBy, DeviceToken=login.DEVICETOKEN  }).ToList() ;
            if (appDataList.Count() > 0)
            {
                var pushNotificationDataList = new PushNotificationDataList();
                foreach (var item in appDataList)
                {
                    var pushNotificationData = new PushNotificationData();
                    pushNotificationData.operativeId = (int)item.CreatedBy ;
                    pushNotificationData.deviceToken = item.DeviceToken;                    
                    pushNotificationData.notificationMessage = string.Format(MessageConstants.DocumentExpiaryPushNotificationMessage, item.Title,item.DevelopmentName );
                    pushNotificationDataList.Add(pushNotificationData);
                }

                status = selectCertificateandSendPush(pushNotificationDataList);

            }
            return status;
            
           
        }

        class DocumentDetail
        {
            public string Title { get; set; }
            public string DevelopmentName { get; set; }
            public DateTime  Expires { get; set; }
            public int? CreatedBy { get; set; }
            public string DeviceToken { get; set; }
        }
        #endregion
        #region send Push Notification for Appliances and Planned Appointment(s)
        /// <summary>
        /// This function will send push notification for Appliances and Planned Appointment(s)
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotification(string appointmentMessage, int operatorId, string hostURL)
        {
            string msg = string.Empty, deviceToken = string.Empty, serverPath = string.Empty, filePath = string.Empty;

            try
            {
                //string[] typeOptions = { "New JSN Scheduled", "JSN Cancelled", "JSN Rearranged" };

                if (appointmentMessage == null || appointmentMessage.Trim() == "")
                {
                    msg = FileHelper.messageForLogger(MessageConstants.PushNotificationMessageIsEmpty);
                    
                    return false;
                }

                var operatorDataList = (from login in context.AC_LOGINS
                                        where login.EMPLOYEEID == operatorId
                                        select new { login.DEVICETOKEN });


                if (operatorDataList.Count() <= 0)
                {
                    msg = FileHelper.messageForLogger(MessageConstants.PushNotificationOperatorIdIsEmpty);
                    
                    return false;
                }
                var operatorData = operatorDataList.First();

                deviceToken = operatorData.DEVICETOKEN;
                //deviceToken = "5aa48d8cfd6ee10e0cee080d0fed745cb05a3b7a6001fde4df00631d491c14ad";

                if (deviceToken == null || deviceToken == "0" || deviceToken == "")
                {
                    msg = FileHelper.messageForLogger(MessageConstants.PushNotificationDeviceTokenIsEmpty);
                    
                    return false;
                }

                

                //var propData = propDataList.First();

                //DateTime appDate = DateTime.Parse(appData.AppointmentDate.ToString());
                //string appointmentDate = appDate.ToString("d MMM yyyy");

                //DateTime sTime = DateTime.Parse(appData.Time);
                //string startTime = sTime.ToString("HH:mm");

                //DateTime eTime = DateTime.Parse(appData.EndTime);
                //string endTime = eTime.ToString("HH:mm");

                //string propertyString = propData.HOUSENUMBER.ToString() + " " + propData.ADDRESS1.ToString() + ", " + propData.TOWNCITY.ToString();

                //string PNString = typeOptions[type - 1] + "\n\n" + appointmentDate + " " + startTime + " to " + endTime + "\n" + propertyString;

                serverPath = HttpContext.Current.Server.MapPath("");

                 

                serverPath = serverPath.Remove(serverPath.LastIndexOf('\\') + 1);

                string password;
                //This is the dev certificate file name with path.
                filePath = serverPath + "APNS_DEV_PSAOfflineCertOnly.p12";
                password = "tkxel";
                //filePath = serverPath + "CertificatesAPNS_com_bhg_psaofflinedev.p12";
                //password = "123";
                ////In case the server in use is Test or Live or UAT change the file name accordingly
                if (hostURL.StartsWith(ApplicationConstants.BHG_Test_Name))
                {
                    filePath = serverPath + "APNS_TEST_PSAOfflineCertOnly.p12";
                    password = "tkxel";
                }
                else if (hostURL.StartsWith(ApplicationConstants.BHG_Live_Name))
                {
                    filePath = serverPath + "APNS_LIVE_PSAOfflineCertOnly.p12";
                    password = "tkxel";
                }
                else if (hostURL.StartsWith(ApplicationConstants.BHG_UAT_Name))
                {
                    filePath = serverPath + "APNS_UAT_PSAOfflineCertOnly.p12";
                    password = "tkxel";
                }

                

                var push = new PushBroker();

                bool isNotificationFailed = false;
                push.OnNotificationFailed += delegate
                {
                    isNotificationFailed = true;
                };
                //-------------------------
                // APPLE NOTIFICATIONS
                //-------------------------
                //Configure and start Apple APNS
                // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
                //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
                //   app with!

                //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
                //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
                //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
                //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')
                //push.RegisterAppleService(new ApplePushChannelSettings(filePath, "tkxel")); //Extension method
                push.RegisterAppleService(new ApplePushChannelSettings(filePath, password)); //Extension method

                //Fluent construction of an iOS notification
                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
                //  for registered for remote notifications is called, and the device token is passed back to you

                var appleNotification = new AppleNotification()
                                           .ForDeviceToken(deviceToken)
                                           .WithAlert(appointmentMessage);
                push.QueueNotification(appleNotification);

                //Stop and wait for the queues to drains
                push.StopAllServices();

                string notificationInfo = String.Format("device token:{0}, notification notificationMessage: {1}, certificate path:{2} ", deviceToken, appointmentMessage, filePath);

                if (isNotificationFailed)
                {
                    msg = FileHelper.messageForLogger(MessageConstants.PushNotificationRejectByApple + notificationInfo);
                   
                    return false;
                }

                msg = FileHelper.messageForLogger(MessageConstants.PushNotificationSuccess + notificationInfo);
                
                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }
        #endregion

        #region Send Push Notification to Operative having incomplete appointments

        /// <summary>
        /// Send push notification to operatives having incomplete appointment after due completion time.
        /// To filter the operative/appointments time is save as a key/value pair in database and may be changed any time.
        /// </summary>
        /// <returns></returns>
        public bool sendOverduePushNotification()
        {
            bool status = false;

            var results = context.PSA_GetOperativesWithOverdueAppointment();

            List<Entities.OperativeIdAndDeviceToken> operativeWithOverdueAppointments = new List<Entities.OperativeIdAndDeviceToken>();

            operativeWithOverdueAppointments.AddRange(results);

            var pushNotificationDataList = new PushNotificationDataList();

            foreach (var operative in operativeWithOverdueAppointments)
            {
                var pushNotificationData = new PushNotificationData();
                pushNotificationData.operativeId = (int)operative.OPERATIVEID;
                pushNotificationData.deviceToken = operative.DEVICETOKEN;
                pushNotificationData.notificationMessage = MessageConstants.OverDuePushNotificationMessage;
                pushNotificationDataList.Add(pushNotificationData);
            }

            status = selectCertificateandSendPush(pushNotificationDataList);

            return status;
        }

        #endregion

        #region select certificate and send push notification

        private bool selectCertificateandSendPush(PushNotificationDataList notificationDataList, string hostURL = "")
        {
            bool status = false;

            string msg = string.Empty
                , serverPath = string.Empty
                , filePath = string.Empty;

            if (hostURL == string.Empty)
                hostURL = HttpContext.Current.Request.Url.Host.ToString();

             

            serverPath = HttpContext.Current.Server.MapPath("");

             

            serverPath = serverPath.Remove(serverPath.LastIndexOf('\\') + 1);

            string password;
            //This is the dev certificate file name with path.
            filePath = serverPath + "APNS_DEV_PSAOfflineCertOnly.p12";
            password = "tkxel";
            //filePath = serverPath + "CertificatesAPNS_com_bhg_psaofflinedev.p12";
            //password = "123";
            ////In case the server in use is Test or Live or UAT change the file name accordingly
            if (hostURL.StartsWith(ApplicationConstants.BHG_Test_Name))
            {
                filePath = serverPath + "APNS_TEST_PSAOfflineCertOnly.p12";
                password = "tkxel";
            }
            else if (hostURL.StartsWith(ApplicationConstants.BHG_Live_Name))
            {
                filePath = serverPath + "APNS_LIVE_PSAOfflineCertOnly.p12";
                password = "tkxel";
            }
            else if (hostURL.StartsWith(ApplicationConstants.BHG_UAT_Name))
            {
                filePath = serverPath + "APNS_UAT_PSAOfflineCertOnly.p12";
                password = "tkxel";
            }

             
            int listCount = notificationDataList.Count;
            int channelCount = listCount * 10 / 15;

            var push = new PushBroker();

            var pushServiceSettings = new PushServiceSettings();
            pushServiceSettings.Channels = (channelCount > 10) ? 10 : channelCount;
            pushServiceSettings.NotificationSendTimeout = 600000;
            pushServiceSettings.MaxNotificationRequeues = listCount;
                        
            push.OnNotificationFailed += notificationFailed;
            //-------------------------
            // APPLE NOTIFICATIONS
            //-------------------------
            //Configure and start Apple APNS
            // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
            //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
            //   app with!

            //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
            //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
            //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
            //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')
            //push.RegisterAppleService(new ApplePushChannelSettings(filePath, "tkxel")); //Extension method
            push.RegisterAppleService(new ApplePushChannelSettings(filePath, password), pushServiceSettings); //Extension method

            //Fluent construction of an iOS notification
            //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
            //  for registered for remote notifications is called, and the device token is passed back to you

            foreach (var notificationObject in notificationDataList)
            {
                var appleNotification = push.AppleNotification()
                           .ForDeviceToken(notificationObject.deviceToken)
                           .WithAlert(notificationObject.notificationMessage);

                push.QueueNotification(appleNotification);
            }

            //Stop and wait for the queues to drains
            push.StopAllServices();

            var deviceToken = string.Empty;
            var appointmentMessage = string.Empty;
            string notificationInfo = String.Format("device token:{0}, notification notificationMessage: {1}, certificate path:{2} ", deviceToken, appointmentMessage, filePath);

            msg = FileHelper.messageForLogger(MessageConstants.PushNotificationSuccess + notificationInfo);
            
            status = true;

            return status;
        }

        #endregion

        #region Send push notification test function
        /// <summary>
        /// Just to test push notification to an iOS device
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <param name="certificateFilepath"></param>
        /// <param name="useSandBox"></param>
        /// <param name="pushMessage"></param>
        /// <param name="certificatePassword">Set an optional push notification password.</param>
        /// <returns></returns>
        public string sendPushNotification(string deviceToken, string certificateFilepath, bool useSandBox, string pushMessage, string certificatePassword)
        {
            string notificatonStatus = "Success";

            var push = new PushBroker();
            //-------------------------
            // APPLE NOTIFICATIONS
            //-------------------------
            //Configure and start Apple APNS
            // IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
            //   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
            //   app with!

            //IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
            //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
            //  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
            //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')
            push.RegisterAppleService(new ApplePushChannelSettings(certificateFilepath, certificatePassword)); //Extension method
            //Fluent construction of an iOS notification
            //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
            //  for registered for remote notifications is called, and the device token is passed back to you
            push.QueueNotification(new AppleNotification()
                                       .ForDeviceToken(deviceToken)
                                       .WithAlert(pushMessage));

            //Stop and wait for the queues to drains
            push.StopAllServices();

            return notificatonStatus;
        }
        #endregion

        #region Log Message on push notification failure

        static void notificationFailed(object sender, INotification notification, Exception e)
        {
            AppleNotification appleNotification = (AppleNotification)notification;
            bool IsValidDeviceRegistrationId = appleNotification.IsValidDeviceRegistrationId();

            string message = String.Format("Over Due Push Notification Failed for {0} Notification Info: Device Token = {1}, EnqueuedTimeStamp: {2}, QueuedCount: {3} {0} Exception: {4}"
                , Environment.NewLine
                , appleNotification.DeviceToken, appleNotification.EnqueuedTimestamp, appleNotification.QueuedCount
                , e.ToString());
             
        }

        #endregion
    }
}

