﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Utilities.Constants;
using PushSharp.Apple;
using PushSharp;
using PushSharp.Core;
using System.Configuration;


namespace PropSurvey.Dal.Push
{
    internal sealed class pushServiceLogger 
    {
        #region Singlton Implementation
        private static volatile pushServiceLogger instance;
        private static object syncRoot = new Object();
        public static pushServiceLogger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new pushServiceLogger();
                    }
                }

                return instance;
            }
        }
        #endregion

        #region Logging Level Static Member

        public static readonly LogLevel Level = (LogLevel)int.Parse(ConfigurationManager.AppSettings.Get("PushServiceLogLevel"));

        #endregion

        #region Constructor(s)
        private pushServiceLogger() { }
        #endregion
        
        
    }
}
