﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;

namespace PropSurvey.Dal.Property
{
    public class PropertyAgentDal:BaseDal
    {
        #region get All Property Agents
        /// <summary>
        /// This function returns all the property agent list
        /// </summary>
        /// <returns>It retruns the list of property agent's object</returns>
        public List<PropertyAgentData> getAllPropertyAgent()
        {
            List<PropertyAgentData> agentData = new List<PropertyAgentData>();

            agentData = (from proAgent in context.PS_PropertyAgent
                        select new PropertyAgentData 
                        {
                            ID = proAgent.ID,
                            AgentName = proAgent.LandlordName,
                            Address = proAgent.Address,
                            PhoneNo = proAgent.PhoneNo
                        }).ToList();

            return agentData;
        }
        #endregion

        #region Update a Property Agent
        /// <summary>
        /// This function updates a property agent
        /// </summary>
        /// <param name="proAgentData">Property Agent Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdatePropertyAgent(PropertyAgentData proAgentData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    PS_PropertyAgent proAgent = new PS_PropertyAgent();
                    var property = context.PS_PropertyAgent.Where(agent => agent.ID == proAgentData.ID);

                    if (property.Count() > 0)
                    {
                        proAgent = property.First();
                    }
                    else
                    {
                        proAgent.ID = proAgentData.ID;
                    }

                    proAgent.LandlordName = proAgentData.AgentName;
                    proAgent.Address = proAgentData.Address;
                    proAgent.PhoneNo = proAgentData.PhoneNo;

                    if (property.Count() <= 0)
                    {
                        context.AddToPS_PropertyAgent(proAgent);
                    }

                    context.SaveChanges();

                    trans.Complete();
                    success = true;
                }
            }

            return success;
        }
        #endregion

        #region save a Property Agent
        /// <summary>
        /// This function saves a property agent
        /// </summary>
        /// <param name="proAgentData">Property Agent Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool SavePropertyAgent(PropertyAgentData proAgentData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    PS_PropertyAgent proAgent = new PS_PropertyAgent();

                    proAgent.LandlordName = proAgentData.AgentName;
                    proAgent.Address = proAgentData.Address;
                    proAgent.PhoneNo = proAgentData.PhoneNo;

                    context.AddToPS_PropertyAgent(proAgent);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    trans.Complete();
                    success = true;
                }
            }

            return success;
        }
        #endregion

        #region get Property Agent to Appointment
        /// <summary>
        /// This function returns Property Agent to Appointment
        /// </summary>
        /// <returns>It retruns Property Agent to Appointment's object</returns>
        public PropertyAgentToAppointmentData getAgentToAppointment(int AppointmentID)
        {
            PropertyAgentToAppointmentData agentData = new PropertyAgentToAppointmentData();

            agentData = (from proAgent in context.GS_PropertyAgent_to_Appointment
                         where proAgent.AppointmentID == AppointmentID
                         select new PropertyAgentToAppointmentData
                         {
                             ID = proAgent.ID,
                             AppointmentID = proAgent.AppointmentID,
                             PropertyAgentID = proAgent.PropertyAgentID,
                             Date = proAgent.Date
                         }).FirstOrDefault();

            return agentData;
        }
        #endregion

        #region get Property Agent details
        /// <summary>
        /// This function returns Property Agent details
        /// </summary>
        /// <returns>It retruns Property Agent details</returns>
        public PropertyAgentData getAgentDetails(int AppointmentID)
        {
            PropertyAgentData agentData = new PropertyAgentData();

            agentData = (from proAgent in context.GS_PropertyAgent_to_Appointment
                         join agent in context.PS_PropertyAgent on proAgent.PropertyAgentID equals agent.ID
                         where proAgent.AppointmentID == AppointmentID
                         select new PropertyAgentData
                         {
                             ID = agent.ID,
                             Address = agent.Address,
                             AgentName = agent.LandlordName,
                             PhoneNo = agent.PhoneNo
                         }).FirstOrDefault();

            return agentData;
        }
        #endregion

        #region Update a Property Agent to Appointment
        /// <summary>
        /// This function updates a Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdateAgentToAppointment(PropertyAgentToAppointmentData proAgentData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    GS_PropertyAgent_to_Appointment proAgent = new GS_PropertyAgent_to_Appointment();
                    var property = context.GS_PropertyAgent_to_Appointment.Where(agent => agent.ID == proAgentData.ID);

                    if (property.Count() > 0)
                    {
                        proAgent = property.First();
                    }
                    else
                    {
                        proAgent.ID = proAgentData.ID;
                    }

                    proAgent.AppointmentID = proAgentData.AppointmentID;
                    proAgent.PropertyAgentID = proAgentData.PropertyAgentID;
                    proAgent.Date = DateTime.Now;

                    if (property.Count() <= 0)
                    {
                        context.AddToGS_PropertyAgent_to_Appointment(proAgent);
                    }

                    context.SaveChanges();

                    trans.Complete();
                    success = true;
                }
            }

            return success;
        }
        #endregion

        #region save a Property Agent to Appointment
        /// <summary>
        /// This function saves  Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public int SaveAgentToAppointment(PropertyAgentToAppointmentData proAgentData)
        {
            bool success = false;
            GS_PropertyAgent_to_Appointment proAgent = new GS_PropertyAgent_to_Appointment();
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    proAgent.PropertyAgentID = proAgentData.PropertyAgentID;
                    proAgent.AppointmentID = proAgentData.AppointmentID;
                    proAgent.Date = DateTime.Now;

                    context.AddToGS_PropertyAgent_to_Appointment(proAgent);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    trans.Complete();
                    success = true;
                }
            }

            if (success)
            {
                return proAgent.ID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app => app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkPropertyAgenID(int PropertyAgentID)
        {
            var id = context.PS_PropertyAgent.Where(app => app.ID == PropertyAgentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
