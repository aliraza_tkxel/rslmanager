﻿using System;
using System.Collections.Generic;
using System.Linq;
using PropSurvey.Contracts.Data;
using PropSurvey.Entities;

namespace PropSurvey.Dal
{    
    public class PowerTypeDal : Base.BaseDal
    {

        public List<PowerType> getPowerSourceTypes()
        {
            List<PowerType> powerTypes = new List<PowerType>();

            var powerTypesQuery = from pst in context.P_PowerSourceType
                                        orderby pst.sortOrder ascending
                                        select new  PowerType
                                        { 
                                            powerTypeId = pst.PowerTypeId,
                                            powerType = pst.PowerType
                                        };
            
            if (powerTypesQuery.Count() > 0) powerTypes = powerTypesQuery.ToList(); 

            return powerTypes;
        }

    }
}
