﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Dal.Customer;
using PropSurvey.Dal.Faults;
using PropSurvey.Dal.IssuedReceivedBy;
using PropSurvey.Dal.Property;
using PropSurvey.Entities;
using PropSurvey.Contracts.Data.Appointment;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using System.Text;
using System.Data;
using PropSurvey.Dal.Survey;
using PropSurvey.Dal.Appliances;
using PropSurvey.Dal.InstallationPipework;
using System.Web;
using PropSurvey.Contracts.Fault;
using System.Data.Common;
using PropSurvey.Contracts.Data.Void;
using PropSurvey.Contracts.Data.StockAPIParameters;
using System.Globalization;
using System.Text.RegularExpressions;
using PropSurvey.Utilities.Validate;

namespace PropSurvey.Dal.Appointment
{
    public class AppointmentDal : BaseDal
    {
        #region get All Fault Appointments Data

        /// <summary>
        /// This function returns all fault appointments by user
        /// </summary>
        /// <param name="operativeID">operative id</param>
        /// <returns>List of Jobs data objects</returns>
        public List<AllAppointmentsList> getAllFaultAppointmentsByUser(string userName, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            #region   get updated appointment from DB
            var _var = (from app in context.FL_CO_APPOINTMENT
                        join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                        join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                        join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                        join ten in context.C_TENANCY on fltl.PROPERTYID equals ten.PROPERTYID into tenancy
                        from tenan in tenancy.Where(tenant => tenant.ENDDATE == null).DefaultIfEmpty()
                        join pro in context.P__PROPERTY on fltl.PROPERTYID equals pro.PROPERTYID
                        join creat in context.AC_LOGINS on fltl.UserId equals creat.EMPLOYEEID
                        join login in context.AC_LOGINS on app.OperativeID equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                        join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                        join noentry in context.FL_FAULT_NOENTRY on fltl.FaultLogID equals noentry.FaultLogId into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        orderby app.AppointmentID
                        where tenan.ENDDATE == null
                           && app.AppointmentDate >= varStartTime && app.AppointmentDate <= varEndTime
                                //&& cad.ISDEFAULT == 1
                                && login.LOGIN == userName
                                && fltAppt.FaultAppointmentId == (context.FL_FAULT_APPOINTMENT.Where(a => a.AppointmentId == app.AppointmentID).Min(a => a.FaultAppointmentId))
                                && flts.Description != "Cancelled"
                                && !(completedStatusesList.Contains(app.AppointmentStatus.Replace(" ", ""))) // Filter out completed appointments.
                        select new AllAppointmentsList
                        {
                            appointmentId = app.AppointmentID,
                            appointmentNotes = app.Notes,
                            appointmentDate = app.AppointmentDate,
                            appointmentEndDate = app.AppointmentEndDate ?? app.AppointmentDate,
                            appointmentStatus = app.AppointmentStatus,
                            appointmentType = "Fault",
                            surveyorUserName = login.LOGIN,
                            tenancyId = tenan.TENANCYID,
                            appointmentStartTimeString = app.Time,
                            appointmentEndTimeString = app.EndTime,
                            createdBy = fltl.UserId,
                            creationDate = app.LastActionDate,
                            createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                            property = new PropertyData
                            {
                                propertyId = pro.PROPERTYID,
                                tenancyId = tenan.TENANCYID,
                                houseNumber = pro.HOUSENUMBER,
                                flatNumber = pro.FLATNUMBER,
                                address1 = pro.ADDRESS1,
                                address2 = pro.ADDRESS2,
                                address3 = pro.ADDRESS3,
                                townCity = pro.TOWNCITY,
                                postCode = pro.POSTCODE,
                                county = pro.COUNTY,
                                defaultPropertyPicId = pro.PropertyPicId
                            }
                        });
            #endregion
            List<AllAppointmentsList> faultAppointments = new List<AllAppointmentsList>();

            if (_var.Count() > 0)
            {
                faultAppointments = _var.ToList();


                foreach (AllAppointmentsList item in faultAppointments)
                {

                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(((DateTime)item.appointmentDate).ToString("dd/MM/yyyy") + " " + item.appointmentStartTimeString);
                    item.appointmentEndDateTime = DateTime.Parse(((DateTime)item.appointmentEndDate).ToString("dd/MM/yyyy") + " " + item.appointmentEndTimeString);
                    if (item.appointmentStatus == "Appointment Arranged")
                        item.appointmentStatus = "NotStarted";
                    setAdditionalDataFault(item);
                    item.scheme = null;
                }
            }


            return faultAppointments;
        }

        #endregion

        #region Get Job Status Name by StatusId

        /// <summary>
        /// Get Job Status Name by StatusId
        /// </summary>
        /// <param name="statusID"></param>
        /// <returns></returns>
        public string GetJobStatusString(int statusID)
        {
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.FaultStatusID == statusID);
            if (fltStatusList.Count() == 0)
                return "";

            FL_FAULT_STATUS fltStatus = fltStatusList.First();

            return fltStatus.Description;
        }

        #endregion

        #region Get Job Status Name by StatusId

        /// <summary>
        /// Get Job Status Id By Status Name
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public int GetJobStatusID(string status)
        {
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description == status);
            if (fltStatusList.Count() == 0)
                return 0;

            FL_FAULT_STATUS fltStatus = fltStatusList.First();

            return fltStatus.FaultStatusID;
        }

        #endregion

        #region get All Stock Appointments

        /// <summary>
        /// This function returns all Stock appointments from particular start date
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getAllStockAppointments(string userName, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            // Converting Date received from iOS
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndDate = DateTime.Parse(endDate);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.CreatedDate descending
                                where tenan.ENDDATE == null
                                && app.SurveyourUserName == userName
                                && app.AppointStartDateTime >= varStartTime && app.AppointStartDateTime <= varEndDate
                                && !(completedStatusesList.Contains(app.AppointProgStatus))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,
                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = false,
                                    appointmentDate = app.AppointStartDateTime,
                                    createdBy = app.CreatedBy,
                                    addToCalendar = app.addToCalendar,
                                    creationDate = app.CreatedDate,
                                    loggedDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    ////Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }
                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get All Planned Appointments Data

        /// <summary>
        /// This function returns all fault appointments by user
        /// </summary>
        /// <param name="operativeID">operative id</param>
        /// <returns>List of Jobs data objects</returns>
        public List<AllAppointmentsList> getAllPlannedAppointments(string userName, string startDate, string endDate, List<ExistingAppointmentParam> existingAppointments, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            #region get updated appointment from DB
            var _var = (from jor in context.PLANNED_JOURNAL
                        join app in context.PLANNED_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                        join status in context.PLANNED_STATUS on jor.STATUSID equals status.STATUSID
                        // join pcomp in context.PLANNED_COMPONENT on jor.COMPONENTID equals pcomp.COMPONENTID
                        join pcomp in context.PLANNED_COMPONENT on jor.COMPONENTID equals pcomp.COMPONENTID into compo
                        from componentdetails in compo.DefaultIfEmpty()

                        join pro in context.P__PROPERTY on jor.PROPERTYID equals pro.PROPERTYID into tempProp
                        from fltProp in tempProp.DefaultIfEmpty()
                        join schemes in context.P_SCHEME on jor.SchemeId equals schemes.SCHEMEID into tempScheme
                        from fltScheme in tempScheme.DefaultIfEmpty()
                        join blocks in context.P_BLOCK on jor.BlockId equals blocks.BLOCKID into tempBlock
                        from fltBlock in tempBlock.DefaultIfEmpty()

                            //join ten in context.C_TENANCY on app.TENANCYID equals ten.TENANCYID //Changed from inner join to left join.
                        join tenant in context.C_TENANCY on app.TENANCYID equals tenant.TENANCYID into tenancy
                        from ten in tenancy.DefaultIfEmpty()
                            //join comTrade in context.PLANNED_COMPONENT_TRADE on app.COMPTRADEID equals comTrade.COMPTRADEID
                        join comTrade in context.PLANNED_COMPONENT_TRADE on app.COMPTRADEID equals comTrade.COMPTRADEID into compoTrade
                        from compoTradeDetails in compoTrade.DefaultIfEmpty()
                            //join trade in context.G_TRADE on comTrade.TRADEID equals trade.TradeId
                        join trade in context.G_TRADE on compoTradeDetails.TRADEID equals trade.TradeId into Trades
                        from tradeDetails in Trades.DefaultIfEmpty()
                        join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                        join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                        join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                        join noentry in context.PLANNED_NOENTRY on app.APPOINTMENTID equals noentry.APPOINTMENTID into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        join jorsubstatus in context.PLANNED_SUBSTATUS on app.JOURNALSUBSTATUS equals jorsubstatus.SUBSTATUSID into tempSubStatus
                        from subStatus in tempSubStatus.DefaultIfEmpty()

                        join appointType in context.Planned_Appointment_Type on app.Planned_Appointment_TypeId equals appointType.Planned_Appointment_TypeId into appType
                        from appointmentType in appType.DefaultIfEmpty()
                        orderby app.APPOINTMENTID descending
                        where app.APPOINTMENTDATE >= varStartTime && app.APPOINTMENTDATE <= varEndTime
                                && login.LOGIN == userName
                                && app.APPOINTMENTSTATUS != "Canceled"
                               && (status.TITLE == "Arranged" || status.TITLE == "Condition Arranged" || status.TITLE == "In Progress")
                               && !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.

                        select new AllAppointmentsList
                        {
                            appointmentId = app.APPOINTMENTID,
                            appointmentNotes = app.CUSTOMERNOTES,
                            appointmentDate = app.APPOINTMENTDATE,
                            appointmentEndDate = app.APPOINTMENTENDDATE,
                            appointmentStatus = app.APPOINTMENTSTATUS,
                            appointmentType = (appointmentType.Planned_Appointment_Type1 == null ? "Planned" : appointmentType.Planned_Appointment_Type1),
                            surveyorUserName = login.LOGIN,
                            tenancyId = ten.TENANCYID,
                            appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                            appointmentEndTimeString = app.APPOINTMENTENDTIME,
                            createdBy = app.CREATEDBY,
                            journalHistoryId = app.JOURNALHISTORYID,
                            journalId = app.JournalId,
                            surveyType = app.SURVEYTYPE,
                            surveyourAvailability = app.SURVEYOURSTATUS,
                            journalSubStatus = status.TITLE,
                            creationDate = app.LOGGEDDATE,
                            loggedDate = app.LOGGEDDATE,
                            isMiscAppointment = app.isMiscAppointment,
                            createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                            property = new PropertyData
                            {
                                propertyId = fltProp.PROPERTYID,
                                tenancyId = ten != null ? (int?)ten.TENANCYID : null,
                                houseNumber = fltProp.HOUSENUMBER,
                                flatNumber = fltProp.FLATNUMBER,
                                address1 = fltProp.ADDRESS1,
                                address2 = fltProp.ADDRESS2,
                                address3 = fltProp.ADDRESS3,
                                townCity = fltProp.TOWNCITY,
                                postCode = fltProp.POSTCODE,
                                county = fltProp.COUNTY,
                                defaultPropertyPicId = fltProp.PropertyPicId
                            },
                            scheme = new SchemeBlockData
                            {
                                schemeId = fltScheme != null ? (int?)fltScheme.SCHEMEID : null,
                                blockId = fltBlock != null ? (int?)fltBlock.BLOCKID : null,
                                schemeName = fltScheme.SCHEMENAME,
                                blockName = fltBlock.BLOCKNAME,
                                address1 = fltBlock.ADDRESS1,
                                address2 = fltBlock.ADDRESS2,
                                address3 = fltBlock.ADDRESS3,
                                towncity = fltBlock.TOWNCITY,
                                postcode = fltBlock.POSTCODE,
                                county = fltBlock.COUNTY,
                            },
                            componentTrade = new PlannedComponentTrade
                            {
                                componentTradeId = compoTradeDetails.COMPTRADEID == null ? -1 : compoTradeDetails.COMPTRADEID,
                                componentId = compoTradeDetails.COMPONENTID,
                                componentName = componentdetails.COMPONENTNAME,
                                trade = tradeDetails.Description,
                                duration = app.DURATION,
                                tradeId = compoTradeDetails.TRADEID,
                                sorder = compoTradeDetails.sorder == null ? 0 : compoTradeDetails.sorder,
                                completionDate = app.APPOINTMENTENDDATE,
                                JSNNotes = app.APPOINTMENTNOTES,
                                reportedDate = app.LOGGEDDATE,
                                jobStatus = subStatus.TITLE
                            }
                        });
            #endregion
            List<AllAppointmentsList> plannedAppointments = new List<AllAppointmentsList>();
            List<ExistingAppointmentParam> appAppointmentList = existingAppointments.OfType<ExistingAppointmentParam>().Where(s => s.appointmentType == "Planned" || s.appointmentType == "Miscellaneous" || s.appointmentType == "Adaptation" || s.appointmentType == "Condition").ToList();
            List<int> appAptIds = appAppointmentList.Select(app => app.appointmentId).ToList();
            List<AllAppointmentsList> plannedAppointmentList = new List<AllAppointmentsList>();
            List<int> deletedIds = new List<int>();
            List<ExistingAppointmentParam> deleteAppointmentList = new List<ExistingAppointmentParam>();
            if (_var.Count() > 0)
            {
                plannedAppointments = _var.ToList();
                List<int> dbAptIds = plannedAppointments.Select(app => app.appointmentId).ToList();
                deletedIds = appAptIds.Where(x => !dbAptIds.Contains(x)).ToList();
                deleteAppointmentList = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => deletedIds.Contains(s.appointmentId)).ToList();

                foreach (AllAppointmentsList item in plannedAppointments)
                {

                    if (item.property.propertyId == null)
                    {
                        item.property = null;
                    }
                    else
                    {
                        item.scheme = null;
                    }

                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(dt.ToString("dd/MM/yyyy") + " " + item.appointmentStartTimeString);
                    DateTime enddt = (DateTime)item.appointmentEndDate;
                    item.appointmentEndDateTime = DateTime.Parse(enddt.ToString("dd/MM/yyyy") + " " + item.appointmentEndTimeString);
                    item.componentTrade.PMO = "PMO" + item.journalId.ToString();
                    item.componentTrade.JSNumber = "JSN" + item.appointmentId;
                    setAdditionalDataPlanned(item);
                    if (item.isMiscAppointment == true || item.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Condition.ToString())
                    {
                        if (item.appointmentType == "Misc")
                        {
                            item.appointmentType = "Miscellaneous";
                            item.componentTrade.PMO = "MWO" + item.journalId.ToString();
                        }
                        if (item.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Condition.ToString())
                        {
                            var itemdetail = from cw in context.PLANNED_CONDITIONWORKS.Where(cw => cw.JournalId == item.journalId)
                                             join att in context.PA_PROPERTY_ATTRIBUTES on cw.AttributeId equals att.ATTRIBUTEID
                                             join ip in context.PA_ITEM_PARAMETER on att.ITEMPARAMID equals ip.ItemParamID
                                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                                             select i;

                            //Set to default values
                            item.componentTrade.componentId = -1;
                            item.componentTrade.componentName = "N/A";

                            if (itemdetail.Count() > 0)
                            {
                                item.componentTrade.componentId = -1;
                                item.componentTrade.componentName = itemdetail.First().ItemName;
                            }
                        }
                        PlannedComponentMiscTrade miscTrade = this.getMiscAppointmentTrade(item.appointmentId);
                        item.componentTrade.tradeId = miscTrade.tradeId;
                        item.componentTrade.trade = miscTrade.trade;
                        item.componentTrade.duration = miscTrade.duration;
                        item.componentTrade.location = miscTrade.location;
                        item.componentTrade.adaptation = miscTrade.adaptation;
                        item.componentTrade.adaptationId = miscTrade.adaptationId;
                        item.componentTrade.locationId = miscTrade.locationId;
                    }
                    item.componentTrade.durationUnit = item.componentTrade.duration.ToString() + " Hour(s)";

                    //if appointment status=NotStarted and journal status =Arranged 
                    if ((item.journalSubStatus == "Arranged" || item.journalSubStatus == "Condition Arranged") && item.appointmentStatus == "NotStarted")
                    {
                        item.appointmentStatus = "NotStarted";
                        item.componentTrade.jobStatus = "NotStarted";
                    }
                    //PLANNED_APPOINTMENTS.JournalSubstatus in ('In Progress')
                    //PLANNED_APPOINTMENTS.AppointmentStatus = ‘InProgress’
                    else if (item.appointmentStatus == "In Progress" && item.componentTrade.jobStatus == "In Progress")
                    {
                        item.appointmentStatus = AppointmentCompleteStatus.InProgress.ToString();
                        int jobStatus = isPlannedJobStarted(item.appointmentId);
                        if (jobStatus == 0)
                            item.componentTrade.jobStatus = "NotStarted";
                        else
                            item.componentTrade.jobStatus = AppointmentCompleteStatus.InProgress.ToString();
                    }
                    //PLANNED_APPOINTMENTS.JournalSubstatus in (‘In Progress’,‘Paused’)
                    //PLANNED_APPOINTMENTS.AppointmentStatus = ‘InProgress’
                    else if ((item.componentTrade.jobStatus == "In Progress".ToString() || item.componentTrade.jobStatus == AppointmentCompleteStatus.Paused.ToString()) && item.appointmentStatus == AppointmentCompleteStatus.InProgress.ToString())
                    {
                        item.appointmentStatus = AppointmentCompleteStatus.InProgress.ToString();
                        item.componentTrade.jobStatus = item.componentTrade.jobStatus.Replace(" ", "");
                    }
                    else if ((item.componentTrade.jobStatus == AppointmentCompleteStatus.Completed.ToString() || item.componentTrade.jobStatus == "No Entry") && item.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                    {
                        item.appointmentStatus = AppointmentCompleteStatus.Complete.ToString();
                        item.componentTrade.jobStatus = AppointmentCompleteStatus.Complete.ToString();
                    }

                    var extAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == item.appointmentId && s.creationDate != item.creationDate);
                    var emptyAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == item.appointmentId);
                    if (emptyAppApt.Count() <= 0 || extAppApt.Count() > 0)
                    {
                        if (extAppApt.Count() > 0)
                            deleteAppointmentList.Add(extAppApt.FirstOrDefault());
                        plannedAppointmentList.Add(item);
                    }
                }
            }
            else
            {

                deleteAppointmentList = appAppointmentList;

            }
            foreach (ExistingAppointmentParam exApptList in deleteAppointmentList)
            {
                DeleteAppointmentParam delApp = new DeleteAppointmentParam();
                delApp.appointmentId = exApptList.appointmentId;
                delApp.appointmentType = exApptList.appointmentType;
                deleteAppointment.Add(delApp);
            }

            return plannedAppointmentList;
        }

        #endregion

        #region Get Misc Appointment Trade by appointmentId

        /// <summary>
        /// Get Misc Appointment Trade by appointmentId
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        public PlannedComponentMiscTrade getMiscAppointmentTrade(int appointmentId)
        {
            var _var = (from jor in context.PLANNED_JOURNAL
                        join app in context.PLANNED_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                        join trade in context.PLANNED_MISC_TRADE on app.APPOINTMENTID equals trade.AppointmentId
                        join tradeDetails in context.G_TRADE on trade.TradeId equals tradeDetails.TradeId
                        where trade.AppointmentId == appointmentId
                        select new PlannedComponentMiscTrade
                        {
                            tradeId = trade.TradeId,
                            trade = tradeDetails.Description,
                            duration = trade.Duration,
                            locationId = trade.ParameterId,
                            adaptationId = trade.ParameterValueId
                        });
            PlannedComponentMiscTrade miscTrade = new PlannedComponentMiscTrade();

            if (_var.Count() > 0)
            {
                miscTrade = _var.First();
                if (miscTrade.locationId != null && miscTrade.locationId > 0)
                {
                    List<PLANNED_GetLocations_Result> locationList = new List<PLANNED_GetLocations_Result>();
                    locationList = this.GetLocation();
                    DataTable locationDt = new DataTable();
                    DataTable filteredLocationDt = new DataTable();
                    locationDt = FileHelper.ConvertToDataTable(locationList);

                    DataView locationDv = new DataView();
                    locationDv = locationDt.AsDataView();
                    locationDv.RowFilter = "ItemID =  " + miscTrade.locationId.ToString();
                    filteredLocationDt = locationDv.ToTable();
                    miscTrade.location = filteredLocationDt.Rows[0]["ItemName"].ToString();
                    if (miscTrade.adaptationId > 0)
                    {
                        var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                          where parv.ValueID == miscTrade.adaptationId
                                          select parv.ValueDetail);
                        if (paramValue.Count() > 0)
                        {
                            miscTrade.adaptation = paramValue.Single().ToString();
                        }
                    }
                }
            }
            return miscTrade;
        }

        #endregion

        #region Get Planned Locations

        public List<PLANNED_GetLocations_Result> GetLocation()
        {
            try
            {
                List<PLANNED_GetLocations_Result> list = new List<PLANNED_GetLocations_Result>();
                list = context.PLANNED_GetLocations().ToList();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region get Stock Appointment by Id
        /// <summary>
        /// This function returns Stock appointments 
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getStockAppointmentById(int appointmentId, int surveyId)
        {

            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where tenan.ENDDATE == null
                                && app.AppointId == appointmentId

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,
                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = false,
                                    appointmentDate = app.AppointStartDateTime,
                                    addToCalendar = app.addToCalendar,
                                    createdBy = app.CreatedBy,
                                    creationDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    //Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,
                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate,
                                        defaultPropertyPicId = pro.PropertyPicId
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item, surveyId);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get Stock Appointments between particular dates

        /// <summary>
        /// This function returns Stock appointments for 1 week (5 days) between particular dates
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getStockAppointmentsByDates(string startDate, string endDate, string username)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            // Converting Dates received from iOS 
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            //Getting Stock Appointment Information
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where tenan.ENDDATE == null
                                && app.SurveyourUserName == username
                                && app.AppointStartDateTime >= varStartTime
                                && app.AppointEndDateTime <= varEndTime
                                && !(completedStatusesList.Contains(app.AppointProgStatus))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,
                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = false,
                                    appointmentDate = app.AppointStartDateTime,
                                    createdBy = app.CreatedBy,
                                    addToCalendar = app.addToCalendar,
                                    creationDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    //Getting Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();
            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get All Stock Overdue Appointments

        /// <summary>
        /// This function will return all over due appointments from particular start date
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getAllOverdueStockAppointments(string startDate, string userName)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where tenan.ENDDATE == null
                                && app.SurveyourUserName == userName
                                && app.AppointEndDateTime <= varStartTime
                                && !(completedStatusesList.Contains(app.AppointProgStatus))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,

                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = true,
                                    appointmentDate = app.AppointStartDateTime,
                                    creationDate = app.CreatedDate,
                                    //Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get Stock Overdue Appointments between particular dates

        /// <summary>
        /// This function will return 4 over due appointments from particular start date
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getOverdueStockAppointmentsByDates(string startDate, string userName)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where tenan.ENDDATE == null
                                && app.SurveyourUserName == userName
                                && app.AppointEndDateTime <= varStartTime
                                && !(completedStatusesList.Contains(app.AppointProgStatus))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,

                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,

                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = true,
                                    appointmentDate = app.AppointStartDateTime,
                                    creationDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    //Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }

                                }).Take(4);

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);

                }

            }
            return appointmentList;
        }

        #endregion

        #region Get all gas appointments for properties

        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getAllAppointmentsGasProperty(string username, string startDate, string endDate, List<ExistingAppointmentParam> existingAppointments, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString(),
                                        AppointmentCompleteStatus.Aborted.ToString()};

            #region Query to get all gas appointment
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            var appointments = (from app in context.AS_APPOINTMENTS
                                join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                                join pro in context.P__PROPERTY on jor.PROPERTYID equals pro.PROPERTYID
                                join ten in context.C_TENANCY
                                        on new { TENANCYID = (Int32)app.TENANCYID, ENDDATE = (DateTime?)null }
                                        equals new { TENANCYID = ten.TENANCYID, ENDDATE = (DateTime?)ten.ENDDATE }
                                        into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join status in context.AS_Status on jor.STATUSID equals status.StatusId
                                join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                                join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID

                                orderby app.APPOINTMENTID

                                where login.LOGIN == username
                                && app.APPOINTMENTDATE >= varStartTime && app.APPOINTMENTDATE <= varEndTime
                                && !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.APPOINTMENTID,
                                    jsgNumber = app.JSGNUMBER,
                                    tenancyId = tenan.TENANCYID,
                                    journalId = app.JournalId,
                                    journalHistoryId = app.JOURNALHISTORYID,
                                    appointmentDate = app.APPOINTMENTDATE,
                                    appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                                    appointmentEndTimeString = app.APPOINTMENTENDTIME,
                                    addToCalendar = app.addToCalendar,
                                    surveyorUserName = login.LOGIN,
                                    creationDate = app.LOGGEDDATE,
                                    loggedDate = app.LOGGEDDATE,
                                    createdBy = app.CREATEDBY,
                                    appointmentNotes = app.NOTES,
                                    appointmentStatus = (app.APPOINTMENTSTATUS == null ? "NotStarted" : app.APPOINTMENTSTATUS),
                                    appointmentCalendar = (app.APPOINTMENTCALENDER == null ? "default" : app.APPOINTMENTCALENDER),
                                    surveyorAlert = (app.APPOINTMENTALERT == null ? "15 minutes before" : app.APPOINTMENTALERT),
                                    surveyourAvailability = (app.SURVEYOURSTATUS == null ? "Free" : app.SURVEYOURSTATUS),
                                    appointmentType = (app.IsVoid == 1 ? "Void Gas" : "Gas"),
                                    surveyType = app.SURVEYTYPE,
                                    createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME
                                    ,
                                    journal = new JournalData
                                    {
                                        actionId = jor.ACTIONID,
                                        creationBy = jor.CREATEDBY,
                                        creationDate = jor.CREATIONDATE,
                                        inspectionTypeId = jor.INSPECTIONTYPEID,
                                        isCurrent = jor.ISCURRENT,
                                        journalId = jor.JOURNALID,
                                        propertyId = jor.PROPERTYID,
                                        statusId = jor.STATUSID,
                                        journalHistoryId = app.JOURNALHISTORYID,
                                    },

                                    property = new PropertyData
                                    {
                                        propertyId = jor.PROPERTYID,
                                        tenancyId = tenan.TENANCYID,
                                        houseNumber = pro.HOUSENUMBER,
                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                    }

                                });
            #endregion
            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();
            List<AllAppointmentsList> gasAppointmentList = new List<AllAppointmentsList>();

            List<ExistingAppointmentParam> appAppointmentList = existingAppointments.OfType<ExistingAppointmentParam>().Where(s => s.appointmentType == "Gas" || s.appointmentType == "Void Gas").ToList();
            List<int> appAptIds = appAppointmentList.Select(app => app.appointmentId).ToList();

            List<int> deletedIds = new List<int>();
            List<ExistingAppointmentParam> deleteAppointmentList = new List<ExistingAppointmentParam>();
            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                List<int> dbAptIds = appointmentList.Select(app => app.appointmentId).ToList();
                deletedIds = appAptIds.Where(x => !dbAptIds.Contains(x)).ToList();
                deleteAppointmentList = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => deletedIds.Contains(s.appointmentId)).ToList();

                foreach (AllAppointmentsList dbAptItem in appointmentList)
                {

                    var extAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(app => app.appointmentId == dbAptItem.appointmentId && app.creationDate != dbAptItem.creationDate);
                    var emptyAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == dbAptItem.appointmentId);

                    if (emptyAppApt.Count() <= 0 || extAppApt.Count() > 0)
                    {
                        if (extAppApt.Count() > 0)
                            deleteAppointmentList.Add(extAppApt.FirstOrDefault());

                        #region "Appointment Info"

                        DateTime dt = (DateTime)dbAptItem.appointmentDate;
                        dbAptItem.appointmentStartDateTime = DateTime.Parse(dt.ToString("dd/MM/yyyy") + " " + dbAptItem.appointmentStartTimeString);
                        dbAptItem.appointmentEndDateTime = DateTime.Parse(dt.ToString("dd/MM/yyyy") + " " + dbAptItem.appointmentEndTimeString);
                        dbAptItem.componentTrade = null;
                        dbAptItem.jobDataList = null;

                        #endregion

                        #region "LGSR data for property"

                        var heatingInfo = context.AS_JournalHeatingMapping
                                                 .Where(x => x.JournalId == dbAptItem.journalId)
                                                 .OrderBy(y => y.HeatingMappingId)
                                                 .FirstOrDefault();
                        if (heatingInfo != null && dbAptItem.property != null)
                        {
                            var lgsrQuery = context.P_LGSR.Where(p => p.PROPERTYID == dbAptItem.property.propertyId && p.HeatingMappingId == heatingInfo.HeatingMappingId)
                                                  .OrderByDescending(x => x.LGSRID)
                                                  .FirstOrDefault();
                            if (lgsrQuery != null)
                            {
                                var issueDate = lgsrQuery.ISSUEDATE;
                                dbAptItem.property.certificateExpiry = issueDate != null ? (DateTime?)(issueDate.Value).AddYears(1) : null;
                            }

                        }

                        #endregion

                        #region "No Entry, CP12Info, Tenancy Info, Property Asbestos Info "
                        getPropertySpecificDataForGas(dbAptItem);
                        #endregion

                        #region "Attribute Notes Info of Property"
                        var data = (from obj in context.AS_GetAttributeNotesByPropertyID(dbAptItem.property.propertyId, true)
                                    select new PropertyAttributeData
                                    {
                                        attributePath = obj.AttributeName,
                                        attributeNotes = obj.AttributeNotes
                                    }
                                   );
                        dbAptItem.propertyAttributeNotes = data.ToList();
                        #endregion

                        gasAppointmentList.Add(dbAptItem);
                    }
                }
            }
            else
            {

                deleteAppointmentList = appAppointmentList;

            }


            foreach (ExistingAppointmentParam exApptList in deleteAppointmentList)
            {
                DeleteAppointmentParam delApp = new DeleteAppointmentParam();
                delApp.appointmentId = exApptList.appointmentId;
                delApp.appointmentType = exApptList.appointmentType;
                deleteAppointment.Add(delApp);
            }
            return gasAppointmentList;
        }

        #endregion

        #region Get all gas appointments

        /// <summary>
        /// This function returns all the appointments of Gas
        /// </summary>
        /// <param name="username"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="existingAppointments"></param>
        /// <param name="deleteAppointment"></param>
        /// <returns></returns>
        public List<AllAppointmentsList> getAllAppointmentsGas(string username, string startDate, string endDate, List<ExistingAppointmentParam> existingAppointments, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString(),
                                        AppointmentCompleteStatus.Aborted.ToString()};

            #region Query to get all gas appointment
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);

            var appointments = (from app in context.AS_APPOINTMENTS
                                join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                                join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                                join sType in context.P_ServicingType on jor.ServicingTypeId equals sType.ServicingTypeID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                                join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                                orderby app.APPOINTMENTID
                                where login.LOGIN == username
                                && jor.ISCURRENT == true
                                && app.APPOINTMENTDATE >= varStartTime && app.APPOINTMENTDATE <= varEndTime
                                && !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.                                
                                select new AllAppointmentsList
                                {
                                    appointmentId = app.APPOINTMENTID,
                                    jsgNumber = app.JSGNUMBER,
                                    appointmentDate = app.APPOINTMENTDATE,
                                    appointmentEndDate = app.APPOINTMENTENDDATE,
                                    appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                                    appointmentEndTimeString = app.APPOINTMENTENDTIME,
                                    addToCalendar = app.addToCalendar,
                                    surveyorUserName = login.LOGIN,
                                    creationDate = app.LOGGEDDATE,
                                    loggedDate = app.LOGGEDDATE,
                                    createdBy = app.CREATEDBY,
                                    appointmentNotes = app.NOTES,
                                    appointmentStatus = (app.APPOINTMENTSTATUS == null ? "NotStarted" : app.APPOINTMENTSTATUS),
                                    appointmentCalendar = (app.APPOINTMENTCALENDER == null ? "default" : app.APPOINTMENTCALENDER),
                                    surveyorAlert = (app.APPOINTMENTALERT == null ? "15 minutes before" : app.APPOINTMENTALERT),
                                    surveyourAvailability = (app.SURVEYOURSTATUS == null ? "Free" : app.SURVEYOURSTATUS),
                                    appointmentType = (app.IsVoid == 1 && sType.Description==ApplicationConstants.GasAppointmentType 
                                                       ? ApplicationConstants.VoidGasAppointmentType : sType.Description),
                                    surveyType = app.SURVEYTYPE,
                                    createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                                    journalId = app.JournalId,
                                    journalHistoryId = app.JOURNALHISTORYID,
                                    journal = new JournalData
                                    {
                                        actionId = jor.ACTIONID,
                                        creationBy = jor.CREATEDBY,
                                        creationDate = jor.CREATIONDATE,
                                        inspectionTypeId = jor.INSPECTIONTYPEID,
                                        isCurrent = jor.ISCURRENT,
                                        journalId = jor.JOURNALID,
                                        schemeId = jor.SchemeId,
                                        blockId = jor.BlockId,
                                        propertyId = jor.PROPERTYID,
                                        statusId = jor.STATUSID,
                                        journalHistoryId = app.JOURNALHISTORYID,
                                    },
                                    property = new PropertyData
                                    {
                                        propertyId = jor.PROPERTYID,
                                        schemeId = jor.SchemeId,
                                        blockId = jor.BlockId
                                    }
                                });
            #endregion


            List<string> applianceAptTypes = new List<string>() {ApplicationConstants.OilAppointmentType,
                                        ApplicationConstants.GasAppointmentType,
                                        ApplicationConstants.AlternativeAppointmentType,
                                        ApplicationConstants.VoidGasAppointmentType};

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();
            List<AllAppointmentsList> gasAppointmentList = new List<AllAppointmentsList>();

            List<ExistingAppointmentParam> appAppointmentList = existingAppointments.OfType<ExistingAppointmentParam>().Where(s => applianceAptTypes.Contains(s.appointmentType)).ToList();
            List<int> appAptIds = appAppointmentList.Select(app => app.appointmentId).ToList();

            List<int> deletedIds = new List<int>();
            List<ExistingAppointmentParam> deleteAppointmentList = new List<ExistingAppointmentParam>();
            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                List<int> dbAptIds = appointmentList.Select(app => app.appointmentId).ToList();
                deletedIds = appAptIds.Where(x => !dbAptIds.Contains(x)).ToList();
                deleteAppointmentList = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => deletedIds.Contains(s.appointmentId)).ToList();

                foreach (AllAppointmentsList dbAptItem in appointmentList)
                {

                    var extAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(app => app.appointmentId == dbAptItem.appointmentId && app.creationDate != dbAptItem.creationDate);
                    var emptyAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == dbAptItem.appointmentId);

                    if (emptyAppApt.Count() <= 0 || extAppApt.Count() > 0)
                    {
                        if (extAppApt.Count() > 0)
                            deleteAppointmentList.Add(extAppApt.FirstOrDefault());

                        var schemeId = dbAptItem.journal.schemeId;
                        var blockId = dbAptItem.journal.blockId;
                        var propertyId = dbAptItem.journal.propertyId;

                        #region "Populate Appointment General Info"

                        DateTime startDt = (DateTime)dbAptItem.appointmentDate;
                        DateTime endDt = (DateTime)dbAptItem.appointmentEndDate;

                        dbAptItem.appointmentStartDateTime = DateTime.Parse(startDt.ToString("dd/MM/yyyy") + " " + dbAptItem.appointmentStartTimeString);
                        dbAptItem.appointmentEndDateTime = DateTime.Parse(endDt.ToString("dd/MM/yyyy") + " " + dbAptItem.appointmentEndTimeString);
                        dbAptItem.componentTrade = null;
                        dbAptItem.jobDataList = null;
                        dbAptItem.scheme = null;

                        #endregion

                        #region "Populate Property/Scheme/Block info"

                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            var prop = context.P__PROPERTY.Where(p => p.PROPERTYID == propertyId).FirstOrDefault();
                            dbAptItem.property.houseNumber = prop.HOUSENUMBER;
                            dbAptItem.property.flatNumber = prop.FLATNUMBER;
                            dbAptItem.property.address1 = prop.ADDRESS1;
                            dbAptItem.property.address2 = prop.ADDRESS2;
                            dbAptItem.property.address3 = prop.ADDRESS3;
                            dbAptItem.property.defaultPropertyPicId = prop.PropertyPicId;
                            dbAptItem.property.townCity = prop.TOWNCITY;
                            dbAptItem.property.postCode = prop.POSTCODE;
                            dbAptItem.property.county = prop.COUNTY;
                        }
                        else if (schemeId != null && schemeId > 0)
                        {
                            var scheme = context.P_SCHEME.Where(s => s.SCHEMEID == schemeId).FirstOrDefault();
                            dbAptItem.property.schemeName = scheme.SCHEMENAME;
                        }
                        else if (blockId != null && blockId > 0)
                        {
                            var block = context.P_BLOCK.Where(b => b.BLOCKID == blockId).FirstOrDefault();
                            dbAptItem.property.blockName = block.BLOCKNAME;
                            dbAptItem.property.address1 = block.ADDRESS1;
                            dbAptItem.property.address2 = block.ADDRESS2;
                            dbAptItem.property.address3 = block.ADDRESS3;
                            dbAptItem.property.townCity = block.TOWNCITY;
                            dbAptItem.property.postCode = block.POSTCODE;
                            dbAptItem.property.county = block.COUNTY;
                        }

                        #endregion

                        #region "Populate Appointment No Entry info"

                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            dbAptItem.appInfoData = this.getTotalNoEntriesGas(propertyId, dbAptItem.journalId);
                        }
                        else if (blockId != null && blockId > 0)
                        {
                            dbAptItem.appInfoData = this.getTotalNoEntriesGasForBlock(dbAptItem.journalId, blockId);
                        }
                        else if (schemeId != null && schemeId > 0)
                        {
                            dbAptItem.appInfoData = this.getTotalNoEntriesGasForScheme(dbAptItem.journalId, schemeId);
                        }
                        #endregion

                        #region "Certificate Info for respective heating/boiler item"
                        var certificateDal = new IssuedReceivedByDal();
                        dbAptItem.cp12Info = certificateDal.getIssuedReceivedByFormData(dbAptItem);
                        #endregion

                        #region "LGSR data"

                        var heatingInfo = (from jm in context.AS_JournalHeatingMapping
                                           join lg in context.P_LGSR on jm.HeatingMappingId equals lg.HeatingMappingId
                                           where jm.JournalId == dbAptItem.journalId && lg.ISSUEDATE != null
                                           select lg).FirstOrDefault();
                        if (heatingInfo != null)
                        {
                            dbAptItem.property.certificateExpiry = (DateTime?)(heatingInfo.ISSUEDATE.Value).AddYears(1);
                        }
                        else
                        {
                            dbAptItem.property.certificateExpiry = null;
                        }

                        #endregion

                        #region "Populate Heating Info"

                        var heatingQuery = (from jm in context.AS_JournalHeatingMapping
                                            join lg in context.PA_HeatingMapping on jm.HeatingMappingId equals lg.HeatingMappingId
                                            join ht in context.PA_PARAMETER_VALUE on lg.HeatingType equals ht.ValueID
                                            where jm.JournalId == dbAptItem.journalId
                                            select ht);
                        if (heatingQuery != null)
                        {
                            dbAptItem.heatingFuel = string.Join(", ", heatingQuery.Select(h => h.ValueDetail));
                        }

                        #endregion

                        #region "Property/Scheme/Block Asbestos"

                        PropertyDal propDal = new PropertyDal();
                        List<AsbestosData> asbListData = new List<AsbestosData>();

                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            propDal.getPropertyAsbestosRisk(propertyId);
                        }
                        else if (blockId != null && blockId > 0)
                        {
                            asbListData = propDal.getBlockAsbestosRisk(blockId);
                        }
                        else if (schemeId != null && schemeId > 0)
                        {
                            asbListData = propDal.getSchemeAsbestosRisk(schemeId);
                        }
                        dbAptItem.property.propertyAsbestosData = asbListData;

                        #endregion

                        #region "Get Property Specific Data For Gas"
                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            getPropertySpecificDataForGas(dbAptItem);
                        }
                        #endregion

                        #region "Get Attribute Notes For Property / Scheme / Block"
                        PropertyAttributesDal attrDal = new PropertyAttributesDal();

                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            var data = (from obj in context.AS_GetAttributeNotesByPropertyID(dbAptItem.property.propertyId, true)
                                        select new PropertyAttributeData
                                        {
                                            attributePath = obj.AttributeName,
                                            attributeNotes = obj.AttributeNotes
                                        }
                                                              );
                            dbAptItem.propertyAttributeNotes = data.ToList();
                        }
                        else
                        {
                            dbAptItem.propertyAttributeNotes = attrDal.getAttributeNotesForSchemeBlock(schemeId, blockId);
                        }

                        #endregion

                        gasAppointmentList.Add(dbAptItem);
                    }

                }
            }
            else
            {

                deleteAppointmentList = appAppointmentList;

            }


            foreach (ExistingAppointmentParam exApptList in deleteAppointmentList)
            {
                DeleteAppointmentParam delApp = new DeleteAppointmentParam();
                delApp.appointmentId = exApptList.appointmentId;
                delApp.appointmentType = exApptList.appointmentType;
                deleteAppointment.Add(delApp);
            }
            return gasAppointmentList;
        }

        #endregion

        #region Update Fault Appointment Progress Status
        /// <summary>
        /// This function update the status of fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <param name="isAppointFinished">this flag represents the finished appointment flag </param>
        /// <param name="lastSurveyDate">The date of last survey which 'll be filled if status is finished</param>
        /// <returns>It returns true or false in update is successful</returns>
        public ResultBoolData updateFaultAppointmentProgressStatus(int appointmentID, string progressStatus, bool isAppointFinished)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;

            //if (progressStatus.Trim().Replace(" ", "") != FaultAppointmentProgressStatus.InProgress.ToString())
            //{
            //    resultBoolData.result = success;
            //    return resultBoolData;
            //}

            //using (TransactionScope trans = new TransactionScope())
            //{
            var appointment = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointmentID).First();
            appointment.AppointmentStatus = progressStatus;

            success = true;

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //    trans.Complete();
            //}
            if (success == false)
            {
                resultBoolData.result = success;
                return resultBoolData;
            }

            if (isAppointFinished == true)
            {
                this.saveLastSurveyDateFault(appointmentID);
            }

            resultBoolData.result = success;
            return resultBoolData;
        }
        #endregion

        #region Update Fault Status

        /// <summary>
        /// This function update the status of all faults of an fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This function accepts the AppointmentData's object </param>      
        /// <returns>It returns true or false in update is successful</returns>
        public Nullable<int> updateFaultStatusId(PropertySurvey_Entities context, JobData jobData, int faultLogID, string progressStatus, int? operativeID, DateTime? submittedDate, DateTime? completedDate = null)
        {
            Nullable<int> faultLogHistoryId = null;

            FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == faultLogID).First();
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description.Replace(" ", "") == progressStatus);

            if (fltStatusList.Count() == 0)
                return faultLogHistoryId;

            FL_FAULT_STATUS fltStatus = fltStatusList.First();
            faultLog.StatusID = fltStatus.FaultStatusID;

            if (progressStatus == FaultJobProgressStatus.Complete.ToString())
            {
                if (completedDate != null)
                {
                    faultLog.CompletedDate = completedDate;
                }

                if (jobData.jsCurrentAppVersion != null)
                {
                    faultLog.JobsheetCurrentAppVersion = jobData.jsCurrentAppVersion;
                }

                if (jobData.jsCompletedAppVersion != null)
                {
                    faultLog.JobsheetCompletedAppVersion = jobData.jsCompletedAppVersion;
                }
            }

            // insert record in fault log history
            faultLogHistoryId = saveFaultLogHistoryData(context, faultLog, operativeID, submittedDate);

            return faultLogHistoryId;
        }

        #endregion

        #region is Property N Customer Exists
        /// <summary>
        /// This function checks the property id against the customer, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="customerId">customer id</param>
        /// <returns>returns true or false</returns>

        public bool isPropertyNCustomerExists(string propertyId, int customerId)
        {
            bool success = false;
            var custRecord = (from cte in context.C_TENANCY
                              join prop in context.P__PROPERTY on cte.PROPERTYID equals prop.PROPERTYID
                              join cusp in context.C_CUSTOMERTENANCY on cte.TENANCYID equals cusp.TENANCYID
                              join cust in context.C__CUSTOMER on cusp.CUSTOMERID equals cust.CUSTOMERID
                              where cte.PROPERTYID.ToLower() == propertyId.ToLower() && cust.CUSTOMERID == customerId
                              select cte);

            if (custRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        #endregion

        #region save Faults Log History Data

        /// <summary>
        /// This function saves the Faults Log History Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Log History Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public Nullable<int> saveFaultLogHistoryData(PropertySurvey_Entities context, FL_FAULT_LOG faultLog, int? operativeID, DateTime? submittedDate)
        {
            Nullable<int> faultLogHistoryId = null;
            FL_FAULT_LOG_HISTORY faultLogHistoryData = new FL_FAULT_LOG_HISTORY();

            var faultJournalList = context.FL_FAULT_JOURNAL.Where(jor => jor.FaultLogID == faultLog.FaultLogID);
            if (faultJournalList.Count() > 0)
            {
                FL_FAULT_JOURNAL faultJournal = faultJournalList.First();
                faultLogHistoryData.JournalID = faultJournal.JournalID;
                faultJournal.FaultStatusID = faultLog.StatusID;
            }
            faultLogHistoryData.FaultStatusID = faultLog.StatusID;
            // faultLogHistoryData.itemActionID;
            faultLogHistoryData.LastActionDate = submittedDate ?? DateTime.Now;
            faultLogHistoryData.LastActionUserID = operativeID;
            faultLogHistoryData.FaultLogID = faultLog.FaultLogID;
            faultLogHistoryData.ORGID = faultLog.ORGID;
            //faultLogHistoryData.scopeID;
            //faultLogHistoryData.title;
            faultLogHistoryData.Notes = faultLog.Notes;
            faultLogHistoryData.PROPERTYID = faultLog.PROPERTYID;
            faultLogHistoryData.ContractorID = faultLog.ContractorID;
            faultLogHistoryData.SchemeID = faultLog.SchemeId;
            faultLogHistoryData.BlockID = faultLog.BlockId;

            context.AddToFL_FAULT_LOG_HISTORY(faultLogHistoryData);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            faultLogHistoryId = faultLogHistoryData.FaultLogHistoryID;

            return faultLogHistoryId;
        }

        #endregion

        #region get misc appointment data
        public bool isFaultCancelled(int faultLogID)
        {
            int cancelledStatusID = GetJobStatusID("Cancelled");

            FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == faultLogID).First();
            if (faultLog.StatusID == cancelledStatusID)
                return true;
            else
                return false;
        }

        public List<FL_FAULT_APPOINTMENT> getFaultAppointmentList(int appointmentID)
        {
            var fltApptList = (from fltAppt in context.FL_FAULT_APPOINTMENT
                               orderby fltAppt.FaultAppointmentId
                               where fltAppt.AppointmentId == appointmentID
                               select fltAppt);
            return fltApptList.ToList();
        }

        public List<FL_FAULT_LOG> getFaultLogList(int appointmentID)
        {
            var flLogList = (from fltl in context.FL_FAULT_LOG
                             join fltAppt in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals fltAppt.FaultLogId
                             orderby fltl.FaultLogID
                             where fltAppt.AppointmentId == appointmentID
                             select fltl);
            return flLogList.ToList();
        }

        #endregion

        #region get And Set Appointment Progress Status

        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>
        public string getFaultJobStatus(int appointmentId)
        {
            string status = "";
            var fltList = (from fltl in context.FL_FAULT_LOG
                           join fltAppt in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals fltAppt.FaultLogId
                           join fltStatus in context.FL_FAULT_STATUS on fltl.StatusID equals fltStatus.FaultStatusID
                           orderby fltl.FaultLogID
                           where fltAppt.AppointmentId == appointmentId
                           && fltAppt.FaultLogId == (context.FL_FAULT_APPOINTMENT.Where(ap => ap.AppointmentId == appointmentId).Min(ap => ap.FaultLogId))
                           select fltStatus.Description);

            if (fltList.Count() == 0)
                status = "{\"jobStatusNameKey\":\"\"}";

            status = fltList.First();



            if (status.Trim().Replace(" ", "").ToLower() == FaultAppointmentProgressStatus.AppointmentArranged.ToString().ToLower())
            {
                status = FaultJobProgressStatus.NotStarted.ToString();
            }
            else if (status.Trim().Replace(" ", "").ToLower() == FaultJobProgressStatus.InProgress.ToString().ToLower())
            {
                status = FaultJobProgressStatus.Started.ToString();
            }

            // Code added  - 12/06/2013 - END

            status = "{\"jobStatusNameKey\":\"" + status + "\"}";

            return status;
        }

        #endregion

        #region save Stock Appointment
        /// <summary>
        /// This function saves the Stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>
        public int saveStockAppointment(AppointmentDataStock varAppointmentDataStock, out int surveyId)
        {
            bool success = false;

            var appData = context.PS_Appointment.Where(app => app.SurveyourUserName == varAppointmentDataStock.surveyorUserName
                                                        && ((app.AppointStartDateTime == varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime < varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime == varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime > varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime < varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime > varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime > varAppointmentDataStock.appointmentStartDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentEndDateTime && app.AppointEndDateTime > varAppointmentDataStock.appointmentEndDateTime))

                                                        );
            if (appData.Count() > 0)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointmentAlreadyExistMsg, varAppointmentDataStock.surveyorUserName), true, MessageCodesConstants.AppointmentAlreadyExist);
                throw new ArgumentException(String.Format(MessageConstants.AppointmentAlreadyExistMsg, varAppointmentDataStock.surveyorUserName), "surveyorUserName");

            }

            PS_Appointment appointment = new PS_Appointment();
            using (TransactionScope trans = new TransactionScope())
            {

                appointment.AppointLocation = varAppointmentDataStock.appointmentLocation;
                appointment.AppointNotes = varAppointmentDataStock.appointmentNotes;
                appointment.AppointProgStatus = varAppointmentDataStock.appointmentStatus;

                appointment.AppointStartDateTime = varAppointmentDataStock.appointmentStartDateTime;
                appointment.AppointEndDateTime = varAppointmentDataStock.appointmentEndDateTime;
                appointment.AppointTitle = varAppointmentDataStock.appointmentTitle;
                appointment.AppointType = varAppointmentDataStock.appointmentType;

                appointment.AppointValidity = varAppointmentDataStock.appointmentValidity;
                appointment.CreatedBy = varAppointmentDataStock.createdBy;
                appointment.ModifiedBy = varAppointmentDataStock.createdBy;
                appointment.addToCalendar = varAppointmentDataStock.addToCalendar;
                appointment.SurveyourStatus = varAppointmentDataStock.surveyourAvailability;
                appointment.SurveyType = varAppointmentDataStock.surveyType;
                appointment.SurveyourUserName = varAppointmentDataStock.surveyorUserName;
                appointment.CreatedDate = varAppointmentDataStock.loggedDate;
                appointment.ModifiedDate = varAppointmentDataStock.loggedDate;

                if (varAppointmentDataStock.surveyorAlert != null)
                {
                    appointment.AppointmentAlert = varAppointmentDataStock.surveyorAlert;
                }
                else
                {
                    appointment.AppointmentAlert = "None";
                }
                if (varAppointmentDataStock.appointmentCalendar != null)
                {
                    appointment.AppointmentCalendar = varAppointmentDataStock.appointmentCalendar;
                }
                else
                {
                    appointment.AppointmentCalendar = "Property Survey";
                }

                context.AddToPS_Appointment(appointment);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                PS_Property2Appointment property2Appointment = new PS_Property2Appointment();

                property2Appointment.AppointId = appointment.AppointId;

                if (varAppointmentDataStock.customer.customerId != null)
                {
                    property2Appointment.CustomerId = (int)varAppointmentDataStock.customer.customerId;
                }
                property2Appointment.PropertyId = varAppointmentDataStock.customer.property.propertyId;
                if (varAppointmentDataStock.customer.property.tenancyId != null)
                {
                    property2Appointment.TenancyId = (int)varAppointmentDataStock.customer.property.tenancyId;
                }

                context.PA_PROPERTY_ATTRIBUTES.Where(attrib => attrib.PROPERTYID == varAppointmentDataStock.customer.property.propertyId).ToList().ForEach(attrib => attrib.IsUpdated = false);
                context.AddToPS_Property2Appointment(property2Appointment);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                // Add a new survey for above appointment.
                PS_Survey survey = new PS_Survey();

                survey.PROPERTYID = property2Appointment.PropertyId;
                survey.CompletedBy = varAppointmentDataStock.createdBy;
                survey.SurveyDate = varAppointmentDataStock.loggedDate;

                context.AddToPS_Survey(survey);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                surveyId = survey.SurveyId;

                // Add a new record in PS_Appointment2Survey to relate survey to appointment.
                PS_Appointment2Survey appointment2Survey = new PS_Appointment2Survey();

                appointment2Survey.AppointId = appointment.AppointId;
                appointment2Survey.SurveyId = surveyId;

                context.AddToPS_Appointment2Survey(appointment2Survey);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return appointment.AppointId;
            }
            else
            {
                return 0;
            }
        }
        #endregion


        #region Save Abort
        /// <summary>
        /// Save Abort Entry For Appliances Appointments
        /// </summary>
        /// <param name="appointmentData"></param>
        /// <returns></returns>
        public bool saveAbortEntry(AllAppointmentsList appointmentData)
        {
            bool isSaved = false;

            #region "Abort appointment entry"
            AS_AbortAppointments abortAppointment = new AS_AbortAppointments();
            abortAppointment.AbortNotes = appointmentData.appointmentAbortNotes;
            int reasonId = context.AS_AbortReason.Where(x => x.AbortReason == appointmentData.appointmentAbortReason).FirstOrDefault().AbortReasonId;
            abortAppointment.AbortReasonId = reasonId;
            abortAppointment.AppointmentId = appointmentData.appointmentId;
            context.AddToAS_AbortAppointments(abortAppointment);
            context.SaveChanges();
            #endregion

            #region "Update journal"

            AS_JOURNAL jorData = context.AS_JOURNAL.Where(jor => jor.JOURNALID == appointmentData.journal.journalId).FirstOrDefault();
            var status = context.AS_Status.Where(s => s.Title == Utilities.Constants.MessageConstants.AbortedInAs_Status).FirstOrDefault();
            var action = context.AS_Action.Where(s => s.Title == Utilities.Constants.MessageConstants.AbortedInAs_Action).FirstOrDefault();

            jorData.STATUSID = status.StatusId;
            if (action != null)
            {
                jorData.ACTIONID = action.ActionId;
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

            #endregion

            #region "Send email to operative"

            string email = context.E_CONTACT.Where(x => x.EMPLOYEEID == appointmentData.createdBy).FirstOrDefault().WORKEMAIL;

            if (Validator.isEmail(email))
            {
                string recieverName = context.E__EMPLOYEE.Where(x => x.EMPLOYEEID == appointmentData.createdBy).FirstOrDefault().FIRSTNAME;
                string name = context.E__EMPLOYEE.Where(x => x.EMPLOYEEID == appointmentData.createdBy).FirstOrDefault().FIRSTNAME;
                var gastAptType = getGasAppointmentType(appointmentData.property);
                var address = getPropertyAddress(appointmentData.property);
                var creationDate = Convert.ToDateTime(appointmentData.creationDate).ToString("dd/MM/yyyy");
                var creationTime = Convert.ToDateTime(appointmentData.creationDate).ToString("hh:mm");

                // 0 - Reciever Name
                // 1 - Surveyor UserName
                // 2 - Address
                // 3 - Creation Date
                // 4 - Creation Time
                // 5 - Abort Reason
                // 6 - Abort Notes
                // 7 - Property / Scheme / Block
                // 8 - Gas / Void Gas / Alternative / Oil
                string emailBodyFormat = "Dear {0} <br><br> The {8} Servicing appointment arranged for {1} to carry out at {2} on {3} at {4} was aborted due to {5}.<br><br> If the operative recorded any additional notes, please find them below: <br> {6} <br><br> The {7} has been added to the ‘Appointments to be Arranged’ list. Please reschedule the appointment if necessary.";
                string emailBody = string.Format(emailBodyFormat, recieverName, appointmentData.surveyorUserName, address, creationDate, creationTime, appointmentData.appointmentAbortReason, appointmentData.appointmentAbortNotes, gastAptType, appointmentData.appointmentType);
                EmailHelper.sendHtmlFormattedEmail(name, email, MessageConstants.AbortAppointmentSubject, emailBody);
            }

            #endregion

            #region "Insert into Journal History"
            AS_APPOINTMENTS appData = context.AS_APPOINTMENTS.Where(app => app.JournalId == appointmentData.journal.journalId).FirstOrDefault();
            AS_JOURNALHISTORY jorHis = new AS_JOURNALHISTORY()
            {
                ACTIONID = jorData.ACTIONID,
                CREATEDBY = appointmentData.updatedBy,
                CREATIONDATE = DateTime.Now,
                NOTES = appData.NOTES,
                INSPECTIONTYPEID = jorData.INSPECTIONTYPEID,
                STATUSID = jorData.STATUSID,
                PROPERTYID = jorData.PROPERTYID,
                SchemeId = jorData.SchemeId,
                BlockId = jorData.BlockId,
                JOURNALID = jorData.JOURNALID,
                ISLETTERATTACHED = false,
                IsDocumentAttached = false
            };

            context.AddToAS_JOURNALHISTORY(jorHis);
            #endregion

            #region "Update Appointment info related to App version"
            if (appointmentData.loggedDate != null)
            {
                appData.AppointmentCompletionDate = appointmentData.loggedDate;
            }

            if (appointmentData.appointmentCompletedAppVersion != null)
            {
                appData.AppointmentCompletedAppVersion = appointmentData.appointmentCompletedAppVersion;
            }

            if (appointmentData.appointmentCurrentAppVersion != null)
            {
                appData.AppointmentCurrentAppVersion = appointmentData.appointmentCurrentAppVersion;
            }
            #endregion

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            isSaved = true;
            appointmentData.journalHistoryId = jorHis.JOURNALHISTORYID;

            return isSaved;

        }
        #endregion

        #region "Get Gas Appoitment Type"
        public string getGasAppointmentType(PropertyData property)
        {
            string aptType = string.Empty;
            if (property.propertyId != null)
            {
                aptType = ApplicationConstants.PropertyGasAppointment;
            }
            else if (property.schemeId > 0)
            {
                aptType = ApplicationConstants.SchemeGasAppointment;
            }
            else if (property.blockId > 0)
            {
                aptType = ApplicationConstants.BlockGasAppointment;
            }
            return aptType;
        }
        #endregion


        #region "Get Property/Scheme/Block Address"

        public string getPropertyAddress(PropertyData property)
        {
            var address = string.Empty;

            if (property.propertyId != null)
            {
                var adrsArray = new[] { property.houseNumber, property.address1, property.townCity, property.postCode };
                address = string.Format("{0}", string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s))));
            }
            else if (property.schemeId > 0)
            {
                var adrsArray = new[] { property.schemeName };
                address = string.Format("{0}", string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s))));
            }
            else if (property.blockId > 0)
            {
                var adrsArray = new[] { property.blockName, property.address1, property.townCity, property.postCode };
                address = string.Format("{0}", string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s))));
            }

            return address;
        }
        #endregion


        #region save NoEntry
        /// <summary>
        /// save NoEntry For Gas
        /// </summary>
        /// <param name="noEntryData"></param>
        /// <returns></returns>
        public int updateApplianceJournalForNoEntryStatus(AllAppointmentsList noEntryData)
        {
            bool success = false;
            AS_NoEntry noEntry = new AS_NoEntry();

            #region "Insert into No Entry"

            //Insert into no Entry
            noEntry.isCardLeft = (bool)noEntryData.appInfoData.isCardLeft;
            noEntry.RecordedBy = (int)noEntryData.createdBy;
            noEntry.RecordedDate = DateTime.Now;
            noEntry.JournalId = (int)noEntryData.journal.journalId;

            context.AddToAS_NoEntry(noEntry);
            #endregion

            #region "Update journal"
            //Update journal
            AS_JOURNAL jorData = context.AS_JOURNAL.Where(jor => jor.JOURNALID == noEntryData.journal.journalId).FirstOrDefault();
            var status = context.AS_Status.Where(s => s.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault();
            var action = context.AS_Action.Where(s => s.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Action).FirstOrDefault();

            jorData.STATUSID = status.StatusId;
            if (action != null)
            {
                jorData.ACTIONID = action.ActionId;
            }

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            #endregion

            #region "Insert into Journal History"
            AS_APPOINTMENTS appData = context.AS_APPOINTMENTS.Where(app => app.JournalId == noEntryData.journal.journalId).FirstOrDefault();
            AS_JOURNALHISTORY jorHis = new AS_JOURNALHISTORY()
            {
                ACTIONID = jorData.ACTIONID,
                CREATEDBY = noEntryData.updatedBy,
                CREATIONDATE = DateTime.Now,
                NOTES = appData.NOTES,
                INSPECTIONTYPEID = jorData.INSPECTIONTYPEID,
                STATUSID = jorData.STATUSID,
                PROPERTYID = jorData.PROPERTYID,
                SchemeId = jorData.SchemeId,
                BlockId = jorData.BlockId,
                JOURNALID = jorData.JOURNALID,
                ISLETTERATTACHED = false,
                IsDocumentAttached = false
            };

            context.AddToAS_JOURNALHISTORY(jorHis);
            #endregion

            #region "Update Appointment info related to App version"
            if (noEntryData.loggedDate != null)
            {
                appData.AppointmentCompletionDate = noEntryData.loggedDate;
            }

            if (noEntryData.appointmentCompletedAppVersion != null)
            {
                appData.AppointmentCompletedAppVersion = noEntryData.appointmentCompletedAppVersion;
            }

            if (noEntryData.appointmentCurrentAppVersion != null)
            {
                appData.AppointmentCurrentAppVersion = noEntryData.appointmentCurrentAppVersion;
            }
            #endregion

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            success = true;

            noEntryData.journalHistoryId = jorHis.JOURNALHISTORYID;

            return (success == true) ? noEntry.NoEntryID : 0;
        }
        #endregion

        #region Get Total No of NoEntries by PropertyID
        /// <summary>
        /// This function get appointment NoEntry from the database
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>

        public AppointmentInfoData getTotalNoEntries(string propertyID)
        {
            AppointmentInfoData noEntries = new AppointmentInfoData();

            var noEntriesData = (from entry in context.PS_NoEntry
                                 where entry.PropertyID == propertyID
                                 select entry);
            var totalAppointmentData = (from app in context.PS_Property2Appointment
                                        join a in context.PS_Appointment on app.AppointId equals a.AppointId
                                        where app.PropertyId == propertyID
                                        select a);
            noEntries.totalNoEntries = noEntriesData.Count();
            noEntries.totalAppointments = totalAppointmentData.Count();

            return noEntries;
        }

        /// <summary>
        /// Get Total No Entries Gas For Scheme
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public AppointmentInfoData getTotalNoEntriesGasForScheme(int? journalId, int? schemeId)
        {
            AppointmentInfoData appInfo = new AppointmentInfoData();

            var journalQuery = (from history in context.AS_JOURNALHISTORY
                                join sts in context.AS_Status on history.STATUSID equals sts.StatusId
                                where history.SchemeId == schemeId
                                && history.JOURNALID == journalId
                                select new { JOURNALHISTORYID = history.JOURNALHISTORYID, status = sts.Title });

            appInfo.totalNoEntries = journalQuery.Where(x => x.status == MessageConstants.NoEntryInAS_Status).Count();
            appInfo.totalAppointments = journalQuery.Where(x => x.status == MessageConstants.AppointmentArrangedInAS_Status).Count();

            return appInfo;
        }

        /// <summary>
        /// Get Total No Entries Gas For Scheme
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <param name="blockId"></param>
        /// <returns></returns>
        public AppointmentInfoData getTotalNoEntriesGasForBlock(int? journalId, int? blockId)
        {
            AppointmentInfoData appInfo = new AppointmentInfoData();

            var journalQuery = (from history in context.AS_JOURNALHISTORY
                                join sts in context.AS_Status on history.STATUSID equals sts.StatusId
                                where history.BlockId == blockId
                                && history.JOURNALID == journalId
                                select new { JOURNALHISTORYID = history.JOURNALHISTORYID, status = sts.Title });

            appInfo.totalNoEntries = journalQuery.Where(x => x.status == MessageConstants.NoEntryInAS_Status).Count();
            appInfo.totalAppointments = journalQuery.Where(x => x.status == MessageConstants.AppointmentArrangedInAS_Status).Count();

            return appInfo;
        }

        /// <summary>
        /// This function get appointment NoEntry from the database related to GAS
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>
        public AppointmentInfoData getTotalNoEntriesGas(string propertyID, int? journalId)
        {
            AppointmentInfoData noEntries = new AppointmentInfoData();

            noEntries.totalNoEntries = 0;
            noEntries.totalAppointments = 0;
            var isCP12Issued = (from app in context.AS_APPOINTMENTS
                                join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                                where jor.PROPERTYID == propertyID && jor.ISCURRENT == true && jor.JOURNALID == journalId && (jor.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentArrangedInAS_Status).FirstOrDefault().StatusId) || jor.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault().StatusId))
                                select app);

            if (isCP12Issued.Count() > 0)
            {
                var noEntriesData = (from history in context.AS_JOURNALHISTORY
                                     where history.PROPERTYID == propertyID && history.JOURNALID == journalId
                                     && history.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault().StatusId)
                                     select history);

                var totalAppointmentData = (from history in context.AS_JOURNALHISTORY
                                            where history.PROPERTYID == propertyID && history.JOURNALID == journalId
                                            && history.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentArrangedInAS_Status).FirstOrDefault().StatusId)
                                            select history.JOURNALID);

                noEntries.totalNoEntries = noEntriesData.Count();
                noEntries.totalAppointments = totalAppointmentData.Count();
            }
            return noEntries;
        }

        #endregion

        #region Get JobSheetNumber List for all jobs of an AppointmentID
        /// <summary>
        /// This function returns the list of JobSheetNumber of all jobs of an appointment.
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>the list of JobSheetNumber Object</returns>

        public List<JobData> getJobDataList(int appointmentID)
        {
            var jobSheetNumbers = (from app in context.FL_CO_APPOINTMENT
                                   join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                                   join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                                   join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                                   join flt in context.FL_FAULT on fltl.FaultID equals flt.FaultID
                                   join are in context.FL_AREA on fltl.areaId equals are.AreaID
                                   join pri in context.FL_FAULT_PRIORITY on flt.PriorityID equals pri.PriorityID
                                   orderby app.AppointmentID
                                   where app.AppointmentID == appointmentID
                                   select new JobData
                                   {
                                       JSNumber = fltl.JobSheetNumber,
                                       faultLogID = fltl.FaultLogID,
                                       JSNDescription = flt.Description,
                                       JSNNotes = fltl.Notes,
                                       JSNLocation = are.AreaName,
                                       duration = (double?)fltl.Duration ?? flt.duration ?? 0,
                                       reportedDate = fltl.SubmitDate,
                                       priority = pri.PriorityName,
                                       responseTime = (pri.ResponseTime == 24 ? "24 Hours" : (pri.ResponseTime == 1 ? "1 Day" : (pri.ResponseTime == 7 ? "7 Days" : "28 Days"))),
                                       jobStatus = flts.Description
                                   });

            List<JobData> jobDataList = new List<JobData>();

            if (jobSheetNumbers.Count() > 0)
            {
                jobDataList = jobSheetNumbers.ToList();

                // Adding fault repair list for complete faults
                foreach (JobData jobData in jobDataList)
                {
                    jobData.durationUnit = jobData.duration.ToString() + " Hour(s)";
                    jobData.repairNotes = GetRepairNotes(jobData.faultLogID);
                    if (jobData.jobStatus.Replace(" ", "") == FaultAppointmentProgressStatus.AppointmentArranged.ToString())
                    {
                        jobData.jobStatus = FaultJobProgressStatus.NotStarted.ToString();
                    }
                    else if (jobData.jobStatus.Replace(" ", "") == FaultJobProgressStatus.InProgress.ToString())
                    {
                        jobData.jobStatus = FaultJobProgressStatus.InProgress.ToString();
                    }

                    var completionDateData = context.C_REPAIR.Where(cr => cr.ITEMDETAILID == jobData.faultLogID);

                    if (completionDateData.Count() > 0)
                    {
                        jobData.completionDate = completionDateData.First().LASTACTIONDATE;
                    }
                    else
                    {
                        jobData.completionDate = null;
                    }

                    var followOnData = context.FL_FAULT_FOLLOWON.Where(fff => fff.FaultLogId == jobData.faultLogID);

                    if (followOnData.Count() > 0)
                    {
                        List<FL_FAULT_FOLLOWON> followOnList = followOnData.ToList();

                        jobData.followOnNotes = followOnList.Last().FollowOnNotes;
                    }
                    else
                    {
                        jobData.followOnNotes = null;
                    }

                    var faultRepair = (from flr in context.FL_CO_FAULTLOG_TO_REPAIR
                                       join frl in context.FL_FAULT_REPAIR_LIST on flr.FaultRepairListID equals frl.FaultRepairListID
                                       where flr.FaultLogID == jobData.faultLogID
                                       select new FaultRepairData
                                       {
                                           faultRepairId = flr.FaultRepairListID,
                                           description = frl.Description
                                       });

                    if (faultRepair.Count() > 0)
                    {
                        jobData.faultRepairList = faultRepair.ToList();
                    }
                    List<RepairImageData> propertyPicList = new List<RepairImageData>();
                    var faultRepairImages = (from fri in context.FL_FAULT_REPAIR_IMAGES
                                             where fri.JobSheetNumber == jobData.JSNumber
                                             select new RepairImageData
                                             {
                                                 faultRepairImageId = fri.FaultRepairImageId,
                                                 propertyId = fri.PropertyId,
                                                 jsNumber = fri.JobSheetNumber,
                                                 imageName = fri.ImageName,
                                                 isBeforeImage = fri.IsBeforeImage,
                                                 createdBy = fri.CreatedBy,
                                                 createdOn = fri.CreatedOn

                                             });

                    if (faultRepairImages.Count() > 0)
                    {
                        propertyPicList = faultRepairImages.ToList();
                        foreach (RepairImageData pic in propertyPicList)
                        {
                            pic.imagePath = FileHelper.getLogicalPropertyImagePath(pic.propertyId, pic.imageName);
                        }
                        jobData.repairImageList = propertyPicList;
                    }


                }


            }

            return jobDataList;
        }

        private string GetRepairNotes(int faultLogID)
        {
            var faultRepair = context.FL_CO_FAULTLOG_TO_REPAIR.Where(rep => rep.FaultLogID == faultLogID);

            string repairNotes = string.Empty;
            if (faultRepair != null && faultRepair.Count() > 0)
            {
                repairNotes = faultRepair.First().Notes;
            }
            return repairNotes;
        }
        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        public List<SurveyorUserData> getAllUsers()
        {
            var users = (from app in context.AC_LOGINS
                         join emp in context.E__EMPLOYEE on app.EMPLOYEEID equals emp.EMPLOYEEID
                         where app.ACTIVE == 1
                         orderby app.LOGIN
                         select new SurveyorUserData
                         {
                             userId = app.LOGINID,
                             userName = app.LOGIN,
                             fullName = emp.FIRSTNAME + " " + emp.LASTNAME
                         });



            List<SurveyorUserData> userList = new List<SurveyorUserData>();
            if (users.Count() > 0)
            {
                userList = users.ToList();
            }

            return userList;
        }

        /// <summary>
        /// This function returns all the users of rsl manager database for Gas
        /// </summary>        
        /// <returns>list of all Gas users</returns>
        public List<SurveyorUserData> getAllUsersGas()
        {
            var users = from user in context.AS_USER
                        join login in context.AC_LOGINS on user.EmployeeId equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on user.EmployeeId equals emp.EMPLOYEEID
                        where user.IsActive == true
                        orderby login.LOGIN
                        select new SurveyorUserData
                        {
                            userId = login.LOGINID,
                            userName = login.LOGIN,
                            fullName = emp.FIRSTNAME + " " + emp.LASTNAME
                        };

            List<SurveyorUserData> userList = new List<SurveyorUserData>();
            if (users.Count() > 0)
            {
                userList = users.ToList();
            }

            return userList;
        }

        #endregion

        #region delete Appointment
        /// <summary>
        /// This function is used to delete the appointment from the server
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true or false based on deleteion</returns>

        public bool deleteAppointment(int appointmentId)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {

                var apptRecord = context.PS_Appointment.Where(app => app.AppointId == appointmentId);
                //Find the survey aginst give appointment id. 
                //var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == appointmentId);
                //If appointment exist but survey is not saved against appointment then user can delete the appointment
                if (apptRecord.Count() > 0)
                {
                    PS_Appointment appointment = apptRecord.First();
                    context.PS_Survey.Join(context.PS_Appointment2Survey.Where(a2s => a2s.AppointId == appointmentId), s => s.SurveyId, a2s => a2s.SurveyId, (s, a2s) => s).ToList().ForEach(s => context.PS_Survey.DeleteObject(s));
                    context.PS_Appointment2Survey.Where(sur => sur.AppointId == appointmentId).ToList().ForEach(s => context.PS_Appointment2Survey.DeleteObject(s));
                    context.PS_Property2Appointment.Where(prop => prop.AppointId == appointmentId).ToList().ForEach(p => context.PS_Property2Appointment.DeleteObject(p));
                    context.PS_Appointment.DeleteObject(appointment);
                    context.SaveChanges();
                    trans.Complete();

                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region is Appointment Exists against property
        /// <summary>
        /// This function checks the property id against a valid appointment, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="appointmentType">appoinmtment type</param>
        /// <returns>returns true or false</returns>
        public bool isAppointmentExistsAgainstProperty(string propertyId, string appointmentType, string appointmentStatus)
        {
            bool success = false;
            var apptRecord = (from pte in context.PS_Property2Appointment
                              join apt in context.PS_Appointment on pte.AppointId equals apt.AppointId
                              where pte.PropertyId == propertyId && apt.AppointType.ToLower() == appointmentType.ToLower()
                              && apt.AppointProgStatus.ToLower() == appointmentStatus
                              //&& apt.AppointStartDateTime >= DateTime.Now
                              select apt);

            if (apptRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        #endregion

        #region save last survey date
        /// <summary>
        /// This function will save the last survey date
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        public void saveLastSurveyDate(int appointmentId)
        {
            var p2app = context.PS_Property2Appointment.Where(app => app.AppointId == appointmentId);
            PS_Property2Appointment property2appointment = p2app.First();
            property2appointment.LastSurveyDate = DateTime.Now;
            context.SaveChanges();
        }
        #endregion

        #region save last survey date of a fault appointment
        /// <summary>
        /// This function will save the last survey date of a fault appointment
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        public void saveLastSurveyDateFault(int appointmentId)
        {
            var apptlist = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointmentId);
            FL_CO_APPOINTMENT apptFault = apptlist.First();
            apptFault.LastActionDate = DateTime.Now;
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
        }
        #endregion

        #region get last survey date

        public DateTime? getLastSurveyDate(string propertyId)
        {
            var p2app = context.PS_Property2Appointment.Where(app => app.PropertyId == propertyId).OrderByDescending(app => app.LastSurveyDate);
            PS_Property2Appointment property2appointment = p2app.First();

            return property2appointment.LastSurveyDate;
        }
        #endregion

        #region Get All Status

        /// <summary>
        /// This method returns status from AS_Status table for gas only
        /// </summary>
        /// <returns></returns>
        public List<StatusData> getAllStatusData()
        {
            var statusData = context.AS_Status.Where(status => status.InspectionTypeID == 1);

            List<StatusData> statuslist = new List<StatusData>();

            if (statusData.Count() > 0)
            {
                List<AS_Status> ldata = statusData.ToList();
                foreach (var item in ldata)
                {
                    StatusData litem = new StatusData();
                    litem.createdBy = item.CreatedBy;
                    litem.createdDate = item.CreatedDate;
                    litem.inspectionTypeId = item.InspectionTypeID;
                    litem.isEditable = item.IsEditable;
                    litem.modifiedBy = item.ModifiedBy;
                    litem.modifiedDate = item.ModifiedDate;
                    litem.ranking = item.Ranking;
                    litem.statusId = item.StatusId;
                    litem.title = item.Title;

                    statuslist.Add(litem);
                }
            }

            return statuslist;
        }

        #endregion

        #region Get Customer List against a tenancy

        /// <summary>
        /// This method returns list of customers against a tenancy id
        /// </summary>
        /// <returns></returns>
        private List<CustomerData> getCustomersInfo(int? tenancyId, bool isVoid = false, int customerId = 0)
        {
            if (isVoid)
            {
                var custData = (from ten in context.C_TENANCY
                                join cut in context.C_CUSTOMERTENANCY on ten.TENANCYID equals cut.TENANCYID
                                join cus in context.C__CUSTOMER on cut.CUSTOMERID equals cus.CUSTOMERID
                                join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                                from gti in tempgti.DefaultIfEmpty()
                                join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                where cad.ISDEFAULT == 1 && ten.TENANCYID == tenancyId && cut.CUSTOMERID == customerId
                                select new CustomerData
                                {
                                    customerId = cus.CUSTOMERID,
                                    title = gti.DESCRIPTION,
                                    firstName = cus.FIRSTNAME,
                                    middleName = cus.MIDDLENAME,
                                    lastName = cus.LASTNAME,
                                    telephone = cad.TEL,
                                    mobile = cad.MOBILE,
                                    fax = cad.FAX,
                                    email = cad.EMAIL
                                });

                if (custData.Count() > 0)
                {
                    return custData.ToList();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var custData = (from ten in context.C_TENANCY
                                join cut in context.C_CUSTOMERTENANCY on ten.TENANCYID equals cut.TENANCYID
                                join cus in context.C__CUSTOMER on cut.CUSTOMERID equals cus.CUSTOMERID
                                join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                                from gti in tempgti.DefaultIfEmpty()
                                join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                where cad.ISDEFAULT == 1 && (cut.ENDDATE == null || cut.ENDDATE > DateTime.Now) && ten.TENANCYID == tenancyId
                                select new CustomerData
                                {
                                    customerId = cus.CUSTOMERID,
                                    title = gti.DESCRIPTION,
                                    firstName = cus.FIRSTNAME,
                                    middleName = cus.MIDDLENAME,
                                    lastName = cus.LASTNAME,
                                    telephone = cad.TEL,
                                    mobile = cad.MOBILE,
                                    fax = cad.FAX,
                                    email = cad.EMAIL
                                });

                if (custData.Count() > 0)
                {
                    return custData.ToList();
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion

        #region Set additional data for Fault Appointment

        /// <summary>
        /// This method sets additional data for Fault Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataFault(AllAppointmentsList alf)
        {
            /* Setting the Tenancy Information */

            if (alf.tenancyId != null)
            {
                alf.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alf.tenancyId == cut.TENANCYID && (cut.ENDDATE == null || cut.ENDDATE > DateTime.Now) select cut.CUSTOMERID).FirstOrDefault();
                alf.customerList = this.getCustomersInfo(alf.tenancyId);

                if (alf.customerList != null)
                {
                    for (int i = 0; i < alf.customerList.Count(); i++)
                    {
                        if (alf.customerList.ElementAt(i).customerId == alf.defaultCustomerId)
                        {
                            alf.defaultCustomerIndex = i;
                        }
                    }
                }
                else
                {
                    alf.defaultCustomerId = 0;
                    alf.defaultCustomerIndex = -1;
                }
            }
            else
            {
                alf.defaultCustomerId = 0;
                alf.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (alf.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in alf.customerList)
                {
                    int customerId = (int)custData.customerId; // Line changed  - 19/06/2013

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<AsbestosData> propAsbListData = new List<AsbestosData>();
            string propertyId = alf.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);
            List<AsbestosData> propertyAsbestosData = new List<AsbestosData>();
            for (int j = 0; j < propAsbListData.Count; j++)
            {
                AsbestosData propAsbData = new AsbestosData();
                propAsbData.asbestosId = propAsbListData[j].asbestosId;
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;
                propAsbData.type = propAsbListData[j].type;
                propAsbData.riskLevel = propAsbListData[j].riskLevel;

                //add object of property risk to  appointment data 
                propertyAsbestosData.Add(propAsbData);
            }
            alf.property.propertyAsbestosData = propertyAsbestosData;
            alf.property.propertyPicture = propDal.getAllPropertyImages(propertyId); ;

            /* Setting the Appointment Job Data Information */

            alf.jobDataList = getJobDataList(alf.appointmentId);
        }

        #endregion

        #region Set additional data for Stock Appointment
        /// <summary>
        /// This method sets additional data for Stock Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataStock(AllAppointmentsList varAppointmentListStock, int surveyId = -1)
        {
            /* Setting the Stock Appointment Specific Data Information */

            varAppointmentListStock.property.lastSurveyDate = this.getLastSurveyDate(varAppointmentListStock.property.propertyId);
            varAppointmentListStock.appInfoData = this.getTotalNoEntries(varAppointmentListStock.property.propertyId);
            varAppointmentListStock.cp12Info = null; //new IssuedReceivedByDal().getIssuedReceivedByFormData(varAppointmentListStock.appointmentId);
            //CP12 Information is not needed in stock.
            //varAppointmentListStock.CP12Info = null; new IssuedReceivedByDal().getIssuedReceivedByFormData(varAppointmentListStock.appointmentId);

            /* Setting Tenancy Data Information */
            if (varAppointmentListStock.tenancyId != null)
            {
                varAppointmentListStock.defaultCustomerId = (int)context.PS_Property2Appointment.Where(p2a => p2a.AppointId == varAppointmentListStock.appointmentId).FirstOrDefault().CustomerId;

                varAppointmentListStock.customerList = this.getCustomersInfo(varAppointmentListStock.tenancyId);

                for (int i = 0; i < varAppointmentListStock.customerList.Count(); i++)
                {
                    if (varAppointmentListStock.customerList.ElementAt(i).customerId == varAppointmentListStock.defaultCustomerId)
                    {
                        varAppointmentListStock.defaultCustomerIndex = i;
                    }
                }
            }
            else
            {
                varAppointmentListStock.defaultCustomerId = 0;
                varAppointmentListStock.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */
            if (varAppointmentListStock.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in varAppointmentListStock.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                } //End Customer Data Loop
            }// End Custome Check

            /* Setting the Property Asbestos Risk Information */
            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<AsbestosData> propAsbListData = new List<AsbestosData>();
            //List<PropertyDimData> propDimDataList = new List<PropertyDimData>();
            string propertyId = varAppointmentListStock.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);
            AsbestosData propAsbData = new AsbestosData();

            //getting property Accommodations
            varAppointmentListStock.property.Accommodations = propDal.getPropertyDimensions(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {

                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;
                propAsbData.type = propAsbListData[j].type;
                propAsbData.riskLevel = propAsbListData[j].riskLevel;

                //add object of property risk to  appointment data 
                if (varAppointmentListStock.property.propertyAsbestosData != null)
                    varAppointmentListStock.property.propertyAsbestosData.Add(propAsbData);
            }

            // in case surveyId contains default value of -1, check for survey id from database.
            if (surveyId == -1)
            {
                var appointment2Survey = context.PS_Appointment2Survey.Where(a2s => a2s.AppointId == varAppointmentListStock.appointmentId);
                if (appointment2Survey.Count() > 0) surveyId = appointment2Survey.First().SurveyId;
            }

            varAppointmentListStock.property.propertyPicture = propDal.getAllStockSurveyImages(propertyId, surveyId);
        }
        #endregion

        #region Set additional data for Gas Appointment

        /// <summary>
        /// This method sets additional data for Gas Appointment
        /// </summary>
        /// <returns></returns>
        private void getPropertySpecificDataForGas(AllAppointmentsList apt)
        {

            #region "Set Tenancy Id"

            var propTenancy = (from ct in context.C_TENANCY
                               join cct in context.C_CUSTOMERTENANCY on ct.TENANCYID equals cct.TENANCYID
                               where ct.PROPERTYID == apt.property.propertyId && ct.ENDDATE == null
                               select cct).FirstOrDefault();
            if (propTenancy != null)
            {
                apt.tenancyId = propTenancy.TENANCYID;
            }

            #endregion

            #region "Setting Tenancy Data Information"

            if (apt.tenancyId != null)
            {
                apt.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where apt.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                apt.customerList = this.getCustomersInfo(apt.tenancyId);

                if (apt.customerList != null)
                {

                    for (int i = 0; i < apt.customerList.Count(); i++)
                    {
                        if (apt.customerList.ElementAt(i).customerId == apt.defaultCustomerId)
                        {
                            apt.defaultCustomerIndex = i;
                        }
                    }
                }
                else
                {
                    apt.defaultCustomerId = 0;
                    apt.defaultCustomerIndex = -1;
                }
            }
            else
            {
                apt.defaultCustomerId = 0;
                apt.customerList = null;
            }

            #endregion

            #region "Setting the Customer Vulnerability Information"

            if (apt.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in apt.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            #endregion

            #region "Accommodations and Property Picture Info"
            PropertyDal propDal = new PropertyDal();
            string propertyId = apt.property.propertyId;
            apt.property.Accommodations = propDal.getPropertyDimensions(propertyId);
            apt.property.propertyPicture = propDal.getAllPropertyImages(propertyId);
            #endregion

        }

        #endregion

        #region Set additional data for Planned Appointment

        /// <summary>
        /// This method sets additional data for Planned Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataPlanned(AllAppointmentsList alf)
        {
            /* Setting the Tenancy Information */

            if (alf.tenancyId != null && alf.tenancyId > 0)
            {
                alf.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alf.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                alf.customerList = this.getCustomersInfo(alf.tenancyId);

                if (alf.customerList != null)
                {
                    for (int i = 0; i < alf.customerList.Count(); i++)
                    {
                        if (alf.customerList.ElementAt(i).customerId == alf.defaultCustomerId)
                        {
                            alf.defaultCustomerIndex = i;
                        }
                    }
                }
                else
                {
                    alf.defaultCustomerId = 0;
                    alf.defaultCustomerIndex = -1;
                }

            }
            else
            {
                alf.defaultCustomerId = 0;
                alf.customerList = null;
                alf.defaultCustomerIndex = -1;
            }

            /* Setting the Customer Vulnerability Information */

            if (alf.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in alf.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();

            #region "Property Asbestos"

            if (alf.property != null)
            {
                string propertyId = alf.property.propertyId;

                //get property asbestos risk
                List<AsbestosData> asbListData = new List<AsbestosData>();
                asbListData = propDal.getPropertyAsbestosRisk(propertyId);
                List<AsbestosData> propertyAsbestosData = new List<AsbestosData>();
                for (int j = 0; j < asbListData.Count; j++)
                {
                    AsbestosData asbData = new AsbestosData();
                    asbData.asbestosId = asbListData[j].asbestosId;
                    asbData.asbRiskLevelDesc = asbListData[j].asbRiskLevelDesc;
                    asbData.riskDesc = asbListData[j].riskDesc;
                    asbData.type = asbListData[j].type;
                    asbData.riskLevel = asbListData[j].riskLevel;

                    //add object of property risk to  appointment data 
                    propertyAsbestosData.Add(asbData);
                }
                alf.property.propertyAsbestosData = propertyAsbestosData;
                alf.property.propertyPicture = propDal.getAllPropertyImages(propertyId);
            }

            #endregion

            #region "Scheme/Block Asbestos"

            if (alf.scheme != null)
            {

                List<AsbestosData> asbListData = new List<AsbestosData>();

                if (alf.scheme.blockId != null && alf.scheme.blockId > 0)
                {
                    asbListData = propDal.getBlockAsbestosRisk(alf.scheme.blockId);
                }
                else if (alf.scheme.schemeId != null && alf.scheme.schemeId > 0)
                {
                    asbListData = propDal.getSchemeAsbestosRisk(alf.scheme.schemeId);
                }
                alf.scheme.schemeBlockAsbestos = asbListData;
            }

            #endregion


            /* Setting the Appointment Job Data Information */

            // alf.jobDataList = getPlannedJobDataList(alf.appointmentId);
        }

        #endregion

        #region is Property Exists
        //Code added  - 11/07/2013 - START
        /// <summary>
        /// This function checks the property id, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>        
        /// <returns>returns true or false</returns>

        public bool isPropertyExists(string propertyId)
        {
            bool success = false;
            var propertyRecord = context.P__PROPERTY.Where(pro => pro.PROPERTYID.ToLower() == propertyId.ToLower());

            if (propertyRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        //Code added  - 11/07/2013 - END
        #endregion

        #region Complete Appointment For Stock
        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForStock(AllAppointmentsList apptData)
        {
            bool success = false;
            int apointmentId = apptData.appointmentId;
            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.PS_Appointment.Where(app => app.AppointId == apointmentId);

            if (apptRecord.Count() > 0)
            {
                PS_Appointment appointment = apptRecord.First();
                if (apptData.appointmentLocation != null)
                {
                    appointment.AppointLocation = apptData.appointmentLocation;
                }
                if (apptData.appointmentNotes != null)
                {
                    appointment.AppointNotes = apptData.appointmentNotes;
                }
                if (apptData.appointmentStatus != null)
                {
                    appointment.AppointProgStatus = apptData.appointmentStatus;
                }
                //if (apptData.appointmentStartDateTime != null)
                //{
                //    appointment.AppointStartDateTime = apptData.appointmentStartDateTime;
                //}
                //if (apptData.appointmentEndDateTime != null)
                //{
                //    appointment.AppointEndDateTime = apptData.appointmentEndDateTime;
                //}
                if (apptData.appointmentTitle != null)
                {
                    appointment.AppointTitle = apptData.appointmentTitle;
                }
                if (apptData.appointmentType != null)
                {
                    appointment.AppointType = apptData.appointmentType;
                }


                if (apptData.createdBy != null)
                {
                    appointment.CreatedBy = apptData.createdBy;
                }

                if (apptData.surveyourAvailability != null)
                {
                    appointment.SurveyourStatus = apptData.surveyourAvailability;
                }
                if (apptData.surveyType != null)
                {
                    appointment.SurveyType = apptData.surveyType;
                }
                if (apptData.surveyorUserName != null)
                {
                    appointment.SurveyourUserName = apptData.surveyorUserName;
                }

                // appointment.ModifiedDate = apptData.loggedDate;
                if (apptData.surveyorAlert != null)
                {
                    appointment.AppointmentAlert = apptData.surveyorAlert;
                }
                if (apptData.addToCalendar != null)
                {
                    appointment.addToCalendar = apptData.addToCalendar;
                }
                appointment.ModifiedDate = DateTime.Now;
                appointment.ModifiedBy = apptData.updatedBy;

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                var apptPropertyRecord = context.PS_Property2Appointment.Where(prop => prop.AppointId == apointmentId);
                if (apptPropertyRecord.Count() > 0)
                {
                    PS_Property2Appointment property = apptPropertyRecord.First();

                    property.AppointId = apointmentId;
                    if ((apptData.customerList != null) & (apptData.customerList.Count > 0))
                    {

                        if (apptData.customerList[0].customerId != null)
                        {
                            property.CustomerId = (int)apptData.customerList[0].customerId;
                        }

                        if (apptData.property.propertyId != null)
                        {
                            property.PropertyId = apptData.property.propertyId;
                        }
                        if (apptData.property.tenancyId != null)
                        {
                            property.TenancyId = (int)apptData.property.tenancyId;
                        }
                    }
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
                //trans.Complete();
                success = true;
            }
            //}
            return success;
        }

        #endregion

        #region Update Appointment Status For Appliance defect
        public void updateAppointmentStatusForApplianceDefect(ref SyncAppointmentStatusResponse response, AppointmentInProgress appApt, int userId)
        {
            bool isSuccessFull = false;
            string message = string.Empty;

            try
            {

                PDR_APPOINTMENTS dbApt = context.PDR_APPOINTMENTS
                                                .Where(x => x.APPOINTMENTID == appApt.appointmentId)
                                                .FirstOrDefault();
                if (dbApt != null)
                {

                    var defectApt = context.P_PROPERTY_APPLIANCE_DEFECTS
                                           .Where(x => x.ApplianceDefectAppointmentJournalId == dbApt.JOURNALID)
                                           .FirstOrDefault();

                    if (defectApt != null
                        && dbApt.APPOINTMENTSTATUS.ToLower() != AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        )
                    {

                        PDR_APPOINTMENT_HISTORY history = context.PDR_APPOINTMENT_HISTORY
                                                                 .Where(x => x.APPOINTMENTID == dbApt.APPOINTMENTID)
                                                                 .OrderByDescending(x => x.APPOINTMENTHISTORYID)
                                                                 .FirstOrDefault();
                        if (appApt.status != history.APPOINTMENTSTATUS)
                        {
                            dbApt.APPOINTMENTSTATUS = appApt.status;
                            DateTime previousTime = Convert.ToDateTime(history.LOGGEDDATE);
                            DateTime newTime = previousTime.AddSeconds(10);
                            dbApt.LOGGEDDATE = newTime;

                            if (appApt.status == ApplicationConstants.AcceptedAppointmentStatus)
                            {
                                var apptJobSheets = (from dft in context.P_PROPERTY_APPLIANCE_DEFECTS
                                                     where dft.ApplianceDefectAppointmentJournalId == history.JOURNALID
                                                     select dft).ToList();
                                foreach (var jobsheet in apptJobSheets)
                                {
                                    jobsheet.DefectJobSheetStatus = GetPdrStatusId(appApt.status);
                                    context.SaveChanges();
                                }
                            }
                        }

                        if (appApt.jobSheets != null || appApt.jobSheets.Count > 0)
                        {
                            foreach (JobSheetStatus jobSheet in appApt.jobSheets)
                            {
                                int? currentJobSheetStatusId = GetPdrStatusId(jobSheet.status);
                                int propertyDefectId = Convert.ToInt32(jobSheet.jobSheetNumber.Substring(3));
                                var previousJobsheetStatusId = context.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY
                                                                .Where(x => x.PropertyDefectId == propertyDefectId)
                                                                .OrderByDescending(y => y.CreatedDate)
                                                                .Select(x => x.DefectJobSheetStatus)
                                                                .FirstOrDefault();

                                if (previousJobsheetStatusId != currentJobSheetStatusId)
                                {

                                    P_PROPERTY_APPLIANCE_DEFECTS appt = context.P_PROPERTY_APPLIANCE_DEFECTS
                                                                               .AsEnumerable()
                                                                               .Where(x => x.PropertyDefectId == propertyDefectId)
                                                                               .FirstOrDefault();
                                    if (appt != null)
                                    {
                                        appt.DefectJobSheetStatus = GetPdrStatusId(jobSheet.status);
                                        context.SaveChanges();

                                        //save in Defect_Pause when status is "Paused"
                                        if (jobSheet.status.ToLower() == AppointmentCompleteStatus.Paused.ToString().ToLower())
                                        {
                                            int? defectHistoryId = null;
                                            int? pauseID = null;
                                            var appointmentHistory = context.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.Where(e => e.PropertyDefectId == propertyDefectId).OrderByDescending(x => x.DefectHistoryId).FirstOrDefault();
                                            if (appointmentHistory != null)
                                            {
                                                defectHistoryId = appointmentHistory.DefectHistoryId;
                                            }
                                            var pauseData = new ApplianceDefectPauseData();
                                            pauseData.pauseNote = jobSheet.notes;
                                            pauseData.pauseReason = jobSheet.reason;
                                            pauseData.pausedBy = userId;
                                            pauseData.actionType = jobSheet.status;
                                            pauseData.pauseDate = DateTime.Now;
                                            pauseID = savedefecdPaused(pauseData, defectHistoryId);
                                        }
                                    }
                                }
                            }
                        }

                        context.SaveChanges();
                        isSuccessFull = true;
                    }
                    else
                    {
                        isSuccessFull = false;
                        message = MessageConstants.AppointmentRescheduledErrorMsg;
                    }
                }
                else
                {
                    isSuccessFull = false;
                    message = MessageConstants.AppointmentIdInvalidMsg;
                }

            }
            catch (Exception ex)
            {
                message = MessageConstants.ProblemUpdatingAppointmentStatusMsg;
                isSuccessFull = false;
            }
            finally
            {
                if (isSuccessFull)
                {
                    response.syncedAppointments.Add(new SyncedAppointment() { appointmentId = appApt.appointmentId });
                }
                else
                {
                    response.failedAppointments.Add(new FailedAppointment() { appointmentId = appApt.appointmentId, reason = message });
                }
            }

        }
        #endregion

        #region Get PDR Status Id
        public int? GetPdrStatusId(string status)
        {
            int? statusId = null;
            var statusResult = context.PDR_STATUS
                                .Where(x => x.AppTitle == status)
                                .FirstOrDefault();
            if (statusResult != null)
            {
                statusId = statusResult.STATUSID;
            }

            return statusId;

        }
        #endregion

        #region Update Appointment Status For Void Appointments
        public void updateAppointmentStatusForVoid(ref SyncAppointmentStatusResponse response, AppointmentInProgress appApt, int userId)
        {
            bool isSuccessFull = false;
            string message = string.Empty;

            try
            {

                PDR_APPOINTMENTS dbApt = context.PDR_APPOINTMENTS
                                                .Where(x => x.APPOINTMENTID == appApt.appointmentId)
                                                .FirstOrDefault();
                if (dbApt != null)
                {
                    DateTime dbAptStartDateTime = DateTime.Parse(((DateTime)dbApt.APPOINTMENTSTARTDATE).ToString("dd/MM/yyyy") + " " + dbApt.APPOINTMENTSTARTTIME);
                    DateTime dbAptEndDateTime = DateTime.Parse(((DateTime)dbApt.APPOINTMENTENDDATE).ToString("dd/MM/yyyy") + " " + dbApt.APPOINTMENTENDTIME);

                    if (dbApt.ASSIGNEDTO == userId
                        && DateTime.Compare(dbAptStartDateTime, (DateTime)appApt.appointmentStartDateTime) == 0
                        && DateTime.Compare(dbAptEndDateTime, (DateTime)appApt.appointmentEndDateTime) == 0
                        && dbApt.APPOINTMENTSTATUS.ToLower() != AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        )
                    {
                        PDR_APPOINTMENT_HISTORY history = context.PDR_APPOINTMENT_HISTORY.Where(x => x.APPOINTMENTID == dbApt.APPOINTMENTID).OrderByDescending(x => x.APPOINTMENTHISTORYID).FirstOrDefault();
                        if (appApt.status != history.APPOINTMENTSTATUS)
                        {
                            dbApt.APPOINTMENTSTATUS = appApt.status;
                            DateTime previousTime = Convert.ToDateTime(history.LOGGEDDATE);
                            DateTime newTime = previousTime.AddSeconds(10);
                            dbApt.LOGGEDDATE = newTime;
                            if (appApt.status == ApplicationConstants.AcceptedAppointmentStatus)
                            {
                                var apptJobSheets = (from fapt in context.PDR_APPOINTMENTS
                                                     join fltg in context.V_RequiredWorks on fapt.JOURNALID equals fltg.WorksJournalId
                                                     where fapt.APPOINTMENTID == appApt.appointmentId
                                                     select fltg
                                                                  ).ToList();
                                foreach (var jobsheet in apptJobSheets)
                                {
                                    jobsheet.StatusId = GetVoidWorksStatusId(appApt.status);
                                    jobsheet.ModifiedDate = DateTime.Now;
                                    context.SaveChanges();
                                }

                            }
                            context.SaveChanges();
                            isSuccessFull = true;
                        }
                        else
                        {
                            isSuccessFull = false;
                            message = MessageConstants.AppointmentRescheduledErrorMsg;
                        }

                        if (appApt.jobSheets != null || appApt.jobSheets.Count > 0)
                        {
                            if (appApt.appointmentType == ApplicationConstants.VoidWorksAppointmentType)
                            {
                                foreach (JobSheetStatus jobSheet in appApt.jobSheets)
                                {
                                    int? faultlogid = Convert.ToInt32(jobSheet.jobSheetNumber.Substring(3));
                                    int currentJobSheetStatusId = GetVoidWorksStatusId(jobSheet.status);
                                    var previousJobsheetStatusId = context.V_RequiredWorksHistory
                                                                    .Where(a => a.RequiredWorksId == faultlogid)
                                                                    .OrderByDescending(y => y.CreatedDate)
                                                                    .Select(x => x.StatusId)
                                                                    .FirstOrDefault();

                                    if (previousJobsheetStatusId != currentJobSheetStatusId)
                                    {
                                        V_RequiredWorks apptJobSheet = context.V_RequiredWorks
                                                                  .AsEnumerable()
                                                                  .Where(x => string.Format("JSV{0}", x.RequiredWorksId) == jobSheet.jobSheetNumber)
                                                                  .FirstOrDefault();
                                        if (apptJobSheet != null)
                                        {
                                            apptJobSheet.StatusId = GetVoidWorksStatusId(jobSheet.status);
                                            apptJobSheet.ModifiedDate = DateTime.Now;
                                            context.SaveChanges();

                                            //save in Planned_Pause when status is "Paused"
                                            if (appApt.status.ToLower() == AppointmentCompleteStatus.InProgress.ToString().ToLower() && faultlogid.HasValue)
                                            {
                                                logActualStartAndEndDateTimeForVoidWorksAppointment(appApt.appointmentType, appApt.appointmentId, faultlogid.Value, dbApt.APPOINTMENTSTARTDATE, jobSheet.status, null);
                                            }
                                            if (jobSheet.status.ToLower() == AppointmentCompleteStatus.Paused.ToString().ToLower())
                                            {
                                                int? pauseID = null;

                                                var jobPauseData = new VoidPauseData();
                                                jobPauseData.pauseNote = jobSheet.notes;
                                                jobPauseData.pauseReason = jobSheet.reason;
                                                jobPauseData.pausedBy = userId;
                                                jobPauseData.actionType = jobSheet.status;
                                                jobPauseData.pauseDate = DateTime.Now;

                                                if (faultlogid.HasValue)
                                                {
                                                    pauseID = pausedVoidAppointment(jobPauseData, faultlogid.Value);
                                                    // Insert update a record in Planned Job Sheet
                                                    logActualStartAndEndDateTimeForVoidWorksAppointment(appApt.appointmentType, appApt.appointmentId, faultlogid.Value, DateTime.Now, jobSheet.status, pauseID);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        context.SaveChanges();
                        isSuccessFull = true;

                    }
                    else
                    {
                        isSuccessFull = false;
                        message = MessageConstants.AppointmentIdInvalidMsg;
                    }

                }
            }
            catch (Exception ex)
            {
                message = MessageConstants.ProblemUpdatingAppointmentStatusMsg;
                isSuccessFull = false;
            }
            finally
            {
                if (isSuccessFull)
                {
                    response.syncedAppointments.Add(new SyncedAppointment() { appointmentId = appApt.appointmentId });
                }
                else
                {
                    response.failedAppointments.Add(new FailedAppointment() { appointmentId = appApt.appointmentId, reason = message });
                }
            }

        }
        #endregion

        #region Update Appointment Status For Planned Appointment
        public void updateAppointmentStatusForPlanned(ref SyncAppointmentStatusResponse response, AppointmentInProgress appApt, int userId)
        {
            bool isSuccessFull = false;
            string message = string.Empty;

            try
            {

                PLANNED_APPOINTMENTS dbApt = context.PLANNED_APPOINTMENTS
                                                    .Where(x => x.APPOINTMENTID == appApt.appointmentId)
                                                    .FirstOrDefault();
                if (dbApt != null)
                {
                    DateTime dbAptStartDateTime = DateTime.Parse(((DateTime)dbApt.APPOINTMENTDATE).ToString("dd/MM/yyyy") + " " + dbApt.APPOINTMENTSTARTTIME);
                    DateTime dbAptEndDateTime = DateTime.Parse(((DateTime)dbApt.APPOINTMENTENDDATE).ToString("dd/MM/yyyy") + " " + dbApt.APPOINTMENTENDTIME);

                    if (dbApt.ASSIGNEDTO == userId
                        && DateTime.Compare(dbAptStartDateTime, (DateTime)appApt.appointmentStartDateTime) == 0
                        && DateTime.Compare(dbAptEndDateTime, (DateTime)appApt.appointmentEndDateTime) == 0
                        && dbApt.APPOINTMENTSTATUS.ToLower() != AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        )
                    {
                        JobSheetStatus jobSheet = appApt.jobSheets.FirstOrDefault();

                        int substatus = 0, previousSub;
                        bool previousSubStatus = false;
                        if (appApt.jobSheets != null && appApt.jobSheets.Count > 0)
                        {
                            substatus = GetPlannedSubStatusID(jobSheet.status);
                            previousSub = GetPlannedSubStatusID("Paused");
                            previousSubStatus = previousSub == dbApt.JOURNALSUBSTATUS ? true : false;
                            dbApt.JOURNALSUBSTATUS = substatus > 0 ? (int?)substatus : null;
                        }
                        else
                        {
                            substatus = GetPlannedSubStatusID(appApt.status);
                        }


                        dbApt.LOGGEDDATE = DateTime.Now;



                        PLANNED_APPOINTMENTS_HISTORY history = context.PLANNED_APPOINTMENTS_HISTORY.Where(y => y.APPOINTMENTID == appApt.appointmentId).OrderByDescending(x => x.APPOINTMENTHISTORYID).FirstOrDefault();
                        if (appApt.status != history.APPOINTMENTSTATUS || jobSheet.status != history.APPOINTMENTSTATUS || previousSubStatus)
                        {

                            dbApt.APPOINTMENTSTATUS = appApt.status;
                            dbApt.JOURNALSUBSTATUS = substatus > 0 ? (int?)substatus : null;
                            context.SaveChanges();


                            if (jobSheet != null)
                            {
                                //save in Planned_Pause when status is "Paused"
                                if (appApt.status.ToLower() == AppointmentCompleteStatus.InProgress.ToString().ToLower())
                                {
                                    insertUpdatePlannedJobTimeSheet(appApt.appointmentId, appApt.appointmentStartDateTime, jobSheet.status, null);
                                }
                                if (jobSheet.status.ToLower() == AppointmentCompleteStatus.Paused.ToString().ToLower())
                                {
                                    int? appointmentHistoryID = null;
                                    int? pauseID = null;
                                    var appointmentHistory = context.PLANNED_APPOINTMENTS_HISTORY.Where(e => e.APPOINTMENTID == appApt.appointmentId).OrderByDescending(x => x.APPOINTMENTHISTORYID).FirstOrDefault();
                                    if (appointmentHistory != null)
                                    {
                                        appointmentHistoryID = appointmentHistory.APPOINTMENTHISTORYID;
                                    }
                                    var jobPauseData = new JobPauseHistoryData();
                                    jobPauseData.pauseNote = jobSheet.notes;
                                    jobPauseData.pauseReason = jobSheet.reason;
                                    jobPauseData.pausedBy = userId;
                                    jobPauseData.actionType = jobSheet.status;
                                    jobPauseData.pauseDate = DateTime.Now;
                                    pauseID = savePlannedPaused(jobPauseData, appApt.appointmentId, appointmentHistoryID);

                                    // Insert update a record in Planned Job Sheet

                                    insertUpdatePlannedJobTimeSheet(appApt.appointmentId, appApt.appointmentStartDateTime, jobPauseData.actionType, pauseID);
                                }
                            }
                        }





                        isSuccessFull = true;

                    }
                    else
                    {
                        isSuccessFull = false;
                        message = MessageConstants.AppointmentRescheduledErrorMsg;
                    }

                }
                else
                {

                    isSuccessFull = false;
                    message = MessageConstants.AppointmentIdInvalidMsg;
                }

            }
            catch (Exception ex)
            {
                message = MessageConstants.ProblemUpdatingAppointmentStatusMsg;
                isSuccessFull = false;
            }
            finally
            {
                if (isSuccessFull)
                {
                    response.syncedAppointments.Add(new SyncedAppointment() { appointmentId = appApt.appointmentId });
                }
                else
                {
                    response.failedAppointments.Add(new FailedAppointment() { appointmentId = appApt.appointmentId, reason = message });
                }
            }

        }
        #endregion

        #region Update Appointment Status For Fault Appointment
        public void updateAppointmentStatusForFault(ref SyncAppointmentStatusResponse response, AppointmentInProgress appApt, int userId)
        {
            bool isSuccessFull = false;
            string message = string.Empty;

            try
            {

                FL_CO_APPOINTMENT dbApt = context.FL_CO_APPOINTMENT
                                                 .Where(x => x.AppointmentID == appApt.appointmentId)
                                                 .FirstOrDefault();
                if (dbApt != null)
                {
                    DateTime aptStartDate = (DateTime)dbApt.AppointmentDate;
                    DateTime aptEndDate = (DateTime)(dbApt.AppointmentEndDate ?? dbApt.AppointmentDate);

                    DateTime dbAptStartDateTime = DateTime.Parse((aptStartDate).ToString("dd/MM/yyyy") + " " + dbApt.Time);
                    DateTime dbAptEndDateTime = DateTime.Parse((aptEndDate).ToString("dd/MM/yyyy") + " " + dbApt.EndTime);

                    if (dbApt.OperativeID == userId
                        && DateTime.Compare(dbAptStartDateTime, (DateTime)appApt.appointmentStartDateTime) == 0
                        && DateTime.Compare(dbAptEndDateTime, (DateTime)appApt.appointmentEndDateTime) == 0
                        && dbApt.AppointmentStatus.ToLower() != AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        )
                    {

                        if (dbApt.AppointmentStatus != appApt.status)
                        {

                            dbApt.AppointmentStatus = appApt.status;

                            // Set 'Accepted' status to all jobsheets attached with appointmentid
                            if (appApt.status == ApplicationConstants.AcceptedAppointmentStatus)
                            {
                                var apptJobSheets = (from fapt in context.FL_FAULT_APPOINTMENT
                                                     join fltg in context.FL_FAULT_LOG on fapt.FaultLogId equals fltg.FaultLogID
                                                     where fapt.AppointmentId == appApt.appointmentId
                                                     select fltg
                                                                  ).ToList();
                                foreach (var jobsheet in apptJobSheets)
                                {
                                    jobsheet.StatusID = GetFaultStatusId(appApt.status);
                                    saveFaultLogHistoryData(context, jobsheet, userId, DateTime.Now);
                                }

                            }
                        }

                        if (appApt.jobSheets != null || appApt.jobSheets.Count > 0)
                        {

                            foreach (JobSheetStatus jobSheet in appApt.jobSheets)
                            {
                                if (jobSheet.status == "InProgress")
                                {
                                    jobSheet.status = "In Progress";
                                }

                                int? faultlogid = Convert.ToInt32(jobSheet.jobSheetNumber.Substring(2));
                                int currentJobSheetStatusId = GetFaultStatusId(jobSheet.status);
                                var previousJobsheetStatusId = context.FL_FAULT_LOG_HISTORY
                                                                    .Where(a => a.FaultLogID == faultlogid)
                                                                    .OrderByDescending(y => y.LastActionDate)
                                                                    .Select(x => x.FaultStatusID)
                                                                    .FirstOrDefault();
                                if (previousJobsheetStatusId != currentJobSheetStatusId)
                                {
                                    FL_FAULT_LOG apptJobSheet = context.FL_FAULT_LOG
                                                                       .Where(x => x.JobSheetNumber == jobSheet.jobSheetNumber)
                                                                       .FirstOrDefault();
                                    if (apptJobSheet != null)
                                    {
                                        int faultStatusId = GetFaultStatusId(jobSheet.status);
                                        apptJobSheet.StatusID = faultStatusId;
                                        int historyId = (int)saveFaultLogHistoryData(context, apptJobSheet, userId, DateTime.Now);
                                        if (jobSheet.status == FaultJobProgressStatus.Paused.ToString())
                                        {
                                            saveFaultPaused(new JobPauseHistoryData { actionType = FaultJobProgressStatus.Paused.ToString(), pauseDate = DateTime.Now, pausedBy = userId, pauseNote = jobSheet.notes, pauseReason = jobSheet.reason }, Convert.ToInt32(jobSheet.jobSheetNumber.Substring(2)), historyId);
                                        }
                                    }
                                }

                            }
                        }

                        context.SaveChanges();
                        // insert record in fault log history


                        isSuccessFull = true;

                    }
                    else
                    {
                        isSuccessFull = false;
                        message = MessageConstants.AppointmentRescheduledErrorMsg;
                    }

                }
                else
                {
                    isSuccessFull = false;
                    message = MessageConstants.AppointmentIdInvalidMsg;
                }

            }
            catch (Exception ex)
            {
                message = MessageConstants.ProblemUpdatingAppointmentStatusMsg;
                isSuccessFull = false;
            }
            finally
            {
                if (isSuccessFull)
                {
                    response.syncedAppointments.Add(new SyncedAppointment() { appointmentId = appApt.appointmentId });
                }
                else
                {
                    response.failedAppointments.Add(new FailedAppointment() { appointmentId = appApt.appointmentId, reason = message });
                }
            }

        }
        #endregion

        #region Get Fault Status Id
        public int GetFaultStatusId(string status)
        {
            return context.FL_FAULT_STATUS
                    .Where(x => x.Description == status)
                    .FirstOrDefault().FaultStatusID;
        }
        #endregion

        #region Get Void Works Status Id
        public int GetVoidWorksStatusId(string status)
        {
            return context.PDR_STATUS
                    .Where(x => x.AppTitle == status)
                    .FirstOrDefault().STATUSID;
        }
        #endregion

        #region Update Appointment Status For Gas Appointment
        public void updateAppointmentStatusForGas(ref SyncAppointmentStatusResponse response, AppointmentInProgress appApt, int userId)
        {
            bool isSuccessFull = false;
            string message = string.Empty;

            try
            {

                AS_APPOINTMENTS dbApt = context.AS_APPOINTMENTS
                                              .Where(x => x.APPOINTMENTID == appApt.appointmentId)
                                              .FirstOrDefault();
                if (dbApt != null)
                {
                    DateTime startdt = (DateTime)dbApt.APPOINTMENTDATE;
                    DateTime enddt = (DateTime)dbApt.APPOINTMENTENDDATE;

                    DateTime dbAptStartDateTime = DateTime.Parse(startdt.ToString("dd/MM/yyyy") + " " + dbApt.APPOINTMENTSTARTTIME);
                    DateTime dbAptEndDateTime = DateTime.Parse(enddt.ToString("dd/MM/yyyy") + " " + dbApt.APPOINTMENTENDTIME);

                    if (dbApt.ASSIGNEDTO == userId
                        && DateTime.Compare(dbAptStartDateTime, (DateTime)appApt.appointmentStartDateTime) == 0
                        && DateTime.Compare(dbAptEndDateTime, (DateTime)appApt.appointmentEndDateTime) == 0
                        && dbApt.APPOINTMENTSTATUS.ToLower() != AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        )
                    {

                        dbApt.APPOINTMENTSTATUS = appApt.status;

                        #region Insertion in AS_APPOINTMENTSHISTORY

                        AS_APPOINTMENTSHISTORY history = context.AS_APPOINTMENTSHISTORY
                                              .Where(x => x.APPOINTMENTID == appApt.appointmentId).OrderByDescending(x => x.APPOINTMENTHISTORYID)
                                              .FirstOrDefault();
                        if (history != null)
                        {
                            if (history.APPOINTMENTSTATUS == appApt.status)
                            {
                                return;
                            }
                        }

                        DateTime previousTime = (DateTime)history.LOGGEDDATE;
                        previousTime = previousTime.AddMinutes(1);



                        AS_APPOINTMENTSHISTORY apphis = new AS_APPOINTMENTSHISTORY();
                        apphis.APPOINTMENTALERT = dbApt.APPOINTMENTALERT;
                        apphis.APPOINTMENTCALENDER = dbApt.APPOINTMENTCALENDER;
                        apphis.APPOINTMENTDATE = dbApt.APPOINTMENTDATE;
                        apphis.APPOINTMENTENDTIME = dbApt.APPOINTMENTENDTIME;
                        apphis.APPOINTMENTSTARTTIME = dbApt.APPOINTMENTSTARTTIME;
                        apphis.APPOINTMENTSTATUS = dbApt.APPOINTMENTSTATUS;
                        apphis.ASSIGNEDTO = dbApt.ASSIGNEDTO;
                        apphis.CREATEDBY = dbApt.CREATEDBY;
                        apphis.JournalId = dbApt.JournalId;
                        apphis.JSGNUMBER = dbApt.JSGNUMBER.ToString();
                        apphis.APPOINTMENTID = dbApt.APPOINTMENTID;
                        apphis.APPOINTMENTSHIFT = dbApt.APPOINTMENTSHIFT;
                        apphis.JOURNALHISTORYID = dbApt.JOURNALHISTORYID;
                        apphis.EmailStatusId = dbApt.EmailStatusId;
                        apphis.LOGGEDDATE = DateTime.Now;
                        apphis.NOTES = dbApt.NOTES;
                        apphis.TENANCYID = dbApt.TENANCYID;
                        apphis.SURVEYOURSTATUS = dbApt.SURVEYOURSTATUS;
                        apphis.IsVoid = dbApt.IsVoid;
                        context.AddToAS_APPOINTMENTSHISTORY(apphis);

                        #endregion

                        context.SaveChanges();
                        isSuccessFull = true;

                    }
                    else
                    {
                        isSuccessFull = false;
                        message = MessageConstants.AppointmentRescheduledErrorMsg;
                    }
                }
                else
                {
                    isSuccessFull = false;
                    message = MessageConstants.AppointmentIdInvalidMsg;
                }
            }
            catch (Exception ex)
            {
                message = MessageConstants.ProblemUpdatingAppointmentStatusMsg;
                isSuccessFull = false;
                throw;
            }
            finally
            {
                if (isSuccessFull)
                {
                    response.syncedAppointments.Add(new SyncedAppointment() { appointmentId = appApt.appointmentId });
                }
                else
                {
                    response.failedAppointments.Add(new FailedAppointment() { appointmentId = appApt.appointmentId, reason = message });
                }
            }

        }
        #endregion


        #region Complete Appointment Data For Appliance Servicing
        /// <summary>
        /// Complete Appointment Data For Appliance Servicing
        /// </summary>
        /// <param name="apptData"></param>
        /// <returns></returns>
        public bool completeAppointmentDataForApplianceServicing(AllAppointmentsList apptData)
        {
            bool success = false;
            int apointmentId = apptData.appointmentId;

            var apptRecord = context.AS_APPOINTMENTS.Where(app => app.APPOINTMENTID == apointmentId);

            if (apptRecord.Count() > 0)
            {
                #region "Appointment Info"

                AS_APPOINTMENTS appointment = apptRecord.First();
                if (apptData.tenancyId != null)
                {
                    appointment.TENANCYID = apptData.tenancyId;
                }
                if (apptData.jsgNumber != null)
                {
                    appointment.JSGNUMBER = apptData.jsgNumber;
                }

                appointment.JournalId = (int)apptData.journalId;

                if (apptData.journalHistoryId != null)
                {
                    appointment.JOURNALHISTORYID = (long)apptData.journalHistoryId;
                }

                if (apptData.appointmentShift != null)
                {
                    appointment.APPOINTMENTSHIFT = apptData.appointmentShift;
                }

                if (apptData.assignedTo != null)
                {
                    appointment.ASSIGNEDTO = (int)apptData.assignedTo;
                }

                if (apptData.createdBy != null)
                {
                    appointment.CREATEDBY = apptData.createdBy;
                }

                if (apptData.appointmentCalendar != null)
                {
                    appointment.APPOINTMENTCALENDER = apptData.appointmentCalendar;
                }
                if (apptData.appointmentStatus != null)
                {
                    appointment.APPOINTMENTSTATUS = apptData.appointmentStatus;
                }

                if (apptData.surveyorAlert != null)
                {
                    appointment.APPOINTMENTALERT = apptData.surveyorAlert;
                }
                if (apptData.surveyType != null)
                {
                    appointment.SURVEYTYPE = apptData.surveyType;
                }
                if (apptData.addToCalendar != null)
                {
                    appointment.addToCalendar = apptData.addToCalendar;
                }

                #endregion

                #region "Update Appointment info related to App version"
                if (apptData.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                {


                    appointment.APPOINTMENTSTATUS = apptData.appointmentStatus;

                    if (apptData.loggedDate != null)
                    {
                        appointment.AppointmentCompletionDate = apptData.loggedDate;
                    }

                    if (apptData.appointmentCompletedAppVersion != null)
                    {
                        appointment.AppointmentCompletedAppVersion = apptData.appointmentCompletedAppVersion;
                    }

                    if (apptData.appointmentCurrentAppVersion != null)
                    {
                        appointment.AppointmentCurrentAppVersion = apptData.appointmentCurrentAppVersion;
                    }

                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #region "Delete appointment history"

                // Delete jobsheet history having statuses 'Accepted', 'Paused', 'Inprogress' inorder to avoid duplication in case of offline syncing
                List<string> historyToBeDeleted = new List<string>() {AppointmentCompleteStatus.Accepted.ToString(),
                                        AppointmentCompleteStatus.InProgress.ToString()};

                var deleteAppointmentHistoryList = (from flh in context.AS_APPOINTMENTSHISTORY
                                                    where historyToBeDeleted.Contains(flh.APPOINTMENTSTATUS.Replace(" ", "")) && flh.APPOINTMENTID == appointment.APPOINTMENTID
                                                    select flh).ToList();

                deleteAppointmentHistoryList.ForEach(context.DeleteObject);
                context.SaveChanges();

                #endregion

                #region "Insertion in appointment history"

                if (apptData.appointmentHistory != null)
                {
                    foreach (var history in apptData.appointmentHistory)
                    {
                        addApplianceAppointmentHistory(appointment, history.actionDate, history.actionType);
                    }
                }

                addApplianceAppointmentHistory(appointment, apptData.loggedDate, appointment.APPOINTMENTSTATUS);

                #endregion

                success = true;
            }


            return success;
        }

        #endregion

        #region "Add appliance appointment history"

        public void addApplianceAppointmentHistory(AS_APPOINTMENTS appointment, DateTime? loggedDate, string status)
        {

            #region "Insert into Appointment history"

            //Insert into Appointment History
            AS_APPOINTMENTSHISTORY apphis = new AS_APPOINTMENTSHISTORY()
            {
                APPOINTMENTALERT = appointment.APPOINTMENTALERT,
                APPOINTMENTCALENDER = appointment.APPOINTMENTCALENDER,
                APPOINTMENTDATE = appointment.APPOINTMENTDATE,
                APPOINTMENTENDDATE = appointment.APPOINTMENTENDDATE,
                APPOINTMENTENDTIME = appointment.APPOINTMENTENDTIME,
                APPOINTMENTSTARTTIME = appointment.APPOINTMENTSTARTTIME,
                APPOINTMENTSTATUS = status,
                ASSIGNEDTO = appointment.ASSIGNEDTO,
                CREATEDBY = appointment.CREATEDBY,
                JournalId = appointment.JournalId,
                JSGNUMBER = appointment.JSGNUMBER.ToString(),
                APPOINTMENTID = appointment.APPOINTMENTID,
                APPOINTMENTSHIFT = appointment.APPOINTMENTSHIFT,
                JOURNALHISTORYID = appointment.JOURNALHISTORYID,
                LOGGEDDATE = loggedDate,
                NOTES = appointment.NOTES,
                TENANCYID = appointment.TENANCYID,
                EmailStatusId = appointment.EmailStatusId,
                SURVEYOURSTATUS = appointment.SURVEYOURSTATUS,
                IsVoid = appointment.IsVoid
            };

            context.AddToAS_APPOINTMENTSHISTORY(apphis);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            #endregion

        }

        #endregion

        #region Log Actual Start And EndDate Time For Gas Appointment
        public bool logActualStartAndEndDateTimeForGasAppointment(string appointmentType, int appointmentId, DateTime startDateTime, DateTime endDatetime)
        {
            // Currently there is no pause feature available in Appliance related appointments
            // When that feature will be added in the future then multiple day logging can be recorded
            // for Timesheet analysis report. This will be new a feature.

            AS_JOBTIMESHEET asTimeSheet = new AS_JOBTIMESHEET();
            asTimeSheet.AppointmentId = appointmentId;
            asTimeSheet.StartTime = startDateTime;
            asTimeSheet.EndTime = endDatetime;
            context.AddToAS_JOBTIMESHEET(asTimeSheet);
            try
            {
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Log Actual Start And EndDate Time For Oil Appointment
        public bool logActualStartAndEndDateTimeForOilAppointment(string appointmentType, int appointmentId, DateTime startDateTime, DateTime endDatetime)
        {
            // Currently there is no pause feature available in Appliance related appointments
            // When that feature will be added in the future then multiple day logging can be recorded
            // for Timesheet analysis report. This will be new a feature.

            AS_JOBTIMESHEET asTimeSheet = new AS_JOBTIMESHEET();
            asTimeSheet.AppointmentId = appointmentId;
            asTimeSheet.StartTime = startDateTime;
            asTimeSheet.EndTime = endDatetime;
            context.AddToAS_JOBTIMESHEET(asTimeSheet);
            try
            {
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Log Actual Start And EndDate Time For Alternative Appointment
        public bool logActualStartAndEndDateTimeForAlternativeAppointment(string appointmentType, int appointmentId, DateTime startDateTime, DateTime endDatetime)
        {
            // Currently there is no pause feature available in Appliance related appointments
            // When that feature will be added in the future then multiple day logging can be recorded
            // for Timesheet analysis report. This will be new a feature.

            AS_JOBTIMESHEET asTimeSheet = new AS_JOBTIMESHEET();
            asTimeSheet.AppointmentId = appointmentId;
            asTimeSheet.StartTime = startDateTime;
            asTimeSheet.EndTime = endDatetime;
            context.AddToAS_JOBTIMESHEET(asTimeSheet);
            try
            {
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region Update Certificate Info
        public void updateLgsrData(AllAppointmentsList apptData)
        {

            if (apptData.cp12Info != null)
            {
                List<LGSRData> lgsrList = apptData.cp12Info;
                foreach (var certificateInfo in lgsrList)
                {
                    // This section contains legacy implement for saving P_LGSR & P_LGSR_HISTORY
                    // In #10828 only schemeId, blockId and heatingId have been added.                

                    #region "Save in P_LGSR"

                    var lgsrRecord = context.P_LGSR.Where(lgsrData => lgsrData.HeatingMappingId == certificateInfo.heatingId);
                    bool isRecordFound = false;

                    P_LGSR lgsr = new P_LGSR();
                    if (lgsrRecord.Count() > 0)
                    {
                        lgsr = lgsrRecord.First();
                        isRecordFound = true;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(certificateInfo.propertyId))
                        {
                            var propertyLgsr = context.P_LGSR.Where(lgsrData => lgsrData.PROPERTYID == certificateInfo.propertyId && lgsrData.HeatingMappingId == null);
                            if (propertyLgsr.Count() > 0)
                            {
                                lgsr = propertyLgsr.First();
                                isRecordFound = true;
                            }
                        }
                    }


                    if (certificateInfo.IssuedBy != null)
                    {
                        lgsr.CP12ISSUEDBY = certificateInfo.IssuedBy;
                    }
                    if (apptData.updatedBy != null)
                    {
                        lgsr.CP12ISSUEDBY = apptData.updatedBy;
                    }
                    if (certificateInfo.IssuedDate != null)
                    {
                        lgsr.ISSUEDATE = certificateInfo.IssuedDate;
                    }
                    if (certificateInfo.CP12Number != null)
                    {
                        lgsr.CP12NUMBER = certificateInfo.CP12Number;
                    }
                    lgsr.DOCUMENTTYPE = "pdf";
                    if (certificateInfo.DocumentType != null)
                    {
                        lgsr.DOCUMENTTYPE = certificateInfo.DocumentType;
                    }

                    lgsr.DTIMESTAMP = certificateInfo.DTimeStamp;

                    if (certificateInfo.InspectionCarried != null)
                    {
                        lgsr.INSPECTIONCARRIED = certificateInfo.InspectionCarried;
                    }
                    if (certificateInfo.Notes != null)
                    {
                        lgsr.NOTES = certificateInfo.Notes;
                    }
                    if (certificateInfo.ReceivedDate != null)
                    {
                        lgsr.RECEVIEDDATE = certificateInfo.ReceivedDate;
                    }
                    if (certificateInfo.ReceivedOnBehalfOf != null)
                    {
                        lgsr.RECEVIEDONBEHALF = certificateInfo.ReceivedOnBehalfOf;
                    }
                    if (certificateInfo.TenantHanded != null)
                    {
                        lgsr.TENANTHANDED = certificateInfo.TenantHanded;
                    }

                    lgsr.PROPERTYID = certificateInfo.propertyId;
                    lgsr.SchemeId = certificateInfo.schemeId;
                    lgsr.BlockId = certificateInfo.blockId;
                    lgsr.HeatingMappingId = certificateInfo.heatingId;

                    if (certificateInfo.IssuedDate != null)
                    {
                        DateTime renewalDate = (DateTime)certificateInfo.IssuedDate.Value.AddYears(1);
                        lgsr.CP12Renewal = renewalDate;
                    }
                    if (apptData.appointmentStatus.ToLower() == AppointmentCompleteStatus.Finished.ToString().ToLower()
                        || apptData.appointmentStatus.ToLower() == AppointmentCompleteStatus.Complete.ToString().ToLower()
                        || apptData.appointmentStatus.ToLower() == AppointmentCompleteStatus.Completed.ToString().ToLower())
                    {
                        lgsr.IsAppointmentCompleted = true;
                        lgsr.JOURNALID = apptData.journalId;
                    }
                    else
                    {
                        lgsr.IsAppointmentCompleted = false;
                    }

                    lgsr.ISPRINTED = false;

                    if (isRecordFound == false)
                    {
                        context.AddToP_LGSR(lgsr);
                    }

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    #endregion

                    #region "Save in P_LGSR_HISTORY"

                    int lgsrId = lgsr.LGSRID;

                    P_LGSR_HISTORY lgsrHistory = new P_LGSR_HISTORY();
                    if (certificateInfo.IssuedBy != null)
                    {
                        lgsrHistory.CP12ISSUEDBY = certificateInfo.IssuedBy;
                    }
                    if (certificateInfo.IssuedDate != null)
                    {
                        lgsrHistory.ISSUEDATE = certificateInfo.IssuedDate;
                    }
                    if (certificateInfo.CP12Number != null)
                    {
                        lgsrHistory.CP12NUMBER = certificateInfo.CP12Number;
                    }
                    if (certificateInfo.DocumentType != null)
                    {
                        lgsrHistory.DOCUMENTTYPE = certificateInfo.DocumentType;
                    }
                    if (certificateInfo.DTimeStamp != null)
                    {
                        lgsrHistory.DTIMESTAMP = certificateInfo.DTimeStamp;
                    }
                    if (certificateInfo.InspectionCarried != null)
                    {
                        lgsrHistory.INSPECTIONCARRIED = certificateInfo.InspectionCarried;
                    }
                    if (certificateInfo.Notes != null)
                    {
                        lgsrHistory.NOTES = certificateInfo.Notes;
                    }
                    if (certificateInfo.ReceivedDate != null)
                    {
                        lgsrHistory.RECEVIEDDATE = certificateInfo.ReceivedDate;
                    }
                    if (certificateInfo.ReceivedOnBehalfOf != null)
                    {
                        lgsrHistory.RECEVIEDONBEHALF = certificateInfo.ReceivedOnBehalfOf;
                    }
                    if (certificateInfo.TenantHanded != null)
                    {
                        lgsrHistory.TENANTHANDED = certificateInfo.TenantHanded;
                    }
                    if (apptData.journal.journalId != null)
                    {
                        lgsrHistory.JOURNALID = apptData.journal.journalId;
                    }

                    lgsrHistory.PROPERTYID = certificateInfo.propertyId;
                    lgsrHistory.SchemeId = certificateInfo.schemeId;
                    lgsrHistory.BlockId = certificateInfo.blockId;
                    lgsrHistory.HeatingMappingId = certificateInfo.heatingId;

                    lgsr.CP12Passed = Convert.ToInt32(certificateInfo.CP12Passed);
                    lgsrHistory.CP12Passed = Convert.ToInt32(certificateInfo.CP12Passed);
                    lgsrHistory.IsAppointmentCompleted = lgsr.IsAppointmentCompleted;
                    lgsrHistory.LGSRID = lgsrId;

                    context.AddToP_LGSR_HISTORY(lgsrHistory);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    #endregion

                }

            }

        }

        #endregion

        #region "Update And Insert Into Journal"
        /// <summary>
        /// This function updates the journal table, inserts in the journal history table. As per the comments of the client
        /// following implementation has been done.
        /// "If the appoinment does not exist against PropertyId (JournalId), then another appointment can be created. To create the appointment, we need to have a Journal Id. If the JournalId exists in AS_APPOINTMENTS, it suggests that appointment already exists. In order to create a new appointment for same property, we need to ensure that the JournalId is not repeated. In other words, one JournalId will only have one record in AS_APPOINTMENTS. The appliance servicing team stance is correct
        /// In terms of your 2nd question, hitting Save on screen 17 (means issuing certificate) will update the status of AS_journal to CP12 Issued. Also it will then make IsCurrent=0 and Insert a new reocord in AS_JOurnal with status 'Appointment to be Arranged' with isCurrent=1 for newly inserted record"
        /// </summary>
        /// <param name="workData"></param>
        public long updateApplianceJournalForCompleteStatus(AllAppointmentsList appointment)
        {
            long journaHistoryId = 0;

            #region "Update AS_JOURNAL Table"

            var currentJournal = context.AS_JOURNAL.Where(jor => jor.JOURNALID == appointment.journal.journalId).FirstOrDefault();
            var inspectionType = context.P_INSPECTIONTYPE.Where(pt => pt.Description == ApplicationConstants.ApplianceInspectionType).FirstOrDefault();

            if (currentJournal != null)
            {
                #region Update Journal Data for Certificate Satus

                #region "Update AS_JOURNAL"

                var certificateStatus = context.AS_Status.Where(status => status.Title == MessageConstants.CertificateIssuedNameInAS_Status).FirstOrDefault();
                int certificateStatusId = certificateStatus.StatusId;
                var certificateAction = context.AS_Action.Where(action => action.Title == MessageConstants.CertificateIssuedNameInAS_Action && action.StatusId == certificateStatusId).FirstOrDefault();

                currentJournal.STATUSID = certificateStatus.StatusId;
                currentJournal.ACTIONID = certificateAction.ActionId;
                currentJournal.ISCURRENT = false;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #region Insert History record into AS_JOURNALHISTORY table for Certificate Status

                AS_JOURNALHISTORY jHistory = new AS_JOURNALHISTORY();
                jHistory.JOURNALID = currentJournal.JOURNALID;
                jHistory.PROPERTYID = currentJournal.PROPERTYID;
                jHistory.SchemeId = currentJournal.SchemeId;
                jHistory.BlockId = currentJournal.BlockId;
                jHistory.STATUSID = currentJournal.STATUSID;
                jHistory.ACTIONID = currentJournal.ACTIONID;
                jHistory.INSPECTIONTYPEID = currentJournal.INSPECTIONTYPEID;
                jHistory.CREATIONDATE = DateTime.Now;
                jHistory.CREATEDBY = appointment.updatedBy;
                jHistory.ISLETTERATTACHED = false;
                jHistory.IsDocumentAttached = false;

                context.AddToAS_JOURNALHISTORY(jHistory);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #endregion

                #region Insert new record into AS_JOURNAL with Appointment To Be Arranged Status

                var toBeArrangedStatus = context.AS_Status.Where(status => status.Title == MessageConstants.AppointmentToBeArrangedStatus).FirstOrDefault();
                var toBeArrangedStatusId = toBeArrangedStatus.StatusId;
                var toBeArngedAction = context.AS_Action.Where(action => action.Title == MessageConstants.AppointmentToBeArrangedStatus && action.StatusId == toBeArrangedStatusId).FirstOrDefault();

                #region "Insert into AS_JOURNAL"

                LGSRData workData = appointment.cp12Info.FirstOrDefault();

                AS_JOURNAL toBeArrangedJournal = new AS_JOURNAL();
                toBeArrangedJournal.PROPERTYID = currentJournal.PROPERTYID;
                toBeArrangedJournal.SchemeId = currentJournal.SchemeId;
                toBeArrangedJournal.BlockId = currentJournal.BlockId;
                toBeArrangedJournal.STATUSID = toBeArrangedStatusId;
                toBeArrangedJournal.ACTIONID = toBeArngedAction.ActionId;
                toBeArrangedJournal.INSPECTIONTYPEID = inspectionType.InspectionTypeID;
                toBeArrangedJournal.CREATIONDATE = DateTime.Now;
                toBeArrangedJournal.CREATEDBY = workData.IssuedBy;
                toBeArrangedJournal.ISCURRENT = true;
                toBeArrangedJournal.ServicingTypeId = currentJournal.ServicingTypeId;

                context.AddToAS_JOURNAL(toBeArrangedJournal);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #region "Insert into AS_JOURNALHISTORY"

                AS_JOURNALHISTORY toBeArrangedJournalHistory = new AS_JOURNALHISTORY();
                toBeArrangedJournalHistory.JOURNALID = toBeArrangedJournal.JOURNALID;
                toBeArrangedJournalHistory.PROPERTYID = toBeArrangedJournal.PROPERTYID;
                toBeArrangedJournalHistory.SchemeId = toBeArrangedJournal.SchemeId;
                toBeArrangedJournalHistory.BlockId = toBeArrangedJournal.BlockId;
                toBeArrangedJournalHistory.STATUSID = toBeArrangedJournal.STATUSID;
                toBeArrangedJournalHistory.ACTIONID = toBeArrangedJournal.ACTIONID;
                toBeArrangedJournalHistory.INSPECTIONTYPEID = inspectionType.InspectionTypeID;
                toBeArrangedJournalHistory.CREATIONDATE = toBeArrangedJournal.CREATIONDATE;
                toBeArrangedJournalHistory.CREATEDBY = toBeArrangedJournal.CREATEDBY;
                toBeArrangedJournalHistory.ISLETTERATTACHED = false;
                toBeArrangedJournalHistory.IsDocumentAttached = false;
                toBeArrangedJournalHistory.ServicingTypeId = currentJournal.ServicingTypeId;

                context.AddToAS_JOURNALHISTORY(toBeArrangedJournalHistory);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #endregion

                journaHistoryId = toBeArrangedJournalHistory.JOURNALHISTORYID;
            }
            #endregion

            return journaHistoryId;
        }
        #endregion

        #region "Update Journal Heating Mapping For Mains Gas"
        public void updateJournalHeatingMappingForMainsGas(long journalHistoryId, AllAppointmentsList appointment)
        {

            var journalHist = context.AS_JOURNALHISTORY.Where(x => x.JOURNALHISTORYID == journalHistoryId).FirstOrDefault();

            List<int> heatingList = new List<int>();

            var heatingQuery = (from phm in context.PA_HeatingMapping
                                join pv in context.PA_PARAMETER_VALUE on phm.HeatingType equals pv.ValueID
                                where phm.IsActive == true && pv.ValueDetail == ApplicationConstants.MainsGasParameterValue
                                select phm);

            PropertyData propertyInfo = appointment.property;
            if (propertyInfo.propertyId != null)
            {
                heatingList = heatingQuery.Where(pm => pm.PropertyId == propertyInfo.propertyId).Select(x => x.HeatingMappingId).ToList();
            }
            else if (propertyInfo.schemeId > 0)
            {
                heatingList = heatingQuery.Where(pm => pm.SchemeID == propertyInfo.schemeId).Select(x => x.HeatingMappingId).ToList();
            }
            else if (propertyInfo.blockId > 0)
            {
                heatingList = heatingQuery.Where(pm => pm.BlockID == propertyInfo.blockId).Select(x => x.HeatingMappingId).ToList();
            }

            foreach (var heatingId in heatingList)
            {
                AS_JournalHeatingMapping jrnlHeating = new AS_JournalHeatingMapping();
                jrnlHeating.HeatingMappingId = heatingId;
                jrnlHeating.JournalId = journalHist.JOURNALID;
                jrnlHeating.JournalHistoryId = journalHist.JOURNALHISTORYID;
                jrnlHeating.Createdon = DateTime.Now;
                context.AddToAS_JournalHeatingMapping(jrnlHeating);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

        }
        #endregion

        #region "Update Journal Heating Mapping For Alternative Heating"
        public void updateJournalHeatingMappingForAlternativeHeating(long journalHistoryId, AllAppointmentsList appointment)
        {

            var journalHist = context.AS_JOURNALHISTORY.Where(x => x.JOURNALHISTORYID == journalHistoryId).FirstOrDefault();

            List<int> heatingList = new List<int>();

            var heatingQuery = (from phm in context.PA_HeatingMapping
                                join pv in context.PA_PARAMETER_VALUE on phm.HeatingType equals pv.ValueID
                                where phm.IsActive == true && pv.IsAlterNativeHeating == true
                                select phm);

            PropertyData propertyInfo = appointment.property;
            if (propertyInfo.propertyId != null)
            {
                heatingList = heatingQuery.Where(pm => pm.PropertyId == propertyInfo.propertyId).Select(x => x.HeatingMappingId).ToList();
            }
            else if (propertyInfo.schemeId > 0)
            {
                heatingList = heatingQuery.Where(pm => pm.SchemeID == propertyInfo.schemeId).Select(x => x.HeatingMappingId).ToList();
            }
            else if (propertyInfo.blockId > 0)
            {
                heatingList = heatingQuery.Where(pm => pm.BlockID == propertyInfo.blockId).Select(x => x.HeatingMappingId).ToList();
            }

            foreach (var heatingId in heatingList)
            {
                AS_JournalHeatingMapping jrnlHeating = new AS_JournalHeatingMapping();
                jrnlHeating.HeatingMappingId = heatingId;
                jrnlHeating.JournalId = journalHist.JOURNALID;
                jrnlHeating.JournalHistoryId = journalHist.JOURNALHISTORYID;
                jrnlHeating.Createdon = DateTime.Now;
                context.AddToAS_JournalHeatingMapping(jrnlHeating);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

        }
        #endregion

        #region "Update Journal Heating Mapping For Oil"
        public void updateJournalHeatingMappingForOil(long journalHistoryId, AllAppointmentsList appointment)
        {

            var journalHist = context.AS_JOURNALHISTORY.Where(x => x.JOURNALHISTORYID == journalHistoryId).FirstOrDefault();

            List<int> heatingList = new List<int>();

            var heatingQuery = (from phm in context.PA_HeatingMapping
                                join pv in context.PA_PARAMETER_VALUE on phm.HeatingType equals pv.ValueID
                                where phm.IsActive == true && pv.ValueDetail == ApplicationConstants.OilParameterValue
                                select phm);

            PropertyData propertyInfo = appointment.property;
            if (propertyInfo.propertyId != null)
            {
                heatingList = heatingQuery.Where(pm => pm.PropertyId == propertyInfo.propertyId).Select(x => x.HeatingMappingId).ToList();
            }
            else if (propertyInfo.schemeId > 0)
            {
                heatingList = heatingQuery.Where(pm => pm.SchemeID == propertyInfo.schemeId).Select(x => x.HeatingMappingId).ToList();
            }
            else if (propertyInfo.blockId > 0)
            {
                heatingList = heatingQuery.Where(pm => pm.BlockID == propertyInfo.blockId).Select(x => x.HeatingMappingId).ToList();
            }

            foreach (var heatingId in heatingList)
            {
                AS_JournalHeatingMapping jrnlHeating = new AS_JournalHeatingMapping();
                jrnlHeating.HeatingMappingId = heatingId;
                jrnlHeating.JournalId = journalHist.JOURNALID;
                jrnlHeating.JournalHistoryId = journalHist.JOURNALHISTORYID;
                jrnlHeating.Createdon = DateTime.Now;
                context.AddToAS_JournalHeatingMapping(jrnlHeating);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

        }
        #endregion

        #region Save Fault paused
        public Nullable<int> saveFaultPaused(JobPauseHistoryData jobPauseData, int faultLogID, Nullable<int> faultLogHistoryId = null)
        {
            Nullable<int> pauseID = null;
            FL_FAULT_PAUSED faultPaused = new FL_FAULT_PAUSED();
            faultPaused.FaultLogId = faultLogID;
            faultPaused.PausedOn = jobPauseData.pauseDate;
            faultPaused.Notes = jobPauseData.pauseNote;
            faultPaused.Reason = jobPauseData.pauseReason;
            faultPaused.PausedBy = jobPauseData.pausedBy;
            faultPaused.FaultLogHistoryID = faultLogHistoryId;
            context.AddToFL_FAULT_PAUSED(faultPaused);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            pauseID = faultPaused.PauseID;
            return pauseID;
        }
        #endregion

        #region Save Fault Repair List
        public bool saveFaultRepairList(JobData jobData, int? userId)
        {
            bool success = false;

            if (jobData.faultRepairList.Count > 0)
            {
                StringBuilder sbRepairIds = new StringBuilder();
                foreach (FaultRepairData faultRepairData in jobData.faultRepairList)
                {
                    sbRepairIds.Append(faultRepairData.faultRepairId);
                    sbRepairIds.Append(",");
                    // apptDal.SaveFaultPaused(jobPauseData, jobData.faultLogID);
                }
                string FaultRepairIDs = sbRepairIds.ToString().Substring(0, sbRepairIds.ToString().Length - 1);
                System.Data.Objects.ObjectParameter result = new ObjectParameter("result", typeof(int));
                context.FL_ADD_FL_CO_FAULTLOG_TO_REPAIR(jobData.faultLogID, FaultRepairIDs, userId, jobData.repairNotes, result);
                if (Convert.ToInt32(result.Value) == 1)
                    success = true;
                else
                    success = false;
            }
            return success;
        }
        #endregion

        #region Save NoEntry for Fault
        /// <summary>
        /// This function update the status of all jobs of an appointment to noEntry. This function use post method to accept data. 
        /// </summary>
        /// <param name="appointment">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>
        public ResultBoolData noEntryFaultAppointment(AllAppointmentsList appointment)
        {
            if (appointment.repairCompletionDateTime == null)
                appointment.repairCompletionDateTime = DateTime.Now;

            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;
            string status = FaultJobProgressStatus.NoEntry.ToString();

            List<FL_FAULT_APPOINTMENT> fltApptList = getFaultAppointmentList(appointment.appointmentId);

            // check if appointment has been cancelled from Web module
            if (fltApptList.Count() > 0)
            {
                var varfaultLogID = fltApptList.First().FaultLogId;
                if (isFaultCancelled(varfaultLogID))
                {
                    resultBoolData.result = success;
                    return resultBoolData;
                }
            }

            //using (TransactionScope trans = new TransactionScope())
            //{
            foreach (FL_FAULT_APPOINTMENT fltAppt in fltApptList)
            {
                int faultLogID = fltAppt.FaultLogId;

                // Get Job data from appointment, in case it is null create a new instance to avoid null reference exception, in function call below.
                JobData Jobdata = (JobData)(appointment.jobDataList.AsEnumerable<JobData>().Where(jobdata => jobdata.faultLogID == faultLogID).FirstOrDefault());
                Jobdata = Jobdata ?? new JobData();

                //1.) INSERT INTO FL_FAULT_NOENTRY                       
                FL_FAULT_NOENTRY faultNoEntry = new FL_FAULT_NOENTRY();
                faultNoEntry.FaultLogId = faultLogID;
                faultNoEntry.RecordedOn = Jobdata.completionDate ?? appointment.repairCompletionDateTime;
                faultNoEntry.Notes = appointment.noEntryNotes;
                context.AddToFL_FAULT_NOENTRY(faultNoEntry);

                //2.) INSERT INTO FL_FAULT_LOG_HISTORY (Status='No Entry')
                //3.) UPDATE FL_FAULT_LOG SET FaultStatusId='Completed'
                //4.) INSERT INTO FL_FAULT_LOG_HISTORY (Status='Completed')
                //5.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId='Completed'                       
                this.updateFaultStatusId(context, Jobdata, faultLogID, FaultJobProgressStatus.NoEntry.ToString(), appointment.createdBy, Jobdata.completionDate);
                this.updateFaultStatusId(context, Jobdata, faultLogID, FaultJobProgressStatus.Complete.ToString(), appointment.createdBy, Jobdata.completionDate, Jobdata.completionDate);

                //6.) UPDATE FL_FAULT_JOBTIMESHEET SET JobEndDate/Time
                saveUpdateFaultTimeSheet(appointment.appointmentId, faultLogID, Jobdata.completionDate ?? appointment.repairCompletionDateTime, FaultJobProgressStatus.NoEntry.ToString());

                //7.) UPDATE FL_CO_APPOINTMENT SET AppointmentStatus='Completed'
                var flAppointment = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointment.appointmentId).First();
                flAppointment.AppointmentStatus = FaultJobProgressStatus.Complete.ToString();
                flAppointment.LastActionDate = Jobdata.completionDate ?? appointment.repairCompletionDateTime;
                flAppointment.RepairCompletionDateTime = flAppointment.RepairCompletionDateTime ?? Jobdata.completionDate ?? appointment.repairCompletionDateTime;
                success = true;
            }

            if (success == true)
            {
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            resultBoolData.result = success;
            return resultBoolData;
        }
        #endregion

        #region Complete Appointment For Fault
        public bool completeAppointmentForFault(JobData apptData, AllAppointmentsList appointment)
        {
            if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
            {
                if (apptData.completionDate == null)
                    apptData.completionDate = DateTime.Now;

                System.Data.Objects.ObjectParameter result1 = new ObjectParameter("result", typeof(int));
                context.FL_CO_COMPLETE_APPOINTMENT(appointment.appointmentId, apptData.faultLogID, apptData.followOnNotes, apptData.repairNotes,
                    apptData.completionDate, true, appointment.repairCompletionDateTime ?? apptData.completionDate, result1);
            }
            return true;
        }
        #endregion

        #region Paused Appointment For Fault

        public bool PausedAppointmentForFault(JobData jobData, AllAppointmentsList appointment)
        {
            //2.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Paused’
            //3.) INSERT INTO FL_FAULT_LOG_HISTORY (Status=’Paused)
            //4.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Paused’                      
            //TODO: Move faultlog update logic to a separate/independent locaiotn and update it for each status change occourance like (start --> pause --> resume(start again) --> complete etc.)
            if (jobData != null)
                this.updateFaultStatusId(context, jobData, jobData.faultLogID, jobData.jobStatus, appointment.createdBy, DateTime.Now);

            bool isAppointmentFinished = appointment.appointmentStatus == FaultAppointmentProgressStatus.Complete.ToString() ? true : false;

            this.updateFaultAppointmentProgressStatus(appointment.appointmentId, appointment.appointmentStatus, isAppointmentFinished);
            //faultJobTimeSheet.EndTime = apptData.reportedDate;
            this.saveLastSurveyDateFault(appointment.appointmentId);

            return true;
        }

        #endregion

        #region Save Planned appointment paused
        /// <summary>
        /// Save Planned appointment paused
        /// </summary>
        /// <param name="jobPauseData"></param>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        public Nullable<int> savePlannedPaused(JobPauseHistoryData jobPauseData, int appointmentId, Nullable<int> appointmentHistoryID = null)
        {
            Nullable<int> pauseID = null;
            PLANNED_PAUSED plannedPaused = new PLANNED_PAUSED();
            plannedPaused.APPOINTMENTID = appointmentId;
            plannedPaused.PausedOn = jobPauseData.pauseDate;
            plannedPaused.Notes = jobPauseData.pauseNote;
            plannedPaused.Reason = jobPauseData.pauseReason;
            plannedPaused.PausedBy = jobPauseData.pausedBy;
            plannedPaused.APPOINTMENTHISTORYID = appointmentHistoryID;
            context.AddToPLANNED_PAUSED(plannedPaused);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            pauseID = plannedPaused.PauseID;
            return pauseID;
        }
        #endregion

        #region Save NoEntry for Planned

        /// <summary>
        /// This function update the status of all jobs of an appointment to noEntry. This function use post method to accept data. 
        /// </summary>
        /// <param name="appointment"> </param>      
        /// <returns>It returns true or false in update is successful</returns>
        public ResultBoolData noEntryPlannedAppointment(AllAppointmentsList appointment, bool callfromAPIV2 = false)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;

            // check if appointment has been cancelled from Web module

            int JournalId = (int)appointment.journalId;
            if (isPlannedCancelled(JournalId))
            {
                resultBoolData.result = success;
                return resultBoolData;
            }
            using (TransactionScope trans = new TransactionScope())
            {
                setNoEntryAppointmentToBeArranged(JournalId);
                //1.) INSERT INTO PLANNED_NOENTRY                       
                PLANNED_NOENTRY plannedNoEntry = new PLANNED_NOENTRY();
                plannedNoEntry.APPOINTMENTID = appointment.appointmentId;
                plannedNoEntry.RecordedOn = appointment.appointmentEndDateTime;
                plannedNoEntry.Notes = appointment.noEntryNotes;
                if (appointment.noEntryNotes == null || appointment.noEntryNotes == string.Empty)
                    plannedNoEntry.Notes = "Job completed because of No Entry";
                context.AddToPLANNED_NOENTRY(plannedNoEntry);
                //2.) UPDATE PLANNED_JOBTIMESHEET SET JobEndDate/Time
                // 3.) UPDATE PLANNED_APPOINTMENT SET
                //AppointmentStatus=’Complete’, JournalSubstatus= ‘No Entry’, Notes=’Job
                //completed because of No Entry’
                //4.) INSERT into PLANNED_APPOINTMENTS_HISTORY
                //5.) Check if there are any other JSN’s or appointments which are (‘Not
                //Started’ or ‘InProgress’)
                //5a) If records found in point 5, then repeat steps 2,3 and 4 for each JSN
                if (callfromAPIV2) insertUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.repairCompletionDateTime ?? appointment.componentTrade.completionDate, AppointmentCompleteStatus.NoEntry.ToString());
                else saveUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);

                this.updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.Complete.ToString(), "No Entry".ToString(), appointment);
                //List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
                //appointmentList = checkPlannedAppointmentByJournalId(appointment.journalId);
                //if (appointmentList.Count() > 0)
                //{
                //    foreach (PLANNED_APPOINTMENTS pAppointment in appointmentList)
                //    {
                //        if (callfromAPIV2) insertUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.repairCompletionDateTime ?? appointment.componentTrade.completionDate, AppointmentCompleteStatus.NoEntry.ToString());
                //        else saveUpdatePlannedJobTimeSheet(pAppointment.APPOINTMENTID, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);

                //        this.updatePlannedAppointment(pAppointment.APPOINTMENTID, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.Complete.ToString(), "No Entry".ToString(), appointment);
                //    }
                //}
                success = true;
                if (success == true)
                {
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                }
            }

            resultBoolData.result = success;
            return resultBoolData;
        }

        public bool setNoEntryAppointmentToBeArranged(int journalId)
        {
            bool status = false;

            PLANNED_JOURNAL plannedJournal = context.PLANNED_JOURNAL.Where(fltl => fltl.JOURNALID == journalId).FirstOrDefault();
            if (plannedJournal != null)
            {
                if (plannedJournal.COMPONENTID != null && plannedJournal.APPOINTMENTTYPEID == null)
                {
                    int toBeArrangedStatusID = GetPlannedStatusID("To be Arranged");
                    plannedJournal.STATUSID = Convert.ToInt16(toBeArrangedStatusID);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    var historyList = context.PLANNED_StatusHistory
                                        .Where(x => x.StatusId == toBeArrangedStatusID).ToList();
                    if (historyList != null)
                    {
                        if (historyList.Count > 0)
                        {
                            int maxHistoryId = historyList.OrderByDescending(y => y.StatusHistoryId).First().StatusHistoryId;

                            plannedJournal = context.PLANNED_JOURNAL.Where(fltl => fltl.JOURNALID == journalId).FirstOrDefault();
                            PLANNED_JOURNAL_HISTORY historyLog = new PLANNED_JOURNAL_HISTORY();
                            historyLog.JOURNALID = plannedJournal.JOURNALID;
                            historyLog.PROPERTYID = plannedJournal.PROPERTYID;
                            historyLog.SchemeId = plannedJournal.SchemeId;
                            historyLog.BlockId = plannedJournal.BlockId;
                            historyLog.COMPONENTID = plannedJournal.COMPONENTID;
                            historyLog.STATUSID = plannedJournal.STATUSID;
                            historyLog.ACTIONID = plannedJournal.ACTIONID;
                            historyLog.CREATEDBY = plannedJournal.CREATEDBY;
                            historyLog.CREATIONDATE = DateTime.Now;
                            context.AddToPLANNED_JOURNAL_HISTORY(historyLog);
                            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                            status = true;

                        }
                    }

                }
            }

            return status;
        }

        public bool isPlannedCancelled(int journalId)
        {
            int cancelledStatusID = GetPlannedStatusID("Cancelled");

            PLANNED_JOURNAL appointment = context.PLANNED_JOURNAL.Where(fltl => fltl.JOURNALID == journalId).First();
            if (appointment.STATUSID == cancelledStatusID)
                return true;
            else
                return false;
        }

        public int GetPlannedStatusID(string status)
        {
            var fltStatusList = context.PLANNED_STATUS.Where(fltSts => fltSts.TITLE.Replace(" ", "") == status.Replace(" ", ""));
            if (fltStatusList.Count() == 0)
                return 0;

            PLANNED_STATUS fltStatus = fltStatusList.First();

            return fltStatus.STATUSID;
        }

        public int GetPlannedSubStatusID(string status)
        {
            var fltStatusList = context.PLANNED_SUBSTATUS.Where(fltSts => fltSts.TITLE.Replace(" ", "") == status.Replace(" ", ""));
            if (fltStatusList.Count() == 0)
                return 0;

            PLANNED_SUBSTATUS fltStatus = fltStatusList.First();

            return fltStatus.SUBSTATUSID;
        }

        #region update planned appointment data

        /// <summary>
        /// This function saves the planned Log History Data in the database
        /// </summary>       
        /// <returns>the true if successfully save otherwise false</returns>
        public Nullable<int> updatePlannedAppointment(int appointmentId, string appointmentNotes, string customerNotes, DateTime? appointmentEndDateTime, string appStatus, string subStatus, AllAppointmentsList appointment, JobPauseHistoryData jph = null)
        {
            Nullable<int> appointmentHistoryID = null;

            int subStatusCode = GetPlannedSubStatusID(subStatus);
            var appData = context.PLANNED_APPOINTMENTS.Where(app => app.APPOINTMENTID == appointmentId);
            if (appData.Count() > 0)
            {
                PLANNED_APPOINTMENTS planned = appData.First();
                planned.APPOINTMENTSTATUS = appStatus;
                planned.JOURNALSUBSTATUS = subStatusCode;
                if (jph != null)
                {
                    planned.LOGGEDDATE = jph.pauseDate;
                }
                else
                {
                    planned.LOGGEDDATE = DateTime.Now;
                }

                if (appStatus == AppointmentCompleteStatus.Complete.ToString())
                {
                    planned.CompletionNotes = customerNotes;

                    if (appointment.loggedDate != null)
                    {
                        planned.AppointmentCompletionDate = appointment.loggedDate;
                    }

                    if (appointment.appointmentCompletedAppVersion != null)
                    {
                        planned.AppointmentCompletedAppVersion = appointment.appointmentCompletedAppVersion;
                    }

                    if (appointment.appointmentCurrentAppVersion != null)
                    {
                        planned.AppointmentCurrentAppVersion = appointment.appointmentCurrentAppVersion;
                    }
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                appointmentHistoryID = context.PLANNED_APPOINTMENTS_HISTORY.Where(appHistory => appHistory.APPOINTMENTID == appointmentId).Max(appHistory => appHistory.APPOINTMENTHISTORYID);
            }
            return appointmentHistoryID;
        }

        #endregion

        #region save Update Planned Job TimeSheet
        /// <summary>
        /// This function save or Update Planned Job TimeSheet in the database
        /// </summary>      
        /// <returns>the true if successfully save otherwise false </returns>
        public bool saveUpdatePlannedJobTimeSheet(int appointmentId, DateTime? startTime, DateTime? endTime)
        {
            bool result = false;

            //using (TransactionScope trans = new TransactionScope())
            //{
            var appData = context.PLANNED_JOBTIMESHEET.Where(app => app.APPOINTMENTID == appointmentId);
            if (appData.Count() > 0)
            {
                PLANNED_JOBTIMESHEET planned = appData.First();
                planned.EndTime = endTime;
            }
            else
            {
                PLANNED_JOBTIMESHEET planned = new PLANNED_JOBTIMESHEET();
                planned.APPOINTMENTID = appointmentId;
                planned.StartTime = startTime;
                planned.EndTime = endTime;
                context.AddToPLANNED_JOBTIMESHEET(planned);
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            result = true;
            //}
            return result;
        }
        #endregion

        #region save planned journal history
        /// <summary>
        /// save planned journal history
        /// </summary>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public bool savePlannedJournalistoryData(int? updatedby, int? journalId, string status)
        {
            bool result = false;
            int plannedStatusId = GetPlannedStatusID(status);
            Nullable<long> journalHistoryID = null;
            PLANNED_JOURNAL pjournal = new PLANNED_JOURNAL();
            var journalData = context.PLANNED_JOURNAL.Where(jor => jor.JOURNALID == journalId);
            if (journalData.Count() > 0)
            {
                //using (TransactionScope trans = new TransactionScope())
                //{
                pjournal = journalData.First();
                pjournal.STATUSID = (short)plannedStatusId;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                PLANNED_JOURNAL_HISTORY pJournalHistory = new PLANNED_JOURNAL_HISTORY();
                pJournalHistory.JOURNALID = pjournal.JOURNALID;
                pJournalHistory.PROPERTYID = pjournal.PROPERTYID;
                pJournalHistory.SchemeId = pjournal.SchemeId;
                pJournalHistory.BlockId = pjournal.BlockId;
                pJournalHistory.COMPONENTID = pjournal.COMPONENTID;
                pJournalHistory.STATUSID = pjournal.STATUSID;
                pJournalHistory.ACTIONID = pjournal.ACTIONID;
                pJournalHistory.CREATEDBY = updatedby;
                pJournalHistory.CREATIONDATE = DateTime.Now;
                context.AddToPLANNED_JOURNAL_HISTORY(pJournalHistory);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                journalHistoryID = context.PLANNED_JOURNAL_HISTORY.Where(journal => journal.JOURNALID == pJournalHistory.JOURNALID).Max(historyId => historyId.JOURNALHISTORYID);

                var appData = from pA in context.PLANNED_APPOINTMENTS
                              orderby pA.APPOINTMENTID descending
                              where pA.JournalId == pjournal.JOURNALID
                              select pA;

                if (appData.Count() > 0)
                {

                    PLANNED_APPOINTMENTS planned = appData.First();
                    planned.JOURNALHISTORYID = journalHistoryID;

                }
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                result = true;
                //}
            }
            return result;

        }
        #endregion

        #region check planned job started or not
        /// <summary>
        /// This function return jobtime sheet id 
        /// </summary>      
        /// <returns>the TimeSheetID if exist otherwise 0 </returns>

        public int isPlannedJobStarted(int appointmentId)
        {
            int result = 0;

            var appData = context.PLANNED_JOBTIMESHEET.Where(app => app.APPOINTMENTID == appointmentId);
            if (appData.Count() > 0)
            {
                PLANNED_JOBTIMESHEET planned = appData.First();
                result = planned.TimeSheetID;

            }

            return result;
        }
        #endregion

        #region check Planned Appointment By PMO

        /// <summary>
        /// This function return appointment List 
        /// </summary>      
        /// <returns>the TimeSheetID if exist otherwise 0 </returns>
        public List<PLANNED_APPOINTMENTS> checkPlannedAppointmentByPMO(int? journalId)
        {
            var cancelledStatues = new List<string>(){
                         AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        ,AppointmentCompleteStatus.Cancel.ToString().ToLower()
                    };

            List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
            var appData = context.PLANNED_APPOINTMENTS.Where(app => app.JournalId == journalId &&
                            !(cancelledStatues.Contains(app.APPOINTMENTSTATUS.ToLower()))
                          );
            if (appData.Count() > 0)
            {
                appointmentList = appData.ToList();
            }

            return appointmentList;
        }

        #endregion

        #region check Planned Appointment By JournalId
        /// <summary>
        /// This function return appointment List 
        /// </summary>       

        public List<PLANNED_APPOINTMENTS> checkPlannedAppointmentByJournalId(int? journalId)
        {

            List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
            var appData = context.PLANNED_APPOINTMENTS.Where(app => app.JournalId == journalId && (app.APPOINTMENTSTATUS == "In Progress" || app.APPOINTMENTSTATUS == "NotStarted"));
            if (appData.Count() > 0)
            {
                appointmentList = appData.ToList();

            }

            return appointmentList;
        }
        #endregion

        #endregion

        #region Complete Appointment For Planned

        public bool completeAppointmentForPlanned(AllAppointmentsList appointment, bool callfromAPIV2 = false)
        {
            if (appointment.appointmentStatus == AppointmentCompleteStatus.InProgress.ToString())
            {
                if (appointment.componentTrade.jobStatus == AppointmentCompleteStatus.InProgress.ToString() || appointment.componentTrade.jobStatus == AppointmentCompleteStatus.Paused.ToString())
                {
                    if (!callfromAPIV2) saveUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);
                }
                if (!callfromAPIV2) this.updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.InProgress.ToString(), appointment.componentTrade.jobStatus, appointment);
            }

            else if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
            {
                this.updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.Complete.ToString(), AppointmentCompleteStatus.Complete.ToString(), appointment);
                if (!callfromAPIV2) saveUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);
                List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
                appointmentList = checkPlannedAppointmentByPMO(appointment.journalId);

                int appointmentListCount = appointmentList.Count();
                if (appointmentListCount >= 1)
                {
                    int inCompleteStatusCount = 0;
                    int miscellaneousAppointmentCount = 0;
                    foreach (PLANNED_APPOINTMENTS plannedApp in appointmentList)
                    {
                        if (plannedApp.APPOINTMENTSTATUS != AppointmentCompleteStatus.Complete.ToString())
                            inCompleteStatusCount = inCompleteStatusCount + 1;
                        if (plannedApp.isMiscAppointment)
                            miscellaneousAppointmentCount = miscellaneousAppointmentCount + 1;
                    }
                    // Check whether there is one (or more) inComplete Appointments, before calling journal completion process.
                    if (inCompleteStatusCount == 0)
                    {
                        bool isMiscJournal = false;
                        if (miscellaneousAppointmentCount == appointmentListCount)
                        { isMiscJournal = true; }
                        completePlannedJournal(appointment, isMiscJournal);
                    }
                }

                if (appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Adaptation.ToString())
                {
                    if (appointment.componentTrade.locationId != null && appointment.componentTrade.adaptationId != null
                        && appointment.componentTrade.locationId > 0 && appointment.componentTrade.adaptationId > 0)
                    {
                        int parameterId = (int)appointment.componentTrade.locationId;
                        int valueId = (int)appointment.componentTrade.adaptationId;
                        string adaptation = appointment.componentTrade.adaptation;
                        int createdBy = (int)appointment.createdBy;

                        if (appointment.property != null)
                        {
                            updatePropertyAttributesArea(appointment.property.propertyId, parameterId, valueId, adaptation, createdBy);
                        }
                        else if (appointment.scheme != null)
                        {
                            if (appointment.scheme.blockId != null && appointment.scheme.blockId > 0)
                            {
                                updateBlockAttributesArea(appointment.scheme.blockId, parameterId, valueId, adaptation, createdBy);
                            }
                            else if (appointment.scheme.schemeId != null && appointment.scheme.schemeId > 0)
                            {
                                updateSchemeAttributesArea(appointment.scheme.schemeId, parameterId, valueId, adaptation, createdBy);
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// This function gets the component id
        /// </summary>
        /// <returns></returns>
        public int getcycleByComponentId(int? componentId)
        {
            int cycle = 0;
            var component = context.PLANNED_COMPONENT.Where(item => item.COMPONENTID == componentId);
            if (component.Count() > 0)
            {
                PLANNED_COMPONENT comp = component.First();
                cycle = (int)comp.CYCLE;
                if (comp.FREQUENCY == "yrs")
                    cycle = (int)comp.CYCLE * 12;

            }
            return cycle;
        }

        /// <summary>
        /// update Planned Journal
        /// </summary>
        /// <param name="appointment"></param>
        /// <param name="isMiscJournal"></param>
        /// <returns></returns>
        public bool completePlannedJournal(AllAppointmentsList appointment, bool isMiscJournal)
        {

            this.savePlannedJournalistoryData(appointment.updatedBy, appointment.journalId, AppointmentCompleteStatus.Completed.ToString());
            if (!isMiscJournal)
            {
                #region Update Last Replaced and Replacement Due for Planned Components based of Componenet Replacement Cycle
                int cycle = getcycleByComponentId(appointment.componentTrade.componentId);
                DateTime? replacementDue = appointment.componentTrade.completionDate.Value.AddMonths(cycle);
                PA_PROPERTY_ITEM_DATES itemDates = new PA_PROPERTY_ITEM_DATES();
                var appData = context.PA_PROPERTY_ITEM_DATES.Where(prop => prop.PROPERTYID == appointment.property.propertyId && prop.PLANNED_COMPONENTID == appointment.componentTrade.componentId);
                if (appData.Count() > 0)
                {
                    itemDates = appData.First();
                    itemDates.LastDone = appointment.componentTrade.completionDate;
                    itemDates.DueDate = replacementDue;
                    //When major work is completed  And once the work is completed within Planned module then status
                    //of the property will be changed back to “Let” with sub-status “Pending Termination”.
                    if (itemDates.isVoidAppointmentAssociated == true)
                    {
                        itemDates.isVoidAppointmentAssociated = false;
                        updatePropertyStatus(appointment.property.propertyId, "Let", "Pending Termination");
                    }
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
                #endregion
            }

            #region Set Condition rating for the item/component to satisfactory

            int attributeId = -1;
            #region Get arrtibuteId for current Journal

            PLANNED_CONDITIONWORKS cw;

            var conditionWorks = context.PLANNED_CONDITIONWORKS.Where(cws => cws.JournalId == appointment.journalId);

            if (conditionWorks.Count() > 0)
            {
                cw = conditionWorks.First();
                attributeId = cw.AttributeId;
            }

            #endregion

            #region Set Attribute/Parameter value for given attribueId (Conditon Rating) to Satisfactory.
            //TODO: May need to check for case where one item is mapped to two or more components.
            if (attributeId > 0)
            {
                var PAs = from pa in context.PA_PROPERTY_ATTRIBUTES
                          join ip in context.PA_ITEM_PARAMETER on pa.ITEMPARAMID equals ip.ItemParamID
                          join pv in context.PA_PARAMETER_VALUE on ip.ParameterId equals pv.ParameterID
                          where pa.ATTRIBUTEID == attributeId && pv.ValueDetail.Equals("Satisfactory")
                          select new
                          {
                              pa,
                              pv
                          };
                if (PAs.Count() > 0)
                {
                    var PropAttrib = PAs.First();
                    PA_PROPERTY_ATTRIBUTES pa = PropAttrib.pa;
                    pa.VALUEID = PropAttrib.pv.ValueID;
                    pa.PARAMETERVALUE = PropAttrib.pv.ValueDetail;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
            }
            #endregion

            #endregion

            return true;
        }

        #endregion

        #region update Property Attributes Area
        /// <summary>
        /// update Property Attributes Area
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="parameterId"></param>
        /// <param name="valueId"></param>
        /// <param name="adaptation"></param>
        /// <param name="createdBy"></param>
        public void updatePropertyAttributesArea(string propertyId, int parameterId, int valueId, string adaptation, int createdBy)
        {
            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();
            var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                                where par.ParameterID == parameterId && itp.IsActive == true && par.IsActive == true
                                orderby par.ParameterSorder ascending
                                select itp).ToList();

            if (itemPrameter.Count() > 0)
            {
                int itemParamId = itemPrameter.First().ItemParamID;

                var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == propertyId && prop.ITEMPARAMID == itemParamId && prop.VALUEID == valueId);

                if (paAttrib.Count() > 0)
                {
                    //fetch the already existed record
                    paAttribItem = paAttrib.First();
                }
                else
                {
                    //if record does not already exist the set the property id and itemparamid
                    paAttribItem.PROPERTYID = propertyId;
                    paAttribItem.ITEMPARAMID = itemParamId;
                    paAttribItem.VALUEID = valueId;
                    paAttribItem.PARAMETERVALUE = adaptation;
                }
                paAttribItem.IsCheckBoxSelected = true;
                paAttribItem.UPDATEDBY = createdBy;
                paAttribItem.UPDATEDON = DateTime.Now;

                if (paAttrib.Count() <= 0)
                {
                    //insert the record
                    context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }
        #endregion

        #region update Scheme Attributes Area
        /// <summary>
        /// update Scheme Attributes Area
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="parameterId"></param>
        /// <param name="valueId"></param>
        /// <param name="adaptation"></param>
        /// <param name="createdBy"></param>
        public void updateSchemeAttributesArea(int? schemeId, int parameterId, int valueId, string adaptation, int createdBy)
        {
            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();
            var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                                where par.ParameterID == parameterId && itp.IsActive == true && par.IsActive == true
                                orderby par.ParameterSorder ascending
                                select itp).ToList();

            if (itemPrameter.Count() > 0)
            {
                int itemParamId = itemPrameter.First().ItemParamID;

                var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.SchemeId == schemeId && prop.ITEMPARAMID == itemParamId && prop.VALUEID == valueId);

                if (paAttrib.Count() > 0)
                {
                    //fetch the already existed record
                    paAttribItem = paAttrib.First();
                }
                else
                {
                    //if record does not already exist the set the schemeId and itemparamid
                    paAttribItem.SchemeId = schemeId;
                    paAttribItem.ITEMPARAMID = itemParamId;
                    paAttribItem.VALUEID = valueId;
                    paAttribItem.PARAMETERVALUE = adaptation;
                }
                paAttribItem.IsCheckBoxSelected = true;
                paAttribItem.UPDATEDBY = createdBy;
                paAttribItem.UPDATEDON = DateTime.Now;

                if (paAttrib.Count() <= 0)
                {
                    //insert the record
                    context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }
        #endregion

        #region update Block Attributes Area
        /// <summary>
        /// update Block Attributes Area
        /// </summary>
        /// <param name="blockId"></param>
        /// <param name="parameterId"></param>
        /// <param name="valueId"></param>
        /// <param name="adaptation"></param>
        /// <param name="createdBy"></param>
        public void updateBlockAttributesArea(int? blockId, int parameterId, int valueId, string adaptation, int createdBy)
        {
            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();
            var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                                where par.ParameterID == parameterId && itp.IsActive == true && par.IsActive == true
                                orderby par.ParameterSorder ascending
                                select itp).ToList();

            if (itemPrameter.Count() > 0)
            {
                int itemParamId = itemPrameter.First().ItemParamID;

                var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.BlockId == blockId && prop.ITEMPARAMID == itemParamId && prop.VALUEID == valueId);

                if (paAttrib.Count() > 0)
                {
                    //fetch the already existed record
                    paAttribItem = paAttrib.First();
                }
                else
                {
                    //if record does not already exist the set the blockId and itemparamid
                    paAttribItem.BlockId = blockId;
                    paAttribItem.ITEMPARAMID = itemParamId;
                    paAttribItem.VALUEID = valueId;
                    paAttribItem.PARAMETERVALUE = adaptation;
                }
                paAttribItem.IsCheckBoxSelected = true;
                paAttribItem.UPDATEDBY = createdBy;
                paAttribItem.UPDATEDON = DateTime.Now;

                if (paAttrib.Count() <= 0)
                {
                    //insert the record
                    context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }
        #endregion

        #region New Complete Appointment Function(s)

        #region save Complete Appointment for gas V2

        /// <summary>
        /// this function calls the method for Gas. and update (appointment Data,SurveyData,Customer data,No entry data in case of "NoEntry" Status,
        /// Journal data,Lgsr data and populate CP12Document 
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForGasV2(AllAppointmentsList appointment)
        {
            bool isSuccess = false;

            try
            {

                if (appointment.appointmentStatus.Equals("Aborted"))
                {
                    if (saveAbortEntry(appointment))
                    {
                        completeAppointmentDataForApplianceServicing(appointment);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }


                long journalHistId = 0;
                using (TransactionScope trans = new TransactionScope())
                {
                    FaultsDal faultDal = new FaultsDal();

                    #region "Saving survey information"
                    if (appointment.property != null)
                    {
                        AppliancesDal appliancesDal = new AppliancesDal();

                        #region Add/Update Appliance(s), add appliance(s) inspection, add appliance(s) defects (if any)
                        if (appointment.property.appliances != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            foreach (ApplianceData appData in appointment.property.appliances)
                            {
                                #region Validation of Appliance inspection data
                                if (appliancesDal.isApplianceDataValid(appData) == false)
                                {
                                    throw new Exception(MessageConstants.InspectionDataMissing);
                                }
                                #endregion

                                #region Add/Update current Appliance

                                AppliancesDal appDal = new AppliancesDal();
                                if (appData.ApplianceType != null && appData.ApplianceType.ApplianceType != null)
                                {
                                    appData.ApplianceType.ApplianceTypeID = appDal.saveApplianceType(appData.ApplianceType, context);
                                }
                                if (appData.ApplianceLocation != null && appData.ApplianceLocation.Location != null)
                                {
                                    appData.ApplianceLocation.LocationID = appDal.saveApplianceLocation(appData.ApplianceLocation, context);
                                }
                                if (appData.ApplianceManufacturer != null && appData.ApplianceManufacturer.Manufacturer != null)
                                {
                                    appData.ApplianceManufacturer.ManufacturerID = appDal.saveApplianceManufacturer(appData.ApplianceManufacturer, context);
                                }
                                if (appData.ApplianceModel != null && appData.ApplianceModel.ApplianceModel != null)
                                {
                                    appData.ApplianceModel.ApplianceModelID = appDal.saveApplianceModel(appData.ApplianceModel, context);
                                }
                                appData.ApplianceID = appDal.saveAppliance(appData, context);

                                #endregion

                                #region Add Appliance Inspection current appliance

                                if (appData.ApplianceID > 0 && appData.isActive == true)
                                {
                                    if (appData.ApplianceInspection != null && appData.ApplianceInspection.inspectionDate != null)
                                    {
                                        appData.ApplianceInspection.APPLIANCEID = appData.ApplianceID;
                                        appData.ApplianceInspection.JOURNALID = appointment.journalId;
                                        appliancesDal.saveApplianceInspectionGas(appData.ApplianceInspection, context);
                                    }
                                }
                                #endregion

                                #region Add Appliance Defects current appliance

                                if (appData.ApplianceDefects != null && appData.ApplianceDefects.Count > 0)
                                {
                                    foreach (FaultsDataGas appDefectData in appData.ApplianceDefects)
                                    {
                                        appDefectData.ApplianceID = appData.ApplianceID;
                                        faultDal.updateFaultDataGas(appDefectData, context);
                                    }
                                }

                                #endregion
                            }
                        }
                        #endregion

                        #region Add/Update Boiler(s), add boiler(s) inspection, add boiler(s) defects (if any)
                        if (appointment.property.boilers != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            foreach (BoilerData boilerData in appointment.property.boilers)
                            {

                                #region Validation of Boiler inspection data
                                if (appliancesDal.isBoilerDataValid(boilerData) == false)
                                {

                                    throw new Exception(MessageConstants.InspectionDataMissing);
                                }
                                #endregion

                                #region Add/Update current Boiler
                                AppliancesDal appDal = new AppliancesDal();
                                appDal.saveBoiler(boilerData, appointment.updatedBy, context);
                                #endregion

                                #region Add Boiler Inspection current boiler
                                if (boilerData.BoilerInspection != null && boilerData.BoilerInspection.inspectionDate != null)
                                {
                                    boilerData.BoilerInspection.heatingId = boilerData.heatingId;
                                    boilerData.BoilerInspection.boilerTypeId = boilerData.boilerTypeId;
                                    boilerData.BoilerInspection.journalId = appointment.journalId;
                                    appliancesDal.saveBoilerInspection(boilerData.BoilerInspection, context);
                                }
                                #endregion

                                #region Add Boiler Defects current boiler
                                if (boilerData.BoilerDefects != null && boilerData.BoilerDefects.Count > 0)
                                {
                                    foreach (FaultsDataGas appDefectData in boilerData.BoilerDefects)
                                    {
                                        appDefectData.boilerTypeId = boilerData.boilerTypeId;
                                        appDefectData.heatingId = boilerData.heatingId;
                                        faultDal.updateFaultDataGas(appDefectData, context);
                                    }
                                }
                                #endregion

                                #region "Saving Installation Pipework"
                                if (boilerData.InstallationPipework != null && boilerData.InstallationPipework.inspectionDate != null)
                                {
                                    InstallationPipeworkDal insWorkDal = new InstallationPipeworkDal();
                                    insWorkDal.saveInstallationPipeworkGas(boilerData, appointment, context);
                                }

                                #endregion
                            }
                        }
                        #endregion

                        #region Update detector(s) Count, add detector(s) Inspection and add detector(s) defects

                        if (appointment.property.detectors != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            foreach (DetectorCountData detector in appointment.property.detectors)
                            {
                                appliancesDal.updateDetectorCount(appointment.property.propertyId, detector, appointment.updatedBy, context);
                                if (detector.detectorDefects != null && detector.detectorDefects.Count > 0)
                                {
                                    foreach (FaultsDataGas appDefectData in detector.detectorDefects)
                                    {
                                        faultDal.updateFaultDataGas(appDefectData, context);
                                    }
                                }
                                if (detector.detectorInspection != null && detector.detectorTypeId > 0)
                                {
                                    detector.detectorInspection.inspectionID = appliancesDal.addUpdateDetectorInfo(detector.detectorInspection
                                            , detector.detectorTypeId, detector.isInspected, appointment.journalId, appointment.property.propertyId, context).result;
                                }
                            }
                        }
                        #endregion

                        #region Add/Update Property Detectors

                        if (appointment.property.propertyDetectors != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            DetectorDal detectorDal = new DetectorDal();
                            foreach (Detector detector in appointment.property.propertyDetectors)
                            {
                                detectorDal.saveDetector(detector, appointment.operativeId, context);

                                if (detector.detectorDefects != null && detector.detectorDefects.Count > 0)
                                {
                                    foreach (FaultsDataGas appDefectData in detector.detectorDefects)
                                    {
                                        faultDal.updateFaultDataGas(appDefectData, context);
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Update Property Meter
                        if (appointment.property.meters != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            MeterDal meterDal = new MeterDal();
                            foreach (Meter meter in appointment.property.meters)
                                meterDal.saveMeter(meter, context);
                        }
                        #endregion

                    }
                    #endregion

                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);

                    // Inserting data for aborted appointments



                    if (appointment.appointmentStatus == "No Entry")
                    {
                        updateApplianceJournalForNoEntryStatus(appointment);
                    }
                    else
                    {
                        if (appointment.cp12Info != null)
                        {
                            updateLgsrData(appointment);
                        }
                    }

                    if (appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                    {
                        journalHistId = updateApplianceJournalForCompleteStatus(appointment);
                        if (journalHistId > 0)
                        {
                            updateJournalHeatingMappingForMainsGas(journalHistId, appointment);
                        }
                    }

                    //Update Appointment Data
                    completeAppointmentDataForApplianceServicing(appointment);

                    trans.Complete();
                }
                context.AcceptAllChanges();
                isSuccess = true;
                if (journalHistId > 0)
                {
                    try
                    {
                        var heatingIdList = appointment.cp12Info.Select(x => x.heatingId.ToString()).ToList();
                        var heatingIdsString = string.Empty;
                        if (heatingIdList.Count() > 0)
                        {
                            heatingIdsString = string.Join(",", heatingIdList);
                        }

                        DocumentHelper.populateCp12Document(appointment.journal, heatingIdsString, appointment.updatedBy.ToString());
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }

                return isSuccess;
            }
            catch (EntityException entityexception)
            {

                throw entityexception;
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region save Complete Appointment for Alternative Heating

        /// <summary>
        /// save Complete Appointment for Alternative Heating
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        public bool completeAppointmentForAlternativeHeating(AllAppointmentsList appointment)
        {
            bool isSuccess = false;

            if (appointment.appointmentStatus.Equals(ApplicationConstants.AbortedAppointmentStatus))
            {
                if (saveAbortEntry(appointment))
                {
                    completeAppointmentDataForApplianceServicing(appointment);
                    isSuccess = true;
                }
            }
            else
            {
                using (TransactionScope trans = new TransactionScope())
                {

                    #region "Saving survey information"
                    if (appointment.property != null)
                    {

                        #region Add/Update Alternative Heating(s) General and Inspection data
                        if (appointment.property.alternativeHeatings != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            AppliancesDal appliancesDal = new AppliancesDal();
                            foreach (AlternativeHeatingData heatingData in appointment.property.alternativeHeatings)
                            {

                                #region Add/Update current Heating
                                appliancesDal.saveAlternativeHeating(heatingData, appointment.updatedBy, context);
                                #endregion

                                #region Add/Update Alternative Heating Inspection
                                if (heatingData.airsourceInspection != null)
                                {
                                    heatingData.airsourceInspection.heatingId = heatingData.heatingId;
                                    heatingData.airsourceInspection.journalId = appointment.journalId;
                                    appliancesDal.saveAirSourceInspection(heatingData, appointment.updatedBy, context);
                                }
                                else if (heatingData.mvhrInspection != null)
                                {
                                    heatingData.mvhrInspection.heatingId = heatingData.heatingId;
                                    heatingData.mvhrInspection.journalId = appointment.journalId;
                                    appliancesDal.saveMVHRInspection(heatingData, appointment.updatedBy, context);
                                }
                                else if (heatingData.solarInspection != null)
                                {
                                    heatingData.solarInspection.heatingId = heatingData.heatingId;
                                    heatingData.solarInspection.journalId = appointment.journalId;
                                    appliancesDal.saveSolarInspection(heatingData, appointment.updatedBy, context);
                                }
                                #endregion

                            }
                        }
                        #endregion

                        #region "Save General Heating Checks"
                        saveGeneralHeatingChecks(appointment, appointment.updatedBy, context);
                        #endregion

                    }
                    #endregion

                    #region "Update property and customer details"

                    updatePropertyAndCustomerDetails(appointment);

                    #endregion

                    #region "Update Journal Data"

                    if (appointment.appointmentStatus.Equals(ApplicationConstants.NoEntryAppointmentStatus))
                    {
                        updateApplianceJournalForNoEntryStatus(appointment);
                    }
                    else if (appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                    {
                        updateLgsrData(appointment);                        

                        var journaHistortId = updateApplianceJournalForCompleteStatus(appointment);
                        if (journaHistortId > 0)
                        {
                            updateJournalHeatingMappingForAlternativeHeating(journaHistortId, appointment);
                        }
                    }

                    #endregion

                    #region "Update Appointment Data"
                    completeAppointmentDataForApplianceServicing(appointment);
                    #endregion

                    trans.Complete();
                }

                context.AcceptAllChanges();
                isSuccess = true;

            }

            return isSuccess;

        }

        #endregion

        #region Save Complete Appointment for Oil Heating

        /// <summary>
        /// save Complete Appointment for Oil Heating
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        public bool completeAppointmentForOilHeating(AllAppointmentsList appointment)
        {
            bool isSuccess = false;

            if (appointment.appointmentStatus.Equals(ApplicationConstants.AbortedAppointmentStatus))
            {
                if (saveAbortEntry(appointment))
                {
                    completeAppointmentDataForApplianceServicing(appointment);
                    isSuccess = true;
                }
            }
            else
            {
                using (TransactionScope trans = new TransactionScope())
                {

                    #region "Saving survey information"
                    if (appointment.property != null)
                    {

                        #region Add/Update Oil Heating(s) General and Inspection data
                        if (appointment.property.oilHeatings != null && appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                        {
                            AppliancesDal appliancesDal = new AppliancesDal();
                            foreach (OilHeatingData heatingData in appointment.property.oilHeatings)
                            {

                                #region Add/Update current Heating
                                appliancesDal.saveOilHeating(heatingData, appointment.updatedBy, context);
                                #endregion

                                #region Add/Update Oil Heating Inspection
                                if (heatingData.oilFiringAndService != null)
                                {
                                    heatingData.oilFiringAndService.heatingId = heatingData.heatingId;
                                    heatingData.oilFiringAndService.journalId = appointment.journalId;
                                    appliancesDal.saveOilFiringServiceAndChecks(heatingData, appointment.updatedBy, context);
                                }
                                #endregion

                                #region Add Oil Defects 
                                if (heatingData.oilDefects != null && heatingData.oilDefects.Count > 0)
                                {
                                    FaultsDal faultDal = new FaultsDal();
                                    foreach (FaultsDataGas oilDefect in heatingData.oilDefects)
                                    {                                        
                                        oilDefect.heatingId = heatingData.heatingId;
                                        faultDal.saveOilDefect(oilDefect, context);
                                    }
                                }
                                #endregion

                            }
                        }
                        #endregion

                    }
                    #endregion

                    #region "Update property and customer details"

                    updatePropertyAndCustomerDetails(appointment);

                    #endregion

                    #region "Update Journal Data"

                    if (appointment.appointmentStatus.Equals(ApplicationConstants.NoEntryAppointmentStatus))
                    {
                        updateApplianceJournalForNoEntryStatus(appointment);
                    }
                    else if (appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                    {
                        updateLgsrData(appointment);
                        var journaHistortId = updateApplianceJournalForCompleteStatus(appointment);
                        if (journaHistortId > 0)
                        {
                            updateJournalHeatingMappingForOil(journaHistortId, appointment);
                        }                        
                    }

                    #endregion

                    #region "Update Appointment Data"
                    completeAppointmentDataForApplianceServicing(appointment);
                    #endregion

                    trans.Complete();
                }

                context.AcceptAllChanges();
                isSuccess = true;

            }

            return isSuccess;

        }

        #endregion

        #region Save General Heating Checks
        public void saveGeneralHeatingChecks(AllAppointmentsList appt, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (appt.property.generalHeatingChecks != null)
            {

                if (currentContext != null) this.context = currentContext;

                GeneralHeatingChecks insp = appt.property.generalHeatingChecks;

                P_GENERAL_HEATING_CHECKS inspDb = new P_GENERAL_HEATING_CHECKS();
                inspDb.RadiatorCondition = insp.radiatorCondition;
                inspDb.HeatingControl = insp.heatingControl;
                inspDb.SystemUsage = insp.systemUsage;
                inspDb.GeneralHeating = insp.generalHeating;
                inspDb.AccessIssues = insp.accessIssues;
                inspDb.AccessIssueNotes = insp.accessIssueNotes;
                inspDb.Confirmation = insp.confirmation;
                inspDb.JournalId = appt.journalId;
                inspDb.CreationDate = insp.checkedDate;
                inspDb.CheckedBy = updatedBy;

                context.AddToP_GENERAL_HEATING_CHECKS(inspDb);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                if (currentContext == null) context.AcceptAllChanges();

            }
        }
        #endregion

        #region save Complete appointment for stock V2
        /// <summary>
        /// this function calls the method for Stock. and update (appointment Data,SurveyData and Customer data
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForStockV2(AllAppointmentsList appointment)
        {
            bool isSuccessful = false;
            try
            {
                SurveyDal surveyDal = new SurveyDal();
                using (TransactionScope trans = new TransactionScope())
                {
                    //Update Appointment Data
                    completeAppointmentForStock(appointment);

                    SurveyData appSurveyData = new SurveyData();
                    if (appointment.survey != null && appointment.survey.surveyData != null)
                    {
                        foreach (SurveyData survData in appointment.survey.surveyData)
                        {
                            survData.completedBy = appointment.updatedBy ?? 0;
                            string propertyId = survData.propertyId;
                            int customerId = survData.customerId;
                            int appointmentId = survData.appointmentId;
                            bool success = false;

                            bool isAppointmentExist = surveyDal.isAppointmentForPropertyExist(propertyId, appointmentId, context);

                            //if appointment does not exist then this is error
                            if (isAppointmentExist == false)
                            {
                                ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, true, MessageCodesConstants.PropertyOrCustomerOrAppointmentDoesNotExistCode);
                                throw new ArgumentException(String.Format(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, propertyId, customerId.ToString(), appointmentId.ToString()), "propertyId or customerId or appointmentId");
                            }
                            else
                            {
                                success = surveyDal.saveSurveyForm(survData, context);
                            }
                        }
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);
                    trans.Complete();
                }
                context.AcceptAllChanges();
                isSuccessful = true;
                //if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                //{
                //    int surveyId = surveyDal.getSurveyId(appointment.appointmentId, context);
                //    if (surveyId > 0)
                //    {
                //        try
                //        {
                //            string result = DocumentHelper.populatePropertyRecordInspection(appointment.property.propertyId, surveyId.ToString(), appointment.updatedBy.ToString());
                //            ErrorFaultSetGet.setErrorFault("+++Result++++++++++" + result + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                //        }
                //        catch (Exception ex)
                //        {
                //            ErrorFaultSetGet.setErrorFault("+++++Resultt++++++++" + ex.ToString() + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                //        }
                //    }
                //}
            }
            catch (EntityException entityexception)
            {

                throw entityexception;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return isSuccessful;
        }
        #endregion

        #region save complete Fault appointment V2

        /// <summary>
        /// save complete Fault appointment V2
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        public bool completeAppointmentForFaultV2(AllAppointmentsList appointment)
        {
            bool isSuccessful = false;
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment.jobDataList != null)
                    {

                        foreach (JobData jobData in appointment.jobDataList)
                        {
                            // Delete jobsheet history having statuses 'Accepted', 'Paused', 'Inprogress' inorder to avoid duplication in case of offline syncing
                            List<string> faultStatusesToBeDeleted = new List<string>() {AppointmentCompleteStatus.Accepted.ToString(),
                                        AppointmentCompleteStatus.InProgress.ToString(),
                                        AppointmentCompleteStatus.Paused.ToString()};
                            //Delete History from Paused table for Offline Syncing
                            var deletePausedData = (from paused in context.FL_FAULT_PAUSED
                                                    where paused.FaultLogId == jobData.faultLogID
                                                    select paused).ToList();

                            deletePausedData.ForEach(context.DeleteObject);
                            context.SaveChanges();

                            var deleteAppointmentHistoryList = (from flh in context.FL_FAULT_LOG_HISTORY
                                                                join fs in context.FL_FAULT_STATUS on flh.FaultStatusID equals fs.FaultStatusID
                                                                where faultStatusesToBeDeleted.Contains(fs.Description.Replace(" ", "")) && jobData.faultLogID == flh.FaultLogID
                                                                select flh).ToList();

                            deleteAppointmentHistoryList.ForEach(context.DeleteObject);
                            context.SaveChanges();

                            //Check job pause data exist
                            if (jobData.jobPauseHistory.Count > 0)
                            {
                                jobData.jobPauseHistory = jobData.jobPauseHistory.OrderBy(i => i.pauseDate).ToList();
                                foreach (JobPauseHistoryData jobPauseData in jobData.jobPauseHistory)
                                {
                                    //Step 1
                                    /*
                                     * Apply condition to save in faultPause only in case of a pause.                                     
                                     * Update FaultLog Status and add an entry in FaultLogHistory with time stamp (pauseDate).                                     * and remove the above process for other code places like inside appointment update functions.
                                     * for example: move/remove from function PausedAppointmentForFault
                                     * save fault paused history                                     
                                     */
                                    Nullable<int> faultLogHistoryId = null;
                                    faultLogHistoryId = this.updateFaultStatusId(context, jobData, jobData.faultLogID, jobPauseData.actionType, appointment.updatedBy, jobPauseData.pauseDate);

                                    //Step 2
                                    /*
                                     * Apply condition to save in faultPause only in case of a pause.                                     
                                     */
                                    Nullable<int> pauseID = null;
                                    if (jobPauseData.actionType == FaultJobProgressStatus.Paused.ToString())
                                        pauseID = saveFaultPaused(jobPauseData, jobData.faultLogID, faultLogHistoryId); // Exsisting implementation.                                    

                                    //Step 3
                                    /*
                                     * update FaultTIMESheet, in case of start/resume(restart) add new record for causes of pause/noentry/complete update latest previous record and update timesheet end time.
                                     * for noentry there may be a case, that the noentry is occoured at first step no data in faultTimeSheet will be recorded.
                                     * The process for the noentry will be implemented in noEntryFaultAppointment (at least for current process and service data).                                     
                                     */
                                    this.saveUpdateFaultTimeSheet(appointment.appointmentId, jobData.faultLogID, jobPauseData.pauseDate, jobPauseData.actionType, pauseID);

                                    //Step 4
                                    /*
                                     * For the case job is completed add repairs for current fault.
                                     * 
                                     */
                                    if (jobPauseData.actionType == FaultJobProgressStatus.Complete.ToString())
                                    {
                                        //check repair list exist
                                        if (jobData.faultRepairList.Count > 0)
                                        {
                                            saveFaultRepairList(jobData, appointment.assignedTo);
                                        }

                                        bool isAppointmentCompleted = false;
                                        if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                                        {
                                            isAppointmentCompleted = true;
                                            saveFaultAppointmentCompletionInfo(appointment);
                                        }
                                        if (jobData.completionDate == null)
                                            jobData.completionDate = DateTime.Now;

                                        System.Data.Objects.ObjectParameter result1 = new ObjectParameter("result", typeof(int));
                                        context.FL_CO_COMPLETE_APPOINTMENT(appointment.appointmentId, jobData.faultLogID, jobData.followOnNotes, jobData.repairNotes,
                                            jobData.completionDate, isAppointmentCompleted, jobData.completionDate, result1);
                                    }
                                }
                            }
                        }
                    }
                    if (appointment.appointmentStatus != "No Entry"
                                     || appointment.appointmentStatus.Replace(" ", "") != AppointmentCompleteStatus.NoEntry.ToString())
                    {
                        PausedAppointmentForFault(null, appointment);
                    }
                    else if (appointment.appointmentStatus == "No Entry")
                    {
                        noEntryFaultAppointment(appointment);
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);


                    if (appointment.faultReportedList != null && appointment.faultReportedList.Count() > 0)
                    {
                        isSuccessful = reportANewFault(appointment.faultReportedList, appointment);
                    }

                    context.AcceptAllChanges();
                    trans.Complete();
                    isSuccessful = true;
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }

            return isSuccessful;
        }


        public void saveFaultAppointmentCompletionInfo(AllAppointmentsList appointment)
        {
            FL_CO_APPOINTMENT faultApt = context.FL_CO_APPOINTMENT.Where(apt => apt.AppointmentID == appointment.appointmentId).First();

            if (faultApt != null)
            {
                if (appointment.appointmentCompletedAppVersion != null)
                {
                    faultApt.AppointmentCompletedAppVersion = appointment.appointmentCompletedAppVersion;
                }

                if (appointment.appointmentCurrentAppVersion != null)
                {
                    faultApt.AppointmentCurrentAppVersion = appointment.appointmentCurrentAppVersion;
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }

        public bool reportANewFault(List<FaultBasket> faultBasket, AllAppointmentsList appointment)
        {
            int itemStatusId = 1;
            if (faultBasket != null)
            {
                foreach (FaultBasket fault in faultBasket)
                {
                    var faultStatusList = context.FL_FAULT_STATUS.Where(pdrSts => pdrSts.Description == "Appointment To Be Arranged");

                    if (faultStatusList.Count() > 0)
                        itemStatusId = faultStatusList.First().FaultStatusID;
                    var faultTradeData = (from fl in context.FL_FAULT
                                          join trade in context.FL_FAULT_TRADE on fl.FaultID equals trade.FaultId
                                          where fl.FaultID == fault.faultId
                                          select new
                                          {
                                              faultTradeId = trade.FaultTradeId,
                                              duration = fl.duration,
                                              isRecharge = fl.Recharge
                                          });
                    if (faultTradeData.Count() > 0)
                    {
                        foreach (var trade in faultTradeData)
                        {

                            FL_TEMP_FAULT tempFault = new FL_TEMP_FAULT();
                            tempFault.CustomerID = appointment.defaultCustomerId;
                            tempFault.PROPERTYID = appointment.property != null ? appointment.property.propertyId : null;
                            tempFault.SchemeId = appointment.scheme != null ? appointment.scheme.schemeId : null;
                            tempFault.BlockId = appointment.scheme != null ? appointment.scheme.blockId : null;
                            tempFault.FaultID = fault.faultId;
                            tempFault.Quantity = 1;
                            tempFault.ISRECALL = false;
                            tempFault.ProblemDays = fault.problemDays;
                            tempFault.Recharge = trade.isRecharge;
                            tempFault.Notes = fault.notes;
                            tempFault.RecuringProblem = fault.isRecurring;
                            tempFault.FaultTradeId = trade.faultTradeId;
                            tempFault.Duration = (decimal)trade.duration;
                            tempFault.ItemStatusId = itemStatusId;
                            tempFault.UserId = appointment.updatedBy;
                            tempFault.SubmitDate = appointment.creationDate;
                            tempFault.areaId = fault.faultAreaId;
                            context.AddToFL_TEMP_FAULT(tempFault);
                            context.SaveChanges();
                        }
                    }
                }
            }
            return true;
        }

        #endregion

        #region save complete Planned appointment V2

        public bool completeAppointmentForPlannedV2(AllAppointmentsList appointment)
        {
            bool isSuccessful = false;
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment.componentTrade != null)
                    {
                        if (appointment.componentTrade.jobPauseHistory != null && appointment.componentTrade.jobPauseHistory.Count > 0)
                        {
                            var notStartedStatus = AppointmentCompleteStatus.NotStarted.ToString();

                            var deleteAppointmentPlannedTimeSheetList = context.PLANNED_JOBTIMESHEET
                                                                      .Where(x => x.APPOINTMENTID == appointment.appointmentId)
                                                                      .ToList();
                            deleteAppointmentPlannedTimeSheetList.ForEach(context.DeleteObject);
                            context.SaveChanges();
                            var deleteAppointmentPlannedPausedList = context.PLANNED_PAUSED
                                                                      .Where(x => x.APPOINTMENTID == appointment.appointmentId)
                                                                      .ToList();
                            deleteAppointmentPlannedPausedList.ForEach(context.DeleteObject);
                            context.SaveChanges();
                            var deleteAppointmentHistoryList = context.PLANNED_APPOINTMENTS_HISTORY
                                                                      .Where(x => x.APPOINTMENTID == appointment.appointmentId
                                                                          && x.APPOINTMENTSTATUS != notStartedStatus)
                                                                      .ToList();
                            deleteAppointmentHistoryList.ForEach(context.DeleteObject);
                            context.SaveChanges();

                            //Check job pause data exist
                            appointment.componentTrade.jobPauseHistory = appointment.componentTrade.jobPauseHistory.OrderBy(i => i.pauseDate).ToList();
                            foreach (JobPauseHistoryData jobPauseData in appointment.componentTrade.jobPauseHistory)
                            {
                                Nullable<int> appointmentHistoryID = null;

                                // update planned appointment with given data.
                                if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString() && jobPauseData.actionType == AppointmentCompleteStatus.Complete.ToString())
                                {
                                    completeAppointmentForPlanned(appointment, callfromAPIV2: true);
                                }
                                else
                                {

                                    appointmentHistoryID = updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.InProgress.ToString(), jobPauseData.actionType, appointment, jobPauseData);

                                }

                                // save fault paused history
                                Nullable<int> pauseID = null;
                                if (jobPauseData.actionType.ToLower() == AppointmentCompleteStatus.Paused.ToString().ToLower())
                                    pauseID = savePlannedPaused(jobPauseData, appointment.appointmentId, appointmentHistoryID);

                                // Insert update a record in Planned Job Sheet
                                insertUpdatePlannedJobTimeSheet(appointment.appointmentId, jobPauseData.pauseDate, jobPauseData.actionType, pauseID);
                            }
                        }
                        else
                        {
                            updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, appointment.appointmentStatus, AppointmentCompleteStatus.NotStarted.ToString(), appointment);
                        }
                    }
                    if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                    {
                        noEntryPlannedAppointment(appointment, callfromAPIV2: true);
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);
                    trans.Complete();
                }
                context.AcceptAllChanges();
            }
            catch (EntityException entityexception)
            {

                throw entityexception;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            isSuccessful = true;
            return isSuccessful;
        }

        #endregion

        #region update property and custumer details (for completeappointment call)
        /// <summary>
        /// update property and custumer details (for completeappointment call)
        /// </summary>
        /// <param name="appointment"></param>
        protected void updatePropertyAndCustomerDetails(List<CustomerData> customerList, PropertyData property)
        {
            if (customerList != null)
            {
                foreach (CustomerData customer in customerList)
                {
                    CustomerDal custDal = new CustomerDal();
                    custDal.UpdateCustomerContactFromAppointment(customer, context);
                }
            }
            if (property != null)
            {
                PropertyDal propDal = new PropertyDal();
                if (property.Accommodations != null)
                {
                    propDal.updatePropertyDimensions(property.Accommodations, property.propertyId, context);
                }
            }
        }
        #endregion

        #region update property and custumer details (for completeappointment call)
        protected void updatePropertyAndCustomerDetails(IAppointment appointment)
        {
            if (appointment.customerList != null)
            {
                foreach (CustomerData customer in appointment.customerList)
                {
                    CustomerDal custDal = new CustomerDal();
                    custDal.UpdateCustomerContactFromAppointment(customer, context);
                }
            }

            if (appointment.property != null)
            {
                PropertyDal propDal = new PropertyDal();
                if (appointment.property.Accommodations != null)
                {
                    propDal.updatePropertyDimensions(appointment.property.Accommodations, appointment.property.propertyId, context);
                }
            }
        }
        #endregion

        #region Update Fault Time Sheet

        /// <summary>
        /// Update Fault Time Sheet
        /// </summary>
        /// <param name="appointment"></param>
        private bool saveUpdateFaultTimeSheet(int appointmentId, int faultLogID, DateTime? pauseDate, string actionType, Nullable<int> pauseID = null)
        {
            FL_FAULT_JOBTIMESHEET faultJobTimeSheetData = new FL_FAULT_JOBTIMESHEET();
            if (actionType.ToLower() == FaultAppointmentProgressStatus.InProgress.ToString().ToLower())
            {
                faultJobTimeSheetData.APPOINTMENTID = appointmentId;
                faultJobTimeSheetData.FaultLogId = faultLogID;
                faultJobTimeSheetData.StartTime = pauseDate;
                context.AddToFL_FAULT_JOBTIMESHEET(faultJobTimeSheetData);
            }
            else
            {
                var faultJobTimeSheetList = context.FL_FAULT_JOBTIMESHEET.Where(flt => flt.FaultLogId == faultLogID && flt.EndTime == null).OrderByDescending(flt => flt.TimeSheetID);
                if (faultJobTimeSheetList.Count() > 0)
                {
                    faultJobTimeSheetData = faultJobTimeSheetList.First();
                    faultJobTimeSheetData.EndTime = pauseDate;
                    faultJobTimeSheetData.PauseID = pauseID;
                }
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            return true;
        }

        #endregion

        #region save Update Planned Job TimeSheet
        /// <summary>
        /// This function save or Update Planned Job TimeSheet in the database
        /// </summary>      
        /// <returns>the true if successfully save otherwise false </returns>
        public bool insertUpdatePlannedJobTimeSheet(int appointmentId, DateTime? actionDateTime, string actionType, Nullable<int> pauseID = null)
        {
            bool result = false;

            if (actionType.ToLower() == AppointmentCompleteStatus.InProgress.ToString().ToLower())
            {
                PLANNED_JOBTIMESHEET planned = new PLANNED_JOBTIMESHEET();
                planned.APPOINTMENTID = appointmentId;
                planned.StartTime = actionDateTime;
                planned.EndTime = null;
                context.AddToPLANNED_JOBTIMESHEET(planned);
            }
            else
            {
                var appData = context.PLANNED_JOBTIMESHEET.Where(app => app.APPOINTMENTID == appointmentId && app.EndTime == null).OrderByDescending(js => js.StartTime);
                if (appData.Count() > 0)
                {
                    PLANNED_JOBTIMESHEET planned = appData.First();
                    planned.EndTime = actionDateTime;
                    planned.PauseID = pauseID;
                }
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            result = true;

            return result;
        }
        #endregion

        #region save complete Appointment For Void
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        /// 

        // Method to log actual start and end time of void appointments
        private bool logActualStartAndEndDateTimeForVoidAppointment(string appointmentType, int appointmentId, int jobSheetId, DateTime? startDateTime, DateTime? endDatetime)
        {

            int type = context.PDR_MSATType.Where(x => x.MSATTypeName == appointmentType).FirstOrDefault().MSATTypeId;
            V_JOBTIMESHEET jobsheet = new V_JOBTIMESHEET();
            jobsheet.AppointmentId = appointmentId;
            jobsheet.JobSheetId = jobSheetId;
            jobsheet.MSATType = type;
            jobsheet.StartTime = startDateTime;
            jobsheet.EndTime = endDatetime;
            context.AddToV_JOBTIMESHEET(jobsheet);
            try
            {
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool logActualStartAndEndDateTimeForVoidWorksAppointment(string appointmentType, int appointmentId, int jobSheetId, DateTime? pauseDate, string actionType, int? pauseId)
        {

            int type = context.PDR_MSATType.Where(x => x.MSATTypeName == appointmentType).FirstOrDefault().MSATTypeId;

            V_JOBTIMESHEET jobsheet = new V_JOBTIMESHEET();
            if (actionType.ToLower() == FaultAppointmentProgressStatus.InProgress.ToString().ToLower())
            {
                jobsheet.AppointmentId = appointmentId;
                jobsheet.JobSheetId = jobSheetId;
                jobsheet.StartTime = pauseDate;
                jobsheet.MSATType = type;
                context.AddToV_JOBTIMESHEET(jobsheet);
            }
            else
            {
                var faultJobTimeSheetList = context.V_JOBTIMESHEET.Where(flt => flt.JobSheetId == jobSheetId && flt.EndTime == null).OrderByDescending(flt => flt.TimeSheetId);
                if (faultJobTimeSheetList.Count() > 0)
                {
                    jobsheet = faultJobTimeSheetList.First();
                    jobsheet.EndTime = pauseDate;
                    jobsheet.PauseId = pauseId;
                }
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            return true;

        }

        public bool completeAppointmentForVoidInspection(VoidInspectionAppointmentData appointment)
        {
            bool isSuccessful = false;
            int? electricCheckJournal = null, gasCheckJournal = null, asbestosEPCCheckJournal = null;
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment != null)
                    {

                        int journalId = appointment.journalId;
                        //Update journal Data
                        UpdatePdrJournalData(journalId, appointment.appointmentStatus, appointment.reletDate);
                        //Update C_Termination Data for Relet Date
                        UpdateTerminationData(journalId, appointment.reletDate, appointment.createdBy);
                        //Update Void Appointment Data
                        updateVoidAppointmentData(appointment.appointmentId, appointment.appointmentNotes, appointment.appointmentEndDateTime, appointment.appointmentStatus, journalId, appointment);
                        V_AppointmentRecordedData voidApptRecordedData = new V_AppointmentRecordedData();
                        var voidAppData = context.V_AppointmentRecordedData.Where(x => x.InspectionJournalId == journalId);
                        if (voidAppData.Count() > 0)
                        {
                            voidApptRecordedData = voidAppData.First();
                        }
                        //Update Data for Gas,Electric,EPC and Asbestos , Update Major Void Works
                        if (appointment.majorWorksRequired != null || appointment.worksRequired != null)
                        {
                            //Add Entry to Journal for Gas,Electric,EPC and Asbestos 
                            if (appointment.worksRequired != null)
                            {
                                //FIRST OF ALL CHECK EITHER RECORD IS ALREADY INSERTED IN PDR_JOURNAL FOR GAS APPOINTMENT / ELECTRIC APPOINTMENT 
                                if (appointment.customerList.Count() > 0)
                                {
                                    V_BritishGasVoidNotification gasElectricData = new V_BritishGasVoidNotification();
                                    int customerId = (int)appointment.customerList[0].customerId;
                                    var britishGasData = context.V_BritishGasVoidNotification.Where(x => x.PropertyId == appointment.property.propertyId && x.CustomerId == customerId && x.TenancyId == appointment.tenancyId);
                                    if (britishGasData.Count() > 0)
                                    {
                                        gasElectricData = britishGasData.First();
                                    }
                                    if (appointment.isElectricCheckRequired == true)
                                    {
                                        if (voidApptRecordedData.ElectricCheckJournalId > 0 || gasElectricData.ElectricCheckJournalId > 0)
                                        {
                                            if (gasElectricData.ElectricCheckJournalId > 0)
                                                electricCheckJournal = gasElectricData.ElectricCheckJournalId;
                                            else
                                                electricCheckJournal = voidApptRecordedData.ElectricCheckJournalId;
                                        }
                                        else
                                        {
                                            electricCheckJournal = createVoidJournalAppointment(appointment, VoidAppointmentTypes.VoidElectricCheck.ToString());
                                            updateBritishGasVoidNotificationData(appointment, VoidAppointmentTypes.VoidElectricCheck.ToString(), electricCheckJournal, null);
                                        }
                                    }

                                    if (appointment.isGasCheckRequired == true)
                                    {
                                        if (voidApptRecordedData.GasCheckJournalId > 0 || gasElectricData.GasCheckJournalId > 0)
                                        {
                                            if (gasElectricData.GasCheckJournalId > 0)

                                                gasCheckJournal = gasElectricData.GasCheckJournalId;
                                            else
                                                gasCheckJournal = voidApptRecordedData.GasCheckJournalId;
                                        }
                                        else
                                        {
                                            gasCheckJournal = createVoidJournalAppointment(appointment, VoidAppointmentTypes.VoidGasCheck.ToString());
                                            updateBritishGasVoidNotificationData(appointment, VoidAppointmentTypes.VoidGasCheck.ToString(), null, gasCheckJournal);
                                        }

                                    }
                                }
                                //if (appointment.isEPCCheckRequired == true || appointment.isAsbestosCheckRequired == true)
                                //{

                                //    if (voidApptRecordedData.IsEpcCheckRequired )
                                //    {
                                //        asbestosEPCCheckJournal = (int)voidApptRecordedData.GasCheckJournalId;
                                //    }
                                //    else
                                //    {
                                //        asbestosEPCCheckJournal = CreateVoidJournalAppointment(appointment, "Asbestos");
                                //    }
                                //}
                            }

                            updateAppointmentRecordedData(journalId, appointment, electricCheckJournal, gasCheckJournal, asbestosEPCCheckJournal);

                            // Logging actual start and end date time
                            logActualStartAndEndDateTimeForVoidAppointment(appointment.appointmentType, appointment.appointmentId, journalId, Convert.ToDateTime(appointment.appointmentActualStartTime), Convert.ToDateTime(appointment.appointmentActualEndTime));

                        }
                        //Update Paint Data
                        if (appointment.paintPacks != null)
                        {
                            updateVoidPaintData((int)appointment.journalId, appointment);
                        }
                    }
                    if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                    {
                        //Add No Entry Detail
                        noEntryVoidAppointment(appointment.appointmentId, appointment.appointmentEndDate, appointment.noEntryNotes, appointment.journalId, appointment.updatedBy, appointment.loggedDate, appointment.appointmentType);
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment.customerList, appointment.property);
                    //Insert record for post void inspection
                    if (appointment.customerList.Count() > 0)
                    {
                        createPostVoidInspection(appointment);
                    }
                    trans.Complete();
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            isSuccessful = true;
            return isSuccessful;
        }

        private void updateBritishGasVoidNotificationData(VoidInspectionAppointmentData appointment, string type, int? electricCheckJournal, int? gasCheckJournal)
        {
            V_BritishGasVoidNotification gasElectricData = new V_BritishGasVoidNotification();
            int customerId = (int)appointment.customerList[0].customerId;
            var msatTypeData = context.V_BritishGasVoidNotification.Where(x => x.PropertyId == appointment.property.propertyId && x.CustomerId == customerId && x.TenancyId == appointment.tenancyId);
            if (msatTypeData.Count() > 0)
            {
                gasElectricData = msatTypeData.FirstOrDefault();
                if (type.Replace(" ", "") == VoidAppointmentTypes.VoidGasCheck.ToString())
                {
                    gasElectricData.GasCheckJournalId = gasCheckJournal;
                }
                else if (type.Replace(" ", "") == VoidAppointmentTypes.VoidElectricCheck.ToString())
                {
                    gasElectricData.ElectricCheckJournalId = electricCheckJournal;
                }
                context.SaveChanges();
            }
        }
        private int pausedVoidAppointment(VoidPauseData voidPauseData, int requiredWorksId)
        {
            int pauseID = 0;
            V_PauseWorks voidPaused = new V_PauseWorks();
            voidPaused.RequiredWorksId = requiredWorksId;
            voidPaused.PausedOn = voidPauseData.pauseDate;
            voidPaused.Notes = voidPauseData.pauseNote;
            voidPaused.Reason = voidPauseData.pauseReason;
            voidPaused.PausedBy = voidPauseData.pausedBy;
            context.AddToV_PauseWorks(voidPaused);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            pauseID = voidPaused.PauseWorksId;
            return pauseID;
        }

        private void saveAddRepairData(VoidWorksAppointmentData appointment)
        {
            if (appointment.jobDataList != null)
            {
                foreach (VoidRecordWorksRequired repairJob in appointment.jobDataList)
                {


                    List<string> faultStatusesToBeDeleted = new List<string>() {AppointmentCompleteStatus.Accepted.ToString(),
                                        AppointmentCompleteStatus.InProgress.ToString(),
                                        AppointmentCompleteStatus.Paused.ToString()};

                    var deleteAppointmentTimeSheetList = context.V_JOBTIMESHEET
                                                                      .Where(x => x.AppointmentId == appointment.appointmentId)
                                                                      .ToList();
                    deleteAppointmentTimeSheetList.ForEach(context.DeleteObject);
                    context.SaveChanges();

                    var deletePausedAppointmentList = (from ppadh in context.V_PauseWorks
                                                       where ppadh.RequiredWorksId == repairJob.roomId
                                                       select ppadh).ToList();

                    deletePausedAppointmentList.ForEach(context.DeleteObject);
                    context.SaveChanges();

                    var deleteAppointmentHistoryList = (from flh in context.V_RequiredWorksHistory
                                                        join fs in context.PDR_STATUS on flh.StatusId equals fs.STATUSID
                                                        where faultStatusesToBeDeleted.Contains(fs.AppTitle.Replace(" ", "")) && repairJob.roomId == flh.RequiredWorksId
                                                        select flh).ToList();

                    deleteAppointmentHistoryList.ForEach(context.DeleteObject);
                    context.SaveChanges();

                    //Update work status 
                    if (repairJob.workPauseHistory.Count > 0)
                    {
                        repairJob.workPauseHistory = repairJob.workPauseHistory.OrderBy(i => i.pauseDate).ToList();
                        foreach (VoidPauseData jobPauseData in repairJob.workPauseHistory)
                        {
                            V_RequiredWorks voidRecordWorks = new V_RequiredWorks();
                            var voidRecordWorksData = context.V_RequiredWorks.Where(x => x.WorksJournalId == appointment.journalId && x.RequiredWorksId == repairJob.roomId);
                            if (voidRecordWorksData.Count() > 0)
                            {

                                voidRecordWorks = voidRecordWorksData.First();

                                voidRecordWorks.StatusId = getPdrStatusId(jobPauseData.actionType);
                                voidRecordWorks.isLegionella = repairJob.isLegionella;
                                voidRecordWorks.ModifiedDate = jobPauseData.pauseDate;

                                saveVoidJobSheetInfo(repairJob, voidRecordWorks);
                                context.SaveChanges();
                            }
                        }
                    }
                    //Add Data in V_VoidWorksRepair table

                    foreach (FaultRepairData repairId in repairJob.voidRepairList)
                    {
                        V_VoidWorksRepair repairWorks = new V_VoidWorksRepair();
                        repairWorks.CreatedDate = appointment.loggedDate;
                        repairWorks.FollowOnNotes = repairJob.followOnNotes;
                        if (!string.IsNullOrEmpty(repairJob.followOnNotes))
                        {
                            repairWorks.IsFollowOnWorksRequired = true;
                        }
                        repairWorks.WorksJournalId = appointment.journalId;
                        repairWorks.RepairNotes = repairJob.repairNotes;
                        repairWorks.RepairId = repairId.faultRepairId;

                        context.AddToV_VoidWorksRepair(repairWorks);
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    }


                    //Check job pause data exist
                    if (repairJob.workPauseHistory.Count > 0)
                    {
                        repairJob.workPauseHistory = repairJob.workPauseHistory.OrderBy(i => i.pauseDate).ToList();
                        foreach (VoidPauseData jobPauseData in repairJob.workPauseHistory)
                        {
                            Nullable<int> pauseID = null;
                            // log pause history
                            if (jobPauseData.actionType == "Paused")
                            {
                                pauseID = pausedVoidAppointment(jobPauseData, (int)repairJob.roomId);
                            }

                            logActualStartAndEndDateTimeForVoidWorksAppointment(appointment.appointmentType, (int)appointment.appointmentId, (int)repairJob.roomId, jobPauseData.pauseDate, jobPauseData.actionType, pauseID);

                        }
                    }
                }
            }
        }

        private void saveVoidJobSheetInfo(VoidRecordWorksRequired repairJob, V_RequiredWorks voidRecordWorks)
        {
            int completeStatusId = getPdrStatusId(AppointmentCompleteStatus.Complete.ToString());
            int noEntryStatusId = getPdrStatusId(AppointmentCompleteStatus.NoEntry.ToString());
            int currentJobStatus = getPdrStatusId(repairJob.workStatus);
            if (completeStatusId == currentJobStatus
                || noEntryStatusId == currentJobStatus)
            {
                if (repairJob.completionDate != null)
                {
                    voidRecordWorks.JobsheetCompletionDate = repairJob.completionDate;
                }

                if (repairJob.jsCurrentAppVersion != null)
                {
                    voidRecordWorks.JobsheetCurrentAppVersion = repairJob.jsCurrentAppVersion;
                }

                if (repairJob.jsCompletedAppVersion != null)
                {
                    voidRecordWorks.JobsheetCompletedAppVersion = repairJob.jsCompletedAppVersion;
                }
            }

        }

        private void updateGasElectricCheckAppointment(int journalId, VoidGasElectricData appointment, string type)
        {
            V_BritishGasVoidNotification gasElectricData = new V_BritishGasVoidNotification();
            int customerId = (int)appointment.customerList[0].customerId;
            var msatTypeData = context.V_BritishGasVoidNotification.Where(x => x.PropertyId == appointment.property.propertyId && x.CustomerId == customerId && x.TenancyId == appointment.tenancyId);
            if (msatTypeData.Count() > 0)
            {
                gasElectricData = msatTypeData.FirstOrDefault();
                gasElectricData.UserId = appointment.updatedBy;
                if (type.Replace(" ", "") == VoidAppointmentTypes.VoidGasCheck.ToString())
                {
                    gasElectricData.GasCheckJournalId = journalId;
                    gasElectricData.GasMeterReading = appointment.meterReading;
                    gasElectricData.GasMeterTypeId = appointment.meterTypeId;
                    gasElectricData.GasMeterReadingDate = appointment.loggedDate;
                    gasElectricData.TenantType = appointment.tenantType;


                    recordMeterReadingDates(appointment, "Reading Date", "Gas");
                    recordMeterReadingAttributeData(appointment, appointment.meterTypeId.ToString(), "Meter Type", "Gas", appointment.meterTypeId);
                    recordMeterReadingAttributeData(appointment, appointment.meterReading.ToString(), "Reading", "Gas");
                    recordMeterReadingAttributeData(appointment, appointment.meterLocation, "Location", "Gas");
                }
                else if (type.Replace(" ", "") == VoidAppointmentTypes.VoidElectricCheck.ToString())
                {
                    gasElectricData.ElectricCheckJournalId = journalId;
                    gasElectricData.ElectricMeterReading = appointment.meterReading;
                    gasElectricData.ElectricMeterTypeId = appointment.meterTypeId;
                    gasElectricData.ElectricMeterReadingDate = appointment.loggedDate;
                    gasElectricData.TenantType = appointment.tenantType;
                    recordMeterReadingDates(appointment, "Reading Date", "Electric");
                    recordMeterReadingAttributeData(appointment, appointment.meterTypeId.ToString(), "Meter Type", "Electric", appointment.meterTypeId);
                    recordMeterReadingAttributeData(appointment, appointment.meterReading.ToString(), "Reading", "Electric");
                    recordMeterReadingAttributeData(appointment, appointment.meterLocation, "Location", "Electric");
                }


                context.SaveChanges();
            }


        }

        private int createVoidJournalAppointment(VoidInspectionAppointmentData appointment, string type)
        {
            int journalId = 0, msatId = 0, msatTypeId = 0;
            //Get MSAT Type Id for MSATTypeId in Journal Table
            var msatTypeData = context.PDR_MSATType.Where(x => x.MSATTypeName.Replace(" ", "") == type.Replace(" ", ""));
            if (msatTypeData.Count() > 0)
            {
                var MsatType = msatTypeData.FirstOrDefault();
                msatTypeId = MsatType.MSATTypeId;
            }
            PDR_MSAT msat = new PDR_MSAT();
            msat.PropertyId = appointment.property.propertyId;
            msat.TenancyId = appointment.tenancyId;
            msat.CustomerId = appointment.customerList[0].customerId;
            msat.IsActive = true;
            msat.MSATTypeId = msatTypeId;
            msat.TerminationDate = appointment.terminationDate;
            msat.ReletDate = appointment.reletDate;
            context.AddToPDR_MSAT(msat);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            msatId = msat.MSATId;
            //Add Data to PDR_Journal
            PDR_JOURNAL journal = new PDR_JOURNAL();
            journal.CREATIONDATE = appointment.loggedDate;
            journal.CREATEDBY = appointment.updatedBy;
            journal.STATUSID = getPdrStatusId("To be Arranged");
            journal.MSATID = msatId;
            //Status
            context.AddToPDR_JOURNAL(journal);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            journalId = journal.JOURNALID;

            return journalId;
        }

        private void updateVoidPaintData(int journalId, VoidInspectionAppointmentData voidAppointmentData)
        {
            V_PaintPack pPackData = new V_PaintPack();
            int paintPackId = 0;
            int paintStatusId = getPaintStatusId("Logged");
            var voidAppData = context.V_PaintPack.Where(x => x.InspectionJournalId == journalId);
            if (voidAppData.Count() > 0)
            {
                pPackData = voidAppData.First();
            }

            pPackData.CreatedBy = voidAppointmentData.updatedBy;
            pPackData.InspectionJournalId = journalId;
            pPackData.CreatedBy = voidAppointmentData.updatedBy;
            pPackData.StatusId = paintStatusId;

            if (voidAppData.Count() > 0)
            {
                pPackData.ModifiedDate = voidAppointmentData.loggedDate;
                paintPackId = pPackData.PaintPackId;
                context.SaveChanges();
            }
            else
            {
                pPackData.CreatedDate = voidAppointmentData.loggedDate;
                context.AddToV_PaintPack(pPackData);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                paintPackId = pPackData.PaintPackId;
            }

            List<V_PaintPackDetails> paintList = new List<V_PaintPackDetails>();
            var existingPaintPackData = context.V_PaintPackDetails.Where(x => x.PaintPackId == paintPackId);
            if (existingPaintPackData.Count() > 0)
            {
                paintList = existingPaintPackData.ToList();
                foreach (V_PaintPackDetails p in paintList)
                {
                    context.V_PaintPackDetails.DeleteObject(p);
                    context.SaveChanges();

                }

            }
            foreach (int r in voidAppointmentData.paintPacks.roomList)
            {
                V_PaintPackDetails paintPackDetails = new V_PaintPackDetails();
                var paintPackDetailsData = context.V_PaintPackDetails.Where(x => x.PaintPackId == paintPackId && x.RoomId == r);
                if (paintPackDetailsData.Count() > 0)
                {
                    paintPackDetails = paintPackDetailsData.First();
                    context.SaveChanges();
                }
                else
                {
                    paintPackDetails.RoomId = r;
                    paintPackDetails.PaintPackId = paintPackId;
                    context.AddToV_PaintPackDetails(paintPackDetails);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
            }

        }

        private void updateAppointmentRecordedData(int journalId, VoidInspectionAppointmentData voidAppointmentData, int? electricCheckJournal, int? gasCheckJournal, int? asbestosEPCCheckJournal)
        {

            //TODO: Split this function
            #region Appointment Recorded Data
            V_AppointmentRecordedData AppData = new V_AppointmentRecordedData();
            var voidAppData = context.V_AppointmentRecordedData.Where(x => x.InspectionJournalId == journalId);
            if (voidAppData.Count() > 0)
            {
                AppData = voidAppData.First();
                AppData.ModifiedDate = voidAppointmentData.loggedDate;
            }
            AppData.IsWorksRequired = true;//(bool)voidAppointmentData.worksRequired.isWorksRequired;
            AppData.IsGasCheckRequired = (bool)voidAppointmentData.isGasCheckRequired;
            AppData.IsElectricCheckRequired = (bool)voidAppointmentData.isElectricCheckRequired;
            AppData.IsEpcCheckRequired = (bool)voidAppointmentData.isEPCCheckRequired;
            AppData.IsAbestosCheckRequired = (bool)voidAppointmentData.isAsbestosCheckRequired;
            AppData.IsMajorWorksRequired = (bool)voidAppointmentData.majorWorksRequired.isMajorWorkRequired;
            AppData.IsPaintPackAssistance = (bool)voidAppointmentData.paintPacks.isPaintPackRequired;
            AppData.InspectionJournalId = journalId;
            AppData.ElectricCheckJournalId = electricCheckJournal;
            AppData.GasCheckJournalId = gasCheckJournal;
            AppData.ReletDate = voidAppointmentData.reletDate;

            if (voidAppData.Count() > 0)
            {
                context.SaveChanges();
            }
            else
            {
                AppData.CreatedDate = voidAppointmentData.loggedDate;
                AppData.CreatedBy = voidAppointmentData.createdBy;
                context.AddToV_AppointmentRecordedData(AppData);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

            }
            #endregion

            #region add Work Required
            if (AppData.IsWorksRequired)
            {
                addWorkRequired(voidAppointmentData);
            }
            #endregion
            #region Add Major work required
            if (AppData.IsMajorWorksRequired)
            {
                addMajorWorkRequired(voidAppointmentData);
            }
            #endregion

            if (voidAppointmentData.appointmentStatus.Equals(AppointmentCompleteStatus.Complete.ToString()) && voidAppointmentData.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidInspection.ToString())
            {
                // Hardcode strings forced to use by Omer Nasir
                string strStandardVoidWorks = getStandardVoidWorksInformation();
                string strSaniCleanInfo = getSaniCleanInformation();
                addDefaultWorksRequired(voidAppointmentData, ApplicationConstants.SaniClean, strSaniCleanInfo);
                addDefaultWorksRequired(voidAppointmentData, ApplicationConstants.StandardVoidWorks, strStandardVoidWorks);
            }

        }

        private void addWorkRequired(VoidInspectionAppointmentData voidAppointmentData)
        {
            foreach (VoidRecordRequiredWorks vWork in voidAppointmentData.worksRequired.recordWorks)
            {
                V_RequiredWorks voidRecordWorks = new V_RequiredWorks();
                var voidRecordWorksData = context.V_RequiredWorks.Where(x => x.RequiredWorksId == vWork.worksId);
                if (voidRecordWorksData.Count() > 0)
                {
                    voidRecordWorks = voidRecordWorksData.First();
                }

                var faultDetails = (from fault in context.FL_FAULT
                                    where fault.FaultID == vWork.faultId
                                    select new VoidRecordWorksRequired
                                    {
                                        duration = fault.duration,
                                        workDescription = fault.Description
                                    });

                var repairDetails = (from repair in context.FL_FAULT_REPAIR_LIST
                                     where repair.FaultRepairListID == vWork.repairId
                                     select new
                                     {
                                         duration = ApplicationConstants.DefaultRepairDuration,
                                         workDescription = repair.Description,
                                         estimate = repair.Gross
                                     });


                Double workDuration = 0;
                String workDescription = "";
                double? estimate = null;
                if (faultDetails.Count() > 0)
                {
                    workDuration = (Double)(faultDetails.FirstOrDefault()).duration;
                    workDescription = faultDetails.FirstOrDefault().workDescription;
                }
                else if (repairDetails.Count() > 0)
                {
                    workDuration = (Double)(repairDetails.FirstOrDefault()).duration;
                    workDescription = repairDetails.FirstOrDefault().workDescription;
                    estimate = repairDetails.FirstOrDefault().estimate;
                }

                //voidRecordWorks.RoomId = vWork.roomId;
                voidRecordWorks.WorkDescription = workDescription;
                voidRecordWorks.faultAreaID = vWork.faultAreaId;
                voidRecordWorks.faultID = vWork.faultId;
                voidRecordWorks.repairId = vWork.repairId;
                voidRecordWorks.isRecurringProblem = vWork.isRecurringProblem;
                voidRecordWorks.Duration = workDuration;
                voidRecordWorks.faultNotes = vWork.repairNotes;
                voidRecordWorks.problemDays = vWork.problemDays;
                voidRecordWorks.TenantNeglectEstimation = (decimal?)estimate;
                voidRecordWorks.IsBrsWorks = vWork.isBRSWork;
                voidRecordWorks.IsTenantWorks = vWork.isTenantWork;
                voidRecordWorks.IsVerified = vWork.isVerified;
                voidRecordWorks.IsCanceled = false;
                if (vWork.isBRSWork == true)
                {
                    vWork.estimate = voidRecordWorks.TenantNeglectEstimation;
                    vWork.workDescription = workDescription;
                }
                if (voidRecordWorksData.Count() > 0)
                {
                    voidRecordWorks.ModifiedDate = voidAppointmentData.loggedDate;
                    context.SaveChanges();
                }
                else
                {
                    voidRecordWorks.CreatedDate = voidAppointmentData.loggedDate;
                    voidRecordWorks.InspectionJournalId = voidAppointmentData.journalId;
                    voidRecordWorks.StatusId = getPdrStatusId("To be Arranged");
                    context.AddToV_RequiredWorks(voidRecordWorks);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
            }



        }

        private void addDefaultWorksRequired(VoidInspectionAppointmentData voidAppointmentData, string workType, string workDescription)
        {

            V_RequiredWorks recordWorks = new V_RequiredWorks();
            recordWorks.WorkDescription = workDescription;
            recordWorks.workType = workType;
            recordWorks.IsBrsWorks = true;
            recordWorks.IsTenantWorks = false;
            recordWorks.IsVerified = false;
            recordWorks.IsCanceled = false;
            recordWorks.CreatedDate = voidAppointmentData.loggedDate;
            recordWorks.InspectionJournalId = voidAppointmentData.journalId;
            recordWorks.StatusId = getPdrStatusId("To be Arranged");
            context.AddToV_RequiredWorks(recordWorks);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

        }

        private void addMajorWorkRequired(VoidInspectionAppointmentData voidAppointmentData)
        {

            foreach (VoidRecordMajorWorks mWork in voidAppointmentData.majorWorksRequired.recordMajorWork)
            {
                V_RequiredWorks voidMajorWorks = new V_RequiredWorks();
                var voidMajorWorksData = context.V_RequiredWorks.Where(x => x.InspectionJournalId == voidAppointmentData.journalId && x.ComponentId == mWork.componentId);
                if (voidMajorWorksData.Count() > 0)
                {
                    voidMajorWorks = voidMajorWorksData.First();
                }
                voidMajorWorks.ComponentId = mWork.componentId;
                voidMajorWorks.Condition = mWork.condition;
                voidMajorWorks.ReplacementDue = mWork.replacementDue;
                voidMajorWorks.InspectionJournalId = voidAppointmentData.journalId;
                voidMajorWorks.MajorWorkNotes = mWork.notes;
                voidMajorWorks.IsCanceled = false;
                voidMajorWorks.IsMajorWorksRequired = true;
                if (voidMajorWorksData.Count() > 0)
                {
                    context.SaveChanges();
                }
                else
                {
                    context.AddToV_RequiredWorks(voidMajorWorks);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
                // Add Replacement due and Condition rating in PA_PROPERTY_ATTRIBUTES AND PA_PROPERTY_ITEM_DATES Tables
                recordReplacementDueMajorWork(mWork, voidAppointmentData);
                recordConditionRatingMajorWork(mWork, voidAppointmentData);
            }


        }

        private void createPostVoidInspection(VoidInspectionAppointmentData voidAppointmentData)
        {
            if (voidAppointmentData.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
            {
                var _varMajorWork = voidAppointmentData.majorWorksRequired.recordMajorWork.Where(p => p.replacementDue > voidAppointmentData.reletDate);
                var _varWork = voidAppointmentData.worksRequired.recordWorks.Where(p => p.isTenantWork == true);
                if (_varMajorWork.Count() > 0)
                {
                    //The status of property will changed from “Let” with sub-status “Pending Termination” to status “Un-available” with sub-status “Under Major Repair”.  
                    updatePropertyStatus(voidAppointmentData.property.propertyId, "Unavailable", "Under Major Repair");
                }
                else if ((_varMajorWork.Count() == 0 || _varWork.Count() > 0) && voidAppointmentData.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidInspection.ToString())
                {
                    createVoidJournalAppointment(voidAppointmentData, VoidAppointmentTypes.PostVoidInspection.ToString());
                }
                //the works that have a Tenant Neglect Estimation allocated to them will be added to the tenants account as a recharge. 
                decimal? totalTenantNeglectEstimation = voidAppointmentData.worksRequired.recordWorks.Sum(x => Convert.ToDecimal(x.estimate));

                //create recharges and sale invoices for void works
                if (totalTenantNeglectEstimation > 0 && voidAppointmentData.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidInspection.ToString())
                {
                    F_RENTJOURNAL rentJournal = new F_RENTJOURNAL();
                    int itemTypeId = context.F_ITEMTYPE.Where(x => x.DESCRIPTION == "Recharges").FirstOrDefault().ITEMTYPEID;
                    int transactionStatusId = context.F_TRANSACTIONSTATUS.Where(x => x.DESCRIPTION == "Due").FirstOrDefault().TRANSACTIONSTATUSID;
                    rentJournal.TENANCYID = (int)voidAppointmentData.tenancyId;
                    rentJournal.TRANSACTIONDATE = voidAppointmentData.loggedDate;
                    rentJournal.ITEMTYPE = itemTypeId;
                    rentJournal.STATUSID = transactionStatusId;
                    rentJournal.AMOUNT = totalTenantNeglectEstimation;
                    rentJournal.ISDEBIT = 1;
                    rentJournal.PAYMENTSTARTDATE = voidAppointmentData.terminationDate;
                    rentJournal.PAYMENTENDDATE = voidAppointmentData.reletDate;
                    context.AddToF_RENTJOURNAL(rentJournal);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    int? rentJournalID = context.F_RENTJOURNAL.Where(appHistory => appHistory.TENANCYID == rentJournal.TENANCYID).Max(appHistory => appHistory.JOURNALID);

                    ///////////////////////////////////////////////////////

                    F_SALESINVOICE SI = new F_SALESINVOICE();
                    SI.SONAME = "TENANT RECHARGE";
                    SI.SODATE = DateTime.Now.Date;
                    SI.SONOTES = "Automated Tenant Recharge for Void Works";
                    SI.USERID = (int)voidAppointmentData.updatedBy;
                    SI.TENANCYID = (int)voidAppointmentData.tenancyId;
                    SI.ACTIVE = true;
                    SI.SOTYPE = 1;
                    SI.SOSTATUS = 1;
                    context.F_RechargesToSalesInvoice(SI.SOTYPE, SI.TENANCYID, SI.SODATE, SI.USERID, SI.ACTIVE, SI.SOSTATUS, SI.SONOTES, SI.SONAME);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    int? SaleID = context.F_SALESINVOICE.Where(appHistory => appHistory.TENANCYID == SI.TENANCYID).Max(appHistory => appHistory.SALEID);

                    ///////////////////////////////////////////////////////

                    int SALEITEMID = 0;
                    int? repairID = 0;
                    double? netCost = 0;
                    int? vatRateID = 0;
                    double? vat = 0;
                    double? gross = 0;
                    string description = "";
                    F_SALESINVOICEITEM salesInvoiceItem;
                    F_SALEITEM_TO_PURCHASEITEM saleToPurchase;
                    for (int i = 0; i < voidAppointmentData.worksRequired.recordWorks.Count(); i++)
                    {
                        if (voidAppointmentData.worksRequired.recordWorks.ElementAt(i).isBRSWork == true)
                        {
                            netCost = vat = gross = vatRateID = SALEITEMID = 0;
                            description = "";
                            repairID = voidAppointmentData.worksRequired.recordWorks.ElementAt(i).repairId;
                            var repairInfo = (from repair in context.FL_FAULT_REPAIR_LIST
                                              join voidWorks in context.V_RequiredWorks on repair.FaultRepairListID equals voidWorks.repairId
                                              where repair.RepairActive == true && repair.FaultRepairListID == repairID
                                              select new
                                              {
                                                  NetCost = repair.NetCost,
                                                  VatType = repair.VatRateID,
                                                  rVAT = repair.Vat,
                                                  GrossCost = repair.Gross,
                                                  FaultDescription = repair.Description
                                              }).Distinct();
                            if (repairInfo.Count() > 0)
                            {
                                netCost = repairInfo.First().NetCost;
                                vatRateID = repairInfo.First().VatType;
                                vat = repairInfo.First().rVAT;
                                gross = repairInfo.First().GrossCost;
                                description = repairInfo.First().FaultDescription;
                            }

                            salesInvoiceItem = new F_SALESINVOICEITEM();
                            salesInvoiceItem.SALEID = SaleID;
                            salesInvoiceItem.SALESCATID = 1;
                            salesInvoiceItem.ITEMNAME = voidAppointmentData.worksRequired.recordWorks.ElementAt(i).workDescription;
                            salesInvoiceItem.ITEMDESC = voidAppointmentData.worksRequired.recordWorks.ElementAt(i).workDescription;
                            salesInvoiceItem.SIDATE = DateTime.Now;
                            salesInvoiceItem.NETCOST = (decimal)netCost;
                            salesInvoiceItem.VATTYPE = vatRateID;
                            salesInvoiceItem.VAT = (decimal)vat;
                            salesInvoiceItem.GROSSCOST = (decimal)gross;
                            salesInvoiceItem.USERID = (int)voidAppointmentData.updatedBy; ;
                            salesInvoiceItem.ACTIVE = 1;
                            salesInvoiceItem.SITYPE = 1;
                            salesInvoiceItem.SISTATUS = 1;
                            salesInvoiceItem.RENTJOURNALID = rentJournalID;
                            context.AddToF_SALESINVOICEITEM(salesInvoiceItem);
                            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                            SALEITEMID = context.F_SALESINVOICEITEM.Where(appHistory => appHistory.SALEID == salesInvoiceItem.SALEID).Max(appHistory => appHistory.SALEITEMID);

                            saleToPurchase = new F_SALEITEM_TO_PURCHASEITEM();
                            saleToPurchase.SALEITEMID = SALEITEMID;
                            context.AddToF_SALEITEM_TO_PURCHASEITEM(saleToPurchase);
                            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        }
                    }
                    context.NL_SALESORDER(SaleID);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
            }
        }

        private int? updateVoidAppointmentData(int appointmentId, string customerNotes, DateTime? appointmentEndDateTime, string appStatus, int journalId, IAppointment appointment)
        {
            long? journalHistoryID = null;
            int? appointmentHistoryID = null;
            List<string> faultStatusesToBeDeleted = new List<string>() {AppointmentCompleteStatus.Accepted.ToString(),
                                        AppointmentCompleteStatus.InProgress.ToString(),
                                       };

            var deleteAppointmentHistoryList = (from flh in context.PDR_APPOINTMENT_HISTORY
                                                where faultStatusesToBeDeleted.Contains(flh.APPOINTMENTSTATUS.Replace(" ", "")) && appointmentId == flh.APPOINTMENTID
                                                select flh).ToList();

            deleteAppointmentHistoryList.ForEach(context.DeleteObject);
            context.SaveChanges();

            #region "Insertion in appointment history"

            if (appointment.appointmentHistory != null)
            {
                var appHis = appointment.appointmentHistory.OrderBy(x => x.actionDate).ToList();
                foreach (var history in appHis)
                {
                    journalHistoryID = context.PDR_JOURNAL_HISTORY.Where(jorHistory => jorHistory.JOURNALID == journalId).Max(jorHistory => jorHistory.JOURNALHISTORYID);
                    var voidApp = context.PDR_APPOINTMENTS.Where(x => x.APPOINTMENTID == appointmentId);
                    if (voidApp.Count() > 0)
                    {
                        PDR_APPOINTMENTS voidAppointment = voidApp.First();
                        voidAppointment.APPOINTMENTSTATUS = history.actionType;
                        voidAppointment.JOURNALHISTORYID = journalHistoryID;
                        voidAppointment.LOGGEDDATE = history.actionDate;
                        context.SaveChanges();
                    }
                }
            }

            #endregion

            journalHistoryID = context.PDR_JOURNAL_HISTORY.Where(jorHistory => jorHistory.JOURNALID == journalId).Max(jorHistory => jorHistory.JOURNALHISTORYID);
            var voidAppData = context.PDR_APPOINTMENTS.Where(x => x.APPOINTMENTID == appointmentId);
            if (voidAppData.Count() > 0)
            {
                PDR_APPOINTMENTS voidAppointment = voidAppData.First();
                voidAppointment.APPOINTMENTSTATUS = appStatus;
                voidAppointment.JOURNALHISTORYID = journalHistoryID;
                if (appStatus == AppointmentCompleteStatus.Complete.ToString())
                {
                    voidAppointment.CUSTOMERNOTES = customerNotes;
                    updateVoidAppointmentCompletionInfo(appointment, voidAppointment);
                }
                else if (appStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                {
                    updateVoidAppointmentCompletionInfo(appointment, voidAppointment);
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                appointmentHistoryID = context.PDR_APPOINTMENT_HISTORY.Where(appHistory => appHistory.APPOINTMENTID == appointmentId).Max(appHistory => appHistory.APPOINTMENTHISTORYID);

            }
            return appointmentHistoryID;
        }

        private void updateVoidAppointmentCompletionInfo(IAppointment appointment, PDR_APPOINTMENTS voidAppointment)
        {
            if (appointment.loggedDate != null)
            {
                voidAppointment.AppointmentCompletionDate = appointment.loggedDate;
                voidAppointment.LOGGEDDATE = appointment.loggedDate;
            }

            if (appointment.appointmentCompletedAppVersion != null)
            {
                voidAppointment.AppointmentCompletedAppVersion = appointment.appointmentCompletedAppVersion;
            }

            if (appointment.appointmentCurrentAppVersion != null)
            {
                voidAppointment.AppointmentCurrentAppVersion = appointment.appointmentCurrentAppVersion;
            }
        }

        private void noEntryVoidAppointment(int appointmentId, DateTime? appointmentEndDateTime, string noEntryNotes, int journalId, int? updatedBy, DateTime? loggedDate, string appointmentType, VoidWorksAppointmentData appointment = null)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                //1.) INSERT INTO V_NoEntry  
                int? msatTypeId = null;
                var msatTypeData = context.PDR_MSATType.Where(x => x.MSATTypeName.Replace(" ", "") == appointmentType.Replace(" ", ""));
                if (msatTypeData.Count() > 0)
                {
                    var MsatType = msatTypeData.FirstOrDefault();
                    msatTypeId = MsatType.MSATTypeId;
                }
                V_NoEntry voidNoEntry = new V_NoEntry();
                voidNoEntry.AppointmentId = appointmentId;
                voidNoEntry.RecordedOn = appointmentEndDateTime;
                voidNoEntry.Notes = noEntryNotes;
                if (noEntryNotes == null || noEntryNotes == string.Empty)
                    voidNoEntry.Notes = "Appointment completed because of No Entry";
                voidNoEntry.IsNoEntryScheduled = false;
                voidNoEntry.RecordedOn = loggedDate;
                voidNoEntry.RecordedBy = updatedBy;
                voidNoEntry.MsatTypeId = msatTypeId;
                context.AddToV_NoEntry(voidNoEntry);

                //2.) Update PDR_journal data Status= Completed
                UpdatePdrJournalData(journalId, AppointmentCompleteStatus.Complete.ToString());


                if (appointment != null)
                {
                    if (appointment.jobDataList != null)
                    {
                        foreach (VoidRecordWorksRequired repairJob in appointment.jobDataList)
                        {
                            V_RequiredWorks voidRecordWorks = new V_RequiredWorks();
                            var voidRecordWorksData = context.V_RequiredWorks.Where(x => x.WorksJournalId == appointment.journalId && x.RequiredWorksId == repairJob.roomId);
                            if (voidRecordWorksData.Count() > 0)
                            {

                                voidRecordWorks = voidRecordWorksData.First();
                                voidRecordWorks.StatusId = getPdrStatusId(ApplicationConstants.NoEntryAppointmentStatus);
                                voidRecordWorks.ModifiedDate = loggedDate;
                                context.SaveChanges();
                            }
                        }
                    }

                }

                success = true;
                if (success == true)
                {
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                }
            }
        }

        public int getPdrStatusId(string status)
        {
            var pdrStatusList = context.PDR_STATUS.Where(pdrSts => pdrSts.AppTitle.Trim() == status);
            if (pdrStatusList.Count() == 0)
                return 0;

            PDR_STATUS fltStatus = pdrStatusList.First();

            return fltStatus.STATUSID;
        }

        public int getPaintStatusId(string status)
        {
            var pdrStatusList = context.V_PaintStatus.Where(pdrSts => pdrSts.Title.Replace(" ", "") == status.Replace(" ", ""));
            if (pdrStatusList.Count() == 0)
                return 0;

            V_PaintStatus fltStatus = pdrStatusList.First();

            return fltStatus.StatusId;
        }

        public void recordReplacementDueMajorWork(VoidRecordMajorWorks mWork, VoidInspectionAppointmentData appointment)
        {
            try
            {

                #region Update  Replacement Due for Planned Components based of Componenet Id
                int itemId = context.PLANNED_COMPONENT_ITEM.Where(item => item.COMPONENTID == mWork.componentId).FirstOrDefault().ITEMID;

                DateTime? replacementDue = mWork.replacementDue;
                PA_PROPERTY_ITEM_DATES itemDates = new PA_PROPERTY_ITEM_DATES();
                var appData = context.PA_PROPERTY_ITEM_DATES.Where(prop => prop.PROPERTYID == appointment.property.propertyId && prop.PLANNED_COMPONENTID == mWork.componentId); //&& prop.ParameterId == null);
                if (appData.Count() > 0)
                {
                    itemDates = appData.First();

                }
                itemDates.DueDate = replacementDue;
                itemDates.PROPERTYID = appointment.property.propertyId;
                itemDates.ItemId = itemId;
                itemDates.PLANNED_COMPONENTID = (short)mWork.componentId;
                itemDates.UPDATEDON = appointment.loggedDate;
                itemDates.UPDATEDBY = appointment.updatedBy;
                if (itemDates.ParameterId == null)
                {
                    itemDates.ParameterId = 0;
                }
                if (mWork.replacementDue > appointment.reletDate)
                {
                    itemDates.isVoidAppointmentAssociated = true;
                }
                else
                {
                    itemDates.isVoidAppointmentAssociated = false;
                }

                if (appData.Count() > 0)
                {
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                }
                else
                {
                    context.AddToPA_PROPERTY_ITEM_DATES(itemDates);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }


            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }

        public void recordConditionRatingMajorWork(VoidRecordMajorWorks mWork, VoidInspectionAppointmentData appointment)
        {
            #region Update  Condition Rating based of Componenet Id
            try
            {
                //get itemParamId,itemId,ParameterName and valueId againt componetid
                var _var = (from comp in context.PLANNED_COMPONENT_ITEM
                            join item in context.PA_ITEM on comp.ITEMID equals item.ItemID
                            join iParam in context.PA_ITEM_PARAMETER on item.ItemID equals iParam.ItemId
                            join param in context.PA_PARAMETER on iParam.ParameterId equals param.ParameterID
                            join val in context.PA_PARAMETER_VALUE on param.ParameterID equals val.ParameterID
                            where param.ParameterName == "Condition Rating" && comp.COMPONENTID == mWork.componentId && val.ValueDetail == mWork.condition
                            select new
                            {
                                itemId = item.ItemID,
                                parameterName = param.ParameterName,
                                itemParamId = iParam.ItemParamID,
                                valueId = val.ValueID
                            });

                if (_var.Count() > 0)
                {
                    var propAttrib = _var.First();


                    DateTime? replacementDue = mWork.replacementDue;
                    PA_PROPERTY_ATTRIBUTES itemAttr = new PA_PROPERTY_ATTRIBUTES();
                    var appData = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == appointment.property.propertyId && prop.ITEMPARAMID == propAttrib.itemParamId);
                    if (appData.Count() > 0)
                    {
                        itemAttr = appData.First();

                    }
                    itemAttr.ITEMPARAMID = propAttrib.itemParamId;
                    itemAttr.PROPERTYID = appointment.property.propertyId;
                    itemAttr.PARAMETERVALUE = mWork.condition;
                    itemAttr.VALUEID = propAttrib.valueId;
                    itemAttr.UPDATEDON = appointment.loggedDate;
                    itemAttr.UPDATEDBY = appointment.updatedBy;
                    itemAttr.IsCheckBoxSelected = false;
                    if (appData.Count() > 0)
                    {
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    }
                    else
                    {
                        context.AddToPA_PROPERTY_ATTRIBUTES(itemAttr);
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    }
                    if (mWork.condition != null && mWork.condition != "satisfactory")
                    {
                        recordConditionRatingWorkRequired(mWork.notes, itemAttr.ATTRIBUTEID, mWork.componentId, appointment.updatedBy, appointment.loggedDate);
                    }
                }

            }
            catch (EntityException entityexception)
            {

                throw entityexception;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            #endregion
        }

        public void recordConditionRatingWorkRequired(string worksrequired, int attributeid, int? componentid, int? createdby, DateTime? createddate)
        {
            PLANNED_CONDITIONWORKS conditionworks = new PLANNED_CONDITIONWORKS();

            var plannedConditionWork = context.PLANNED_CONDITIONWORKS.Where(pcw => pcw.AttributeId == attributeid);
            int action = context.PLANNED_Action.Where(ac => ac.Title == "Recommended").First().ActionId;
            if (plannedConditionWork.Count() > 0)
            {
                conditionworks = plannedConditionWork.First();

            }
            conditionworks.AttributeId = attributeid;
            conditionworks.ComponentId = (short)componentid;
            conditionworks.ConditionAction = action;
            conditionworks.WorksRequired = worksrequired;
            if (plannedConditionWork.Count() > 0)
            {
                conditionworks.ModifiedBy = (int)createdby;
                conditionworks.ModifiedDate = (DateTime)createddate;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            else
            {
                conditionworks.CreatedBy = (int)createdby;
                conditionworks.CreatedDate = (DateTime)createddate;
                context.AddToPLANNED_CONDITIONWORKS(conditionworks);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }

        #region save PDR journal history
        /// <summary>
        /// save planned journal history
        /// </summary>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public bool UpdatePdrJournalData(int? journalId, string status, DateTime? reletDate = null)
        {
            bool result = false;

            PDR_JOURNAL pjournal = new PDR_JOURNAL();
            var journalData = context.PDR_JOURNAL.Where(jor => jor.JOURNALID == journalId);
            if (journalData.Count() > 0)
            {

                pjournal = journalData.First();
                if (status == AppointmentCompleteStatus.Complete.ToString())
                {
                    int pdrStatusId = getPdrStatusId(AppointmentCompleteStatus.Complete.ToString());
                    pjournal.STATUSID = (short)pdrStatusId;
                }

                context.SaveChanges();
                PDR_MSAT msat = new PDR_MSAT();
                var msatData = context.PDR_MSAT.Where(m => m.MSATId == pjournal.MSATID);
                if (msatData.Count() > 0 && reletDate != null)
                {
                    msat = msatData.First();
                    msat.ReletDate = reletDate;
                    context.SaveChanges();
                }
                result = true;

            }
            return result;

        }
        #endregion

        #region Update C_Termination Data
        /// <summary>
        /// Update C_Termination Data
        /// </summary>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public bool UpdateTerminationData(int? journalId, DateTime? reletDate = null, int? userId = 0)
        {
            bool result = true;

            context.V_UpdateCTerminationData(journalId, reletDate, userId);


            return result;

        }
        #endregion

        #region update Property

        /// <summary>
        /// This function updates the property 
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property  data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public bool updatePropertyStatus(string propertyId, string status, string substatus)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                int propStatusId = context.P_STATUS.Where(app => app.DESCRIPTION == status).FirstOrDefault().STATUSID;
                int propSubStatusId = context.P_SUBSTATUS.Where(app => app.DESCRIPTION == substatus).FirstOrDefault().SUBSTATUSID;
                var propRecord = context.P__PROPERTY.Where(app => app.PROPERTYID == propertyId);
                if (propRecord.Count() > 0)
                {
                    P__PROPERTY property = propRecord.First();

                    property.STATUS = propStatusId;
                    property.SUBSTATUS = propSubStatusId;
                    context.SaveChanges();
                    context.Detach(property);
                }

                trans.Complete();
                success = true;
            }

            return success;
        }

        #endregion

        public void recordMeterReadingDates(VoidGasElectricData appointment, string pName, string itemName)
        {
            try
            {
                #region Update  Condition Rating based of Componenet Id
                //get itemParamId,itemId,ParameterName and valueId 
                var _var = (from item in context.PA_ITEM
                            join iParam in context.PA_ITEM_PARAMETER on item.ItemID equals iParam.ItemId
                            join param in context.PA_PARAMETER on iParam.ParameterId equals param.ParameterID
                            where param.ParameterName == pName && item.ItemName == itemName
                            select new
                            {
                                itemId = item.ItemID,
                                parameterId = param.ParameterID
                            });

                if (_var.Count() > 0)
                {
                    var propAttrib = _var.First();
                    PA_PROPERTY_ITEM_DATES itemDates = new PA_PROPERTY_ITEM_DATES();
                    var appData = context.PA_PROPERTY_ITEM_DATES.Where(prop => prop.PROPERTYID == appointment.property.propertyId && prop.ParameterId == propAttrib.parameterId);
                    if (appData.Count() > 0)
                    {
                        itemDates = appData.First();
                    }
                    itemDates.DueDate = appointment.loggedDate;
                    itemDates.PROPERTYID = appointment.property.propertyId;
                    itemDates.ParameterId = propAttrib.parameterId;
                    itemDates.ItemId = propAttrib.itemId;
                    itemDates.UPDATEDON = appointment.loggedDate;
                    itemDates.UPDATEDBY = appointment.updatedBy;

                    if (appData.Count() > 0)
                    {
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    }
                    else
                    {
                        context.AddToPA_PROPERTY_ITEM_DATES(itemDates);
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    }

                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }

        public void recordMeterReadingAttributeData(VoidGasElectricData appointment, string paramValue, string pName, string itemName, int? meterTypeId = null)
        {
            #region Update  Condition Rating based of Componenet Id
            try
            {
                //get itemParamId,itemId,ParameterName and valueId againt componetid
                var _var = (from item in context.PA_ITEM
                            join iParam in context.PA_ITEM_PARAMETER on item.ItemID equals iParam.ItemId
                            join param in context.PA_PARAMETER on iParam.ParameterId equals param.ParameterID
                            where param.ParameterName == pName && item.ItemName == itemName
                            select new
                            {
                                itemId = item.ItemID,
                                parameterName = param.ParameterName,
                                itemParamId = iParam.ItemParamID,
                                controlType = param.ControlType

                            });

                if (_var.Count() > 0)
                {
                    var propAttrib = _var.First();



                    PA_PROPERTY_ATTRIBUTES itemAttr = new PA_PROPERTY_ATTRIBUTES();
                    var appData = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == appointment.property.propertyId && prop.ITEMPARAMID == propAttrib.itemParamId);
                    if (appData.Count() > 0)
                    {
                        itemAttr = appData.First();

                    }
                    itemAttr.ITEMPARAMID = propAttrib.itemParamId;
                    itemAttr.PROPERTYID = appointment.property.propertyId;
                    itemAttr.PARAMETERVALUE = paramValue;
                    itemAttr.VALUEID = meterTypeId;
                    itemAttr.UPDATEDON = appointment.loggedDate;
                    itemAttr.UPDATEDBY = appointment.updatedBy;
                    itemAttr.IsCheckBoxSelected = false;
                    if (appData.Count() > 0)
                    {
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    }
                    else
                    {
                        context.AddToPA_PROPERTY_ATTRIBUTES(itemAttr);
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    }
                }

            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion
        }

        public bool completeGasElectricCheckAppointment(VoidGasElectricData appointment)
        {
            bool isSuccessful = false;

            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment != null)
                    {
                        int apptId = (int)appointment.appointmentId;
                        int journalId = (int)appointment.journalId;
                        //Update journal Data
                        UpdatePdrJournalData(journalId, appointment.appointmentStatus, appointment.reletDate);

                        //Update Void Appointment Data
                        updateVoidAppointmentData(apptId, appointment.appointmentNotes, appointment.appointmentEndDateTime, appointment.appointmentStatus, journalId, appointment);

                        // Appointment is with Paused Status
                        //if (appointment.appointmentStatus == AppointmentCompleteStatus.Paused.ToString())
                        //{
                        //    pausedVoidAppointment(appointment.pausedWorks);
                        //}

                        ////Complete Electric or Gas Appointment
                        if (appointment.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidElectricCheck.ToString() || appointment.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidGasCheck.ToString())
                        {
                            //Save Gas/Electric Appointment Data
                            updateGasElectricCheckAppointment(journalId, appointment, appointment.appointmentType);
                            // Save Add Repair Data
                            //SaveAddRepairData(appointment);
                        }


                        if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                        {
                            noEntryVoidAppointment(apptId, appointment.appointmentEndDate, appointment.noEntryNotes, journalId, appointment.updatedBy, appointment.loggedDate, appointment.appointmentType);
                        }

                        logActualStartAndEndDateTimeForVoidAppointment(appointment.appointmentType, apptId, journalId, Convert.ToDateTime(appointment.appointmentActualStartTime), Convert.ToDateTime(appointment.appointmentActualEndTime));

                    }

                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment.customerList, appointment.property);
                    trans.Complete();
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            isSuccessful = true;
            return isSuccessful;
        }

        public bool completeVoidWorksAppointment(VoidWorksAppointmentData appointment)
        {
            bool isSuccessful = false;

            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment != null)
                    {
                        int apptId = (int)appointment.appointmentId;

                        int journalId = (int)appointment.journalId;
                        //Update journal Data
                        UpdatePdrJournalData(journalId, appointment.appointmentStatus, appointment.reletDate);
                        //Update C_Termination Data for Relet Date
                        UpdateTerminationData(journalId, appointment.reletDate, appointment.createdBy);
                        //Update Void Appointment Data
                        updateVoidAppointmentData(apptId, appointment.appointmentNotes, appointment.appointmentEndDateTime, appointment.appointmentStatus, journalId, appointment);

                        ////Complete Electric or Gas Appointment
                        if (appointment.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidWorks.ToString())
                        {

                            // Save Add Repair Data
                            saveAddRepairData(appointment);
                        }


                        if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                        {
                            noEntryVoidAppointment(apptId, appointment.appointmentEndDate, appointment.noEntryNotes, journalId, appointment.updatedBy, appointment.loggedDate, appointment.appointmentType, appointment);
                        }

                    }

                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment.customerList, appointment.property);
                    trans.Complete();
                }
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            isSuccessful = true;
            return isSuccessful;
        }

        #endregion

        #region Save complete Appliance Defect Appointment

        public bool completeApplianceDefectAppointment(ApplianceDefectAppointmentData appointment)
        {
            // if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
            // appointment.appointmentStatus = AppointmentCompleteStatus.Completed.ToString();

            bool isSuccessful = false;
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment.jobDataList != null)
                    {

                        foreach (ApplianceDefectJobData jobData in appointment.jobDataList)
                        {

                            // Delete jobsheet history having statuses 'Accepted', 'Paused', 'Inprogress' inorder to avoid duplication in case of offline syncing
                            List<string> defectStatusesToBeDeleted = new List<string>() {AppointmentCompleteStatus.Accepted.ToString(),
                                        AppointmentCompleteStatus.InProgress.ToString(),
                                        AppointmentCompleteStatus.Paused.ToString()};

                            var deletePausedAppointmentList = (from ppadh in context.P_PROPERTY_APPLIANCE_DEFECTS_PAUSE
                                                               join ps in context.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY on ppadh.DefectHistoryId equals ps.DefectHistoryId
                                                               where ps.PropertyDefectId == jobData.defectId
                                                               select ppadh).ToList();

                            deletePausedAppointmentList.ForEach(context.DeleteObject);
                            context.SaveChanges();

                            var deleteAppointmentHistoryList = (from ppadh in context.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY
                                                                join ps in context.PDR_STATUS on ppadh.DefectJobSheetStatus equals ps.STATUSID
                                                                where defectStatusesToBeDeleted.Contains(ps.TITLE.Replace(" ", "")) && ppadh.PropertyDefectId == jobData.defectId
                                                                select ppadh).ToList();

                            deleteAppointmentHistoryList.ForEach(context.DeleteObject);
                            context.SaveChanges();


                            if (jobData.jobActivityHistory != null && jobData.jobActivityHistory.Count > 0)
                            {
                                jobData.jobActivityHistory = jobData.jobActivityHistory.OrderBy(i => i.pauseDate).ToList();
                                foreach (ApplianceDefectPauseData pauseData in jobData.jobActivityHistory)
                                {
                                    int defectHistoryId = 0;

                                    if (pauseData.actionType == AppointmentCompleteStatus.Complete.ToString())
                                        completeApplianceDefect(jobData, pauseData.pausedBy, pauseData.pauseDate);
                                    else defectHistoryId = updateApplianceDefectStatus(jobData.defectId, pauseData.actionType, pauseData.pausedBy, pauseData.pauseDate);

                                    // save fault paused history
                                    Nullable<int> pauseID = null;
                                    if (pauseData.actionType.ToLower() == AppointmentCompleteStatus.Paused.ToString().ToLower())
                                        pauseID = savedefecdPaused(pauseData, defectHistoryId);
                                }
                            }
                        }
                    }
                    // update appliance defect appointment status
                    updateApplianceDefectAppointmentStatus(appointment);

                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);

                    foreach (ApplianceDefectJobData defectJobSheet in appointment.jobDataList)
                    {
                        logActualStartAndEndDateTimeForVoidAppointment(appointment.appointmentType, Convert.ToInt32(appointment.appointmentId), defectJobSheet.defectId, Convert.ToDateTime(defectJobSheet.jsActualStartTime), Convert.ToDateTime(defectJobSheet.jsActualEndTime));
                    }

                    trans.Complete();
                }
                context.AcceptAllChanges();
            }
            catch (EntityException entityexception)
            {
                throw entityexception;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            isSuccessful = true;
            return isSuccessful;
        }

        private void updateApplianceDefectAppointmentStatus(ApplianceDefectAppointmentData appointment)
        {
            PDR_APPOINTMENTS appt = context.PDR_APPOINTMENTS.Where(app => app.APPOINTMENTID == appointment.appointmentId).FirstOrDefault();

            if (appt != null && appt.APPOINTMENTSTATUS != appointment.appointmentStatus)
            {

                long? journalHistoryId = null;
                PDR_JOURNAL pdrJournal = context.PDR_JOURNAL.Where(pj => pj.JOURNALID == appointment.journalId).FirstOrDefault();

                if (pdrJournal != null)
                {
                    int pdrStatusId = getPdrStatusId(appointment.appointmentStatus);
                    pdrJournal.STATUSID = pdrStatusId;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    journalHistoryId = context.PDR_JOURNAL_HISTORY.Where(pjh => pjh.JOURNALID == appointment.journalId).Max(pjh => pjh.JOURNALHISTORYID);
                }

                appt.APPOINTMENTSTATUS = appointment.appointmentStatus;
                appt.JOURNALHISTORYID = journalHistoryId;

                if (appointment.appointmentStatus.ToLower() == AppointmentCompleteStatus.NoEntry.ToString().ToLower())
                {
                    PDR_MSAT msat = context.PDR_MSAT.Where(ms => ms.MSATId == pdrJournal.MSATID).FirstOrDefault();

                    V_NoEntry noEntry = new V_NoEntry()
                    {
                        AppointmentId = appt.APPOINTMENTID,
                        Notes = appointment.noEntryNotes,
                        MsatTypeId = msat.MSATTypeId,
                        RecordedBy = appointment.operativeId,
                        RecordedOn = DateTime.Now
                    };

                    context.V_NoEntry.AddObject(noEntry);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }

                if (appointment.appointmentStatus.ToLower() == AppointmentCompleteStatus.Complete.ToString().ToLower()
                    || appointment.appointmentStatus.ToLower() == AppointmentCompleteStatus.NoEntry.ToString().ToLower())
                {

                    if (appointment.loggedDate != null)
                    {
                        appt.AppointmentCompletionDate = appointment.loggedDate;
                    }

                    if (appointment.appointmentCompletedAppVersion != null)
                    {
                        appt.AppointmentCompletedAppVersion = appointment.appointmentCompletedAppVersion;
                    }

                    if (appointment.appointmentCurrentAppVersion != null)
                    {
                        appt.AppointmentCurrentAppVersion = appointment.appointmentCurrentAppVersion;
                    }
                }

            }
        }

        private void completeApplianceDefect(ApplianceDefectJobData jobData, int? modifiedBy, DateTime? modifiedDate)
        {
            P_PROPERTY_APPLIANCE_DEFECTS existingDefect = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(d => d.PropertyDefectId == jobData.defectId).FirstOrDefault();

            if (existingDefect != null)
            {
                int pdrStatusId = getPdrStatusId(AppointmentCompleteStatus.Complete.ToString());

                existingDefect.DefectJobSheetStatus = pdrStatusId;
                existingDefect.ModifiedBy = modifiedBy;
                existingDefect.ModifiedDate = modifiedDate;
                existingDefect.IsActionTaken = jobData.isRemedialActionTaken;
                existingDefect.ActionNotes = jobData.remedialActionNotes;
                existingDefect.IsWarningIssued = jobData.isAdviceNoteIssued;
                existingDefect.IsWarningFixed = jobData.warningTagFixed;
                existingDefect.IsDisconnected = jobData.isDisconnected;
                existingDefect.IsCustomerHaveHeating = jobData.isCustomerHaveHeating;
                existingDefect.IsHeatersLeft = jobData.isHeatersLeft;
                existingDefect.DefectCompletionNotes = jobData.defectCompletionNotes;
                existingDefect.NumberOfHeatersLeft = (byte?)jobData.numberOfHeatersLeft;
                existingDefect.IsCustomerHaveHotWater = jobData.isCustomerHaveHotWater;

                if (jobData.completionDate != null)
                {
                    existingDefect.JobsheetCompletionDate = jobData.completionDate;
                }

                if (jobData.jsCurrentAppVersion != null)
                {
                    existingDefect.JobsheetCurrentAppVersion = jobData.jsCurrentAppVersion;
                }

                if (jobData.jsCompletedAppVersion != null)
                {
                    existingDefect.JobsheetCompletedAppVersion = jobData.jsCompletedAppVersion;
                }

                if (jobData.defectCategoryId != -1 && jobData.jobStatus == "Complete")
                {
                    P_PROPERTY_APPLIANCE_DEFECTS newDefect = new P_PROPERTY_APPLIANCE_DEFECTS()
                    {
                        ActionNotes = string.Empty,
                        ApplianceId = existingDefect.ApplianceId,
                        BoilerTypeId = existingDefect.BoilerTypeId,
                        CategoryId = jobData.defectCategoryId,
                        CreatedBy = modifiedBy,
                        CreatedDate = modifiedDate,
                        DateCreated = modifiedDate,
                        DefectDate = modifiedDate,
                        DefectNotes = jobData.defectNotes,
                        Duration = existingDefect.Duration,
                        GasCouncilNumber = jobData.gasCouncilNumber,
                        IsActionTaken = false,
                        IsCustomerHaveHeating = jobData.isCustomerHaveHeating,
                        IsCustomerHaveHotWater = jobData.isCustomerHaveHotWater,
                        IsDefectIdentified = true,
                        IsDisconnected = jobData.isDisconnected,
                        DefectCompletionNotes = jobData.defectCompletionNotes,
                        IsHeatersLeft = jobData.isHeatersLeft,
                        IsWarningFixed = jobData.warningTagFixed,
                        IsWarningIssued = jobData.isAdviceNoteIssued,
                        JournalId = existingDefect.JournalId,
                        NumberOfHeatersLeft = (byte?)jobData.numberOfHeatersLeft,
                        PropertyId = existingDefect.PropertyId,
                        SchemeId = existingDefect.SchemeId,
                        BlockId = existingDefect.BlockId,
                        HeatingMappingId = existingDefect.HeatingMappingId,
                        Priority = existingDefect.Priority,
                        SerialNumber = jobData.serialNumber,
                        TradeId = existingDefect.TradeId,
                        DefectJobSheetStatus = GetPdrStatusId(ApplicationConstants.ToBeApprovedDefectStatus)
                    };

                    context.P_PROPERTY_APPLIANCE_DEFECTS.AddObject(newDefect);
                }
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }


        }

        private int? savedefecdPaused(ApplianceDefectPauseData pauseData, int? defectHistoryId)
        {
            int? pauseId = null;

            short pauseReasonId = context.FL_PAUSED_REASON.Where(pr => pr.Reason == pauseData.pauseReason).Select(pr => pr.PauseId).FirstOrDefault();

            P_PROPERTY_APPLIANCE_DEFECTS_PAUSE defectPause = new P_PROPERTY_APPLIANCE_DEFECTS_PAUSE()
            {
                PauseBy = (int)pauseData.pausedBy,
                PauseNote = pauseData.pauseNote,
                PauseOn = pauseData.pauseDate,
                PauseReasonId = pauseReasonId,
                DefectHistoryId = (int)defectHistoryId
            };

            context.P_PROPERTY_APPLIANCE_DEFECTS_PAUSE.AddObject(defectPause);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

            pauseId = defectPause.PauseId;

            return pauseId;
        }

        private int updateApplianceDefectStatus(int propertyDefectId, string defectStatus, int? modifiedBy, DateTime? modifiedDate)
        {
            int defectHistoryId = -1;

            P_PROPERTY_APPLIANCE_DEFECTS defect = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(d => d.PropertyDefectId == propertyDefectId).FirstOrDefault();

            if (defect != null)
            {
                int pdrStatusId = getPdrStatusId(defectStatus);

                defect.DefectJobSheetStatus = pdrStatusId;
                defect.ModifiedBy = modifiedBy;
                defect.ModifiedDate = modifiedDate;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            defectHistoryId = context.P_PROPERTY_APPLIANCE_DEFECTS_HISTORY.Where(defHis => defHis.PropertyDefectId == propertyDefectId).Max(defHis => defHis.DefectHistoryId);

            return defectHistoryId;
        }

        #endregion

        #endregion

        #region Get All Scheme/Block Fault Appointments By User

        public List<AllAppointmentsList> getAllSchemeBlockFaultAppointmentsByUser(string userName, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            #region get updated appointment from DB
            List<AllAppointmentsList> faultAppointments = new List<AllAppointmentsList>();

            var _var = (from app in context.FL_CO_APPOINTMENT
                        join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                        join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                        join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                        join creat in context.AC_LOGINS on fltl.UserId equals creat.EMPLOYEEID
                        join login in context.AC_LOGINS on app.OperativeID equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                        join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                        join schemes in context.P_SCHEME on fltl.SchemeId equals schemes.SCHEMEID
                        join blocks in context.P_BLOCK on fltl.BlockId equals blocks.BLOCKID into tempBlock
                        from blocks1 in tempBlock.DefaultIfEmpty()
                        join noentry in context.FL_FAULT_NOENTRY on fltl.FaultLogID equals noentry.FaultLogId into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        orderby app.AppointmentID
                        where app.AppointmentDate >= varStartTime && app.AppointmentDate <= varEndTime
                                && login.LOGIN == userName
                                && fltAppt.FaultAppointmentId == (context.FL_FAULT_APPOINTMENT.Where(a => a.AppointmentId == app.AppointmentID).Min(a => a.FaultAppointmentId))
                                && flts.Description != "Cancelled"
                                && !(completedStatusesList.Contains(app.AppointmentStatus.Replace(" ", ""))) // Filter out completed appointments.
                        select new AllAppointmentsList
                        {
                            appointmentId = app.AppointmentID,
                            appointmentNotes = app.Notes,
                            appointmentDate = app.AppointmentDate,
                            //appointmentEndDate = app.a
                            appointmentStatus = app.AppointmentStatus,
                            appointmentType = "Fault",
                            surveyorUserName = login.LOGIN,
                            appointmentStartTimeString = app.Time,
                            appointmentEndTimeString = app.EndTime,
                            createdBy = fltl.UserId,
                            creationDate = app.LastActionDate,
                            createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                            scheme = new SchemeBlockData
                            {
                                schemeId = schemes.SCHEMEID,
                                blockId = blocks1.BLOCKID,
                                schemeName = schemes.SCHEMENAME,
                                blockName = blocks1.BLOCKNAME,
                                address1 = blocks1.ADDRESS1,
                                address2 = blocks1.ADDRESS2,
                                address3 = blocks1.ADDRESS3,
                                towncity = blocks1.TOWNCITY,
                                postcode = blocks1.POSTCODE,
                                county = blocks1.COUNTY,
                            }

                        });
            #endregion

            if (_var.Count() > 0)
            {
                faultAppointments = _var.ToList();
                foreach (AllAppointmentsList item in faultAppointments)
                {

                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(dt.ToLongDateString() + " " + item.appointmentStartTimeString);
                    item.appointmentEndDateTime = DateTime.Parse(dt.ToLongDateString() + " " + item.appointmentEndTimeString);
                    if (item.appointmentStatus == "Appointment Arranged")
                        item.appointmentStatus = "NotStarted";
                    //setAdditionalDataFault(item);
                    item.jobDataList = getJobDataList(item.appointmentId);
                    item.property = null;
                }
            }

            return faultAppointments;
        }

        #endregion

        #region Get All Void Appointments
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<VoidAppointmentData> getAllVoidAppointments(string username, string startDate, string endDate, List<ExistingAppointmentParam> existingAppointments, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                                AppointmentCompleteStatus.Complete.ToString(),
                                                AppointmentCompleteStatus.Completed.ToString(),
                                                AppointmentCompleteStatus.NoEntry.ToString()};
            #region get updated appointment from DB
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            var appointments = (from app in context.PDR_APPOINTMENTS
                                join jor in context.PDR_JOURNAL on app.JOURNALID equals jor.JOURNALID
                                join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                join pro in context.P__PROPERTY on msat.PropertyId equals pro.PROPERTYID
                                join msatType in context.PDR_MSATType on msat.MSATTypeId equals msatType.MSATTypeId
                                from cj in context.C_JOURNAL.Where(c => c.PROPERTYID == pro.PROPERTYID && c.ITEMNATUREID == 27 && (c.CURRENTITEMSTATUSID == 13 || c.CURRENTITEMSTATUSID == 14 || c.CURRENTITEMSTATUSID == 15)).OrderByDescending(c => c.JOURNALID).Take(1)
                                from term in context.C_TERMINATION.Where(c => c.JOURNALID == cj.JOURNALID).OrderByDescending(c => c.TERMINATIONHISTORYID).Take(1)
                                join ten in context.C_TENANCY
                                        on new { TENANCYID = (Int32)msat.TenancyId }
                                        equals new { TENANCYID = ten.TENANCYID }
                                        into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join status in context.PDR_STATUS on jor.STATUSID equals status.STATUSID
                                join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                                join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                                join voidAppointData in context.V_AppointmentRecordedData on jor.JOURNALID equals voidAppointData.InspectionJournalId into allApptData
                                from voidApptsDetails in allApptData.DefaultIfEmpty()
                                join gasNotificationJoin in context.V_BritishGasVoidNotification on pro.PROPERTYID equals gasNotificationJoin.PropertyId into ElecGasNotification
                                from gasNotification in ElecGasNotification.DefaultIfEmpty()
                                orderby app.APPOINTMENTID
                                where msatType.MSATTypeName.Contains("Void") && login.LOGIN == username
                                && app.APPOINTMENTSTARTDATE >= varStartTime && app.APPOINTMENTENDDATE <= varEndTime && status.AppTitle != "Complete" && status.AppTitle != "Cancelled" && status.AppTitle != "No Entry"
                                //&& !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.

                                select new VoidAppointmentData
                                {

                                    appointmentId = app.APPOINTMENTID,
                                    tenancyId = tenan.TENANCYID,
                                    journalId = app.JOURNALID,
                                    journalHistoryId = app.JOURNALHISTORYID,
                                    appointmentDate = app.APPOINTMENTSTARTDATE,
                                    appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                                    appointmentEndTimeString = app.APPOINTMENTENDTIME,
                                    appointmentEndDate = app.APPOINTMENTENDDATE,
                                    surveyorUserName = login.LOGIN,
                                    loggedDate = app.LOGGEDDATE,
                                    creationDate = app.LOGGEDDATE,
                                    createdBy = app.CREATEDBY,
                                    appointmentNotes = app.APPOINTMENTNOTES,
                                    appointmentStatus = (app.APPOINTMENTSTATUS == null ? "NotStarted" : app.APPOINTMENTSTATUS),
                                    appointmentType = msatType.MSATTypeName,
                                    isWorksRequired = voidApptsDetails.IsWorksRequired,
                                    isGasCheckRequired = voidApptsDetails.IsGasCheckRequired,
                                    isElectricCheckRequired = voidApptsDetails.IsElectricCheckRequired,
                                    isEPCCheckRequired = voidApptsDetails.IsEpcCheckRequired,
                                    isAsbestosCheckRequired = voidApptsDetails.IsAbestosCheckRequired,
                                    createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                                    duration = app.DURATION,
                                    operativeId = app.ASSIGNEDTO,
                                    defaultCustomerId = msat.CustomerId,
                                    journal = new JournalData
                                    {
                                        creationBy = jor.CREATEDBY,
                                        creationDate = jor.CREATIONDATE,
                                        journalId = jor.JOURNALID,
                                        propertyId = msat.PropertyId,
                                        statusId = jor.STATUSID

                                    },

                                    property = new PropertyData
                                    {
                                        propertyId = msat.PropertyId,
                                        tenancyId = tenan.TENANCYID,
                                        houseNumber = pro.HOUSENUMBER,
                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                    },
                                    reletDate = term.RELETDATE,
                                    terminationDate = term.TERMINATIONDATE,
                                    majorWorksRequired = new VoidMajorWorksRequired
                                    {
                                        isMajorWorkRequired = voidApptsDetails.IsMajorWorksRequired,
                                    },
                                    worksRequired = new VoidRequiredWorks
                                    {
                                        isWorksRequired = voidApptsDetails.IsWorksRequired,
                                    },
                                    voidWorksRequired = new VoidWorksRequired
                                    {
                                        isWorksRequired = voidApptsDetails.IsWorksRequired,

                                    },
                                    paintPacks = new VoidPaintPackWorks
                                    {
                                        isPaintPackRequired = voidApptsDetails.IsPaintPackAssistance
                                    },
                                    electricGasData = new GasElectricData
                                    {
                                        gasMeterReading = gasNotification.GasMeterReading,
                                        gasMeterTypeId = gasNotification.GasMeterTypeId,
                                        tenantTypeId = gasNotification.TenancyId,
                                        electricMeterReading = gasNotification.ElectricMeterReading,
                                        electricMeterTypeId = gasNotification.ElectricMeterTypeId
                                    }


                                });
            #endregion
            List<VoidAppointmentData> appointmentList = new List<VoidAppointmentData>();
            List<VoidAppointmentData> voidAppointmentList = new List<VoidAppointmentData>();
            List<ExistingAppointmentParam> appAppointmentList = existingAppointments.OfType<ExistingAppointmentParam>().Where(s => s.appointmentType == "Void Inspection" || s.appointmentType == "Post Void Inspection" || s.appointmentType == "Void Works" || s.appointmentType == "Void Gas Check" || s.appointmentType == "Void Electric Check").ToList();
            List<int> appAptIds = appAppointmentList.Select(app => app.appointmentId).ToList();

            List<int> deletedIds = new List<int>();
            List<ExistingAppointmentParam> deleteAppointmentList = new List<ExistingAppointmentParam>();
            if (appointments.Count() > 0)
            {
                appointmentList = new List<VoidAppointmentData>(appointments.ToList());
                List<int> dbAptIds = appointmentList.Select(app => app.appointmentId).ToList();
                deletedIds = appAptIds.Where(x => !dbAptIds.Contains(x)).ToList();
                deleteAppointmentList = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => deletedIds.Contains(s.appointmentId)).ToList();


                //get rooms list and send it with All Appointments
                foreach (VoidAppointmentData appt in appointmentList)
                {

                    var extAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == appt.appointmentId && (((s.endDate.Date != appt.appointmentEndDate.Value.Date && s.startDate.Date != appt.appointmentDate.Value.Date && s.endTime != appt.appointmentEndTimeString && s.startTime != appt.appointmentStartTimeString && s.operativeId == appt.operativeId) || s.operativeId != appt.operativeId) || appt.appointmentStatus == "NotStarted"));
                    var emptyAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == appt.appointmentId);
                    if (emptyAppApt.Count() == 0 || extAppApt.Count() > 0)
                    {
                        if (extAppApt.Count() > 0)
                            deleteAppointmentList.Add(extAppApt.FirstOrDefault());

                        DateTime? gasCheckDate = null;
                        string gasCheckStatus = string.Empty;
                        DateTime? electricCheckDate = null;
                        string electricCheckStatus = string.Empty;
                        //Additional Data of property and customer for appointment.
                        setAdditionalDataVoid(appt);
                        int apptId = appt.appointmentId;
                        int? cusId = appt.defaultCustomerId;
                        if (appt.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidWorks.ToString())
                        {
                            appt.voidWorksRequired.recordWorks = getVoidRequiredWorks(apptId);

                        }
                        else if (appt.appointmentType.Replace(" ", "") == VoidAppointmentTypes.PostVoidInspection.ToString())
                        {
                            // If the surveyor selects the “Tenant” button, those works that
                            //the tenant agreed to complete themselves will be listed. Each one will have the option (buttons)
                            //to update its status to either “Complete” or “Assign to BRS”.
                            cusId = appt.customerList[0].customerId;
                            int inspectionJournalId = getInspectionJournalId(appt.property.propertyId, (int)appt.tenancyId, (int)cusId);
                            appt.worksRequired.recordWorks = getVoidWorksForPostVoidInspection(appt.journalId, inspectionJournalId);
                            if (appt.worksRequired.recordWorks.Count() > 0)
                            {
                                appt.worksRequired.isWorksRequired = true;
                            }

                            //If a gas or electric check has been arranged or completed, the status and date of the
                            //appointment will be displayed on this page. However, the surveyor will have the option to select
                            //Y/N for Gas and Electric checks if this was not selected during the Void Inspection.
                            getGasElectricCheckData(appt, inspectionJournalId, ref gasCheckDate, ref gasCheckStatus, ref electricCheckDate, ref electricCheckStatus);
                            appt.gasCheckDate = gasCheckDate;
                            appt.gasCheckStatus = gasCheckStatus;
                            appt.electricCheckDate = electricCheckDate;
                            appt.electricCheckStatus = electricCheckStatus;
                            if (!string.IsNullOrEmpty(electricCheckStatus))
                            {
                                appt.isElectricCheckRequired = true;
                            }
                            if (!string.IsNullOrEmpty(gasCheckStatus))
                            {
                                appt.isGasCheckRequired = true;
                            }
                        }
                        else
                        {
                            appt.worksRequired.recordWorks = getVoidWorks(apptId);
                        }
                        //Get Property Components Data
                        appt.majorWorksRequired.propertyComponents = getPropertyCompenentDetails(appt.property.propertyId);

                        //Get Property Components Data
                        appt.majorWorksRequired.recordMajorWork = getVoidMajorWorks(apptId);
                        //Get Pain Packs Data for an appointment
                        appt.paintPacks.roomList = getPaintPackData(appt.journalId);

                        //Concat Date and Time in single string.
                        DateTime dt = (DateTime)appt.appointmentDate;
                        appt.appointmentStartDateTime = DateTime.Parse(((DateTime)appt.appointmentDate).ToString("dd/MM/yyyy") + " " + appt.appointmentStartTimeString);
                        appt.appointmentEndDateTime = DateTime.Parse(((DateTime)appt.appointmentEndDate).ToString("dd/MM/yyyy") + " " + appt.appointmentEndTimeString);
                        appt.jsvNumber = "JSV" + appt.journalId;
                        if (appt.reletDate == null && appt.terminationDate != null)
                        {
                            DateTime terDate = (DateTime)appt.terminationDate;
                            appt.reletDate = Convert.ToDateTime(terDate.AddDays(7));
                        }


                        //Check Nullable properties
                        checkNullable(appt);
                        appt.defaultCustomerId = cusId;
                        voidAppointmentList.Add(appt);
                    }

                }
            }
            else
            {

                deleteAppointmentList = appAppointmentList;

            }
            foreach (ExistingAppointmentParam exApptList in deleteAppointmentList)
            {
                DeleteAppointmentParam delApp = new DeleteAppointmentParam();
                delApp.appointmentId = exApptList.appointmentId;
                delApp.appointmentType = exApptList.appointmentType;
                deleteAppointment.Add(delApp);
            }

            return voidAppointmentList;
        }

        private List<int> getPaintPackData(int apptId)
        {
            List<int> paintRooms = new List<int>();
            var paintData = (from paintPack in context.V_PaintPack
                             join paintPackDetails in context.V_PaintPackDetails on paintPack.PaintPackId equals paintPackDetails.PaintPackId
                             where paintPack.InspectionJournalId == apptId
                             select new RoomList
                             {
                                 roomId = paintPackDetails.RoomId
                             }
                            );
            if (paintData.Count() > 0)
            {
                List<RoomList> roomList = paintData.ToList();
                foreach (RoomList room in roomList)
                {
                    int rId = (int)room.roomId;
                    paintRooms.Add(rId);
                }
            }
            return paintRooms;
        }

        private void checkNullable(VoidAppointmentData appt)
        {
            if (appt.terminationDate == null)
            {
                appt.terminationDate = DateTime.Now;
            }
            if (appt.reletDate == null)
            {
                appt.reletDate = DateTime.Now;
            }
            if (appt.appointmentNotes == null)
            {
                appt.appointmentNotes = "none";
            }
            if (appt.isAsbestosCheckRequired == null)
            {
                appt.isAsbestosCheckRequired = false;
            }
            if (appt.isElectricCheckRequired == null)
            {
                appt.isElectricCheckRequired = false;
            }
            if (appt.isEPCCheckRequired == null)
            {
                appt.isEPCCheckRequired = false;
            }
            if (appt.isGasCheckRequired == null)
            {
                appt.isGasCheckRequired = false;
            }
            if (appt.isWorksRequired == null)
            {
                appt.isWorksRequired = false;
            }

            if (appt.majorWorksRequired.isMajorWorkRequired == null)
            {
                appt.majorWorksRequired.isMajorWorkRequired = false;
            }
            if (appt.worksRequired != null)
            {
                if (appt.worksRequired.isWorksRequired == null)
                {
                    appt.worksRequired.isWorksRequired = false;
                }
            }
            if (appt.paintPacks.isPaintPackRequired == null)
            {
                appt.paintPacks.isPaintPackRequired = false;
            }
            if (appt.electricGasData.electricMeterLocation == null)
            {
                appt.electricGasData.electricMeterLocation = "none";
            }
            if (appt.electricGasData.gasMeterLocation == null)
            {
                appt.electricGasData.gasMeterLocation = "none";
            }
            if (appt.electricGasData.electricMeterReading == null)
            {
                appt.electricGasData.electricMeterReading = 0;
            }
            if (appt.electricGasData.gasMeterReading == null)
            {
                appt.electricGasData.gasMeterReading = 0;
            }
            if (appt.electricGasData.tenantTypeId == null)
            {
                appt.electricGasData.tenantTypeId = 0;
            }
            if (appt.electricGasData.gasMeterTypeId == null)
            {
                appt.electricGasData.gasMeterTypeId = 0;
            }
            if (appt.electricGasData.electricMeterTypeId == null)
            {
                appt.electricGasData.electricMeterTypeId = 0;
            }
            if (appt.gasCheckStatus == null)
            {
                appt.gasCheckStatus = string.Empty;
            }
            if (appt.electricCheckStatus == null)
            {
                appt.electricCheckStatus = string.Empty;
            }

        }

        private List<VoidRecordMajorWorks> getPropertyCompenentDetails(string propertyId)
        {
            List<VoidRecordMajorWorks> majorWorks = new List<VoidRecordMajorWorks>();
            var componentList = context.GetPropertyCompenentDetails(propertyId);
            IList<GetPropertyCompenentDetails_Result> plannedComponentsDetails = componentList.ToList();
            if (plannedComponentsDetails.Count() > 0)
            {
                for (int i = 0; i < plannedComponentsDetails.Count(); i++)
                {
                    VoidRecordMajorWorks mWorks = new VoidRecordMajorWorks();
                    mWorks.component = plannedComponentsDetails.ElementAt(i).Component;
                    mWorks.condition = plannedComponentsDetails.ElementAt(i).Condition;
                    mWorks.componentId = plannedComponentsDetails.ElementAt(i).ComponentId;
                    if (plannedComponentsDetails.ElementAt(i).DueDate != "-")
                    {
                        mWorks.replacementDue = Convert.ToDateTime(plannedComponentsDetails.ElementAt(i).DueDate);
                    }
                    majorWorks.Add(mWorks);
                }
            }

            return majorWorks;
        }

        private List<VoidRecordRequiredWorks> getVoidWorks(int apptId)
        {
            List<VoidRecordRequiredWorks> result = new List<VoidRecordRequiredWorks>();
            var appointments = (from app in context.PDR_APPOINTMENTS
                                join jor in context.PDR_JOURNAL on app.JOURNALID equals jor.JOURNALID
                                join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                join voidAppointData in context.V_AppointmentRecordedData on jor.JOURNALID equals voidAppointData.InspectionJournalId into allApptData
                                from voidApptsDetails in allApptData.DefaultIfEmpty()
                                join requirdWorksJoin in context.V_RequiredWorks on voidApptsDetails.InspectionJournalId equals requirdWorksJoin.InspectionJournalId into requirdWorksData
                                from requirdWorks in requirdWorksData.DefaultIfEmpty()
                                join fault in context.FL_FAULT on requirdWorks.faultID equals fault.FaultID into faults
                                from fault in faults.DefaultIfEmpty()
                                join st in context.PDR_STATUS on requirdWorks.StatusId equals st.STATUSID into WorkStatusData
                                from status in WorkStatusData.DefaultIfEmpty()
                                where app.APPOINTMENTID == apptId
                                && voidApptsDetails.IsWorksRequired == true
                                && requirdWorks.faultAreaID != null
                                && requirdWorks.IsCanceled == false
                                select new VoidRecordRequiredWorks
                                {

                                    isRecurringProblem = requirdWorks.isRecurringProblem,
                                    problemDays = requirdWorks.problemDays,
                                    repairNotes = (requirdWorks.faultNotes == null ? String.Empty : requirdWorks.faultNotes),
                                    faultId = requirdWorks.faultID,
                                    faultAreaId = requirdWorks.faultAreaID,
                                    repairId = requirdWorks.repairId,
                                    roomId = requirdWorks.faultAreaID,
                                    workDescription = requirdWorks.WorkDescription,
                                    estimate = requirdWorks.TenantNeglectEstimation,
                                    isBRSWork = requirdWorks.IsBrsWorks,
                                    isTenantWork = requirdWorks.IsTenantWorks,
                                    worksId = requirdWorks.RequiredWorksId,
                                    status = status.AppTitle
                                });
            if (appointments.Count() > 0)
            {
                result = appointments.ToList();
            }
            return result;

        }

        private List<VoidRecordMajorWorks> getVoidMajorWorks(int apptId)
        {
            List<VoidRecordMajorWorks> result = new List<VoidRecordMajorWorks>();
            var appointments = (from app in context.PDR_APPOINTMENTS
                                join jor in context.PDR_JOURNAL on app.JOURNALID equals jor.JOURNALID
                                join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                join voidAppointData in context.V_AppointmentRecordedData on jor.JOURNALID equals voidAppointData.InspectionJournalId into allApptData
                                from voidApptsDetails in allApptData.DefaultIfEmpty()
                                join requirdWorksJoin in context.V_RequiredWorks on voidApptsDetails.InspectionJournalId equals requirdWorksJoin.InspectionJournalId into requirdWorksData
                                from requirdWorks in requirdWorksData.DefaultIfEmpty()
                                join comp in context.PLANNED_COMPONENT on requirdWorks.ComponentId equals comp.COMPONENTID
                                where app.APPOINTMENTID == apptId && voidApptsDetails.IsMajorWorksRequired == true && requirdWorks.ComponentId != null && requirdWorks.IsCanceled == false
                                select new VoidRecordMajorWorks
                                {
                                    component = comp.COMPONENTNAME,
                                    condition = requirdWorks.Condition,
                                    componentId = requirdWorks.ComponentId,
                                    replacementDue = requirdWorks.ReplacementDue,
                                    notes = (requirdWorks.MajorWorkNotes == null ? "" : requirdWorks.MajorWorkNotes)
                                });
            if (appointments.Count() > 0)
            {
                result = appointments.ToList();
            }
            return result;
        }

        public List<RoomList> getRoomsList()
        {
            List<RoomList> result = new List<RoomList>();
            var roomsList = (from rooms in context.V_RoomList
                             select new RoomList
                             {
                                 roomId = rooms.RoomId,
                                 name = rooms.Name,
                             });

            if (roomsList.Count() > 0)
            {
                result = roomsList.ToList();
            }
            return result;
        }

        public List<ElectricMeterType> getElectricMeterTypes()
        {
            List<ElectricMeterType> result = new List<ElectricMeterType>();
            var EMeterTypeList = (from values in context.PA_PARAMETER_VALUE
                                  join p in context.PA_PARAMETER on values.ParameterID equals p.ParameterID
                                  join ip in context.PA_ITEM_PARAMETER on p.ParameterID equals ip.ParameterId
                                  join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                                  where i.ItemName.Contains("Electric") && p.ParameterName.Contains("Meter Type") && p.IsActive == true && values.IsActive == true
                                  select new ElectricMeterType
                                  {
                                      meterId = values.ValueID,
                                      meterType = values.ValueDetail,
                                  });

            if (EMeterTypeList.Count() > 0)
            {
                result = EMeterTypeList.ToList();
            }
            return result;
        }

        public List<GasAbortReason> getGasAbortReasons()
        {
            List<GasAbortReason> reasonList = new List<GasAbortReason>();
            var AbortReasonList = (from values in context.AS_AbortReason
                                   select new GasAbortReason()
                                   {
                                       AbortReasonId = values.AbortReasonId,
                                       AbortReason = values.AbortReason
                                   });
            if (AbortReasonList.Count() > 0)
            {
                reasonList = AbortReasonList.ToList();
            }
            return reasonList;
        }

        public List<GasMeterType> getGasMeterTypes()
        {
            List<GasMeterType> result = new List<GasMeterType>();
            var GMeterTypeList = (from values in context.PA_PARAMETER_VALUE
                                  join p in context.PA_PARAMETER on values.ParameterID equals p.ParameterID
                                  join ip in context.PA_ITEM_PARAMETER on p.ParameterID equals ip.ParameterId
                                  join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                                  where i.ItemName.Contains("Gas") && p.ParameterName.Contains("Meter Type") && p.IsActive == true && values.IsActive == true
                                  select new GasMeterType
                                  {
                                      meterId = values.ValueID,
                                      meterType = values.ValueDetail,
                                  });

            if (GMeterTypeList.Count() > 0)
            {
                result = GMeterTypeList.ToList();
            }
            return result;
        }

        private List<VoidRecordWorksRequired> getVoidRequiredWorks(int apptId)
        {
            List<VoidRecordWorksRequired> result = new List<VoidRecordWorksRequired>();
            var appointments = (from app in context.PDR_APPOINTMENTS
                                join jor in context.PDR_JOURNAL on app.JOURNALID equals jor.JOURNALID
                                join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                join requirdWorks in context.V_RequiredWorks on jor.JOURNALID equals requirdWorks.WorksJournalId
                                join room in context.V_RoomList on requirdWorks.RoomId equals room.RoomId into rooms
                                from room in rooms.DefaultIfEmpty()
                                join faultArea in context.FL_AREA on requirdWorks.faultAreaID equals faultArea.AreaID into faultAreas
                                from faultArea in faultAreas.DefaultIfEmpty()
                                join fault in context.FL_FAULT on requirdWorks.faultID equals fault.FaultID into faults
                                from fault in faults.DefaultIfEmpty()
                                join status in context.PDR_STATUS on requirdWorks.StatusId equals status.STATUSID
                                where app.APPOINTMENTID == apptId //&& status.TITLE != "To Be Arranged"
                                && (requirdWorks.IsCanceled == false || requirdWorks.IsCanceled == null)
                                select new VoidRecordWorksRequired
                                {

                                    isRecurringProblem = requirdWorks.isRecurringProblem,
                                    problemDays = requirdWorks.problemDays,
                                    faultNotes = requirdWorks.faultNotes,
                                    faultId = requirdWorks.faultID,
                                    faultAreaId = requirdWorks.faultAreaID,
                                    repairId = requirdWorks.repairId,
                                    roomId = requirdWorks.RequiredWorksId,
                                    workLocation = faultArea.AreaName,
                                    workDescription = requirdWorks.WorkDescription,
                                    workType = requirdWorks.workType,
                                    worksNotes = requirdWorks.faultNotes,
                                    requiredWorksId = requirdWorks.RequiredWorksId,
                                    duration = requirdWorks.Duration,
                                    workStatus = status.AppTitle
                                });
            if (appointments.Count() > 0)
            {


                result = appointments.ToList();
                foreach (VoidRecordWorksRequired appt in result)
                {
                    appt.voidJSV = "JSV" + appt.requiredWorksId;
                    appt.workType = appt.workType ?? String.Empty;
                }
            }
            return result;
        }

        /// <summary>
        /// It will be required to move in database if needed in future by Omer Nasir
        /// </summary>
        /// <returns></returns>
        private string getSaniCleanInformation()
        {
            IList<String> infoList = new List<String>()
            { "Sani clean kitchen inside and outside of units, sink and worktops"
              , "Sani clean all white goods and flooring to bathroom" };

            StringBuilder sbInfo = new StringBuilder();
            int counter = 1;
            foreach (string item in infoList)
            {
                sbInfo.Append(String.Format(" \n ({0}) {1}", Convert.ToString(counter), item));
                counter++;
            }
            return sbInfo.ToString();
        }

        /// <summary>
        /// It will be required to move in database if needed in future by Omer Nasir
        /// </summary>
        /// <returns></returns>
        private string getStandardVoidWorksInformation()
        {
            IList<String> infoList = new List<String>()
            {"Check kitchen sink waste pipe"
            ,"Cap washing machine waste"
            ,"Check washing machine taps"
            ,"Clear rubbish from property"
            ,"Remove all items from property"
            ,"Fill cracks in walls"
            ,"Check bath, sinks & basins plugs/chains"
            ,"Check bath panel"
            ,"Replace w.c. seat"
            ,"Check pipework"
            ,"Check heating pipework"
            ,"Check doors are opening/closing"
            ,"Check stop taps/isolating valves"
            ,"Check windows/doors operate"
            ,"Check locks to doors/windows"
            ,"Ensure 2 keys are left for each lock"
            ,"Leave all communal/EVA/Service keys"
            ,"Check roof space and clear out"
            ,"Ensure meter cupboard keys are left"};

            StringBuilder sbInfo = new StringBuilder();
            int counter = 1;
            foreach (string item in infoList)
            {
                sbInfo.Append(String.Format(" \n ({0}) {1}", Convert.ToString(counter), item));
                counter++;
            }
            return sbInfo.ToString();
        }

        private List<VoidRecordRequiredWorks> getVoidWorksForPostVoidInspection(int journalId, int inspectionJournalId)
        {
            List<VoidRecordRequiredWorks> result = new List<VoidRecordRequiredWorks>();
            if (inspectionJournalId > 0)
            {
                var appointments = (from app in context.PDR_APPOINTMENTS
                                    join jor in context.PDR_JOURNAL on app.JOURNALID equals jor.JOURNALID
                                    join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                    join voidAppointData in context.V_AppointmentRecordedData on jor.JOURNALID equals voidAppointData.InspectionJournalId into allApptData
                                    from voidApptsDetails in allApptData.DefaultIfEmpty()
                                    join requirdWorksJoin in context.V_RequiredWorks on voidApptsDetails.InspectionJournalId equals requirdWorksJoin.InspectionJournalId into requirdWorksData
                                    from requirdWorks in requirdWorksData.DefaultIfEmpty()
                                    join fault in context.FL_FAULT on requirdWorks.faultID equals fault.FaultID into faults
                                    from fault in faults.DefaultIfEmpty()
                                    join st in context.PDR_STATUS on requirdWorks.StatusId equals st.STATUSID into WorkStatusData
                                    from status in WorkStatusData.DefaultIfEmpty()
                                    where (jor.JOURNALID == inspectionJournalId || jor.JOURNALID == journalId)
                                    //&& requirdWorks.faultAreaID != null
                                    && (requirdWorks.IsCanceled == null || requirdWorks.IsCanceled == false)
                                    select new VoidRecordRequiredWorks
                                    {
                                        isRecurringProblem = requirdWorks.isRecurringProblem,
                                        problemDays = requirdWorks.problemDays,
                                        repairNotes = (requirdWorks.faultNotes == null ? String.Empty : requirdWorks.faultNotes),
                                        faultId = requirdWorks.faultID,
                                        faultAreaId = requirdWorks.faultAreaID,
                                        repairId = requirdWorks.repairId,
                                        roomId = requirdWorks.faultAreaID,
                                        workDescription = requirdWorks.WorkDescription,
                                        estimate = requirdWorks.TenantNeglectEstimation,
                                        isBRSWork = requirdWorks.IsBrsWorks,
                                        isTenantWork = requirdWorks.IsTenantWorks,
                                        status = status.AppTitle,
                                        isVerified = (bool)requirdWorks.IsVerified,
                                        worksId = requirdWorks.RequiredWorksId
                                    });
                if (appointments.Count() > 0)
                {
                    foreach (VoidRecordRequiredWorks item in appointments.ToList())
                    {
                        if (item.workDescription != null && item.workDescription != string.Empty)
                        {
                            if (item.workDescription == getStandardVoidWorksInformation())
                            {
                                item.workDescription = "Standard Void Works";
                            }
                            else if (item.workDescription == getSaniCleanInformation())
                            {
                                item.workDescription = "Sani Clean";
                            }
                            else
                            {
                                item.workDescription = item.workDescription;
                            }
                            result.Add(item);
                        }
                    }
                }
            }
            return result;
        }
        private int getInspectionJournalId(string propertyId, int tenancyId, int customerId)
        {
            int inspectionJournalId = 0;
            var voidInspection = (from jor in context.PDR_JOURNAL
                                  join status in context.PDR_STATUS on jor.STATUSID equals status.STATUSID
                                  join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                  join msatType in context.PDR_MSATType on msat.MSATTypeId equals msatType.MSATTypeId
                                  where msat.PropertyId == propertyId && msat.TenancyId == tenancyId && msat.CustomerId == customerId
                                  && msatType.MSATTypeName == "Void Inspection"
                                  select new
                                  {
                                      inspectionJournalId = jor.JOURNALID
                                  });
            if (voidInspection.Count() > 0)
            {
                inspectionJournalId = voidInspection.First().inspectionJournalId;
            }
            return inspectionJournalId;
        }
        private void getGasElectricCheckData(VoidAppointmentData appointment, int inspectionJournalId, ref DateTime? gasCheckDate, ref string gasCheckStatus, ref DateTime? electricCheckDate, ref string electricCheckStatus)
        {
            V_AppointmentRecordedData AppData = new V_AppointmentRecordedData();
            var voidAppData = context.V_AppointmentRecordedData.Where(x => x.InspectionJournalId == inspectionJournalId);
            if (voidAppData.Count() > 0)
            {
                AppData = voidAppData.First();
                if (AppData.IsGasCheckRequired == true && AppData.GasCheckJournalId != null)
                {
                    var gasAppData = (from jor in context.PDR_JOURNAL
                                      join app in context.PDR_APPOINTMENTS on jor.JOURNALID equals app.JOURNALID into checksAppData
                                      from check in checksAppData.DefaultIfEmpty()
                                      join status in context.PDR_STATUS on jor.STATUSID equals status.STATUSID
                                      where jor.JOURNALID == AppData.GasCheckJournalId
                                      select new
                                      {
                                          apptDate = check.APPOINTMENTSTARTDATE,
                                          apptstatus = status.TITLE
                                      });
                    if (gasAppData.Count() > 0)
                    {
                        var gasCheckData = gasAppData.First();
                        gasCheckDate = gasCheckData.apptDate;
                        gasCheckStatus = gasCheckData.apptstatus;
                    }
                }
                if (AppData.IsElectricCheckRequired == true && AppData.ElectricCheckJournalId != null)
                {
                    var electricAppData = (from jor in context.PDR_JOURNAL
                                           join app in context.PDR_APPOINTMENTS on jor.JOURNALID equals app.JOURNALID into checksAppData
                                           from check in checksAppData.DefaultIfEmpty()
                                           join status in context.PDR_STATUS on jor.STATUSID equals status.STATUSID
                                           where jor.JOURNALID == AppData.ElectricCheckJournalId
                                           select new
                                           {
                                               apptDate = check.APPOINTMENTSTARTDATE,
                                               apptstatus = status.TITLE
                                           });
                    if (electricAppData.Count() > 0)
                    {
                        var electricCheckData = electricAppData.First();
                        electricCheckDate = electricCheckData.apptDate;
                        electricCheckStatus = electricCheckData.apptstatus;
                    }
                }
            }
        }
        #endregion

        #region set Additional Data Void

        private void setAdditionalDataVoid(VoidAppointmentData alf)
        {
            /* Setting the Tenancy Information */

            if (alf.tenancyId != null)
            {
                alf.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alf.tenancyId == cut.TENANCYID && cut.CUSTOMERID == alf.defaultCustomerId select cut.CUSTOMERID).FirstOrDefault();

                alf.customerList = this.getCustomersInfo(alf.tenancyId, true, (int)alf.defaultCustomerId);


                if (alf.customerList != null)
                {
                    for (int i = 0; i < alf.customerList.Count(); i++)
                    {
                        if (alf.customerList.ElementAt(i).customerId == alf.defaultCustomerId)
                        {
                            alf.defaultCustomerIndex = i;
                        }
                    }
                }
                else
                {
                    alf.defaultCustomerId = 0;
                    alf.defaultCustomerIndex = -1;
                }
            }
            else
            {
                alf.defaultCustomerId = 0;
                alf.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (alf.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in alf.customerList)
                {
                    int customerId = (int)custData.customerId; // Line changed  - 19/06/2013

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<AsbestosData> propAsbListData = new List<AsbestosData>();
            string propertyId = alf.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);
            List<AsbestosData> propertyAsbestosData = new List<AsbestosData>();
            for (int j = 0; j < propAsbListData.Count; j++)
            {
                AsbestosData propAsbData = new AsbestosData();
                propAsbData.asbestosId = propAsbListData[j].asbestosId;
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;
                propAsbData.type = propAsbListData[j].type;
                propAsbData.riskLevel = propAsbListData[j].riskLevel;

                //add object of property risk to  appointment data 
                propertyAsbestosData.Add(propAsbData);
            }
            alf.property.propertyAsbestosData = propertyAsbestosData;
            alf.property.propertyPicture = propDal.getAllPropertyImages(propertyId); ;

        }

        #endregion

        #region Set Addition Data Appliance Defect Appointment

        private void setAdditionalDataApplianceDefectAppointment(ApplianceDefectAppointmentData defectData)
        {
            #region "Setting the Tenancy Information"

            if (defectData.tenancyId != null)
            {
                defectData.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where defectData.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                defectData.customerList = this.getCustomersInfo(defectData.tenancyId);

                if (defectData.customerList != null)
                {

                    if (defectData.customerList != null)
                    {
                        for (int i = 0; i < defectData.customerList.Count(); i++)
                        {
                            if (defectData.customerList.ElementAt(i).customerId == defectData.defaultCustomerId)
                            {
                                defectData.defaultCustomerIndex = i;
                            }
                        }
                    }
                }
                else
                {
                    defectData.defaultCustomerId = 0;
                    defectData.defaultCustomerIndex = -1;
                }
            }
            else
            {
                defectData.defaultCustomerId = 0;
                defectData.customerList = null;
            }

            #endregion

            #region "Setting the Customer Vulnerability Information"

            if (defectData.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in defectData.customerList)
                {
                    int customerId = (int)custData.customerId; // Line changed  - 19/06/2013

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            #endregion

            #region "Setting the Property Asbestos Risk Information"

            string propertyId = defectData.property.propertyId;
            int? schemeId = defectData.property.schemeId;
            int? blockId = defectData.property.blockId;

            //get property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<AsbestosData> asbListData = new List<AsbestosData>();

            if (!string.IsNullOrEmpty(propertyId))
            {
                asbListData = propDal.getPropertyAsbestosRisk(propertyId);
            }
            else if (schemeId > 0)
            {
                asbListData = propDal.getSchemeAsbestosRisk(schemeId);
            }
            else if (blockId > 0)
            {
                asbListData = propDal.getBlockAsbestosRisk(blockId);
            }

            defectData.property.propertyAsbestosData = asbListData;

            #endregion

            #region "Property Images"

            if (!string.IsNullOrEmpty(propertyId))
            {
                defectData.property.propertyPicture = propDal.getAllPropertyImages(propertyId);
            }

            #endregion

        }

        #endregion

        #region Get Appliance Defect Appointments

        public List<ApplianceDefectAppointmentData> GetApplianceDefectAppointments(string username, string startDate, string endDate, List<ExistingAppointmentParam> existingAppointments, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                                AppointmentCompleteStatus.Complete.ToString(),
                                                AppointmentCompleteStatus.Completed.ToString(),
                                                AppointmentCompleteStatus.NoEntry.ToString()};
            #region   get updated appointment from DB
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            var appointments = (from app in context.PDR_APPOINTMENTS
                                join jor in context.PDR_JOURNAL on app.JOURNALID equals jor.JOURNALID
                                join msat in context.PDR_MSAT on jor.MSATID equals msat.MSATId
                                join msatType in context.PDR_MSATType on msat.MSATTypeId equals msatType.MSATTypeId
                                join ten in context.C_TENANCY
                                        on new { TENANCYID = (Int32)msat.TenancyId }
                                        equals new { TENANCYID = ten.TENANCYID }
                                        into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join status in context.PDR_STATUS on jor.STATUSID equals status.STATUSID
                                join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                                join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                                join appDef in context.P_PROPERTY_APPLIANCE_DEFECTS on app.JOURNALID equals appDef.ApplianceDefectAppointmentJournalId
                                orderby app.APPOINTMENTID
                                where msatType.MSATTypeName.Contains("Defect") && login.LOGIN == username
                                && msat.isPending == false
                                && app.APPOINTMENTSTARTDATE >= varStartTime && app.APPOINTMENTENDDATE <= varEndTime && status.TITLE != "Completed" && status.TITLE != "Cancelled"
                                && !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.
                                select new ApplianceDefectAppointmentData
                                {
                                    appointmentId = app.APPOINTMENTID,
                                    tenancyId = tenan.TENANCYID,
                                    journalId = app.JOURNALID,
                                    journalHistoryId = app.JOURNALHISTORYID,
                                    appointmentDate = app.APPOINTMENTSTARTDATE,
                                    appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                                    appointmentEndTimeString = app.APPOINTMENTENDTIME,
                                    appointmentEndDate = app.APPOINTMENTENDDATE,
                                    surveyorUserName = login.LOGIN,
                                    loggedDate = app.LOGGEDDATE,
                                    creationDate = app.LOGGEDDATE,
                                    createdBy = app.CREATEDBY,
                                    appointmentNotes = app.APPOINTMENTNOTES ?? string.Empty,
                                    appointmentStatus = app.APPOINTMENTSTATUS == null ? "NotStarted" : app.APPOINTMENTSTATUS,
                                    appointmentType = msatType.MSATTypeName,
                                    createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                                    defaultCustomerId = msat.CustomerId,
                                    property = new PropertyData
                                    {
                                        propertyId = msat.PropertyId,
                                        tenancyId = tenan.TENANCYID,
                                        schemeId = msat.SchemeId,
                                        blockId = msat.BlockId
                                    }
                                });
            #endregion
            List<ApplianceDefectAppointmentData> appointmentList = new List<ApplianceDefectAppointmentData>();
            List<ExistingAppointmentParam> appAppointmentList = existingAppointments.OfType<ExistingAppointmentParam>().Where(s => s.appointmentType == "Appliance Defect").ToList();
            List<int> appAptIds = appAppointmentList.Select(app => app.appointmentId).ToList();
            List<ApplianceDefectAppointmentData> defectAppointmentList = new List<ApplianceDefectAppointmentData>();
            List<int> deletedIds = new List<int>();
            List<ExistingAppointmentParam> deleteAppointmentList = new List<ExistingAppointmentParam>();
            if (appointments.Count() > 0)
            {

                appointmentList = new List<ApplianceDefectAppointmentData>(appointments.Distinct().ToList());
                List<int?> dbAptIds = appointmentList.AsEnumerable().Select(app => app.appointmentId).ToList();
                deletedIds = appAptIds.Where(x => !dbAptIds.Contains(x)).ToList();
                deleteAppointmentList = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => deletedIds.Contains(s.appointmentId)).ToList();


                //get rooms list and send it with All Appointments
                foreach (ApplianceDefectAppointmentData appt in appointmentList)
                {
                    var extAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == appt.appointmentId && s.creationDate != appt.loggedDate);
                    var emptyAppApt = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == appt.appointmentId);
                    if (emptyAppApt.Count() <= 0 || extAppApt.Count() > 0)
                    {
                        if (extAppApt.Count() > 0)
                        {
                            deleteAppointmentList.Add(extAppApt.FirstOrDefault());
                        }

                        #region "Populate Property/Scheme/Block info"

                        var propertyId = appt.property.propertyId;
                        var schemeId = appt.property.schemeId;
                        var blockId = appt.property.blockId;

                        if (!string.IsNullOrEmpty(propertyId))
                        {
                            var prop = context.P__PROPERTY.Where(p => p.PROPERTYID == propertyId).FirstOrDefault();
                            appt.property.houseNumber = prop.HOUSENUMBER;
                            appt.property.flatNumber = prop.FLATNUMBER;
                            appt.property.address1 = prop.ADDRESS1;
                            appt.property.address2 = prop.ADDRESS2;
                            appt.property.address3 = prop.ADDRESS3;
                            appt.property.defaultPropertyPicId = prop.PropertyPicId;
                            appt.property.townCity = prop.TOWNCITY;
                            appt.property.postCode = prop.POSTCODE;
                            appt.property.county = prop.COUNTY;
                        }
                        else if (schemeId != null && schemeId > 0)
                        {
                            var scheme = context.P_SCHEME.Where(s => s.SCHEMEID == schemeId).FirstOrDefault();
                            appt.property.schemeName = scheme.SCHEMENAME;
                        }
                        else if (blockId != null && blockId > 0)
                        {
                            var block = context.P_BLOCK.Where(b => b.BLOCKID == blockId).FirstOrDefault();
                            appt.property.blockName = block.BLOCKNAME;
                            appt.property.address1 = block.ADDRESS1;
                            appt.property.address2 = block.ADDRESS2;
                            appt.property.address3 = block.ADDRESS3;
                            appt.property.townCity = block.TOWNCITY;
                            appt.property.postCode = block.POSTCODE;
                            appt.property.county = block.COUNTY;
                        }

                        #endregion

                        #region "Populate Defect Jobsheet Info"
                        setJobDataAppliaceDefectAppointment(appt);
                        #endregion

                        #region "Appointment Info"

                        //Concat Date and Time in single string.                
                        appt.appointmentStartDateTime = DateTime.Parse(((DateTime)appt.appointmentDate).ToString("dd/MM/yyyy") + " " + appt.appointmentStartTimeString);
                        appt.appointmentEndDateTime = DateTime.Parse(((DateTime)appt.appointmentEndDate).ToString("dd/MM/yyyy") + " " + appt.appointmentEndTimeString);

                        #endregion

                        #region "Additional Data of property and customer for appointment"

                        setAdditionalDataApplianceDefectAppointment(appt);

                        #endregion

                        defectAppointmentList.Add(appt);
                    }
                }
            }
            else
            {
                deleteAppointmentList = appAppointmentList;
            }

            foreach (ExistingAppointmentParam exApptList in deleteAppointmentList)
            {
                DeleteAppointmentParam delApp = new DeleteAppointmentParam();
                delApp.appointmentId = exApptList.appointmentId;
                delApp.appointmentType = exApptList.appointmentType;
                deleteAppointment.Add(delApp);
            }
            return defectAppointmentList;
        }

        #endregion

        #region Set Job Data for Appliance Defect Appointment

        private void setJobDataAppliaceDefectAppointment(ApplianceDefectAppointmentData appt)
        {
            var applianceDefectJobDataQuery = from appdefect in context.P_PROPERTY_APPLIANCE_DEFECTS
                                              join pdrStatus in context.PDR_STATUS on appdefect.DefectJobSheetStatus equals pdrStatus.STATUSID
                                              join t in context.G_TRADE on appdefect.TradeId equals t.TradeId

                                              join appliance in context.GS_PROPERTY_APPLIANCE on appdefect.ApplianceId equals appliance.PROPERTYAPPLIANCEID into ga
                                              from gasAppliance in ga.DefaultIfEmpty()
                                              join applianceType in context.GS_APPLIANCE_TYPE on gasAppliance.APPLIANCETYPEID equals applianceType.APPLIANCETYPEID into gat
                                              from gasApplianceType in gat.DefaultIfEmpty()
                                              join m in context.GS_MANUFACTURER on gasAppliance.MANUFACTURERID equals m.MANUFACTURERID into tempmanf
                                              from manf in tempmanf.DefaultIfEmpty()

                                              join parameterValue in context.PA_PARAMETER_VALUE on appdefect.BoilerTypeId equals parameterValue.ValueID into pv
                                              from boilerType in pv.DefaultIfEmpty()

                                              where appdefect.ApplianceDefectAppointmentJournalId == appt.journalId
                                              select new ApplianceDefectJobData
                                              {
                                                  defectId = appdefect.PropertyDefectId,
                                                  jobStatus = pdrStatus.TITLE,
                                                  applianceId = appdefect.ApplianceId,
                                                  boilerTypeId = appdefect.BoilerTypeId,
                                                  heatingId = appdefect.HeatingMappingId,
                                                  defectRecordedBy = appdefect.CreatedBy,// ?? appointment.ASSIGNEDTO, //TODO:Check alternative for defectRecordedBy
                                                  defectDate = appdefect.DefectDate ?? appdefect.DateCreated,
                                                  trade = t.Description,
                                                  appliance = gasApplianceType.APPLIANCETYPE ?? boilerType.ValueDetail,
                                                  make = manf.MANUFACTURER ?? string.Empty,
                                                  model = gasAppliance.MODEL ?? string.Empty,
                                                  isTwoPersonsJob = appdefect.IsTwoPersonsJob ?? false,
                                                  defectCategoryId = (int)appdefect.CategoryId,
                                                  defectNotes = appdefect.DefectNotes ?? String.Empty,
                                                  isRemedialActionTaken = appdefect.IsActionTaken ?? false,
                                                  remedialActionNotes = appdefect.ActionNotes ?? String.Empty,
                                                  isAdviceNoteIssued = appdefect.IsWarningIssued,
                                                  warningNoteSerialNo = appdefect.WarningNoteSerialNo,
                                                  serialNumber = appdefect.SerialNumber ?? String.Empty,
                                                  gasCouncilNumber = appdefect.GasCouncilNumber ?? String.Empty,
                                                  warningTagFixed = appdefect.IsWarningFixed ?? false,
                                                  isDisconnected = appdefect.IsDisconnected,
                                                  partsOrderedBy = appdefect.PartsOrderedBy,
                                                  partsDueDate = appdefect.PartsDue,
                                                  partsLocation = appdefect.PartsLocation ?? string.Empty
                                              };

            if (applianceDefectJobDataQuery.Count() > 0)
            {
                appt.jobDataList = applianceDefectJobDataQuery.ToList();

                FaultsDal objFaultsDal = new FaultsDal();

                foreach (ApplianceDefectJobData jobData in appt.jobDataList)
                {
                    jobData.jobStatus = (jobData.jobStatus.Trim() == "Arranged") ? "NotStarted" : jobData.jobStatus.Trim().Replace(" ", "");
                    jobData.JSNumber = GeneralHelper.getJobSheetDefectNumber(jobData.defectId);
                    jobData.inspectionRef = appt.journalId.ToString().PadLeft(6, '0');

                    //fetch boiler model and manufacturer information
                    if (jobData.heatingId != null && jobData.heatingId > 0)
                    {
                        var heatingInfo = context.PA_HeatingMapping.Where(j => j.HeatingMappingId == jobData.heatingId).FirstOrDefault();

                        if (heatingInfo != null)
                        {
                            var itemName = (heatingInfo.PropertyId != null) ? ApplicationConstants.HeatingItemName : ApplicationConstants.BoilerRoomItemName;
                            jobData.model = getSelectedParameterValueOfProperty(jobData.heatingId, itemName, ApplicationConstants.HeatingModelParameterKey);
                            jobData.make = getSelectedParameterValueOfProperty(jobData.heatingId, itemName, ApplicationConstants.HeatingManufacturerParameterKey);
                        }
                    }

                    jobData.jobSheetImageList = objFaultsDal.getAllDefectImagesByDefectId(jobData.defectId);
                }
            }
        }

        #endregion

        #region Get Selected Parameter Value Of Property
        public string getSelectedParameterValueOfProperty(int? heatingId, string itemName, string parameterName)
        {
            string result = string.Empty;
            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == parameterName && i.ItemName == itemName
                             select new { ItemParamID = ip.ItemParamID, HeatingFuel = ip.ParameterValueId });


            // Currently multiple heating types exists against the property but in future when scheme/block will
            // contains multiple heating types same filter will be applied to scheme/block as well. 
            var heating = context.PA_HeatingMapping.Where(x => x.HeatingMappingId == heatingId).FirstOrDefault();
            if (heating.PropertyId != null)
            {
                itemParam = itemParam.Where(x => x.HeatingFuel == heating.HeatingType);
            }

            if (itemParam.Count() > 0)
            {
                int itemParamId = itemParam.First().ItemParamID;
                var attributeId = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.HeatingMappingId == heatingId && patt.ITEMPARAMID == itemParamId);
                if (attributeId.Count() > 0)
                {
                    result = attributeId.First().PARAMETERVALUE;
                }
            }
            return result;
        }
        #endregion

        #region get fault Detail List for common data
        /// <summary>
        /// get fault Detail List for common data
        /// </summary>
        /// <returns></returns>
        public List<FaultDetailList> getFaultDetailList()
        {
            List<FaultDetailList> faultList = new List<FaultDetailList>();

            var faultRepairList = (from fault in context.FL_FAULT
                                   join priority in context.FL_FAULT_PRIORITY on fault.PriorityID equals priority.PriorityID
                                   where fault.FaultActive == true
                                   select new FaultDetailList
                                   {
                                       faultId = fault.FaultID,
                                       faultDescription = fault.Description,
                                       gross = fault.Gross,
                                       isRecharge = fault.Recharge,
                                       priorityName = priority.PriorityName,
                                       days = priority.Days,
                                       response = priority.ResponseTime
                                   }
            );

            if (faultRepairList.Count() > 0)
            {
                faultList = faultRepairList.ToList();
                foreach (FaultDetailList detail in faultList)
                {
                    if (detail.days != null)
                    {
                        if (detail.days == true)
                        {
                            detail.responseTime = detail.response.ToString() + "days";
                        }
                        else
                        {
                            detail.responseTime = detail.response.ToString() + "hours";
                        }
                    }
                }
            }

            return faultList;


        }
        #endregion

        #region get fault area List for common data
        /// <summary>
        /// get fault area List for common data
        /// </summary>
        /// <returns></returns>
        public List<FaultArea> getFaultAreaList()
        {
            List<FaultArea> areaList = new List<FaultArea>();

            var faultAreaList = (from area in context.FL_AREA
                                 orderby area.AreaName
                                 select new FaultArea
                                 {
                                     faultAreaId = area.AreaID,
                                     faultAreaName = area.AreaName
                                 }
            );

            if (faultAreaList.Count() > 0)
            {
                areaList = faultAreaList.ToList();
            }

            return areaList;
        }
        #endregion
    }
}
