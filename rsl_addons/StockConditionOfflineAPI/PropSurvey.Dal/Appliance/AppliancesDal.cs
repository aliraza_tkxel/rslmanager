﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Dal.Faults;
using PropSurvey.Dal.InstallationPipework;

namespace PropSurvey.Dal.Appliances
{
    public class AppliancesDal : BaseDal
    {
        #region get All Appliances
        /// <summary>
        /// This function returns all the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        //public List<ApplianceData> getAllAppliancesByPropertyId(string propertyID, int appointmentID)
        //{
        //    var appliances = (from papp in context.GS_PROPERTY_APPLIANCE
        //                      join p2a in context.PS_Property2Appointment on papp.PROPERTYID equals p2a.PropertyId
        //                      join app in context.PS_Appointment on p2a.AppointId equals app.AppointId
        //                      join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID into tempLocation
        //                      from Locations in tempLocation.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - START
        //                      join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID into tempManufacturer
        //                      from Manufacturer in tempManufacturer.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - END
        //                      join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
        //                      from Models in tempModel.DefaultIfEmpty()
        //                      //join isIns in context.GS_ApplianceInspection on app.AppointId equals isIns.APPOINTMENTID into Insp
        //                      //from isInspected in Insp.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - START
        //                      join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID into tempType
        //                      from ApplianceType in tempType.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - END
        //                      where papp.PROPERTYID == propertyID && app.AppointId == appointmentID
        //                      select new ApplianceData
        //                      {
        //                          ApplianceID = papp.PROPERTYAPPLIANCEID,
        //                          //ApplianceOrgID = papp.ORGID,
        //                          FluType = papp.FLUETYPE,
        //                          InstalledDate = papp.DATEINSTALLED,
        //                          PropertyID = propertyID,
        //                          //isInspected = isInspected.APPLIANCEID != null,
        //                          isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
        //                          ReplacementDate = papp.REPLACEMENTDATE,
        //                          Model = papp.MODEL,
        //                          ApplianceLocation = new AppliancesLocationData
        //                          {
        //                              //Location = loc.LOCATION,
        //                              //LocationID = loc.LOCATIONID
        //                              Location = Locations.LOCATION != null ? Locations.LOCATION : "",
        //                              LocationID = Locations.LOCATIONID != null ? Locations.LOCATIONID : 0
        //                          },
        //                          ApplianceManufacturer = new ManufacturerData
        //                          {
        //                              // Line modified  - 05/07/2013 - START
        //                              Manufacturer = Manufacturer.MANUFACTURER != null ? Manufacturer.MANUFACTURER : "",
        //                              ManufacturerID = Manufacturer.MANUFACTURERID != null ? Manufacturer.MANUFACTURERID : 0
        //                              // Line modified  - 05/07/2013 - END
        //                          },
        //                          ApplianceModel = new ApplianceModelData
        //                          {
        //                              ApplianceModelID = Models.ModelID != null ? Models.ModelID : 0,
        //                              ApplianceModel = Models.Model != null ? Models.Model : ""
        //                          },
        //                          ApplianceType = new ApplianceTypeData
        //                          {
        //                              // Line modified  - 05/07/2013 - START
        //                              ApplianceType = ApplianceType.APPLIANCETYPE != null ? ApplianceType.APPLIANCETYPE : "",
        //                              ApplianceTypeID = ApplianceType.APPLIANCETYPEID != null ? ApplianceType.APPLIANCETYPEID : 0
        //                              // Line modified  - 05/07/2013 - END
        //                          }
        //                      });

        //    List<ApplianceData> appliancesList = new List<ApplianceData>();
        //    if (appliances.Count() > 0)
        //    {
        //        appliancesList = appliances.ToList();
        //        foreach (ApplianceData appl in appliancesList)
        //        {
        //            appl.isInspected = this.isInspected(appl.ApplianceID);
        //        }
        //    }

        //    return appliancesList;
        //}

        ////Change#31 - Behroz - 12/07/2012 - Start
        #endregion

        #region get All Appliances By GAS Appointment
        /// <summary>
        /// Get All Appliances By GAS Appointment
        /// </summary>
        /// <param name="propertyID"></param>
        /// <param name="journalId"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <returns></returns>
        public List<ApplianceData> getAllGasAppliances(ApplianceSurveyDataParam requestParam)
        {

            var appliances = (from papp in context.GS_PROPERTY_APPLIANCE

                              join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID into tempLocation
                              from Locations in tempLocation.DefaultIfEmpty()

                              join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID into tempManufacturer
                              from Manufacturer in tempManufacturer.DefaultIfEmpty()

                              join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
                              from Models in tempModel.DefaultIfEmpty()

                              join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID into tempType
                              from ApplianceType in tempType.DefaultIfEmpty()

                              where ((papp.PROPERTYID == requestParam.propertyId) || (papp.SchemeID == requestParam.schemeId) || (papp.BlockID == requestParam.blockId))
                              && papp.IsActive == true

                              select new ApplianceData
                              {
                                  ApplianceID = papp.PROPERTYAPPLIANCEID,
                                  SerialNumber = papp.SerialNumber,
                                  FluType = papp.FLUETYPE,
                                  InstalledDate = papp.DATEINSTALLED,
                                  propertyId = papp.PROPERTYID,
                                  schemeId = papp.SchemeID,
                                  blockId = papp.BlockID,
                                  isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
                                  isActive = papp.IsActive,
                                  GCNumber = papp.GasCouncilNumber,
                                  Model = papp.MODEL,
                                  ReplacementDate = papp.REPLACEMENTDATE,
                                  ApplianceLocation = new AppliancesLocationData
                                  {
                                      Location = Locations.LOCATION,
                                      LocationID = Locations.LOCATIONID
                                  },
                                  ApplianceManufacturer = new ManufacturerData
                                  {
                                      Manufacturer = Manufacturer.MANUFACTURER,
                                      ManufacturerID = Manufacturer.MANUFACTURERID
                                  },
                                  ApplianceModel = new ApplianceModelData
                                  {
                                      ApplianceModelID = Models.ModelID,
                                      ApplianceModel = Models.Model
                                  },
                                  ApplianceType = new ApplianceTypeData
                                  {
                                      ApplianceType = ApplianceType.APPLIANCETYPE,
                                      ApplianceTypeID = ApplianceType.APPLIANCETYPEID
                                  }
                              });

            List<ApplianceData> appliancesList = new List<ApplianceData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();

                foreach (ApplianceData applData in appliancesList)
                {
                    applData.isInspected = false;
                    int count = getInspectedAppliancesGas(requestParam.journalId, applData.ApplianceID);
                    if (count > 0)
                    {
                        applData.isInspected = true;
                    }
                    FaultsDal faultDal = new FaultsDal();
                    applData.ApplianceDefects = faultDal.getFaultsDataGas(applData.ApplianceID, requestParam);
                    applData.ApplianceInspection = getApplianceInspectionByApplianceIDGas(applData.ApplianceID, (int)requestParam.journalId);
                }
            }

            return appliancesList;
        }
        #endregion

        #region get All Boilers
        public List<BoilerData> getAllHeatingBoilers(ApplianceSurveyDataParam requestParam)
        {
            List<BoilerData> boilerList = new List<BoilerData>();
            string itemName = string.Empty;
            string boilerInitial = string.Empty;
            string boilerParamName = ApplicationConstants.HeatingTypeParameterKey;
            var boilerListQuery = (from phm in context.PA_HeatingMapping
                                   join hf in context.PA_PARAMETER_VALUE on phm.HeatingType equals hf.ValueID
                                   where phm.IsActive == true
                                         && hf.ValueDetail == ApplicationConstants.MainsGasParameterValue
                                   orderby phm.HeatingMappingId ascending
                                   select phm);

            if (requestParam.propertyId != null)
            {
                boilerListQuery = boilerListQuery.Where(x => x.PropertyId == requestParam.propertyId);
                itemName = ApplicationConstants.HeatingItemName;                
                boilerInitial = ApplicationConstants.HeatingTypePrefix;
            }
            else if (requestParam.schemeId != null && requestParam.schemeId > 0)
            {
                boilerListQuery = boilerListQuery.Where(x => x.SchemeID == requestParam.schemeId);                
                itemName = ApplicationConstants.BoilerRoomItemName;
                boilerInitial = ApplicationConstants.BoilerPrefix;
            }
            else if (requestParam.blockId != null && requestParam.blockId > 0)
            {
                boilerListQuery = boilerListQuery.Where(x => x.BlockID == requestParam.blockId);                
                itemName = ApplicationConstants.BoilerRoomItemName;
                boilerInitial = ApplicationConstants.BoilerPrefix;
            }

            int counter = 0;
            foreach (var boiler in boilerListQuery)
            {

                counter++;

                BoilerData objBoiler = new BoilerData();
                objBoiler.boilerName = string.Format("{0} {1}", boilerInitial, counter);
                objBoiler.itemId = context.PA_ITEM.Where(x => x.ItemName == itemName).Select(y => y.ItemID).FirstOrDefault();
                objBoiler.heatingId = boiler.HeatingMappingId;
                objBoiler.boilerTypeId = getSelectedDropdownValue(itemName, boilerParamName, boiler.HeatingMappingId);
                objBoiler.manufacturerId = getSelectedDropdownValue(itemName, ApplicationConstants.HeatingManufacturerParameterKey, boiler.HeatingMappingId);
                objBoiler.flueTypeId = getSelectedDropdownValue(itemName, ApplicationConstants.FlueTypeParameterKey, boiler.HeatingMappingId);
                objBoiler.model = getPopulatedTextBoxValue(itemName, ApplicationConstants.HeatingModelParameterKey, boiler.HeatingMappingId);
                objBoiler.gcNumber = getPopulatedTextBoxValue(itemName, ApplicationConstants.GcNumberParameterKey, boiler.HeatingMappingId);
                objBoiler.serialNumber = getPopulatedTextBoxValue(itemName, ApplicationConstants.SerialNoParameterKey, boiler.HeatingMappingId);
                objBoiler.location = getPopulatedTextBoxValue(itemName, ApplicationConstants.BoilerLocationParameterKey, boiler.HeatingMappingId);
                objBoiler.isLandlordAppliance = getSelectedCheckBoxParameterValue(itemName, ApplicationConstants.LandlordApplianceParameterKey, boiler.HeatingMappingId);
                objBoiler.gasketReplacementDate = getSelectedParameterDateValue(itemName, ApplicationConstants.GasketSetReplacementParameterKey, boiler.HeatingMappingId);

                var itemDatesQuery = (from pid in context.PA_PROPERTY_ITEM_DATES
                                      join pit in context.PA_ITEM on pid.ItemId equals pit.ItemID
                                      where pit.ItemName == itemName && pid.HeatingMappingId == boiler.HeatingMappingId && (pid.LastDone != null && pid.DueDate != null)
                                      select pid);
                if (itemDatesQuery.Count() > 0)
                {
                    objBoiler.lastReplaced = itemDatesQuery.First().LastDone;
                    objBoiler.replacementDue = itemDatesQuery.First().DueDate;
                }

                objBoiler.propertyId = requestParam.propertyId;
                objBoiler.schemeId = requestParam.schemeId;
                objBoiler.blockId = requestParam.blockId;

                int count = getInspectedBoilers(requestParam.journalId, boiler.HeatingMappingId);
                objBoiler.isInspected = (count > 0) ? true : false;
                objBoiler.BoilerInspection = getBoilerInspectionByJournalId(boiler.HeatingMappingId, requestParam.journalId);

                FaultsDal faultDal = new FaultsDal();
                objBoiler.BoilerDefects = faultDal.getBoilerDefects(boiler.HeatingMappingId, requestParam.journalId);

                InstallationPipeworkDal instDal = new InstallationPipeworkDal();
                objBoiler.InstallationPipework = instDal.getInstallationPipeworkFormDataGas(boiler.HeatingMappingId, requestParam.journalId);

                boilerList.Add(objBoiler);

            }


            return boilerList;
        }
        #endregion

        #region Get All Oil Heatings
        public List<OilHeatingData> getOilHeatings(OilSurveyDataParam requestParam)
        {
            List<OilHeatingData> heatingList = new List<OilHeatingData>();
            string itemName = string.Empty;
            string heatingInitial = string.Empty;
            string heatingParamName = string.Empty;
            var heatingListQuery = (from phm in context.PA_HeatingMapping
                                    join hf in context.PA_PARAMETER_VALUE on phm.HeatingType equals hf.ValueID
                                    where phm.IsActive == true
                                          && hf.ValueDetail == ApplicationConstants.OilParameterValue
                                    orderby phm.HeatingMappingId ascending
                                    select phm);

            if (requestParam.propertyId != null)
            {
                heatingListQuery = heatingListQuery.Where(x => x.PropertyId == requestParam.propertyId);
                itemName = ApplicationConstants.HeatingItemName;
                heatingParamName = ApplicationConstants.HeatingTypeParameterKey;
                heatingInitial = ApplicationConstants.HeatingTypePrefix;
            }
            else if (requestParam.schemeId != null && requestParam.schemeId > 0)
            {
                heatingListQuery = heatingListQuery.Where(x => x.SchemeID == requestParam.schemeId);
                heatingParamName = ApplicationConstants.BoilerTypeParameterKey;
                itemName = ApplicationConstants.BoilerRoomItemName;
                heatingInitial = ApplicationConstants.BoilerPrefix;
            }
            else if (requestParam.blockId != null && requestParam.blockId > 0)
            {
                heatingListQuery = heatingListQuery.Where(x => x.BlockID == requestParam.blockId);
                heatingParamName = ApplicationConstants.BoilerTypeParameterKey;
                itemName = ApplicationConstants.BoilerRoomItemName;
                heatingInitial = ApplicationConstants.BoilerPrefix;
            }

            int counter = 0;
            foreach (var heating in heatingListQuery)
            {

                counter++;

                OilHeatingData objHeating = new OilHeatingData();

                #region "Heating General Info"

                objHeating.heatingId = heating.HeatingMappingId;
                objHeating.heatingName = string.Format("{0} {1}", heatingInitial, counter);
                objHeating.heatingFuel = context.PA_PARAMETER_VALUE.Where(x => x.ValueID == heating.HeatingType).Select(y => y.ValueDetail).FirstOrDefault();
                objHeating.itemId = context.PA_ITEM.Where(x => x.ItemName == itemName).Select(y => y.ItemID).FirstOrDefault();
                var heatingTypeID = getSelectedDropdownValue(itemName, heatingParamName, heating.HeatingMappingId);
                objHeating.heatingType = context.PA_PARAMETER_VALUE.Where(x => x.ValueID == heatingTypeID).Select(y => y.ValueDetail).FirstOrDefault();

                objHeating.applianceMake = getPopulatedTextBoxValue(itemName, ApplicationConstants.ApplianceMakeParameterKey, heating.HeatingMappingId);
                objHeating.applianceModel = getPopulatedTextBoxValue(itemName, ApplicationConstants.ApplianceModelParameterKey, heating.HeatingMappingId);
                objHeating.applianceSerialNumber = getPopulatedTextBoxValue(itemName, ApplicationConstants.ApplianceSerialNumberParameterKey, heating.HeatingMappingId);
                objHeating.applianceLocation = getPopulatedTextBoxValue(itemName, ApplicationConstants.BoilerLocationParameterKey, heating.HeatingMappingId);
                objHeating.originalInstallDate = getSelectedParameterDateValue(itemName, ApplicationConstants.OriginalInstallDateParameterKey, heating.HeatingMappingId);

                objHeating.burnerMake = getPopulatedTextBoxValue(itemName, ApplicationConstants.BurnerMakeParameterKey, heating.HeatingMappingId);
                objHeating.burnerModel = getPopulatedTextBoxValue(itemName, ApplicationConstants.BurnerModelParameterKey, heating.HeatingMappingId);
                objHeating.burnerTypeId = getSelectedDropdownValue(itemName, ApplicationConstants.BurnerTypeParameterKey, heating.HeatingMappingId);

                objHeating.tankTypeId = getSelectedDropdownValue(itemName, ApplicationConstants.TankTypeParameterKey, heating.HeatingMappingId);
                objHeating.flueTypeId = getSelectedDropdownValue(itemName, ApplicationConstants.FlueTypeParameterKey, heating.HeatingMappingId);
                objHeating.fuelTypeId = getSelectedDropdownValue(itemName, ApplicationConstants.FuelTypeParameterKey, heating.HeatingMappingId);

                objHeating.propertyId = requestParam.propertyId;
                objHeating.schemeId = requestParam.schemeId;
                objHeating.blockId = requestParam.blockId;

                #endregion

                #region "Certificate Info"

                var certificateName = context.P_LGSR_Certificates
                                         .Where(x => x.HeatingType == heating.HeatingType)
                                         .Select(y => y.Title)
                                         .FirstOrDefault();

                if (certificateName != null)
                {
                    objHeating.certificateName = certificateName;

                    var certificateQuery = (from plg in context.P_LGSR
                                            join ht in context.PA_HeatingMapping on plg.HeatingMappingId equals ht.HeatingMappingId
                                            where ht.HeatingType == heating.HeatingType
                                            orderby plg.ISSUEDATE descending
                                            select plg);


                    if (requestParam.propertyId != null)
                    {
                        certificateQuery = certificateQuery.Where(x => x.PROPERTYID == requestParam.propertyId).OrderByDescending(o => o.ISSUEDATE);
                    }
                    else if (requestParam.schemeId != null && requestParam.schemeId > 0)
                    {
                        certificateQuery = certificateQuery.Where(x => x.SchemeId == requestParam.schemeId);
                    }
                    else if (requestParam.blockId != null && requestParam.blockId > 0)
                    {
                        certificateQuery = certificateQuery.Where(x => x.BlockId == requestParam.blockId);
                    }

                    var certificateValues = certificateQuery.FirstOrDefault();

                    if (certificateValues != null)
                    {
                        objHeating.certificateIssued = (certificateValues.ISSUEDATE != null ? certificateValues.ISSUEDATE : null);
                        objHeating.certificateNumber = (certificateValues.CP12NUMBER != null ? certificateValues.CP12NUMBER : null);
                        objHeating.certificateRenewel = (certificateValues.CP12Renewal != null ? certificateValues.CP12Renewal : null);
                    }
                }

                #endregion

                #region "Inspection Info"

                objHeating.oilFiringAndService = getOilInspectionByJournalId(heating.HeatingMappingId, requestParam.journalId);
                objHeating.isInspected = false;

                #endregion

                heatingList.Add(objHeating);

            }


            return heatingList;
        }
        #endregion

        #region Get Alternative Heatings
        public List<AlternativeHeatingData> getAlternativeHeatings(AlternativeSurveyDataParam requestParam)
        {
            List<AlternativeHeatingData> heatingList = new List<AlternativeHeatingData>();
            string itemName = string.Empty;
            string heatingInitial = string.Empty;
            string heatingParamName = string.Empty;
            var heatingListQuery = (from phm in context.PA_HeatingMapping
                                    join hf in context.PA_PARAMETER_VALUE on phm.HeatingType equals hf.ValueID
                                    where phm.IsActive == true
                                          && hf.IsAlterNativeHeating == true
                                    orderby phm.HeatingMappingId ascending
                                    select phm);

            if (requestParam.propertyId != null)
            {
                heatingListQuery = heatingListQuery.Where(x => x.PropertyId == requestParam.propertyId);
                itemName = ApplicationConstants.HeatingItemName;
                heatingParamName = ApplicationConstants.HeatingTypeParameterKey;
                heatingInitial = ApplicationConstants.HeatingTypePrefix;
            }
            else if (requestParam.schemeId != null && requestParam.schemeId > 0)
            {
                heatingListQuery = heatingListQuery.Where(x => x.SchemeID == requestParam.schemeId);
                heatingParamName = ApplicationConstants.BoilerTypeParameterKey;
                itemName = ApplicationConstants.BoilerRoomItemName;
                heatingInitial = ApplicationConstants.BoilerPrefix;
            }
            else if (requestParam.blockId != null && requestParam.blockId > 0)
            {
                heatingListQuery = heatingListQuery.Where(x => x.BlockID == requestParam.blockId);
                heatingParamName = ApplicationConstants.BoilerTypeParameterKey;
                itemName = ApplicationConstants.BoilerRoomItemName;
                heatingInitial = ApplicationConstants.BoilerPrefix;
            }

            int counter = 0;
            foreach (var heating in heatingListQuery)
            {

                counter++;

                AlternativeHeatingData objHeating = new AlternativeHeatingData();

                #region "Heating General Info"

                objHeating.heatingId = heating.HeatingMappingId;
                objHeating.itemId = context.PA_ITEM.Where(x => x.ItemName == itemName).Select(y => y.ItemID).FirstOrDefault();
                objHeating.heatingName = string.Format("{0} {1}", heatingInitial, counter);
                objHeating.heatingFuel = context.PA_PARAMETER_VALUE.Where(x => x.ValueID == heating.HeatingType).Select(y => y.ValueDetail).FirstOrDefault();
                var heatingTypeID = getSelectedDropdownValue(itemName, heatingParamName, heating.HeatingMappingId);
                objHeating.heatingType = context.PA_PARAMETER_VALUE.Where(x => x.ValueID == heatingTypeID).Select(y => y.ValueDetail).FirstOrDefault();

                objHeating.manufacturerId = getSelectedDropdownValue(itemName, ApplicationConstants.HeatingManufacturerParameterKey, heating.HeatingMappingId);
                objHeating.model = getPopulatedTextBoxValue(itemName, ApplicationConstants.HeatingModelParameterKey, heating.HeatingMappingId);
                objHeating.serialNumber = getPopulatedTextBoxValue(itemName, ApplicationConstants.SerialNoParameterKey, heating.HeatingMappingId);
                objHeating.location = getPopulatedTextBoxValue(itemName, ApplicationConstants.BoilerLocationParameterKey, heating.HeatingMappingId);
                objHeating.installedDate = getSelectedParameterDateValue(itemName, ApplicationConstants.OriginalInstallDateParameterKey, heating.HeatingMappingId);

                objHeating.solarTypeId = getSelectedDropdownValue(itemName, ApplicationConstants.SolarTypeParameterKey, heating.HeatingMappingId);

                objHeating.propertyId = requestParam.propertyId;
                objHeating.schemeId = requestParam.schemeId;
                objHeating.blockId = requestParam.blockId;

                #endregion

                #region "Certificate Info"

                var certificateName = context.P_LGSR_Certificates
                                         .Where(x => x.HeatingType == heating.HeatingType)
                                         .Select(y => y.Title)
                                         .FirstOrDefault();

                if (certificateName != null)
                {
                    objHeating.certificateName = certificateName;

                    var certificateQuery = (from plg in context.P_LGSR
                                            join ht in context.PA_HeatingMapping on plg.HeatingMappingId equals ht.HeatingMappingId
                                            where ht.HeatingType == heating.HeatingType
                                            orderby plg.ISSUEDATE descending
                                            select plg);


                    if (requestParam.propertyId != null)
                    {
                        certificateQuery = certificateQuery.Where(x => x.PROPERTYID == requestParam.propertyId).OrderByDescending(o => o.ISSUEDATE);
                    }
                    else if (requestParam.schemeId != null && requestParam.schemeId > 0)
                    {
                        certificateQuery = certificateQuery.Where(x => x.SchemeId == requestParam.schemeId);
                    }
                    else if (requestParam.blockId != null && requestParam.blockId > 0)
                    {
                        certificateQuery = certificateQuery.Where(x => x.BlockId == requestParam.blockId);
                    }

                    var certificateValues = certificateQuery.FirstOrDefault();

                    if (certificateValues != null)
                    {
                        objHeating.certificateIssued = (certificateValues.ISSUEDATE != null ? certificateValues.ISSUEDATE : null);
                        objHeating.certificateNumber = (certificateValues.CP12NUMBER != null ? certificateValues.CP12NUMBER : null);
                        objHeating.certificateRenewel = (certificateValues.CP12Renewal != null ? certificateValues.CP12Renewal : null);
                    }
                }

                #endregion

                #region "Inspection Info"

                objHeating.airsourceInspection = getAirSourceInspectionByJournalId(heating.HeatingMappingId, requestParam.journalId);
                objHeating.mvhrInspection = getMvhrInspectionByJournalId(heating.HeatingMappingId, requestParam.journalId);
                objHeating.solarInspection = getSolarInspectionByJournalId(heating.HeatingMappingId, requestParam.journalId);

                objHeating.isInspected = objHeating.airsourceInspection == null && objHeating.mvhrInspection == null && objHeating.solarInspection == null ? false : true;

                #endregion

                heatingList.Add(objHeating);

            }


            return heatingList;
        }
        #endregion

        #region Get TextBox Parameter Value
        public string getPopulatedTextBoxValue(string itemName, string parameterName, int? heatingId)
        {
            string result = "";

            var resultQuery = (from ppa in context.PA_PROPERTY_ATTRIBUTES
                               join pip in context.PA_ITEM_PARAMETER on ppa.ITEMPARAMID equals pip.ItemParamID
                               join pp in context.PA_PARAMETER on pip.ParameterId equals pp.ParameterID
                               join pii in context.PA_ITEM on pip.ItemId equals pii.ItemID
                               where ppa.HeatingMappingId == heatingId && pp.ParameterName == parameterName && pii.ItemName == itemName
                               select ppa);

            if (resultQuery.Count() > 0)
            {
                var attrResult = resultQuery.FirstOrDefault();

                if (attrResult != null)
                {
                    result = attrResult.PARAMETERVALUE;
                }
            }

            return result;
        }
        #endregion

        #region Get selected parameter date value
        public DateTime? getSelectedParameterDateValue(string itemName, string parameterName, int? heatingId)
        {
            DateTime? parameterDate = null;
            var itemDatesQuery = (from pid in context.PA_PROPERTY_ITEM_DATES
                                  join pit in context.PA_ITEM on pid.ItemId equals pit.ItemID
                                  join pp in context.PA_PARAMETER on pid.ParameterId equals pp.ParameterID
                                  where pit.ItemName == itemName
                                 && pp.ParameterName == parameterName
                                 && pid.HeatingMappingId == heatingId
                                 && pid.DueDate != null
                                  select pid);

            if (itemDatesQuery.Count() > 0)
            {
                parameterDate = itemDatesQuery.FirstOrDefault().DueDate;
            }

            return parameterDate;
        }
        #endregion

        #region Get Checkbox Parameter Value
        public bool? getSelectedCheckBoxParameterValue(string itemName, string parameterName, int? heatingId)
        {
            bool? result = null;


            var resultQuery = (from ppa in context.PA_PROPERTY_ATTRIBUTES
                               join pip in context.PA_ITEM_PARAMETER on ppa.ITEMPARAMID equals pip.ItemParamID
                               join pp in context.PA_PARAMETER on pip.ParameterId equals pp.ParameterID
                               join pii in context.PA_ITEM on pip.ItemId equals pii.ItemID
                               where ppa.HeatingMappingId == heatingId && pp.ParameterName == parameterName && pii.ItemName == itemName
                               select ppa);

            if (resultQuery.Count() > 0)
            {
                var attrResult = resultQuery.FirstOrDefault();

                if (attrResult != null)
                {
                    result = attrResult.IsCheckBoxSelected;
                }
            }
            else if (resultQuery.Count() == 0 && parameterName == "Landlord Appliance") // This is the business rule to set Landlord Appliance value true
            {
                result = true;
            }

            return result;
        }
        #endregion

        #region Get Selected Dropdown Value Id
        public int? getSelectedDropdownValue(string itemName, string parameterName, int? heatingId)
        {
            int? result = null;

            var resultQuery = (from ppa in context.PA_PROPERTY_ATTRIBUTES
                               join pip in context.PA_ITEM_PARAMETER on ppa.ITEMPARAMID equals pip.ItemParamID
                               join pp in context.PA_PARAMETER on pip.ParameterId equals pp.ParameterID
                               join pii in context.PA_ITEM on pip.ItemId equals pii.ItemID
                               where ppa.HeatingMappingId == heatingId && pp.ParameterName == parameterName && pii.ItemName == itemName
                               select ppa);

            if (resultQuery.Count() > 0)
            {
                var attrResult = resultQuery.FirstOrDefault();

                if (attrResult != null)
                {
                    result = (int?)attrResult.VALUEID;
                }
            }
            return result;
        }
        #endregion

        #region Get Parameter Value Id
        public int? getParameterValueIdValue(AttributeGeneralInfo attrInfo, string itemName, string parameterName)
        {
            int? result = null;
            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == parameterName
                            && i.ItemName == itemName
                             select new { ip.ItemParamID, heatingFuel = ip.ParameterValueId });


            // Currently multiple heating types exists against the property but in future when scheme/block will
            // contains multiple heating types same filter will be applied to scheme/block as well. 
            if (attrInfo.propertyId != null)
            {
                var heatingType = context.PA_HeatingMapping.Where(x => x.HeatingMappingId == attrInfo.heatingId).First();
                itemParam = itemParam.Where(x => x.heatingFuel == heatingType.HeatingType);
            }

            if (itemParam.Count() > 0)
            {
                int itemParamId = itemParam.First().ItemParamID;
                var attributeId = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.HeatingMappingId == attrInfo.heatingId && patt.ITEMPARAMID == itemParamId);
                if (attributeId.Count() > 0)
                {
                    result = (int?)attributeId.First().VALUEID;
                }
            }
            return result;
        }
        #endregion

        #region get All Appliances Type
        /// <summary>
        /// This function returns all the Appliances Type
        /// </summary>
        /// <returns>List of Appliances Type data objects</returns>
        public List<ApplianceTypeData> getAllApplianceTypes()
        {
            var applianceType = (from type in context.GS_APPLIANCE_TYPE
                                 select new ApplianceTypeData
                                 {
                                     ApplianceTypeID = type.APPLIANCETYPEID,
                                     ApplianceType = type.APPLIANCETYPE
                                 });



            List<ApplianceTypeData> appliancesTypeList = new List<ApplianceTypeData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Models
        /// <summary>
        /// This function returns all the Appliances Model
        /// </summary>
        /// <returns>List of Appliances Model data objects</returns>
        public List<ApplianceModelData> getAllApplianceModel()
        {
            var applianceType = (from model in context.GS_ApplianceModel
                                 select new ApplianceModelData
                                 {
                                     ApplianceModelID = model.ModelID,
                                     ApplianceModel = model.Model
                                 });

            List<ApplianceModelData> appliancesTypeList = new List<ApplianceModelData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Manufacturer
        /// <summary>
        /// This function returns all the Appliances Manufacturer
        /// </summary>
        /// <returns>List of Appliances Manufacturer data objects</returns>
        public List<ManufacturerData> getAllApplianceManufacturer()
        {
            var applianceType = (from manufactur in context.GS_MANUFACTURER
                                 select new ManufacturerData
                                 {
                                     ManufacturerID = manufactur.MANUFACTURERID,
                                     Manufacturer = manufactur.MANUFACTURER
                                 });

            List<ManufacturerData> appliancesTypeList = new List<ManufacturerData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Locations
        /// <summary>
        /// This function returns all the Appliances Location
        /// </summary>
        /// <returns>List of Appliances Location data objects</returns>
        public List<AppliancesLocationData> getAllApplianceLocations()
        {
            var applianceType = (from location in context.GS_LOCATION
                                 select new AppliancesLocationData
                                 {
                                     LocationID = location.LOCATIONID,
                                     Location = location.LOCATION
                                 });

            List<AppliancesLocationData> appliancesTypeList = new List<AppliancesLocationData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region Update Heating Attribute Info
        public bool updateHeatingAttributeInfo(AttributeGeneralInfo attrInfo, string itemName, string parameterName, object parameterValue, int? updatedBy)
        {

            bool result = false;
            var itemParams = (from ip in context.PA_ITEM_PARAMETER
                              join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                              join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                              where p.ParameterName == parameterName && i.ItemName == itemName
                              select new { itemParamId = ip.ItemParamID, controlType = p.ControlType, heatingFuel = ip.ParameterValueId });

            // Currently multiple heating types exists against the property but in future when scheme/block will
            // contains multiple heating types same filter will be applied to scheme/block as well. 
            if (attrInfo.propertyId != null)
            {
                var heatingType = context.PA_HeatingMapping.Where(x => x.HeatingMappingId == attrInfo.heatingId).First();
                itemParams = itemParams.Where(x => x.heatingFuel == heatingType.HeatingType);
            }


            if (itemParams.Count() > 0)
            {

                var itemParam = itemParams.First();

                #region "Get Attribute Value Detail"

                int? valueId = null;
                bool? isCheckBoxSelected = null;

                string paramValue = null;
                if (itemParam.controlType.Equals(ApplicationConstants.DropdownParameterKey) && parameterValue != null)
                {
                    valueId = (int?)parameterValue;
                    paramValue = context.PA_PARAMETER_VALUE.Where(ppv => ppv.ValueID == valueId).FirstOrDefault().ValueDetail;
                }
                else if (itemParam.controlType.Equals(ApplicationConstants.TextBoxParameterKey) && parameterValue != null)
                {
                    paramValue = (string)parameterValue;
                }
                else if (itemParam.controlType.Equals(ApplicationConstants.CheckboxesParameterKey) && parameterValue != null)
                {
                    valueId = getParameterValueIdValue(attrInfo, itemName, parameterName);
                    isCheckBoxSelected = parameterValue.ToString().ToLower().Equals("true") ? true : false;
                }

                #endregion

                #region "Update PA_PROPERTY_ATTRIBUTES"

                var propertyAttribute = context.PA_PROPERTY_ATTRIBUTES
                                               .Where(patt => patt.HeatingMappingId == attrInfo.heatingId && patt.ITEMPARAMID == itemParam.itemParamId);

                if (propertyAttribute.Count() > 0)
                {
                    PA_PROPERTY_ATTRIBUTES attribute = propertyAttribute.First();
                    attribute.PARAMETERVALUE = paramValue;
                    attribute.VALUEID = valueId;
                    attribute.IsCheckBoxSelected = isCheckBoxSelected;
                    attribute.UPDATEDON = DateTime.Now;
                    attribute.UPDATEDBY = updatedBy;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }
                else
                {
                    PA_PROPERTY_ATTRIBUTES attributes = new PA_PROPERTY_ATTRIBUTES();
                    attributes.PROPERTYID = attrInfo.propertyId;
                    attributes.SchemeId = attrInfo.schemeId;
                    attributes.BlockId = attrInfo.blockId;
                    attributes.HeatingMappingId = attrInfo.heatingId;
                    attributes.ITEMPARAMID = itemParam.itemParamId;
                    attributes.PARAMETERVALUE = paramValue;
                    attributes.VALUEID = valueId;
                    attributes.IsCheckBoxSelected = isCheckBoxSelected;
                    attributes.UPDATEDON = DateTime.Now;
                    attributes.UPDATEDBY = updatedBy;
                    context.AddToPA_PROPERTY_ATTRIBUTES(attributes);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }

                #endregion

            }
            return result;
        }
        #endregion

        #region Save a Boiler Info
        public void saveBoiler(BoilerData boiler, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            string itemName = (boiler.propertyId != null) ? ApplicationConstants.HeatingItemName : ApplicationConstants.BoilerRoomItemName;
            string boilerTypeParamName = (boiler.propertyId != null) ? ApplicationConstants.HeatingTypeParameterKey : ApplicationConstants.BoilerTypeParameterKey;

            AttributeGeneralInfo genInfo = new AttributeGeneralInfo();
            genInfo.heatingId = boiler.heatingId;
            genInfo.propertyId = boiler.propertyId;
            genInfo.schemeId = boiler.schemeId;
            genInfo.blockId = boiler.blockId;

            updateHeatingAttributeInfo(genInfo, itemName, boilerTypeParamName, boiler.boilerTypeId, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.HeatingManufacturerParameterKey, boiler.manufacturerId, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.HeatingModelParameterKey, boiler.model, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.SerialNoParameterKey, boiler.serialNumber, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.GcNumberParameterKey, boiler.gcNumber, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.BoilerLocationParameterKey, boiler.location, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.LandlordApplianceParameterKey, boiler.isLandlordAppliance, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.FlueTypeParameterKey, boiler.flueTypeId, updatedBy);
            //updateHeatingBoilerInfo(boilerData.propertyId, heatingItemName, ApplicationConstants.LastReplacedParameterKey, boilerData.lastReplaced, updatedBy);
            //updateHeatingBoilerInfo(boilerData.propertyId, heatingItemName, ApplicationConstants.ReplacementDueParameterKey, boilerData.replacementDue, updatedBy);
            //updateHeatingBoilerInfo(boilerData.propertyId, heatingItemName, ApplicationConstants.GasketSetReplacementParameterKey, boilerData.gasketReplacementDate, updatedBy);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save Attribute Date Fields
        /// <summary>
        /// Save Attribute Date Fields
        /// </summary>
        /// <param name="survData"></param>
        /// <param name="survFieldsData"></param>
        /// <param name="survItemsFieldsData"></param>

        private void saveAttributeDateFields(AttributeGeneralInfo attrInfo, string itemName, string paramName, DateTime? dateAttributeValue, int? updatedBy)
        {
            // Note : Currently this method Update/Insert Non-Replacement(Non-Component) date fields e.g "Original Install Date", Electric Meter Reading Date 
            // If there is need in the future to update Replacement and Due Dates then this method need to be modified

            PA_PROPERTY_ITEM_DATES paItemDates = new PA_PROPERTY_ITEM_DATES();
            int paramId = context.PA_PARAMETER.Where(x => x.ParameterName == paramName).Select(y => y.ParameterID).First();
            int itemId = context.PA_ITEM.Where(x => x.ItemName == itemName).Select(y => y.ItemID).First();

            IQueryable<PA_PROPERTY_ITEM_DATES> paItemDateResult;
            paItemDateResult = context.PA_PROPERTY_ITEM_DATES
                                          .Where(app => app.ItemId == itemId
                                                        && app.ParameterId == paramId
                                                        && ((attrInfo.heatingId != null && attrInfo.heatingId == app.HeatingMappingId)
                                                            || (attrInfo.heatingId == null && app.HeatingMappingId == null))
                                                        );


            if (attrInfo.propertyId != null)
            {
                paItemDateResult = paItemDateResult.Where(x => x.PROPERTYID == attrInfo.propertyId).OrderBy(app => app.SID);
            }
            else if (attrInfo.schemeId != null && attrInfo.schemeId > 0)
            {
                paItemDateResult = paItemDateResult.Where(x => x.SchemeId == attrInfo.schemeId).OrderBy(app => app.SID);
            }
            else if (attrInfo.blockId != null && attrInfo.blockId > 0)
            {
                paItemDateResult = paItemDateResult.Where(x => x.BlockId == attrInfo.blockId).OrderBy(app => app.SID);
            }

            if (paItemDateResult.Count() > 0)
            {
                paItemDates = paItemDateResult.First();
            }

            paItemDates.DueDate = dateAttributeValue;
            paItemDates.UPDATEDBY = updatedBy;
            paItemDates.UPDATEDON = DateTime.Now;

            if (paItemDates.HeatingMappingId == null && attrInfo.heatingId != null)
            {
                paItemDates.HeatingMappingId = attrInfo.heatingId;
            }


            if (paItemDateResult.Count() <= 0)
            {
                paItemDates.PROPERTYID = attrInfo.propertyId;
                paItemDates.SchemeId = attrInfo.schemeId;
                paItemDates.BlockId = attrInfo.blockId;
                paItemDates.ItemId = itemId;
                paItemDates.ParameterId = paramId;

                context.AddToPA_PROPERTY_ITEM_DATES(paItemDates);
            }
            context.SaveChanges();
            context.Detach(paItemDates);

        }
        #endregion

        #region Save a Alternative Heating Info
        public void saveAlternativeHeating(AlternativeHeatingData heating, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            string itemName = (heating.propertyId != null) ? ApplicationConstants.HeatingItemName : ApplicationConstants.BoilerRoomItemName;

            AttributeGeneralInfo genInfo = new AttributeGeneralInfo();
            genInfo.heatingId = heating.heatingId;
            genInfo.propertyId = heating.propertyId;
            genInfo.schemeId = heating.schemeId;
            genInfo.blockId = heating.blockId;

            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.HeatingManufacturerParameterKey, heating.manufacturerId, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.HeatingModelParameterKey, heating.model, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.SerialNoParameterKey, heating.serialNumber, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.BoilerLocationParameterKey, heating.location, updatedBy);
            saveAttributeDateFields(genInfo, itemName, ApplicationConstants.OriginalInstallDateParameterKey, heating.installedDate, updatedBy);

            if (heating.heatingFuel.Equals(ApplicationConstants.SolarParameterValue))
            {
                updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.SolarTypeParameterKey, heating.solarTypeId, updatedBy);
            }

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save a Oil Heating Info
        public void saveOilHeating(OilHeatingData heating, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            string itemName = (heating.propertyId != null) ? ApplicationConstants.HeatingItemName : ApplicationConstants.BoilerRoomItemName;

            AttributeGeneralInfo genInfo = new AttributeGeneralInfo();
            genInfo.heatingId = heating.heatingId;
            genInfo.propertyId = heating.propertyId;
            genInfo.schemeId = heating.schemeId;
            genInfo.blockId = heating.blockId;

            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.ApplianceMakeParameterKey, heating.applianceMake, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.ApplianceModelParameterKey, heating.applianceModel, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.ApplianceSerialNumberParameterKey, heating.applianceSerialNumber, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.BoilerLocationParameterKey, heating.applianceLocation, updatedBy);            
            saveAttributeDateFields(genInfo, itemName, ApplicationConstants.OriginalInstallDateParameterKey, heating.originalInstallDate, updatedBy);

            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.BurnerMakeParameterKey, heating.burnerMake, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.BurnerModelParameterKey, heating.burnerModel, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.BurnerTypeParameterKey, heating.burnerTypeId, updatedBy);

            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.TankTypeParameterKey, heating.tankTypeId, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.FlueTypeParameterKey, heating.flueTypeId, updatedBy);
            updateHeatingAttributeInfo(genInfo, itemName, ApplicationConstants.FuelTypeParameterKey, heating.fuelTypeId, updatedBy);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save Air Source Inspection Info
        public void saveAirSourceInspection(AlternativeHeatingData heating, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            AirSourceInspection insp = heating.airsourceInspection;

            P_AirSource_Inspection inspDb = new P_AirSource_Inspection();
            inspDb.CheckWaterSupplyTurnedOff = insp.checkWaterSupplyTurnedOff;
            inspDb.TurnedOffDetail = insp.turnedOffDetail;
            inspDb.CheckWaterSupplyTurnedOn = insp.checkWaterSupplyTurnedOn;
            inspDb.TurnedOnDetail = insp.turnedOnDetail;
            inspDb.CheckValves = insp.checkValves;
            inspDb.ValveDetail = insp.valveDetail;
            inspDb.CheckSupplementaryBonding = insp.checkSupplementaryBonding;
            inspDb.SupplementaryBondingDetail = insp.supplementaryBondingDetail;
            inspDb.CheckFuse = insp.checkFuse;
            inspDb.FuseDetail = insp.fuseDetail;
            inspDb.CheckThermostat = insp.checkThermostat;
            inspDb.ThermostatDetail = insp.thermostatDetail;
            inspDb.CheckOilLeak = insp.checkOilLeak;
            inspDb.OilLeakDetail = insp.oilLeakDetail;
            inspDb.CheckWaterPipework = insp.checkWaterPipework;
            inspDb.PipeworkDetail = insp.pipeworkDetail;
            inspDb.CheckElectricConnection = insp.checkElectricConnection;
            inspDb.ConnectionDetail = insp.connectionDetail;
            inspDb.HeatingMappingId = insp.heatingId;
            inspDb.JournalId = insp.journalId;
            inspDb.IsInspected = insp.isInspected;
            inspDb.InspectionDate = insp.inspectionDate;
            inspDb.InspectedBy = updatedBy;

            context.AddToP_AirSource_Inspection(inspDb);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save Oil Firing Service & checks
        public void saveOilFiringServiceAndChecks(OilHeatingData heating, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            OilFiringAndService insp = heating.oilFiringAndService;

            P_Oil_Inspection inspDb = new P_Oil_Inspection();

            inspDb.OilStorage = insp.oilStorage;
            inspDb.OilStorageDetail = insp.oilStorageDetail;
            inspDb.OilSupplySystem = insp.oilSupplySystem;
            inspDb.OilSupplySystemDetail = insp.oilSupplySystemDetail;
            inspDb.AirSupply = insp.airSupply;
            inspDb.AirSupplyDetail = insp.airSupplyDetail;
            inspDb.ChimneyFlue = insp.chimneyFlue;
            inspDb.ChimneyFlueDetail = insp.chimneyFlueDetail;
            inspDb.ElectricalSafety = insp.electricalSafety;
            inspDb.ElectricalSafetyDetail = insp.electricalSafetyDetail;
            inspDb.HeatExchanger = insp.heatExchanger;
            inspDb.HeatExchangerDetail = insp.heatExchangerDetail;
            inspDb.CombustionChamber = insp.combustionChamber;
            inspDb.CombustionChamberDetail = insp.combustionChamberDetail;
            inspDb.PressureJet = insp.pressureJet;
            inspDb.PressureJetDetail = insp.pressureJetDetail;
            inspDb.VaporisingBurner = insp.vaporisingBurner;
            inspDb.VaporisingBurnerDetail = insp.vaporisingBurnerDetail;

            inspDb.WallflameBurner = insp.wallflameBurner;
            inspDb.WallflameBurnerDetail = insp.wallflameBurnerDetail;
            inspDb.ApplianceSafety = insp.applianceSafety;
            inspDb.ApplianceSafetyDetail = insp.applianceSafetyDetail;
            inspDb.ControlCheck = insp.controlCheck;
            inspDb.ControlCheckDetail = insp.controlCheckDetail;
            inspDb.HotWaterType = insp.hotWaterType;
            inspDb.HotWaterTypeDetail = insp.hotWaterTypeDetail;
            inspDb.WarmAirType = insp.warmAirType;
            inspDb.WarmAirTypeDetail = insp.warmAirTypeDetail;

            inspDb.HeatingMappingId = insp.heatingId;
            inspDb.JournalId = insp.journalId;
            inspDb.IsInspected = insp.isInspected;
            inspDb.InspectionDate = insp.inspectionDate;
            inspDb.InspectedBy = updatedBy;

            context.AddToP_Oil_Inspection(inspDb);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save MVHR Inspection Info
        public void saveMVHRInspection(AlternativeHeatingData heating, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            MvhrInspection insp = heating.mvhrInspection;

            P_MVHR_Inspection inspDb = new P_MVHR_Inspection();
            inspDb.CheckAirFlow = insp.checkAirFlow;
            inspDb.AirFlowDetail = insp.airFlowDetail;
            inspDb.DuctingInspection = insp.ductingInspection;
            inspDb.DuctingDetail = insp.ductingDetail;
            inspDb.CheckFilters = insp.checkFilters;
            inspDb.FilterDetail = insp.filterDetail;
            inspDb.HeatingMappingId = insp.heatingId;
            inspDb.JournalId = insp.journalId;
            inspDb.IsInspected = insp.isInspected;
            inspDb.InspectionDate = insp.inspectionDate;
            inspDb.InspectedBy = updatedBy;

            context.AddToP_MVHR_Inspection(inspDb);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save Solar Inspection Info
        public void saveSolarInspection(AlternativeHeatingData heating, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            SolarInspection insp = heating.solarInspection;

            P_Solar_Inspection inspDb = new P_Solar_Inspection();
            inspDb.VesselProtectionInstalled = insp.vesselProtectionInstalled;
            inspDb.VesselCapacity = insp.vesselCapacity;
            inspDb.MinPressure = insp.minPressure;
            inspDb.FreezingTemp = insp.freezingTemp;
            inspDb.SystemPressure = insp.systemPressure;
            inspDb.BackPressure = insp.backPressure;
            inspDb.DeltaOn = insp.deltaOn;
            inspDb.DeltaOff = insp.deltaOff;
            inspDb.MaxTemperature = insp.maxTemperature;
            inspDb.CalculationRate = insp.calculationRate;
            inspDb.ThermostatTemperature = insp.thermostatTemperature;
            inspDb.AntiScaldingControl = insp.antiScaldingControl;
            inspDb.AntiScaldingControlDetail = insp.antiScaldingControlDetail;
            inspDb.CheckDirection = insp.checkDirection;
            inspDb.DirectionDetail = insp.directionDetail;
            inspDb.CheckElectricalControl = insp.checkElectricalControl;
            inspDb.ElectricalControlDetail = insp.electricalControlDetail;            
            inspDb.HeatingMappingId = insp.heatingId;
            inspDb.JournalId = insp.journalId;
            inspDb.IsInspected = insp.isInspected;
            inspDb.InspectionDate = insp.inspectionDate;
            inspDb.InspectedBy = updatedBy;

            context.AddToP_Solar_Inspection(inspDb);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            if (currentContext == null) context.AcceptAllChanges();
        }
        #endregion

        #region Save a new Appliance
        /// <summary>
        /// This function saves a new appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveAppliance(ApplianceData appData, PropertySurvey_Entities currentContext = null)
        {
            int itemId = 0;
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            var item = from pItem in context.PA_ITEM
                       where pItem.ItemName == "Appliances"
                       select pItem.ItemID;
            if (item.Count() > 0)
            {
                itemId = int.Parse(item.FirstOrDefault().ToString());
            }
            GS_PROPERTY_APPLIANCE appliance = new GS_PROPERTY_APPLIANCE();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var appl = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYAPPLIANCEID == appData.ApplianceID);
            if (appl.Count() > 0)
            {
                //fetch the already existed record
                appliance = appl.First();
            }
            if (appData.propertyId != null)
            {
                appliance.PROPERTYID = appData.propertyId;
            }
            appliance.SchemeID = appData.schemeId;
            appliance.BlockID = appData.blockId;
            appliance.DATEINSTALLED = appData.InstalledDate;
            appliance.FLUETYPE = appData.FluType;

            if (appData.ApplianceLocation != null)
            {
                appliance.LOCATIONID = appData.ApplianceLocation.LocationID;
            }

            if (appData.ApplianceModel != null)
            {
                appliance.MODELID = appData.ApplianceModel.ApplianceModelID;
                appliance.MODEL = appData.ApplianceModel.ApplianceModel;
            }

            if (appData.ApplianceManufacturer != null)
            {
                appliance.MANUFACTURERID = appData.ApplianceManufacturer.ManufacturerID;
            }

            if (appData.ApplianceType != null)
            {
                appliance.APPLIANCETYPEID = appData.ApplianceType.ApplianceTypeID;
            }

            if (appl.Count() > 0)
            {
                if (appliance.IsActive != false)
                {
                    appliance.IsActive = appData.isActive;
                }
            }
            else
            {
                appliance.IsActive = appData.isActive;
            }

            appliance.ISLANDLORDAPPLIANCE = appData.isLandlordAppliance;
            appliance.REPLACEMENTDATE = appData.ReplacementDate;
            appliance.SerialNumber = appData.SerialNumber;
            appliance.GasCouncilNumber = appData.GCNumber;
            appliance.ItemId = itemId;
            if (!(appl.Count() > 0))
            {
                context.AddToGS_PROPERTY_APPLIANCE(appliance);
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            success = true;
            //}
            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return appliance.PROPERTYAPPLIANCEID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region Update a existing Appliance
        /// <summary>
        /// This function updates a existing appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateAppliance(ApplianceData appData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var appl = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYAPPLIANCEID == appData.ApplianceID);
                if (appl.Count() > 0)
                {
                    GS_PROPERTY_APPLIANCE appliance = appl.First();

                    appliance.PROPERTYID = appData.propertyId;
                    appliance.DATEINSTALLED = appData.InstalledDate;
                    appliance.FLUETYPE = appData.FluType;

                    if (appData.ApplianceLocation != null)
                    {
                        appliance.LOCATIONID = appData.ApplianceLocation.LocationID;
                    }

                    if (appData.ApplianceModel != null)
                    {
                        appliance.MODELID = appData.ApplianceModel.ApplianceModelID;
                    }

                    if (appData.ApplianceManufacturer != null)
                    {
                        appliance.MANUFACTURERID = appData.ApplianceManufacturer.ManufacturerID;
                    }

                    if (appData.ApplianceType != null)
                    {
                        appliance.APPLIANCETYPEID = appData.ApplianceType.ApplianceTypeID;
                    }

                    if (appliance.IsActive != false)
                    {
                        appliance.IsActive = appData.isActive;
                    }

                    //Change#27 - Behroz - 20/12/2012 - Start
                    appliance.MODEL = appData.Model;
                    appliance.ISLANDLORDAPPLIANCE = appData.isLandlordAppliance;
                    appliance.REPLACEMENTDATE = appData.ReplacementDate;
                    //Change#27 - Behroz - 20/12/2012 - End
                    // Code added  15/05/2013 - START
                    appliance.SerialNumber = appData.SerialNumber;
                    appliance.GasCouncilNumber = appData.GCNumber;
                    // Code added  15/05/2013 - END
                    //if (appData.ApplianceOrgID == 0)
                    //{
                    //    appliance.ORGID = null;
                    //}
                    //else
                    //{
                    //    appliance.ORGID = appData.ApplianceOrgID;
                    //}

                    context.SaveChanges();
                    trans.Complete();
                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region save new Appliances Locations
        /// <summary>
        /// This function save new Appliances Location
        /// </summary>
        /// <returns>true for success</returns>
        public int saveApplianceLocation(AppliancesLocationData location, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            GS_LOCATION app_location = new GS_LOCATION();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_LOCATION.Where(app => app.LOCATIONID == location.LocationID);
            if (apptRecord.Count() <= 0)
            {
                app_location.LOCATION = location.Location;
                context.AddToGS_LOCATION(app_location);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_location.LOCATIONID;
            }
            else
            {
                return apptRecord.First().LOCATIONID;
            }
            //}
        }
        #endregion

        #region save new Appliances Type
        /// <summary>
        /// This function save new Appliances Type
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceType(ApplianceTypeData type, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            bool success = false;
            GS_APPLIANCE_TYPE app_type = new GS_APPLIANCE_TYPE();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_APPLIANCE_TYPE.Where(app => app.APPLIANCETYPEID == type.ApplianceTypeID);
            if (apptRecord.Count() <= 0)
            {
                app_type.APPLIANCETYPE = type.ApplianceType;
                context.AddToGS_APPLIANCE_TYPE(app_type);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_type.APPLIANCETYPEID;
            }
            else
            {
                return apptRecord.First().APPLIANCETYPEID;
            }
            //}
        }
        #endregion

        #region save new Appliances Model
        /// <summary>
        /// This function save new Appliances Model
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceModel(ApplianceModelData model, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            bool success = false;
            GS_ApplianceModel app_model = new GS_ApplianceModel();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_ApplianceModel.Where(app => app.ModelID == model.ApplianceModelID);
            if (apptRecord.Count() <= 0)
            {
                app_model.Model = model.ApplianceModel;
                context.AddToGS_ApplianceModel(app_model);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_model.ModelID;
            }
            else
            {
                return apptRecord.First().ModelID;
            }
            //}
        }
        #endregion

        #region save new Appliances manufacturer
        /// <summary>
        /// This function save new Appliances manufacturer
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceManufacturer(ManufacturerData manufacture, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;

            GS_MANUFACTURER app_manufacture = new GS_MANUFACTURER();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_MANUFACTURER.Where(app => app.MANUFACTURERID == manufacture.ManufacturerID);
            if (apptRecord.Count() <= 0)
            {
                app_manufacture.MANUFACTURER = manufacture.Manufacturer;
                context.AddToGS_MANUFACTURER(app_manufacture);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_manufacture.MANUFACTURERID;
            }
            else
            {
                return apptRecord.First().MANUFACTURERID;
            }
            //}
        }
        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID)
        {
            var count = (from app in context.GS_ApplianceInspection
                         where app.APPOINTMENTID == appointmentID
                         select app.APPLIANCEINSPECTIONID);
            return count.Count();
        }

        //Change#25 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns number of inspected Appliances for gas
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        //public int getInspectedAppliances(string propertyID)
        //{
        //    var count = (from app in context.GS_PROPERTY_APPLIANCE
        //                 where app.PROPERTYID == propertyID
        //                 select app.PROPERTYAPPLIANCEID);

        //    return count.Count();
        //}
        public int getInspectedAppliancesGas(int? journalId, int applianceId)
        {
            var count = (from app in context.P_APPLIANCE_INSPECTION
                         where app.Journalid == journalId && app.PROPERTYAPPLIANCEID == applianceId
                         select app.PROPERTYAPPLIANCEID);

            return count.Count();
        }
        //Change#25 - Behroz - 20/12/2012 - End

        #endregion

        #region get number of inspected Boilers
        public int getInspectedBoilers(int? journalId, int? heatingId)
        {
            var count = (from app in context.P_BOILER_INSPECTION
                         where app.JOURNALID == journalId && app.HeatingMappingId == heatingId
                         select app.HeatingMappingId);
            return count.Count();
        }
        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID)
        {
            var appliances = (from AppIns in context.GS_ApplianceInspection
                              where AppIns.APPLIANCEID == ApplianceID && AppIns.APPOINTMENTID == appointmentID
                              select new ApplianceInspectionData
                              {
                                  adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.APPLIANCEID,
                                  inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                  applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                  applianceServiced = AppIns.APPLIANCESERVICED,
                                  APPOINTMENTID = AppIns.APPOINTMENTID,
                                  combustionReading = AppIns.COMBUSTIONREADING,
                                  fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                  flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                  inspectionDate = AppIns.INSPECTIONDATE,
                                  operatingPressure = AppIns.OPERATINGPRESSURE,
                                  //isInspected = AppIns.ISINSPECTED,
                                  safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                  satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                  smokePellet = AppIns.SMOKEPELLET,
                                  spillageTest = AppIns.SPILLAGETEST
                              });

            ApplianceInspectionData appliancesList = new ApplianceInspectionData();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.First();
            }

            return appliancesList;
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        //public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, string propertyID)
        //{
        //    var appliance = (from propApp in context.GS_PROPERTY_APPLIANCE
        //                     join AppIns in context.P_APPLIANCE_INSPECTION on
        //                     propApp.PROPERTYAPPLIANCEID equals AppIns.PROPERTYAPPLIANCEID
        //                     where propApp.PROPERTYAPPLIANCEID == ApplianceID && propApp.PROPERTYID == propertyID
        //                     select new ApplianceInspectionData {
        //                         ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
        //                         APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
        //                         APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
        //                         APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
        //                         APPLIANCESERVICED = AppIns.APPLIANCESERVICED,                                 
        //                         COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
        //                         FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
        //                         FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
        //                         INSPECTIONDATE = AppIns.INSPECTIONDATE,
        //                         OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
        //                         ISINSPECTED = AppIns.ISINSPECTED,
        //                         SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
        //                         SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
        //                         SMOKEPELLET = AppIns.SMOKEPELLET,
        //                         SPILLAGETEST = AppIns.SPILLAGETEST,
        //                         INSPECTEDBY = AppIns.INSPECTEDBY.Value                                 
        //                     });            

        //    ApplianceInspectionData appliancesList = new ApplianceInspectionData();
        //    if (appliance.Count() > 0)
        //    {
        //        appliancesList = appliance.First();
        //    }

        //    return appliancesList;
        //}
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId)
        {
            var appliance = (from AppIns in context.P_APPLIANCE_INSPECTION
                             where AppIns.PROPERTYAPPLIANCEID == ApplianceID && AppIns.Journalid == journalId
                             select new ApplianceInspectionData
                             {
                                 adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                 APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
                                 inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                 applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                 applianceServiced = AppIns.APPLIANCESERVICED,
                                 combustionReading = AppIns.COMBUSTIONREADING,
                                 fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                 flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                 inspectionDate = AppIns.INSPECTIONDATE,
                                 operatingPressure = AppIns.OPERATINGPRESSURE,
                                 operatingPressureUnit = AppIns.OperatingPressureUnit,
                                 isInspected = AppIns.ISINSPECTED,
                                 safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                 satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                 smokePellet = AppIns.SMOKEPELLET,
                                 spillageTest = AppIns.SPILLAGETEST,
                                 inspectedBy = AppIns.INSPECTEDBY.Value
                             });

            ApplianceInspectionData appliancesList = new ApplianceInspectionData();
            if (appliance.Count() > 0)
            {
                appliancesList = appliance.First();
            }

            return appliancesList;
        }
        //Change#26 - Behroz - 20/12/2012 - End

        #endregion

        #region get Boiler Inspection by JournalId
        public BoilerInspectionData getBoilerInspectionByJournalId(int? heatingId, int? journalId)
        {
            var boilerInspection = (from boilerIns in context.P_BOILER_INSPECTION
                                    where boilerIns.JOURNALID == journalId && boilerIns.HeatingMappingId == heatingId
                                    select new BoilerInspectionData
                                    {
                                        adequateVentilation = boilerIns.ADEQUATEVENTILATION,
                                        boilerTypeId = (int?)boilerIns.BOILERTYPEID,
                                        heatingId = boilerIns.HeatingMappingId,
                                        inspectionId = boilerIns.BOILERINSPECTIONID,
                                        boilerSafeToUse = boilerIns.BOILERSAFETOUSE,
                                        boilerServiced = boilerIns.BOILERSERVICED,
                                        combustionReading = boilerIns.COMBUSTIONREADING,
                                        fluePerformanceChecks = boilerIns.FLUEPERFORMANCECHECKS,
                                        flueVisualCondition = boilerIns.FLUEVISUALCONDITION,
                                        inspectionDate = boilerIns.INSPECTIONDATE,
                                        operatingPressure = boilerIns.OPERATINGPRESSURE,
                                        operatingPressureUnit = boilerIns.OPERATINGPRESSUREUNIT,
                                        isInspected = boilerIns.ISINSPECTED,
                                        safetyDeviceOperational = boilerIns.SAFETYDEVICEOPERATIONAL,
                                        satisfactoryTermination = boilerIns.SATISFACTORYTERMINATION,
                                        smokePellet = boilerIns.SMOKEPELLET,
                                        spillageTest = boilerIns.SPILLAGETEST,
                                        inspectedBy = boilerIns.INSPECTEDBY.Value
                                    });
            BoilerInspectionData inspectionData = new BoilerInspectionData();
            if (boilerInspection.Count() > 0)
            {
                inspectionData = boilerInspection.First();
            }
            else
            {
                inspectionData = null;
            }

            return inspectionData;
        }
        #endregion

        #region Get Oil Inspection by JournalId
        public OilFiringAndService getOilInspectionByJournalId(int? heatingId, int? journalId)
        {
            var inspectionQuery = (from insp in context.P_Oil_Inspection
                                   where insp.JournalId == journalId && insp.HeatingMappingId == heatingId
                                   select new OilFiringAndService
                                   {
                                       heatingId = insp.HeatingMappingId,
                                       inspectionId = insp.InspectionId,
                                       inspectionDate = insp.InspectionDate,
                                       isInspected = insp.IsInspected,
                                       inspectedBy = insp.InspectedBy.Value,
                                       oilStorage = insp.OilStorage,
                                       oilStorageDetail = insp.OilStorageDetail,
                                       oilSupplySystem = insp.OilSupplySystem,
                                       oilSupplySystemDetail = insp.OilSupplySystemDetail,
                                       airSupply = insp.AirSupply,
                                       airSupplyDetail = insp.AirSupplyDetail,
                                       chimneyFlue = insp.ChimneyFlue,
                                       chimneyFlueDetail = insp.ChimneyFlueDetail,
                                       electricalSafety = insp.ElectricalSafety,
                                       electricalSafetyDetail = insp.ElectricalSafetyDetail,
                                       heatExchanger = insp.HeatExchanger,
                                       heatExchangerDetail = insp.HeatExchangerDetail,
                                       combustionChamber = insp.CombustionChamber,
                                       combustionChamberDetail = insp.CombustionChamberDetail,
                                       pressureJet = insp.PressureJet,
                                       pressureJetDetail = insp.PressureJetDetail,
                                       vaporisingBurner = insp.VaporisingBurner,
                                       vaporisingBurnerDetail = insp.VaporisingBurnerDetail,
                                       wallflameBurner = insp.WallflameBurner,
                                       wallflameBurnerDetail = insp.WallflameBurnerDetail,
                                       applianceSafety = insp.ApplianceSafety,
                                       applianceSafetyDetail = insp.ApplianceSafetyDetail,
                                       controlCheck = insp.ControlCheck,
                                       controlCheckDetail = insp.ControlCheckDetail,
                                       hotWaterType = insp.HotWaterType,
                                       hotWaterTypeDetail = insp.HotWaterTypeDetail,
                                       warmAirType = insp.WarmAirType,
                                       warmAirTypeDetail = insp.WarmAirTypeDetail
                                   });
            OilFiringAndService inspectionData = new OilFiringAndService();
            if (inspectionQuery.Count() > 0)
            {
                inspectionData = inspectionQuery.First();
            }
            else
            {
                inspectionData = null;
            }

            return inspectionData;
        }
        #endregion


        #region Get Air Source Inspection by JournalId
        public AirSourceInspection getAirSourceInspectionByJournalId(int? heatingId, int? journalId)
        {
            var inspectionQuery = (from insp in context.P_AirSource_Inspection
                                   where insp.JournalId == journalId && insp.HeatingMappingId == heatingId
                                   select new AirSourceInspection
                                   {
                                       heatingId = insp.HeatingMappingId,
                                       inspectionId = insp.InspectionId,
                                       inspectionDate = insp.InspectionDate,
                                       isInspected = insp.IsInspected,
                                       inspectedBy = insp.InspectedBy.Value,
                                       checkWaterSupplyTurnedOff = insp.CheckWaterSupplyTurnedOff,
                                       turnedOffDetail = insp.TurnedOffDetail,
                                       checkWaterSupplyTurnedOn = insp.CheckWaterSupplyTurnedOn,
                                       turnedOnDetail = insp.TurnedOnDetail,
                                       checkValves = insp.CheckValves,
                                       valveDetail = insp.ValveDetail,
                                       checkSupplementaryBonding = insp.CheckSupplementaryBonding,
                                       supplementaryBondingDetail = insp.SupplementaryBondingDetail,
                                       checkFuse = insp.CheckFuse,
                                       fuseDetail = insp.FuseDetail,
                                       checkThermostat = insp.CheckThermostat,
                                       thermostatDetail = insp.ThermostatDetail,
                                       checkOilLeak = insp.CheckOilLeak,
                                       oilLeakDetail = insp.OilLeakDetail,
                                       checkWaterPipework = insp.CheckWaterPipework,
                                       pipeworkDetail = insp.PipeworkDetail,
                                       checkElectricConnection = insp.CheckElectricConnection,
                                       connectionDetail = insp.ConnectionDetail
                                   });
            AirSourceInspection inspectionData = new AirSourceInspection();
            if (inspectionQuery.Count() > 0)
            {
                inspectionData = inspectionQuery.First();
            }
            else
            {
                inspectionData = null;
            }

            return inspectionData;
        }
        #endregion

        #region Get MVHR Inspection by JournalId
        public MvhrInspection getMvhrInspectionByJournalId(int? heatingId, int? journalId)
        {
            var inspectionQuery = (from insp in context.P_MVHR_Inspection
                                   where insp.JournalId == journalId && insp.HeatingMappingId == heatingId
                                   select new MvhrInspection
                                   {
                                       heatingId = insp.HeatingMappingId,
                                       inspectionId = insp.InspectionId,
                                       inspectionDate = insp.InspectionDate,
                                       isInspected = insp.IsInspected,
                                       inspectedBy = insp.InspectedBy.Value,
                                       checkAirFlow = insp.CheckAirFlow,
                                       airFlowDetail = insp.AirFlowDetail,
                                       ductingInspection = insp.DuctingInspection,
                                       ductingDetail = insp.DuctingDetail,
                                       checkFilters = insp.CheckFilters,
                                       filterDetail = insp.FilterDetail
                                   });
            MvhrInspection inspectionData = new MvhrInspection();
            if (inspectionQuery.Count() > 0)
            {
                inspectionData = inspectionQuery.First();
            }
            else
            {
                inspectionData = null;
            }

            return inspectionData;
        }
        #endregion

        #region Get Solar Inspection by JournalId
        public SolarInspection getSolarInspectionByJournalId(int? heatingId, int? journalId)
        {
            var inspectionQuery = (from insp in context.P_Solar_Inspection
                                   where insp.JournalId == journalId && insp.HeatingMappingId == heatingId
                                   select new SolarInspection
                                   {
                                       heatingId = insp.HeatingMappingId,
                                       inspectionId = insp.InspectionId,
                                       inspectionDate = insp.InspectionDate,
                                       isInspected = insp.IsInspected,
                                       inspectedBy = insp.InspectedBy.Value,
                                       vesselProtectionInstalled = insp.VesselProtectionInstalled,
                                       vesselCapacity = insp.VesselCapacity,
                                       minPressure = insp.MinPressure,
                                       freezingTemp = insp.FreezingTemp,
                                       systemPressure = insp.SystemPressure,
                                       backPressure = insp.BackPressure,
                                       deltaOff = insp.DeltaOff,
                                       deltaOn = insp.DeltaOn,
                                       maxTemperature = insp.MaxTemperature,
                                       calculationRate = insp.CalculationRate,
                                       thermostatTemperature = insp.ThermostatTemperature,
                                       antiScaldingControl = insp.AntiScaldingControl,
                                       antiScaldingControlDetail = insp.AntiScaldingControlDetail,
                                       checkDirection = insp.CheckDirection,
                                       directionDetail = insp.DirectionDetail,
                                       checkElectricalControl = insp.CheckElectricalControl,
                                       electricalControlDetail = insp.ElectricalControlDetail
                                   });
            SolarInspection inspectionData = new SolarInspection();
            if (inspectionQuery.Count() > 0)
            {
                inspectionData = inspectionQuery.First();
            }
            else
            {
                inspectionData = null;
            }

            return inspectionData;
        }
        #endregion

        #region get ApplianceInspection details by Appointment Id
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID)
        {
            var appliances = (from AppIns in context.GS_ApplianceInspection
                              where AppIns.APPOINTMENTID == appointmentID
                              select new ApplianceInspectionData
                              {
                                  adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.APPLIANCEID,
                                  inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                  applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                  applianceServiced = AppIns.APPLIANCESERVICED,
                                  APPOINTMENTID = AppIns.APPOINTMENTID,
                                  combustionReading = AppIns.COMBUSTIONREADING,
                                  fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                  flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                  inspectionDate = AppIns.INSPECTIONDATE,
                                  operatingPressure = AppIns.OPERATINGPRESSURE,
                                  isInspected = AppIns.ISINSPECTED,
                                  safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                  satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                  smokePellet = AppIns.SMOKEPELLET,
                                  spillageTest = AppIns.SPILLAGETEST
                              });

            List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
            }

            return appliancesList;
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        ///// <summary>
        ///// This function returns all the ApplianceInspection details
        ///// </summary>
        ///// <returns>List of ApplianceInspection data objects</returns>
        //public List<ApplianceInspectionData> getApplianceInspectionDetails(string propertyID)
        //{
        //    var appliances = (from propApp in context.GS_PROPERTY_APPLIANCE
        //                      join AppIns in context.P_APPLIANCE_INSPECTION on
        //                      propApp.PROPERTYAPPLIANCEID equals AppIns.PROPERTYAPPLIANCEID
        //                      where propApp.PROPERTYID == propertyID
        //                      select new ApplianceInspectionData
        //                      {
        //                          ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
        //                          APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
        //                          APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
        //                          APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
        //                          APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
        //                          COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
        //                          FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
        //                          FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
        //                          INSPECTIONDATE = AppIns.INSPECTIONDATE,
        //                          OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
        //                          ISINSPECTED = AppIns.ISINSPECTED,
        //                          SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
        //                          SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
        //                          SMOKEPELLET = AppIns.SMOKEPELLET,
        //                          SPILLAGETEST = AppIns.SPILLAGETEST,
        //                          INSPECTEDBY = AppIns.INSPECTEDBY.Value  
        //                      });

        //    List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
        //    if (appliances.Count() > 0)
        //    {
        //        appliancesList = appliances.ToList();
        //    }

        //    return appliancesList;
        //}
        //Change#40 - Behroz - 26/12/2012 - End
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId)
        {
            var appliances = (from AppIns in context.P_APPLIANCE_INSPECTION
                              where AppIns.Journalid == journalId
                              select new ApplianceInspectionData
                              {
                                  adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
                                  inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                  applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                  applianceServiced = AppIns.APPLIANCESERVICED,
                                  combustionReading = AppIns.COMBUSTIONREADING,
                                  fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                  flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                  inspectionDate = AppIns.INSPECTIONDATE,
                                  operatingPressure = AppIns.OPERATINGPRESSURE,
                                  isInspected = AppIns.ISINSPECTED,
                                  safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                  satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                  smokePellet = AppIns.SMOKEPELLET,
                                  spillageTest = AppIns.SPILLAGETEST,
                                  inspectedBy = AppIns.INSPECTEDBY.Value,
                                  JOURNALID = AppIns.Journalid
                              });

            List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
            }

            return appliancesList;
        }
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData)
        {
            bool success = false;
            GS_ApplianceInspection appliance = new GS_ApplianceInspection();

            //using (TransactionScope trans = new TransactionScope())
            //{
            appliance.ADEQUATEVENTILATION = appData.adequateVentilation;
            appliance.APPLIANCEID = (int)appData.APPLIANCEID;
            //appliance.APPLIANCEINSPECTIONID = appData.APPLIANCEINSPECTIONID;
            appliance.APPLIANCESAFETOUSE = appData.applianceSafeToUse;
            appliance.APPLIANCESERVICED = appData.applianceServiced;
            appliance.APPOINTMENTID = appData.APPOINTMENTID;
            appliance.COMBUSTIONREADING = appData.combustionReading;
            appliance.FLUEPERFORMANCECHECKS = appData.fluePerformanceChecks;
            appliance.FLUEVISUALCONDITION = appData.flueVisualCondition;
            appliance.INSPECTIONDATE = appData.inspectionDate;
            appliance.OPERATINGPRESSURE = appData.operatingPressure;
            appliance.ISINSPECTED = (bool)appData.isInspected;
            appliance.SAFETYDEVICEOPERATIONAL = appData.safetyDeviceOperational;
            appliance.SATISFACTORYTERMINATION = appData.satisfactoryTermination;
            appliance.SMOKEPELLET = appData.smokePellet;
            appliance.SPILLAGETEST = appData.spillageTest;

            context.AddToGS_ApplianceInspection(appliance);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            success = true;
            //}

            if (success == true)
            {
                return appliance.APPLIANCEINSPECTIONID;
            }
            else
            {
                return 0;
            }
        }


        public int saveApplianceInspectionGas(ApplianceInspectionData appData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            P_APPLIANCE_INSPECTION appliance = new P_APPLIANCE_INSPECTION();

            //var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID);
            var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID && app.Journalid == appData.JOURNALID);

            if (PropertyApplianceExist.Count() > 0)
            {
                appliance = PropertyApplianceExist.First();

                #region "Update"

                //using (TransactionScope trans = new TransactionScope())
                //{
                appliance.ADEQUATEVENTILATION = appData.adequateVentilation;
                appliance.PROPERTYAPPLIANCEID = (int)appData.APPLIANCEID;
                appliance.APPLIANCESAFETOUSE = appData.applianceSafeToUse;
                appliance.APPLIANCESERVICED = appData.applianceServiced;
                appliance.COMBUSTIONREADING = appData.combustionReading;
                appliance.FLUEPERFORMANCECHECKS = appData.fluePerformanceChecks;
                appliance.FLUEVISUALCONDITION = appData.flueVisualCondition;
                appliance.INSPECTIONDATE = appData.inspectionDate;
                appliance.OPERATINGPRESSURE = appData.operatingPressure;
                appliance.ISINSPECTED = (bool)appData.isInspected;
                appliance.SAFETYDEVICEOPERATIONAL = appData.safetyDeviceOperational;
                appliance.SATISFACTORYTERMINATION = appData.satisfactoryTermination;
                appliance.SMOKEPELLET = appData.smokePellet;
                appliance.SPILLAGETEST = appData.spillageTest;
                appliance.INSPECTEDBY = appData.inspectedBy;
                appliance.Journalid = appData.JOURNALID;
                appliance.OperatingPressureUnit = appData.operatingPressureUnit;

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                //}
                success = true;

                if (currentContext == null) context.AcceptAllChanges();

                if (success)
                    return 1;
                else return 0;

                #endregion
            }
            else
            {
                #region "Insert"

                //Insert if the Property Appliance Id does not exist
                using (TransactionScope trans = new TransactionScope())
                {
                    appliance.ADEQUATEVENTILATION = appData.adequateVentilation;
                    appliance.PROPERTYAPPLIANCEID = (int)appData.APPLIANCEID;
                    appliance.APPLIANCESAFETOUSE = appData.applianceSafeToUse;
                    appliance.APPLIANCESERVICED = appData.applianceServiced;
                    appliance.COMBUSTIONREADING = appData.combustionReading;
                    appliance.FLUEPERFORMANCECHECKS = appData.fluePerformanceChecks;
                    appliance.FLUEVISUALCONDITION = appData.flueVisualCondition;
                    appliance.INSPECTIONDATE = appData.inspectionDate;
                    appliance.OPERATINGPRESSURE = appData.operatingPressure;
                    appliance.ISINSPECTED = (bool)appData.isInspected;
                    appliance.SAFETYDEVICEOPERATIONAL = appData.safetyDeviceOperational;
                    appliance.SATISFACTORYTERMINATION = appData.satisfactoryTermination;
                    appliance.SMOKEPELLET = appData.smokePellet;
                    appliance.SPILLAGETEST = appData.spillageTest;
                    appliance.INSPECTEDBY = appData.inspectedBy;
                    appliance.Journalid = appData.JOURNALID;
                    appliance.OperatingPressureUnit = appData.operatingPressureUnit;

                    context.AddToP_APPLIANCE_INSPECTION(appliance);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }

                if (success == true)
                {
                    return appliance.APPLIANCEINSPECTIONID;
                }
                else
                {
                    return 0;
                }

                #endregion
            }
        }
        #endregion

        #region "Get Appliance Count"

        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns the appliance count 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId)
        {
            var appliances = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYID == propertyId);
            return appliances.Count();
        }

        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        #region "Is Appliance Data Valid"
        public bool isApplianceDataValid(ApplianceData appData)
        {
            bool isValid = true;
            if (appData.ApplianceID > 0 && appData.isActive == true)
            {

                if (appData.ApplianceInspection == null)
                {
                    isValid = false;
                }
                else if (appData.ApplianceInspection.isInspected == null)
                {
                    isValid = false;
                }
                else if (appData.ApplianceInspection.isInspected == true)
                {
                    ApplianceInspectionData objIns = appData.ApplianceInspection;
                    if (String.IsNullOrEmpty(objIns.adequateVentilation)
                        || String.IsNullOrEmpty(objIns.applianceSafeToUse)
                        || String.IsNullOrEmpty(objIns.applianceServiced)
                        || String.IsNullOrEmpty(objIns.combustionReading)
                        || String.IsNullOrEmpty(objIns.fluePerformanceChecks)
                        || String.IsNullOrEmpty(objIns.flueVisualCondition)
                        || objIns.inspectionDate == null
                        || String.IsNullOrEmpty(objIns.safetyDeviceOperational)
                        || String.IsNullOrEmpty(objIns.satisfactoryTermination)
                        || String.IsNullOrEmpty(objIns.smokePellet)
                        || String.IsNullOrEmpty(objIns.spillageTest)
                        || String.IsNullOrEmpty(objIns.operatingPressureUnit)
                        )
                    {
                        isValid = false;
                    }

                }
            }

            return isValid;
        }
        #endregion

        #region "Is Boiler Data Valid"
        public bool isBoilerDataValid(BoilerData boilerData)
        {
            bool isValid = true;

            if (boilerData.BoilerInspection == null)
            {
                isValid = false;
            }
            else if (boilerData.BoilerInspection.isInspected == null)
            {
                isValid = false;
            }
            else if (boilerData.BoilerInspection.isInspected == true)
            {
                BoilerInspectionData objIns = boilerData.BoilerInspection;
                if (String.IsNullOrEmpty(objIns.adequateVentilation)
                    || String.IsNullOrEmpty(objIns.boilerSafeToUse)
                    || String.IsNullOrEmpty(objIns.boilerServiced)
                    || String.IsNullOrEmpty(objIns.combustionReading)
                    || String.IsNullOrEmpty(objIns.fluePerformanceChecks)
                    || String.IsNullOrEmpty(objIns.flueVisualCondition)
                    || objIns.inspectionDate == null
                    || String.IsNullOrEmpty(objIns.safetyDeviceOperational)
                    || String.IsNullOrEmpty(objIns.satisfactoryTermination)
                    || String.IsNullOrEmpty(objIns.smokePellet)
                    || String.IsNullOrEmpty(objIns.spillageTest)
                    || String.IsNullOrEmpty(objIns.operatingPressureUnit)
                    )
                {
                    isValid = false;
                }

            }

            return isValid;
        }
        #endregion

        public bool isInspected(int applianceID) //, int appointmentID
        {
            var appId = (from appIns in context.GS_ApplianceInspection
                         where appIns.APPLIANCEID == applianceID //&& appIns.APPOINTMENTID == appointmentID
                         select appIns.APPLIANCEINSPECTIONID);
            if (appId.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isInspectedGas(int propertyApplianceId, int journalId)
        {
            var appId = (from appIns in context.P_APPLIANCE_INSPECTION
                         where appIns.PROPERTYAPPLIANCEID == propertyApplianceId && appIns.Journalid == journalId
                         select appIns.APPLIANCEINSPECTIONID);
            if (appId.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region get Detector Count
        /// <summary>
        /// This function returns detector count against a property
        /// </summary>
        /// <returns>Property Attributes Object</returns>
        public string getDetectorCount(string propertyID, string itemName)
        {
            string result = "0";

            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == "Quantity"
                            && i.ItemName == itemName
                             select ip.ItemParamID);

            if (itemParam.Count() > 0)
            {
                int ItemParamID = itemParam.First();
                var detectorsCount = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyID && patt.ITEMPARAMID == ItemParamID);

                if (detectorsCount.Count() > 0)
                {
                    result = detectorsCount.First().PARAMETERVALUE;
                }

            }
            return result;
        }

        #endregion

        #region update Property Detector Count
        /// <summary>
        /// This function returns update Property Detector Count against a property
        /// </summary>
        /// <returns>Property Attributes Object</returns>
        public bool updatePropertyDetectorCount(string propertyID, string itemName, string quantity, int? updatedBy)
        {
            bool result = false;

            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == "Quantity"
                            && i.ItemName == itemName
                             select ip.ItemParamID);

            if (itemParam.Count() > 0)
            {
                int ItemParamID = itemParam.First();
                var detectorsCount = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyID && patt.ITEMPARAMID == ItemParamID);

                if (detectorsCount.Count() > 0)
                {

                    PA_PROPERTY_ATTRIBUTES attributes = detectorsCount.First();
                    attributes.PARAMETERVALUE = quantity;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }
                else
                {
                    PA_PROPERTY_ATTRIBUTES attributes = new PA_PROPERTY_ATTRIBUTES();
                    attributes.PROPERTYID = propertyID;
                    attributes.ITEMPARAMID = ItemParamID;
                    attributes.PARAMETERVALUE = quantity;
                    attributes.IsCheckBoxSelected = false;
                    attributes.UPDATEDON = DateTime.Now;
                    attributes.UPDATEDBY = updatedBy;
                    context.AddToPA_PROPERTY_ATTRIBUTES(attributes);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }

            }
            return result;
        }

        #endregion

        #region Update Detector Count
        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>  
        public ResultBoolData updateDetectorCount(string propertyId, DetectorCountData detectorCountData, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            ResultBoolData resultData = new ResultBoolData();

            //using (TransactionScope trans = new TransactionScope())
            //{
            if (detectorCountData.detectorTypeId == 1)
            {
                success = updatePropertyDetectorCount(propertyId, "Smoke", detectorCountData.detectorCount.ToString(), updatedBy);

            }
            if (detectorCountData.detectorTypeId == 2)
            {
                success = updatePropertyDetectorCount(propertyId, "CO", detectorCountData.detectorCount.ToString(), updatedBy);
            }
            //trans.Complete();
            //}

            if (currentContext == null) context.AcceptAllChanges();

            resultData.result = success;
            return resultData;
        }
        #endregion

        #region Add Update Detector Info
        /// <summary>
        /// This function adds or updates detector info
        /// </summary>
        /// <param name="detectorType"></param>
        /// <param name="detectorInspectionData">DetectorInspectionData object</param>
        /// <returns>ID in case of success</returns>  
        public ResultIntData addUpdateDetectorInfo(DetectorInspectionData detectorInspectionData, int detectorTypeId, bool isInspected, int? journalId, string propertyId, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            int detectorInspectionId = 0;
            ResultIntData resultData = new ResultIntData();

            //using (TransactionScope trans = new TransactionScope())
            //{
            if (detectorTypeId == 1)
            {
                var smokeDetectorInfo = context.P_Smoke_Inspection.Where(psi => psi.PropertyId == propertyId && psi.Journalid == journalId);

                if (smokeDetectorInfo.Count() > 0)
                {
                    P_Smoke_Inspection smokeIspectionObject = smokeDetectorInfo.First();
                    smokeIspectionObject.DateStamp = detectorInspectionData.inspectionDate;
                    smokeIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    smokeIspectionObject.IsInspected = isInspected;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = smokeIspectionObject.DectectorId;
                }
                else
                {
                    P_Smoke_Inspection smokeIspectionObject = new P_Smoke_Inspection();
                    smokeIspectionObject.DateStamp = detectorInspectionData.inspectionDate;
                    smokeIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    smokeIspectionObject.IsInspected = isInspected;
                    smokeIspectionObject.Journalid = journalId;
                    smokeIspectionObject.PropertyId = propertyId;
                    context.AddToP_Smoke_Inspection(smokeIspectionObject);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = smokeIspectionObject.DectectorId;
                }
            }
            else if (detectorTypeId == 2)
            {
                var coDetectorInfo = context.P_CO2_Inspection.Where(pci => pci.PropertyId == propertyId && pci.Journalid == journalId);

                if (coDetectorInfo.Count() > 0)
                {
                    P_CO2_Inspection coIspectionObject = coDetectorInfo.First();
                    coIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    coIspectionObject.IsInspected = isInspected;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = coIspectionObject.DectectorId;
                }
                else
                {
                    P_CO2_Inspection coIspectionObject = new P_CO2_Inspection();
                    coIspectionObject.DateStamp = detectorInspectionData.inspectionDate;
                    coIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    coIspectionObject.IsInspected = isInspected;
                    coIspectionObject.Journalid = journalId;
                    coIspectionObject.PropertyId = propertyId;
                    context.AddToP_CO2_Inspection(coIspectionObject);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = coIspectionObject.DectectorId;
                }
            }
            //}

            if (currentContext == null) context.AcceptAllChanges();

            resultData.result = detectorInspectionId;

            return resultData;
        }
        #endregion

        #region Get Detector List

        public List<DetectorCountData> getDetectorList(string propertyId, int appointmentId)
        {

            List<DetectorCountData> detectorList = new List<DetectorCountData>();
            FaultsDal fltDal = new FaultsDal();
            var detectorsCount = context.P_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyId);

            //if (detectorsCount.Count() > 0)
            //{
            DetectorCountData smokeDetectorCount = new DetectorCountData();
            DetectorCountData CO2DetectorCount = new DetectorCountData();

            smokeDetectorCount.detectorType = MessageConstants.SmokeDetectorName;
            smokeDetectorCount.detectorTypeId = getDetectorId(MessageConstants.SmokeDetectorName);
            smokeDetectorCount.detectorCount = 0;

            smokeDetectorCount.detectorCount = Convert.ToInt32(getDetectorCount(propertyId, "Smoke"));
            smokeDetectorCount.propertyId = propertyId;
            smokeDetectorCount.isInspected = fltDal.isInspectedSmokeDetector(propertyId, appointmentId);
            smokeDetectorCount.detectorInspection = fltDal.smokeDetectorInspection(propertyId, appointmentId);

            CO2DetectorCount.detectorType = MessageConstants.CODetectorName;
            CO2DetectorCount.detectorTypeId = getDetectorId(MessageConstants.CODetectorName);
            CO2DetectorCount.detectorCount = 0;

            CO2DetectorCount.detectorCount = Convert.ToInt32(getDetectorCount(propertyId, "CO"));
            CO2DetectorCount.propertyId = propertyId;
            CO2DetectorCount.isInspected = fltDal.isInspectedCO2Detector(propertyId, appointmentId);
            CO2DetectorCount.detectorInspection = fltDal.CO2DetectorInspection(propertyId, appointmentId);
            detectorList.Add(smokeDetectorCount);
            detectorList.Add(CO2DetectorCount);

            //}
            return detectorList;
        }

        public int getDetectorId(string detectorType)
        {
            return context.AS_DetectorType.Where(item => item.DetectorType.ToLower() == detectorType).FirstOrDefault().DetectorTypeId;
        }
        #endregion

        #region Update appliance defect
        /// <summary>
        /// Update appliance defect
        /// </summary>
        /// <param name="appDefectData"></param>
        /// <returns></returns>
        public bool updateApplianceDefects(ApplianceDefectData appDefectData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                P_PROPERTY_APPLIANCE_DEFECTS appDefect = new P_PROPERTY_APPLIANCE_DEFECTS();
                var appl = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(app => app.PropertyDefectId == appDefectData.PropertyDefectId);
                if (appl.Count() > 0)
                {
                    appDefect = appl.First();
                }
                if (appDefectData.ApplianceID != 0)
                {
                    appDefect.ApplianceId = appDefectData.ApplianceID;
                }
                if (appDefectData.CategoryID != null)
                {
                    appDefect.CategoryId = (int)appDefectData.CategoryID;
                }
                if (appDefectData.DefectDate != null)
                {
                    appDefect.DefectDate = appDefectData.DefectDate;
                }
                if (appDefectData.IsDefectIdentified != null)
                {
                    appDefect.IsDefectIdentified = appDefectData.IsDefectIdentified;
                }
                if (appDefectData.IsWarningFixed != null)
                {
                    appDefect.IsWarningFixed = appDefectData.IsWarningFixed;
                }
                if (appDefectData.IsWarningIssued != null)
                {
                    appDefect.IsWarningIssued = appDefectData.IsWarningIssued;
                }
                if (appDefectData.SerialNumber != null)
                {
                    appDefect.SerialNumber = appDefectData.SerialNumber;
                }
                if (appl.Count() <= 0)
                {
                    context.AddToP_PROPERTY_APPLIANCE_DEFECTS(appDefect);
                }
                context.SaveChanges();
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            return success;
        }
        #endregion

        #region Get Employees List for Parts Ordered By

        public List<PartsOrderedBy> getEmployeesListForPartsOrderedBy()
        {
            List<PartsOrderedBy> empList = new List<PartsOrderedBy>();

            var employeesList = (from emp in context.E__EMPLOYEE
                                 join ac in context.AC_LOGINS on emp.EMPLOYEEID equals ac.EMPLOYEEID
                                 where ac.ACTIVE == 1
                                 select new PartsOrderedBy
                                 {
                                     employeeId = emp.EMPLOYEEID,
                                     employeeName = emp.FIRSTNAME + " " + emp.LASTNAME
                                 });

            if (employeesList.Count() > 0)
                empList = employeesList.ToList();

            return empList;
        }

        #endregion

        #region Get Defect Priority List

        public List<DefectsPriority> getDefectPriorities()
        {
            List<DefectsPriority> defectPriorties = new List<DefectsPriority>();

            var defectsPrio = (from prio in context.P_DEFECTS_PRIORITY
                               select new DefectsPriority
                               {
                                   priorityId = prio.PriorityID,
                                   priority = prio.PriorityName
                               });

            if (defectsPrio.Count() > 0)
                defectPriorties = defectsPrio.ToList();

            return defectPriorties;
        }

        #endregion

        #region Get Trades List for Defects

        public List<Trades> getAllTrades()
        {
            List<Trades> trades = new List<Trades>();

            var tradez = (from t in context.G_TRADE
                          select new Trades
                          {
                              tradeId = t.TradeId,
                              trade = t.Description
                          });

            if (tradez.Count() > 0)
                trades = tradez.ToList();

            return trades;
        }

        #endregion

        #region Get Heating Types
        public List<BoilerType> getBoilerTypes()
        {
            int heatingTypeParameterId = getParameterId("Heating", "Heating Type");
            int boilerTypeParameterId = getParameterId("Boiler Room", "Heating Type");

            List<BoilerType> boilerTypeList = new List<BoilerType>();
            var boilerTypes = (from ppv in context.PA_PARAMETER_VALUE
                               where (ppv.ParameterID == heatingTypeParameterId || ppv.ParameterID == boilerTypeParameterId) && ppv.IsActive == true
                               select new BoilerType
                               {
                                   boilerTypeId = ppv.ValueID,
                                   boilerTypeDescription = ppv.ValueDetail,
                                   isSchemeBlock = ppv.ParameterID == boilerTypeParameterId ? true : false
                               });
            if (boilerTypes.Count() > 0)
                boilerTypeList = boilerTypes.ToList();
            return boilerTypeList;
        }
        #endregion

        #region Get Boiler Manufacturers
        public List<BoilerManufacturer> getBoilerManufacturers()
        {
            int propertyManufacturerParameterId = getParameterId("Heating", "Manufacturer");
            int schemeBlockManufacturerParameterId = getParameterId("Boiler Room", "Manufacturer");

            List<BoilerManufacturer> boilerManufacturerList = new List<BoilerManufacturer>();
            var boilerManufacturers = (from ppv in context.PA_PARAMETER_VALUE
                                       where (ppv.ParameterID == propertyManufacturerParameterId || ppv.ParameterID == schemeBlockManufacturerParameterId) && ppv.IsActive == true
                                       select new BoilerManufacturer
                                       {
                                           manufacturerId = ppv.ValueID,
                                           manufacturerDescription = ppv.ValueDetail,
                                           isSchemeBlock = ppv.ParameterID == schemeBlockManufacturerParameterId ? true : false,
                                           isAlternativeHeating = ppv.IsAlterNativeHeating == true ? true : false
                                       });
            if (boilerManufacturers.Count() > 0)
                boilerManufacturerList = boilerManufacturers.ToList();
            return boilerManufacturerList;
        }
        #endregion

        #region Get Mains Gas Flue Types
        public List<LookupData> getMainsGasFlueTypes()
        {
            int parameterId = getParameterId(ApplicationConstants.HeatingItemName, ApplicationConstants.FlueTypeParameterKey, ApplicationConstants.MainsGasParameterValue);

            List<LookupData> flueTypeList = new List<LookupData>();
            var flueTypes = (from ppv in context.PA_PARAMETER_VALUE
                             where (ppv.ParameterID == parameterId) && ppv.IsActive == true
                             select new LookupData
                             {
                                 lookupId = ppv.ValueID,
                                 value = ppv.ValueDetail
                             });
            if (flueTypes.Count() > 0)
                flueTypeList = flueTypes.ToList();
            return flueTypeList;
        }
        #endregion

        #region Get Oil Flue Types
        public List<LookupData> getOilFlueTypes()
        {
            int parameterId = getParameterId(ApplicationConstants.HeatingItemName, ApplicationConstants.FlueTypeParameterKey, ApplicationConstants.OilParameterValue);

            List<LookupData> flueTypeList = new List<LookupData>();
            var flueTypes = (from ppv in context.PA_PARAMETER_VALUE
                             where (ppv.ParameterID == parameterId) && ppv.IsActive == true
                             select new LookupData
                             {
                                 lookupId = ppv.ValueID,
                                 value = ppv.ValueDetail
                             });
            if (flueTypes.Count() > 0)
                flueTypeList = flueTypes.ToList();
            return flueTypeList;
        }
        #endregion

        #region Get Oil Fuel Types
        public List<LookupData> getOilFuelTypes()
        {
            int parameterId = getParameterId(ApplicationConstants.HeatingItemName, ApplicationConstants.FuelTypeParameterKey);

            List<LookupData> fuelTypeList = new List<LookupData>();
            var fuelTypesQuery = (from ppv in context.PA_PARAMETER_VALUE
                                  where (ppv.ParameterID == parameterId) && ppv.IsActive == true
                                  select new LookupData
                                  {
                                      lookupId = ppv.ValueID,
                                      value = ppv.ValueDetail
                                  });
            if (fuelTypesQuery.Count() > 0)
                fuelTypeList = fuelTypesQuery.ToList();
            return fuelTypeList;
        }
        #endregion

        #region Get Tank Types
        public List<LookupData> getTankTypes()
        {
            int parameterId = getParameterId(ApplicationConstants.HeatingItemName, ApplicationConstants.TankTypeParameterKey);

            List<LookupData> list = new List<LookupData>();
            var query = (from ppv in context.PA_PARAMETER_VALUE
                         where (ppv.ParameterID == parameterId) && ppv.IsActive == true
                         select new LookupData
                         {
                             lookupId = ppv.ValueID,
                             value = ppv.ValueDetail
                         });
            if (query.Count() > 0)
                list = query.ToList();
            return list;
        }
        #endregion

        #region Get Burner Types
        public List<LookupData> getBurnerTypes()
        {
            int parameterId = getParameterId(ApplicationConstants.HeatingItemName, ApplicationConstants.BurnerTypeParameterKey);

            List<LookupData> list = new List<LookupData>();
            var query = (from ppv in context.PA_PARAMETER_VALUE
                         where (ppv.ParameterID == parameterId) && ppv.IsActive == true
                         select new LookupData
                         {
                             lookupId = ppv.ValueID,
                             value = ppv.ValueDetail
                         });
            if (query.Count() > 0)
                list = query.ToList();
            return list;
        }
        #endregion

        #region Get Solar Types
        public List<LookupData> getSolarTypes()
        {
            int parameterId = getParameterId(ApplicationConstants.HeatingItemName, ApplicationConstants.SolarTypeParameterKey);

            List<LookupData> list = new List<LookupData>();
            var query = (from ppv in context.PA_PARAMETER_VALUE
                         where (ppv.ParameterID == parameterId) && ppv.IsActive == true
                         select new LookupData
                         {
                             lookupId = ppv.ValueID,
                             value = ppv.ValueDetail
                         });
            if (query.Count() > 0)
                list = query.ToList();
            return list;
        }
        #endregion

        #region Get Parameter Id
        public int getParameterId(string itemName, string parameterName, string subParameterValue = null)
        {
            var paramQuery = (from ip in context.PA_ITEM_PARAMETER
                              join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                              join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                              join subpv in context.PA_PARAMETER_VALUE on ip.ParameterValueId equals subpv.ValueID into subpvs
                              from subpv in subpvs.DefaultIfEmpty()
                              where p.ParameterName == parameterName && i.ItemName == itemName && ip.IsActive == true
                              select new { ip = ip, heatingValue = (subpv == null) ? null : subpv.ValueDetail });

            if (!string.IsNullOrEmpty(subParameterValue))
            {
                paramQuery = paramQuery.Where(x => x.heatingValue == subParameterValue);
            }


            int parameterId = 0;
            if (paramQuery.Count() > 0)
            {
                parameterId = (int)paramQuery.First().ip.ParameterId;
            }
            return parameterId;
        }
        #endregion

        #region Save Boiler Inspection
        public int saveBoilerInspection(BoilerInspectionData boilerData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            P_BOILER_INSPECTION boilerInspection = new P_BOILER_INSPECTION();
            var propertyBoilerInspection = context.P_BOILER_INSPECTION.Where(boil => boil.JOURNALID == boilerData.journalId && boil.HeatingMappingId == boilerData.heatingId);
            if (propertyBoilerInspection.Count() > 0)
            {
                boilerInspection = propertyBoilerInspection.First();

                #region "Update"
                boilerInspection.ADEQUATEVENTILATION = boilerData.adequateVentilation;
                boilerInspection.BOILERTYPEID = boilerData.boilerTypeId;
                boilerInspection.BOILERSAFETOUSE = boilerData.boilerSafeToUse;
                boilerInspection.BOILERSERVICED = boilerData.boilerServiced;
                boilerInspection.COMBUSTIONREADING = boilerData.combustionReading;
                boilerInspection.FLUEPERFORMANCECHECKS = boilerData.fluePerformanceChecks;
                boilerInspection.FLUEVISUALCONDITION = boilerData.flueVisualCondition;
                boilerInspection.INSPECTIONDATE = boilerData.inspectionDate;
                boilerInspection.OPERATINGPRESSURE = boilerData.operatingPressure;
                boilerInspection.ISINSPECTED = (bool)boilerData.isInspected;
                boilerInspection.SAFETYDEVICEOPERATIONAL = boilerData.safetyDeviceOperational;
                boilerInspection.SATISFACTORYTERMINATION = boilerData.satisfactoryTermination;
                boilerInspection.SMOKEPELLET = boilerData.smokePellet;
                boilerInspection.SPILLAGETEST = boilerData.spillageTest;
                boilerInspection.INSPECTEDBY = boilerData.inspectedBy;
                boilerInspection.JOURNALID = boilerData.journalId;
                boilerInspection.HeatingMappingId = boilerData.heatingId;
                boilerInspection.OPERATINGPRESSUREUNIT = boilerData.operatingPressureUnit;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                success = true;
                if (currentContext == null) context.AcceptAllChanges();
                if (success)
                    return 1;
                else return 0;
                #endregion

            }
            else
            {
                #region "Insert"
                using (TransactionScope trans = new TransactionScope())
                {
                    boilerInspection.ADEQUATEVENTILATION = boilerData.adequateVentilation;
                    boilerInspection.BOILERTYPEID = boilerData.boilerTypeId;
                    boilerInspection.HeatingMappingId = boilerData.heatingId;
                    boilerInspection.BOILERSAFETOUSE = boilerData.boilerSafeToUse;
                    boilerInspection.BOILERSERVICED = boilerData.boilerServiced;
                    boilerInspection.COMBUSTIONREADING = boilerData.combustionReading;
                    boilerInspection.FLUEPERFORMANCECHECKS = boilerData.fluePerformanceChecks;
                    boilerInspection.FLUEVISUALCONDITION = boilerData.flueVisualCondition;
                    boilerInspection.INSPECTIONDATE = boilerData.inspectionDate;
                    boilerInspection.OPERATINGPRESSURE = boilerData.operatingPressure;
                    boilerInspection.ISINSPECTED = (bool)boilerData.isInspected;
                    boilerInspection.SAFETYDEVICEOPERATIONAL = boilerData.safetyDeviceOperational;
                    boilerInspection.SATISFACTORYTERMINATION = boilerData.satisfactoryTermination;
                    boilerInspection.SMOKEPELLET = boilerData.smokePellet;
                    boilerInspection.SPILLAGETEST = boilerData.spillageTest;
                    boilerInspection.INSPECTEDBY = boilerData.inspectedBy;
                    boilerInspection.JOURNALID = boilerData.journalId;
                    boilerInspection.OPERATINGPRESSUREUNIT = boilerData.operatingPressureUnit;
                    context.AddToP_BOILER_INSPECTION(boilerInspection);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                if (success == true)
                {
                    return boilerInspection.BOILERINSPECTIONID;
                }
                else
                {
                    return 0;
                }
                #endregion
            }
        }
        #endregion

    }
}