﻿using System.Collections.Generic;
using System.Linq;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.Transactions;
using System;
using System.Data.Objects;

namespace PropSurvey.Dal.Customer
{
    public class CustomerDal : BaseDal
    {
        #region get Customer Info
        /// <summary>
        /// This function returns the customer record 
        /// </summary>
        /// <param name="customerId">customer id </param>
        /// <returns>It returns the customer data object</returns>
        public CustomerData getCustomerInfo(int customerId)
        {

            var customer = (from cus in context.C__CUSTOMER
                            join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                            from gti in tempgti.DefaultIfEmpty()
                            join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                            join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                            join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                            join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                            join p2a in context.PS_Property2Appointment on pro.PROPERTYID equals p2a.PropertyId into prop2appt
                            from fin in prop2appt.DefaultIfEmpty()
                            where ten.ENDDATE == null && cus.CUSTOMERID == customerId && cad.ISDEFAULT == 1
                            select new CustomerData
                            {
                                customerId = cus.CUSTOMERID,
                                title = gti.DESCRIPTION,
                                firstName = cus.FIRSTNAME,
                                middleName = cus.MIDDLENAME,
                                lastName = cus.LASTNAME,
                                telephone = cad.TEL,
                                mobile = cad.MOBILE,
                                fax = cad.FAX,
                                email = cad.EMAIL,
                                property = new PropertyData
                                {
                                    houseNumber = pro.HOUSENUMBER,
                                    flatNumber = pro.FLATNUMBER,
                                    address1 = pro.ADDRESS1,
                                    address2 = pro.ADDRESS2,
                                    address3 = pro.ADDRESS3,
                                    townCity = pro.TOWNCITY,
                                    postCode = pro.POSTCODE,
                                    county = pro.COUNTY,
                                    tenancyId = ten.TENANCYID,
                                    propertyId = pro.PROPERTYID,
                                    lastSurveyDate = fin.LastSurveyDate
                                }

                            });

            CustomerData custData = new CustomerData();
            if (customer.Count() > 0)
            {
                custData = customer.First();
            }

            return custData;

        }
        #endregion

        #region "get Customer Vulnerabilities"
        /// <summary>
        /// This function returns the list of vulnerabilities of the customer.
        /// </summary>
        /// <param name="propertyId">customer id</param>
        /// <returns>the object of vulnerabilities of the customer</returns>

        public List<CustomerVulnerabilityData> getCustomerVulnerabilities(int customerId)
        {
            var customerVul = (from cvu in context.C_VULNERABILITY
                               join ccv in context.C_Customer_Vulnerability on cvu.VULNERABILITYHISTORYID equals ccv.VulnerabilityHistoryId
                               join ccc in context.C_VULNERABILITY_CATEGORY on ccv.CategoryId equals ccc.CategoryId
                               join ccs in context.C_VULNERABILITY_SUBCATEGORY on ccv.SubCategoryId equals ccs.SubCategoryId
                               where ccv.CustomerId == customerId
                               orderby cvu.VULNERABILITYHISTORYID descending

                               select new CustomerVulnerabilityData
                               {
                                   vulHistoryId = cvu.VULNERABILITYHISTORYID,
                                   vulCatDesc = ccc.Description,
                                   vulSubCatDesc = ccs.Description
                               });

            List<CustomerVulnerabilityData> cusVu = new List<CustomerVulnerabilityData>();
            if (customerVul.Count() > 0)
            {
                cusVu = customerVul.ToList();
            }

            return cusVu;
        }
        #endregion

        #region get Customer Risk
        /// <summary>
        /// This function 'll return the risk of customer. This function calls the stored procedure
        /// </summary>
        /// <param name="customerId">customer id </param>
        /// <returns>It returns the List returned from stored procedure</returns>
        public List<PS_GetCustomerRisk_Result> getCustomerRisk(int customerId)
        {

            var riskList = context.PS_GetCustomerRisk(customerId);
            List<PS_GetCustomerRisk_Result> customerRiskList = new List<PS_GetCustomerRisk_Result>();
            customerRiskList = riskList.ToList();
            return customerRiskList;
        }
        #endregion

        #region Update a Customer Contact data
        /// <summary>
        /// This function updates customer data (mobile, telephone,email)
        /// </summary>
        /// <param name="CustomerData">Customer Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdateCustomerContactFromAppointment(CustomerData cusData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            //using (TransactionScope trans = new TransactionScope())
            //{
            {
                var cAddList = context.C_ADDRESS.Where(cadd => cadd.CUSTOMERID == cusData.customerId);

                if (cAddList.Count() == 0)
                    return false;

                C_ADDRESS cusAdd = cAddList.First();
                cusAdd.MOBILE = cusData.mobile;
                cusAdd.TEL = cusData.telephone;
                cusAdd.EMAIL = cusData.email;

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                //       trans.Complete();
                success = true;
            }
            //}

            if (currentContext == null) context.AcceptAllChanges();

            return success;
        }
        #endregion

        #region get Employee Signature Image Name
        /// <summary>
        /// This function returns the employee signature image name which was saved against employee
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <returns>returns the name of the image</returns>
        /// 
        public string getEmployeeSignatureImageName(int employeeId)
        {
            string imageName = String.Empty;

            var employee = (from emp in context.E__EMPLOYEE
                            where emp.EMPLOYEEID == employeeId
                            select emp.SIGNATUREPATH);

            if (employee.Count() > 0)
            {
                if (employee.Single() != null)
                {
                    imageName = employee.Single().ToString();
                }
            }

            return imageName;
        }
        #endregion

    }
}
