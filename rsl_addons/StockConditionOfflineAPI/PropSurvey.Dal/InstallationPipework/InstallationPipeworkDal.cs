﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Contracts.Data.Appointment;

namespace PropSurvey.Dal.InstallationPipework
{
    public class InstallationPipeworkDal : BaseDal
    {

        #region get InstallationPipework Data
        /// <summary>
        /// This function returns the InstallationPipework data
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>

        public InstallationPipeworkData getInstallationPipeworkFormData(int AppointmentID)
        {
            InstallationPipeworkData _var = (from work in context.GS_InstallationPipeWork
                                             where work.AppointmentID == AppointmentID
                                             select new InstallationPipeworkData
                                                {
                                                    ID = work.ID,
                                                    AppointmentID = work.AppointmentID,
                                                    inspectionDate = work.Date,
                                                    emergencyControl = work.EmergencyControl,
                                                    equipotentialBonding = work.EquipotentialBonding,
                                                    gasTightnessTest = work.GasTightnessTest,
                                                    visualInspection = work.VisualInspection
                                                }).FirstOrDefault();

            return _var;
        }

        //Change#12 - Behroz - 12/14/2012 - Start
        /// <summary>
        /// This function returns the InstallationPipework data for gas
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        //public InstallationPipeworkData getInstallationPipeworkFormDataGas(string propertyId)
        //{
        //    InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork
        //                                     where work.PropertyId == propertyId
        //    //InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork join jor in context.AS_JOURNAL
        //    //                                                                             on work.PropertyId equals jor.PROPERTYID
        //    //                                 where work.PropertyId == propertyId && jor.ISCURRENT
        //                                     select new InstallationPipeworkData
        //                                     {
        //                                         ID = work.PipeWorkID,
        //                                         PropertyId = work.PropertyId,
        //                                         Date = work.DateStamp,
        //                                         EmergencyControl = work.EmergencyControl,
        //                                         EquipotentialBonding = work.EquipotentialBonding,
        //                                         GasTightnessTest = work.GasTightnessTest,
        //                                         VisualInspection = work.VisualInspection,
        //                                         JournalId = work.JournalId
        //                                     }).FirstOrDefault();

        //    return _var;
        //}
        

        #region "Get Installation Pipework Form Data Gas"
        public InstallationPipeworkData getInstallationPipeworkFormDataGas(int? heatingMappingId,int? journalId)
        {
            InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork
                                             where work.Journalid == journalId && work.heatingMappingId == heatingMappingId
                                             select new InstallationPipeworkData
                                             {
                                                 ID = work.PipeWorkID,
                                                 propertyId = work.PropertyId,
                                                 schemeId = work.schemeId,
                                                 blockId = work.blockId,
                                                 heatingId = work.heatingMappingId,
                                                 inspectionDate = work.DateStamp,
                                                 emergencyControl = work.EmergencyControl,
                                                 equipotentialBonding = work.EquipotentialBonding,
                                                 gasTightnessTest = work.GasTightnessTest,
                                                 visualInspection = work.VisualInspection,
                                                 JournalId = work.Journalid
                                             }).FirstOrDefault();

            return _var;
        }
        #endregion

        /// <summary>
        /// This function returns the InstallationPipework data for gas
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        public InstallationPipeworkData getInstallationPipeworkFormDataGas(int journalId)
        {
            InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork
                                             where work.Journalid == journalId
                                             select new InstallationPipeworkData
                                             {
                                                 ID = work.PipeWorkID,
                                                 propertyId = work.PropertyId,
                                                 inspectionDate = work.DateStamp,
                                                 emergencyControl = work.EmergencyControl,
                                                 equipotentialBonding = work.EquipotentialBonding,
                                                 gasTightnessTest = work.GasTightnessTest,
                                                 visualInspection = work.VisualInspection,
                                                 JournalId = work.Journalid
                                             }).FirstOrDefault();

            return _var;
        }
        //Change#12 - Behroz - 12/14/2012 - End
        #endregion

        #region save InstallationPipework Data
        /// <summary>
        /// This function saves the InstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>

        public int saveInstallationPipework(InstallationPipeworkData workData)
        {
            bool success = false;

            GS_InstallationPipeWork insPipework = new GS_InstallationPipeWork();
            using (TransactionScope trans = new TransactionScope())
            {
                insPipework.AppointmentID = workData.AppointmentID;
                insPipework.Date = DateTime.Now;
                insPipework.EmergencyControl = workData.emergencyControl.ToUpper();
                insPipework.EquipotentialBonding = workData.equipotentialBonding.ToUpper();
                insPipework.GasTightnessTest = workData.gasTightnessTest.ToUpper();
                insPipework.VisualInspection = workData.visualInspection.ToUpper();

                context.AddToGS_InstallationPipeWork(insPipework);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return insPipework.ID;
            }
            else
            {
                return 0;
            }
        }


        /// <summary>
        /// This function saves the InstallationPipework Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        public int saveInstallationPipeworkGas(BoilerData boilerData, AllAppointmentsList workData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            P_InstallationPipeWork installationwork = new P_InstallationPipeWork();
            var instalPipWrk = boilerData.InstallationPipework;
            var installObj = context.P_InstallationPipeWork.Where(ins => ins.Journalid == workData.journalId && ins.heatingMappingId == boilerData.heatingId);

            if (installObj.Count() > 0)
            {                
                installationwork = installObj.First();
                installationwork.DateStamp = (DateTime)instalPipWrk.inspectionDate;
                installationwork.EmergencyControl = instalPipWrk.emergencyControl.ToUpper();
                installationwork.EquipotentialBonding = instalPipWrk.equipotentialBonding.ToUpper();
                installationwork.GasTightnessTest = instalPipWrk.gasTightnessTest.ToUpper();
                installationwork.VisualInspection = instalPipWrk.visualInspection.ToUpper();                
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                return 0;
            }
            else
            {
                installationwork.PropertyId = instalPipWrk.propertyId;
                installationwork.schemeId = instalPipWrk.schemeId;
                installationwork.blockId = instalPipWrk.blockId;
                installationwork.heatingMappingId = instalPipWrk.heatingId;
                installationwork.Journalid = workData.journalId;

                if (instalPipWrk.inspectionDate != null)
                {
                    installationwork.DateStamp = (DateTime)instalPipWrk.inspectionDate;
                }
                if (instalPipWrk.emergencyControl != null)
                {
                    installationwork.EmergencyControl = instalPipWrk.emergencyControl.ToUpper();
                }
                if (instalPipWrk.equipotentialBonding != null)
                {
                    installationwork.EquipotentialBonding = instalPipWrk.equipotentialBonding.ToUpper();
                }
                if (instalPipWrk.gasTightnessTest != null)
                {
                    installationwork.GasTightnessTest = instalPipWrk.gasTightnessTest.ToUpper();
                }
                if (instalPipWrk.visualInspection != null)
                {
                    installationwork.VisualInspection = instalPipWrk.visualInspection.ToUpper();
                }

                context.AddToP_InstallationPipeWork(installationwork);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                if (currentContext == null) context.AcceptAllChanges();
                return installationwork.PipeWorkID;
            }
        }
        #endregion

        #region update InstallationPipework Data
        /// <summary>
        /// This function updates theInstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateInstallationPipeworkData(InstallationPipeworkData insData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.GS_InstallationPipeWork
                            where ins.ID == insData.ID
                            select ins);
                if (_var.Count() > 0)
                {
                    GS_InstallationPipeWork Ins = _var.FirstOrDefault();
                    Ins.VisualInspection = insData.visualInspection.ToUpper();
                    Ins.GasTightnessTest = insData.gasTightnessTest.ToUpper();
                    Ins.EquipotentialBonding = insData.equipotentialBonding.ToUpper();
                    Ins.EmergencyControl = insData.emergencyControl.ToUpper();
                    Ins.AppointmentID = insData.AppointmentID;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }

            }

            return success;
        }

        #endregion

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app => app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
