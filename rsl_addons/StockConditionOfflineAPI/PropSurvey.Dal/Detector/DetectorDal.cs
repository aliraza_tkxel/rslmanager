﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using PropSurvey.Contracts.Data;
using PropSurvey.Entities;
using PropSurvey.Dal.Base;

namespace PropSurvey.Dal
{
    public class DetectorDal : BaseDal
    {

        #region Get Detector Types

        public List<DetectorType> getDetectorTypes()
        {
            List<DetectorType> detectorTypes = (from dt in context.AS_DetectorType
                                                select new DetectorType
                                                {
                                                    detectorTypeId = dt.DetectorTypeId,
                                                    detectorType = dt.DetectorType
                                                }).ToList();

            return detectorTypes;
        }


        #endregion

        #region Get All Detectors

        public List<Detector> GetAllDetectors(ApplianceSurveyDataParam requestParam)
        {
            List<Detector> Detectors = new List<Detector>();

            //Getting detectors detail from database, joined detectors type table to get detector type description.
            var detectorsQueryResult = from detector in context.P_DETECTOR
                                       join detectorType in context.AS_DetectorType on detector.DetectorTypeId equals detectorType.DetectorTypeId                                       
                                       select new Detector
                                       {
                                           detectorId = detector.DetectorId,
                                           propertyId = detector.PropertyId,
                                           schemeId = detector.SchemeId,
                                           blockId = detector.BlockId,
                                           detectorTypeId = detector.DetectorTypeId,
                                           detectorType = detectorType.DetectorType,
                                           location = detector.Location,
                                           manufacturer = detector.Manufacturer,
                                           serialNumber = detector.SerialNumber,
                                           powerTypeId = detector.PowerSource,
                                           installedDate = detector.InstalledDate,
                                           isLandlordsDetector = detector.IsLandlordsDetector,
                                           lastTestedDate = detector.TestedDate,
                                           batteryReplaced = detector.BatteryReplaced,
                                           isPassed = detector.Passed,
                                           notes = detector.Notes,
                                           inspectionDate = detector.TestedDate
                                       };


            if (requestParam.propertyId != null)
            {
                detectorsQueryResult = detectorsQueryResult.Where(x => x.propertyId == requestParam.propertyId);                
            }
            else if (requestParam.schemeId != null && requestParam.schemeId > 0)
            {
                detectorsQueryResult = detectorsQueryResult.Where(x => x.schemeId == requestParam.schemeId);                
            }
            else if (requestParam.blockId != null && requestParam.blockId > 0)
            {
                detectorsQueryResult = detectorsQueryResult.Where(x => x.blockId == requestParam.blockId);                
            }

            if (detectorsQueryResult.Count() > 0)
                Detectors = detectorsQueryResult.ToList();

            return Detectors;
        }

        #endregion

        #region save (insert/update) detector

        public bool saveDetector(Detector detector, int operativeId, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;

            //if (currentContext != null) this.context = currentContext;


            bool addNewDetector = true;

            P_DETECTOR dbDetector = new P_DETECTOR();

            #region Check for existing detector, if exists get its context reference.

            if (detector.detectorId > 0)
            {
                var detctorsQueryResult = from d in context.P_DETECTOR
                                          where d.DetectorId == detector.detectorId
                                          select d;

                if (detctorsQueryResult.Count() > 0)
                {
                    dbDetector = detctorsQueryResult.First();
                    addNewDetector = false;
                }
            }

            #endregion

            #region Set/Update dector values for detector object

            if (detector.detectorTypeId != null)
                dbDetector.DetectorTypeId = (int)detector.detectorTypeId;
            if (detector.installedDate != null)
                dbDetector.InstalledDate = (DateTime)detector.installedDate;
            if (detector.isLandlordsDetector != null)
                dbDetector.IsLandlordsDetector = (bool)detector.isLandlordsDetector;

            dbDetector.Location = detector.location;
            dbDetector.Manufacturer = detector.manufacturer;
            if (detector.powerTypeId != null)
                dbDetector.PowerSource = (int)detector.powerTypeId;
            dbDetector.PropertyId = detector.propertyId;
            dbDetector.SchemeId = detector.schemeId;
            dbDetector.BlockId = detector.blockId;
            dbDetector.SerialNumber = detector.serialNumber;
            if (detector.isInspected == true)
            {
                dbDetector.Notes = detector.notes;
                dbDetector.Passed = detector.isPassed;
                dbDetector.BatteryReplaced = detector.batteryReplaced;
                dbDetector.TestedDate = detector.inspectionDate;
                dbDetector.TestedBy = operativeId;
            }
            #endregion

            if (addNewDetector)
            {
                // In case a new detector is added from app, installBy is same as inspectedBy
                // so we will set it as following:
                dbDetector.InstalledBy = operativeId;
                context.P_DETECTOR.AddObject(dbDetector);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            context.SaveChanges();

            return saveStatus;
        }

        #endregion

    }
}
