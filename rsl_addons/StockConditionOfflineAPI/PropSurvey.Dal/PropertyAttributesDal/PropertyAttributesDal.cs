﻿using System;
using System.Linq;
using PropSurvey.Contracts.Data;
using PropSurvey.Entities;
using System.Collections.Generic;

namespace PropSurvey.Dal
{
    public class PropertyAttributesDal : Base.BaseDal
    {

        #region Save Attributes

        public bool saveMeterInfo(string propertyId, int itemId, string parameterName
            , string parameterValue, Nullable<long> valueId, Nullable<DateTime> updatedOn, Nullable<int> updatedBy, bool isCheckBoxSelected
            , Nullable<int> schemeId = null, Nullable<int> blockId = null, bool isUpdated = false, bool checkValueId = false, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;

            var getItemParameterIdQuery = from ip in context.PA_ITEM_PARAMETER
                                          join p in context.PA_PARAMETER on (int)ip.ParameterId equals p.ParameterID
                                          where ip.ItemId==itemId && p.ParameterName==parameterName
                                          select ip;

            if (getItemParameterIdQuery.Count() > 0)
            {
                var itemParameter = getItemParameterIdQuery.First();
                int itemParameterId = itemParameter.ItemParamID;
                if (checkValueId && valueId == null) valueId = getValueIdByParameterIdAndValueDetail((int)itemParameter.ParameterId, parameterValue);
                saveStatus = savePropertyAttribueByItemParameterId(propertyId, itemParameterId, parameterValue, valueId, updatedOn, updatedBy, isCheckBoxSelected, schemeId, blockId, isUpdated, currentContext);
            }

            return saveStatus;
        }

        public Nullable<long> getValueIdByParameterIdAndValueDetail(int parameterId, string valueDetail)
        {
            Nullable<long> valueId = null;

            PA_PARAMETER_VALUE parameterValue = context.PA_PARAMETER_VALUE.Where(pv => pv.ParameterID == parameterId
                                                        && pv.ValueDetail == valueDetail).FirstOrDefault();

            if (parameterValue != null)
                valueId = parameterValue.ValueID;

            return valueId;
        }        

        public bool savePropertyAttribueByItemParameterId(string propertyId, int itemParameterId
            , string parameterValue, Nullable<long> valueId, Nullable<DateTime> updatedOn, Nullable<int> updatedBy, Nullable<bool> isCheckBoxSelected
            , Nullable<int> schemeId = null, Nullable<int> blockId = null, bool isUpdated = false, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;
            
            PA_PROPERTY_ATTRIBUTES propertyAttributeToSave = new PA_PROPERTY_ATTRIBUTES()
            {
                PROPERTYID = propertyId,
                ITEMPARAMID = itemParameterId,
                PARAMETERVALUE = parameterValue,
                VALUEID = valueId,
                UPDATEDON = updatedOn,
                UPDATEDBY = updatedBy,
                IsCheckBoxSelected = isCheckBoxSelected,
                SchemeId = schemeId,
                BlockId = blockId,
                IsUpdated = isUpdated
            };

            saveStatus = savePropertyAttribute(propertyAttributeToSave, currentContext);

            return saveStatus;
        }

        public bool savePropertyAttribute(PA_PROPERTY_ATTRIBUTES propertyAttributeToSave, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool saveStatus = false;

            var propAttribQuery = from pa in context.PA_PROPERTY_ATTRIBUTES
                                  where pa.ITEMPARAMID == propertyAttributeToSave.ITEMPARAMID
                                        && (pa.PROPERTYID == propertyAttributeToSave.PROPERTYID
                                            || pa.SchemeId == propertyAttributeToSave.SchemeId
                                            || pa.BlockId == propertyAttributeToSave.BlockId
                                            )
                                  select pa;

            if (propAttribQuery.Count() > 0)
            {
                PA_PROPERTY_ATTRIBUTES propertyAttribute = propAttribQuery.First();

                propertyAttribute.PARAMETERVALUE = propertyAttributeToSave.PARAMETERVALUE ?? propertyAttribute.PARAMETERVALUE;
                propertyAttribute.VALUEID = propertyAttributeToSave.VALUEID ?? propertyAttribute.VALUEID;
                propertyAttribute.UPDATEDON = propertyAttributeToSave.UPDATEDON ?? propertyAttribute.UPDATEDON;
                propertyAttribute.UPDATEDBY = propertyAttributeToSave.UPDATEDBY ?? propertyAttribute.UPDATEDBY;
                propertyAttribute.IsCheckBoxSelected = propertyAttributeToSave.IsCheckBoxSelected ?? propertyAttribute.IsCheckBoxSelected;

                // Skipping is updated as it is only related to property inspection
                // propertyAttribute.IsUpdated = propertyAttributeToSave.IsUpdated;

                context.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
                propertyAttributeToSave.ATTRIBUTEID = propertyAttribute.ATTRIBUTEID;
            }
            else
            {
                context.PA_PROPERTY_ATTRIBUTES.AddObject(propertyAttributeToSave);
                context.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            }

            saveStatus = true;
            return saveStatus;
        }

        #endregion

        #region Save Property Item Dates

        public bool savePropertyDateAttribueByItemIdAndParameterName(string propertyId, int itemId, string parameterName
            , Nullable<DateTime> DueDate, Nullable<DateTime> LastDone, Nullable<DateTime> updatedOn, Nullable<int> updatedBy
            , Nullable<int> schemeId = null, Nullable<int> blockId = null, bool isVoidAppointmentAssociated = false, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;

            var getItemParameterIdQuery = from ip in context.PA_ITEM_PARAMETER
                                          join p in context.PA_PARAMETER on (int)ip.ParameterId equals p.ParameterID
                                          where ip.ItemId == itemId && p.ParameterName == parameterName
                                          select ip;

            if (getItemParameterIdQuery.Count() > 0)
            {
                var itemParameter = getItemParameterIdQuery.First();
                int parameterId = (int) itemParameter.ParameterId;
                saveStatus = savePropertyDateAttribueByItemIdAndParameterId(propertyId, itemId, parameterId
                        , DueDate, LastDone, updatedOn, updatedBy, schemeId, blockId, isVoidAppointmentAssociated, currentContext);
            }

            return saveStatus;
        }

        public bool savePropertyDateAttribueByItemIdAndParameterId(string propertyId, int itemId, int parameterId
            , Nullable<DateTime> dueDate, Nullable<DateTime> lastDone, Nullable<DateTime> updatedOn, Nullable<int> updatedBy
            , Nullable<int> schemeId = null, Nullable<int> blockId = null, bool isVoidAppointmentAssociated = false, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;

            PA_PROPERTY_ITEM_DATES propertyDateAttributeToSave = new PA_PROPERTY_ITEM_DATES()
            {
                PROPERTYID = propertyId,
                ItemId = itemId,
                ParameterId = parameterId,                
                LastDone = lastDone,
                DueDate = dueDate,
                UPDATEDON = updatedOn,
                UPDATEDBY = updatedBy,
                isVoidAppointmentAssociated = isVoidAppointmentAssociated,
                SchemeId = schemeId,
                BlockId = blockId,                
            };

            saveStatus = savePropertyDateAttribute(propertyDateAttributeToSave, currentContext);

            return saveStatus;
        }

        public bool savePropertyDateAttribute(PA_PROPERTY_ITEM_DATES propertyDateAttributeToSave, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool saveStatus = false;

            var pidQuery = context.PA_PROPERTY_ITEM_DATES.Where(pid => pid.ItemId == propertyDateAttributeToSave.ItemId && pid.ParameterId == propertyDateAttributeToSave.ParameterId && 
                                            (pid.PROPERTYID == propertyDateAttributeToSave.PROPERTYID
                                            || pid.SchemeId == propertyDateAttributeToSave.SchemeId
                                            || pid.BlockId == propertyDateAttributeToSave.BlockId)
                                            );

            if (pidQuery.Count() > 0)
            {
                PA_PROPERTY_ITEM_DATES propertyDateAttribute = pidQuery.First();

                propertyDateAttribute.DueDate = propertyDateAttributeToSave.DueDate;
                propertyDateAttribute.LastDone = propertyDateAttributeToSave.LastDone;
                //propertyDateAttribute.Frequency = propertyDateAttributeToSave.Frequency;
                propertyDateAttribute.isVoidAppointmentAssociated= propertyDateAttributeToSave.isVoidAppointmentAssociated;
                propertyDateAttribute.UPDATEDBY = propertyDateAttributeToSave.UPDATEDBY;
                propertyDateAttribute.UPDATEDON = propertyDateAttributeToSave.UPDATEDON;
                                
                context.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
                propertyDateAttributeToSave.SID = propertyDateAttribute.SID;
            }
            else
            {
                context.PA_PROPERTY_ITEM_DATES.AddObject(propertyDateAttributeToSave);
                context.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            }


            saveStatus = true;
            return saveStatus;
        }

        #endregion

        #region "Get Attribute notes for scheme/block"

        public List<PropertyAttributeData> getAttributeNotesForSchemeBlock(int? schemeId, int? blockId)
        {
            List<PropertyAttributeData> attrNotesList = new List<PropertyAttributeData>();
            var attrNotesQuery = (from nt in context.PA_PROPERTY_ITEM_NOTES
                                  join it in context.PA_ITEM on nt.ItemId equals it.ItemID
                                  join pa in context.PA_AREA on it.AreaID equals pa.AreaID
                                  where it.IsActive == true && nt.ShowOnApp == true
                                  select new 
                                  {
                                     nt = nt,
                                     it = it,
                                     pa = pa
                                  });

            if (schemeId != null && schemeId > 0)
            {
               attrNotesQuery = attrNotesQuery.Where(x => x.nt.SchemeId == schemeId);
            }
            else if (blockId != null && blockId > 0)
            {
                attrNotesQuery = attrNotesQuery.Where(x => x.nt.BlockId == blockId);
            }

            if (attrNotesQuery.Count() > 0)
            {
                foreach (var item in attrNotesQuery)
                {
                    string parentString = string.Empty;
                    if (item.it.ParentItemId != null)
                    {
                        var parentItemQuery = context.PA_ITEM
                                                     .Where(it => it.ItemID == item.it.ParentItemId && it.IsActive == true)
                                                     .FirstOrDefault();
                        if (parentItemQuery != null)
                        {
                            parentString = parentItemQuery.ItemName;
                        }
                    }

                    var arrTitle = new string[] { item.pa.AreaName, parentString,  item.it.ItemName};

                    PropertyAttributeData atr = new PropertyAttributeData();
                    atr.attributeNotes = item.nt.Notes;
                    atr.attributePath = string.Join(" : ", arrTitle.Where(ar=> !string.IsNullOrEmpty(ar)) );
                    attrNotesList.Add(atr);
                }
            }
            return attrNotesList;

        }

        #endregion

    }
}
