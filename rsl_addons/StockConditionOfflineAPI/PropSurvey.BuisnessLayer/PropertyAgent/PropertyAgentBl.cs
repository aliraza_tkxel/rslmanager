﻿using System;
using System.Collections.Generic;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Property;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.PropertyAgent
{
    public class PropertyAgentBl
    {
        #region get all  Property agents
        /// <summary>
        /// This function returns all the property agent list
        /// </summary>
        /// <returns>It retruns the list of property agent's object</returns>
        public List<PropertyAgentData> getAllPropertyAgent()
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                List<PropertyAgentData> agentData = new List<PropertyAgentData>();
                agentData = agentDal.getAllPropertyAgent();
                return agentData;

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save a Property agent
        /// <summary>
        /// This function saves a property agent list
        /// </summary>
        /// <param name="proAgentData">Property agent data's object</param>
        /// <returns>It retruns the list of property agent's object</returns>
        public bool SavePropertyAgent(PropertyAgentData proAgentData)
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                return agentDal.SavePropertyAgent(proAgentData);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region updates a Property agent
        /// <summary>
        /// This function updates a property agent list
        /// </summary>
        /// <param name="proAgentData">Property agent data's object</param>
        /// <returns>It retruns the list of property agent's object</returns>
        public bool UpdatePropertyAgent(PropertyAgentData proAgentData)
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                return agentDal.UpdatePropertyAgent(proAgentData);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get Property Agent to Appointment
        /// <summary>
        /// This function returns Property Agent to Appointment
        /// </summary>
        /// <returns>It retruns Property Agent to Appointment's object</returns>
        public PropertyAgentToAppointmentData getAgentToAppointment(int AppointmentID)
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                return agentDal.getAgentToAppointment(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get Property Agent details
        /// <summary>
        /// This function returns Property Agent details
        /// </summary>
        /// <returns>It retruns Property Agent details</returns>
        public PropertyAgentData getAgentDetails(int AppointmentID)
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                return agentDal.getAgentDetails(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Update a Property Agent to Appointment
        /// <summary>
        /// This function updates a Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdateAgentToAppointment(PropertyAgentToAppointmentData proAgentData)
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                if (agentDal.checkAppointmentID(proAgentData.AppointmentID) && agentDal.checkPropertyAgenID(proAgentData.PropertyAgentID))
                {
                    return agentDal.UpdateAgentToAppointment(proAgentData);    
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyAgentInvalid, true, MessageCodesConstants.PropertyAgentInvalid);
                    throw new ArgumentException(MessageConstants.PropertyAgentInvalid);
                }

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }   
            }
        }
        #endregion

        #region save a Property Agent to Appointment
        /// <summary>
        /// This function saves  Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public int SaveAgentToAppointment(PropertyAgentToAppointmentData proAgentData)
        {
            try
            {
                PropertyAgentDal agentDal = new PropertyAgentDal();
                if (agentDal.checkAppointmentID(proAgentData.AppointmentID) && agentDal.checkPropertyAgenID(proAgentData.PropertyAgentID))
                {
                    return agentDal.SaveAgentToAppointment(proAgentData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyAgentInvalid, true, MessageCodesConstants.PropertyAgentInvalid);
                    throw new ArgumentException(MessageConstants.PropertyAgentInvalid);
                }

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        #endregion

    }
}
