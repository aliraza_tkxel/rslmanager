﻿using System;
using System.Collections.Generic;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Appointment;
using PropSurvey.Dal.Authentication;
using PropSurvey.Dal.Customer;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Dal.Property;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Dal.Appliances;
using PropSurvey.Dal.Faults;

namespace PropSurvey.BuisnessLayer.Appliances
{
    public class AppliancesBl
    {

        #region get Appliances Data
        /// <summary>
        /// This function returns the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        public ResponseTemplate<AppliancesAllData> getAppliancesData()
        {
            try
            {
                ResponseTemplate<AppliancesAllData> objResponseTemplate = new ResponseTemplate<AppliancesAllData>();
                AppliancesDal appDal = new AppliancesDal();
                FaultsDal faultDal = new FaultsDal();
                AppliancesAllData objAppllianceData = new AppliancesAllData();

                objAppllianceData.ApplianceLocation = appDal.getAllApplianceLocations();
                objAppllianceData.ApplianceManufacturer = appDal.getAllApplianceManufacturer();
                objAppllianceData.ApplianceModel = appDal.getAllApplianceModel();
                objAppllianceData.ApplianceType = appDal.getAllApplianceTypes();
                objResponseTemplate.response = objAppllianceData;
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                return objResponseTemplate;

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get All Gas Appliances
        /// <summary>
        /// This function returns all the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        public List<ApplianceData> getAllGasAppliances(ApplianceSurveyDataParam requestParam)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllGasAppliances(requestParam);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Get Boilers By PropertyId
        public List<BoilerData> getAllHeatingBoilers(ApplianceSurveyDataParam requestParam)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllHeatingBoilers(requestParam);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Get Oil Heatings
        public List<OilHeatingData> getOilHeatings(OilSurveyDataParam requestParam)
        {

                AppliancesDal appDal = new AppliancesDal();
                return appDal.getOilHeatings(requestParam);

        }
        #endregion

        #region Get Alternative Heatings
        public List<AlternativeHeatingData> getAlternativeHeatings(AlternativeSurveyDataParam requestParam)
        {
            AppliancesDal appDal = new AppliancesDal();
            return appDal.getAlternativeHeatings(requestParam);
        }
        #endregion

        #region Save a new Appliance
        /// <summary>
        /// This function saves a new appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public ResponseTemplate<ApplianceData> saveAppliance(ApplianceData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                if (appData.ApplianceType != null)
                {

                    appData.ApplianceType.ApplianceTypeID = appDal.saveApplianceType(appData.ApplianceType);

                }
                if (appData.ApplianceLocation != null)
                {
                    appData.ApplianceLocation.LocationID = appDal.saveApplianceLocation(appData.ApplianceLocation);

                }

                if (appData.ApplianceManufacturer != null)
                {
                    appData.ApplianceManufacturer.ManufacturerID = appDal.saveApplianceManufacturer(appData.ApplianceManufacturer);
                }
                if (appData.ApplianceModel != null)
                {
                    appData.ApplianceModel.ApplianceModelID = appDal.saveApplianceModel(appData.ApplianceModel);
                }
                int returnId = appDal.saveAppliance(appData);
                ResponseTemplate<ApplianceData> objResponseTemplate = new ResponseTemplate<ApplianceData>();
                MessageData objMessageData = new MessageData();
                if (returnId > 0)
                {
                    //<Assigning List to template>
                    appData.ApplianceID = returnId;
                    objResponseTemplate.response = appData;
                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;
                }
                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Update a existing Appliance
        /// <summary>
        /// This function updates a existing appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateAppliance(ApplianceData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.updateAppliance(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances Locations
        /// <summary>
        /// This function save new Appliances Location
        /// </summary>
        /// <returns>true for success</returns>
        public int saveApplianceLocation(AppliancesLocationData location)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceLocation(location);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances Type
        /// <summary>
        /// This function save new Appliances Type
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceType(ApplianceTypeData type)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceType(type);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances Model
        /// <summary>
        /// This function save new Appliances Model
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceModel(ApplianceModelData model)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceModel(model);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances manufacturer
        /// <summary>
        /// This function save new Appliances manufacturer
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceManufacturer(ManufacturerData manufacture)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceManufacturer(manufacture);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getInspectedAppliances(appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }



        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionByApplianceID(ApplianceID, appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionByApplianceIDGas(ApplianceID, journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#26 - Behroz - 20/12/2012 - End
        #endregion

        #region get ApplianceInspection details by Appointment Id
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionDetails(appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection details for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionDetailsGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#40 - Behroz - 26/12/2012 - End
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceInspection(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#24 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function saves a new ApplianceInspection for gas
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspectionGas(ApplianceInspectionData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceInspectionGas(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#24 - Behroz - 20/12/2012 - End
        #endregion

        #region Update a existing ApplianceInspection
        /// <summary>
        /// This function updates a existing ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        //public bool updateApplianceInspection(ApplianceInspectionData appData)
        //{
        //    try
        //    {
        //        AppliancesDal appDal = new AppliancesDal();
        //        return appDal.updateApplianceInspection(appData);
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
        //        throw ex;
        //    }
        //}
        #endregion

        #region "Get Appliance Count"
        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns the application count
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceCount(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        #region Update Detector Count

        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>        
        //public ResultBoolData updateDetectorCount( DetectorCountData detectorCountData)
        //{
        //    try
        //    {
        //        AppliancesDal appDal = new AppliancesDal();
        //        return appDal.updateDetectorCount(detectorCountData.propertyId, detectorCountData);
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
        //        throw ex;
        //    }
        //}

        #endregion

        #region Get Detector List
        /// <summary>
        /// Get Detector List
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public List<DetectorCountData> getDetectorList(string propertyId, int appointmentId)
        {
            AppliancesDal appDal = new AppliancesDal();
            FaultsDal faultDal = new FaultsDal();
            List<DetectorCountData> detectorList = new List<DetectorCountData>();
            detectorList = appDal.getDetectorList(propertyId, appointmentId);
            foreach (DetectorCountData detectorData in detectorList)
            {
                detectorData.detectorDefects = faultDal.getDetectorDefectList(detectorData.detectorTypeId, propertyId);
            }
            return detectorList;
        }
        #endregion

        #region Save a new Appliance
        /// <summary>
        /// This function saves a new appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveUpdateOfflineAppliance(ApplianceData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                if (appData.ApplianceType != null)
                {
                    appData.ApplianceType.ApplianceTypeID = appDal.saveApplianceType(appData.ApplianceType);
                }
                if (appData.ApplianceLocation != null)
                {
                    appData.ApplianceLocation.LocationID = appDal.saveApplianceLocation(appData.ApplianceLocation);
                }
                if (appData.ApplianceManufacturer != null)
                {
                    appData.ApplianceManufacturer.ManufacturerID = appDal.saveApplianceManufacturer(appData.ApplianceManufacturer);
                }
                if (appData.ApplianceModel != null)
                {
                    appData.ApplianceModel.ApplianceModelID = appDal.saveApplianceModel(appData.ApplianceModel);
                }
                int returnId = appDal.saveAppliance(appData);
                return returnId;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

    }
}
