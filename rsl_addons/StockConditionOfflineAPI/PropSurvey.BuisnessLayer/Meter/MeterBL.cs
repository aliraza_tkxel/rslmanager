﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal;

namespace PropSurvey.BuisnessLayer
{
    public class MeterBL
    {
        #region Get Gas Meter By PropertyId

        public Meter GetGasMeter(ApplianceSurveyDataParam requestParam)
        {
            MeterDal meterDal = new MeterDal();
            Meter meter = new Meter();
            if (requestParam.propertyId != null)
            {
                meter = meterDal.GetGasMeterByPropertyId(requestParam.propertyId);
            }
            else if (requestParam.schemeId != null && requestParam.schemeId > 0)
            { 
                meter = meterDal.GetGasMeterBySchemeId(requestParam.schemeId);
            }
            else if (requestParam.blockId != null && requestParam.blockId > 0)
            {
                meter = meterDal.GetGasMeterByBlockId(requestParam.blockId);
            }

            return meter;
        }

        #endregion

        #region Get Electic Meter

        public Meter GetElectricMeter(ApplianceSurveyDataParam requestParam)
        {
            MeterDal meterDal = new MeterDal();
            Meter meter = new Meter();
            if (requestParam.propertyId != null)
            {
                meter = meterDal.GetElectricMeterByPropertyId(requestParam.propertyId);
            }
            else if (requestParam.schemeId != null && requestParam.schemeId > 0)
            {
                meter = meterDal.GetElectricMeterBySchemeId(requestParam.schemeId);
            }
            else if (requestParam.blockId != null && requestParam.blockId > 0)
            {
                meter = meterDal.GetElectricMeterByBlockId(requestParam.blockId);
            }

            return meter;
        }

        #endregion

        #region Unused Functions

        #region Get All Meters By Property Id

        //public List<Meter> GetAllMetersByPropertyId(string PropertyId)
        //{
        //    #region Get All Meters By Property Id

        //    MeterDal meterDal = new MeterDal();
        //    return meterDal.GetAllMetersByPropertyId(PropertyId);

        //    #endregion
        //}

        #endregion

        #endregion
    }
}
