﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Faults;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Dal.Appliances;

namespace PropSurvey.BuisnessLayer.Faults
{
    public class FaultsBl
    {

        #region get Faults Data
        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>

        public List<FaultsData> getFaultsData(int AppointmentID)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsData(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion
        #region Fetch Gas defects
        /// <summary>
        /// Fetch gas defects api
        /// </summary>
        /// <param name="journalId">accept a journalId as parameter</param>
        /// <returns></returns>
        public ResponseTemplate<GasDefectsData> fetchGasDefects(RequestGasDefectsData requestData)
        {
            try
            {
                GasDefectsData gasDefect = new GasDefectsData();
                FaultsDal faultDal = new FaultsDal();
                AppliancesDal appDal = new AppliancesDal();

                // gasDefect.appliancesInspected = appDal.getInspectedAppliancesGas(journalId);
                gasDefect.defects = faultDal.fetchFaultsDataGas(requestData.journalId);
                gasDefect.defectCategories = faultDal.getAllDefectCategories();
                gasDefect.detectorDefects = faultDal.fetchDetectorDefectList(requestData.journalId);  
                ResponseTemplate<GasDefectsData> objResponseTemplate = new ResponseTemplate<GasDefectsData>();
                MessageData objMessageData = new MessageData();
                objResponseTemplate.response = gasDefect;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get Faults Data Gas
        ///// <summary>
        ///// This function returns the Faults data for gas
        ///// </summary>
        ///// <returns>List of Faults data objects</returns>
        //public List<FaultsDataGas> getFaultsDataGas(int journalId)
        //{
        //    try
        //    {
        //        FaultsDal faultDal = new FaultsDal();
        //        return faultDal.getFaultsDataGas(journalId);
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
        //        throw ex;
        //    }
        //}


        #endregion

        #region get General Comments
        /// <summary>
        /// This function returns General Comments
        /// </summary>
        /// <returns>List of General Comments</returns>

        public List<FaultsData> getGeneralComments(int AppointmentID)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getGeneralComments(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns General Comments for gas
        /// </summary>
        /// <returns>List of General Comments</returns>
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getGeneralCommentsGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#21 - Behroz - 19/12/2012 - End

        #endregion

        #region get Faults only.
        /// <summary>
        /// This function returns Faults only.
        /// </summary>
        /// <returns>List of Faults.</returns>

        public List<FaultsData> getFaultsOnly(int AppointmentID)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsOnly(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns Faults only for gas
        /// </summary>
        /// <returns>List of Faults.</returns>
        public List<FaultsDataGas> getFaultsOnlyGas(int journalId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsOnlyGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#22 - Behroz - 19/12/2012 - End

        #endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        public int saveFaultsData(FaultsData faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentID(faultData.AppointmentID))
                {
                    return faultDal.saveFaultsData(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        #endregion
        #region save Faults Data Gas
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>response template with FaultsDataGas </returns>
        public ResponseTemplate<FaultsDataGas> saveFaultsDataGas(FaultsDataGas faultData,String username = null)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentIDGas(faultData.JournalId.Value))
                {
                    faultData.ID = faultDal.updateFaultDataGas(faultData,null, username);
                    ResponseTemplate<FaultsDataGas> objResponseTemplate = new ResponseTemplate<FaultsDataGas>();
                    MessageData objMessageData = new MessageData();
                    objResponseTemplate.response = faultData;
                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;

                    return objResponseTemplate;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }


        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateFaultData(FaultsData faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentID(faultData.AppointmentID))
                {
                    return faultDal.updateFaultData(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change#20 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public int  updateFaultDataGas(FaultsDataGas faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentIDGas(faultData.JournalId.Value))
                {
                    return faultDal.updateFaultDataGas(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change#20 - Behroz - 19/12/2012 - End

        #endregion

        #region "Get all Defect Categories"
        //Change#15 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of Defect Category Data</returns>
        public List<DefectCategoryData> getDefectCategories()
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getAllDefectCategories();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#15 - Behroz - 12/17/2012 - End
        #endregion

        #region "Get Fault Id"
        //Change#51 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns fault id
        /// </summary>
        /// <returns></returns>
        public int getFaultId(string propertyId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultId(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#51 - Behroz - 16/01/2013 - End
        #endregion

        #region "Delete Fault Id"
      
        /// <summary>
        /// This function returns fault id
        /// </summary>
        /// <returns></returns>
        public bool deleteFault(int faultId, string propertyId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.deleteFault(faultId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion


        #region Save Property Defect Image

        public int saveFaultImage(string fileName, int faultId, bool? isBeforeImage = null, string imageIdentifier = "")
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.saveFaultImageData(fileName, faultId, isBeforeImage, imageIdentifier);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        #endregion

        #region Delete Fault Image

        /// <summary>
        /// This function returns faultImageData
        /// </summary>
        /// <returns></returns>
        public FaultImagesData deleteFaultImage(int faultId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.deleteFaultImage(faultId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region Validate Fault Data
        
        /// <summary>
        /// This function validate Faults Data for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>
        public void validateFaultsData(FaultsDataGas insData)
        {
            FaultsDataGas validate = new FaultsDataGas();
            FaultsDal faultDal = new FaultsDal();

            string faultCategory = faultDal.validateFaultsCategories(insData.faultCategory);
            if (faultCategory == string.Empty)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidFaultCategory, true, MessageCodesConstants.InvalidFaultCategory);
                throw new ArgumentException(string.Format(MessageConstants.InvalidFaultCategory, faultCategory));
            }
        }
        
        /// <summary>
        /// This function validate fault id
        /// </summary>
        /// <param name="appData"></param>
        /// <returns>true if successfully validate otherwise 0 </returns>
        public void validateFaultsId(int faultId)
        {
            FaultsDataGas validate = new FaultsDataGas();
            FaultsDal faultDal = new FaultsDal();

            bool faultExist = faultDal.validateFaultsId(faultId);
            if (!faultExist)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidFaultId, true, MessageCodesConstants.InvalidFaultId);
                throw new ArgumentException(string.Format(MessageConstants.InvalidFaultId));
            }
        }
        
        #endregion
        
        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair </returns>
        public ResponseTemplate<ResponseFaultRepairData> getFaultRepairList(int faultLogId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                ResponseFaultRepairData responseData = new ResponseFaultRepairData();
                responseData.faultRepairList = faultDal.getFaultRepairList(faultLogId);

                ResponseTemplate<ResponseFaultRepairData> objResponseTemplate = new ResponseTemplate<ResponseFaultRepairData>();
                MessageData objMessageData = new MessageData();
                objResponseTemplate.response = responseData;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        
        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        public ResponseTemplate<ResponsePauseReasonList> getPauseReasonList()
        {
            try
            {
                ResponsePauseReasonList pausedList = new ResponsePauseReasonList();
                FaultsDal faultDal = new FaultsDal();
                pausedList.pauseReasonList = faultDal.getPauseReasonList();

                ResponseTemplate<ResponsePauseReasonList> objResponseTemplate = new ResponseTemplate<ResponsePauseReasonList>();
                MessageData objMessageData = new MessageData();
                objResponseTemplate.response = pausedList;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        
        #endregion

        #region "Save Fault Repair Image"
        public int saveFaultRepairImage(string PropertyId, int schemeId, int blockId, string jsNumber, string imageName, bool isBeforeImage, int createdBy, string ImageType = "", string imageIdentifier = "")
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.saveFaultRepairImage(PropertyId, schemeId, blockId, jsNumber, imageName, isBeforeImage, createdBy, ImageType, imageIdentifier);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        #endregion

        #region is Fault Repair Image already exists

        /// <summary>
        /// check if fault repair Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="faultRepairImageId"></param>
        public bool isFaultRepairImageAlreadyExists(string imageIdentifier, out string imageName, out int faultRepairImageId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.isFaultRepairImageAlreadyExists(imageIdentifier, out imageName, out faultRepairImageId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        #endregion

        #region is Property Defect Image already exists

        /// <summary>
        /// check if property defect Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="faultRepairImageId"></param>
        public bool ispropertyDefectImageAlreadyExists(string imageIdentifier, out string imageName, out int propertyDefectImageId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.ispropertyDefectImageAlreadyExists(imageIdentifier, out imageName, out propertyDefectImageId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        #endregion
    }
}
