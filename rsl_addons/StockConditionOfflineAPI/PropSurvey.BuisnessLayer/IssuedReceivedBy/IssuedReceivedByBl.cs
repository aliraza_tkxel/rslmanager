﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.IssuedReceivedBy;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Dal.Authentication;
using System.IO;

namespace PropSurvey.BuisnessLayer.IssuedReceivedBy
{
    public class IssuedReceivedByBl
    {

        #region get IssuedReceivedBy Data
        /// <summary>
        /// This function returns the IssuedReceivedBy data
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>

        public IssuedReceivedByData getIssuedReceivedByFormData(int AppointmentID)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();
                return insDal.getIssuedReceivedByFormData(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#34 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns the IssuedReceivedBy data
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        public LGSRData getIssuedReceivedByFormDataGas(int journalId)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();
                return insDal.getIssuedReceivedByFormDataGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#34 - Behroz - 12/17/2012 - End
        #endregion

        #region save IssuedReceivedBy Data
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>

        public int saveIssuedReceivedByData(IssuedReceivedByData workData)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();
                if (insDal.checkAppointmentID(workData.AppointmentID))
                {
                    return insDal.saveIssuedReceivedBy(workData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>
        public int saveIssuedReceivedByData(LGSRData workData)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();

                if (insDal.checkAppointmentIDGas(workData.propertyId))
                {
                    //The iPhone app send the Employee id and we need to save the userId from AS_USER table
                    AuthenticationDal authDal = new AuthenticationDal();
                    workData.IssuedBy = authDal.GetUserId(workData.IssuedBy.Value);

                    return insDal.saveIssuedReceivedBy(workData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalidGas, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalidGas);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change - Behroz - 19/12/2012 - End
        #endregion

        #region update IssuedReceivedBy Data
        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateIssuedReceivedByData(IssuedReceivedByData insData)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();
                if (insDal.checkAppointmentID(insData.AppointmentID))
                {
                    return insDal.updateIssuedReceivedByData(insData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change#35 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateIssuedReceivedByData(LGSRData insData)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();
                
                if (insDal.checkAppointmentIDGas(insData.propertyId))
                {
                    AuthenticationDal authDal = new AuthenticationDal();
                    insData.IssuedBy = authDal.GetUserId(insData.IssuedBy.Value);

                    return insDal.updateIssuedReceivedByData(insData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, MessageCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// This function saves the CP12 Document in database
        /// </summary>
        /// <param name="documentStream"></param>
        /// <param name="LGSRId"></param>
        /// <returns></returns>
        public bool updateCP12Document(Stream documentStream, int LGSRId, bool tenantHanded)
        {
            try
            {
                IssuedReceivedByDal insDal = new IssuedReceivedByDal();
                bool result = insDal.updateCP12Document(documentStream, LGSRId, tenantHanded);
                
                if (result)
                    return true;
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.LGSRIdInvalid, true, MessageCodesConstants.LGSRIdInvalid);
                    throw new ArgumentException(MessageConstants.LGSRIdInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change#35 - Behroz - 12/17/2012 - End
        #endregion

    }
}
