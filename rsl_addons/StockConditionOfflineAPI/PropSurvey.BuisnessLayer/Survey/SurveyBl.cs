﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Survey;
using System.Data;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;


namespace PropSurvey.BuisnessLayer.Survey
{
    public class SurveyBl
    {
        #region save Survey Form

        /// <summary>
        /// This function save the survey form. Data should be post at specific URL. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns true false</returns>
        public bool saveSurveyForm(SurveyData survData)
        {
            try
            {
                SurveyDal survDal = new SurveyDal();

                string propertyId = survData.propertyId;
                int customerId = survData.customerId;
                int appointmentId = survData.appointmentId;
                bool success = false;

                bool isAppointmentExist = survDal.isAppointmentForPropertyExist(propertyId, appointmentId);

                //if appointment does not exist then this is error
                if (isAppointmentExist == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, true, MessageCodesConstants.PropertyOrCustomerOrAppointmentDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, propertyId, customerId.ToString(), appointmentId.ToString()), "propertyId or customerId or appointmentId");
                }
                else
                {

                    success = survDal.saveSurveyForm(survData);
                }


                return success;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }
        #endregion

        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>

        public List<SurveyData> getAllSavedSurveyForms(int appointmentId)
        {
            //fetch all the survey forms against appointment id
            try
            {
                SurveyDal survDal = new SurveyDal();
                return survDal.getAllSavedSurveyForms(appointmentId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get survey Id by appointment id
        public int getSurveyId(int appointmentId)
        {
            SurveyDal survDal = new SurveyDal();
            return survDal.getSurveyId(appointmentId);
        }
        #endregion

    }
}
