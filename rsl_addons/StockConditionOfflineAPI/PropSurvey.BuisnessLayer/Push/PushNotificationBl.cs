﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Constants;
using PropSurvey.Dal.Push;

namespace PropSurvey.BuisnessLayer.Push
{
    public class PushNotificationBl
    {
        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotificationFault(int appointmentId, int type, string serverinUse, bool isSchemeBlockAppointment = false)
        {
            try
            {
                PushNotificationDal pushNotificationDal = new PushNotificationDal();
                return pushNotificationDal.sendPushNotificationFault(appointmentId, type, serverinUse, isSchemeBlockAppointment);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotificationDocumentExpiary()
        {
            try
            {
                PushNotificationDal pushNotificationDal = new PushNotificationDal();
                return pushNotificationDal.sendPushNotificationDocumentExpiary();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region send Push Notification for Appliances and Planned Appointment(s)
        /// <summary>
        /// This function will send push notification for Appliances and Planned Appointment(s)
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotification(string appointmentMessage, int operatorId, string hostURL)
        {
            try
            {
                PushNotificationDal pushNotificationDal = new PushNotificationDal();
                return pushNotificationDal.sendPushNotification(appointmentMessage, operatorId, hostURL);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
        }
        #endregion

        #region Send push notification test function
        /// <summary>
        /// Just to test push notification to an iOS device
        /// </summary>
        /// <param name="deviceToken"></param>
        /// <param name="certificateFilepath"></param>
        /// <param name="useSandBox"></param>
        /// <param name="pushMessage"></param>
        /// <param name="certificatePassword">Set an optional push notification password.</param>
        /// <returns></returns>
        public string sendPushNotification(string deviceToken, string certificateFilepath, bool useSandBox, string pushMessage, string certificatePassword)
        {
            return new PushNotificationDal().sendPushNotification(deviceToken, certificateFilepath, useSandBox, pushMessage, certificatePassword);
        }
        #endregion

        #region

        public bool sendOverduePushNotification()
        {
            bool status = false;
            try
            {
                status = new PushNotificationDal().sendOverduePushNotification();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
            return status;
        }

        #endregion

    }
}
