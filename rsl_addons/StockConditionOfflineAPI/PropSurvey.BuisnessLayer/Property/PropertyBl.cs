﻿using System;
using System.Collections.Generic;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Property;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.Property
{
    public class PropertyBl
    {
        #region find Property
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sure name of customer</param>
        /// <returns>It returns the list of customer data's object</returns>
        public ResponseTemplate<PropertiesListData> findProperty(PropertySearchParam varPropertySearchParam)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                ResponseTemplate<PropertiesListData> objResponseTemplate = new ResponseTemplate<PropertiesListData>();

                objResponseTemplate.response = propDal.findProperty(varPropertySearchParam);
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                return objResponseTemplate;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SkipTopInvalidFromtMsg, true, MessageCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findPropertyGas(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                List<CustomerData> custData = new List<CustomerData>();

                //convert skip to integer
                int skipNumber = 0;
                int topNumber = 0;

                skipNumber = Int32.Parse(skip);
                topNumber = Int32.Parse(top);

                custData = propDal.findPropertyGas(skipNumber, topNumber, reference, houseNumber, street, postCode, surName);

                return custData;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SkipTopInvalidFromtMsg, true, MessageCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get Property Images

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>returns the list of image names against property</returns>

        public List<PropertyPictureData> getPropertyImages(string propertyId, int itemId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                List<PropertyPictureData> propData = new List<PropertyPictureData>();

                propData = propDal.getPropertyImages(propertyId, itemId);

                return propData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get Default Property Image Name

        /// <summary>
        /// This function returns the default image name which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>returns the name of the image</returns>

        public string getDefaultPropertyImageName(string propertyId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.getDefaultPropertyImageName(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save Property Image

        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="propPicData">property picture data</param>        
        /// <returns>true or false upon successfull saving on disk and on db</returns>

        public int savePropertyImage(PropertyPictureData propPicData)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.savePropertyImage(propPicData);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
        }


        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="propPicData">property picture data</param>        
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public int savePropertyImageGas(PropertyPictureData propPicData)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.savePropertyImageGas(propPicData);


            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
        }

        public int getItemId()
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.getItemId();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }


        #endregion

        #region delete Property Image
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>property picture data which is deleted</returns>

        public PropertyPictureData deletePropertyImage(int propertyPictureId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                PropertyPictureData propPicData = new PropertyPictureData();

                propPicData = propDal.deletePropertyImage(propertyPictureId);

                return propPicData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>List of property dimension data object</returns>

        public List<PropertyDimData> getPropertyDimensions(string propertyId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.getPropertyDimensions(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region update Property Dimensions

        /// <summary>
        /// This function updates the property dimensions
        /// </summary>
        /// <param name="propDimData">This function accepts the property dimension data object</param>
        /// <returns>true on successful update and false on un successful update</returns>

        //public bool updatePropertyDimensions(List<PropertyDimData> propertyDimBo)
        //{
        //    try
        //    {                
        //        PropertyDal propertyDal = new PropertyDal();
        //        return propertyDal.updatePropertyDimensions(propertyDimBo);

        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
        //        throw ex;
        //    }
        //}
        #endregion

        #region is Property Appointment Exist

        /// <summary>
        /// This function checks that either appointment exist against this property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>true / false</returns>
        public void isPropertyAppointmentExist(string propertyId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();

                //varifies property id exists in database against any appointment
                if (propDal.isPropertyAppointmentExist(propertyId) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyIdDoesNotExistMsg, propertyId), true, MessageCodesConstants.PropertyIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.PropertyIdDoesNotExistMsg, propertyId), "propertyId");
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw;
                }
            }
        }

        /// <summary>
        /// This function checks that either appointment exist against this property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>true / false</returns>
        public void isPropertyAppointmentExist(int appointmentId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();

                //varifies property id exists in database against any appointment
                if (propDal.isPropertyAppointmentExist(appointmentId) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyIdDoesNotExistMsgGas, appointmentId), true, MessageCodesConstants.AppointmentIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.PropertyIdDoesNotExistMsgGas, appointmentId), "appointmentId");
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        #endregion

        #region Get Property Asbestos Risk

        /// <summary>
        /// This function returns the list of asbestos risk against the property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>the list of asbestos risks</returns>
        public List<AsbestosData> getPropertyAsbestosRisk(string propertyId)
        {
            try
            {
                PropertyDal propertyDal = new PropertyDal();
                return propertyDal.getPropertyAsbestosRisk(propertyId);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region Update Property Default Image

        /// <summary>
        /// This function updates the property default image
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property  data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public int updatePropertyDefaultImage(string PropertyId, int PropertyPicId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.updatePropertyDefaultImage(PropertyId, PropertyPicId);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
        }

        #endregion

        #region Get Picture Name By Property Picture Id

        /// <summary>
        /// This function updates the property default image
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property  data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public string GetPicNameByPropertyPicId(int PropertyPicId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.GetPicNameByPropertyPicId(PropertyPicId);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
        }

        #endregion

        #region is Property Image already exists

        /// <summary>
        /// check if property Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="propertyPicId"></param>
        public bool isPropertyImageAlreadyExists(string imageIdentifier, out string imageName, out int propertyPicId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.isPropertyImageAlreadyExists(imageIdentifier, out imageName, out propertyPicId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
        }

        #endregion

        #region Is Heating Inspection Image already exists

        /// <summary>
        /// Is Inspection Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="inspectionImageId"></param>
        public bool isHeatingInspectionImageAlreadyExists(string imageIdentifier, out string imageName, out int inspectionImageId)
        {
            PropertyDal propDal = new PropertyDal();
            return propDal.isHeatingInspectionImageAlreadyExists(imageIdentifier, out imageName, out inspectionImageId);
        }

        #endregion

        #region "Validate Heating Id"
        /// <summary>
        /// Validate Heating Id
        /// </summary>
        /// <param name="heatingId"></param>
        /// <returns></returns>
        public bool validateHeatingId(int heatingId)
        {
            PropertyDal propDal = new PropertyDal();
            return propDal.validateHeatingId(heatingId);
        }

        #endregion

        #region Save Heating Inspection Image Data

        /// <summary>
        /// Save Heating Inspection Image Data
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="heatingId"></param>
        /// <param name="journalId"></param>
        /// <param name="imageIdentifier"></param>
        /// <returns></returns>
        public int saveHeatingInspectionImageData(PropertyPictureData propPicData)
        {
            PropertyDal propDal = new PropertyDal();
            return propDal.saveHeatingInspectionImageData(propPicData);
        }

        #endregion

    }
}
