﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Appointment;
using PropSurvey.Dal.Authentication;
using PropSurvey.Dal.Customer;
using PropSurvey.Dal.Property;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Contracts.Data.Appointment;
using PropSurvey.Dal.Survey;
using System.Text;
using System.Web;
using System.IO;
using PropSurvey.Dal.Appliances;
using PropSurvey.Dal.InstallationPipework;
using PropSurvey.Dal.Faults;
using PropSurvey.BuisnessLayer.Survey;
using PropSurvey.BuisnessLayer.Appliances;
using System.Transactions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.Contracts.Data.Void;
using System.Reflection;
using System.Runtime.Serialization;
using PropSurvey.Contracts.Data.Stock;
using PropSurvey.Dal;
using PropSurvey.Contracts.Data.StockAPIParameters;

namespace PropSurvey.BuisnessLayer.Appointment
{
    public class AppointmentBl
    {
        #region get All Appointments By Application Type

        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accomodations','Surveyors List' and 'Customer List'.
        /// Code at <01-Aug-2013>
        /// </summary>
        /// <returns>
        /// List of appointments,Property Info,Accomodations,Surveyors List and Customer List, data objects
        /// </returns>
        public List<IAppointment> getAllAppointmentsByAppType(AppointmentParam varAppointmentParam, ref CommonData commonData, List<ExistingAppointmentParam> existingAppointments, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            try
            {
                //Parsing User info {username,salt,application type}
                string[] arryUserInfo = varAppointmentParam.userinfo.Split(',');
                List<IAppointment> objAppointments = new List<IAppointment>();

                AppointmentDal apptDal = new AppointmentDal();

                #region Get Gas (Appliance Servicing) Appointments 

                List<AllAppointmentsList> objAppointmentListGas = apptDal.getAllAppointmentsGas(arryUserInfo[0], varAppointmentParam.startdate, varAppointmentParam.enddate, existingAppointments, ref deleteAppointment);
                 objAppointments = objAppointments.Union(objAppointmentListGas).ToList();

                #endregion

                #region Get Fault (Rective Repair) Apppointments

                //Get Fault appointments and merge with stock and Gas appointments.
                List<AllAppointmentsList> objAppointmentListFault = apptDal.getAllFaultAppointmentsByUser(arryUserInfo[0], varAppointmentParam.startdate, varAppointmentParam.enddate);
                //Combining the Appointments {Stock, Gas and Fault Locator}
                // objAppointments = objAppointments.Union(objAppointmentListFault).ToList();

                //Get Scheme Block Fault Appointments.
                List<AllAppointmentsList> objAppointmentListSchemeBlockFault = apptDal.getAllSchemeBlockFaultAppointmentsByUser(arryUserInfo[0], varAppointmentParam.startdate, varAppointmentParam.enddate);
                //Combining the Appointments {Stock, Gas and Fault Locator}
                objAppointmentListFault = objAppointmentListFault.Union(objAppointmentListSchemeBlockFault).ToList();
                List<AllAppointmentsList> faultAppointmentList = filterFaultAppointment(objAppointmentListFault, existingAppointments, ref deleteAppointment);
                objAppointments = objAppointments.Union(faultAppointmentList).ToList();

                #endregion

                #region Get Planned Apppointments

                //Get Planned appointments .
                List<AllAppointmentsList> objAppointmentListPlanned = apptDal.getAllPlannedAppointments(arryUserInfo[0], varAppointmentParam.startdate, varAppointmentParam.enddate, existingAppointments, ref deleteAppointment);
                //Combining the Appointments {Stock, Gas  Fault and Planned appointment}
                objAppointments = objAppointments.Union(objAppointmentListPlanned).ToList();

                #endregion

                #region Get Void Apppointments

                //Get Void Appointments
                List<VoidAppointmentData> objAppointmentListVoid = apptDal.getAllVoidAppointments(arryUserInfo[0], varAppointmentParam.startdate, varAppointmentParam.enddate, existingAppointments, ref deleteAppointment);

                // Get Void Inspection and Post Void Inspection Appointments
                List<VoidInspectionAppointmentData> objVoidInspectionAppointmentList = getVoidInspectionAppointments(objAppointmentListVoid, "Inspection", ref commonData);
                objAppointments = objAppointments.Union(objVoidInspectionAppointmentList).ToList();

                //Get  Void Gas Appointments
                List<VoidGasElectricData> objGasCheckList = getGasElectricAppointments(objAppointmentListVoid, "Gas", ref commonData);
                objAppointments = objAppointments.Union(objGasCheckList).ToList();

                //Get Void Electric  Appointments
                List<VoidGasElectricData> objElectricCheckList = getGasElectricAppointments(objAppointmentListVoid, "Electric", ref commonData);
                objAppointments = objAppointments.Union(objElectricCheckList).ToList();

                //Get Void Works Appointments 
                List<VoidWorksAppointmentData> objVoidWorksAppointmentList = getVoidWorkAppointments(objAppointmentListVoid, "VoidWorks");
                objAppointments = objAppointments.Union(objVoidWorksAppointmentList).ToList();

                #endregion

                #region Get Defect Apppointments

                // Get Defect Appointments
                List<ApplianceDefectAppointmentData> objAppllianceDefectAppointments = apptDal.GetApplianceDefectAppointments(arryUserInfo[0], varAppointmentParam.startdate, varAppointmentParam.enddate, existingAppointments, ref deleteAppointment);
                objAppointments = objAppointments.Union(objAppllianceDefectAppointments).ToList();

                #endregion

                #region Get Common Data

                // ========================================================================================
                // Get Common Data

                commonData.abortReasons = apptDal.getGasAbortReasons();
                if (objElectricCheckList.Count() > 0 || objAppointmentListGas.Count > 0)
                {
                    //get Electric Meter Type List
                    commonData.electricMeterType = apptDal.getElectricMeterTypes();

                }
                else
                {
                    commonData.electricMeterType = null;
                }

                if (objGasCheckList.Count() > 0 || objAppointmentListGas.Count > 0)
                {
                    //get Gas Meter Types List 
                    commonData.gasMeterType = apptDal.getGasMeterTypes();
                }
                else
                {
                    commonData.gasMeterType = null;
                }

                if (objAppointmentListGas.Count > 0)
                {
                    PowerTypeDal powerTypeDal = new PowerTypeDal();
                    commonData.powerTypes = powerTypeDal.getPowerSourceTypes();

                    DetectorDal detectorDal = new DetectorDal();
                    commonData.detectorTypes = detectorDal.getDetectorTypes();


                }

                if (objAppointmentListGas.Count > 0 || objAppllianceDefectAppointments.Count > 0)
                {
                    AppliancesDal appDal = new AppliancesDal();
                    commonData.partsOrderedBy = appDal.getEmployeesListForPartsOrderedBy();
                    commonData.trades = appDal.getAllTrades();
                    commonData.defectsPriority = appDal.getDefectPriorities();
                    commonData.boilerTypes = appDal.getBoilerTypes();
                    commonData.boilerManufacturers = appDal.getBoilerManufacturers();
                    commonData.mainsGasFlueTypes = appDal.getMainsGasFlueTypes();
                    commonData.oilFlueTypes = appDal.getOilFlueTypes();
                    commonData.oilFuelTypes = appDal.getOilFuelTypes();
                    commonData.tankTypes = appDal.getTankTypes();
                    commonData.burnerTypes = appDal.getBurnerTypes();
                    commonData.solarTypes = appDal.getSolarTypes();

                    FaultsDal faultDal = new FaultsDal();
                    commonData.defectCategories = faultDal.getAllDefectCategories();
                    commonData.jobPauseReasonList = faultDal.getPauseReasonList();
                }
                //if fault appointment exist then get faultDetailList to create fault in fault basket
                if (objAppointmentListFault.Count() > 0 || objVoidInspectionAppointmentList.Count > 0)
                {
                    commonData.faultsDetailList = apptDal.getFaultDetailList();
                    commonData.faultAreaList = apptDal.getFaultAreaList();
                }
                // ========================================================================================

                #endregion


                return objAppointments;
            }
            catch (EntityException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw;
            }
            catch (Exception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
        }

        #region Get Void Work Appointments

        private List<VoidWorksAppointmentData> getVoidWorkAppointments(List<VoidAppointmentData> objAppointmentListVoid, string type)
        {
            List<VoidWorksAppointmentData> objVoidWorksAppointments = new List<VoidWorksAppointmentData>();
            var appointmentList = (from appts in objAppointmentListVoid
                                   where appts.appointmentType.Replace(" ", "").Contains("VoidWorks")
                                   select new VoidWorksAppointmentData
                                   {
                                       appointmentId = appts.appointmentId,
                                       tenancyId = appts.tenancyId,
                                       journalId = appts.journalId,
                                       journalHistoryId = appts.journalHistoryId,
                                       appointmentDate = appts.appointmentDate,
                                       appointmentStartTimeString = appts.appointmentStartTimeString,
                                       appointmentEndTimeString = appts.appointmentEndTimeString,
                                       appointmentEndDate = appts.appointmentEndDate,
                                       surveyorUserName = appts.surveyorUserName,
                                       loggedDate = appts.loggedDate,
                                       creationDate = appts.creationDate,
                                       createdBy = appts.createdBy,
                                       appointmentNotes = appts.appointmentNotes,
                                       appointmentStatus = appts.appointmentStatus,
                                       appointmentType = appts.appointmentType,
                                       jsvNumber = appts.jsvNumber,
                                       createdByPerson = appts.createdByPerson,
                                       appointmentStartDateTime = appts.appointmentStartDateTime,
                                       appointmentEndDateTime = appts.appointmentEndDateTime,
                                       journal = appts.journal,
                                       customerList = appts.customerList,
                                       property = appts.property,
                                       reletDate = appts.reletDate,
                                       terminationDate = appts.terminationDate,
                                       //voidWorkDetails = appts.voidWorkDetails,
                                       //voidWorkNotes = appts.voidWorkNotes,
                                       duration = appts.duration,
                                       defaultCustomerId = appts.defaultCustomerId,
                                       defaultCustomerIndex = appts.defaultCustomerIndex,
                                       jobDataList = appts.voidWorksRequired.recordWorks

                                   });

            if (appointmentList.Count() > 0)
            {
                objVoidWorksAppointments = new List<VoidWorksAppointmentData>(appointmentList.ToList());
            }

            return objVoidWorksAppointments;
        }

        #endregion

        #region Get Gas/Electric Appointments

        private List<VoidGasElectricData> getGasElectricAppointments(List<VoidAppointmentData> objAppointmentListVoid, string type, ref CommonData commonData)
        {
            List<VoidGasElectricData> objGasAppointments = new List<VoidGasElectricData>();

            var appointmentList = (from appts in objAppointmentListVoid
                                   where appts.appointmentType.Contains(type)
                                   select new VoidGasElectricData
                                   {
                                       appointmentId = appts.appointmentId,
                                       journalId = appts.journalId,
                                       journalHistoryId = appts.journalHistoryId,
                                       tenancyId = appts.tenancyId,
                                       appointmentDate = appts.appointmentDate,
                                       appointmentStartTimeString = appts.appointmentStartTimeString,
                                       appointmentEndTimeString = appts.appointmentEndTimeString,
                                       appointmentEndDate = appts.appointmentEndDate,
                                       surveyorUserName = appts.surveyorUserName,
                                       loggedDate = appts.loggedDate,
                                       creationDate = appts.creationDate,
                                       createdBy = appts.createdBy,
                                       appointmentNotes = appts.appointmentNotes,
                                       appointmentStatus = appts.appointmentStatus,
                                       appointmentType = appts.appointmentType,
                                       jsvNumber = appts.jsvNumber,
                                       createdByPerson = appts.createdByPerson,
                                       appointmentStartDateTime = appts.appointmentStartDateTime,
                                       appointmentEndDateTime = appts.appointmentEndDateTime,
                                       journal = appts.journal,
                                       customerList = appts.customerList,
                                       property = appts.property,
                                       reletDate = appts.reletDate,
                                       terminationDate = appts.terminationDate,
                                       meterLocation = appts.electricGasData.gasMeterLocation,
                                       meterReading = appts.electricGasData.gasMeterReading,
                                       meterTypeId = appts.electricGasData.gasMeterTypeId,
                                       tenantType = string.Empty
                                   });

            if (appointmentList.Count() > 0)
            {
                objGasAppointments = new List<VoidGasElectricData>(appointmentList.ToList());
                AppointmentDal apptDal = new AppointmentDal();
                foreach (VoidGasElectricData voidGData in objGasAppointments)
                {
                    if (voidGData.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidGasCheck.ToString())
                    {
                        voidGData.deviceType = "Gas";
                        if (commonData.gasMeterType.Count() == 0)
                        {
                            //get Gas Meter Types List 
                            commonData.gasMeterType = apptDal.getGasMeterTypes();
                        }
                    }
                    else if (voidGData.appointmentType.Replace(" ", "") == VoidAppointmentTypes.VoidElectricCheck.ToString())
                    {
                        voidGData.deviceType = "Electric";
                        if (commonData.electricMeterType.Count() == 0)
                        {
                            //get Electric Meter Type List
                            commonData.electricMeterType = apptDal.getElectricMeterTypes();
                        }
                    }
                }
            }

            return objGasAppointments;
        }

        #endregion

        #region "Get Void Inspection Appointments"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objAppointmentListVoid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private List<VoidInspectionAppointmentData> getVoidInspectionAppointments(List<VoidAppointmentData> objAppointmentListVoid, string type, ref CommonData commonData)
        {
            List<VoidInspectionAppointmentData> objVoidInspectionData = new List<VoidInspectionAppointmentData>();
            var appointmentList = (from appts in objAppointmentListVoid
                                   where appts.appointmentType.Contains("Inspection")
                                   select new VoidInspectionAppointmentData
                                     {
                                         appointmentId = appts.appointmentId,
                                         tenancyId = appts.tenancyId,
                                         journalId = appts.journalId,
                                         journalHistoryId = appts.journalHistoryId,
                                         appointmentDate = appts.appointmentDate,
                                         appointmentStartTimeString = appts.appointmentStartTimeString,
                                         appointmentEndTimeString = appts.appointmentEndTimeString,
                                         appointmentEndDate = appts.appointmentEndDate,
                                         surveyorUserName = appts.surveyorUserName,
                                         loggedDate = appts.loggedDate,
                                         creationDate = appts.creationDate,
                                         createdBy = appts.createdBy,
                                         appointmentNotes = appts.appointmentNotes,
                                         appointmentStatus = appts.appointmentStatus,
                                         appointmentType = appts.appointmentType,
                                         jsvNumber = appts.jsvNumber,
                                         createdByPerson = appts.createdByPerson,
                                         appointmentStartDateTime = appts.appointmentStartDateTime,
                                         appointmentEndDateTime = appts.appointmentEndDateTime,
                                         isGasCheckRequired = appts.isGasCheckRequired,
                                         isElectricCheckRequired = appts.isElectricCheckRequired,
                                         isEPCCheckRequired = appts.isEPCCheckRequired,
                                         isAsbestosCheckRequired = appts.isAsbestosCheckRequired,
                                         journal = appts.journal,
                                         property = appts.property,
                                         reletDate = appts.reletDate,
                                         terminationDate = appts.terminationDate,
                                         defaultCustomerId = appts.defaultCustomerId,
                                         gasCheckDate = appts.gasCheckDate,
                                         gasCheckStatus = appts.gasCheckStatus,
                                         electricCheckDate = appts.electricCheckDate,
                                         electricCheckStatus = appts.electricCheckStatus,
                                         worksRequired = new VoidRequiredWorks
                                         {
                                             isWorksRequired = appts.worksRequired.isWorksRequired,
                                             recordWorks = appts.worksRequired.recordWorks
                                         },
                                         majorWorksRequired = new VoidMajorWorksRequired
                                         {
                                             isMajorWorkRequired = appts.majorWorksRequired.isMajorWorkRequired,
                                             propertyComponents = appts.majorWorksRequired.propertyComponents,
                                             recordMajorWork = appts.majorWorksRequired.recordMajorWork

                                         },
                                         paintPacks = new VoidPaintPackWorks
                                         {
                                             isPaintPackRequired = appts.paintPacks.isPaintPackRequired,
                                             roomList = appts.paintPacks.roomList
                                         },
                                         customerList = appts.customerList

                                     });
            if (appointmentList.Count() > 0)
            {
                objVoidInspectionData = new List<VoidInspectionAppointmentData>(appointmentList.ToList());
                if (commonData.rooms.Count() == 0)
                {
                    AppointmentDal apptDal = new AppointmentDal();
                    commonData.rooms = apptDal.getRoomsList();
                }
                else
                {
                    commonData.rooms = null;
                }

            }
            /***
             * Reason: Repair list will send back to App on every pull to refresh call 
             * because Operatives can change matadata of appointment at anytime on web.
             * However, change of metadata should reflect on App after pull to refresh.
             ***/
            FaultsDal faultDal = new FaultsDal();
            commonData.repairList = faultDal.getCompleteRepairList();

            return objVoidInspectionData;
        }
        #endregion

        private List<AllAppointmentsList> filterFaultAppointment(List<AllAppointmentsList> objAppointmentListFault, List<ExistingAppointmentParam> allAppAppointmentList, ref List<DeleteAppointmentParam> deleteAppointment)
        {
            List<ExistingAppointmentParam> appAppointmentList = allAppAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentType == "Fault").ToList();
            List<int> appAptIds = appAppointmentList.Select(app => app.appointmentId).ToList();
            List<AllAppointmentsList> faultAppointmentList = new List<AllAppointmentsList>();
            List<int> aptIdListForDelete = new List<int>();
            List<ExistingAppointmentParam> deleteAppointmentList = new List<ExistingAppointmentParam>();
            if (objAppointmentListFault.Count() > 0)
            {
                List<int> dbAptIds = objAppointmentListFault.Select(app => app.appointmentId).ToList();
                aptIdListForDelete = appAptIds.Where(x => !dbAptIds.Contains(x)).ToList();
                deleteAppointmentList = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => aptIdListForDelete.Contains(s.appointmentId)).ToList();
                foreach (AllAppointmentsList item in objAppointmentListFault)
                {
                    var aptListWithModifiedDate = appAppointmentList.OfType<ExistingAppointmentParam>().Where(s => s.appointmentId == item.appointmentId && s.creationDate != item.creationDate);
                    if (aptListWithModifiedDate == null || aptListWithModifiedDate.Count() == 1)
                    {
                        ExistingAppointmentParam appApt = aptListWithModifiedDate.FirstOrDefault();
                        if (aptListWithModifiedDate != null)
                        {
                            deleteAppointmentList.Add(aptListWithModifiedDate.FirstOrDefault());
                        }
                        faultAppointmentList.Add(item);
                    }
                    else // For New Appointment Assignment
                    {
                        faultAppointmentList.Add(item);
                    }
                }
            }
            else
            {

                deleteAppointmentList = appAppointmentList;

            }
            foreach (ExistingAppointmentParam exApptList in deleteAppointmentList)
            {
                DeleteAppointmentParam delApp = new DeleteAppointmentParam();
                delApp.appointmentId = exApptList.appointmentId;
                delApp.appointmentType = exApptList.appointmentType;
                deleteAppointment.Add(delApp);
            }
            return faultAppointmentList;
        }
        #endregion

        #region get All Stock Appointments
        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accomodations','Surveyors List' and 'Customer List'.
        /// Code at <31-March-2015>
        /// </summary>
        /// <returns>
        /// List of stock appointments,Property Info,Accomodations,Surveyors List and Customer List, data objects
        /// </returns>
        public ResponseTemplate<List<AllAppointmentsList>> getAllStockAppointments(AppointmentParam varAppointmentParam)
        {
            try
            {

                //Parsing User info {username,salt,application type}
                string[] arryUserInfo = varAppointmentParam.userinfo.Split(',');
                List<AllAppointmentsList> objStockAppointments = new List<AllAppointmentsList>();
                //Contain all appointments
                List<AllAppointmentsList> objAllAppointmentsList = new List<AllAppointmentsList>();
                //Response Template 
                ResponseTemplate<List<AllAppointmentsList>> objResponseTemplate = new ResponseTemplate<List<AllAppointmentsList>>();
                //Message Object
                MessageData objMessageData = new MessageData();
                //Checking Application Type {Stock}
                if (int.Parse(arryUserInfo[2]) == 0)
                {
                    // Getting all Stock appointments against userName
                    objStockAppointments = this.getStockAppointments(varAppointmentParam.startdate, varAppointmentParam.enddate, arryUserInfo[0], varAppointmentParam.fetchall);
                    //Getting Overdue Appointments
                    if (varAppointmentParam.includeoverdue == 1)
                    {
                        //Get Stock Overdue Appointments, and merging the Lists of Stock appointments{new and overdue} appointments
                        objStockAppointments = objStockAppointments.Union(this.getOverdueStockAppointments(varAppointmentParam.startdate, varAppointmentParam.enddate, arryUserInfo[0], varAppointmentParam.fetchall)).ToList();
                    }

                    objResponseTemplate.response = objStockAppointments;

                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;
                }//End Check

                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region add Customer Risk And Vul
        /// <summary>
        /// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        /// </summary>
        /// <param name="apptData">appointment data inculding customer id & property id</param>
        public void addRiskVulAsbestos(ref List<AppointmentListFault> apptData)
        {
            CustomerDal custDal = new CustomerDal();
            List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
            List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

            foreach (AppointmentListFault singleApptData in apptData)
            {
                if (singleApptData.customerList != null)
                {
                    foreach (CustomerData custData in singleApptData.customerList)
                    {
                        int customerId = (int)custData.customerId; // Line changed populateAppointmentToBeArrangedList - 19/06/2013

                        //add customer vulnerability data
                        custVulData = custDal.getCustomerVulnerabilities(customerId);

                        //for testing
                        //custVulData.vulCatDesc = "Vulnerability Category Description";
                        //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

                        custData.customerVulnerabilityData = custVulData;

                        //get custoemr risk list
                        custRiskList = custDal.getCustomerRisk(customerId);

                        for (int i = 0; i < custRiskList.Count; i++)
                        {
                            CustomerRiskData custRiskData = new CustomerRiskData();
                            custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                            custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                            //add object of risk to  appointment data 
                            custData.customerRiskData.Add(custRiskData);
                        }

                        custData.Address = null;
                        custData.property = null;
                    }
                }

                //add property asbestos risk
                PropertyDal propDal = new PropertyDal();
                List<AsbestosData> propAsbListData = new List<AsbestosData>();
                string propertyId = singleApptData.property.propertyId;

                //get property asbestos risk
                propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

                for (int j = 0; j < propAsbListData.Count; j++)
                {
                    AsbestosData propAsbData = new AsbestosData();
                    propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                    propAsbData.riskDesc = propAsbListData[j].riskDesc;
                    propAsbData.type = propAsbListData[j].type;
                    propAsbData.riskLevel = propAsbListData[j].riskLevel;
                    //add object of property risk to  appointment data 
                    singleApptData.property.propertyAsbestosData.Add(propAsbData);
                }

                singleApptData.property.propertyPicture = null;
            }
        }
        #endregion

        #region get And Set Appointment Progress Status

        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>
        public string getFaultJobStatus(int appointmentId, string userName)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                // if (authDal.isUserExist(userName) > 0)
                //{
                return apptDal.getFaultJobStatus(appointmentId);
                //}
                //else
                //{
                //    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, userName), true, MessageCodesConstants.SurveyourUserNameInvalidCode);
                //    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, userName), "surveyorUserName");
                //}
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }


        #endregion

        #region save Stock Appointment

        /// <summary>
        /// This function saves the stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>

        public ResponseTemplate<List<AllAppointmentsList>> saveStockAppointment(AppointmentDataStock varAppointmentDataStock)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                int appointmentId = 0;
                ResponseTemplate<List<AllAppointmentsList>> objResponseTemplate = new ResponseTemplate<List<AllAppointmentsList>>();
                MessageData objMessageData = new MessageData();
                string propertyId = varAppointmentDataStock.customer.property.propertyId;

                if (varAppointmentDataStock.customer.customerId != null)
                {
                    int customerId = (int)varAppointmentDataStock.customer.customerId;

                    if (apptDal.isPropertyNCustomerExists(propertyId, customerId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, true, MessageCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, propertyId, customerId.ToString()), "propertyId or customer Id");
                    }
                    else if (apptDal.isAppointmentExistsAgainstProperty(propertyId, varAppointmentDataStock.appointmentType, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;

                    }
                }
                else
                {
                    if (apptDal.isPropertyExists(propertyId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyDoesNotExistMsg, true, MessageCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyDoesNotExistMsg, propertyId), "propertyId");
                    }
                    else if (apptDal.isAppointmentExistsAgainstProperty(propertyId, varAppointmentDataStock.appointmentType, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;

                    }
                }

                AuthenticationDal authDal = new AuthenticationDal();

                int loginId = authDal.isUserExist(varAppointmentDataStock.surveyorUserName);

                if (loginId > 0)
                {
                    //varAppointmentDataStock.createdBy = loginId;
                    int surveyId;
                    appointmentId = apptDal.saveStockAppointment(varAppointmentDataStock, out surveyId);

                    SendMessage.sendPushMessage(authDal.getDeviceToken(loginId), "An appointment has been added against your account in property survey application.");
                    //<Preparing the response>
                    //listSaveAppointmentData.Add(objSaveAppointmentData);
                    objResponseTemplate.response = apptDal.getStockAppointmentById(appointmentId, surveyId);

                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, varAppointmentDataStock.surveyorUserName), true, MessageCodesConstants.SurveyourUserNameInvalidCode);
                    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, varAppointmentDataStock.surveyorUserName), "surveyorUserName");
                }

                return objResponseTemplate;

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }

        }

        #endregion

        #region  get Stock Appointments
        /// <summary>
        /// This function Stock appointments information.
        /// Coded at <01-Aug-2013>
        /// </summary>
        /// <returns>
        /// List of appointments
        /// </returns>

        public List<AllAppointmentsList> getStockAppointments(string startDate, string endDate, string userName, byte fetchAll)
        {
            try
            {
                AppointmentDal objAppointmentDal = new AppointmentDal();

                if (fetchAll == 0) // Get Stock Appointments between particular dates
                {
                    return objAppointmentDal.getStockAppointmentsByDates(startDate, endDate, userName);
                }
                else // Get all Stock Appointments from particular start date
                {
                    return objAppointmentDal.getAllStockAppointments(userName, startDate, endDate);
                }
                //return appointmentData;
            }
            catch (EntityException entityException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityException;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region  get Overdue Stock Appointments between particular dates
        /// <summary>
        /// This function returns 4 Over Due  and Next 5 days appointments.
        /// Coded at <01-Aug-2013>
        /// </summary>
        /// <returns>
        /// List of appointments
        /// </returns>

        public List<AllAppointmentsList> getOverdueStockAppointments(string startDate, string endDate, string userName, byte fetchAll)
        {
            try
            {
                AppointmentDal objAppointmentDal = new AppointmentDal();
                //List<AppointmentListStock> overdueAppointmentData = new List<AppointmentListStock>();
                if (fetchAll == 0)
                {
                    return objAppointmentDal.getOverdueStockAppointmentsByDates(startDate, userName);
                }
                else
                {
                    return objAppointmentDal.getAllOverdueStockAppointments(startDate, userName);
                }
                //return overdueAppointmentData;
            }
            catch (EntityException entityException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityException;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all or Gas users</returns>

        public ResponseTemplate<List<SurveyorUserData>> getAllUsers(SurveyorParam varSurveyorParam)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                MessageData objMessageData = new MessageData();
                ResponseTemplate<List<SurveyorUserData>> objResponseTemplate = new ResponseTemplate<List<SurveyorUserData>>();
                //Adding Response Json
                if (varSurveyorParam.applicationtype == 1)
                {
                    objResponseTemplate.response = apptDal.getAllUsersGas();
                }
                else
                {
                    objResponseTemplate.response = apptDal.getAllUsers();
                }
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region delete Stock Appointment
        /// <summary>
        /// This function is used to delete the appointment from the server
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true or false based on delete action</returns>

        public ResponseTemplate<List<DelAppointmentData>> deleteStockAppointment(int appointmentId)
        {
            try
            {
                //<Declaring objects>
                AppointmentDal appointmentDal = new AppointmentDal();
                ResponseTemplate<List<DelAppointmentData>> objResponseTemplate = new ResponseTemplate<List<DelAppointmentData>>();
                DelAppointmentData objDelAppointmentData = new DelAppointmentData();
                List<DelAppointmentData> listDelAppointmentData = new List<DelAppointmentData>();
                MessageData objMessageData = new MessageData();
                //<Adding custom values in List>
                objDelAppointmentData.appointmentId = appointmentId;
                objDelAppointmentData.isDeleted = appointmentDal.deleteAppointment(appointmentId);
                listDelAppointmentData.Add(objDelAppointmentData);
                //<Assigning List to template>
                objResponseTemplate.response = listDelAppointmentData;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }
        #endregion


        #region Update appointment status

        public SyncAppointmentStatusResponse updateAppointmentStatus(SyncAppointmentStatusRequest request)
        {
            AppointmentDal apptDal = new AppointmentDal();
            SyncAppointmentStatusResponse response = new SyncAppointmentStatusResponse();
            int userId = request.loggedInUserId;

            foreach (AppointmentInProgress apt in request.appointments)
            {
                if (apt.appointmentType == ApplicationConstants.GasAppointmentType
                    || apt.appointmentType == ApplicationConstants.VoidGasAppointmentType
                    || apt.appointmentType == ApplicationConstants.AlternativeAppointmentType
                    || apt.appointmentType == ApplicationConstants.OilAppointmentType
                    )
                {
                    apptDal.updateAppointmentStatusForGas(ref response, apt, userId);
                }
                else if (apt.appointmentType == ApplicationConstants.FaultAppointmentType)
                {
                    apptDal.updateAppointmentStatusForFault(ref response, apt, userId);
                }
                else if (apt.appointmentType == ApplicationConstants.PlannedAppointmentType
                    || apt.appointmentType == ApplicationConstants.MiscellaneousAppointmentType
                    || apt.appointmentType == ApplicationConstants.AdaptationAppointmentType
                    || apt.appointmentType == ApplicationConstants.ConditionAppointmentType)
                {
                    apptDal.updateAppointmentStatusForPlanned(ref response, apt, userId);
                }
                else if (apt.appointmentType == ApplicationConstants.VoidInspectionAppointmentType
                    || apt.appointmentType == ApplicationConstants.PostVoidInspectionAppointmentType
                    || apt.appointmentType == ApplicationConstants.VoidWorksAppointmentType
                    || apt.appointmentType == ApplicationConstants.VoidGasCheckAppointmentType
                    || apt.appointmentType == ApplicationConstants.VoidElectricCheckAppointmentType)
                {
                    apptDal.updateAppointmentStatusForVoid(ref response, apt, userId);
                }
                else if (apt.appointmentType == ApplicationConstants.ApplianceDefectAppointmentType)
                {
                    apptDal.updateAppointmentStatusForApplianceDefect(ref response, apt, userId);
                }
            }

            return response;
        }

        #endregion

        #region save Complete Appointment for gas

        /// <summary>
        /// this function calls the method for Gas. and update (appointment Data,SurveyData,Customer data,No entry data in case of "NoEntry" Status,
        /// Journal data,Lgsr data and populate CP12Document 
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        private bool completeAppointmentForGas(AllAppointmentsList appointment)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                SurveyDal surveyDal = new SurveyDal();
                FaultsDal faultDal = new FaultsDal();
                //Update Appointment Data
                apptDal.completeAppointmentDataForApplianceServicing(appointment);
                apptDal.logActualStartAndEndDateTimeForGasAppointment(appointment.appointmentType, appointment.appointmentId, Convert.ToDateTime(appointment.appointmentStartDateTime), Convert.ToDateTime(appointment.appointmentEndDateTime));
                if (appointment.property != null)
                {
                    AppliancesDal appliancesDal = new AppliancesDal();
                    InstallationPipeworkDal insWorkDal = new InstallationPipeworkDal();

                    foreach (ApplianceData appData in appointment.property.appliances)
                    {
                        AppliancesBl appliancesBl = new Appliances.AppliancesBl();
                        appData.ApplianceID = appliancesBl.saveUpdateOfflineAppliance(appData);
                        //appliancesDal.saveAppliance(appData);
                        if (appData.ApplianceInspection != null && appData.ApplianceInspection.inspectionDate != null)
                        {
                            appData.ApplianceInspection.APPLIANCEID = appData.ApplianceID;
                            appData.ApplianceInspection.JOURNALID = appointment.journalId;
                            appliancesDal.saveApplianceInspectionGas(appData.ApplianceInspection);
                        }
                        if (appData.ApplianceDefects != null && appData.ApplianceDefects.Count > 0)
                        {
                            foreach (FaultsDataGas appDefectData in appData.ApplianceDefects)
                            {
                                appDefectData.ApplianceID = appData.ApplianceID;
                                faultDal.updateFaultDataGas(appDefectData);
                            }
                        }
                    }
                    foreach (DetectorCountData detector in appointment.property.detectors)
                    {

                        appliancesDal.updateDetectorCount(appointment.property.propertyId, detector, appointment.updatedBy);
                        if (detector.detectorDefects != null && detector.detectorDefects.Count > 0)
                        {
                            foreach (FaultsDataGas appDefectData in detector.detectorDefects)
                            {
                                faultDal.updateFaultDataGas(appDefectData);
                            }
                        }
                        if (detector.detectorInspection != null && detector.detectorTypeId > 0)
                        {
                            detector.detectorInspection.inspectionID = appliancesDal.addUpdateDetectorInfo(detector.detectorInspection, detector.detectorTypeId, detector.isInspected, appointment.journalId, appointment.property.propertyId).result;
                        }
                    }
                    /*
                    if (appointment.property.installationpipework != null && appointment.property.installationpipework.inspectionDate != null)
                        insWorkDal.saveInstallationPipeworkGas(appointment);
                     */

                }
                if (appointment.appointmentStatus == "No Entry")
                {
                    apptDal.updateApplianceJournalForNoEntryStatus(appointment);
                }
                else
                {
                    if (appointment.cp12Info != null)
                    {
                        apptDal.updateLgsrData(appointment);
                    }

                    if (appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                    {
                        //  apptDal.updateJournalData(appointment, Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status);

                        // completeAppointmentForGas function has been deprecated that is why this section has been commented out.
                        /*
                        bool response = apptDal.UpdateAndInsertJournal(appointment);
                        if (response)
                            DocumentHelper.populateCp12Document(appointment.journal, appointment.journal.journalId.ToString(), appointment.updatedBy.ToString());
                         * 
                         * */
                    }
                }
                return true;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }

        #endregion

        #region save Complete appointment for stock
        /// <summary>
        /// this function calls the method for Stock. and update (appointment Data,SurveyData and Customer data
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        private bool completeAppointmentForStock(AllAppointmentsList appointment)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                SurveyDal surveyDal = new SurveyDal();
                SurveyBl survBl = new SurveyBl();
                //Update Appointment Data
                apptDal.completeAppointmentForStock(appointment);

                SurveyData appSurveyData = new SurveyData();
                if (appointment.survey != null && appointment.survey.surveyData != null)
                {
                    foreach (SurveyData surveylist in appointment.survey.surveyData)
                    {
                        surveylist.completedBy = appointment.updatedBy ?? 0;
                        //surveyDal.saveSurveyForm(surveylist);
                        survBl.saveSurveyForm(surveylist);
                    }
                }
                if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                {
                    int surveyId = survBl.getSurveyId(appointment.appointmentId);
                    if (surveyId > 0)
                    {
                        try
                        {
                            string result = DocumentHelper.populatePropertyRecordInspection(appointment.property.propertyId, surveyId.ToString(), appointment.createdBy.ToString());
                            ErrorFaultSetGet.setErrorFault("+++Result++++++++++" + result + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                        }
                        catch (Exception ex)
                        {
                            ErrorFaultSetGet.setErrorFault("+++++Resultt++++++++" + ex.ToString() + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                        }
                    }
                }

                return true;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(entityexception.Message, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }
        #endregion

        #region save Complete Appointment

        /// <summary>
        /// This function saves the stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>
        public ResponseTemplate<ReturnAppointmentStatus> completeAppointment(AppointmentContractData varUpdateAppointmentData)
        {
            try
            {
                //using (TransactionScope trans = new TransactionScope())
                //{
                ResponseTemplate<ReturnAppointmentStatus> objResponseTemplate = new ResponseTemplate<ReturnAppointmentStatus>();
                ReturnAppointmentStatus objUpdateCompleteAppointmentData = new ReturnAppointmentStatus();
                //ReturnAppointmentStatus listCompleteAppointmentData = new ReturnAppointmentStatus();
                MessageData objMessageData = new MessageData();
                objUpdateCompleteAppointmentData.isUpdated = false;
                foreach (AllAppointmentsList appointment in varUpdateAppointmentData.appointments)
                {
                    AppointmentDal apptDal = new AppointmentDal();
                    SurveyDal surveyDal = new SurveyDal();
                    if (appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Gas.ToString() ||
                        appointment.appointmentType.Replace(" ", "") == PropSurvey.Contracts.Data.AppointmentTypes.VoidGas.ToString())
                    {
                        completeAppointmentForGas(appointment);
                    }
                    else if (appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Stock.ToString())
                    {
                        completeAppointmentForStock(appointment);
                    }
                    else if (appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Fault.ToString())
                    {
                        completeAppointmentForFault(appointment);
                    }
                    else if (appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Planned.ToString()
                            || appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Miscellaneous.ToString()
                            || appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Adaptation.ToString()
                            || appointment.appointmentType == PropSurvey.Contracts.Data.AppointmentTypes.Condition.ToString())
                    {
                        completeAppointmentForPlanned(appointment);
                    }
                    if (appointment.customerList != null)
                    {
                        foreach (CustomerData customer in appointment.customerList)
                        {
                            CustomerDal custDal = new CustomerDal();
                            custDal.UpdateCustomerContactFromAppointment(customer);
                        }
                    }

                    if (appointment.property != null)
                    {
                        PropertyDal propDal = new PropertyDal();
                        //propDal.updateProperty(appointment.property);
                        if (appointment.property.Accommodations != null)
                        {
                            propDal.updatePropertyDimensions(appointment.property.Accommodations, appointment.property.propertyId);
                        }
                    }
                    objUpdateCompleteAppointmentData.isUpdated = true;
                    if (varUpdateAppointmentData.respondWithData == true)
                    {
                        objUpdateCompleteAppointmentData.Appointment = appointment;
                    }
                    else
                    {
                        objUpdateCompleteAppointmentData.Appointment = null;
                    }
                }
                // listCompleteAppointmentData.Add(objUpdateCompleteAppointmentData);
                //<Assigning List to template>
                objResponseTemplate.response = objUpdateCompleteAppointmentData;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                //trans.Complete(); 
                return objResponseTemplate;

                //}
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }

        #endregion

        #region save complete Fault appointment
        private bool completeAppointmentForFault(AllAppointmentsList appointment)
        {

            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                if (appointment.jobDataList != null)
                {
                    foreach (JobData jobData in appointment.jobDataList)
                    {
                        //Check job pause data exist
                        if (jobData.jobPauseHistory.Count > 0)
                        {
                            foreach (JobPauseHistoryData jobPauseData in jobData.jobPauseHistory)
                            {
                                // save fault paused history
                                apptDal.saveFaultPaused(jobPauseData, jobData.faultLogID);

                            }
                        }
                        //check repair list exist
                        if (jobData.faultRepairList.Count > 0)
                        {
                            apptDal.saveFaultRepairList(jobData, appointment.createdBy);
                        }

                        if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                        {
                            apptDal.completeAppointmentForFault(jobData, appointment);

                        }
                        else if (appointment.appointmentStatus != "No Entry"
                                 || appointment.appointmentStatus.Replace(" ", "") != AppointmentCompleteStatus.NoEntry.ToString()
                                )
                        {
                            apptDal.PausedAppointmentForFault(jobData, appointment);
                        }
                    }
                }
                if (appointment.appointmentStatus == "No Entry")
                {
                    apptDal.noEntryFaultAppointment(appointment);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
            return true;
        }
        #endregion

        #region save complete Planned appointment

        private bool completeAppointmentForPlanned(AllAppointmentsList appointment)
        {

            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                if (appointment.componentTrade != null)
                {
                    //Check job pause data exist
                    if (appointment.componentTrade.jobPauseHistory != null && appointment.componentTrade.jobPauseHistory.Count > 0)
                    {
                        foreach (JobPauseHistoryData jobPauseData in appointment.componentTrade.jobPauseHistory)
                        {
                            // save fault paused history
                            apptDal.savePlannedPaused(jobPauseData, appointment.appointmentId);
                        }
                    }
                }

                if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString() || appointment.appointmentStatus == AppointmentCompleteStatus.InProgress.ToString())
                {
                    apptDal.completeAppointmentForPlanned(appointment);
                }

                if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                {
                    apptDal.noEntryPlannedAppointment(appointment);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
            return true;
        }

        #endregion

        #region save Complete Appointment V2

        /// <summary>
        /// This function saves the stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>
        public ResponseTemplate<CompleteAppointmentsStatus> completeAppointmentV2(AppointmentContractData varUpdateAppointmentData)
        {
            AuthenticationDal authDal = new AuthenticationDal();
            int operativeId = authDal.getEmployeeId(varUpdateAppointmentData.username);
            ResponseTemplate<CompleteAppointmentsStatus> objResponseTemplate = new ResponseTemplate<CompleteAppointmentsStatus>();
            CompleteAppointmentsStatus objCompleteAppointmentsStatus = new CompleteAppointmentsStatus();
            objResponseTemplate.response = objCompleteAppointmentsStatus;
            try
            {
                foreach (IAppointment iAppointment in varUpdateAppointmentData.appointments)
                {
                    iAppointment.operativeId = operativeId;
                    SyncAppointmentDetail currentSyncappointment;
                    if (iAppointment.GetType() == typeof(AllAppointmentsList))
                    {
                        AllAppointmentsList appointment = (AllAppointmentsList)iAppointment;
                        string address = string.Empty;
                        if (appointment.property != null)
                        {
                            address = String.Format("{0}{1}{2}{3}", appointment.property.houseNumber ?? "", (" " + appointment.property.address1) ?? "", (" " + appointment.property.address2) ?? "", (" " + appointment.property.address3) ?? "").Trim();
                        }
                        else
                        {
                            address = String.Format("{0}{1}{2}", appointment.scheme.address1 ?? "", (" " + appointment.scheme.address2) ?? "", (" " + appointment.scheme.address3) ?? "").Trim();

                        }
                        currentSyncappointment = new SyncAppointmentDetail(appointment.appointmentId, appointment.appointmentType, address);

                        completeAppointmentExceptVoid(ref objCompleteAppointmentsStatus, ref currentSyncappointment, appointment);
                    }
                    else if (iAppointment.GetType() == typeof(VoidInspectionAppointmentData))
                    {
                        VoidInspectionAppointmentData appointment = (VoidInspectionAppointmentData)iAppointment;
                        string address = string.Empty;

                        address = String.Format("{0}{1}{2}{3}", appointment.property.houseNumber ?? "", (" " + appointment.property.address1) ?? "", (" " + appointment.property.address2) ?? "", (" " + appointment.property.address3) ?? "").Trim();

                        currentSyncappointment = new SyncAppointmentDetail(appointment.appointmentId, appointment.appointmentType, address);

                        completeVoidAppointment(ref objCompleteAppointmentsStatus, ref currentSyncappointment, iAppointment);
                    }

                    else if (iAppointment.GetType() == typeof(VoidGasElectricData))
                    {
                        VoidGasElectricData appointment = (VoidGasElectricData)iAppointment;
                        string address = string.Empty;

                        address = String.Format("{0}{1}{2}{3}", appointment.property.houseNumber ?? "", (" " + appointment.property.address1) ?? "", (" " + appointment.property.address2) ?? "", (" " + appointment.property.address3) ?? "").Trim();
                        int apId = (int)appointment.appointmentId;
                        currentSyncappointment = new SyncAppointmentDetail(apId, appointment.appointmentType, address);

                        completeVoidAppointment(ref objCompleteAppointmentsStatus, ref currentSyncappointment, iAppointment);
                    }
                    else if (iAppointment.GetType() == typeof(VoidWorksAppointmentData))
                    {
                        VoidWorksAppointmentData appointment = (VoidWorksAppointmentData)iAppointment;
                        string address = string.Empty;

                        address = String.Format("{0}{1}{2}{3}", appointment.property.houseNumber ?? "", (" " + appointment.property.address1) ?? "", (" " + appointment.property.address2) ?? "", (" " + appointment.property.address3) ?? "").Trim();
                        int apId = (int)appointment.appointmentId;
                        currentSyncappointment = new SyncAppointmentDetail(apId, appointment.appointmentType, address);

                        completeVoidAppointment(ref objCompleteAppointmentsStatus, ref currentSyncappointment, iAppointment);
                    }
                    else if (iAppointment.GetType() == typeof(ApplianceDefectAppointmentData))
                    {
                        ApplianceDefectAppointmentData appointment = (ApplianceDefectAppointmentData)iAppointment;
                        string address = String.Format("{0}{1}{2}{3}", appointment.property.houseNumber ?? "", (" " + appointment.property.address1) ?? "", (" " + appointment.property.address2) ?? "", (" " + appointment.property.address3) ?? "").Trim();
                        currentSyncappointment = new SyncAppointmentDetail((int)appointment.appointmentId, appointment.appointmentType, address);

                        completeApplianceDefectAppointment(ref objCompleteAppointmentsStatus, ref currentSyncappointment, appointment);
                    }
                }

                //<Assigning List to template>
                objResponseTemplate.response = objCompleteAppointmentsStatus;
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
            return objResponseTemplate;
        }

        #endregion

        #region Complete Appliance Defect Appointment

        private static void completeApplianceDefectAppointment(ref CompleteAppointmentsStatus objCompleteAppointmentsStatus, ref SyncAppointmentDetail currentSyncappointment, ApplianceDefectAppointmentData appointment)
        {
            bool isSuccessFull = false;
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                isSuccessFull = apptDal.completeApplianceDefectAppointment(appointment);
            }
            catch (TransactionAbortedException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
            }
            catch (FormatException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.InvalidData;
            }
            catch (EntityException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConErrorMsg;
                //Suggestion: If need more catch blocks add a bool type variable with name isError = false 
                //and set isError = true in all catch block.
                // and take following line in finally block and call with conditon isError = true.                        
            }
            finally
            {
                if (isSuccessFull)
                {
                    currentSyncappointment.failureReason = null;
                    objCompleteAppointmentsStatus.savedAppointments.Add(currentSyncappointment);
                }
                else
                    objCompleteAppointmentsStatus.failedAppointments.Add(currentSyncappointment);
            }
        }

        #endregion

        #region Complete Appointment Except Void (i.e Gas, Stock, Fault, Planned/Misc/Adaptation/Condition)

        private static void completeAppointmentExceptVoid(ref CompleteAppointmentsStatus objCompleteAppointmentsStatus, ref SyncAppointmentDetail currentSyncappointment, AllAppointmentsList appointment)
        {
            bool isSuccessFull = false;
            try
            {
                AppointmentDal apptDal = new AppointmentDal();

                if (appointment.appointmentType == ApplicationConstants.GasAppointmentType ||
                    appointment.appointmentType == ApplicationConstants.VoidGasAppointmentType)
                {
                    isSuccessFull = apptDal.completeAppointmentForGasV2(appointment);
                    apptDal.logActualStartAndEndDateTimeForGasAppointment(appointment.appointmentType, appointment.appointmentId, Convert.ToDateTime(appointment.appointmentStartDateTime), Convert.ToDateTime(appointment.appointmentEndDateTime));
                }
                else if (appointment.appointmentType == ApplicationConstants.AlternativeAppointmentType)
                {
                    isSuccessFull = apptDal.completeAppointmentForAlternativeHeating(appointment);
                    apptDal.logActualStartAndEndDateTimeForAlternativeAppointment(appointment.appointmentType, appointment.appointmentId, Convert.ToDateTime(appointment.appointmentStartDateTime), Convert.ToDateTime(appointment.appointmentEndDateTime));
                }
                else if (appointment.appointmentType == ApplicationConstants.OilAppointmentType)
                {
                    isSuccessFull = apptDal.completeAppointmentForOilHeating(appointment);
                    apptDal.logActualStartAndEndDateTimeForOilAppointment(appointment.appointmentType, appointment.appointmentId, Convert.ToDateTime(appointment.appointmentStartDateTime), Convert.ToDateTime(appointment.appointmentEndDateTime));
                }
                else if (appointment.appointmentType == AppointmentTypes.Stock.ToString())
                {
                    isSuccessFull = apptDal.completeAppointmentForStockV2(appointment);
                }
                else if (appointment.appointmentType == AppointmentTypes.Fault.ToString())
                {
                    isSuccessFull = apptDal.completeAppointmentForFaultV2(appointment);
                }
                else if (appointment.appointmentType == AppointmentTypes.Planned.ToString()
                        || appointment.appointmentType == AppointmentTypes.Miscellaneous.ToString()
                        || appointment.appointmentType == AppointmentTypes.Adaptation.ToString()
                        || appointment.appointmentType == AppointmentTypes.Condition.ToString())
                {
                    isSuccessFull = apptDal.completeAppointmentForPlannedV2(appointment);
                }

            }
            catch (TransactionAbortedException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
            }
            catch (FormatException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.InvalidData;
            }
            catch (EntityException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = ex.Message;
                //Suggestion: If need more catch blocks add a bool type variable with name isError = false 
                //and set isError = true in all catch block.
                // and take following line in finally block and call with conditon isError = true.                        
            }
            finally
            {
                if (isSuccessFull)
                {
                    currentSyncappointment.failureReason = null;
                    objCompleteAppointmentsStatus.savedAppointments.Add(currentSyncappointment);
                }
                else
                    objCompleteAppointmentsStatus.failedAppointments.Add(currentSyncappointment);
            }
        }

        #endregion

        #region Complete Void Appointments

        private static void completeVoidAppointment(ref CompleteAppointmentsStatus objCompleteAppointmentsStatus, ref SyncAppointmentDetail currentSyncappointment, IAppointment iAppointment)
        {
            bool isSuccessFull = false;
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                if (iAppointment.GetType() == typeof(VoidInspectionAppointmentData))
                {
                    VoidInspectionAppointmentData appointment = (VoidInspectionAppointmentData)iAppointment;
                    isSuccessFull = apptDal.completeAppointmentForVoidInspection(appointment);
                }
                else if (iAppointment.GetType() == typeof(VoidGasElectricData))
                {
                    VoidGasElectricData appointment = (VoidGasElectricData)iAppointment;
                    isSuccessFull = apptDal.completeGasElectricCheckAppointment(appointment);
                }

                else if (iAppointment.GetType() == typeof(VoidWorksAppointmentData))
                {
                    VoidWorksAppointmentData appointment = (VoidWorksAppointmentData)iAppointment;
                    isSuccessFull = apptDal.completeVoidWorksAppointment(appointment);
                }

            }
            catch (TransactionAbortedException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
            }
            catch (FormatException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.InvalidData;
            }
            catch (EntityException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                isSuccessFull = false;
                currentSyncappointment.failureReason = MessageConstants.DbConErrorMsg;
                //Suggestion: If need more catch blocks add a bool type variable with name isError = false 
                //and set isError = true in all catch block.
                // and take following line in finally block and call with conditon isError = true.                        
            }
            finally
            {
                if (isSuccessFull)
                {
                    currentSyncappointment.failureReason = null;
                    objCompleteAppointmentsStatus.savedAppointments.Add(currentSyncappointment);
                }
                else
                    objCompleteAppointmentsStatus.failedAppointments.Add(currentSyncappointment);
            }
        }

        #endregion

        #region Populate Stock Inspection Documents.
        /// <summary>
        /// This function used to Populate Stock Inspection Documents.
        /// </summary>        
        /// <returns>string Message</returns>

        public string populateStockInspection(StockInspectionDocument stockInspectionDocList)
        {
            string message = string.Empty;
            int userId = stockInspectionDocList.userId;
            foreach (StockAppointmentDetail appointment in stockInspectionDocList.appointmentList)
            {
                SurveyBl survBl = new SurveyBl();
                int surveyId = survBl.getSurveyId(appointment.appointmentId);
                if (surveyId > 0)
                {
                    try
                    {
                        message = DocumentHelper.populatePropertyRecordInspection(appointment.propertyId, surveyId.ToString(), userId.ToString());
                        ErrorFaultSetGet.setErrorFault("+++Result++++++++++" + message + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                    }
                    catch (Exception ex)
                    {
                        ErrorFaultSetGet.setErrorFault("+++++Resultt++++++++" + ex.ToString() + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                    }
                }
            }

            return message;
        }
        #endregion
    }
}
