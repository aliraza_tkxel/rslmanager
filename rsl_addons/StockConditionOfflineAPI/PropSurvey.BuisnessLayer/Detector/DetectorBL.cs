﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal;
using PropSurvey.Contracts.Data;

namespace PropSurvey.BuisnessLayer
{
    public class DetectorBL
    {

        #region Get All Detectors

        public List<Detector> GetAllDetectors(ApplianceSurveyDataParam requestParam)
        {
            DetectorDal detectorDal = new DetectorDal();
            return detectorDal.GetAllDetectors(requestParam);            
        }

        #endregion

    }
}
