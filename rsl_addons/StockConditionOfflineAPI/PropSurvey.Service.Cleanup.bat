#@del /F /Q *.user
@PUSHD PropSurvey.ServiceHost
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD .\PropSurvey.BuisnessLayer
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PropSurvey.Contracts
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PropSurvey.Dal
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PropSurvey.Entities
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PropSurvey.Utilities
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PushSharp\PushSharp.Apple
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PushSharp\PushSharp.Core
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PAUSE