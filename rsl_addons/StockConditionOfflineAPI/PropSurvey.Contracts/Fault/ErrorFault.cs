﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropSurvey.Contracts.Fault
{
    public static class ErrorFault
    {
        public static string message { get; set; }

        public static bool isErrorOccured { get; set; }
        
        public static string errorCode { get; set; }
    
    }
}
