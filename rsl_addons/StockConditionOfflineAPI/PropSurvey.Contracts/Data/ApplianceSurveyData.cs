﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceSurveyData
    {
        [DataMember]
        public List<ApplianceData> appliancesList = new List<ApplianceData>();

        [DataMember]        
        public List<DetectorCountData> detectorsList = new List<DetectorCountData>();

        [DataMember]
        public List<Detector> propertyDetectors = new List<Detector>();

        [DataMember]
        public List<Meter> Meters = new List<Meter>();

        [DataMember]
        public List<BoilerData> boilers = new List<BoilerData>();
    }
}
