﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AirSourceInspection
    {
        [DataMember]
        public bool? checkWaterSupplyTurnedOff { get; set; }

        [DataMember]
        public string turnedOffDetail { get; set; }

        [DataMember]
        public bool? checkWaterSupplyTurnedOn { get; set; }

        [DataMember]
        public string turnedOnDetail { get; set; }

        [DataMember]
        public bool? checkValves { get; set; }

        [DataMember]
        public string valveDetail { get; set; }

        [DataMember]
        public bool? checkSupplementaryBonding { get; set; }

        [DataMember]
        public string supplementaryBondingDetail { get; set; }

        [DataMember]
        public bool? checkFuse { get; set; }

        [DataMember]
        public string fuseDetail { get; set; }

        [DataMember]
        public bool? checkThermostat { get; set; }

        [DataMember]
        public string thermostatDetail { get; set; }

        [DataMember]
        public bool? checkOilLeak { get; set; }

        [DataMember]
        public string oilLeakDetail { get; set; }

        [DataMember]
        public bool? checkWaterPipework { get; set; }

        [DataMember]
        public string pipeworkDetail { get; set; }

        [DataMember]
        public bool? checkElectricConnection { get; set; }

        [DataMember]
        public string connectionDetail { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public int? inspectionId { get; set; }

        [DataMember]
        public int inspectedBy { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int? journalId { get; set; }


        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA

        }

    }
}
