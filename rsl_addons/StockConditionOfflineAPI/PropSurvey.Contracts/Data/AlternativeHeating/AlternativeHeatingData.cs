﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AlternativeHeatingData
    {
        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public int? manufacturerId { get; set; }

        [DataMember]
        public string model { get; set; }

        [DataMember]
        public string serialNumber { get; set; }

        [DataMember]
        public string location { get; set; }

        [DataMember]
        public DateTime? installedDate { get; set; }

        [DataMember]
        public string heatingName { get; set; }

        [DataMember]
        public string certificateName { get; set; }

        [DataMember]
        public DateTime? certificateIssued { get; set; }

        [DataMember]
        public string certificateNumber { get; set; }

        [DataMember]
        public DateTime? certificateRenewel { get; set; }

        [DataMember]
        public int? solarTypeId { get; set; }

        [DataMember]
        public int? itemId { get; set; }

        [DataMember]
        public string heatingFuel { get; set; }

        [DataMember]
        public string heatingType { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public AirSourceInspection airsourceInspection = new AirSourceInspection();

        [DataMember]
        public MvhrInspection mvhrInspection = new MvhrInspection();

        [DataMember]
        public SolarInspection solarInspection = new SolarInspection();

    }
}
