﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class GeneralHeatingChecks
    {
        [DataMember]
        public string radiatorCondition { get; set; }

        [DataMember]
        public string heatingControl { get; set; }

        [DataMember]
        public string systemUsage { get; set; }

        [DataMember]
        public string generalHeating { get; set; }

        [DataMember]
        public string accessIssues { get; set; }

        [DataMember]
        public string accessIssueNotes { get; set; }

        [DataMember]
        public string confirmation { get; set; }

        [DataMember]
        public DateTime? checkedDate { get; set; }
    }

}
