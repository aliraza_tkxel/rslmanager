﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SolarInspection
    {
        [DataMember]
        public string vesselProtectionInstalled { get; set; }

        [DataMember]
        public string vesselCapacity { get; set; }

        [DataMember]
        public string minPressure { get; set; }

        [DataMember]
        public string freezingTemp { get; set; }

        [DataMember]
        public string systemPressure { get; set; }

        [DataMember]
        public string backPressure { get; set; }

        [DataMember]
        public string deltaOn { get; set; }

        [DataMember]
        public string deltaOff { get; set; }

        [DataMember]
        public string maxTemperature { get; set; }

        [DataMember]
        public string calculationRate { get; set; }

        [DataMember]
        public string thermostatTemperature { get; set; }

        [DataMember]
        public bool? antiScaldingControl { get; set; }

        [DataMember]
        public string antiScaldingControlDetail { get; set; }

        [DataMember]
        public bool? checkDirection { get; set; }

        [DataMember]
        public string directionDetail { get; set; }

        [DataMember]
        public bool? checkElectricalControl { get; set; }

        [DataMember]
        public string electricalControlDetail { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public int? inspectionId { get; set; }

        [DataMember]
        public int inspectedBy { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int? journalId { get; set; }


        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA

        }

    }
}
