﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class MvhrInspection
    {
        [DataMember]
        public bool? checkAirFlow { get; set; }

        [DataMember]
        public string airFlowDetail { get; set; }

        [DataMember]
        public bool? ductingInspection { get; set; }

        [DataMember]
        public string ductingDetail { get; set; }

        [DataMember]
        public bool? checkFilters { get; set; }

        [DataMember]
        public string filterDetail { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public int? inspectionId { get; set; }

        [DataMember]
        public int inspectedBy { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int? journalId { get; set; }


        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA

        }

    }
}
