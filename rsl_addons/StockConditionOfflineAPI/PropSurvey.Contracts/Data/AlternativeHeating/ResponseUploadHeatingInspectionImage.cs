﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    /// <summary>
    ///DataContract for  DeletepropertyImage request
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponseUploadHeatingInspectionImage
    {
        [DataMember]
        public int? HeatingInspectionImageId { get; set; }
        [DataMember]
        public string imagePath { get; set; }
    }
}
