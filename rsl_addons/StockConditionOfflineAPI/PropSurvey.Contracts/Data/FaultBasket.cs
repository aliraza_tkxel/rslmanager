﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class FaultBasket
    {
        [DataMember]
        public int faultId { get; set; }
      
        [DataMember]
        public int faultAreaId { get; set; }
        
        [DataMember]
        public bool? isRecurring { get; set; }

        [DataMember]
        public int? problemDays { get; set; }

        [DataMember]
        public string notes { get; set; }

    }
}
