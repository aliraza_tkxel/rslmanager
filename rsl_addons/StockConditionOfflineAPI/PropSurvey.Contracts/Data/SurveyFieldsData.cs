﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]

    public class SurveyFieldsData
    {

        [DataMember]
        public int surveyParameterId { get; set; }

        [DataMember]
        public int surveyItemParamId { get; set; }


        //[DataMember]
        //public int surveyPramItemFieldId { get; set; }

        //[DataMember, Newtonsoft.Json.JsonProperty]
        //public object  surveyParamItemFieldValue { get; set; }

        [DataMember]
        public List<SurveyParamItemFieldsData> surveyParamItemField = new List<SurveyParamItemFieldsData>();

        [DataMember]
        public string controlType = string.Empty;

        [DataMember]
        public string surveyParamName = string.Empty;

     
    }
}
