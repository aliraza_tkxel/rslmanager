﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PropSurvey.Contracts.Data.Void;
using PropSurvey.Contracts.Data.Appointment;

namespace PropSurvey.Contracts.Data
{
    [KnownType(typeof(AllAppointmentsList))]
    [KnownType(typeof(VoidGasElectricData))]
    [KnownType(typeof(VoidInspectionAppointmentData))]
    [KnownType(typeof(VoidWorksAppointmentData))]
    [KnownType(typeof(ApplianceDefectAppointmentData))]

    [DataContract]
    [Serializable]
    public abstract class IAppointment
    {
        [DataMember]
        public List<CustomerData> customerList = new List<CustomerData>();

        [IgnoreDataMember]
        public int operativeId { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public DateTime? creationDate { get; set; }

        [DataMember]
        public String appointmentCompletedAppVersion { get; set; }

        [DataMember]
        public String appointmentCurrentAppVersion { get; set; }

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public List<AppointmentHistoryData> appointmentHistory = new List<AppointmentHistoryData>();
    }
}
