﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class OilSurveyDataParam
    {
        [DataMember]
        public string propertyId { get; set; }
        [DataMember]
        public int? schemeId { get; set; }
        [DataMember]
        public int? blockId { get; set; }
        [DataMember]
        public int appointmentid { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public int? journalId { get; set; }

    }
}
