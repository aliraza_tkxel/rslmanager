﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class OilSurveyData
    {
        [DataMember]
        public List<OilHeatingData> oilHeatings = new List<OilHeatingData>();
    }
}
