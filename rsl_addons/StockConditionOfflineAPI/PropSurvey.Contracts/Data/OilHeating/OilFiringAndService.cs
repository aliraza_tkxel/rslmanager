﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class OilFiringAndService
    {
        [DataMember]
        public bool? oilStorage { get; set; }

        [DataMember]
        public string oilStorageDetail { get; set; }

        [DataMember]
        public bool? oilSupplySystem { get; set; }

        [DataMember]
        public string oilSupplySystemDetail { get; set; }

        [DataMember]
        public bool? airSupply { get; set; }

        [DataMember]
        public string airSupplyDetail { get; set; }

        [DataMember]
        public bool? chimneyFlue { get; set; }

        [DataMember]
        public string chimneyFlueDetail { get; set; }

        [DataMember]
        public bool? electricalSafety { get; set; }

        [DataMember]
        public string electricalSafetyDetail { get; set; }

        [DataMember]
        public bool? heatExchanger { get; set; }

        [DataMember]
        public string heatExchangerDetail { get; set; }

        [DataMember]
        public bool? combustionChamber { get; set; }

        [DataMember]
        public string combustionChamberDetail { get; set; }

        [DataMember]
        public bool? pressureJet { get; set; }

        [DataMember]
        public string pressureJetDetail { get; set; }

        [DataMember]
        public bool? vaporisingBurner { get; set; }

        [DataMember]
        public string vaporisingBurnerDetail { get; set; }

        [DataMember]
        public bool? wallflameBurner { get; set; }

        [DataMember]
        public string wallflameBurnerDetail { get; set; }

        [DataMember]
        public bool? applianceSafety { get; set; }

        [DataMember]
        public string applianceSafetyDetail { get; set; }

        [DataMember]
        public bool? controlCheck { get; set; }

        [DataMember]
        public string controlCheckDetail { get; set; }

        [DataMember]
        public bool? hotWaterType { get; set; }

        [DataMember]
        public string hotWaterTypeDetail { get; set; }

        [DataMember]
        public bool? warmAirType { get; set; }

        [DataMember]
        public string warmAirTypeDetail { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public int? inspectionId { get; set; }

        [DataMember]
        public int inspectedBy { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int? journalId { get; set; }


        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA

        }

    }
}
