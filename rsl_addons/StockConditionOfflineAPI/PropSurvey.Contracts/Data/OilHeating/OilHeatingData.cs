﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class OilHeatingData
    {
        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public string heatingName { get; set; }

        [DataMember]
        public string applianceMake { get; set; }

        [DataMember]
        public string applianceModel { get; set; }

        [DataMember]
        public string applianceSerialNumber { get; set; }

        [DataMember]
        public string applianceLocation { get; set; }

        [DataMember]
        public DateTime? originalInstallDate { get; set; }

        [DataMember]
        public string burnerMake { get; set; }

        [DataMember]
        public string burnerModel { get; set; }

        [DataMember]
        public int? burnerTypeId { get; set; }

        [DataMember]
        public int? tankTypeId { get; set; }

        [DataMember]
        public int? flueTypeId { get; set; }

        [DataMember]
        public int? fuelTypeId { get; set; }

        [DataMember]
        public string certificateName { get; set; }

        [DataMember]
        public DateTime? certificateIssued { get; set; }

        [DataMember]
        public string certificateNumber { get; set; }

        [DataMember]
        public DateTime? certificateRenewel { get; set; }

        [DataMember]
        public int? itemId { get; set; }

        [DataMember]
        public string heatingFuel { get; set; }

        [DataMember]
        public string heatingType { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public OilFiringAndService oilFiringAndService = new OilFiringAndService();

        [DataMember]
        public List<FaultsDataGas> oilDefects = new List<FaultsDataGas>();

    }
}
