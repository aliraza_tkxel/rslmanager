﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Stock
{
    [DataContract]
    [Serializable]
    public class StockInspectionDocument
    {


        [DataMember]
        public int userId { get; set; }
        [DataMember]
        public List<StockAppointmentDetail> appointmentList = new List<StockAppointmentDetail>();
    }

    public class StockAppointmentDetail
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string propertyId { get; set; }




    }
}
