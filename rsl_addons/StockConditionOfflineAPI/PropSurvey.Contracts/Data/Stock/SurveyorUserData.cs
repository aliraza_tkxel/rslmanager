﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SurveyorUserData
    {
        [DataMember]
        public string userName { get; set; }

     
        [DataMember]
        public int? userId { get; set; }

        [DataMember]
        public string fullName { get; set; }

            
        
    }
}
