﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PropSurvey.Contracts.Data.Appointment;
namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AllAppointmentsList : IAppointment
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public String appointmentTitle { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public String heatingFuel { get; set; }

        [DataMember]
        public String appointmentLocation { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public String surveyourAvailability { get; set; }

        [DataMember]
        public int defaultCustomerId { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [IgnoreDataMember]
        public Boolean? appointmentValidity { get; set; }

        [DataMember]
        public string appointmentAbortReason { get; set; }

        [DataMember]
        public string appointmentAbortNotes { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [IgnoreDataMember]
        public String journalSubStatus { get; set; }

        [DataMember]
        public String surveyType { get; set; }

        //[DataMember]
        //public DateTime? loggedDate { get; set; }
        [DataMember]
        public int? createdBy { get; set; }
        [DataMember]
        public string createdByPerson { get; set; }

        [DataMember]
        public int defaultCustomerIndex { get; set; }

        [DataMember]
        public int? tenancyId { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public string surveyorAlert { get; set; }

        [DataMember]
        public string appointmentCalendar { get; set; }

        [IgnoreDataMember]
        public bool appointmentOverdue { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }
               
        public DateTime? appointmentEndDate { get; set; }
               
        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [IgnoreDataMember]
        public String appointmentStartTimeString { get; set; }

        [IgnoreDataMember]
        public String appointmentEndTimeString { get; set; }

        [DataMember]
        public bool? addToCalendar { get; set; }

        [DataMember]
        public bool? isMiscAppointment { get; set; }

        [DataMember]
        public string appointmentShift { get; set; }
        [DataMember]
        public DateTime? repairCompletionDateTime { get; set; }
        [DataMember]
        public int? assignedTo { get; set; }
        [DataMember]
        public string noEntryNotes { get; set; }
        [DataMember]
        public int? updatedBy { get; set; }
        
        [DataMember]
        public SchemeBlockData scheme { get; set; }

        [DataMember]
        public AppointmentInfoData appInfoData = new AppointmentInfoData();

        // cp12Info contains multiple type of certificates (CP11, CP8 etc) but due to huge change on app side 
        // this key cannot be renamed in current ticket.
        [DataMember]
        public List<LGSRData> cp12Info = new List<LGSRData>();
       
        [DataMember]
        public List<JobData> jobDataList = new List<JobData>();
     
        [DataMember]
        public PlannedComponentTrade componentTrade = new PlannedComponentTrade();

        [DataMember]
        public AppointmentSurveyData survey = new AppointmentSurveyData();

        [DataMember]
        public JournalData journal = new JournalData();

        [DataMember]
        public List<FaultBasket> faultReportedList = new List<FaultBasket>();


        [DataMember]
        public List<PropertyAttributeData> propertyAttributeNotes { get; set; }  
   
        public bool validateAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyourAvailabilityStatus(string status)
        {
            if (Enum.IsDefined(typeof(SurveyourAvailabilityStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateAppointmentTypes(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyTypes(string status)
        {
            if (Enum.IsDefined(typeof(SurveyTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public AllAppointmentsList()
        {
            appointmentStartDateTime = DateTime.Parse("01/01/1970");
            appointmentEndDateTime = DateTime.Parse("01/01/1970");
            appointmentDate = DateTime.Parse("01/01/1970");
            appointmentEndDate = DateTime.Parse("01/01/1970");
        }
    }
    public enum AppointmentCompleteStatus
    {
        NotStarted,
        InProgress,
        NoEntry,
        Finished,
        Complete,
        Completed,
        Paused,
        Cancel,
        Cancelled,
        Aborted,
        Accepted
    }
    public enum SurveyourAvailabilityStatus
    {
        Busy,
        Available,
        Free,
        Tentative,
        OutOfOffice
    }
    public enum SurveyTypes
    {
        Appointments,
        ConditionSurveys,
        WorkOrders
    }
    public enum AppointmentTypes
    {
        Stock,
        Pre,
        Post,
        Void,
        Gas,
        VoidGas,
        Fault,
        Planned,
        Miscellaneous,
        Adaptation,
        Condition
    }
    public enum AppointmentProgressStatus
    {
        NotStarted,
        InProgress,
        NoEntry,
        Finished
    }
    public enum JournalProgressStatus
    {
        
        Arranged,
        Completed,
        Cancelled,
        InProgress,
        AssignedToContractor,
        NoEntry
    }
    public enum VoidAppointmentTypes
    {
        VoidInspection,
        PostVoidInspection,
        VoidWorks,
        VoidGasCheck,
        VoidElectricCheck
    }
}