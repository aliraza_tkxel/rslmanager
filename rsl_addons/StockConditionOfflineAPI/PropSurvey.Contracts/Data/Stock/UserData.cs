﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class UserData
    {
        [DataMember]
        public string userName { get; set; }

        [DataMember]
        public int? isActive { get; set; }

        [DataMember]
        public int? userId { get; set; }

        [DataMember]
        public DateTime? lastLoggedInDate { get; set; }

        [DataMember]
        public string salt { get; set; }

        [DataMember]
        public string fullName { get; set; }

            
    }
}
