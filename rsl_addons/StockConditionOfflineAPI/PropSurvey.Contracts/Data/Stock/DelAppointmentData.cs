﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class DelAppointmentData
    {
        [DataMember]
        public int appointmentId { get; set; }
        
        [DataMember]
        public bool isDeleted { get; set; }

           
        
    }
}
