﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added  - 24/05/2013
    public class ResultIntData
    {
        [DataMember]
        public int result { get; set; }
    }
}
