﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    public class AttributeGeneralInfo
    {
        public int? heatingId { get; set; }

        public string propertyId { get; set; }
          
        public int? schemeId { get; set; }
        
        public int? blockId { get; set; }

    }
}
