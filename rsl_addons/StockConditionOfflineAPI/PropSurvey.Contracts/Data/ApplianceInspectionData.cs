﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceInspectionData
    {
        [DataMember]
        public int? inspectionID { get; set; }

        [DataMember]
        public int? APPLIANCEID { get; set; }

        [DataMember]
        public int APPOINTMENTID { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public string combustionReading { get; set; }

        [DataMember]
        public string operatingPressure { get; set; }

        [DataMember]
        public string safetyDeviceOperational { get; set; }

        [DataMember]
        public string spillageTest { get; set; }

        [DataMember]
        public string smokePellet { get; set; }

        [DataMember]
        public string adequateVentilation { get; set; }

        [DataMember]
        public string flueVisualCondition { get; set; }

        [DataMember]
        public string satisfactoryTermination { get; set; }

        [DataMember]
        public string fluePerformanceChecks { get; set; }

        [DataMember]
        public string applianceServiced { get; set; }

        [DataMember]
        public string applianceSafeToUse { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public string operatingPressureUnit { get; set; }

        [DataMember]
        public int inspectedBy { get; set; }

        [DataMember]
        public int? JOURNALID { get; set; }



        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA
            
        }
    }
}
