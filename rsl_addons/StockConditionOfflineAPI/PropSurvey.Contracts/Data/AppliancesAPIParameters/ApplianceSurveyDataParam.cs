﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceSurveyDataParam
    {
        [DataMember]
        public string propertyId { get; set; }
        [DataMember]
        public int? schemeId { get; set; }
        [DataMember]
        public int? blockId { get; set; }
        [DataMember]
        public int appointmentid { get; set; } 
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public int? journalId { get; set; } 
      
    }
}
