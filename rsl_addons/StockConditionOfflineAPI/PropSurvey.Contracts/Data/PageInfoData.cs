﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{

    [DataContract]
    [Serializable]
    public class PageInfoData
    {
        [DataMember]
        public int PageSize { get; set; }

        [DataMember]
        public int PageNumber { get; set; }

        [DataMember]
        public int TotalCount { get; set; }

        [DataMember]
        public int TotalPages { get; set; }
    }
}
