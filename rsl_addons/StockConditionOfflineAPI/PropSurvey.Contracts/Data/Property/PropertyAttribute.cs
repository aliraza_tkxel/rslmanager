﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyAttribute
    {
        [DataMember]
        public string ParameterName { get; set; }

        [DataMember]
        public string ValueDetail { get; set; }

        [DataMember]
        public long? ValueId { get; set; }

        [DataMember]
        public bool? IsCheckBoxSelected { get; set; }

        [DataMember]
        public int itemID { get; set; }

        [DataMember]
        public Nullable<int> updatedBy { get; set; }

        [DataMember]
        public bool isDate { get; set; }

        [DataMember]
        public Nullable<DateTime> dateValue { get; set; }
    }
}
