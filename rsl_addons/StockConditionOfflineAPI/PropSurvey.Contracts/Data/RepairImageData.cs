﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class RepairImageData
    {
        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public DateTime? createdOn { get; set; }
        
        [DataMember]
        public int faultRepairImageId { get; set; }

        [DataMember]
        public string imageName { get; set; }

        [DataMember]
        public string imagePath { get; set; }

        [DataMember]
        public bool? isBeforeImage { get; set; }

        [DataMember]
        public string jsNumber { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }
        
        public RepairImageData()
        {
            createdOn = DateTime.Parse("01/01/1999");
        }

    }

}
