﻿// -----------------------------------------------------------------------
// <copyright file="FaultImagesData.cs" >
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PropSurvey.Contracts.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    /// <summary>
    /// Data Contract for Fault Images.
    /// </summary>
    public class FaultImagesData
    {
        [DataMember]
        public string ImageTitle { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public int? PropertyDefectImageId { get; set; }
        [DataMember]
        public int? PropertyDefectId { get; set; }
    }
    
    /// <summary>
    /// Contract data for Delete Defect Image 
    /// </summary>
    [DataContract]
    [Serializable]
    public class RequestDeleteDefectImageData
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
        [DataMember]
        public int faultImageId { get; set; }
        [DataMember]
        public string propertyId { get; set; }
    }
}
