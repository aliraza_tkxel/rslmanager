﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppointmentInfoData
    {
        [DataMember]
        public int totalNoEntries { get; set; }

        [DataMember]
        public int totalAppointments{ get; set; }
        [DataMember]
        public bool? isCardLeft { get; set; }
    }
}
