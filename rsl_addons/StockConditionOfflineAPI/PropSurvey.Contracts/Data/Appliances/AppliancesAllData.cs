﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppliancesAllData
    {
        //[DataMember]
        //public int ApplianceID { get; set; }

        //[DataMember]
        //public string PropertyID { get; set; }

        [DataMember]
        public List<ApplianceTypeData> ApplianceType = new List<ApplianceTypeData>();

        [DataMember]
        public List<ManufacturerData> ApplianceManufacturer = new List<ManufacturerData>();

        [DataMember]
        public List<AppliancesLocationData> ApplianceLocation = new List<AppliancesLocationData>();

        [DataMember]
        public List<ApplianceModelData> ApplianceModel = new List<ApplianceModelData>();
        
        public bool validateApplianceFlueType(string FlueType)
        {
            if (Enum.IsDefined(typeof(ApplianceFlueType), FlueType) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum ApplianceFlueType
        {
            RS,
            OF,
            FL
        }
    }

    //TODO: Move the below classed to seperate files
    [DataContract]
    [Serializable]
    public class DefectsPriority
    {
        [DataMember]
        public int priorityId { get; set; }

        [DataMember]
        public string priority { get; set; }        
    }

    [DataContract]
    [Serializable]
    public class Trades
    {
        [DataMember]
        public int tradeId { get; set; }

        [DataMember]
        public string trade { get; set; }
    }

    [DataContract]
    [Serializable]
    public class PartsOrderedBy
    {
        [DataMember]
        public int employeeId { get; set; }

        [DataMember]
        public string employeeName { get; set; }        
    }

}
