﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppliancesLocationData
    {
        [DataMember]
        public int? LocationID { get; set; }

        [DataMember]
        public string Location { get; set; }
    }
}
