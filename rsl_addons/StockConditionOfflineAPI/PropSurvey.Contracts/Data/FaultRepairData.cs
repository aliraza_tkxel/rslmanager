﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultRepairData
    {
        [DataMember]
        public int? faultRepairId { get; set; }

        [DataMember]
        public string description { get; set; }

    }

    [DataContract]
    [Serializable]
    public class FaultRepairsData
    {
        [DataMember]
        public int? faultRepairId { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public int isAssociated { get; set; }

    }

    [DataContract]
    [Serializable]
    public class RepairsData
    {
        [DataMember]
        public int? faultRepairId { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public int isAssociated { get; set; }

        [DataMember]
        public double? gross { get; set; }

    }

/// <summary>
/// To get fault repair list
/// </summary>
    [DataContract]
    [Serializable]
    public class RequestFault
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
    }

    /// <summary>
    /// To get fault repair list
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponseFaultRepairData
    {
        [DataMember]
        public List<FaultRepairsData> faultRepairList = new List<FaultRepairsData>();
    }

    /// <summary>
    /// To get fault repair list
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponsePauseReasonList
    {
        [DataMember]
        public List<string> pauseReasonList = new List<string>();
    }

    /// <summary>
    /// To get fault repair list
    /// </summary>
    [DataContract]
    [Serializable]
    public class RequestFaultRepair
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
        [DataMember]
        public int faultLogId { get; set; }
    }

}
