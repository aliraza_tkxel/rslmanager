﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class DetectorType
    {
        [DataMember]
        public int detectorTypeId { get; set; }

        [DataMember]
        public string detectorType { get; set; }
    }

}
