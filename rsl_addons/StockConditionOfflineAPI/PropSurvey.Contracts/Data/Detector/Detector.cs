﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class Detector
    {
        [DataMember]
        public Nullable<DateTime> batteryReplaced { get; set; }

        [DataMember]
        public List<FaultsDataGas> detectorDefects = new List<FaultsDataGas>();
        
        [DataMember]
        public int? detectorId { get; set; }
        
        [DataMember]
        public int? detectorTypeId { get; set; }

        [DataMember]
        public string detectorType { get; set; }

        [DataMember]
        public int? inspectedBy { get; set; }

        [DataMember]
        public Nullable<DateTime> inspectionDate { get; set; }

        [DataMember]
        public int? installedBy { get; set; }
        
        [DataMember]
        public DateTime? installedDate { get; set; }
        
        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public bool? isLandlordsDetector { get; set; }

        [DataMember]
        public Nullable<bool> isPassed { get; set; }

        [DataMember]
        public Nullable<DateTime> lastTestedDate { get; set; }

        [DataMember]
        public string location { get; set; }

        [DataMember]
        public string manufacturer { get; set; }
        
        [DataMember]
        public string notes { get; set; }

        [DataMember]
        public int? powerTypeId { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }

        [DataMember]
        public string serialNumber { get; set; }
        
        [DataMember]
        public Nullable<int> testedBy { get; set; }
                
        [DataMember]
        public Nullable<int> journalId { get; set; }
                
    }
}
