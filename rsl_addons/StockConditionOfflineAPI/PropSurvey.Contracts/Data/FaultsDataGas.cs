﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultsDataGas
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }

        [DataMember]
        public int? faultCategory { get; set; }

        [DataMember]
        public int? JournalId { get; set; }

        [DataMember]
        public bool? isDefectIdentified { get; set; }

        [DataMember]
        public string defectNotes { get; set; }

        [DataMember]
        public bool? isRemedialActionTaken { get; set; }

        [DataMember]
        public string remedialActionNotes { get; set; }

        [DataMember]
        public bool? isAdviceNoteIssued { get; set; }
        
        [DataMember]
        public string warningNoteSerialNo { get; set; }
        
        [DataMember]
        public int? ApplianceID { get; set; }

        [DataMember]
        public string serialNumber { get; set; }

        [DataMember]
        public bool? warningTagFixed { get; set; }

        [DataMember]
        public DateTime? defectDate { get; set; }

        [DataMember]
        public int? detectorTypeId { get; set; }

        [DataMember]
        public long? boilerTypeId { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public string defectType { get; set; }

        [DataMember]
        public string gasCouncilNumber { get; set; }

        [DataMember]
        public bool? isDisconnected { get; set; }

        [DataMember]
        public bool? isPartsRequired { get; set; }

        [DataMember]
        public bool? isPartsOrdered { get; set; }

        [DataMember]
        public int? partsOrderedBy { get; set; }

        [DataMember]
        public DateTime? partsDueDate { get; set; }

        [DataMember]
        public string partsDescription { get; set; }

        [DataMember]
        public string partsLocation { get; set; }

        [DataMember]
        public bool? isTwoPersonsJob { get; set; }

        [DataMember]
        public string reasonForTwoPerson { get; set; }

        [DataMember]
        public decimal? duration { get; set; }

        [DataMember]
        public int? priorityId { get; set; }

        [DataMember]
        public int? tradeId { get; set; }

        [DataMember]
        public bool? IsCustomerHaveHeating { get; set; }

        [DataMember]
        public bool? IsHeatersLeft { get; set; }

        [DataMember]
        public int? NumberOfHeatersLeft { get; set; }

        [DataMember]
        public bool? IsCustomerHaveHotWater { get; set; }

        [DataMember]
        public string NoEntryNotes { get; set; }

        //[DataMember(IsRequired = false, EmitDefaultValue = false)]
        //public List<Tuple<Dictionary<string, int>, Dictionary<string, string>>> defectImages = new List<Tuple<Dictionary<string, int>, Dictionary<string, string>>>();
    }

    [DataContract]
    [Serializable]
    public class RequestDefectDataGas
    {
        [DataMember]
        public string username { get; set; }

        [DataMember]
        public string salt { get; set; }

        [DataMember]
        public FaultsDataGas defect = new FaultsDataGas();
    }

    //[DataContract]
    //public class DefectImages
    //{
    //    [DataMember]
    //    public int defectImageId { get; set; }

    //    [DataMember]
    //    public string defectImagePath { get; set; }
    //}
}
