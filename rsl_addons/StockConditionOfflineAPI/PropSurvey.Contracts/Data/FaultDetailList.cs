﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
   public  class FaultDetailList
    {
        [DataMember]
        public int faultId { get; set; }

        [DataMember]
        public string faultDescription { get; set; }

        [DataMember]
        public string  responseTime { get; set; }

        [DataMember]
        public double? gross { get; set; }

        [DataMember]
        public bool? isRecharge { get; set; }

        [DataMember]
        public string priorityName { get; set; }

        public bool? days { get; set; }

        public int? response { get; set; }
    }
}
