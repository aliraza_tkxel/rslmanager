﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AlternativeSurveyData
    {
        [DataMember]
        public List<AlternativeHeatingData> heatings = new List<AlternativeHeatingData>();
    }
}
