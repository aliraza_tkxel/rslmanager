﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class StatusData
    {
        [DataMember]
        public int statusId { get; set; }

        [DataMember]
        public string title { get; set; }

        [DataMember]
        public int ranking { get; set; }

        [DataMember]
        public DateTime? createdDate { get; set; }

        [DataMember]
        public int createdBy { get; set; }

        [DataMember]
        public DateTime? modifiedDate { get; set; }

        [DataMember]
        public int? modifiedBy { get; set; }

        [DataMember]
        public int? inspectionTypeId { get; set; }

        [DataMember]
        public bool? isEditable { get; set; }

    }
}
