﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class LookupData
    {
        [DataMember]
        public long lookupId { get; set; }

        [DataMember]
        public string value { get; set; }

    }
}
