﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
   
    public class DetectorInspectionData
    {
        [DataMember]
        public int inspectionID { get; set; }    

        [DataMember]
        public string detectorTest { get; set; }
        [DataMember]
        public DateTime? inspectionDate { get; set; }
        [DataMember]
        public int inspectedBy { get; set; }


      
    }
}
