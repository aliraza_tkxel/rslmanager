﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class JobData
    {
        [DataMember]
        public string JSNumber { get; set; }

        [DataMember]
        public int faultLogID { get; set; }

        [DataMember]
        public string JSNDescription { get; set; }

        [DataMember]
        public string JSNNotes { get; set; }

        [DataMember]
        public string JSNLocation { get; set; }

        [DataMember]
        public string repairNotes { get; set; }

        [DataMember]
        public double? duration { get; set; }
        [DataMember]
        public string durationUnit { get; set; }

        [DataMember]
        public DateTime? reportedDate { get; set; }

        [DataMember]
        public string responseTime { get; set; }

        [DataMember]
        public string priority { get; set; }

        [DataMember]
        public bool? isFollowOnRequired { get; set; }

        [DataMember]
        public string jobStatus { get; set; }

        [DataMember]
        public DateTime? completionDate { get; set; }

        [DataMember]
        public string jsCompletedAppVersion { get; set; }     
  
        [DataMember]
        public string jsCurrentAppVersion { get; set; }      

        [DataMember]
        public string followOnNotes { get; set; }

        [DataMember]
        public List<FaultRepairData> faultRepairList = new List<FaultRepairData>();

        [DataMember]
        public List<RepairImageData> repairImageList = new List<RepairImageData>();

        [DataMember]
        public List<JobPauseHistoryData> jobPauseHistory = new List<JobPauseHistoryData>();

        public bool validateFaultJobProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(JobProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public enum JobProgressStatus
    {
        NotStarted,
        InProgress,
        Started,
        NoEntry,
        Paused,
        ReStarted,
        Complete
    }
    /// <summary>
    /// To get fault repair list
    /// </summary>
    [DataContract]
    [Serializable]
    public class JobPauseHistoryData
    {
        [DataMember]
        public DateTime? pauseDate { get; set; }
        [DataMember]
        public string pauseNote { get; set; }
        [DataMember]
        public string pauseReason { get; set; }
        [DataMember]
        public int? pausedBy { get; set; }
        [DataMember(IsRequired = false,EmitDefaultValue=false)]
        public string actionType = "";
    }
}
