﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
   public class PropertyAttributeData
    {
        [DataMember]
        public string attributePath { get; set; }

        [DataMember]
        public string attributeNotes { get; set; }
    }
}
