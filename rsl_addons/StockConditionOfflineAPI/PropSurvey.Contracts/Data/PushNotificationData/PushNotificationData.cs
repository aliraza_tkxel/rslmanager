﻿using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    public class PushNotificationData
    {
        [DataMember]
        public string notificationMessage { get; set; }
                
        [DataMember(IsRequired=false,EmitDefaultValue=false)]
        public string deviceToken { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        public int operativeId { get; set; }        
    }
}
