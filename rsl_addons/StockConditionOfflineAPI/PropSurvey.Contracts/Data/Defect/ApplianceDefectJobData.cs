﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    public class ApplianceDefectJobData
    {
        [DataMember]
        public string JSNumber { get; set; } //defectJsdNumber

        [DataMember]
        public string appliance { get; set; }

        [DataMember]
        public int? applianceId { get; set; }

        [DataMember]
        public long? boilerTypeId { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public Nullable<DateTime> completionDate { get; set; }

        [DataMember]
        public string jsCompletedAppVersion { get; set; }

        [DataMember]
        public string jsCurrentAppVersion { get; set; }   

        [DataMember]
        public Nullable<DateTime> defectDate { get; set; }

        [DataMember]
        public int defectCategoryId { get; set; }

        [DataMember]
        public int defectId { get; set; }

        [DataMember]
        public string defectNotes { get; set; }

        [DataMember]
        public Nullable<int> defectRecordedBy { get; set; }

        [DataMember]
        public string gasCouncilNumber { get; set; }

        [DataMember]
        public string inspectionRef { get; set; }

        [DataMember]
        public Nullable<bool> isAdviceNoteIssued { get; set; }

        [DataMember]
        public bool? isCustomerHaveHeating { get; set; }

        [DataMember]
        public bool? isCustomerHaveHotWater { get; set; }

        [DataMember]
        public Nullable<bool> isDisconnected { get; set; }

        [DataMember]
        public bool? isHeatersLeft { get; set; }

        [DataMember]
        public Nullable<bool> isRemedialActionTaken { get; set; }

        [DataMember]
        public bool? isTwoPersonsJob { get; set; }

        [DataMember]
        public List<ApplianceDefectPauseData> jobActivityHistory = new List<ApplianceDefectPauseData>(); //applianceDefectPauseHistory

        public List<RepairImageData> jobSheetImageList = new List<RepairImageData>();

        [DataMember]
        public string jobStatus { get; set; }

        [DataMember]
        public string make { get; set; }

        [DataMember]
        public string model { get; set; }

        [DataMember]
        public Nullable<int> numberOfHeatersLeft { get; set; }

        [DataMember]
        public DateTime? partsDueDate { get; set; }

        [DataMember]
        public string partsLocation { get; set; }

        [DataMember]
        public Nullable<int> partsOrderedBy { get; set; }

        [DataMember]
        public string remedialActionNotes { get; set; }

        [DataMember]
        public string serialNumber { get; set; }

        [DataMember]
        public string trade { get; set; }

        [DataMember]
        public Nullable<bool> warningTagFixed { get; set; }
        [DataMember]
        public string warningNoteSerialNo { get; set; }

        [DataMember]
        public DateTime? jsActualStartTime { get; set; }

        [DataMember]
        public DateTime? jsActualEndTime { get; set; }

        [DataMember]
        public string defectCompletionNotes { get; set; }


    }
}
