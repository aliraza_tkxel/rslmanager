﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
   public  class FaultArea
    {
        [DataMember]
        public int faultAreaId { get; set; }

        [DataMember]
        public string faultAreaName { get; set; }

    }
}
