﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyData
    {
        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }

        [DataMember]
        public string schemeName { get; set; }

        [DataMember]
        public string blockName { get; set; }

        [DataMember]
        public int? tenancyId { get; set; } // type changed to int? from int  - 18/06/2013

        [DataMember]
        public string houseNumber { get; set; }

        [DataMember]
        public string flatNumber { get; set; }

        [DataMember]
        public string address1 { get; set; }

        [DataMember]
        public string address2 { get; set; }

        [DataMember]
        public string address3 { get; set; }

        [DataMember]
        public int? defaultPropertyPicId { get; set; }

        [DataMember]
        public string townCity { get; set; }

        [DataMember]
        public string postCode { get; set; }

        [DataMember]
        public string county { get; set; }

        [DataMember]
        public DateTime? certificateExpiry { get; set; }

        [DataMember]
        public DateTime? lastSurveyDate { get; set; }

        [DataMember]
        public List<PropertyPictureData> propertyPicture = null;//new List<PropertyPictureData>;

        [DataMember]
        public List<AsbestosData> propertyAsbestosData = null;//new List<PropertyAsbestosData>();

        [DataMember]
        public List<PropertyDimData> Accommodations = null;//new List<PropertyDimData>();

        [DataMember]
        public List<ApplianceData> appliances = new List<ApplianceData>();

        [DataMember]
        public List<BoilerData> boilers = new List<BoilerData>();

        [DataMember]
        public List<AlternativeHeatingData> alternativeHeatings = new List<AlternativeHeatingData>();

        [DataMember]
        public List<OilHeatingData> oilHeatings = new List<OilHeatingData>();

        [DataMember]
        public GeneralHeatingChecks generalHeatingChecks = new GeneralHeatingChecks();        

        [DataMember]
        public List<DetectorCountData> detectors = new List<DetectorCountData>();

        [DataMember]
        public List<Detector> propertyDetectors = new List<Detector>();

        [DataMember]
        public List<Meter> meters = new List<Meter>();
        
        [DataMember]
        public InstallationPipeworkData installationpipework = null;

        public PropertyData()
        {
            certificateExpiry = DateTime.Parse("01/01/1970");
            lastSurveyDate = DateTime.Parse("01/01/1970");
        }

    }
}
