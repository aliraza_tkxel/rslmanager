﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppointmentInProgress
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string appointmentType { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public string status { get; set; }

        [DataMember]
        public List<JobSheetStatus> jobSheets;
    }
}
