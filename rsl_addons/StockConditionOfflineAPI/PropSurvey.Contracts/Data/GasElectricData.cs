﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class GasElectricData
    {
        [DataMember]
        public int? gasMeterTypeId { get; set; }

        [DataMember]
        public long? gasMeterReading { get; set; }

        [IgnoreDataMember]
        public DateTime gasMeterReadingDate { get; set; }

        [DataMember]
        public string gasMeterLocation { get; set; }

        [DataMember]
        public int? tenantTypeId { get; set; }

        [DataMember]
        public int? electricMeterTypeId { get; set; }

        [DataMember]
        public long? electricMeterReading { get; set; }

        [IgnoreDataMember]
        public DateTime electricMeterReadingDate { get; set; }

        [DataMember]
        public string electricMeterLocation { get; set; }
        

    }
}
