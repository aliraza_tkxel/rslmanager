﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class BoilerData
    {
        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public string boilerName { get; set; }

        [DataMember]
        public int? flueTypeId { get; set; }

        [DataMember]
        public string gcNumber { get; set; }

        [DataMember]
        public int? itemId { get; set; }

        [DataMember]
        public bool? isLandlordAppliance { get; set; }

        [DataMember]
        public DateTime? lastReplaced { get; set; }

        [DataMember]
        public DateTime? replacementDue { get; set; }

        [DataMember]
        public DateTime? gasketReplacementDate { get; set; }
        
        [DataMember]
        public string location { get; set; }

        [DataMember]
        public int? manufacturerId { get; set; }

        [DataMember]
        public string model { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }

        [DataMember]
        public string serialNumber { get; set; }

        [DataMember]
        public int? boilerTypeId { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public BoilerInspectionData BoilerInspection = new BoilerInspectionData();

        [DataMember]
        public List<FaultsDataGas> BoilerDefects = new List<FaultsDataGas>();

        [DataMember]
        public InstallationPipeworkData InstallationPipework = new InstallationPipeworkData();
    }
}
