﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class BoilerManufacturer
    {
        [DataMember]
        public long manufacturerId { get; set; }

        [DataMember]
        public string manufacturerDescription { get; set; }

        [DataMember]
        public bool? isSchemeBlock { get; set; }

        [DataMember]
        public bool? isAlternativeHeating { get; set; }

    }
}
