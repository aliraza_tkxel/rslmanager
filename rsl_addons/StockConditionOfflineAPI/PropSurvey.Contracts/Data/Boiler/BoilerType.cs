﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class BoilerType
    {
        [DataMember]
        public long boilerTypeId { get; set; }

        [DataMember]
        public string boilerTypeDescription { get; set; }

        [DataMember]
        public bool? isSchemeBlock { get; set; }
    }
}
