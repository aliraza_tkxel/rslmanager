﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class BoilerInspectionData
    {
        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public string combustionReading { get; set; }

        [DataMember]
        public string operatingPressure { get; set; }

        [DataMember]
        public string safetyDeviceOperational { get; set; }

        [DataMember]
        public string smokePellet { get; set; }

        [DataMember]
        public string adequateVentilation { get; set; }

        [DataMember]
        public string flueVisualCondition { get; set; }

        [DataMember]
        public string satisfactoryTermination { get; set; }

        [DataMember]
        public string fluePerformanceChecks { get; set; }

        [DataMember]
        public string boilerServiced { get; set; }

        [DataMember]
        public string boilerSafeToUse { get; set; }

        [DataMember]
        public int? inspectionId { get; set; }

        [DataMember]
        public int inspectedBy { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public string spillageTest { get; set; }

        [DataMember]
        public string operatingPressureUnit { get; set; }

        [DataMember]
        public long? boilerTypeId { get; set; }

        [DataMember]
        public int? heatingId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int? journalId { get; set; }


        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA

        }

    }
}
