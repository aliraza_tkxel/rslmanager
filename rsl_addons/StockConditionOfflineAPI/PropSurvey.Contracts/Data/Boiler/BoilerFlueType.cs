﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class BoilerFlueType
    {
        [DataMember]
        public long flueTypeId { get; set; }

        [DataMember]
        public string flueTypeDescription { get; set; }

    }
}
