﻿
using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class UpdatePasswordParam
    {
        [DataMember]
        public string username { get; set; }

        [DataMember]
        public string oldPassword { get; set; }

        [DataMember]
        public string newPassword { get; set; }

        [DataMember]
        public string devicetoken { get; set; }  
    }
}
