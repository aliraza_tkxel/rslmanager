﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.StockAPIParameters
{
    [DataContract]
    [Serializable]
    public class ExistingAppointmentParam
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string appointmentType { get; set; }
        
        [DataMember]
        public DateTime creationDate { get; set; }

        [DataMember]
        public DateTime startDate { get; set; }

        [DataMember]
        public DateTime endDate { get; set; }

        [DataMember]
        public string startTime { get; set; }

        [DataMember]
        public string endTime { get; set; }

        [DataMember]
        public int? operativeId { get; set; }
    }

    [DataContract]
    [Serializable]
    public class DeleteAppointmentParam
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string appointmentType { get; set; }

       
    }
}
