﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SurveyorParam
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
        [DataMember]
        public int applicationtype { get; set; }
       
    }
}
