﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class JobSheetStatus
    {
        [DataMember]
        public string jobSheetNumber { get; set; }

        [DataMember]
        public string status { get; set; }

        [DataMember]
        public string reason { get; set; }

        [DataMember]
        public string notes { get; set; }

    }
}
