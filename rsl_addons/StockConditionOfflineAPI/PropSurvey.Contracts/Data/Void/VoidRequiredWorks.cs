﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class VoidRequiredWorks
    {

        [DataMember]
        public bool? isWorksRequired { get; set; }
        
        //List of Required Works
        [DataMember]
        public List<VoidRecordRequiredWorks> recordWorks = new List<VoidRecordRequiredWorks>();

       


    }
}
