﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class RoomList
    {
        [DataMember]
        public Int32? roomId { get; set; }

        [DataMember]
        public string name { get; set; }


    }


    [Serializable]
    [DataContract]
    public class ElectricMeterType
    {
        [DataMember]
        public long meterId { get; set; }

        [DataMember]
        public string meterType { get; set; }
    }


    [Serializable]
    [DataContract]
    public class GasAbortReason
    {
        [DataMember]
        public int AbortReasonId { get; set; }

        [DataMember]
        public string AbortReason { get; set; }
    }

    [Serializable]
    [DataContract]
    public class GasMeterType
    {
        [DataMember]
        public long meterId { get; set; }

        [DataMember]
        public string meterType { get; set; }
    }
}
