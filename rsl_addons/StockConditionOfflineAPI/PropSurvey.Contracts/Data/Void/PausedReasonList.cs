﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class PausedReasonList
    {
        [DataMember]
        public int pauseId { get; set; }

        [DataMember]
        public string reason { get; set; }
    }
}
