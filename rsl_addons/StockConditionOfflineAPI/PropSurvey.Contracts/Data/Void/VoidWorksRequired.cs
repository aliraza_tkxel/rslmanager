﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class VoidWorksRequired
    {

        [DataMember]
        public bool? isWorksRequired { get; set; }
        
        //List of Required Works
        [DataMember]
        public List<VoidRecordWorksRequired> recordWorks = new List<VoidRecordWorksRequired>();

        [DataMember]
        public double? totalNeglectEstimate { get; set; }


    }

}
