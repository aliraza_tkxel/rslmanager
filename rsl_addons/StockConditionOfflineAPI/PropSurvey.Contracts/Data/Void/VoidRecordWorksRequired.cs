﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidRecordWorksRequired
    {
        [DataMember(Name = "faultLogID")]
        public Int32? roomId { get; set; }

        [DataMember(Name = "JSNDescription")]
        public string workDescription { get; set; }

        [DataMember(Name = "jsType")]
        public string workType { get; set; }

        [DataMember(Name = "JSNLocation")]
        public string workLocation { get; set; }

        [DataMember(Name = "JSNNotes")]
        public string worksNotes { get; set; }

        [DataMember(Name = "JSNumber")]
        public string voidJSV { get; set; }

        [IgnoreDataMember]
        public int? requiredWorksId { get; set; }

        [DataMember]
        public DateTime? completionDate { get; set; }

        [DataMember]
        public string jsCompletedAppVersion { get; set; }

        [DataMember]
        public string jsCurrentAppVersion { get; set; }

        [DataMember]
        public double? duration { get; set; }

        [DataMember]
        public bool? isFollowOnRequired { get; set; }

        [DataMember]
        public bool? isLegionella { get; set; }

        [DataMember]
        public string repairNotes { get; set; }

        [DataMember]
        public string followOnNotes { get; set; }

        [DataMember(Name = "faultRepairList")]
        public List<FaultRepairData> voidRepairList = new List<FaultRepairData>();

        [DataMember(Name = "jobPauseHistory")]
        public List<VoidPauseData> workPauseHistory = new List<VoidPauseData>();

        [DataMember]
        public List<RepairImageData> repairImageList = new List<RepairImageData>();

        [DataMember(Name = "jobStatus")]
        public string workStatus { get; set; }

        [DataMember]
        public int? faultAreaId { get; set; }

        [DataMember]
        public int? faultId { get; set; }

        [DataMember]
        public int? repairId { get; set; }

        [DataMember]
        public bool? isRecurringProblem { get; set; }

        [DataMember]
        public string faultNotes { get; set; }

        [DataMember]
        public int? problemDays { get; set; }

        [DataMember]
        public DateTime? jsActualStartTime { get; set; }

        [DataMember]
        public DateTime? jsActualEndTime { get; set; }
    }
}
