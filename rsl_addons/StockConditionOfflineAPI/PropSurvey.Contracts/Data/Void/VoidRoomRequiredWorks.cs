﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidRecordRequiredWorks
    {
        [DataMember]
        public Int32? roomId { get; set; }

        [DataMember]
        public string description { get; set; }

        [DataMember]
        public decimal? tenantNeglectEstimate { get; set; }

        [DataMember]
        public bool? isBRSWork { get; set; }

        [DataMember]
        public bool? isTenantWork { get; set; }

        [DataMember]
        public string status { get; set; }
    }
}
