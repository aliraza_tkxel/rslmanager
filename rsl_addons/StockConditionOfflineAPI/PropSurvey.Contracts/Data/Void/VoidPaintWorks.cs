﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidPaintPackWorks
    {
        [DataMember]
        public bool? isPaintPackRequired { get; set; }

        [DataMember]
        public List<int> roomList = new List<int>();
    }
}
