﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidRecordRequiredWorks
    {
        [DataMember]
        public Int32? roomId { get; set; }

        [DataMember]
        public string workDescription { get; set; }

        [DataMember]
        public decimal? estimate { get; set; }

        [DataMember]
        public bool? isBRSWork { get; set; }

        [DataMember]
        public bool? isTenantWork { get; set; }

        [DataMember]
        public bool? isVerified { get; set; }

        [DataMember]
        public string status { get; set; }

        [DataMember]
        public int? worksId { get; set; }

        [DataMember]
        public int? faultAreaId { get; set; }

        [DataMember]
        public int? faultId { get; set; }

        [DataMember]
        public int? repairId { get; set; }
        
        [DataMember]
        public bool? isRecurringProblem { get; set; }
        
        [DataMember]
        public string repairNotes { get; set; }
        
        [DataMember]
        public int? problemDays { get; set; }

    }
}
