﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Void
{
    [Serializable]
    [DataContract]
    public class VoidGasElectricData : IAppointment
    {
        [DataMember]
        public DateTime? reletDate { get; set; }

        [DataMember]
        public DateTime? terminationDate { get; set; }

        [DataMember]
        public string jsvNumber { get; set; }

        [DataMember]
        public int? appointmentId { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }
        
        //[DataMember]
        //public DateTime? loggedDate { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [IgnoreDataMember]
        public String journalSubStatus { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public string createdByPerson { get; set; }

        [DataMember]
        public string tenantType { get; set; }
                
        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? appointmentEndDate { get; set; }

        [DataMember]
        public JournalData journal = null;

        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [IgnoreDataMember]
        public String appointmentStartTimeString { get; set; }

        [IgnoreDataMember]
        public String appointmentEndTimeString { get; set; }

        [DataMember]
        public int? meterTypeId { get; set; }

        [DataMember]
        public string meterLocation { get; set; }

        [DataMember]
        public long? meterReading { get; set; }

        [DataMember]
        public string noEntryNotes { get; set; }

        [DataMember]
        public int? updatedBy { get; set; }
        
        [DataMember]
        public int? tenancyId { get; set; }
        
        [DataMember]
        public string deviceType { get; set; }

        [DataMember]
        public DateTime? appointmentActualStartTime { get; set; }

        [DataMember]
        public DateTime? appointmentActualEndTime { get; set; }
    }
}
