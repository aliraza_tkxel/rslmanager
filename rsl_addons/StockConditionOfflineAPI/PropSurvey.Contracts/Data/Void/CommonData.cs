﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Void
{
    [Serializable]
    [DataContract]
   public class CommonData
    {
        
        [DataMember]
        public List<GasAbortReason> abortReasons { get; set; }

        [DataMember]
        public List<DefectCategoryData> defectCategories = new List<DefectCategoryData>();

        [DataMember]
        public List<DefectsPriority> defectsPriority = new List<DefectsPriority>();
        
        [DataMember]
        public List<DetectorType> detectorTypes = new List<DetectorType>(); 

        [DataMember]
        public List<RoomList> rooms = new List<RoomList>();

        [DataMember]
        public List<FaultDetailList> faultsDetailList = new List<FaultDetailList>();

        [DataMember]
        public List<FaultArea> faultAreaList = new List<FaultArea>();

        [DataMember]
        public List<ElectricMeterType> electricMeterType = new List<ElectricMeterType>();

        [DataMember]
        public List<GasMeterType> gasMeterType = new List<GasMeterType>();

        [DataMember]
        public List<string> jobPauseReasonList = new List<string>();

        [DataMember]
        public List<PartsOrderedBy> partsOrderedBy = new List<PartsOrderedBy>();
        
        [DataMember]
        public List<Trades> trades = new List<Trades>();
        
        [DataMember]
        public List<PowerType> powerTypes = new List<PowerType>();

        [DataMember]
        public List<BoilerType> boilerTypes = new List<BoilerType>();

        [DataMember]
        public List<BoilerManufacturer> boilerManufacturers = new List<BoilerManufacturer>();

        [DataMember]
        public List<LookupData> mainsGasFlueTypes = new List<LookupData>();

        [DataMember]
        public List<LookupData> oilFlueTypes = new List<LookupData>();

        [DataMember]
        public List<LookupData> oilFuelTypes = new List<LookupData>();

        [DataMember]
        public List<LookupData> tankTypes = new List<LookupData>();

        [DataMember]
        public List<LookupData> burnerTypes = new List<LookupData>();

        [DataMember]
        public List<LookupData> solarTypes = new List<LookupData>();

        [DataMember]
        public List<RepairsData> repairList = new List<RepairsData>();
    }    
}
