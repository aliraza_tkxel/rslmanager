﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidPauseData
    {
        [DataMember]
        public int? requiredWorksId { get; set; }

        [DataMember]
        public DateTime? pauseDate { get; set; }

        [DataMember]
        public string pauseNote { get; set; }

        [DataMember]
        public string pauseReason { get; set; }

        [DataMember]
        public int? pausedBy { get; set; }

        [DataMember]
        public string actionType { get; set; }
    }
}
