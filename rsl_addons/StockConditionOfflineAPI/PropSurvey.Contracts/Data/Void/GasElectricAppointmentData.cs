﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class GasElectricAppointmentData
    {
        [DataMember]
        public DateTime? reletDate { get; set; }

        [DataMember]
        public DateTime? terminationDate { get; set; }

        [DataMember]
        public string jsvNumber { get; set; }

        [DataMember]
        public VoidRequiredWorks worksRequired = new VoidRequiredWorks();

        [DataMember]
        public VoidMajorWorksRequired majorWorksRequired = new VoidMajorWorksRequired();

        [DataMember]
        public VoidPaintPackWorks paintPacks = new VoidPaintPackWorks();

        [DataMember]
        public List<RoomList> rooms = new List<RoomList>();

        [DataMember]
        public VoidPauseData pausedWorks = new VoidPauseData();

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public String appointmentTitle { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public String appointmentLocation { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [IgnoreDataMember]
        public Boolean? appointmentValidity { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [IgnoreDataMember]
        public String journalSubStatus { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public string createdByPerson { get; set; }

        [DataMember]
        public int defaultCustomerId { get; set; }

        [DataMember]
        public List<CustomerData> customerList = new List<CustomerData>();

        [DataMember]
        public int? tenancyId { get; set; }

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? appointmentEndDate { get; set; }

        [DataMember]
        public JournalData journal = null;

        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public int journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [IgnoreDataMember]
        public String appointmentStartTimeString { get; set; }

        [IgnoreDataMember]
        public String appointmentEndTimeString { get; set; }

        [DataMember]
        public List<string> electricMeterType = new List<string>();

        [DataMember]
        public List<string> gasMeterType = new List<string>();

        [DataMember]
        public List<PausedReasonList> pauseReasonList = new List<PausedReasonList>();

        [DataMember]
        public List<FaultRepairData> faultRepairList = new List<FaultRepairData>();

        [DataMember]
        public int defaultCustomerIndex { get; set; }
    }
}
