﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data.StockAPIParameters;

namespace PropSurvey.Contracts.Data.Void
{
   public class AppointmentResponse
    {

       public CommonData commonData = new CommonData();
       public List<IAppointment> appointmentList = new List<IAppointment>();
      public List<DeleteAppointmentParam> deleteAppointments = new List<DeleteAppointmentParam>();
    }
}
