﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PropSurvey.Contracts.Data.Void;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class VoidAppointmentData : IAppointment
    {
        [DataMember]
        public DateTime? reletDate { get; set; }

        [DataMember]
        public DateTime? terminationDate { get; set; }

        [DataMember]
        public string jsvNumber { get; set; }

        [DataMember]
        public VoidRequiredWorks worksRequired = new VoidRequiredWorks();

        [DataMember]
        public VoidWorksRequired voidWorksRequired = new VoidWorksRequired();
        [DataMember]
        public VoidMajorWorksRequired majorWorksRequired = new VoidMajorWorksRequired();

        [DataMember]
        public VoidPaintPackWorks paintPacks = new VoidPaintPackWorks();

        [DataMember]
        public GasElectricData electricGasData = new GasElectricData();

        [DataMember]
        public VoidPauseData pausedWorks = new VoidPauseData();

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public String appointmentTitle { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public String appointmentLocation { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [IgnoreDataMember]
        public Boolean? appointmentValidity { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [IgnoreDataMember]
        public String journalSubStatus { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public string createdByPerson { get; set; }

        [DataMember]
        public int? defaultCustomerId { get; set; }

        [DataMember]
        public int? defaultCustomerIndex { get; set; }
        
        [DataMember]
        public int? tenancyId { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? appointmentEndDate { get; set; }

        [DataMember]
        public JournalData journal = null;

        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public int journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [IgnoreDataMember]
        public String appointmentStartTimeString { get; set; }

        [IgnoreDataMember]
        public String appointmentEndTimeString { get; set; }


        [DataMember]
        public bool? isGasCheckRequired { get; set; }

        [DataMember]
        public bool? isElectricCheckRequired { get; set; }

        [DataMember]
        public bool? isEPCCheckRequired { get; set; }

        [DataMember]
        public bool? isAsbestosCheckRequired { get; set; }

        [DataMember]
        public bool? isWorksRequired { get; set; }

        [DataMember]
        public double? duration { get; set; }

        [DataMember]
        public DateTime? gasCheckDate { get; set; }
        [DataMember]
        public string gasCheckStatus { get; set; }

        [DataMember]
        public DateTime? electricCheckDate { get; set; }
        [DataMember]
        public string electricCheckStatus { get; set; }
    }
}
