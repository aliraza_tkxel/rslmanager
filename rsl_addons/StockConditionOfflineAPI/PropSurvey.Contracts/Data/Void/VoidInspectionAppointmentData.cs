﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Void
{
    [Serializable]
    [DataContract]
    public class VoidInspectionAppointmentData : IAppointment
    {
        [DataMember]
        public DateTime? reletDate { get; set; }

        [DataMember]
        public DateTime? terminationDate { get; set; }

        [DataMember]
        public string jsvNumber { get; set; }
        
        [DataMember]
        public VoidMajorWorksRequired majorWorksRequired = new VoidMajorWorksRequired();
        
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        //[DataMember]
        //public DateTime? loggedDate { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [IgnoreDataMember]
        public String journalSubStatus { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public string createdByPerson { get; set; }

        [DataMember]
        public int? defaultCustomerId { get; set; }
                
        [DataMember]
        public int? tenancyId { get; set; }
        
        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? appointmentEndDate { get; set; }

        [DataMember]
        public JournalData journal = new JournalData();

        [DataMember]
        public int journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [DataMember]
        public String appointmentStartTimeString { get; set; }

        [DataMember]
        public String appointmentEndTimeString { get; set; }

        [DataMember]
        public int defaultCustomerIndex { get; set; }

        [DataMember]
        public bool? isGasCheckRequired { get; set; }

        [DataMember]
        public bool? isElectricCheckRequired { get; set; }

        [DataMember]
        public bool? isEPCCheckRequired { get; set; }

        [DataMember]
        public bool? isAsbestosCheckRequired { get; set; }

        [DataMember]
        public string noEntryNotes { get; set; }
        
        [DataMember]
        public int? updatedBy { get; set; }
        
        [DataMember]
        public VoidRequiredWorks worksRequired = new VoidRequiredWorks();

        [DataMember]
        public VoidPaintPackWorks paintPacks = new VoidPaintPackWorks();
        
        [DataMember]
        public DateTime? gasCheckDate { get; set; }
        
        [DataMember]
        public string gasCheckStatus { get; set; }
        
        [DataMember]
        public DateTime? electricCheckDate { get; set; }
        
        [DataMember]
        public string electricCheckStatus { get; set; }

        [DataMember]
        public DateTime? appointmentActualStartTime { get; set; }

        [DataMember]
        public DateTime? appointmentActualEndTime { get; set; }
    }
}
