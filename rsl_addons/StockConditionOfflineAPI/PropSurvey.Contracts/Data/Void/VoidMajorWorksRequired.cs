﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidMajorWorksRequired
    {
        [DataMember]
        public bool? isMajorWorkRequired { get; set; }

        [DataMember]
        public List<VoidRecordMajorWorks> propertyComponents = new List<VoidRecordMajorWorks>();


        [DataMember]
        public List<VoidRecordMajorWorks> recordMajorWork = new List<VoidRecordMajorWorks>();
    }
}
