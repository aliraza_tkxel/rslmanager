﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Void
{
    [Serializable]
    [DataContract]
    public class VoidWorksAppointmentData : IAppointment
    {
        [DataMember]
        public DateTime? reletDate { get; set; }

        [DataMember]
        public DateTime? terminationDate { get; set; }

        [DataMember]
        public string jsvNumber { get; set; }

        [DataMember]
        public List<VoidRecordWorksRequired> jobDataList = new List<VoidRecordWorksRequired>();
        [DataMember]
        public int? appointmentId { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        //[DataMember]
        //public DateTime? loggedDate { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public string createdByPerson { get; set; }

        [DataMember]
        public int? defaultCustomerId { get; set; }
        
        [DataMember]
        public int? tenancyId { get; set; }
                
        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? appointmentEndDate { get; set; }

        [DataMember]
        public JournalData journal = null;

        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [DataMember]
        public String appointmentStartTimeString { get; set; }

        [DataMember]
        public String appointmentEndTimeString { get; set; }

        [DataMember]
        public int? defaultCustomerIndex { get; set; }

        [DataMember]
        public int? updatedBy { get; set; }

        [DataMember]
        public double? duration { get; set; }

        [DataMember]
        public string  noEntryNotes { get; set; }

    }
}
