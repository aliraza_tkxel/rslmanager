﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [Serializable]
    [DataContract]
    public class VoidRecordMajorWorks
    {
        [DataMember]
        public Int32? componentId { get; set; }

        [DataMember]
        public string component { get; set; }

        [DataMember]
        public DateTime?  replacementDue { get; set; }

        [DataMember]
        public string condition { get; set; }

        [DataMember]
        public string notes { get; set; }

        
    }
}
