﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
  public class CreateAppliancesData
    {
        [DataMember]
        public string salt { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public ApplianceData ApplianceData = new ApplianceData();
    }
}
