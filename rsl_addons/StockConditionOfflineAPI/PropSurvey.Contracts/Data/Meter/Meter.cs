﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class Meter
    {
        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; }
        
        [DataMember]
        public string deviceType { get; set; }

        [DataMember]
        public int deviceTypeId { get; set; }

        //[DataMember]
        //public string meterType { get; set; }

        [DataMember]
        public Nullable<long> meterTypeId { get; set; }

        [DataMember]
        public string location { get; set; }

        [DataMember]
        public string manufacturer { get; set; }

        [DataMember]
        public string serialNumber { get; set; }

        [DataMember]
        public DateTime? installedDate { get; set; }
        
        [DataMember]
        public Nullable<decimal> reading { get; set; }

        [DataMember]
        public DateTime? readingDate { get; set; }

        [DataMember]
        public Nullable<bool> isPassed { get; set; }

        [DataMember]
        public string notes { get; set; }

        [DataMember]
        public Nullable<bool> isCapped { get; set; }

        [DataMember]
        public string lastReading { get; set; }

        [DataMember]
        public DateTime? lastReadingDate { get; set; }

        [DataMember]
        public Nullable<int> inspectedBy { get; set; }

        [DataMember]
        public Nullable<DateTime> inspectionDate { get; set; }

        [DataMember]
        public Nullable<bool> isInspected { get; set; }
    }
}
