﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class Meters
    {
        [DataMember]
        public Meter ElectricMeter = new Meter();

        [DataMember]
        public Meter GasMeter = new Meter();
    }
}
