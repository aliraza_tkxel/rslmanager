﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{    
    [DataContract]
    [Serializable]
    public class AppointmentDataFault
    {
        [DataMember]
        public int appointmentId { get; set; }
        [DataMember]
        public string appointmentType { get; set; }       
        [DataMember]
        public CustomerData customer = new CustomerData();
        [DataMember]
        public DateTime? appointmentDate { get; set; }       
        [DataMember]
        public DateTime? repairCompletionDateTime { get; set; }
        [DataMember]
        public string appointmentStartTime { get; set; }
        [DataMember]
        public string appointmentEndTime { get; set; }
        [DataMember]
        public string appointmentStatus { get; set; }
        [DataMember]
        public string jobStatus { get; set; }
        [DataMember]
        public string appointmentNotes { get; set; }
        [DataMember]
        public string repairNotes { get; set; }
        [DataMember]
        public string noEntryNotes { get; set; }
        [DataMember]
        public string surveyorUserName { get; set; }        
        [DataMember]
        public List<JobData> JobDataList = new List<JobData>();
        public bool validateFaultAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(FaultAppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }       
    }
    public enum FaultAppointmentProgressStatus
    {        
        AppointmentArranged,
        InProgress,       
        Complete
    }    
}
