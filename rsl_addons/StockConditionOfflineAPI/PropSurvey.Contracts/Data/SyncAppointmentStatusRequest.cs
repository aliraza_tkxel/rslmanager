﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SyncAppointmentStatusRequest
    {
        [DataMember]
        public List<AppointmentInProgress> appointments { get; set; }

        [DataMember]
        public int loggedInUserId { get; set; }
    }
}
