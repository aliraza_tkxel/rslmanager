﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class SyncAppointmentStatusResponse
    {
        [DataMember]
        public List<SyncedAppointment> syncedAppointments = new List<SyncedAppointment>();

        [DataMember]
        public List<FailedAppointment> failedAppointments = new List<FailedAppointment>();
    }
}
