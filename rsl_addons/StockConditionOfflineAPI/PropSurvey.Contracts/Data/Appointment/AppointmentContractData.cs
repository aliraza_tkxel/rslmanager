﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class AppointmentContractData
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }

        [DataMember]
        public bool? respondWithData { get; set; }

        [DataMember(IsRequired = false,EmitDefaultValue=false)]
        public bool responseWithIndividualStatus = false;

        [DataMember]
        public List<IAppointment> appointments = new List<IAppointment>();
    }
}
