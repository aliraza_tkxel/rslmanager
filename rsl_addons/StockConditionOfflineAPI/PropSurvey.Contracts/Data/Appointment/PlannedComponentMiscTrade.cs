﻿// -----------------------------------------------------------------------
// <copyright file="PlannedComponentMiscTrade.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PropSurvey.Contracts.Data.Appointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PlannedComponentMiscTrade
    {
        public int? tradeId { get; set; }      
        public double? duration { get; set; }     
        public string durationUnit { get; set; }     
        public string trade { get; set; }

        public string location { get; set; }
        public string adaptation { get; set; }
        public int? locationId { get; set; }
        public int? adaptationId { get; set; }

    }
}
