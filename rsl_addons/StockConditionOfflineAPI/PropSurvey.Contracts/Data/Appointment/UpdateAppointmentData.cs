using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class UpdateAppointmentData1
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string appointmentTitle { get; set; }

        [DataMember]
        public string appointmentType { get; set; }

        [DataMember]
        public string appointmentLocation { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public string surveyourAvailability { get; set; }

        [DataMember]
        public int? defaultCustomerId { get; set; }

        [DataMember]
        public string appointmentNotes { get; set; }

        [DataMember]
        public string surveyType { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public int? updatedBy { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public string surveyorAlert { get; set; }

        [DataMember]
        public string appointmentCalendar { get; set; }

        [DataMember]
        public JournalData journal = new JournalData();

        [DataMember]
        public string noEntryNotes { get; set; }

        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public string repairNotes { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public string appointmentStatus { get; set; }

        [DataMember]
        public LGSRData CP12Info = new LGSRData();

        [DataMember]
        public int? appointmentEventIdentifier { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public SchemeBlockData scheme = new SchemeBlockData();

        [DataMember]
        public List<JobData> jobDataList = new List<JobData>();

        [DataMember]
        public List<CustomerData> customerList = new List<CustomerData>();

        [DataMember]
        public bool? appointmentOverdue { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [DataMember]
        public int? tenancyId { get; set; }

        [DataMember]
        public AppointmentInfoData appInfoData = new AppointmentInfoData();

        [DataMember]
        public int? assignedTo { get; set; }

        [DataMember]
        public int? defaultCustomerIndex { get; set; }

        [DataMember]
        public bool? addToCalendar { get; set; }

        [DataMember]
        public string appointmentShift { get; set; }

        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public DateTime? repairCompletionDateTime { get; set; }

        [DataMember]
        public AppointmentSurveyData Survey = new AppointmentSurveyData();

        [DataMember]
        public PlannedComponentTrade componentTrade = new PlannedComponentTrade();

        [DataMember]
        public VoidAppointmentData voidData = new VoidAppointmentData();

        [DataMember]
        public GasElectricData gasElectrcData = new GasElectricData();

        //public bool validateAppointmentProgressStatus(string status)
        //{
        //    if (Enum.IsDefined(typeof(AppointmentProgressStatus), status) == true)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //public bool validateSurveyourAvailabilityStatus(string status)
        //{
        //    if (Enum.IsDefined(typeof(SurveyourAvailabilityStatus), status) == true)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //public bool validateAppointmentTypes(string status)
        //{
        //    if (Enum.IsDefined(typeof(AppointmentTypes), status) == true)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //public bool validateSurveyTypes(string status)
        //{
        //    if (Enum.IsDefined(typeof(SurveyTypes), status) == true)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
    }

    //public enum AppointmentCompleteStatus
    //{
    //    NotStarted,
    //    InProgress,
    //    NoEntry,
    //    Finished,
    //    Complete,
    //    Completed,
    //    Paused,
    //    Cancel,
    //    Cancelled
    //}

    //public enum SurveyourAvailabilityStatus
    //{
    //    Busy,
    //    Available,
    //    Free,
    //    Tentative,
    //    OutOfOffice
    //}

    //public enum SurveyTypes
    //{
    //    Appointments,
    //    ConditionSurveys,
    //    WorkOrders
    //}


    //public enum AppointmentTypes
    //{
    //    Stock,
    //    Pre,
    //    Post,
    //    Void,
    //    Gas,
    //    Fault,
    //    Planned,
    //    Miscellaneous,
    //    Adaptation,
    //    Condition
    //}

}
