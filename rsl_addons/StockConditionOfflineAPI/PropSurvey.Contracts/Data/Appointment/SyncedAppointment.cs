﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class SyncedAppointment
    {
        [DataMember]
        public int appointmentId { get; set; }
    }
}
