﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class CompleteAppointmentsStatus
    {
        [DataMember]
        public List<SyncAppointmentDetail> savedAppointments = new List<SyncAppointmentDetail>();

        [DataMember]
        public List<SyncAppointmentDetail> failedAppointments = new List<SyncAppointmentDetail>();
    }    
}
