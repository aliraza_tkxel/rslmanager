﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class ReturnAppointmentStatus
    {
     

        [DataMember]
        public bool isUpdated { get; set; }

        //[DataMember]
        //public int appointmentId { get; set; }

        [DataMember]
        public AllAppointmentsList Appointment = new AllAppointmentsList();
    }
}
