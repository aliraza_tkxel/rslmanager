﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class AppointmentHistoryData
    {
        [DataMember]
        public DateTime? actionDate { get; set; }
        [DataMember]
        public int? actionBy { get; set; }
        [DataMember]
        public string actionType = "";
    }
}
