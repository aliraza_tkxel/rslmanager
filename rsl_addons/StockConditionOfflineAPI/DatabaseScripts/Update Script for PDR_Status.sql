
Update PDR_STATUS Set AppTitle = 'To be Arranged' where TITLE='To be Arranged'
Update PDR_STATUS Set AppTitle = 'NotStarted' where TITLE='Arranged'
Update PDR_STATUS Set AppTitle = 'Complete' where TITLE='Completed'
Update PDR_STATUS Set AppTitle = 'Cancelled' where TITLE='Cancelled'
Update PDR_STATUS Set AppTitle = 'InProgress' where TITLE='In Progress'
Update PDR_STATUS Set AppTitle = 'AssignedToContractor' where TITLE='Assigned To Contractor'
Update PDR_STATUS Set AppTitle = 'No Entry' where TITLE='No Entry'
Update PDR_STATUS Set AppTitle = 'Paused' where TITLE='Paused'