/*
Author: Aamir Waheed
Date: 27 Jan 2014
*/

--=========================================================================
--====== Add ImageIdentifier Column in PS_Survey_Item_Images
--=========================================================================
IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'PS_Survey_Item_Images'
		AND COLUMN_NAME = 'ImageIdentifier'
) BEGIN
PRINT 'Not Exists'

--==============================================================
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.PS_Survey_Item_Images ADD
ImageIdentifier NVARCHAR(64) NULL
ALTER TABLE dbo.PS_Survey_Item_Images SET (LOCK_ESCALATION = TABLE)
COMMIT
SELECT
	HAS_PERMS_BY_NAME(N'dbo.PS_Survey_Item_Images', 'Object', 'ALTER')				AS ALT_Per
	,HAS_PERMS_BY_NAME(N'dbo.PS_Survey_Item_Images', 'Object', 'VIEW DEFINITION')	AS View_def_Per
	,HAS_PERMS_BY_NAME(N'dbo.PS_Survey_Item_Images', 'Object', 'CONTROL')			AS Contr_Per

--==============================================================

PRINT 'Column inserted successfully.'
END
ELSE
 PRINT 'Column already exists.'
GO