/*
Author: Aamir Waheed
Date: 25 Feb 2014
*/

--=========================================================================
--====== Add ImageIdentifier Column in P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
--=========================================================================
IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'P_PROPERTY_APPLIANCE_DEFECTS_IMAGES'
		AND COLUMN_NAME = 'ImageIdentifier'
) BEGIN
PRINT 'Not Exists'

--==============================================================
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES ADD
ImageIdentifier NVARCHAR(64) NULL

ALTER TABLE dbo.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES SET (LOCK_ESCALATION = TABLE)

COMMIT
SELECT
	HAS_PERMS_BY_NAME(N'dbo.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES', 'Object', 'ALTER')			AS ALT_Per
	,HAS_PERMS_BY_NAME(N'dbo.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES', 'Object', 'VIEW DEFINITION')	AS View_def_Per
	,HAS_PERMS_BY_NAME(N'dbo.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES', 'Object', 'CONTROL')			AS Contr_Per

--==============================================================

PRINT 'Column inserted successfully.'
END
ELSE
	PRINT 'Column already exists.'
GO