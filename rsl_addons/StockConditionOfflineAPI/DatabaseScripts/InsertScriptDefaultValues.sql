
DECLARE @DefaultName NVARCHAR(100) = 'PushNotificationAfter'
DECLARE @DefaultValue NVARCHAR(2000) = '30'
DECLARE @Description NVARCHAR(2000) = 'The push notification for not completed appointment will be sent after mentioned minutes.'

IF NOT EXISTS (SELECT IDENTITYCOL FROM RSL_DEFAULTS WHERE DEFAULTNAME = @DefaultName)
BEGIN
	INSERT INTO RSL_DEFAULTS(DEFAULTNAME,DEFAULTVALUE,[DESCRIPTION])
	VALUES(@DefaultName,@DefaultValue,@Description)
	PRINT 'Inserted: Default Vaule = "'+ @DefaultValue + '", for Deafult Name => "' + @DefaultName + '"'
END
ELSE
BEGIN
	UPDATE RSL_DEFAULTS
		SET DEFAULTVALUE = @DefaultValue
			,[DESCRIPTION] = @Description
	WHERE DEFAULTNAME = @DefaultName
	
	PRINT 'UPDATED: Default Vaule = "'+ @DefaultValue + '", for Deafult Name => "' + @DefaultName + '"'
END