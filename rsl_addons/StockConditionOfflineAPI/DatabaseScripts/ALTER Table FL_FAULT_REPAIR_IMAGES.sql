/*
   Wednesday, January 28, 20154:46:34 PM
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_FL_FAULT_REPAIR_IMAGES
	(
	FaultRepairImageId int NOT NULL IDENTITY (1, 1),
	PropertyId nvarchar(50) NULL,
	JobSheetNumber nvarchar(50) NOT NULL,
	ImageName nvarchar(100) NULL,
	IsBeforeImage bit NULL,
	CreatedOn smalldatetime NULL,
	CreatedBy int NULL,
	SchemeId int NULL,
	BlockId int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_FL_FAULT_REPAIR_IMAGES SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_FL_FAULT_REPAIR_IMAGES ON
GO
IF EXISTS(SELECT * FROM dbo.FL_FAULT_REPAIR_IMAGES)
	 EXEC('INSERT INTO dbo.Tmp_FL_FAULT_REPAIR_IMAGES (FaultRepairImageId, PropertyId, JobSheetNumber, ImageName, IsBeforeImage, CreatedOn, CreatedBy, SchemeId, BlockId)
		SELECT FaultRepairImageId, PropertyId, JobSheetNumber, ImageName, IsBeforeImage, CreatedOn, CreatedBy, SchemeId, BlockId FROM dbo.FL_FAULT_REPAIR_IMAGES WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_FL_FAULT_REPAIR_IMAGES OFF
GO
DROP TABLE dbo.FL_FAULT_REPAIR_IMAGES
GO
EXECUTE sp_rename N'dbo.Tmp_FL_FAULT_REPAIR_IMAGES', N'FL_FAULT_REPAIR_IMAGES', 'OBJECT' 
GO
ALTER TABLE dbo.FL_FAULT_REPAIR_IMAGES ADD CONSTRAINT
	PK_FL_FAULT_REPAIR_IMAGES PRIMARY KEY CLUSTERED 
	(
	FaultRepairImageId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_REPAIR_IMAGES', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_REPAIR_IMAGES', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_REPAIR_IMAGES', 'Object', 'CONTROL') as Contr_Per 