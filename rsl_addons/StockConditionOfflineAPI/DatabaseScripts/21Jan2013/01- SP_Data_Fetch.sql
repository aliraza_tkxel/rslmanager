USE [RSLBHALive2]
GO

/****** Object:  StoredProcedure [dbo].[MainSP]    Script Date: 01/20/2013 22:02:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[MainSP]
	-- Add the parameters for the stored procedure here
	@LocationId INT,
	@ItemName varchar(50),
	@ParameterName varchar(50),
	@ParameterValue varchar(max),
	@QueryName varchar(50),
	@Area varchar(50),
	@ParameterIdOut INT OUT,
	@IsDelete bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN TRAN

DECLARE @ParameterId INT

CREATE TABLE #TempTable
(
   ValueId int,
   ParameterId int
)

print(@LocationId)
print(@ItemName)
print(@ParameterName)
print(@ParameterValue)
print(@QueryName)

----SELECT and INSERT INTO Temp Table
	DECLARE @SQL     VARCHAR(2000)
	SET @SQL = 'INSERT INTO #TempTable 
		select Distinct valueid,ParameterId from 
		(
			select 
				PA_AREA.AreaId,PA_LOCATION.LocationId,PA_LOCATION.LocationName,PA_ITEM.ItemId,PA_ITEM.ItemName,
				PA_PARAMETER_VALUE.PARAMETERID,PA_PARAMETER_VALUE.VALUEDETAIL, PA_PARAMETER_VALUE.VALUEID 
			from   PA_AREA  INNER JOIN PA_LOCATION on PA_AREA.LocationId = PA_LOCATION.LocationId 
							INNER JOIN PA_ITEM on PA_ITEM.AreaID = PA_AREA.AreaID 
							INNER JOIN PA_ITEM_PARAMETER on PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
							INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterId
							INNER JOIN PA_PARAMETER_VALUE on PA_PARAMETER.ParameterID = PA_PARAMETER_VALUE.ParameterId
							
			where PA_LOCATION.LocationId = ' + convert(varchar(50),@LocationId)  + '
				  and PA_ITEM.ItemName = ''' + @ItemName + '''
				  and PA_AREA.AreaId IN(' + @Area + ')
				  and PA_PARAMETER.ParameterName = ''' + @ParameterName + '''
				  and PA_PARAMETER_VALUE.ValueDetail IN (' + @ParameterValue + ')

		) as temp'	  
	
	print(@SQL)
	
	EXEC (@SQL)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET @QueryName = 'Error in inserting into Temp Table.' + @QueryName
			-- Raise an error and return
			RAISERROR (@QueryName, 16, 1)
		RETURN
	END
	
 select * from #TempTable
 /*************************************************/
	
IF @IsDelete = 1
	BEGIN
		
	
--	-- DELETE FROM PROPERTY ATTRIBUTES
	DELETE FROM PA_PROPERTY_ATTRIBUTES Where ValueId In 
	(	
		select valueid from #TempTable
	)	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET @QueryName = 'Error in deleting PA_PROPERTY_ATTRIBUTES.' + @QueryName
			-- Raise an error and return
			RAISERROR (@QueryName, 16, 1)
		RETURN
	END
	
--	-- DELETE from PS_SURVEY_PARAMETERS
	--DELETE FROM PS_Survey_Parameters Where ItemParamId In 	
	--(
	--	select distinct ParameterId from #TempTable
	--)
	DELETE FROM PS_Survey_Parameters Where ValueId In 	
	(
		select valueId from #TempTable
	)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET @QueryName = 'Error in deleting PS_Survey_Parameters.' + @QueryName
			-- Raise an error and return
			RAISERROR (@QueryName, 16, 1)
		RETURN
	END
	
--	-- DELETE FROM PA_PARAMETER_VALUE
	DELETE FROM PA_PARAMETER_VALUE Where ValueId In
	(
		select valueid from #TempTable
	)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK
			SET @QueryName = 'Error in deleting PA_PARAMETER_VALUE.' + @QueryName
			-- Raise an error and return
			RAISERROR (@QueryName, 16, 1)
		RETURN
	END

END
/*************************************************/

	select @ParameterIdOut = parameterid from #TempTable
	--SET @ParameterIdOut = @ParameterId
	print(@ParameterIdOut)



DROP TABLE #TEMPTABLE

IF @@TRANCOUNT > 0 
	COMMIT TRAN
END

GO


