USE [RSLBHALive2]
GO
/****** Object:  StoredProcedure [dbo].[InputData]    Script Date: 01/21/2013 11:37:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Behroz Sikander>
-- Create date: <19/1/2013>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InputData] 
	-- Add the parameters for the stored procedure here	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ParameterId INT
	DECLARE @Yes INT = 1
	DEClARE @No INT = 0

/************************ First Module **********START********************/

--CELL 5 Query	
	--GET LOCATION ID and save it in URL
	DECLARE @LocationID INT
	select @LocationID = LocationID from PA_LOCATION where LocationName = 'Main Construction'
	
	--GET AREA ID and save it in URL
	DECLARE @AreaId varchar(50)
	select @AreaId = COALESCE(@AreaId + ',','') + CAST(AreaId AS varchar(50)) from PA_AREA where AreaName in ('Dwelling','Garage','Outbuildings','Communal Building','Apartment Block') and LocationId = @LocationID
	
	EXECUTE MainSP @LocationID, 'Roof','Type','''Gable Roof'',''Cross Gabled Roof'', ''Mansard Roof'', ''Hip Roof'', ''Pyramid Hip Roof'', ''Cross Hipped Roof'', ''Saltbox Roof'', ''Flat Roof'',''Shed Roof''',
				'CELL 5',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	--INSERT NEW VALUES
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Pitched Roof',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Flat Roof',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Mono Pitch Roof',3)
	
	print ('Values Inserted -- CELL 5')
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE.', 16, 1)
		RETURN
	END

	
--CELL 6 QUERY
	EXECUTE MainSP @LocationID, 'Structure','Wall Type','''cavity'', ''solid''',
				'CELL 6',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	--INSERT NEW VALUES
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Timber Framed',1)
	
	print ('Values Inserted -- CELL 6')
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 6.', 16, 1)
		RETURN
	END
	
--CELL 7 QUERY
	EXECUTE MainSP @LocationID, 'Structure','Ground Floor Materials','''INSULATED''',
				'CELL 7',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. -- CELL 7', 16, 1)
		RETURN
	END

--CELL 8 QUERY
	EXECUTE MainSP @LocationID, 'Structure','First Floor Materials','''INSULATED''',
				'CELL 8',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 8.', 16, 1)
		RETURN
	END

--CELL 9 QUERY
	EXECUTE MainSP @LocationID, 'Structure','Second Floors','''INSULATED''',
				'CELL 9',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 9.', 16, 1)
		RETURN
	END

--CELL 10 QUERY
	EXECUTE MainSP @LocationID, 'Structure','Other Floors','''INSULATED''',
				'CELL 10',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 10.', 16, 1)
		RETURN
	END

--CELL 11 QUERY
	EXECUTE MainSP @LocationID, 'Structure','Floor Insulation','''< 50'',''> 50''',
				'CELL 11',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'50',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'100',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'150',3)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'200',4)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'250',5)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'300',6)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'350',7)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'400+',8)
	
	print ('Values Inserted -- CELL 11')
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. -- CELL 11', 16, 1)
		RETURN
	END

/****************************FIRST MODULE****************END**************************/


/****************************SECOND MODULE****************START************************/
--CELL 14 QUERY
	--GET LOCATION ID and save it in URL	
	SET @AreaId = null
	select @LocationID = LocationID from PA_LOCATION where LocationName = 'Externals'
	
	--GET AREA ID and save it in URL	
	select @AreaId = COALESCE(@AreaId + ',','') + CAST(AreaId AS varchar(50)) from PA_AREA where AreaName in ('Dwelling','Communal')	and LocationId = @LocationID

	EXECUTE MainSP @LocationID, 'Surface water drainage','Guttering Type','''UPVC'', ''cast iron''',
				'CELL 14',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)
	
	--STAINLESS STEEL, ALLOY and GALVANIZED
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Stainless Steel',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Alloy',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Galvanized',3)
	
	print ('Values Inserted -- CELL 14')
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 14.', 16, 1)
		RETURN
	END

--- CELL 15

EXECUTE MainSP @LocationID, 'Surface water drainage','Downpipe Type','''UPVC'', ''cast iron''',
				'CELL 15',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)
	
	--STAINLESS STEEL, ALLOY and GALVANIZED
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Stainless Steel',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Alloy',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Galvanized',3)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 15', 16, 1)
		RETURN
	END

--- CELL 18
--EXECUTE MainSP @LocationID, 'Fascias','Material type','''UPVC'', ''timber''',
--				'CELL 12',@AreaId , @ParameterId OUT ,@No
	
--	print (@ParameterId)
	
--	--INSERT HERE
	
--	IF @@ERROR <> 0
--		BEGIN
--			-- Rollback the transaction
--			ROLLBACK

--			-- Raise an error and return
--			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE.', 16, 1)
--		RETURN
--	END

--CELL 19
--EXECUTE MainSP @LocationID, 'Fascias','Condition Rating','''1'', ''2''',
--				'CELL 12',@AreaId , @ParameterId OUT 
	
--	print (@ParameterId)
	
--	--INSERT HERE
	
--	IF @@ERROR <> 0
--		BEGIN
--			-- Rollback the transaction
--			ROLLBACK

--			-- Raise an error and return
--			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE.', 16, 1)
--		RETURN
--	END


--CELL 22
EXECUTE MainSP @LocationID, 'Front Windows','Glazing Type','''Double LE'', ''Secondary''',
				'CELL 22',@AreaId , @ParameterId OUT ,@No
	
	print (@ParameterId)
	
	--INSERT HERE	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Single',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Triple',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Double',3)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 22.', 16, 1)
		RETURN
	END


--CELL 25
--SAME --THE VALUES HAVE ALREADY BEEN INSERTED AGAINST THEM
--EXECUTE MainSP @LocationID, 'Rear Windows','Glazing Type','''Double LE'', ''Secondary''',
--				'CELL 24',@AreaId , @ParameterId OUT , @No
	
--	print (@ParameterId)
	
--	--INSERT HERE	
--	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'SINGLE',1)
--	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'TRIPLE',1)
--	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'DOUBLE',1)
	
--	IF @@ERROR <> 0
--		BEGIN
--			-- Rollback the transaction
--			ROLLBACK

--			-- Raise an error and return
--			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE --CELL 25.', 16, 1)
--		RETURN
--	END


--CELL 28
--SAME --THE VALUES HAVE ALREADY BEEN INSERTED AGAINST THEM
--EXECUTE MainSP @LocationID, 'Side Windows','Glazing Type','''Double LE'', ''Secondary''',
--				'CELL 28',@AreaId , @ParameterId OUT , @No
	
--	print (@ParameterId)
	
--	--INSERT HERE	
--	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'SINGLE',1)
--	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'TRIPLE',1)
--	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'DOUBLE',1)
	
--	IF @@ERROR <> 0
--		BEGIN
--			-- Rollback the transaction
--			ROLLBACK

--			-- Raise an error and return
--			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 28.', 16, 1)
--		RETURN
--	END

/****************************SECOND MODULE****************END**************************/

/****************************THIRD MODULE****************START**************************/

SET @AreaId = null
	select @LocationID = LocationID from PA_LOCATION where LocationName = 'Internals'
	
	--GET AREA ID and save it in URL	
	select @AreaId = COALESCE(@AreaId + ',','') + CAST(AreaId AS varchar(50)) from PA_AREA where AreaName in ('Services')	and LocationId = @LocationID

--CELL 31
	EXECUTE MainSP @LocationID, 'Electrics','Fuse Box Location','''Understairs'',''hallway'', ''kitchen'', ''lobby'', ''outside''',
				'CELL 31',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Living Room',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'CloakRoom',2)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 31.', 16, 1)
		RETURN
	END


--CELL 32
	EXECUTE MainSP @LocationID, 'Electrics','Meter Location','''Understairs'',''hallway'', ''kitchen'', ''lobby'', ''outside''',
				'CELL 32',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Living Room',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'CloakRoom',2)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 32.', 16, 1)
		RETURN
	END


--CELL 34
	EXECUTE MainSP @LocationID, 'Water','Stop Cock Location','''Under sink'', ''hallway'', ''porch'', ''outside'', ''other'', ''not known''',
				'CELL 34',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Living Room',1)
	--Already Exists in database. Uncomment if needed.
	--INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Cloakroom',2)
	--INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Bathroom',3)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE --CELl 34.', 16, 1)
		RETURN
	END

--CELL 37
--SAME -- ALREADY ADDED
	EXECUTE MainSP @LocationID, 'Gas','Meter Location','''Understairs'',''hallway'', ''kitchen'', ''lobby'', ''outside''',
				'CELL 37',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Bathroom',1)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 37.', 16, 1)
		RETURN
	END


--CELL 41
	EXECUTE MainSP @LocationID, 'Heating','Heating Fuel','''Main gas'', ''LPG'', ''Oil'', ''Wind''',
				'CELL 41',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	
	
	--ADD E7 and ELECTRIC TO DROP DOWN

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'E7',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Electric',2)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 41.', 16, 1)
		RETURN
	END
	
	
--CELL 42
	EXECUTE MainSP @LocationID, 'Heating','Secondary heating fuel','''Main gas'', ''LPG'', ''Oil'', ''Wind''',
				'CELL 42',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	
	
	--ADD E7 and ELECTRIC TO DROP DOWN

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'E7',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Electric',2)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 42.', 16, 1)
		RETURN
	END

--CELL 43
	EXECUTE MainSP @LocationID, 'Heating','Heating System','''Radiators'', ''gas fires'', ''HVAC'', ''underfloor'', ''nightstorage''',
				'CELL 43',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Warm Air',1)	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 43.', 16, 1)
		RETURN
	END


--CELL 44
	EXECUTE MainSP @LocationID, 'Heating','Manufacturer','''Ideal''',
				'CELL 44',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	
	--ADD DIMPLEX and CREDA TO DROP DOWN
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Dimplex',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Creda',2)	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 44.', 16, 1)
		RETURN
	END

/****************************THIRD MODULE****************END**************************/

/****************************FOURTH MODULE****************END**************************/

SET @AreaId = null

	select @LocationID = LocationID from PA_LOCATION where LocationName = 'Internals'
	
	--GET AREA ID and save it in URL	
	select @AreaId = COALESCE(@AreaId + ',','') + CAST(AreaId AS varchar(50)) from PA_AREA where AreaName in ('Accommodation')	and LocationId = @LocationID
	
--CELL 48
	EXECUTE MainSP @LocationID, 'Bathroom','Balcony','''0'',''1'',''2'',''3'',''4''',
				'CELL 48',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)	
	
	DELETE FROM PA_PROPERTY_ATTRIBUTES where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PS_SURVEY_PARAMETERS where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PA_ITEM_PARAMETER Where ParameterId = @ParameterId
	--DELETE FROM PA_PARAMETER Where ParameterId = @ParameterId
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 48.', 16, 1)
		RETURN
	END


	--CELL 51
	EXECUTE MainSP @LocationID, 'Kitchen','Manufacturer','''MFI''',
				'CELL 51',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)	
	--ADD HOWDENS, RIXONWAY, SYMPHONY, BENCHMARK, BOULTON AND PAUL AND REMOVE MFI FROM DROP DOWN

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Howdens',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Rixonway',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Symphony',3)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Benchmark',4)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Boulton',5)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Paul',6)	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 51.', 16, 1)
		RETURN
	END
	
	
	--CELL 58
	EXECUTE MainSP @LocationID, 'Hallway','Stairs','''Timber'',''Concrete''',
				'CELL 58',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	
	--ADD HOWDENS, RIXONWAY, SYMPHONY, BENCHMARK, BOULTON AND PAUL AND REMOVE MFI FROM DROP DOWN

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Steel',1)	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 58.', 16, 1)
		RETURN
	END
	
	
	--CELL 61
	EXECUTE MainSP @LocationID, 'Bedrooms','Quantity','''0'',''1'',''2'',''3'',''4''',
				'CELL 61',@AreaId , @ParameterId OUT , @No
	
	print (@ParameterId)	

	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'5',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'6',2)	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 61.', 16, 1)
		RETURN
	END
	
	--CELL 62
	
	EXECUTE MainSP @LocationID, 'Bedrooms','Balcony','''0'',''1'',''2'',''3'',''4''',
				'CELL 62',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)	
	--Remove completely
	DELETE FROM PA_PROPERTY_ATTRIBUTES where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PS_SURVEY_PARAMETERS where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PA_ITEM_PARAMETER Where ParameterId = @ParameterId
	--DELETE FROM PA_PARAMETER Where ParameterId = @ParameterId
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 62.', 16, 1)
		RETURN
	END
	
	
	--CELL 65
	EXECUTE MainSP @LocationID, 'Cloakroom','Manufacturer','''MFI''',
				'CELL 65',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)	
	DELETE FROM PA_PROPERTY_ATTRIBUTES where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PS_SURVEY_PARAMETERS where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PA_ITEM_PARAMETER Where ParameterId = @ParameterId
	--DELETE FROM PA_PARAMETER Where ParameterId = @ParameterId
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 65.', 16, 1)
		RETURN
	END
	
	--CELL 68
	EXECUTE MainSP @LocationID, 'Separate W/C','Manufacturer','''MFI''',
				'CELL 68',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)	
	DELETE FROM PA_PROPERTY_ATTRIBUTES where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PS_SURVEY_PARAMETERS where itemparamid = (select itemParamid from PA_ITEM_PARAMETER where parameterid = @ParameterId)
	DELETE FROM PA_ITEM_PARAMETER Where ParameterId = @ParameterId
	--DELETE FROM PA_PARAMETER Where ParameterId = @ParameterId
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE -- CELL 68.', 16, 1)
		RETURN
	END
	
	
	--CELL 72 QUERY
	EXECUTE MainSP @LocationID, 'Loft','Roof Insulation','''< 50'',''> 50''',
				'CELL 72',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'50',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'100',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'150',3)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'200',4)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'250',5)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'300',6)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'350',7)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'400+',8)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. --CELL 72', 16, 1)
		RETURN
	END
	
	
	--CELL 73 QUERY
	EXECUTE MainSP @LocationID, 'Loft','Loft Insulation','''< 50'',''> 50''',
				'CELL 73',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'50',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'100',2)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'150',3)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'200',4)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'250',5)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'300',6)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'350',7)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'400+',8)
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. --CELL 73', 16, 1)
		RETURN
	END

	

------- CELL 45
Update PA_PARAMETER SET DataType = 'String', ControlType = 'Textbox' ,
	 ParameterName='Number of Radiators/Storage Heaters' where ParameterName = 'Number of Radiators'
DELETE from PS_Survey_Parameters where ValueId in (select ValueId FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = 'Number of Radiators'))
DELETE from PA_PROPERTY_ATTRIBUTES where ValueId in (select ValueId FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = 'Number of Radiators'))
DELETE FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = 'Number of Radiators')
------- CELL 52
Update PA_PARAMETER SET DataType = 'String', ControlType = 'Textbox' where ParameterName = 'No. of Base Units'
DELETE from PS_Survey_Parameters where ValueId in (select ValueId FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = 'No. of Base Units'))
DELETE from PA_PROPERTY_ATTRIBUTES where ValueId in (select ValueId FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = 'No. of Base Units'))
DELETE FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = 'No. of Base Units')

------CELL 53
Update PA_PARAMETER SET DataType = 'String', ControlType = 'Textbox' where ParameterName like '%of Wall Units%'
DELETE from PS_Survey_Parameters where ValueId in (select ValueId FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = '%of Wall Units%'))
DELETE from PA_PROPERTY_ATTRIBUTES where ValueId in (select ValueId FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername = '%of Wall Units%'))
DELETE FROM PA_PARAMETER_VALUE where parameterId in (select parameterId from PA_PARAMETER where parametername like '%of Wall Units%')

-----CELL 54
EXECUTE MainSP @LocationID, 'Kitchen','Extractor Fan','''0'',''1'',''2'',''3'',''4''',
				'CELL 54',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Yes',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'No',2)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. --CELL 54', 16, 1)
		RETURN
	END


------CELL 55
EXECUTE MainSP @LocationID, 'Kitchen','Balcony','''0'',''1'',''2'',''3'',''4''',
				'CELL 55',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Yes',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'No',2)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. --CELL 55', 16, 1)
		RETURN
	END


------CELL 69
EXECUTE MainSP @LocationID, 'Separate W/C','Extractor Fan','''Automatic'', ''Chord Pull''',
				'CELL 69',@AreaId , @ParameterId OUT , @Yes
	
	print (@ParameterId)
	
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'Yes',1)
	INSERT INTO PA_PARAMETER_VALUE VALUES (@ParameterId,'No',2)
	
	
	IF @@ERROR <> 0
		BEGIN
			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			RAISERROR ('Error in INSERTING PA_PARAMETER_VALUE. --CELL 69', 16, 1)
		RETURN
	END



--COMMIT

END
