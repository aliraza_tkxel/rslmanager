--PAGE 4 Change
--INSERT CURR DUE FIELD IN DB 

/***************************************************************/
DECLARE @ItemId INT
DECLARE @PARAMETERID INT
DECLARE @LocationID INT
DECLARE @AreaId INT

select 
			distinct	@ItemId = PA_ITEM.ItemId
			from   PA_AREA  INNER JOIN PA_LOCATION on PA_AREA.LocationId = PA_LOCATION.LocationId 
							INNER JOIN PA_ITEM on PA_ITEM.AreaID = PA_AREA.AreaID 
							INNER JOIN PA_ITEM_PARAMETER on PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
							
			where PA_LOCATION.LocationId = (select LocationId from PA_Location where LocationName= 'Internals')
				  and PA_ITEM.ItemName = 'Electrics'
				  and PA_AREA.AreaId IN (select AreaId from PA_AREA where AreaName = 'Services')
				  
print (@ItemId)

INSERT INTO PA_PARAMETER VALUES ('Cur Due','Date','Date',1,1)
SELECT @PARAMETERID = SCOPE_IDENTITY()
INSERT INTO PA_ITEM_PARAMETER VALUES (@ItemId,@PARAMETERID)

/***************************************************************/
--PAGE 5 Change
--ADD UPGRADE DUE
INSERT INTO PA_PARAMETER VALUES ('Upgrade Due','Date','Date',1,1)
SELECT @PARAMETERID = SCOPE_IDENTITY()
INSERT INTO PA_ITEM_PARAMETER VALUES (@ItemId,@PARAMETERID)


/***************************************************************/
--PAGE 5 Change
--Change LAST REWIRED
Update PA_PARAMETER SET ParameterName = 'Last Upgraded' 
	where ParameterId = (select ParameterID from PA_PARAMETER where parametername = 'Last Rewired')

/***************************************************************/
