﻿Please follow these steps before you install/execute program.

/* App setting template
 <appSettings>
        <!-- set the server url for i.e "devcrm.broadlandhousinggroup.org", please do not add any slash("/") -->
        <add key="hostUrl" value="devcrm.broadlandhousinggroup.org" />
        <!-- application path to the web servie (in case it is not install on root of web server), other wise set the vaule to "" (empty string) -->
        <add key="webServiceAPI" value="StockConditionOfflineAPI" />
        <!-- this is the route to the api on serve, no need to change it unless api name is changed on server -->
        <add key="APIfunction" value="push/sendPushNotificationDocumentExpiry"/>
        <!-- for https connection set vaule as true, otherwise false  -->
        <add key="useSecureConnction" value="true"/>        
 </appSettings>
*/

1- Open "DocumentExpiryPushNotificationTrigger.exe.config" with a text editor (i.e notepad)
2- Configure the values for server to use i.e for test crm: testcrm.broadlandhousinggroup.org
3- Install the using setup.msi or excute from file location, or execute it from same location.
4- Create a schedule in Schedule tasks to execute it on schedule time/time intervals