﻿Imports System
Imports System.Web.HttpContext
Imports System.Web
Imports P_BusinessObject
Imports P_Utilities

Namespace P_Utilities

    Public Class SessionManager

#Region "Set / Get Property Id"

#Region "Set Property Id"

        Public Shared Sub setPropertyId(ByRef propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id"

        Public Shared Function getPropertyId()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id"

        Public Shared Sub removePropertyId()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Job Sheet Number"

#Region "Set Job Sheet Number"

        Public Shared Sub setJobSheetNumber(ByRef JobSheetNumber As String)
            Current.Session(SessionConstants.JobSheetNumber) = JobSheetNumber
        End Sub

#End Region

#Region "get Job Sheet Number"

        Public Shared Function getJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.JobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Job Sheet Number"

        Public Shared Sub removeJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.JobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Login In User "

#Region "Set / Get  Property Module User Id"

#Region "Set   Property Module User Id"
        Public Shared Sub setPropertyModuleUserId(ByRef userId As Integer)
            Current.Session(SessionConstants.PropertyModuleUserId) = userId
        End Sub
#End Region

#Region "get  Property Module User Id"
        Public Shared Function getPropertyModuleUserId() As Integer

            If (IsNothing(Current.Session(SessionConstants.PropertyModuleUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.PropertyModuleUserId), Integer)
            End If
        End Function
#End Region

#Region "remove Property Module User Id"

        Public Sub removePropertyModuleUserId()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyModuleUserId))) Then
                Current.Session.Remove(SessionConstants.PropertyModuleUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"

#Region "Set User Full Name"

        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"

        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"

        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"

#Region "Set User Employee Id"

        Public Shared Sub setUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"

        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"

        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#End Region

    End Class

End Namespace

