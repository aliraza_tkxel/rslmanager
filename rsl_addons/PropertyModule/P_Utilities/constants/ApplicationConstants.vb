﻿Imports System
Imports System.Text

Namespace P_Utilities
    Public Class ApplicationConstants

#Region "General"
        Public Shared SearchMenu As String = "Search"
#End Region

#Region "Stored procedure column names"
        Public Shared MenuCol As String = "Menu"
        Public Shared UrlCol As String = "Url"
        Public Shared MenuIdCol As String = "MenuId"
        Public Shared PageFileNameCol As String = "PageFileName"
        Public Shared GrantModulesModuleIdCol As String = "MODULEID"
        Public Shared GrantMenuModuleIdCol As String = "ModuleId"
        Public Shared GrantMenuMenuIdCol As String = "MenuId"
        Public Shared GrantMenuUrlCol As String = "Url"
        Public Shared GrantMenuDescriptionCol As String = "Menu"
        Public Shared GrantPageParentPageCol As String = "ParentPage"
        Public Shared GrantPageAccessLevelCol As String = "AccessLevel"
        Public Shared GrantPageQueryStringCol As String = "PageQueryString"
        Public Shared GrantPageFileNameCol As String = "PageFileName"
        Public Shared GrantPageUrlCol As String = "PageUrl"
        Public Shared GrantPageCoreUrlCol As String = "PageCoreUrl"
        Public Shared GrantPagePageNameCol As String = "PageName"
        Public Shared GrantPageIdCol As String = "PageId"
        Public Shared GrantPageMenuIdCol As String = "MenuId"
        Public Shared GrantPageModuleIdCol As String = "ModuleId"
        Public Shared GrantOrderTextCol As String = "ORDERTEXT"
        Public Shared RandomParentPageCol As String = "ParentPage"
        Public Shared RandomAccessLevelCol As String = "AccessLevel"
        Public Shared RandomQueryStringCol As String = "PageQueryString"
        Public Shared RandomFileNameCol As String = "PageFileName"
        Public Shared RandomPageNameCol As String = "PageName"
        Public Shared RandomPageIdCol As String = "PageId"
#End Region

#Region "CSS"
        Public Shared TabClickedCssClass As String = "TabClicked"
        Public Shared TabInitialCssClass As String = "TabInitial"
#End Region

#Region "Datatable Names"
        Public Shared AccessGrantedModulesDt As String = "AccessGrantedModulesDt"
        Public Shared AccessGrantedMenusDt As String = "AccessGrantedMenusDt"
        Public Shared AccessGrantedPagesDt As String = "AccessGrantedPagesDt"
        Public Shared RandomPageDt As String = "RandomPageDt"
#End Region
    End Class
End Namespace



