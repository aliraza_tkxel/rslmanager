﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text

Namespace P_Utilities
    Public Class RegularExpConstants

        Public Shared AmountExp As String = "^\d+(\.\d+)?$"
        Public Shared numbersExp As String = "^[0-9.]+$"
        Public Shared emailExp As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$"
        Public Shared stringLengthExp As String = "^[\s\S]{0,30}$"

    End Class
End Namespace
