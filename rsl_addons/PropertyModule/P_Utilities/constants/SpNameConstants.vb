﻿Namespace P_Utilities

    Public Class SpNameConstants

#Region "Property"
        Public Shared GetSearchPropertyAndScheme As String = "P_GetSearchPropertyAndScheme"
        Public Shared GetRSLModulesList As String = "P_GetRSLModulesList"
        Public Shared GetPropertyMenuList As String = "P_GetPropertyMenuList"
        Public Shared GetTenancyPropertyDetail As String = "P_GetTenancyPropertyDetail"
        Public Shared GetAccommodationSummary As String = "P_GetAccommodationSummary"
        Public Shared GetTenancyHistory As String = "P_GetTenancyHistory"
        Public Shared GetAsbestosDetail As String = "P_GetAsbestosInformation"
        Public Shared GetPropertyImages As String = "P_GetPropertyImages"
        Public Shared GetFaultsAndDefects As String = "P_GetFaultsAndDefects"
        Public Shared GetPropertyAddress As String = "P_GetPropertyAddress"
        Public Shared GetPropertyMenuPages As String = "P_GetPropertyMenuPages"
#End Region

#Region "Login"
        Public Shared GetEmployeeById As String = "P_GetEmployeeById"
#End Region

#Region "JobSheet"

        Public Shared GetJobSheetDetail As String = "FL_GetJobSheetDetail"

#End Region

    End Class
End Namespace