﻿Imports System

Namespace P_Utilities

    Public Class SessionConstants

#Region "User Session"
        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared PropertyModuleUserId As String = "AppServicingUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"

#End Region

#Region "Property"
        Public Shared PropertyId As String = "PropertyId"
        Public Shared JobSheetNumber As String = "JobSheetNumber"
#End Region

    End Class

End Namespace


