﻿
Namespace P_Utilities


    Public Class UserMessageConstants

#Region "Property"
        Public Shared InvalidPropertyId As String = "Property Id is invalid."
        Public Shared InvalidQueryStringPropertyId As String = "Invalid Query String PropertyId."
        Public Shared NoRecordFound As String = "No record found."
        Public Shared InvalidJobSheetNumber As String = "Invalid Job Sheet Number"

#End Region

#Region "Login"
        Public Shared UserDoesNotExist As String = "You don't have access to Property Module. Please contact administrator."
        Public Shared UsersAccountDeactivated As String = "Your access to Property Module has been de-activiated. Please contact administrator."
#End Region

    End Class
End Namespace

