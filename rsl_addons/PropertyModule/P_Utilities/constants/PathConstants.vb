﻿Namespace P_Utilities

    Public Class PathConstants

#Region "URL Constants"
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const SeparatorImagePath As String = "../Images/PropertyModule/sep.jpg"
        Public Const SearchPath As String = "~/Views/Search/SearchProperty.aspx"
        Public Const AppliancesImagePath As String = "../../../RSLApplianceServicing/Photographs/Images/"
        Public Const AccessDeniedPath As String = "~/Views/General/AccessDenied.aspx"
#End Region

#Region "Query String Constants"
        Public Const Pid As String = "pid"
#End Region

#Region "Src Keys"

#End Region


    End Class
End Namespace