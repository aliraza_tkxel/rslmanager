﻿Imports P_BusinessObject
Imports P_Utilities
Imports P_DataAccess

Namespace P_DataAccess

    Public Class PropertyDAL : Inherits BaseDAL

#Region "Get Tenancy/Property detail"
        ''' <summary>
        ''' Get Tenancy/Property detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyPropertyDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtTenancyPropertyDetail As DataTable = New DataTable()

            dtTenancyPropertyDetail.TableName = "TenancyPropertyDetail"

            resultDataSet.Tables.Add(dtTenancyPropertyDetail)

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTenancyPropertyDetail)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtTenancyPropertyDetail)

        End Sub

#End Region

#Region "Get Accommodation Summary"
        ''' <summary>
        ''' Get Accommodation Summary
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAccommodationSummary(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtAccommodation As DataTable = New DataTable()
            dtAccommodation.TableName = "Accommodation"
            resultDataSet.Tables.Add(dtAccommodation)

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAccommodationSummary)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAccommodation)

        End Sub

#End Region

#Region "Get Tenancy History"
        ''' <summary>
        ''' Get Tenancy History
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtTenancyHistory As DataTable = New DataTable()
            dtTenancyHistory.TableName = "TenancyHistory"
            resultDataSet.Tables.Add(dtTenancyHistory)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTenancyHistory)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtTenancyHistory)

        End Sub

#End Region

#Region "Get Asbestos Detail"
        ''' <summary>
        ''' Get Asbestos Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getAsbestosDetail(ByRef propertyId As String) As Integer

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("asbestosCount", "", DbType.Int32)
            outParametersList.Add(resultParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.GetAsbestosDetail)
            Return CType(outParametersList.Item(0).Value, Integer)

        End Function

#End Region

#Region "Get Reported Faults/Defects"
        ''' <summary>
        ''' Get Reported Faults/Defects
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getReportedFaultsDefects(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtReportedFaults As DataTable = New DataTable()
            dtReportedFaults.TableName = "ReportedFaults"
            resultDataSet.Tables.Add(dtReportedFaults)

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetFaultsAndDefects)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtReportedFaults)

        End Sub

#End Region

#Region "Get Property Address"
        ''' <summary>
        ''' Get Property Address
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyAddress(ByRef propertyId As String) As String

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim propertyIdParam As ParameterBO = New ParameterBO("propertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim resultParam As ParameterBO = New ParameterBO("propertyAddress", "", DbType.String)
            outParametersList.Add(resultParam)

            outParametersList = MyBase.SelectRecord(parametersList, outParametersList, SpNameConstants.GetPropertyAddress)
            Return CType(outParametersList.Item(0).Value, String)

        End Function

#End Region

#Region "Get Job Sheet Detail"

        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Try
                Dim parametersList As ParameterList = New ParameterList()

                Dim dtFaultAndAppointment As DataTable = New DataTable()
                Dim dtAsbestos As DataTable = New DataTable()
                Dim dtCustomer As DataTable = New DataTable()

                dtFaultAndAppointment.TableName = "FaultAppointment"
                dtAsbestos.TableName = "Asbestos"
                dtCustomer.TableName = "Customer"


                resultDataSet.Tables.Add(dtFaultAndAppointment)
                resultDataSet.Tables.Add(dtAsbestos)
                resultDataSet.Tables.Add(dtCustomer)


                Dim jsn As ParameterBO = New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
                parametersList.Add(jsn)


                Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJobSheetDetail)
                resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtFaultAndAppointment, dtAsbestos, dtCustomer)

            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Get Property Images"
        ''' <summary>
        ''' Get Property Images
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPropertyImages(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPropertyImages As DataTable = New DataTable()
            dtPropertyImages.TableName = "PropertyImages"
            resultDataSet.Tables.Add(dtPropertyImages)

            Dim propertyIdParam As ParameterBO = New ParameterBO("PropertyId", propertyId, DbType.String)
            parametersList.Add(propertyIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPropertyImages)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyImages)

        End Sub

#End Region

    End Class

End Namespace