﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports System.Web
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports P_BusinessObject
Imports P_Utilities


Namespace P_DataAccess

    Public Class UsersDAL : Inherits BaseDAL

#Region "get Employee By Id"
        Function getEmployeeById(ByVal employeeId As Int32) As DataSet
            Dim parametersList As ParameterList = New ParameterList()
            Dim userBo As UserBO = New UserBO()
            Dim ds As DataSet = New DataSet()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(ds, parametersList, SpNameConstants.GetEmployeeById)
            Return ds
        End Function
#End Region



    End Class


End Namespace
