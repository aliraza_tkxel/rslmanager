﻿Imports P_BusinessObject
Imports P_Utilities
Imports P_DataAccess

Namespace P_DataAccess

    Public Class SearchPropertyDAL : Inherits BaseDAL

        Public Sub getPropertyAndSchemeSearchResult(ByRef resultDataSet As DataSet, ByRef searchText As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPropertyResult As DataTable = New DataTable()
            Dim dtSchemeResult As DataTable = New DataTable()

            dtPropertyResult.TableName = "PropertySearchResult"
            dtSchemeResult.TableName = "SchemeSearchResult"

            resultDataSet.Tables.Add(dtPropertyResult)
            resultDataSet.Tables.Add(dtSchemeResult)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSearchPropertyAndScheme)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult, dtSchemeResult)

        End Sub

    End Class

End Namespace