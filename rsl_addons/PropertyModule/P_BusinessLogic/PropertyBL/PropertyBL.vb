﻿
Imports P_DataAccess
Imports P_BusinessObject

Namespace P_BusinessLogic
    Public Class PropertyBL

#Region "Get Tenancy/Property detail"
        ''' <summary>
        ''' Get Tenancy/Property detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyPropertyDetail(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getTenancyPropertyDetail(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Accommodation Summary"
        ''' <summary>
        ''' Get Accommodation Summary
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getAccommodationSummary(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getAccommodationSummary(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Property Address"
        ''' <summary>
        ''' Get Property Address
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getPropertyAddress(ByRef propertyId As String) As String

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            Return objPropertyDal.getPropertyAddress(propertyId)

        End Function

#End Region

#Region "Get Tenancy History"
        ''' <summary>
        ''' Get Tenancy History
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getTenancyHistory(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getTenancyHistory(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Asbestos Detail"
        ''' <summary>
        ''' Get Asbestos Detail
        ''' </summary>    
        ''' <remarks></remarks>

        Public Function getAsbestosDetail(ByRef propertyId As String) As Integer

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            Return objPropertyDal.getAsbestosDetail(propertyId)

        End Function

#End Region

#Region "Get Reported Faults/Defects"
        ''' <summary>
        ''' Get Reported Faults/Defects
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getReportedFaultsDefects(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getReportedFaultsDefects(resultDataSet, propertyId)

        End Sub

#End Region

#Region "Get Job Sheet Details"
        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim objPropertyDAL As PropertyDAL = New PropertyDAL()
            objPropertyDAL.getJobSheetDetails(resultDataSet, jobSheetNumber)


        End Sub
#End Region

#Region "Get Property Images"
        ''' <summary>
        ''' Get Property Images
        ''' </summary>    
        ''' <remarks></remarks>

        Public Sub getPropertyImages(ByRef resultDataSet As DataSet, ByRef propertyId As String)

            Dim objPropertyDal As PropertyDAL = New PropertyDAL()
            objPropertyDal.getPropertyImages(resultDataSet, propertyId)

        End Sub

#End Region


    End Class
End Namespace
