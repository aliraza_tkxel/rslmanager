﻿Imports P_DataAccess
Imports P_BusinessObject

Namespace P_BusinessLogic
    Public Class SearchPropertyBL

        Public Sub getPropertyAndSchemeSearchResult(ByRef resultDataSet As DataSet, ByVal search As String)

            Dim objSerPropertyDal As SearchPropertyDAL = New SearchPropertyDAL()
            objSerPropertyDal.getPropertyAndSchemeSearchResult(resultDataSet, search)

        End Sub


    End Class
End Namespace
