USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/*  
EXEC P_GetFaultsAndDefects  
@propertyId = 'A010060001'  
 Author:  <Author,,Noor Muhammad>  
Create date: <Create Date,,16 Sep,2013>  
 Description: <Description,,This stored procedure returns the faults and defects of property in property summary page>  
*/
-- =============================================
ALTER PROCEDURE [dbo].[P_GetFaultsAndDefects]   
	@propertyId as varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
CONVERT(VARCHAR(10),FL_FAULT_LOG.SUBMITDATE, 103) as ReportedDate
,FAULTLOGID as Id
,JobSheetNumber JobSheetNumber
,FL_AREA.AreaName +' > '+ FL_FAULT.DESCRIPTION as Description
,FL_FAULT_STATUS.Description as Status
,'Reactive' as Type
FROM
FL_FAULT_LOG
LEFT JOIN P__PROPERTY ON P__PROPERTY.PROPERTYID=FL_FAULT_LOG.PROPERTYID
INNER JOIN FL_FAULT ON FL_FAULT_LOG.FAULTID=FL_FAULT.FAULTID 
INNER JOIN FL_AREA ON FL_FAULT_LOG.AREAID = FL_AREA.AreaID 
INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FAULTSTATUSID=FL_FAULT_LOG.STATUSID
WHERE
P__PROPERTY.PROPERTYID=@propertyId
ORDER BY FL_FAULT_LOG.SUBMITDATE DESC
END
