USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC P_GetEmployeeById 	@employeeId = 615
-- Author:		<Ahmed Mehmood>
-- Create date: <24/06/2013>
-- Description:	<This Stored Proceedure get the Employee by id >
-- Web Page: Bridge.aspx
-- =============================================
ALTER PROCEDURE [dbo].[P_GetEmployeeById](
	@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT E__EMPLOYEE.EMPLOYEEID as EmployeeId, E__EMPLOYEE.FirstName +' ' + E__EMPLOYEE.LastName as FullName, AC_LOGINS.Active as IsActive
	,(Select Description 
	FROM E__EMPLOYEE 
	INNER JOIN AS_USER ON E__EMPLOYEE.EMPLOYEEID = AS_USER.EmployeeId
		INNER JOIN AS_USERType ON AS_USER.UserTypeId = AS_USERType.UserTypeId
		Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
	) as UserType
	FROM E__EMPLOYEE 	
	INNER JOIN AC_LOGINS on E__EMPLOYEE.EmployeeId = AC_LOGINS.EmployeeId
	Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
END
