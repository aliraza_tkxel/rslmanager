USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetSearchPropertyAndScheme]    Script Date: 06/19/2013 16:01:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[P_GetSearchPropertyAndScheme] 

/* ===========================================================================
 ' NAME: P_GetSearchPropertyAndScheme
-- EXEC	 [dbo].[P_GetSearchPropertyAndScheme]
--		
-- Author:		<Ahmed Mehmood>
-- Create date: <14/06/2013>
-- Description:	< Returns the search results for property and scheme >
-- Web Page: SearchProperty.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@searchText varchar (MAX) = ''
	)
	
		
AS
	
	DECLARE @SelectClause varchar(8000),
	        @FromClause   varchar(8000),
	        @WhereClause  varchar(8000),		                
	        @SearchCriteria varchar(8000),
	        @mainSelectQuery varchar(8000)
	        

    --========================================================================================
	--							PROPERTY SEARCH
    --========================================================================================
         
    SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'SearchProperty.PROPERTYID LIKE ''%'+ LTRIM(@searchText) +'%'' OR'
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'SearchProperty.Address LIKE ''%'+ LTRIM(@searchText) +'%'' AND' 
                                                                                                                                                                                                                      
    -- Building Main Query for Property Search
                             
    SET @SelectClause = 'SELECT TOP(100)' +                      
    CHAR(10) + 'SearchProperty.PropertyId ,SearchProperty.TypeDescription ,SearchProperty.Address'                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + '( SELECT	P__PROPERTY.PROPERTYID PropertyId,P_PROPERTYTYPE.DESCRIPTION TypeDescription,ISNULL(P__PROPERTY.FLATNUMBER,'''')+'' ''+ISNULL(P__PROPERTY.HouseNumber,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS1,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS2,'''') +'' ''+ ISNULL(P__PROPERTY.ADDRESS3,'''') as Address
									FROM	P__PROPERTY INNER JOIN
											P_PROPERTYTYPE ON p__property.PROPERTYTYPE = P_PROPERTYTYPE.PROPERTYTYPEID ) SearchProperty'  	                      
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	    
			
	
    --========================================================================================
	--							SCHEME SEARCH
    --========================================================================================							
	
	SET @SearchCriteria = ''                              
    SET @SearchCriteria = @SearchCriteria + CHAR(10) + CHAR(9) + 
                            'P_SCHEME.SCHEMENAME LIKE ''%'+ LTRIM(@searchText) +'%'' AND'                                                          
    
                             
    -- Building Main Query for Property Search
                             
    SET @SelectClause = 'SELECT DISTINCT TOP(100)' +                      
    CHAR(10) + 'P_SCHEME.SCHEMEID ,P_SCHEME.SCHEMENAME, P_SCHEME.SCHEMENAME as Description '                     
                           
    SET @FromClause = CHAR(10) + 'FROM ' + 
                      CHAR(10) + 'P_SCHEME'
                     
                              
      
    SET @WhereClause =  CHAR(10)+  CHAR(10) + 'WHERE ( ' +                      
                        CHAR(10) + CHAR(10) + @SearchCriteria +                                        
                        CHAR(10) + CHAR(9) + ' 1=1 )'   
                        
    Set @mainSelectQuery = @selectClause +@fromClause + @whereClause             
                              
    print(@mainSelectQuery)
	EXEC (@mainSelectQuery)	 












