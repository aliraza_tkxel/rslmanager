USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetAccommodationSummary]    Script Date: 09/13/2013 17:20:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[P_GetAccommodationSummary] 

/* ===========================================================================
 ' NAME: P_GetAccommodationSummary
-- EXEC	 [dbo].[P_GetAccommodationSummary]
-- @PropertyId = 'A720040007'	
-- Author:		<Ahmed Mehmood>
-- Create date: <11/09/2013>
-- Description:	< Returns the property summary info >
-- Web Page: PropertyRecordSummary.aspx
 '==============================================================================*/
	
	(
	    -- These Parameters are passed as Search Criteria
		@PropertyId varchar (MAX) = ''
	)
	
AS
BEGIN
SET NOCOUNT ON;

SELECT
	ISNULL(PFA.PARAMETERVALUE, '?')	AS FLOORAREA
	,ISNULL(PA.PARAMETERVALUE, '?')	AS MAXPEOPLE
	,ISNULL(A.PARAMETERVALUE, '?')	AS BEDROOMS
	,ISNULL(ANM.Description, 'N/A')	AS NROSHASSETTYPEMAIN
FROM
	P__PROPERTY PP
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = PP.PROPERTYID
				AND
				A.ITEMPARAMID =
				(
					SELECT
						ItemParamID
					FROM
						PA_ITEM_PARAMETER
							INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
							INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
					WHERE
						ParameterName = 'Quantity'
						AND ItemName = 'Bedrooms'
				)
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES PA ON PA.PROPERTYID = PP.PROPERTYID
				AND
				PA.ITEMPARAMID =
				(
					SELECT
						ItemParamID
					FROM
						PA_ITEM_PARAMETER
							INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
							INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
					WHERE
						ParameterName = 'Max People'
						AND ItemName = 'Structure'
				)

	
		LEFT OUTER JOIN PA_PROPERTY_ATTRIBUTES PFA ON PFA.PROPERTYID = PP.PROPERTYID
				AND
				PFA.ITEMPARAMID =
				(
					SELECT
						ItemParamID
					FROM
						PA_ITEM_PARAMETER
							INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
							INNER JOIN PA_ITEM ON PA_ITEM_PARAMETER.ItemId = PA_ITEM.ItemID
					WHERE
						ParameterName = 'Floor Area(sq m)'
						AND ItemName = 'Structure'
				)
		LEFT OUTER JOIN P_ASSETTYPE_NROSH_MAIN AS ANM ON PP.NROSHASSETTYPEMAIN = ANM.Sid
WHERE
	PP.PROPERTYID = @PropertyId

END