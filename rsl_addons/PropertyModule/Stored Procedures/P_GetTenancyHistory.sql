USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetTenancyHistory]    Script Date: 09/18/2013 15:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC P_GetTenancyHistory
--@propertyId = 'BHA0000748'
-- Author:		<Author,,Noor Muhammad>
-- Create date: <Create Date,,16 Sep,2013>
-- Description:	<Description,, This stroed procedure returns the tenancy history against the property id>
-- =============================================
ALTER PROCEDURE [dbo].[P_GetTenancyHistory] 
	@propertyId as varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from	
	SET NOCOUNT ON;

    SELECT 
    CONVERT(VARCHAR(10),ct.startdate, 103) as StartDate
    , ct.startdate
    ,CONVERT(VARCHAR(10),CT.enddate, 103) as EndDate
    ,ct.customerid as CustomerId
    ,ct.tenancyid as TenancyId
    ,tl.description + ' ' + c.firstname + ' ' + c.lastname as FullCustomerName		 
	 ,'₤ '+(SELECT Convert(varchar,sum(rent)) 
			FROM F_RENTJOURNAL_MONTHLY 
			WHERE F_RENTJOURNAL_MONTHLY.tenancyid = t.tenancyid 
			AND PROPERTYID = @propertyId) as TotalRent
	 FROM c_customertenancy ct
		 inner join c_tenancy t ON  t.tenancyid = ct.tenancyid 
		 inner join c__customer c ON c.customerid = ct.customerid 
		 left join g_title tl ON tl.titleid = c.title 				 
	 WHERE
		ct.tenancyid in (SELECT tenancyid FROM c_tenancy WHERE propertyid = @propertyId)				 
	 ORDER BY ct.startdate DESC
END
