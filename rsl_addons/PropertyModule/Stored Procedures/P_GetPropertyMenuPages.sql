USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GetPropertyMenuPages]    Script Date: 07/01/2014 15:01:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,16/06/2014>
-- Description:	<Description,,Get the pages for Menu selected on master page.>
-- Web Page: PropertyModuleMaster.Master
-- =============================================
ALTER PROCEDURE [dbo].[P_GetPropertyMenuPages](
@employeeId int,
@menuText varchar(100)
)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--====================================================
-- Access Granted Modules
--====================================================
 EXEC P_GetRSLModulesList @USERID = @employeeId, @OrderASC = 1

--====================================================
-- Access Granted Menus
--====================================================
 EXEC P_GetPropertyMenuList @employeeId = @employeeId
 
--====================================================
-- Access Granted Pages
--====================================================
SELECT	DISTINCT
		AC_PAGES.PAGEID AS PageId
		,AC_PAGES.DESCRIPTION AS PageName
		,ISNULL(CASE
		WHEN  AC_PAGES.BridgeActualPage = '' or AC_PAGES.BridgeActualPage is NULL THEN
			AC_PAGES.PAGE
		ELSE 
			AC_PAGES.BridgeActualPage
		END,'')
		AS	PageUrl
		,AC_PAGES.PAGE as PageCoreUrl
		,ISNULL(CASE
		WHEN AC_PAGES.BridgeActualPage = '' or AC_PAGES.BridgeActualPage is NULL THEN
			dbo.FN_GetFileNameFromUrl(AC_PAGES.PAGE)
		ELSE 
			dbo.FN_GetFileNameFromUrl(AC_PAGES.BridgeActualPage)
		END,'')
		AS PageFileName
		,ISNULL(CASE
		WHEN AC_PAGES.BridgeActualPage = '' or AC_PAGES.BridgeActualPage is NULL THEN
			dbo.FN_GetQueryStringFromUrl(AC_PAGES.PAGE)
		ELSE 
			dbo.FN_GetQueryStringFromUrl(AC_PAGES.BridgeActualPage)
		END,'')
		AS PageQueryString
		,AccessLevel 
		,ParentPage
		,AC_PAGES.MENUID as MenuId
		,AC_MENUS.MODULEID as ModuleId
		,AC_PAGES.ORDERTEXT
		,AC_PAGES.DESCRIPTION
		,AC_MENUS.DESCRIPTION AS MenuName
FROM	AC_PAGES_ACCESS		
		INNER JOIN E__EMPLOYEE ON AC_PAGES_ACCESS.JobRoleTeamId = E__EMPLOYEE.JobRoleTeamId 
		INNER JOIN AC_PAGES ON AC_PAGES_ACCESS.PageId = AC_PAGES.PAGEID 
		INNER JOIN AC_MENUS_ACCESS ON  AC_MENUS_ACCESS.MenuId  = AC_PAGES.MENUID
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID 
		INNER JOIN AC_MODULES ON AC_MENUS.MODULEID = AC_MODULES.MODULEID
WHERE	E__EMPLOYEE.EMPLOYEEID = @employeeId
		AND AC_PAGES.ACTIVE = 1
		AND AC_MENUS.DESCRIPTION = @menuText
		AND AC_MODULES.DESCRIPTION = 'Property'
		AND LINK = 1

ORDER BY AC_PAGES.ORDERTEXT, AC_PAGES.DESCRIPTION


--====================================================
-- Random Pages (Pages that are not in Menus, but they do exist in projects)
-- These pages have link column equal to 0.
-- if they have parent page 
--====================================================

SELECT	AC_PAGES.PAGEID AS PageId
		,AC_PAGES.DESCRIPTION AS PageName
		,ISNULL(CASE
		WHEN  AC_PAGES.BridgeActualPage = '' or AC_PAGES.BridgeActualPage is NULL THEN
			AC_PAGES.PAGE
		ELSE 
			AC_PAGES.BridgeActualPage
		END,'')
		AS	PageUrl
		,ISNULL(CASE
		WHEN AC_PAGES.BridgeActualPage = '' or AC_PAGES.BridgeActualPage is NULL THEN
			dbo.FN_GetFileNameFromUrl(AC_PAGES.PAGE)
		ELSE 
			dbo.FN_GetFileNameFromUrl(AC_PAGES.BridgeActualPage)
		END,'')
		AS PageFileName
		,ISNULL(CASE
		WHEN AC_PAGES.BridgeActualPage = '' or AC_PAGES.BridgeActualPage is NULL THEN
			dbo.FN_GetQueryStringFromUrl(AC_PAGES.PAGE)
		ELSE 
			dbo.FN_GetQueryStringFromUrl(AC_PAGES.BridgeActualPage)
		END,'')
		AS PageQueryString
		,AccessLevel 
		,ParentPage 
		,AC_PAGES.MENUID AS MenuId
		,AC_MENUS.MODULEID AS ModuleId
		,AC_PAGES.ORDERTEXT
		,AC_PAGES.DESCRIPTION
FROM	
		AC_MODULES
		INNER JOIN AC_MENUS ON AC_MODULES.MODULEID = AC_MENUS.MODULEID 
		INNER JOIN AC_PAGES ON AC_MENUS.MENUID = AC_PAGES.MENUID 
		
WHERE	AC_PAGES.ACTIVE = 1
		AND AC_MENUS.DESCRIPTION = @menuText
		AND AC_MODULES.DESCRIPTION = 'Property'
		AND LINK = 0

ORDER BY parentpage,AC_PAGES.ORDERTEXT, AC_PAGES.DESCRIPTION asc

END
