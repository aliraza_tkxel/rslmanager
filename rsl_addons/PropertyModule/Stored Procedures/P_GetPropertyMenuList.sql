
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC P_GetPropertyMenuList @employeeId = 143
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,13/06/2014>
-- Description:	<Description,,TO the menus for master page according to the employee rights>
-- WebPage: PropertyModuleMaster.Master
-- =============================================
ALTER PROCEDURE [dbo].[P_GetPropertyMenuList](
@employeeId int)
AS
BEGIN

DECLARE @PropertyModuleId int

SELECT	@PropertyModuleId = MODULEID
FROM	AC_MODULES
WHERE	DESCRIPTION = 'Property'

SELECT	DISTINCT
		AC_MENUS.DESCRIPTION AS Menu,
		ISNULL(AC_MENUS.PAGE,'') AS	Url,
		AC_MENUS.MENUID AS MenuId,
		AC_MENUS.MODULEID AS ModuleId,
		AC_MENUS.ORDERTEXT,
		AC_MENUS.DESCRIPTION
FROM	E__EMPLOYEE 
		INNER JOIN AC_MENUS_ACCESS ON E__EMPLOYEE.JobRoleTeamId = AC_MENUS_ACCESS.JobRoleTeamId 
		INNER JOIN AC_MENUS ON AC_MENUS_ACCESS.MenuId = AC_MENUS.MENUID 
		
WHERE	AC_MENUS.ACTIVE = 1 
		AND EMPLOYEEID = @employeeId
		AND AC_MENUS.MODULEID = @PropertyModuleId
		AND EXISTS(	SELECT	*
					FROM	E__EMPLOYEE 
							INNER JOIN AC_MODULES_ACCESS ON E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId
					WHERE	MODULEID = @PropertyModuleId
							AND EMPLOYEEID = @employeeId )
ORDER BY AC_MENUS.ORDERTEXT, AC_MENUS.DESCRIPTION
END
