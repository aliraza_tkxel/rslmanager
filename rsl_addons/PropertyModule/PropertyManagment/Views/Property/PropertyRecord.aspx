﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/PropertyModuleMaster.Master"
    CodeBehind="PropertyRecord.aspx.vb" Inherits="PropertyManagmentModule.PropertyRecord" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="SummaryControl" Src="~/Controls/Property/SummaryControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link href="../../Styles/PropertyModule.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/PropertySummary.css" rel="stylesheet" type="text/css" />
     <link href="../../Styles/faultPopupStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Scripts/jquery-v1.9.1.js"></script>

    <link rel="stylesheet" href="../../Styles/lightbox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="../../Scripts/prototype.js"></script>
    <script type="text/javascript" src="../../Scripts/scriptaculous.js?load=effects"></script>
    <script type="text/javascript" src="../../Scripts/lightbox.js"></script>


    <style type="text/css">
        .pnlPropertyAddress
        {
            float: left;
        }
    </style>
    <script type="text/javascript">
        function PrintJobSheet() {
            
            javascript: window.open('PrintJobSheetDetail.aspx?JobSheetNumber=' + document.getElementById("lblFaultId").innerHTML);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelPropertyRecord" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
            <table style="border: thin solid #000000; width: 100%;">
                <tr>
                    <td style="border-bottom-style: solid; border-width: thin; border-color: #000000">
                        <div>
                            <asp:Panel ID="pnlPropertyAddress" runat="server" CssClass="pnlPropertyAddress">
                                &nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblProperty" runat="server" Font-Bold="True" Text="Property &gt;"></asp:Label>
                                <asp:Label ID="lblPropertyAddres" runat="server" Font-Bold="True"></asp:Label>
                            </asp:Panel>
                            <div style="float: right;">
                                <asp:Button ID="btnBack" runat="server" Text="&nbsp;&lt; Back &nbsp;" /></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 8px; padding-right: 1px; padding-top: 20px;">
                        <asp:LinkButton ID="lnkBtnSummaryTab" OnClick="lnkBtnSummaryTab_Click" CssClass="TabInitial"
                            runat="server" BorderStyle="Solid">Summary: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnActivitiesTab" OnClick="lnkBtnActivitiesTab_Click" CssClass="TabInitial"
                            runat="server" BorderStyle="Solid">Activities: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnAttributesTab" OnClick="lnkBtnAttributesTab_Click" CssClass="TabInitial"
                            runat="server" BorderStyle="Solid">Attributes: </asp:LinkButton>
                        <asp:LinkButton ID="lnkBtnDocTab" OnClick="lnkBtnDocTab_Click" CssClass="TabInitial"
                            runat="server" BorderStyle="Solid">Documents: </asp:LinkButton>
                        <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                            width: 100%;">
                        </div>
                        <div style="clear: both; margin-bottom: 5px;">
                        </div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <uc1:SummaryControl ID="SummaryControl" runat="server" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <%--<asp:UpdateProgress ID="updateProgressPropertyRecord" runat="server" AssociatedUpdatePanelID="updPanelPropertyRecord"
                DisplayAfter="10">
                <ProgressTemplate>
                    <div class="loading-image">
                        <img alt="Please Wait" src="../../Images/progress.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>
            <%--z<asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
