﻿Imports P_Utilities
Imports P_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PropertyRecord
    Inherits PageBase

    Public ddlLetter As DropDownList = New DropDownList()
    Public ckBoxRefreshDataSet As New CheckBox()
    Public ckBoxDocumentUpload As New CheckBox()
    Public ckBoxPhotoUpload As New CheckBox()
    Public ckBoxDefectPhotoUpload As New CheckBox()

#Region "Events"


#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            If Not IsPostBack Then
                getQueryStringValues()
                lnkBtnSummaryTab.CssClass = ApplicationConstants.TabClickedCssClass
                MainView.ActiveViewIndex = 0
                Me.populateFullSummary()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "lnk Btn Summary Tab Click"
    Protected Sub lnkBtnSummaryTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        lnkBtnSummaryTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnActivitiesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDocTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 0
        Me.populateFullSummary()

    End Sub
#End Region

#Region "lnk Btn Activities Tab Click"

    Protected Sub lnkBtnActivitiesTab_Click(ByVal sender As Object, ByVal e As EventArgs)

        lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnActivitiesTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDocTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 1

    End Sub
#End Region

#Region "lnk Btn Attributes Tab Click"

    Protected Sub lnkBtnAttributesTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnActivitiesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAttributesTab.CssClass = ApplicationConstants.TabClickedCssClass
        lnkBtnDocTab.CssClass = ApplicationConstants.TabInitialCssClass
        MainView.ActiveViewIndex = 2

    End Sub
#End Region

#Region "lnk Btn Doc Tab Click"
    Protected Sub lnkBtnDocTab_Click(ByVal sender As Object, ByVal e As EventArgs)
        lnkBtnSummaryTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnActivitiesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass
        lnkBtnDocTab.CssClass = ApplicationConstants.TabClickedCssClass
        MainView.ActiveViewIndex = 3
    End Sub
#End Region


#Region "Back button event"
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        redirectToSearch()
    End Sub
#End Region

#End Region

#Region "Function"

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        Dim objPropertyBL As PropertyBL = New PropertyBL
        If Request.QueryString(PathConstants.Pid) IsNot Nothing Then
            Dim propertyId As String = CType(Request.QueryString(PathConstants.Pid), String)
            lblPropertyAddres.Text = objPropertyBL.getPropertyAddress(propertyId)
            SessionManager.setPropertyId(propertyId)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
        End If
    End Sub
#End Region

#Region "Populate full summary"

    Private Sub populateFullSummary()

        Dim propertyId As String = SessionManager.getPropertyId()
        SummaryControl.populateTenancyPropertyDetail(propertyId)
        SummaryControl.populateAccommodationSummary(propertyId)
        SummaryControl.populateAsbestosDetail(propertyId)
        SummaryControl.populateReportedFaultsDefects()
        SummaryControl.populateTenancyHistory(propertyId)
        SummaryControl.populatePropertyImages()

    End Sub

#End Region

#Region "Redirect to search screen"

    Private Sub redirectToSearch()

        Response.Redirect(PathConstants.SearchPath)

    End Sub

#End Region

#End Region

End Class