﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/PropertyModuleMaster.Master"
    CodeBehind="AccessDenied.aspx.vb" Inherits="PropertyManagmentModule.AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <asp:Image ID="imgAccessDenied" Style="position: absolute; margin: auto; top: 0;
            left: 0; right: 0; bottom: 0;" ImageUrl="~/Images/access_denied.png" runat="server" />
    </div>
</asp:Content>
