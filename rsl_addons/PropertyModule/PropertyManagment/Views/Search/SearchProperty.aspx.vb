﻿Imports P_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports P_BusinessObject
Imports P_BusinessLogic

Public Class SearchProperty
    Inherits PageBase


#Region "Properties"
    Dim objSearchBL As SearchPropertyBL = New SearchPropertyBL()
#End Region


#Region "Events"

#Region "Page load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                pnlControls.Enabled = False
            End If
        End Try

    End Sub

#End Region

#Region "Text Field Search Text Changed Event"

    Protected Sub txtSearchHidden_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSearchHidden.TextChanged

        Try
            If (String.IsNullOrEmpty(txtSearchHidden.Text) Or String.IsNullOrWhiteSpace(txtSearchHidden.Text)) Then
                tblProperty.Visible = False
                tblScheme.Visible = False
            Else
                searchResults()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub

#End Region

#End Region


#Region "Functions"

#Region "Search Results"

    Sub searchResults()

        Dim resultDataset As DataSet = New DataSet
        Dim searchText As String = txtSearchHidden.Text.Trim
        searchText = searchText.Replace("'", "''")
        objSearchBL.getPropertyAndSchemeSearchResult(resultDataset, searchText)
        
        ' Populating Property search results
        If Not resultDataset.Tables(0).Rows.Count = 0 Then
            tblProperty.Visible = True
            rptProperty.DataSource = resultDataset.Tables(0)
            rptProperty.DataBind()
        Else
            tblProperty.Visible = False
        End If


        ' Populating Scheme search results
        If Not resultDataset.Tables(1).Rows.Count = 0 Then
            tblScheme.Visible = True
            rptScheme.DataSource = resultDataset.Tables(1)
            rptScheme.DataBind()
        Else
            tblScheme.Visible = False
        End If

    End Sub

#End Region


#Region "Highlight Text"
    Protected Function HighlightText(ByVal inputText As String) As String

        Dim expression As Regex = New Regex(txtSearchHidden.Text.Replace(" ", "|"), RegexOptions.IgnoreCase)
        Return expression.Replace(inputText, New MatchEvaluator(AddressOf ReplaceKeywords))

    End Function
#End Region

#Region "Replace Keywords"
    Public Function ReplaceKeywords(ByVal m As Match) As String

        Return "<span class='highlight'>" + m.Value + "</span>"

    End Function
#End Region

#End Region




End Class