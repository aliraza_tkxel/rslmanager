﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="../../MasterPage/PropertyModuleMaster.Master"
    CodeBehind="SearchProperty.aspx.vb" Inherits="PropertyManagmentModule.SearchProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <link href="../../Styles/layout.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .form-background
        {
            /* position: absolute;
    background-image: url('../../Images/imageStBensHDPic.jpg');
    z-index: -1;
    background-repeat: no-repeat;
    height: 100%;
    width: 100%;
    background-size:cover;
    background-position: bottom right;*/
            background: url('../../Images/imageStBensHDPic.jpg') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            width: 100%;
            height: 100%;
            position: absolute;
            z-index: -1;
        }
    </style>
    <link href="../../Styles/PropertyModule.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.caret.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">

        function ShowPropertySummary(id) {
            javascript: window.location.assign('../../../RSLApplianceServicing/Bridge.aspx?pid=' + id);
            return false;
        }

        function ShowSchemeSummary(id) {
            javascript: window.location.assign('../../../PropertyDataRestructure/Bridge.aspx?pg=schemerecord&id=' + id + '&requestType=scheme&src=search');
            return false;
        }

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            if ($("#<%= txtSearchHidden.ClientID %>").val()) {
                typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            }
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearchHidden.ClientID %>', '');
        }

        function pageLoad(sender, args) {
            $("#<%= txtSearchHidden.ClientID %>").focus().val($("#<%= txtSearchHidden.ClientID %>").val());
        }


        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded)
        });

        function PageLoaded(sender, args) {
            $("#<%= txtSearchHidden.ClientID %>").caretToEnd();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="search-main-content">
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">

                    </asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlControls" runat="server">
                    <asp:UpdatePanel ID="updPanelSearchBox" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="search-content-wrapper">
                                <br />
                                <br />
                                <asp:TextBox ID="txtSearchHidden" runat="server" Height="30px" Width="75%" AutoPostBack="false"
                                    Style="background-image: url(../../Images/imgMagnifir.png); background-position: right;
                                    background-repeat: no-repeat; margin-top: 58px; margin-left: 10%;" ToolTip=""
                                    AutoCompleteType="Search" class="searchbox" onkeyup="TypingInterval();"></asp:TextBox>
                                <br />
                                <br />
                                <div style="width: 75%; height: 400px; overflow: auto; margin-left: 10%;">
                                    <asp:Table ID="tblProperty" CssClass="search-table" runat="server" Width="100%" Visible="false">
                                        <asp:TableRow>
                                            <asp:TableCell Width="10%" VerticalAlign="Top" BorderWidth="1"><img src="../../Images/icon_property.png" alt="Completed"  style="border:none;width:50px;height:50px;" /></asp:TableCell>
                                            <asp:TableCell Width="90%" BorderWidth="1">
                                                <table class="repeaterTable">
                                                    <asp:Repeater ID="rptProperty" runat="server">
                                                        <ItemTemplate>
                                                            <tr onclick="ShowPropertySummary('<%# DataBinder.Eval(Container.DataItem, "PropertyId") %>')">
                                                                <td>
                                                                    <%# HighlightText(Container.DataItem("Address"))%>
                                                                </td>
                                                                <td>
                                                                    ( Ref:
                                                                    <%# HighlightText(Container.DataItem("PropertyId"))%>
                                                                    )
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Container.DataItem("TypeDescription")%>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <asp:Table ID="tblScheme" BorderWidth="1" CssClass="search-table" runat="server"
                                        Width="100%" Visible="false">
                                        <asp:TableRow>
                                            <asp:TableCell Width="10%" BorderWidth="1" VerticalAlign="Top"><img src="../../Images/icon_scheme.png" alt="Completed"  style="border:none;width:50px;height:50px;" /></asp:TableCell>
                                            <asp:TableCell Width="90%" BorderWidth="1">
                                                <table class="repeaterTable">
                                                    <asp:Repeater ID="rptScheme" runat="server">
                                                        <ItemTemplate>
                                                            <tr onclick="ShowSchemeSummary('<%# DataBinder.Eval(Container.DataItem, "SCHEMEID") %>')">
                                                                <td>
                                                                    <%# HighlightText(Container.DataItem("SCHEMENAME"))%>
                                                                </td>
                                                                <td>
                                                                    &nbsp &nbsp
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Container.DataItem("Description")%>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%-- 
                <div class="roundcorner" id="fault_search_result">
                    <div id="fault_result_header">
                        <b>
                            <asp:Label ID="lblSearchStatus" runat="server" Text="Recent faults:"></asp:Label>
                        </b>
                    </div>
                   <asp:GridView ID="grdRecentFaults" runat="server" AutoGenerateColumns="False" GridLines="None"
                        Style="margin-left: 10px;" ShowHeaderWhenEmpty="True" DataKeyNames="FaultId">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:LinkButton Font-Underline="false" ID="lnkBtnDescription" runat="server" Text='<%# HighlightText(Eval("Description"))%>'
                                        ForeColor="Black" CommandArgument='<%# EVAL("FaultId") %>'></asp:LinkButton>
                                    &nbsp;<asp:Label ID="lblDescription" runat="server" Text='<%# EVAL("Description") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" BorderColor="Black" />
                        <RowStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:GridView>--%>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
