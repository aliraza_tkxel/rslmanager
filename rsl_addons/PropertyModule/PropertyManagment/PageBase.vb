﻿Imports P_Utilities

Imports System.Web
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports P_BusinessLogic

Public Class PageBase
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

    Public pathsData As New Dictionary(Of String, String)

    Sub New()

    End Sub


#Region "is Session Exist"

    Public Sub isSessionExist()

        Dim userId As Integer = SessionManager.getUserEmployeeId()
        If (userId = 0) Then
            Response.Redirect(PathConstants.LoginPath)
        End If

    End Sub
#End Region

#Region "Session Time out"
    ''' <summary>
    ''' This event handles the page_init event of the base page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
        Me.checkPageAccess()
    End Sub
#End Region
       
#Region "Check Page Access"
    Sub checkPageAccess()
        Dim objRSLModulesBL As New RSLModulesBL
        If (Not objRSLModulesBL.checkPageAccess()) Then
            Response.Redirect(PathConstants.AccessDeniedPath)
        End If
    End Sub
#End Region

#Region "OnInit"
    ''' <summary>
    ''' This function is used to destroy browser cache. so that every time page load 'll be called
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnInit(ByVal e As EventArgs)

        MyBase.OnInit(e)
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        'Response.Cache.SetNoStore()
        'Response.Cache.SetExpires(DateTime.Now.AddDays(-1))
        'Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
        'Response.Expires = -1500
        'Response.CacheControl = "no-cache"


    End Sub
#End Region

    '#Region "set Back Button Post Back Url"
    '    Public Sub setBackButtonPostBackUrl(ByRef btnBack As System.Web.UI.WebControls.Button, Optional ByVal postBackUrl As String = "", Optional ByVal currentPageSrcVal As String = "")
    '        If (Not (String.IsNullOrEmpty(postBackUrl))) Then
    '            btnBack.PostBackUrl = postBackUrl
    '        Else
    '            If Not String.IsNullOrEmpty(Request.QueryString(PathConstants.Src)) Then
    '                Dim srcVal As String = Request.QueryString(PathConstants.Src)

    '                If pathsData.ContainsKey(srcVal) Then
    '                    btnBack.PostBackUrl = pathsData.Item(srcVal) + currentPageSrcVal
    '                Else
    '                    Throw New Exception(UserMessageConstants.SrcDoesNotExist)
    '                End If

    '            Else
    '                Throw New Exception(UserMessageConstants.InvalidSrc)
    '            End If
    '        End If
    '    End Sub
    '#End Region



End Class
