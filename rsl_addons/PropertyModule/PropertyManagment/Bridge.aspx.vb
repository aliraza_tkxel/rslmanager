﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports P_BusinessLogic
Imports P_Utilities
Imports P_BusinessObject

Public Class Bridge
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    Dim customerId As Integer = 0
    Dim propertyId As String = String.Empty
    Dim classicUserId As Integer = 0


#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "load User"
    Private Sub loadUser()

        Dim objUserBo As UserBO = New UserBO()
        Dim objUserBl As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()

        ' Me.getQueryStringValues()
        Me.checkClassicAspSession()

        resultDataSet = objUserBl.getEmployeeById(classicUserId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
        Else
            Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

            If isActive = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
            Else
                setUserSession(resultDataSet)
                redirectToAccessGrantedPage()

                'If isCalendarView = True Then
                '    Response.Redirect(PathConstants.SchedulersCalanderSearch, True)
                'ElseIf isReportView = True Then
                '    Response.Redirect(PathConstants.ReportAreaPath, True)
                'Else
                '    Response.Redirect(PathConstants.SearchFault + "?" + PathConstants.Cid + "=" + Me.customerId.ToString() + "&" + PathConstants.Pid + "=" + Me.propertyId)
                'End If

            End If

        End If
    End Sub
#End Region

#Region "set User Session"
    Private Sub setUserSession(ByRef resultDataSet)

        SessionManager.setPropertyModuleUserId(classicUserId)
        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())

    End Sub
#End Region

#Region "Redirect to access granted page"
    ''' <summary>
    ''' Redirect to access granted page
    ''' </summary>
    ''' <remarks></remarks>
    Sub redirectToAccessGrantedPage()
        'SessionManager.setUserEmployeeId(760)
        Dim objRSLModulesBL As RSLModulesBL = New RSLModulesBL
        Dim pageResultDatatable As DataTable
        Dim menuResultDatatable As DataTable
        Dim userId As Integer = SessionManager.getUserEmployeeId()

        Dim searchMenu As String = ApplicationConstants.SearchMenu
        Dim resultDataset As DataSet = New DataSet
        objRSLModulesBL.getPropertyPageList(resultDataset, userId, searchMenu)

        Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
        Dim accessGrantedMenusDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt)
        pageResultDatatable = accessGrantedPagesDt.Clone()
        menuResultDatatable = accessGrantedMenusDt.Clone()

        Dim navigateUrl As String = String.Empty
        Dim menuName As String = String.Empty

        Dim pageQuery = (From dataRow In accessGrantedPagesDt _
                  Where dataRow.Field(Of String)(ApplicationConstants.GrantPageCoreUrlCol).Trim() <> String.Empty _
                  Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol), _
                            dataRow.Field(Of Integer)(ApplicationConstants.GrantPageIdCol) Ascending _
                  Select dataRow)

        If pageQuery.Count > 0 Then
            pageResultDatatable = pageQuery.CopyToDataTable()
            navigateUrl = pageResultDatatable.Rows(0).Item(ApplicationConstants.GrantPageCoreUrlCol)
        Else
            Dim menuQuery = (From dataRow In accessGrantedMenusDt _
               Where dataRow.Field(Of String)(ApplicationConstants.GrantMenuUrlCol).Trim() <> String.Empty _
               Order By dataRow.Field(Of String)(ApplicationConstants.GrantOrderTextCol) Ascending _
               Select dataRow)

            If (menuQuery.Count > 0) Then
                menuResultDatatable = menuQuery.CopyToDataTable()
                navigateUrl = menuResultDatatable.Rows(0).Item(ApplicationConstants.GrantMenuUrlCol)
                menuName = menuResultDatatable.Rows(0).Item(ApplicationConstants.GrantMenuDescriptionCol)
                If (menuName.Equals(ApplicationConstants.SearchMenu)) Then
                    navigateUrl = PathConstants.SearchPath
                End If

            Else
                navigateUrl = PathConstants.AccessDeniedPath
            End If

            End If

        Response.Redirect(navigateUrl, True)

    End Sub

#End Region

    '#Region "get Query String Values"

    '    Private Sub getQueryStringValues()
    '        If Request.QueryString(PathConstants.Cid) IsNot Nothing And Request.QueryString(PathConstants.Pid) IsNot Nothing Then

    '            Me.customerId = CType(Request.QueryString(PathConstants.Cid), Integer)
    '            Me.propertyId = CType(Request.QueryString(PathConstants.Pid), String)

    '        ElseIf Request.QueryString(PathConstants.CalView) IsNot Nothing Then
    '            'the user want to see screen 16, 17
    '            isCalendarView = IIf(Request.QueryString(PathConstants.CalView) = PathConstants.Yes, True, False)

    '        ElseIf Request.QueryString(PathConstants.ReportView) IsNot Nothing Then
    '            'the user want to go to report area
    '            isReportView = IIf(Request.QueryString(PathConstants.ReportView) = PathConstants.Yes, True, False)

    '        Else
    '            Me.redirectToLoginPage()
    '        End If
    '    End Sub
    '#End Region

#Region "check Classic Asp Session"
    Private Sub checkClassicAspSession()
        '''''''''comment this line below for production
        ' classicUserId = 201
        '''''''''comment this line ábove for production

        ''''''''''Un comment these below lines for production
        If ASPSession("USERID") IsNot Nothing Then
            classicUserId = Integer.Parse(ASPSession("USERID").ToString())
        Else
            Me.redirectToLoginPage()
        End If
        ''''''''''Un comment these above lines for production
    End Sub
#End Region

#Region "redirect To Login Page"
    Private Sub redirectToLoginPage()
        Response.Redirect(PathConstants.LoginPath, True)
    End Sub
#End Region
End Class
