﻿Imports P_BusinessLogic
Imports P_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PropertyModuleMaster
    Inherits System.Web.UI.MasterPage


#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Page Load
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'SessionManager.setUserEmployeeId(615)

        If Not Page.IsPostBack Then
            populateMenus()
        End If

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate Menus"
    ''' <summary>
    ''' Populate Menus
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateMenus()

        populateModules()
        populateHeaderMenus()

    End Sub

#End Region

#Region "Get RSL Modules"
    ''' <summary>
    ''' Get RSL Modules
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateModules()
        Dim objRSLModulesBL As New RSLModulesBL
        Dim resultDataset As DataSet = New DataSet
        Dim employeeId As Integer = SessionManager.getUserEmployeeId

        objRSLModulesBL.getRSLModules(resultDataset, employeeId) ' Need to change UserId
        rptRSLMenu.DataSource = resultDataset.Tables(0)
        rptRSLMenu.DataBind()
    End Sub

#End Region

#Region "Populate header menu"
    ''' <summary>
    ''' Populate header menu
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateHeaderMenus()

        Dim objRSLModulesBL As New RSLModulesBL
        Dim resultDataset As DataSet = New DataSet
        Dim employeeId As Integer = SessionManager.getUserEmployeeId

        objRSLModulesBL.getPropertyMenuList(resultDataset, employeeId) ' Need to change UserId  
        For Each parentItem As DataRow In resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt).Rows

            Dim item As MenuItem = New MenuItem()
            item.Text = parentItem(ApplicationConstants.MenuCol)
            item.Value = parentItem(ApplicationConstants.MenuIdCol).ToString()
            item.SeparatorImageUrl = PathConstants.SeparatorImagePath
            item.NavigateUrl = parentItem(ApplicationConstants.UrlCol)
            If (parentItem(ApplicationConstants.MenuCol).Equals(ApplicationConstants.SearchMenu)) Then
                item.Selected = True
            End If

            If (String.IsNullOrEmpty(item.NavigateUrl) Or String.IsNullOrWhiteSpace(item.NavigateUrl)) Then
                item.Enabled = False
            End If

            menuHeader.Items.Add(item)
        Next
        menuHeader.DataBind()

    End Sub

#End Region

#End Region



End Class