﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SummaryControl.ascx.vb"
    Inherits="PropertyManagmentModule.SummaryControl" %>
<div>
    <asp:Panel ID="pnlControlMessage" runat="server" Visible="false">
        <asp:Label ID="lblControlMessage" runat="server">
        </asp:Label>
    </asp:Panel>
    <div id="leftContainer">
        <div class="propertyInfo" style="height: 150px">
            <asp:Panel ID="pnlTenancyProperty" runat="server" Visible="false">
                <asp:Label ID="lblTenancyProperty" runat="server">
                </asp:Label>
            </asp:Panel>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Ref Number:"></asp:Label>
                <asp:Label ID="lblRefNumber" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Address:"></asp:Label>
                <asp:Label ID="lblAddress" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Postcode:"></asp:Label>
                <asp:Label ID="lblPostCode" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Dev:"></asp:Label>
                <asp:Label ID="lblDev" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Scheme"></asp:Label>
                <asp:Label ID="lblScheme" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Status:"></asp:Label>
                <asp:Label ID="lblStatus" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label runat="server" CssClass="leftControl" Text="Type:"></asp:Label>
                <asp:Label ID="lblType" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
        </div>
        <div class="propertyInfo">
            <asp:Panel ID="pnlAccomodation" runat="server" Visible="false">
                <asp:Label ID="lblAccommodation" runat="server">
                </asp:Label>
            </asp:Panel>
            <div class="labelSpace">
                <asp:Label ID="Label1" runat="server" CssClass="leftControl" Text="Floor Area:"></asp:Label>
                <asp:Label ID="lblFloorArea" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label3" runat="server" CssClass="leftControl" Text="Maximum People:"></asp:Label>
                <asp:Label ID="lblMaxPeople" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
            <div class="labelSpace">
                <asp:Label ID="Label7" runat="server" CssClass="leftControl" Text="Bedrooms:"></asp:Label>
                <asp:Label ID="lblBedrooms" runat="server" CssClass="rightControl" Text=""></asp:Label>
                <br />
            </div>
        </div>
        <div class="propertyInfo" style="height: 100px;">
            <b>Appointments:</b>
            <br />
            <asp:GridView ID="grdAppointments" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                Width="90%" GridLines="None" DataKeyNames="FaultLogID" CellSpacing="5">
                <Columns>
                    <asp:BoundField ItemStyle-Width="100px" DataField="AptType" />
                    <asp:BoundField HeaderText="Status" DataField="AptTime" ItemStyle-Width="150px" />
                    <asp:BoundField HeaderText="Status" DataField="AptDate" ItemStyle-Width="150px" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="propertyInfo" style="height: 100px;">
            <b>Planned Maintenance Programme:</b><br />
            <asp:GridView ID="grdPlannedMaintenance" runat="server" AutoGenerateColumns="False"
                ShowHeader="false" Width="90%" GridLines="None" DataKeyNames="FaultLogID" CellSpacing="5">
                <Columns>
                    <asp:BoundField ItemStyle-Width="100px" DataField="AptType" />
                    <asp:BoundField HeaderText="Status" DataField="AptTime" ItemStyle-Width="150px" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="propertyInfo">
            <asp:Label runat="server" CssClass="leftControl" Text="Service charges :" Font-Bold="true"></asp:Label>
            <asp:Label ID="lblServiceCharges" runat="server" CssClass="rightControl" Text=""></asp:Label>
            <br />
        </div>
    </div>
    <div id="rightContainer">
        <div id="topLevel1">
            <div class="topLevel2Left">
                <div class="topLeftLevel3Top">
                    <b>Interesting Stats:</b><br />
                    <br />
                </div>
                <div class="topLeftLevel3Bottom">
                    <b>Tenancy Details:</b><br />
                    <br />
                    <asp:GridView ID="grdTenancyHistory" HeaderStyle-HorizontalAlign="left"
                        runat="server" AutoGenerateColumns="False" ShowHeader="true" HorizontalAlign="left"
                        Width="100%" GridLines="None" CellPadding="3" CellSpacing="5">
                        <Columns>
                            <asp:BoundField HeaderText="Name" HeaderStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-Width="150px" DataField="FullCustomerName" />
                            <asp:BoundField HeaderText="Start" DataField="StartDate" ItemStyle-Width="100px"
                                HeaderStyle-HorizontalAlign="left" />
                            <asp:BoundField HeaderText="End" DataField="EndDate" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="left" />
                            <asp:BoundField HeaderText="Total Rent" DataField="TotalRent" ItemStyle-Width="100px"
                                HeaderStyle-HorizontalAlign="left" />
                        </Columns>
                        <EmptyDataTemplate>
                            No Records Found</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
            <div class="topLevel2Middle">
                <div class="topMiddleLevel3Top">
                    <asp:Image ID="Image1" runat="server" Width="100%" Height="100%" />
                </div>
                <div class="topMiddleLevel3Bottom">
                    <asp:Label ID="Label15" runat="server" CssClass="leftControl" Text="Site/Floor plans:"></asp:Label>
                    (<asp:Label ID="lblSiteFloorPlansCount" runat="server" CssClass="rightControl" Text=""></asp:Label>)
                    <br />
                </div>
            </div>
            <div class="topLevel2Right">
                <div class="topMiddleLevel3Top">
                    <asp:DataList ID="dlImages" runat="server" CellSpacing="50" RepeatColumns="5" CellPadding="15">
                        <ItemTemplate>
                            <a id="imageLink" href='<%# Eval("ImageName","../../../RSLApplianceServicing/Photographs/Images/{0}") %>'
                                rel="lightbox[Property]" runat="server">
                                <asp:Image ID="imgProperty" ImageUrl='<%# Bind("ImageName", "../../../RSLApplianceServicing/Photographs/Images/{0}") %>'
                                    runat="server" Height="200px" Width="150px" CssClass="removeBorder" />
                            </a>
                        </ItemTemplate>
                    </asp:DataList>
                    <asp:Image ID="imgEmpty" runat="server" ImageUrl="../../Images/noimage.gif" Visible="false"
                        Width="100%" Height="100%" />
                </div>
                <div class="topMiddleLevel3Bottom">
                    <asp:Label ID="Label17" runat="server" CssClass="leftControl" Text="Asbestos:"></asp:Label>
                    <asp:Image ID="imgAsbestos" ImageUrl="../../Images/asbestosSignal.png" CssClass="asbestoscontrol"
                        Visible="false" runat="server" Width="20px" Height="20px" />
                    <br />
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="updPanelReportedFaults" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="middleLevel1">
                    <b>
                        <asp:Label ID="Label19" runat="server" CssClass="leftControl" Text="Reported Faults/Defects:"></asp:Label></b>
                    <asp:DropDownList Width="150px" ID="ddlFaultType" runat="server" CssClass="rightControl"
                        AutoPostBack="True">
                        <asp:ListItem Text="Select Type" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Gas" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Reactive" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                    <br />
                    <br />
                    <asp:GridView ID="grdReportedFaults" HeaderStyle-HorizontalAlign="left" 
                        runat="server" AutoGenerateColumns="False" ShowHeader="true" Width="100%" GridLines="None"
                        CellSpacing="5" CellPadding="4">
                        <Columns>
                            <asp:TemplateField HeaderText="Reported:">
                                <ItemTemplate>
                                    <asp:Label ID="lblReported" runat="server" Text='<%#Eval("ReportedDate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ref:">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkbtnRef" OnClick="ViewFault" runat="server" Text='<%#Eval("JobSheetNumber") %>'></asp:LinkButton>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Fault/Defect:">
                                <ItemTemplate>
                                    <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="200px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type:">
                                <ItemTemplate>
                                    <asp:Label ID="lblFaultType" runat="server" Text='<%#Eval("Type") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status:">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="70px" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No Records Found</EmptyDataTemplate>
                    </asp:GridView>
                    <asp:LinkButton ID="lnkBtnViewAll" runat="server" Text="View All ..." CssClass="rightControl"
                        ForeColor="#000000" Font-Underline="false" Visible="false"></asp:LinkButton>
                </div>
                <!-- ModalPopupExtender -->
                <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                <ajaxToolkit:ModalPopupExtender ID="popupJobSheet" runat="server" PopupControlID="pnlJobSheet"
                    TargetControlID="btnHidden" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="height: 65%;
                    overflow: auto;">
                    <div id="header" style="height: 70px; width: 100%; background-color: #E51837;">
                        <p id="header_label">
                            BHG Fault Locator
                        </p>
                        <div id="header_box">
                        </div>
                    </div>
                    <br />
                    <br />
                    <asp:Panel ID="pnlPopupMessage" runat="server" Visible="false">
                        <asp:Label ID="lblPopupMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                    <div style="height: auto; clear: both;">
                        <table id="jobsheet_detail_table" style="width: 100%; margin-top: 10px; text-align: left;">
                            <tr>
                                <td colspan="3">
                                    <asp:Label ID="lblRecallDetailsFor" runat="server" Text="Job sheet summary for : "
                                        Font-Bold="true"></asp:Label>
                                    <asp:Label ID="lblClientNameHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    &nbsp;<asp:Label ID="lblClientStreetAddressHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    &nbsp;,
                                    <asp:Label ID="lblClientCityHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    &nbsp;,
                                    <asp:Label ID="lblClientPostalcodeHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    &nbsp;<asp:Label ID="lblClientTelHeader" runat="server" Text=" Tel : " Font-Bold="true"> </asp:Label><asp:Label
                                        ID="lblClientTelPhoneNumberHeader" runat="server" Text="" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="lblClientName" runat="server" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contractor :
                                </td>
                                <td>
                                    <asp:Label ID="lblContractor" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClientStreetAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Operative :
                                </td>
                                <td>
                                    <asp:Label ID="lblOperativeName" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClientCity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Priority :
                                </td>
                                <td>
                                    <asp:Label ID="lblFaultPriority" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClientRegion" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Completion due :
                                </td>
                                <td>
                                    <asp:Label ID="lblFaultCompletionDateTime" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblClientPostCode" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Order date :
                                </td>
                                <td>
                                    <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                                </td>
                                <td>
                                    Tel :
                                    <asp:Label ID="lblClientTelPhoneNumber" runat="server" Text="" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Location :
                                </td>
                                <td>
                                    <asp:Label ID="lblFaultLocation" runat="server"></asp:Label>
                                </td>
                                <td>
                                    Mobile :
                                    <asp:Label ID="lblClientMobileNumber" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Description :
                                </td>
                                <td>
                                    <asp:Label ID="lblFaultDescription" runat="server" Text="" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    Email :
                                    <asp:Label ID="lblClientEmailId" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Trade :
                                </td>
                                <td>
                                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Note :
                                </td>
                                <td>
                                    <asp:TextBox Enabled="false" ID="lblFaultOperatorNote" CssClass="roundcornerby5"
                                        runat="server" TextMode="MultiLine" Height="65px" Width="248px"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Appointment :</b>
                                </td>
                                <td>
                                    <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <b>Asbestos :</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Label ID="lblFaultAppointmentDateTime" runat="server" Text=""></asp:Label>
                                </td>
                                <td rowspan="2">
                                    <div style="height: 65px; overflow: auto;">
                                        <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                            BorderStyle="None" CellSpacing="3" GridLines="None">
                                            <Columns>
                                                <asp:BoundField DataField="AsbRiskID" />
                                                <asp:BoundField DataField="Description" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:TextBox ID="txtAppointmentNote" Enabled="false" runat="server" TextMode="MultiLine"
                                        CssClass="roundcornerby5" Height="53px" Width="366px"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;
                                    <asp:TextBox ID="txtJsnumber" runat="server" BorderStyle="None" ForeColor="White"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <%--<asp:TextBox ID="txtJsnumber" runat="server" OnTextChanged="txtJsnumber_TextChanged"></asp:TextBox>--%>
                    </div>
                    <div style="width: 100%; text-align: left; clear: both;">
                        <div style="float: right;">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="margin_right20" />
                            <input id="btnPrintJobSheet" type="button" value="Print Job Sheet" class="margin_right20"
                                onclick="PrintJobSheet()" />
                        </div>
                    </div>
                </asp:Panel>
                <!-- ModalPopupExtender -->
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="bottomLevel1">
            <div id="bottomLeftLevel2">
                <b>Warranties:</b><br />
                <br />
                <asp:GridView ID="GridView2" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="False"
                    ShowHeader="true" Width="100%" GridLines="None" DataKeyNames="FaultLogID" CellSpacing="5">
                    <Columns>
                        <asp:BoundField HeaderText="Type:" DataField="Type" ItemStyle-Width="150px" />
                        <asp:BoundField HeaderText="For:" DataField="For" ItemStyle-Width="150px" />
                        <asp:BoundField HeaderText="Expiry:" DataField="Expiry" ItemStyle-Width="150px" />
                    </Columns>
                </asp:GridView>
            </div>
            <div id="bottomRightLevel2">
                <b>Rent Details:</b><br />
                <br />
                <asp:GridView ID="GridView3" runat="server" ShowHeaderWhenEmpty="true" AutoGenerateColumns="False"
                    ShowHeader="true" Width="100%" GridLines="None" DataKeyNames="FaultLogID" CellSpacing="5">
                    <Columns>
                        <asp:BoundField HeaderText="Rent:" DataField="Rent" ItemStyle-Width="150px" />
                        <asp:BoundField HeaderText="Set:" DataField="Set" ItemStyle-Width="150px" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</div>
