﻿Imports P_BusinessLogic
Imports P_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling


Public Class SummaryControl
    Inherits UserControlBase


#Region "Properties"

    Dim objPropertyBL As PropertyBL = New PropertyBL()

#End Region

#Region "Events"

#Region "Page Load Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "View Fault Link Button"

    Protected Sub ViewFault(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim lnkBtnJobSheet As LinkButton = DirectCast(sender, LinkButton)

            Dim row As GridViewRow = DirectCast(lnkBtnJobSheet.NamingContainer, GridViewRow)
            Dim lblFaultType As Label = DirectCast(row.FindControl("lblFaultType"), Label)

            If (lblFaultType.Text.Equals("Reactive")) Then
                Me.displayJobSheetInfo(CType(lnkBtnJobSheet.Text, String))
            Else

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Fault type dropdown index changed"
    ''' <summary>
    ''' Event fires when dropdown value is changed from fault type.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlFaultType_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlFaultType.SelectedIndexChanged

        Try

            Dim faultDataset As DataSet = getFaultsResultDataSetViewState()
            Dim faultDatatable As DataTable = New DataTable()
            Dim type As String

            If (ddlFaultType.SelectedValue = 1) Then
                type = "Gas"
            ElseIf (ddlFaultType.SelectedValue = 2) Then
                type = "Reactive"
            Else
                type = "Default"
            End If

            If (faultDataset.Tables.Count > 0) Then
                faultDatatable = faultDataset.Tables(0)

                If (Not type.Equals("Default")) Then
                    Dim query = _
                                From faultsDefects In faultDatatable.AsEnumerable() _
                                Where faultsDefects.Field(Of String)("Type") = type _
                                Select faultsDefects

                    Dim boundTable As DataTable = faultDataset.Tables(0).Clone()

                    If (query.Count > 0) Then
                        boundTable = query.CopyToDataTable()
                    End If

                    setViewAllButtonState(boundTable)

                    grdReportedFaults.DataSource = boundTable
                Else
                    grdReportedFaults.DataSource = faultDataset.Tables(0)
                    setViewAllButtonState(faultDataset.Tables(0))
                End If

            End If
            grdReportedFaults.DataBind()
            updPanelReportedFaults.Update()


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Display Job Sheet Details"
    Private Sub displayJobSheetInfo(ByVal jobSheetNumber As String)


        Try
            Me.resetJobsheetControls()

            Dim resultDataSet As DataSet = New DataSet()
            Dim objPropertyBl As PropertyBL = New PropertyBL()

            objPropertyBl.getJobSheetDetails(resultDataSet, jobSheetNumber)


            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.NoRecordFound, True)
                grdAsbestos.Visible = False
            Else
                grdAsbestos.Visible = True
                grdAsbestos.DataSource = resultDataSet.Tables("Asbestos")
                grdAsbestos.DataBind()

                lblFaultId.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(0).ToString
                lblContractor.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(1).ToString
                lblOperativeName.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(2).ToString
                lblFaultPriority.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(3).ToString
                lblFaultCompletionDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(4).ToString
                lblOrderDate.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(5).ToString
                lblFaultLocation.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(6).ToString
                lblFaultDescription.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(7).ToString
                lblFaultOperatorNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(8).ToString
                lblFaultAppointmentDateTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(9).ToString
                txtAppointmentNote.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(10).ToString
                lblTime.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(11).ToString
                lblTrade.Text = resultDataSet.Tables("FaultAppointment").Rows(0)(12).ToString


            End If


            If (resultDataSet.Tables("Customer").Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, UserMessageConstants.NoRecordFound, True)
                grdAsbestos.Visible = False
            Else

                ' Header Customer Labels
                lblClientNameHeader.Text = resultDataSet.Tables("Customer").Rows(0)(0).ToString
                lblClientStreetAddressHeader.Text = resultDataSet.Tables("Customer").Rows(0)(1).ToString
                lblClientCityHeader.Text = resultDataSet.Tables("Customer").Rows(0)(2).ToString
                lblClientPostalcodeHeader.Text = resultDataSet.Tables("Customer").Rows(0)(3).ToString
                lblClientTelPhoneNumberHeader.Text = resultDataSet.Tables("Customer").Rows(0)(5).ToString

                ' Body customer Labels

                lblClientName.Text = resultDataSet.Tables("Customer").Rows(0)(0).ToString
                lblClientStreetAddress.Text = resultDataSet.Tables("Customer").Rows(0)(1).ToString
                lblClientCity.Text = resultDataSet.Tables("Customer").Rows(0)(2).ToString
                lblClientPostCode.Text = resultDataSet.Tables("Customer").Rows(0)(3).ToString
                lblClientRegion.Text = resultDataSet.Tables("Customer").Rows(0)(4).ToString
                lblClientTelPhoneNumber.Text = resultDataSet.Tables("Customer").Rows(0)(5).ToString
                lblClientMobileNumber.Text = resultDataSet.Tables("Customer").Rows(0)(6).ToString
                lblClientEmailId.Text = resultDataSet.Tables("Customer").Rows(0)(7).ToString

            End If
            ' reBindGridForLastPage()

            Me.popupJobSheet.Show()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Reset Job sheet summary"

    Sub resetJobsheetControls()

        ' Job Sheet Detail
        lblFaultId.Text = String.Empty
        lblContractor.Text = String.Empty
        lblOperativeName.Text = String.Empty
        lblFaultPriority.Text = String.Empty
        lblFaultCompletionDateTime.Text = String.Empty
        lblOrderDate.Text = String.Empty
        lblFaultLocation.Text = String.Empty
        lblFaultDescription.Text = String.Empty
        lblFaultOperatorNote.Text = String.Empty
        lblTrade.Text = String.Empty

        ' Header Customer Labels
        lblClientNameHeader.Text = String.Empty
        lblClientStreetAddressHeader.Text = String.Empty
        lblClientCityHeader.Text = String.Empty
        lblClientTelPhoneNumberHeader.Text = String.Empty

        ' Body customer Labels

        lblClientName.Text = String.Empty
        lblClientStreetAddress.Text = String.Empty
        lblClientCity.Text = String.Empty
        lblClientPostCode.Text = String.Empty
        lblClientRegion.Text = String.Empty
        lblClientTelPhoneNumber.Text = String.Empty
        lblClientMobileNumber.Text = String.Empty
        lblClientEmailId.Text = String.Empty
        grdAsbestos.DataSource = Nothing

        lblTime.Text = String.Empty
        lblFaultAppointmentDateTime.Text = String.Empty
        txtAppointmentNote.Text = String.Empty

        lblPopupMessage.Text = String.Empty
    End Sub
#End Region

#Region "Populate Tenancy/Property detail"
    ''' <summary>
    ''' Populate Tenancy/Property detail
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Public Sub populateTenancyPropertyDetail(ByVal propertyId As String)

        Me.resetTenancyPropertyControl()

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getTenancyPropertyDetail(resultDataSet, propertyId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblTenancyProperty, pnlTenancyProperty, UserMessageConstants.NoRecordFound, True)
        Else

            lblRefNumber.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("PROPERTYID").ToString()
            lblAddress.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("ADDRESS").ToString()
            lblPostCode.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("POSTCODE").ToString()
            lblDev.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("DEVELOPMENTNAME").ToString()
            lblScheme.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("SCHEMENAME").ToString()
            lblStatus.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("STATUS").ToString() + " (" + Me.getCurrentCustomer(propertyId) + ") "
            lblType.Text = resultDataSet.Tables("TenancyPropertyDetail").Rows(0)("PROPERTYTYPE").ToString()

        End If


    End Sub

#End Region

#Region "Reset Tenancy/Property controls"
    ''' <summary>
    ''' Reset Tenancy/Property controls
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub resetTenancyPropertyControl()

        lblRefNumber.Text = String.Empty
        lblAddress.Text = String.Empty
        lblPostCode.Text = String.Empty
        lblDev.Text = String.Empty
        lblScheme.Text = String.Empty
        lblStatus.Text = String.Empty
        lblType.Text = String.Empty
        lblTenancyProperty.Text = String.Empty

    End Sub

#End Region

#Region "Populate Accommodation summary"
    ''' <summary>
    ''' Populate Accommodation summary
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub populateAccommodationSummary(ByVal propertyId As String)

        Me.resetAccommodationSummary()

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getAccommodationSummary(resultDataSet, propertyId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblAccommodation, pnlAccomodation, UserMessageConstants.NoRecordFound, True)
        Else

            lblFloorArea.Text = IIf(IsNothing(resultDataSet.Tables("Accommodation").Rows(0)("FLOORAREA")), "N/A", resultDataSet.Tables("Accommodation").Rows(0)("FLOORAREA").ToString() + " sq m")
            lblMaxPeople.Text = IIf(IsNothing(resultDataSet.Tables("Accommodation").Rows(0)("MAXPEOPLE")), "N/A", resultDataSet.Tables("Accommodation").Rows(0)("MAXPEOPLE").ToString())
            lblBedrooms.Text = IIf(IsNothing(resultDataSet.Tables("Accommodation").Rows(0)("BEDROOMS")), "N/A", resultDataSet.Tables("Accommodation").Rows(0)("BEDROOMS").ToString())

        End If

    End Sub

#End Region

#Region "Reset Accommodation summary"
    ''' <summary>
    ''' Reset Accommodation summary
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub resetAccommodationSummary()

        lblFloorArea.Text = String.Empty
        lblMaxPeople.Text = String.Empty
        lblBedrooms.Text = String.Empty
        lblAccommodation.Text = String.Empty

    End Sub

#End Region

#Region "Populate Tenancy History"
    ''' <summary>
    ''' Populate Tenancy History
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub populateTenancyHistory(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getTenancyHistory(resultDataSet, propertyId)
        grdTenancyHistory.DataSource = resultDataSet.Tables(0).DefaultView
        grdTenancyHistory.DataBind()

    End Sub

#End Region

#Region "Populate Asbestos"
    ''' <summary>
    ''' Populate Asbestos detail
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub populateAsbestosDetail(ByVal propertyId As String)

        Dim resultDataSet As DataSet = New DataSet()
        Dim count As Integer = 0
        count = objPropertyBL.getAsbestosDetail(propertyId)

        If (count > 0) Then
            imgAsbestos.Visible = True
        Else
            imgAsbestos.Visible = False
        End If

    End Sub

#End Region

#Region "Set View All button state"
    ''' <summary>
    ''' Set View All button state
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub setViewAllButtonState(ByVal dtFaults As DataTable)

        If (dtFaults.Rows.Count >= 5) Then
            lnkBtnViewAll.Visible = True

        Else
            lnkBtnViewAll.Visible = False
        End If

    End Sub

#End Region

#Region "Populate reported Faults/Defects"
    ''' <summary>
    ''' Populate reported Faults/Defects
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub populateReportedFaultsDefects()

        Dim propertyId As String = SessionManager.getPropertyId()

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getReportedFaultsDefects(resultDataSet, propertyId)

        grdReportedFaults.DataSource = resultDataSet.Tables(0).DefaultView
        grdReportedFaults.DataBind()
        setFaultsResultDataSetViewState(resultDataSet)
        setViewAllButtonState(resultDataSet.Tables(0))


    End Sub

#End Region

#Region "Populate property Images"
    ''' <summary>
    ''' Populate property Images
    ''' </summary>    
    ''' <remarks></remarks>
    '''     
    Sub populatePropertyImages()

        Dim propertyId As String = SessionManager.getPropertyId()

        Dim resultDataSet As DataSet = New DataSet()

        objPropertyBL.getPropertyImages(resultDataSet, propertyId)

        Dim dtFiltered As DataTable = Me.checkImage(resultDataSet.Tables(0))

        If (dtFiltered.Rows.Count > 0) Then

            imgEmpty.Visible = False
            dlImages.DataSource = dtFiltered.DefaultView
            dlImages.DataBind()
        Else
            imgEmpty.Visible = True
        End If

    End Sub

#End Region

#Region "Result Faults DataSet Set/Get/Remove"
    ''' <summary>
    ''' Result Faults DataSet Set/Get/Remove
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Protected Sub setFaultsResultDataSetViewState(ByRef resultDataSet As DataSet)
        ViewState.Item(ViewStateConstants.FaultsResultDataSet) = resultDataSet
    End Sub

    Protected Function getFaultsResultDataSetViewState() As DataSet
        If IsNothing(ViewState.Item(ViewStateConstants.FaultsResultDataSet)) Then
            Return New DataSet()
        Else
            Return CType(ViewState.Item(ViewStateConstants.FaultsResultDataSet), DataSet)
        End If
    End Function

    Protected Sub removeFaultsResultDataSetViewState()
        ViewState.Remove(ViewStateConstants.FaultsResultDataSet)
    End Sub

#End Region

#Region "Get Empty Table"
    ''' <summary>
    ''' Get Empty Table
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetEmptyTable() As DataTable
        ' Create new DataTable instance.
        Dim table As New DataTable
        ' Create four typed columns in the DataTable.
        table.Columns.Add("ImageName", GetType(String))

        ' Add five rows with those columns filled in the DataTable.
        table.Rows.Add("noimage.gif")
        Return table
    End Function

#End Region

#Region "Check whether image exists."

    Function checkImage(ByVal dtImages As DataTable) As DataTable

        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr(1).ToString()
            Dim fullPath As String = PathConstants.AppliancesImagePath + imageName
            If Not System.IO.File.Exists(Server.MapPath(fullPath)) Then
                dr.Row.Delete()
            End If
        Next

        dtImages.AcceptChanges()

        Return dtImages

    End Function

#End Region

#Region "Get Current Customer"

    Function getCurrentCustomer(ByVal propertyId As String) As String
        Dim resultDataSet As DataSet = New DataSet()
        objPropertyBL.getTenancyHistory(resultDataSet, propertyId)
        Dim dtTenant As DataTable = resultDataSet.Tables(0)
        Dim customerName As String = ""
        If dtTenant.Rows.Count > 0 Then
            customerName = dtTenant.Rows(0)("FullCustomerName").ToString()
            Return customerName
        Else
            Return customerName
        End If

    End Function

#End Region

#End Region


End Class