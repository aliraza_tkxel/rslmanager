﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports JRA_Utilities
Imports JRA_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports JRA_BusinessObject
Imports System.Data.SqlClient

Namespace JRA_BusinessLogic

    Public Class MasterPageBL

        Sub getRSLModules(ByRef businessMenuDataset As DataSet, ByVal employeeId As Integer)
            Dim objDashboard As DashboardDAL = New DashboardDAL()
            objDashboard.getBusinessMenus(businessMenuDataset, employeeId)

        End Sub

        Sub getRSLPage(ByRef pageMenuDatset As DataSet, ByVal employeeId As Integer, ByVal selectedMenuId As Integer)
            Dim objDashboard As DashboardDAL = New DashboardDAL()
            objDashboard.getRSLPage(pageMenuDatset, employeeId, selectedMenuId)
        End Sub

        Sub getRSLMenus(ByRef resultDataset As DataSet, ByVal employeeId As Integer)
            Dim objDashboard As DashboardDAL = New DashboardDAL()
            objDashboard.getRSLMenus(resultDataset, employeeId)
        End Sub

#Region "Check employee page access"
        ''' <summary>
        ''' Check employee page access
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Sub checkEmployeePageAccess(ByRef resultDataset As DataSet, ByVal employeeId As Integer, ByVal pageName As String)

            Dim objDashboard As DashboardDAL = New DashboardDAL()
            objDashboard.checkEmployeePageAccess(resultDataset, employeeId, pageName)

        End Sub

#End Region

    End Class
End Namespace
