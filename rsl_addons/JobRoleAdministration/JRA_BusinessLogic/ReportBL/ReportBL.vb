﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports JRA_Utilities
Imports JRA_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports JRA_BusinessObject
Imports System.Data.SqlClient

Public Class ReportBL

    Dim objReportDAL As ReportDAL = New ReportDAL()

#Region "Get Job Role Actions"
    Public Sub getjobRoles(ByRef resultDataSet As DataSet)
        objReportDAL.getjobRoles(resultDataSet)
    End Sub
#End Region

#Region "Get Users"
    Public Sub getUsers(ByRef resultDataSet As DataSet)
        objReportDAL.getUsers(resultDataSet)
    End Sub
#End Region

#Region "get Report Data"
    Public Function getReportData(ByVal userID As Integer, ByVal actionID As Integer, ByVal selectedDate As String, ByRef reportDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal searchText As String, ByVal allowPaging As Boolean)
        Return objReportDAL.getReportData(userID, actionID, selectedDate, reportDataSet, objPageSortBo, searchText, allowPaging)
    End Function
#End Region

    'Sub getSearchResult(ByRef resultDataset As DataSet, ByVal searchText As String)
    '    objReportDAL.getReportData(userID, actionID, selectedDate, reportDataSet, objPageSortBo, searchText)
    'End Sub

End Class
