﻿Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports JRA_Utilities
Imports JRA_DataAccess

Imports System
Imports System.IO
Imports System.Data.Common
Imports JRA_BusinessObject
Imports System.Data.SqlClient


Namespace JRA_BusinessLogic

    Public Class JobRoleBL

#Region "Get Team Modules"
        ''' <summary>
        ''' Get Team Modules
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getTeamModules(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)
            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getTeamModules(resultDataSet, teamJobRoleId)
        End Sub
#End Region

#Region "Get Team Menus"
        ''' <summary>
        ''' Get Team Menus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getTeamMenus(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal moduleId As Integer)
            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getTeamMenus(resultDataSet, teamJobRoleId, moduleId)
        End Sub
#End Region

#Region "Get Team Pages"
        ''' <summary>
        ''' Get Team Pages
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getTeamPages(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal menuId As Integer)
            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getTeamPages(resultDataSet, teamJobRoleId, menuId)
        End Sub
#End Region

#Region "Get Alerts List"
        ''' <summary>
        ''' Get Alerts List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAlertsList(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)
            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getAlertsList(resultDataSet, teamJobRoleId)
        End Sub
#End Region

#Region "Get Intranet Main Menus"
        ''' <summary>
        ''' Get Intranet Main Menus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getIntranetMainMenus(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getIntranetMainMenus(resultDataSet, teamJobRoleId)

        End Sub
#End Region

#Region "Get Intranet SubMenus"
        ''' <summary>
        ''' Get Intranet SubMenus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getIntranetSubMenus(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal mainmenuId As Integer)

               Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getIntranetSubMenus(resultDataSet, teamJobRoleId, mainmenuId)

        End Sub
#End Region

#Region "Get Intranet Pages"
        ''' <summary>
        ''' Get Intranet Pages
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getIntranetPages(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal submenuId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getIntranetPages(resultDataSet, teamJobRoleId, submenuId)

        End Sub
#End Region

#Region "Get All Teams"
        ''' <summary>
        ''' Get All Teams
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAllTeams(ByRef resultDataSet As DataSet)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getAllTeams(resultDataSet)

        End Sub
#End Region

#Region "Check Team JoRole Exist"
        ''' <summary>
        ''' Check Team JoRole Exist
        ''' </summary>
        ''' <param name="objTeamJobRoleBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function checkTeamJobRoleExist(ByVal objTeamJobRoleBO As TeamJobRoleBO)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.checkTeamJobRoleExist(objTeamJobRoleBO)

        End Function

#End Region

#Region "Save Team JoRole"
        ''' <summary>
        ''' Save Team JoRole
        ''' </summary>
        ''' <param name="objTeamJobRoleBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveTeamJobRole(ByVal objTeamJobRoleBO As TeamJobRoleBO, ByVal userId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.saveTeamJobRole(objTeamJobRoleBO, userId)

        End Function

#End Region

#Region "Search Team JoRole"
        ''' <summary>
        ''' Search Team JoRole
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="searchText"></param>
        ''' <param name="teamId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function searchTeamJobRole(ByRef resultDataset As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal teamId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.searchTeamJobRole(resultDataset, objPageSortBo, searchText, teamId)

        End Function

#End Region

#Region "Update Team JoRole"
        ''' <summary>
        ''' Update Team JoRole
        ''' </summary>
        ''' <param name="objTeamJobRoleBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function updateTeamJobRole(ByVal objTeamJobRoleBO As TeamJobRoleBO, ByVal userId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.updateTeamJobRole(objTeamJobRoleBO, userId)

        End Function

#End Region

#Region "Get team employees"
        ''' <summary>
        ''' Get team employees
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getTeamEmployees(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.getTeamEmployees(resultDataSet, teamJobRoleId)

        End Sub
#End Region

#Region "Delete Team JoRole"
        ''' <summary>
        ''' Delete Team JoRole
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function deleteTeamJobRole(ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.deleteTeamJobRole(teamJobRoleId)

        End Function

#End Region

#Region "Search Full Team JoRole"
        ''' <summary>
        ''' Search Full Team JoRole
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="teamId"></param>        
        ''' <remarks></remarks>
        Public Sub searchFullTeamJobRole(ByRef resultDataset As DataSet, ByVal searchText As String, ByVal teamId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.searchFullTeamJobRole(resultDataset, searchText, teamId)

        End Sub

#End Region

#Region "Remove Team JobRole Alerts Access Rights"
        ''' <summary>
        ''' Remove Team JobRole Alerts Access Rights
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub removeTeamJobRoleAlertsAccessRights(ByVal teamJobRoleId As Integer)
            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.removeTeamJobRoleAlertsAccessRights(teamJobRoleId)
        End Sub

#End Region

#Region "Save Team JobRole Alerts Access Right"
        ''' <summary>
        ''' Save Team JobRole Alerts Access Right
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub saveTeamJobRoleAlertsAccessRights(ByVal alertDt As DataTable, ByVal teamJobRoleId As Integer, ByVal userId As Integer)
            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.saveTeamJobRoleAlertsAccessRights(alertDt, teamJobRoleId, userId)
        End Sub

#End Region

#Region "Reset Intranet Team JobRole Modules Access Rights"
        ''' <summary>
        ''' Reset Intranet Team JobRole Modules Access Rights
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub resetIntranetTeamAccessRights(ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.resetIntranetTeamAccessRights(teamJobRoleId)

        End Sub

#End Region

#Region "Reset Team JobRole Modules Access Rights"
        ''' <summary>
        ''' Reset Team JobRole Modules Access Rights
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub resetTeamAccessRights(ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            objJobRoleDal.resetTeamAccessRights(teamJobRoleId)

        End Sub

#End Region

#Region "Save Modules Access Rights"
        ''' <summary>
        ''' Save Modules Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveModulesAccessRights(ByVal moduleDt As DataTable, ByVal menuDt As DataTable, ByVal pageDt As DataTable, ByVal teamJobRoleId As Integer, ByVal userId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.saveModulesAccessRights(moduleDt, menuDt, pageDt, teamJobRoleId, userId)

        End Function

#End Region

#Region "Save Menu Access Rights"
        ''' <summary>
        ''' Save Menu Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveMenuAccessRights(ByVal menuId As Integer, ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.saveMenuAccessRights(menuId, teamJobRoleId)

        End Function

#End Region

#Region "Save Page Access Rights"
        ''' <summary>
        ''' Save Page Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function savePageAccessRights(ByVal pageId As Integer, ByVal teamJobRoleId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.savePageAccessRights(pageId, teamJobRoleId)

        End Function

#End Region

#Region "Save Intranet Page Access Rights"
        ''' <summary>
        ''' Save Intranet Page Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveIntranetPageAccessRights(ByVal pageDt As DataTable, ByVal teamJobRoleId As Integer, ByVal userId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.saveIntranetPageAccessRights(pageDt, teamJobRoleId, userId)

        End Function

#End Region

#Region "Search Team Job Role History"
        ''' <summary>
        ''' Search Team Job Role History
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="empId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function searchTeamJobRoleHistory(ByRef resultDataset As DataSet, ByRef objPageSortBo As PageSortBO, ByVal empId As Integer)

            Dim objJobRoleDal As JobRoleDAL = New JobRoleDAL()
            Return objJobRoleDal.searchTeamJobRoleHistory(resultDataset, objPageSortBo, empId)

        End Function

#End Region

    End Class

End Namespace

