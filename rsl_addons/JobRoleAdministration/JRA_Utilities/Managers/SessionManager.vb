﻿Imports System.Web.HttpContext
Imports JRA_BusinessObject


Namespace JRA_Utilities
    Public Class SessionManager


#Region "Set / Get User Full Name"
        ''' <summary>
        ''' This key shall be used to save the logged in user's name
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UserFullName As String = "UserFullName"
#Region "Set User Full Name"
        ''' <summary>
        ''' This function shall be used to save the logged in user's name in session
        ''' </summary>
        ''' <param name="userFullName"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionManager.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"
        ''' <summary>
        ''' This function shall be used to get the logged in user's name from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionManager.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"
        ''' <summary>
        ''' This function shall be used to remove the logged in user's name from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionManager.UserFullName))) Then
                Current.Session.Remove(SessionManager.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"
        ''' <summary>
        ''' This key shall be used to get the Employee Id of logged in user
        ''' </summary>
        ''' <remarks></remarks>
        Public Const EmployeeId As String = "UserEmployeeId"
#Region "Set User Employee Id"
        ''' <summary>
        ''' This function shall be used to save the Employee Id of logged in user in session
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserEmployeeId(ByRef employeeId As Integer)
            Current.Session(SessionManager.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"
        ''' <summary>
        ''' This function shall be used to get the Employee Id of logged in user from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionManager.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"
        ''' <summary>
        ''' This function shall be used to remove the Employee Id of logged in user from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionManager.EmployeeId))) Then
                Current.Session.Remove(SessionManager.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get teamJobRoleId"
        ''' <summary>
        ''' This key will be used to save teamJobRoleId
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TeamJobRoleId As String = "TeamJobRoleId"

#Region "Set TeamJobRoleId"
        ''' <summary>
        ''' Set TeamJobRoleId
        ''' </summary>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTeamJobRoleId(ByRef teamJobRoleId As Integer)
            Current.Session(SessionManager.TeamJobRoleId) = teamJobRoleId
        End Sub
#End Region

#Region "Get TeamJobRoleId"
        ''' <summary>
        ''' Get TeamJobRoleId
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTeamJobRoleId() As Integer

            If (IsNothing(Current.Session(SessionManager.TeamJobRoleId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.TeamJobRoleId), Integer)
            End If
        End Function
#End Region

#Region "Remove TeamJobRoleId"
        ''' <summary>
        ''' Remove TeamJobRoleId
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTeamJobRoleId()
            If (Not IsNothing(Current.Session(SessionManager.TeamJobRoleId))) Then
                Current.Session.Remove(SessionManager.TeamJobRoleId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get  Job Role Access User Id "
        ''' <summary>
        ''' This key shall be used to save the Job Role Access user id
        ''' </summary>
        ''' <remarks></remarks>
        Public Const JobRoleAccessUserId As String = "JobRoleAccessUserId"

#Region "Set  Job Role Access User Id"
        ''' <summary>
        ''' Set  Job Role Access User Id
        ''' </summary>
        ''' <param name="jobRoleAccessUserId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setJobRoleAccessUserId(ByRef jobRoleAccessUserId As Integer)
            Current.Session(SessionManager.JobRoleAccessUserId) = jobRoleAccessUserId
        End Sub
#End Region

#Region "Get Job Role Access User Id"
        ''' <summary>
        ''' This function shall be used to get the Job Role Access user id from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getJobRoleAccessUserId() As Integer

            If (IsNothing(Current.Session(SessionManager.JobRoleAccessUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.JobRoleAccessUserId), Integer)
            End If
        End Function
#End Region

#Region "Remove Job Role Access User Id"
        ''' <summary>
        ''' This function shall be used to remove the Job Role Access user id in session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removedJobRoleAccessUserId()

            If (Not IsNothing(Current.Session(SessionManager.JobRoleAccessUserId))) Then
                Current.Session.Remove(SessionManager.JobRoleAccessUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Team Job Role BO"
        ''' <summary>
        ''' This key shall be used to get letter bo for preview
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TeamJobRoleBO As String = "TeamJobRoleBO"

#Region "Set Team Job Role BO"
        ''' <summary>
        ''' This function shall be used to save Team Job Role BO in session
        ''' </summary>
        ''' <param name="teamJobRoleBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTeamJobRoleBO(ByRef teamJobRoleBO As TeamJobRoleBO)
            Current.Session(SessionManager.TeamJobRoleBO) = teamJobRoleBO
        End Sub

#End Region

#Region "Get Team Job Role BO"
        ''' <summary>
        ''' This function shall be used to get Team Job Role BO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTeamJobRoleBO() As TeamJobRoleBO
            If (IsNothing(Current.Session(SessionManager.TeamJobRoleBO))) Then
                Dim teamJobRoleBO As TeamJobRoleBO = New TeamJobRoleBO()
                Return teamJobRoleBO
            Else
                Return CType(Current.Session.Item(SessionManager.TeamJobRoleBO), TeamJobRoleBO)
            End If
        End Function

#End Region

#Region "Remove Team Job Role BO"
        ''' <summary>
        ''' This function shall be used to remove Team Job Role BO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTeamJobRoleBO()
            If (Not IsNothing(Current.Session(SessionManager.TeamJobRoleBO))) Then
                Current.Session.Remove(SessionManager.TeamJobRoleBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Intranet Page Datatable"
        ''' <summary>
        ''' This key shall be used to set/get intranet page Datatable.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const IntranetPageDt As String = "IntranetPageDt"

#Region "Set Intranet Page Datatable"
        ''' <summary>
        ''' This function shall be used to save intranet page Datatable in session
        ''' </summary>
        ''' <param name="intranetPageDt"></param>
        ''' <remarks></remarks>
        Public Shared Sub setIntranetPageDt(ByRef intranetPageDt As DataTable)
            Current.Session(SessionManager.IntranetPageDt) = intranetPageDt
        End Sub

#End Region

#Region "Get Intranet Page Datatable"
        ''' <summary>
        ''' This function shall be used to get intranet page Datatable from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getIntranetPageDt() As DataTable
            If (IsNothing(Current.Session(SessionManager.IntranetPageDt))) Then
                Dim pageDt As DataTable = New DataTable()
                pageDt.Columns.Add(ApplicationConstants.jobRoleTeamIdCol, GetType(System.Int32))
                pageDt.Columns.Add(ApplicationConstants.PageIdCol, GetType(System.Int32))
                Return pageDt
            Else
                Return CType(Current.Session.Item(SessionManager.IntranetPageDt), DataTable)
            End If
        End Function

#End Region

#Region "Remove Intranet Page Datatable"
        ''' <summary>
        ''' This function shall be used to remove intranet page Datatable from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeIntranetPageDt()
            If (Not IsNothing(Current.Session(SessionManager.IntranetPageDt))) Then
                Current.Session.Remove(SessionManager.IntranetPageDt)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Pages Ds"
        ''' <summary>
        ''' This key shall be used to get and set the pages data Set
        ''' </summary>
        ''' <remarks></remarks>
        Public Const PagesDs As String = "PagesDs"

#Region "Set Pages Ds"
        ''' <summary>
        ''' This function shall be used to save the pages data Set in session
        ''' </summary>
        ''' <param name="objPageDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPagesDs(ByRef objPageDs As DataSet)
            Current.Session(SessionManager.PagesDs) = objPageDs
        End Sub
#End Region

#Region "Get Pages Ds"
        ''' <summary>
        ''' This function shall be used to get the pages data Set from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPagesDs() As DataSet
            If (IsNothing(Current.Session(SessionManager.PagesDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionManager.PagesDs), DataSet)
            End If
        End Function

#End Region

#Region "remove Pages Ds"
        ''' <summary>
        ''' This function shall be used to remove the pages data Set from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePagesDs()
            If (Not IsNothing(Current.Session(SessionManager.PagesDs))) Then
                Current.Session.Remove(SessionManager.PagesDs)
            End If
        End Sub

#End Region

#End Region

    End Class
End Namespace