﻿Imports System.Runtime.Caching
Imports System.IO

Public Class CacheManager

    ''' <summary>
    ''' This key shall be used to save the cache items
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared cache As ObjectCache = MemoryCache.Default

#Region "Set / Get All Statuses"

    ''' <summary>
    ''' This key will be used to save status
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared AllStatuses As String = "AllStatuses"

#Region "Set All Statuses"
    ''' <summary>
    ''' This function shall be used to save all statueses dataset in cache
    ''' </summary>
    ''' <param name="allStatusDs"></param>
    ''' <remarks></remarks>
    Public Shared Sub setAllStatuses(ByRef allStatusDs As DataSet)
        Dim expirationPolicy As New CacheItemPolicy()
        expirationPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddHours(1)
        cache.Set(CacheManager.AllStatuses, allStatusDs, expirationPolicy)        
    End Sub
#End Region

#Region "get All Statuses"
    ''' <summary>
    ''' This function will return the all status from cache it does not exist then it will return the empty dataset
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getAllStatuses() As DataSet
        If IsNothing(cache.GetCacheItem(CacheManager.AllStatuses)) Then
            Dim allStatusDs As DataSet = New DataSet()
            Return allStatusDs
        Else
            Return CType(cache.GetCacheItem(CacheManager.AllStatuses).Value, DataSet)
        End If

    End Function
#End Region

#Region "remove All Statuses"
    ''' <summary>
    ''' This funciton 'll remove all the statuses from the cache
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub removeAllStatuses()
        If Not IsNothing(cache.GetCacheItem(CacheManager.AllStatuses)) Then
            cache.Remove(CacheManager.AllStatuses)
        End If
    End Sub
#End Region



#End Region

#Region "Set / Get All Statuses"

    ''' <summary>
    ''' This key will be used to save the all actions
    ''' </summary>
    ''' <remarks></remarks>
    Private Shared AllActions As String = "AllActions"

#Region "Set All Actions"
    ''' <summary>
    ''' This function shall be used to save all actions dataset in cache
    ''' </summary>
    ''' <param name="allActionsDs"></param>
    ''' <remarks></remarks>
    Public Shared Sub setAllActions(ByRef allActionsDs As DataSet)
        Dim expirationPolicy As New CacheItemPolicy()
        expirationPolicy.AbsoluteExpiration = DateTimeOffset.Now.AddHours(1)
        cache.Set(CacheManager.AllActions, allActionsDs, expirationPolicy)
    End Sub
#End Region

#Region "get All Actions"
    ''' <summary>
    ''' This function will return the all actions from cache it does not exist then it will return the empty dataset
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getAllActions() As DataSet
        If IsNothing(cache.GetCacheItem(CacheManager.AllActions)) Then
            Dim allActionsDs As DataSet = New DataSet()
            Return allActionsDs
        Else
            Return CType(cache.GetCacheItem(CacheManager.AllActions).Value, DataSet)
        End If

    End Function
#End Region

#Region "remove All Actions"
    ''' <summary>
    ''' This funciton 'll remove all the actions from the cache
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub removeAllActions()
        If Not IsNothing(cache.GetCacheItem(CacheManager.AllActions)) Then
            cache.Remove(CacheManager.AllActions)
        End If
    End Sub
#End Region



#End Region
End Class
