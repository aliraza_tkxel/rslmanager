﻿
Namespace JRA_Utilities
    Public Class ViewStateConstants

#Region "Page Sort Constants"

        Public Const PageSortBo As String = "PageSortBo"
        Public Const TotalCount As String = "TotalCount"
        Public Const ResultDataSet As String = "ResultDataSet"
        Public Const CompleteDataSet As String = "CompleteDataSet"
        Public Const VirtualItemCount As String = "VirtualItemCount"

#End Region

#Region "View State Key Names"
        'Trades Grid View State
        'Public Const TradesGridViewState As String = "TradesViewState"
        Public Const ExistingTradesDataTable As String = "ExistingTradesDataTable"
        Public Const TradesToDeleteViewState As String = "TradesToDeleteViewState"
        Public Const TradesToInsertViewState As String = "TradesToInsertViewState"
#End Region

#Region "General"
        Public Const Search As String = "Search"
        Public Const SchemeId As String = "SchemeId"
        Public Const AppointmentToBeArrangedDataSet As String = "AppointmentToBeArrangedDataSet"
        Public Const AppointmentsArrangedDataSet As String = "AppointmentsArrangedDataSet"
        Public Shared CurrentIndex As String = "CurrentIndex"
        Public Shared TotalJobsheets As String = "TotalJobsheets"
#End Region

#Region "JobRole"
        Public Shared TeamJobRoleBo As String = "TeamJobRoleBo"
#End Region

    End Class
End Namespace

