﻿Namespace JRA_Utilities
    Public Class PathConstants

#Region "URL Constants"

        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const BridgePath As String = "~/Bridge.aspx"
        Public Const JobRoleHighlight As String = "JobRoles.aspx"
        Public Const JobRoleAuditReportHighlight As String = "JobRoleAuditReport.aspx"
        Public Const JobRoleAccessPath As String = "~/Views/JobRoles/JobRoles.aspx"
        Public Const JobRoleAuditReportPath As String = "~/Views/Reports/JobRoleAuditReport.aspx"
        Public Const AccessDeniedPath As String = "~/Views/General/AccessDenied.aspx"

#End Region

#Region "Image path constant"

        Public Const ModuleImagePath As String = "~/Images/tree/module.gif"
        Public Const MenuImagePath As String = "~/Images/tree/menu.gif"
        Public Const PageImagePath As String = "~/Images/tree/page.gif"
        Public Const FolderImagePath As String = "~/Images/tree/folder.gif"

#End Region

#Region "Query String Constants"

        Public Const Src As String = "src"
        Public Const Resources As String = "resources"
        Public Const Index As String = "index"
        Public Const empId As String = "empId"


#End Region

    End Class
End Namespace

