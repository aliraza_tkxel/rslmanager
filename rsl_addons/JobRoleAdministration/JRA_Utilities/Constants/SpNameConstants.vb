﻿Imports System

Namespace JRA_Utilities
    Public Class SpNameConstants

#Region "Master Page"

        Public Const GetEmployeeById As String = "FL_GetEmployeeById"
        Public Const GetBusinessMenus As String = "JRA_MasterBusinessMenus"
        Public Const GetModuleMenus As String = "JRA_MasterPageMenus"
        Public Const GetMenuPages As String = "JRA_MasterPageMenusPages"
        Public Const CheckEmployeePageAccess As String = "JRA_CheckEmployeePageAccess"

#End Region

#Region "Customer"
        Public Const AmendCustomerAddress As String = "FL_AmendCustomerAddress"
#End Region

#Region "Job Role"

        Public Const GetModules As String = "JRA_GetModules"
        Public Const GetMenus As String = "JRA_GetMenus"
        Public Const GetPages As String = "JRA_GetPages"
        Public Const GetAlertsList As String = "JRA_GetAlertsList"
        Public Const GetIntranetMainMenus As String = "JRA_GetIntranetMainMenus"
        Public Const GetIntranetSubMenus As String = "JRA_GetIntranetSubMenus"
        Public Const GetIntranetPages As String = "JRA_GetIntranetPages"
        Public Const GetAllTeams As String = "JRA_GetAllTeams"
        Public Const CheckTeamJobRoleExist As String = "JRA_CheckTeamJobRoleExist"
        Public Const SaveTeamJobRole As String = "JRA_SaveTeamJobRole"
        Public Const UpdateTeamJobRole As String = "JRA_UpdateTeamJobRole"
        Public Const SearchJobRoleList As String = "JRA_SearchJobRoleList"
        Public Const SearchFullJobRoleList As String = "JRA_SearchFullJobRoleList"
        Public Const GetTeamEmployees As String = "JRA_GetTeamEmployees"
        Public Const DeleteTeamJobRole As String = "JRA_DeleteTeamJobRole"
        Public Const RemoveAlertsAccessRightsForTeam As String = "JRA_RemoveAlertsAccessRightsForTeam"
        Public Const SaveAlertsAccessRightsForTeam As String = "JRA_SaveAlertsAccessRightsForTeam"
        Public Const ResetTeamAccessRights As String = "JRA_ResetTeamAccessRights"
        Public Const SavePageAccessRights As String = "JRA_SavePageAccessRights"
        Public Const SaveMenuAccessRights As String = "JRA_SaveMenuAccessRights"
        Public Const SaveModuleAccessRights As String = "JRA_SaveModuleAccessRights"
        Public Const ResetIntranetTeamAccessRights As String = "JRA_ResetIntranetTeamAccessRights"
        Public Const SaveIntranetPageAccessRights As String = "JRA_SaveIntranetPageAccessRights"

#End Region

#Region "Report"
        Public Const GetJRAActions As String = "JRA_GetActions"
        Public Const GetJRAUsers As String = "JRA_GetUsers"
        Public Const JRAGetReportData As String = "JRA_GetReportData"
        Public Const SearchJobRoleHistoryList As String = "JRA_SearchJobRoleHistoryList"
#End Region

    End Class
End Namespace

