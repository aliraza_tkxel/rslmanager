﻿
Namespace JRA_Utilities
    Public Class ApplicationConstants

#Region "General"
        Public Const DropDownDefaultValue As String = "-1"
        ''' <summary>
        ''' This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        ''' </summary>
        ''' <remarks></remarks>
        Public Const LinkButtonType As String = "LinkButtonType"
        ''' <summary>
        ''' This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UpdatePanelType As String = "UpdatePanelType"
        Public Const NotAvailable As String = "N/A"
        Public Const DefaultValue As String = "-1"
        Public Const UpdateValue As String = "Update"
        Public Const SaveValue As String = "Save"

        Public Const PanelDataChanged As String = "1"
        Public Const PanelDataDefault As String = "0"

#End Region

#Region "Resources"
        Public Const DefaultStatusLabelForTreeNode As String = "Add New Status"
        Public Const DefaultActionLabelForTreeNode As String = "Add New Action"
        Public Const DefaultStatusIdForTreeNode As Int16 = -11
        Public Const DefaultActionIdForTreeNode As Int32 = -12
        Public Const StatusTargetForTreeNode As String = "status"
        Public Const ActionTargetForTreeNode As String = "action"

#End Region

#Region "Master Layout"

        Public Const ConWebBackgroundColor As String = "#e6e6e6"
        Public Const ConWebBackgroundProperty As String = "background-color"
        Public Const BusinessMenusDt As String = "BusinessMenusDt"
        Public Const PageMenusDt As String = "PageMenusDt"

        Public Const JobRolePageName As String = "Job Role"
        Public Const JobRoleReportPageName As String = "Job Role Audit"
        Public Const AccessControlMenuName As String = "Access Control"
        Public Const ReportsMenuName As String = "Reports"

#End Region

#Region "CSS"
        Public Const TabClickedCssClass As String = "TabClicked"
        Public Const TabInitialCssClass As String = "TabInitial"
#End Region

#Region "Job Role"

        ' DATA TABLE NAME

        Public Const AllModulesDt As String = "AllModulesDt"
        Public Const TeamModulesDt As String = "TeamModulesDt"

        Public Const AllMenusDt As String = "AllMenusDt"
        Public Const TeamMenusDt As String = "TeamMenusDt"

        Public Const AllPagesDt As String = "AllPagesDt"
        Public Const TeamPagesDt As String = "TeamPagesDt"

        Public Const AlertsDt As String = "AlertsDt"
        Public Const TeamsDt As String = "TeamsDt"

        Public Const IntranetMenusDt As String = "IntranetMenusDt"
        Public Const IntranetSubMenusDt As String = "IntranetSubMenusDt"
        Public Const IntranetPagesDt As String = "IntranetPagesDt"
        Public Const IntranetTeamPagesDt As String = "IntranetTeamPagesDt"
        Public Const EmployeesDt As String = "EmployeesDt"

        ' COLUMN NAMES

        Public Const ModuleNameCol As String = "ModuleName"
        Public Const ModuleIdCol As String = "ModuleId"

        Public Const MenuNameCol As String = "MenuName"
        Public Const MenuIdCol As String = "MenuId"

        Public Const PageNameCol As String = "PageName"
        Public Const PageIdCol As String = "PageId"
        Public Const AccessLevelCol As String = "AccessLevel"
        Public Const ParentPageCol As String = "ParentPage"
        Public Const SortOrderCol As String = "SortOrder"

        Public Const SubMenuNameCol As String = "SubMenuName"
        Public Const SubMenuIdCol As String = "SubMenuId"

        Public Const HideRootStyle As String = "hideRoot"
        Public Const ActionsDt As String = "ActionsDt"
        Public Const UsersDt As String = "UsersDt"
        Public Const ReportDt As String = "ReportDt"

        Public Const jobRoleTeamIdCol As String = "JobRoleTeamId"
        Public Const AlertIdCol As String = "AlertId"

        Public Const MenuTypeId As Integer = 1
        Public Const PageTypeId As Integer = 2
        Public Const PageLevelOne As Integer = 1

        Public Const ModuleTab As Integer = 0
        Public Const AlertTab As Integer = 1
        Public Const IntranetTab As Integer = 2


#End Region

#Region "Report"

        Public Const AllowPaging As Boolean = True

#End Region


    End Class
End Namespace

