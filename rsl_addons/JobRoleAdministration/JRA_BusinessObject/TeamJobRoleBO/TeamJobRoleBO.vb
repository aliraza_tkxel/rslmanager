﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System

Public Class TeamJobRoleBO

#Region "Attributes"

    Private _teamJobRoleId As Integer
    Private _teamId As Integer
    Private _jobRoleId As Integer
    Private _isActive As Boolean

    Private _jobRole As String

#End Region

#Region "Construtor"
    Public Sub New()

        _teamId = -1
        _jobRole = String.Empty

        _teamJobRoleId = -1
        _jobRoleId = -1
        _isActive = False

    End Sub
#End Region

#Region "Properties"

    <RangeValidator(GetType(Integer), "0", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate:="Please select team")> _
    Public Property TeamId() As Integer
        Get
            Return _teamId
        End Get
        Set(ByVal value As Integer)
            _teamId = value
        End Set
    End Property

    <StringLengthValidator(1, 50, MessageTemplate:="Please enter Job Role.")> _
    Public Property JobRole() As String
        Get
            Return _jobRole
        End Get
        Set(ByVal value As String)
            _jobRole = value
        End Set
    End Property

    Public Property TeamJobRoleId() As Integer
        Get
            Return _teamJobRoleId
        End Get
        Set(ByVal value As Integer)
            _teamJobRoleId = value
        End Set
    End Property

    Public Property JobRoleId() As Integer
        Get
            Return _jobRoleId
        End Get
        Set(ByVal value As Integer)
            _jobRoleId = value
        End Set
    End Property

    Public Property IsActive() As Boolean
        Get
            Return _isActive
        End Get
        Set(ByVal value As Boolean)
            _isActive = value
        End Set
    End Property

#End Region

End Class

