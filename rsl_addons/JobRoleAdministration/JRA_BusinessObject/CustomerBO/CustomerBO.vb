﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation

Namespace JRA_BusinessObject
    Public Class CustomerBO

#Region "Attributes"
        Private _customerid As Integer
        Private _name As String
        Private _street As String
        Private _address As String
        Private _area As String
        Private _telephone As String
        Private _mobile As String
        Private _email As String
        Private _city As String
        Private _postCode As String
        Private _patchId As Integer
        Private _patchName As String
#End Region

#Region "Construtor"
        Public Sub New()
            _customerid = 0
            _name = String.Empty
            _street = String.Empty
            _address = String.Empty
            _area = String.Empty
            _telephone = String.Empty
            _mobile = String.Empty
            _email = String.Empty
            _city = String.Empty
            _postCode = String.Empty
            _patchId = 0
            _patchName = String.Empty
        End Sub
#End Region

#Region "Properties"
        ' Get / Set property for _customerid
        Public Property CustomerId() As Integer

            Get
                Return _customerid
            End Get

            Set(ByVal value As Integer)
                _customerid = value
            End Set

        End Property
        ' Get / Set property for _name
        Public Property Name() As String

            Get
                Return _name
            End Get

            Set(ByVal value As String)
                _name = value
            End Set

        End Property
        ' Get / Set property for _street
        Public Property Street() As String

            Get
                Return _street
            End Get

            Set(ByVal value As String)
                _street = value
            End Set

        End Property
        ' Get / Set property for _address
        Public Property Address() As String

            Get
                Return _address
            End Get

            Set(ByVal value As String)
                _address = value
            End Set

        End Property
        ' Get / Set property for _area
        Public Property Area() As String

            Get
                Return _area
            End Get

            Set(ByVal value As String)
                _area = value
            End Set

        End Property
        ' Get / Set property for _telephone

        <RegexValidator("^\+?\d+(-\d+)*$", MessageTemplate:="Invalid Telephone number.")> _
        <StringLengthValidator(0, 18, MessageTemplate:="Invalid Telephone number.")> _
        Public Property Telephone() As String

            Get
                Return _telephone
            End Get

            Set(ByVal value As String)
                _telephone = value
            End Set

        End Property
        ' Get / Set property for _mobile
        <RegexValidator("^\+?\d+(-\d+)*$", MessageTemplate:="Invalid Mobile number.")> _
        <StringLengthValidator(0, 18, MessageTemplate:="Invalid Mobile number.")> _
        Public Property Mobile() As String

            Get
                Return _mobile
            End Get

            Set(ByVal value As String)
                _mobile = value
            End Set

        End Property

        ' Get / Set email for _email
        <RegexValidator("^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$", MessageTemplate:="Invalid Email address.")> _
        Public Property Email() As String

            Get
                Return _email
            End Get

            Set(ByVal value As String)
                _email = value
            End Set

        End Property

        ' Get / Set cit for _city
        Public Property City() As String

            Get
                Return _city
            End Get

            Set(ByVal value As String)
                _city = value
            End Set

        End Property

        ' Get / Set post code for _email
        Public Property PostCode() As String

            Get
                Return _postCode
            End Get

            Set(ByVal value As String)
                _postCode = value
            End Set

        End Property

        ' Get / Set patch id for _patchid
        Public Property PatchId() As Integer

            Get
                Return _patchId
            End Get

            Set(ByVal value As Integer)
                _patchId = value
            End Set

        End Property

        ' Get / Set patchname for _patchName
        Public Property PatchName() As String

            Get
                Return _patchName
            End Get

            Set(ByVal value As String)
                _patchName = value
            End Set

        End Property

#End Region

    End Class
End Namespace
