﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Modules.ascx.vb" Inherits="JobRoleAccess.Modules" %>

<asp:UpdatePanel ID="updPanelModules" UpdateMode="Conditional"  runat="server">
    <ContentTemplate>

        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <asp:HiddenField ID="hdnModulesChange" runat="server" />
        <div id = "moduleTreeTab" class="viewContainer">
            <asp:Panel runat="server" ID="pnlAccessRight" Visible="true" CssClass="pnlAccessRight">
               
                <asp:TreeView runat="server" ID="trVwAccess"  ShowLines="True" 
                    ForeColor="Black" ExpandDepth="1"                
                    ShowCheckBoxes="All"  LineImagesFolder="~/Images/tree/TreeLineImages" 
                ClientIDMode="Static" >
                    <Nodes>
                        <asp:TreeNode PopulateOnDemand="true"  Text ="Access Control" ImageUrl ="~/Images/tree/base.gif" ShowCheckBox="false"></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>      
                     
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
