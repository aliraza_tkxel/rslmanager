﻿Imports System.Data
Imports System.Data.SqlClient
Imports JRA_BusinessLogic
Imports JRA_Utilities
Imports JRA_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Alerts
    Inherits UserControlBase

#Region "Events"

#Region "Page Load event"
    ''' <summary>
    ''' Page Load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If (Not IsPostBack) Then
                Me.populateAlertGrid()
                'If Convert.ToInt32((Me.Parent.FindControl("count").ToString())) < 1 Then
                'grdAlerts.Attributes.Add("onclick", "postBack(event)")
                'End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "GridView row data bound"
    ''' <summary>
    ''' GridView row data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdAlerts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdAlerts.RowDataBound

        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim chkAlerts As CheckBox = DirectCast(e.Row.FindControl("chkAlerts"), CheckBox)
                Dim lblIsChecked As Label = DirectCast(e.Row.FindControl("lblIsChecked"), Label)
                Dim isChecked As Boolean = IIf(Convert.ToInt32(lblIsChecked.Text) = -1, False, True)
                chkAlerts.Checked = isChecked

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Check Alert Changed"
    ''' <summary>
    ''' Check Alert Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub chkAlerts_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim pnlsavecontrols As Panel = CType(Me.Parent.FindControl("pnlSaveControls"), Panel)
        pnlsavecontrols.Visible = True

        Dim hdnCheckAlert As HiddenField = CType(Me.Parent.FindControl("hdnCheckAlert"), HiddenField)
        hdnCheckAlert.Value = ApplicationConstants.PanelDataChanged

        Dim updPanelJobRoles As UpdatePanel = CType(Me.Parent.FindControl("updPanelJobRoles"), UpdatePanel)
        updPanelJobRoles.Update()

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Populate alert grid"
    ''' <summary>
    ''' Populate alert grid
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateAlertGrid()

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()

        objJobRoleBL.getAlertsList(resultDataset, teamJobRoleId)

        grdAlerts.DataSource = resultDataset.Tables(0)
        grdAlerts.DataBind()

    End Sub

#End Region

#Region "Save Team Alerts"
    ''' <summary>
    ''' Save Team Alerts
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveTeamAlertsAccessRights()

        Dim chkStatus As CheckBox
        Dim lblAlertId As Label
        Dim alertId As Integer

        Dim alertDt As DataTable = New DataTable()
        alertDt.Columns.Add(ApplicationConstants.jobRoleTeamIdCol, GetType(System.Int32))
        alertDt.Columns.Add(ApplicationConstants.AlertIdCol, GetType(System.Int32))

        Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()


        For Each alertGridRow As GridViewRow In grdAlerts.Rows

            chkStatus = DirectCast(alertGridRow.FindControl("chkAlerts"), CheckBox)
            lblAlertId = DirectCast(alertGridRow.FindControl("lblAlertId"), Label)
            alertId = Convert.ToInt32(lblAlertId.Text)

            If (chkStatus.Checked = True) Then
                alertDt.Rows.Add(teamJobRoleId, alertId)
            End If

        Next

        objJobRoleBL.saveTeamJobRoleAlertsAccessRights(alertDt, teamJobRoleId, userId)

    End Sub

#End Region


#End Region

    

End Class