﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Intranet.ascx.vb" Inherits="JobRoleAccess.Intranet" %>

<style type="text/css">
.hideRoot
{
    display:none;
}
</style>
<script language="javascript" type="text/javascript">
//    function postBack(e) {
//        var o = e ? e.target : window.event.srcElement;
//        //var o = window.event.srcElement || o;
//        if (o.tagName == "INPUT" && o.type == "checkbox") {
//            __doPostBack("", "");
//        }
//    }
</script>

<asp:UpdatePanel ID="updPanelIntranet" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="viewContainer">
            <asp:Panel runat="server" ID="pnlIntranet" Visible="true">             
                
                <asp:TreeView runat="server" ID="trVwIntranet" ShowLines="True" ForeColor="Black" ExpandDepth="1"
                     LineImagesFolder="~/Images/tree/TreeLineImages">
                    <Nodes>
                        <asp:TreeNode PopulateOnDemand="true"  Text ="" ImageUrl ="~/Images/tree/folder.gif" ShowCheckBox="false"></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>      
                     
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>