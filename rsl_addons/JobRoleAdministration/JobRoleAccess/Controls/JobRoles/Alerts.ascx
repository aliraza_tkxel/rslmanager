﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Alerts.ascx.vb" Inherits="JobRoleAccess.Alerts" %>
<asp:UpdatePanel ID="updPanelAlerts" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </asp:Panel>
        <div class="viewContainer">
            <asp:Panel runat="server" ID="pnlAlerts" Visible="true">
                <asp:GridView ID="grdAlerts" runat="server" AutoGenerateColumns="False" Width="100%"
                    AllowSorting="True" AllowPaging="False" BorderStyle="None" GridLines="None" CellSpacing="6" 
                    >
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox  onclick="postBack(event,1)" ID="chkAlerts" runat="server">
                                </asp:CheckBox>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            <ItemStyle Width="10px" HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lblAlert" runat="server" Text='<%#Eval("AlertName") %>'></asp:Label>
                                <asp:Label ID="lblAlertId" runat="server" Text='<%#Eval("AlertId") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblIsChecked" runat="server" Text='<%#Eval("IsChecked") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                            <ItemStyle Width="120px" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle Wrap="True" />
                    <HeaderStyle BorderStyle="None" Font-Bold="True" />
                    <RowStyle BorderStyle="None" />
                </asp:GridView>
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
