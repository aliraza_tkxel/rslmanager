﻿Imports System.Data
Imports System.Data.SqlClient
Imports JRA_BusinessLogic
Imports JRA_Utilities
Imports JRA_BusinessObject
Imports System.Drawing

Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Modules
    Inherits UserControlBase

#Region "Events"

#Region "Page Load event"
    ''' <summary>
    ''' Page Load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If (Not IsPostBack) Then
                trVwAccess.ExpandDepth = 1
                trVwAccess.Nodes(0).Expanded = True
                'Dim id As Int32 = Convert.ToInt32(Me.Parent.FindControl("count"))
                'If Convert.ToInt32((Me.Parent.FindControl("count").ToString())) < 1 Then
                trVwAccess.Attributes.Add("onclick", "postBack(event,0)")
                'End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "TreeNode Check Changed"
    ''' <summary>
    ''' TreeNode Check Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub trVwAccess_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwAccess.TreeNodeCheckChanged
        Me.Parent.FindControl("pnlSaveControls").Visible = True

        Dim hdnCheckModule As HiddenField = CType(Me.Parent.FindControl("hdnCheckModule"), HiddenField)
        hdnCheckModule.Value = ApplicationConstants.PanelDataChanged

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Save Modules Access Rights"
    ''' <summary>
    ''' Save Modules Access Rights
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveModulesAccessRights()

        Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()

        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()

        Dim moduleDt As DataTable = New DataTable()
        moduleDt.Columns.Add(ApplicationConstants.jobRoleTeamIdCol, GetType(System.Int32))
        moduleDt.Columns.Add(ApplicationConstants.ModuleIdCol, GetType(System.Int32))

        Dim menuDt As DataTable = New DataTable()
        menuDt.Columns.Add(ApplicationConstants.jobRoleTeamIdCol, GetType(System.Int32))
        menuDt.Columns.Add(ApplicationConstants.MenuIdCol, GetType(System.Int32))

        Dim pageDt As DataTable = New DataTable()
        pageDt.Columns.Add(ApplicationConstants.jobRoleTeamIdCol, GetType(System.Int32))
        pageDt.Columns.Add(ApplicationConstants.PageIdCol, GetType(System.Int32))


        'objJobRoleBl.resetTeamAccessRights(teamJobRoleId)

        Dim parentNode As TreeNode

        For Each parentNode In trVwAccess.Nodes
            If parentNode.ChildNodes.Count > 0 Then

                Dim modulesNode As TreeNode
                For Each modulesNode In parentNode.ChildNodes

                    Dim moduleId As Integer = modulesNode.Value
                    If (modulesNode.Checked = True) Then
                        moduleDt.Rows.Add(teamJobRoleId, moduleId)
                    End If

                    Dim menuNode As TreeNode
                    For Each menuNode In modulesNode.ChildNodes

                        Dim menuId As Integer = menuNode.Value
                        If (menuNode.Checked = True) Then
                            menuDt.Rows.Add(teamJobRoleId, menuId)
                        End If

                        If menuNode.ChildNodes.Count > 0 Then
                            Dim pageNodeLevelOne As TreeNode
                            For Each pageNodeLevelOne In menuNode.ChildNodes
                                If (pageNodeLevelOne.Checked = True) Then
                                    Dim pageId As Integer = Convert.ToInt32(pageNodeLevelOne.Value)
                                    pageDt.Rows.Add(teamJobRoleId, pageId)
                                End If

                                If (pageNodeLevelOne.ChildNodes.Count > 0) Then
                                    Dim pageNodeLevelTwo As TreeNode
                                    For Each pageNodeLevelTwo In pageNodeLevelOne.ChildNodes
                                        If (pageNodeLevelTwo.Checked = True) Then
                                            Dim pageId As Integer = Convert.ToInt32(pageNodeLevelTwo.Value)
                                            pageDt.Rows.Add(teamJobRoleId, pageId)
                                        End If

                                        If (pageNodeLevelTwo.ChildNodes.Count > 0) Then
                                            Dim pageNodeLevelThree As TreeNode
                                            For Each pageNodeLevelThree In pageNodeLevelTwo.ChildNodes
                                                If (pageNodeLevelThree.Checked = True) Then
                                                    Dim pageId As Integer = Convert.ToInt32(pageNodeLevelThree.Value)
                                                    pageDt.Rows.Add(teamJobRoleId, pageId)
                                                End If
                                            Next
                                        End If

                                    Next
                                End If

                            Next
                        End If

                    Next

                Next

            End If
        Next

        objJobRoleBl.saveModulesAccessRights(moduleDt, menuDt, pageDt, teamJobRoleId, userId)

    End Sub

#End Region

#Region "Refresh modules Tree"
    ''' <summary>
    ''' Refresh modules Tree
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub refreshModulesTree()

        trVwAccess.Nodes(0).ChildNodes.Clear()
        createModules(trVwAccess.Nodes(0))

    End Sub


#End Region

#Region "Create Modules"
    ''' <summary>
    ''' Create Modules
    ''' </summary>
    ''' <param name="node"></param>
    ''' <remarks></remarks>
    Sub createModules(ByVal node As TreeNode)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()


        Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
        objJobRoleBl.getTeamModules(resultDataSet, teamJobRoleId)

        If resultDataSet.Tables(ApplicationConstants.AllModulesDt).Rows.Count > 0 Then
            If resultDataSet.Tables(ApplicationConstants.TeamModulesDt).Rows.Count > 0 Then
                For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.AllModulesDt).Rows

                    Dim newNode As TreeNode = New TreeNode()
                    'newNode.PopulateOnDemand = True
                    newNode.Text = row(ApplicationConstants.ModuleNameCol)
                    newNode.Value = row(ApplicationConstants.ModuleIdCol)
                    Dim moduleId As Integer = newNode.Value

                    Dim modulesQuery = (From dataRow In resultDataSet.Tables(ApplicationConstants.TeamModulesDt) _
                    Where _
                        dataRow.Field(Of Integer)(ApplicationConstants.ModuleIdCol) = moduleId _
                    Select dataRow).ToList()

                    If modulesQuery.Count > 0 Then
                        newNode.Checked = True
                    Else
                        newNode.Checked = False
                    End If
                    newNode.ShowCheckBox = True                    
                    newNode.ImageUrl = PathConstants.ModuleImagePath
                    node.ChildNodes.Add(newNode)

                    createMenus(newNode)


                Next
            Else
                For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.AllModulesDt).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    'newNode.PopulateOnDemand = True
                    newNode.Text = row(ApplicationConstants.ModuleNameCol)
                    newNode.Value = row(ApplicationConstants.ModuleIdCol)
                    newNode.ShowCheckBox = True
                    newNode.Checked = False
                    newNode.ImageUrl = PathConstants.ModuleImagePath
                    node.ChildNodes.Add(newNode)

                    createMenus(newNode)
                Next
            End If
        End If

    End Sub
#End Region

#Region "Create Menus"
    ''' <summary>
    ''' Create Menus
    ''' </summary>
    ''' <param name="node"></param>
    ''' <remarks></remarks>
    Sub createMenus(ByVal node As TreeNode)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()

        Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
        Dim moduleId As Integer = node.Value

        objJobRoleBl.getTeamMenus(resultDataSet, teamJobRoleId, moduleId)

        If resultDataSet.Tables(ApplicationConstants.AllMenusDt).Rows.Count > 0 Then
            If resultDataSet.Tables(ApplicationConstants.TeamMenusDt).Rows.Count > 0 Then
                For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.AllMenusDt).Rows

                    Dim newNode As TreeNode = New TreeNode()
                    'newNode.PopulateOnDemand = True
                    newNode.Text = row(ApplicationConstants.MenuNameCol)
                    newNode.Value = row(ApplicationConstants.MenuIdCol)
                    Dim menuId As Integer = newNode.Value

                    Dim menuQuery = (From dataRow In resultDataSet.Tables(ApplicationConstants.TeamMenusDt) _
                    Where _
                        dataRow.Field(Of Integer)(ApplicationConstants.MenuIdCol) = menuId _
                    Select dataRow).ToList()

                    If menuQuery.Count > 0 Then
                        newNode.Checked = True
                    Else
                        newNode.Checked = False
                    End If
                    newNode.ShowCheckBox = True
                    newNode.ImageUrl = PathConstants.MenuImagePath
                    node.ChildNodes.Add(newNode)

                    SessionManager.removePagesDs()
                    createPages(newNode, ApplicationConstants.MenuTypeId)


                Next
            Else
                For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.AllMenusDt).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    'newNode.PopulateOnDemand = True
                    newNode.Text = row(ApplicationConstants.MenuNameCol)
                    newNode.Value = row(ApplicationConstants.MenuIdCol)
                    newNode.ShowCheckBox = True
                    newNode.ImageUrl = PathConstants.MenuImagePath
                    newNode.Checked = False
                    node.ChildNodes.Add(newNode)

                    SessionManager.removePagesDs()
                    createPages(newNode, ApplicationConstants.MenuTypeId)
                Next
            End If
        End If

    End Sub

#End Region

#Region "Create Pages"
    ''' <summary>
    ''' Create Pages
    ''' </summary>
    ''' <param name="node"></param>
    ''' <remarks></remarks>
    Sub createPages(ByVal node As TreeNode, ByVal idType As Integer)

        Dim resultDatatable As DataTable = New DataTable()
        Dim id As Integer = node.Value
        Dim resultDataSet As DataSet = New DataSet()

        If (idType = ApplicationConstants.MenuTypeId) Then

            Dim objJobRoleBl As JobRoleBL = New JobRoleBL()
            Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
            objJobRoleBl.getTeamPages(resultDataSet, teamJobRoleId, id)
            SessionManager.setPagesDs(resultDataSet)
            Dim query = (From res In resultDataSet.Tables(ApplicationConstants.AllPagesDt).AsEnumerable _
                         Where res.Item(ApplicationConstants.AccessLevelCol) = ApplicationConstants.PageLevelOne _
                         Order By Len(res.Field(Of String)(ApplicationConstants.SortOrderCol)), res.Field(Of String)(ApplicationConstants.SortOrderCol) Ascending _
                         Select res)
            resultDatatable = resultDataSet.Tables(ApplicationConstants.AllPagesDt).Clone()

            If (query.Count > 0) Then
                resultDatatable = query.CopyToDataTable()
            Else
                resultDatatable = Nothing
            End If

        Else
            resultDataSet = SessionManager.getPagesDs()
            Dim query = (From res In resultDataSet.Tables(ApplicationConstants.AllPagesDt).AsEnumerable _
                         Where res.Item(ApplicationConstants.ParentPageCol) = id _
                         Order By Len(res.Field(Of String)(ApplicationConstants.SortOrderCol)), res.Field(Of String)(ApplicationConstants.SortOrderCol) Ascending _
                         Select res)
            resultDatatable = resultDataSet.Tables(ApplicationConstants.AllPagesDt).Clone()
            If (query.Count > 0) Then
                resultDatatable = query.CopyToDataTable()
            Else
                resultDatatable = Nothing
            End If

        End If


        If Not IsNothing(resultDatatable) Then
            If (resultDatatable.Rows.Count > 0) Then
                If resultDataSet.Tables(ApplicationConstants.TeamPagesDt).Rows.Count > 0 Then
                    For Each row As DataRow In resultDatatable.Rows

                        Dim newNode As TreeNode = New TreeNode()
                        'newNode.PopulateOnDemand = True
                        newNode.Text = row(ApplicationConstants.PageNameCol)
                        newNode.Value = row(ApplicationConstants.PageIdCol)
                        Dim pageId As Integer = newNode.Value

                        Dim pagesQuery = (From dataRow In resultDataSet.Tables(ApplicationConstants.TeamPagesDt) _
                                            Where dataRow.Field(Of Integer)(ApplicationConstants.PageIdCol) = pageId _
                                            Select dataRow).ToList()

                        If pagesQuery.Count > 0 Then
                            newNode.Checked = True
                        Else
                            newNode.Checked = False
                        End If
                        newNode.ImageUrl = PathConstants.PageImagePath
                        newNode.ShowCheckBox = True
                        node.ChildNodes.Add(newNode)

                        createPages(newNode, ApplicationConstants.PageTypeId)
                    Next
                Else
                    For Each row As DataRow In resultDatatable.Rows
                        Dim newNode As TreeNode = New TreeNode()
                        'newNode.PopulateOnDemand = True
                        newNode.Text = row(ApplicationConstants.PageNameCol)
                        newNode.Value = row(ApplicationConstants.PageIdCol)
                        newNode.ImageUrl = PathConstants.PageImagePath
                        newNode.ShowCheckBox = True
                        newNode.Checked = False
                        node.ChildNodes.Add(newNode)
                        createPages(newNode, ApplicationConstants.PageTypeId)
                    Next
                End If
            End If
        End If

    End Sub

#End Region

#End Region


    
End Class