﻿Imports System.Data
Imports System.Data.SqlClient
Imports JRA_BusinessLogic
Imports JRA_Utilities
Imports JRA_BusinessObject
Imports System.Drawing
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class Intranet
    Inherits UserControlBase

#Region "Events"

#Region "Page Load event"
    ''' <summary>
    ''' Page Load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If (Not IsPostBack) Then
                trVwIntranet.ExpandDepth = 1
                trVwIntranet.Nodes(0).Expanded = True
                trVwIntranet.RootNodeStyle.CssClass = ApplicationConstants.HideRootStyle
                'If Convert.ToInt32((Me.Parent.FindControl("count").ToString())) < 1 Then
                trVwIntranet.Attributes.Add("onclick", "postBack(event,2)")
                'End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region "Tree Node Check Changed"
    ''' <summary>
    ''' Tree Node Check Changed
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub trVwIntranet_TreeNodeCheckChanged(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.TreeNodeEventArgs) Handles trVwIntranet.TreeNodeCheckChanged
        Me.Parent.FindControl("pnlSaveControls").Visible = True

        Dim hdnCheckIntranet As HiddenField = CType(Me.Parent.FindControl("hdnCheckIntranet"), HiddenField)
        hdnCheckIntranet.Value = ApplicationConstants.PanelDataChanged

        ScriptManager.RegisterStartupScript(Me, GetType(String), "AKey", "$('#intranetTab').click();", True)

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Save Intranet Access Rights"
    ''' <summary>
    ''' Save Intranet Access Rights
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveIntranetAccessRights()

        Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()

        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()

        Dim pageDt As DataTable = New DataTable()
        pageDt.Columns.Add(ApplicationConstants.jobRoleTeamIdCol, GetType(System.Int32))
        pageDt.Columns.Add(ApplicationConstants.PageIdCol, GetType(System.Int32))

        For Each pageNode In trVwIntranet.CheckedNodes()

            Dim pageId As Integer = Convert.ToInt32(pageNode.Value)
            pageDt.Rows.Add(teamJobRoleId, pageId)

        Next

        objJobRoleBl.saveIntranetPageAccessRights(pageDt, teamJobRoleId, userId)


    End Sub

#End Region

#Region "Refresh Intranet Tree"
    ''' <summary>
    ''' Refresh Intranet Tree
    ''' </summary>
    ''' <remarks></remarks>
    Sub refreshIntranetTree()

        trVwIntranet.Nodes(0).ChildNodes.Clear()
        createMainMenuNodes(trVwIntranet.Nodes(0))

    End Sub

#End Region

#Region "Create Main Menu Nodes"
    ''' <summary>
    ''' Create Main Menu Nodes
    ''' </summary>
    ''' <remarks></remarks>
    Sub createMainMenuNodes(ByVal node As TreeNode)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()

        Try
            Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
            objJobRoleBl.getIntranetMainMenus(resultDataSet, teamJobRoleId)

            If resultDataSet.Tables(ApplicationConstants.IntranetMenusDt).Rows.Count > 0 Then

                For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.IntranetMenusDt).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.Text = row(ApplicationConstants.MenuNameCol)
                    newNode.Value = row(ApplicationConstants.MenuIdCol)
                    newNode.ImageUrl = PathConstants.FolderImagePath
                    node.ChildNodes.Add(newNode)

                    createSubMenuNodes(newNode)
                Next

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Create SubMenu Nodes"
    ''' <summary>
    ''' Create SubMenu Nodes
    ''' </summary>
    ''' <remarks></remarks>
    Sub createSubMenuNodes(ByVal node As TreeNode)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()

        Try
            Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
            Dim mainmenuId As Integer = node.Value

            objJobRoleBl.getIntranetSubMenus(resultDataSet, teamJobRoleId, mainmenuId)

            If resultDataSet.Tables(ApplicationConstants.IntranetSubMenusDt).Rows.Count > 0 Then

                For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.IntranetSubMenusDt).Rows
                    Dim newNode As TreeNode = New TreeNode()
                    newNode.Text = row(ApplicationConstants.SubMenuNameCol)
                    newNode.Value = row(ApplicationConstants.SubMenuIdCol)
                    newNode.ImageUrl = PathConstants.FolderImagePath
                    node.ChildNodes.Add(newNode)


                    createSubMenuNodes(newNode)
                    createPageNodes(newNode)

                Next
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Create Page Nodes"
    ''' <summary>
    ''' Create Page Nodes
    ''' </summary>
    ''' <remarks></remarks>
    Sub createPageNodes(ByVal node As TreeNode)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobRoleBl As JobRoleBL = New JobRoleBL()

        Try
            Dim teamJobRoleId As Integer = SessionManager.getTeamJobRoleId()
            Dim submenuId As Integer = node.Value
            objJobRoleBl.getIntranetPages(resultDataSet, teamJobRoleId, submenuId)

            If resultDataSet.Tables(ApplicationConstants.IntranetPagesDt).Rows.Count > 0 Then
                If resultDataSet.Tables(ApplicationConstants.IntranetTeamPagesDt).Rows.Count > 0 Then
                    For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.IntranetPagesDt).Rows

                        Dim newNode As TreeNode = New TreeNode()
                        'newNode.PopulateOnDemand = True
                        newNode.Text = row(ApplicationConstants.PageNameCol)
                        newNode.Value = row(ApplicationConstants.PageIdCol)
                        Dim pageId As Integer = newNode.Value

                        Dim pagesQuery = (From dataRow In resultDataSet.Tables(ApplicationConstants.IntranetTeamPagesDt) _
                        Where _
                            dataRow.Field(Of Integer)(ApplicationConstants.PageIdCol) = pageId _
                        Select dataRow).ToList()

                        If pagesQuery.Count > 0 Then
                            newNode.Checked = True
                        Else
                            newNode.Checked = False
                        End If
                        newNode.ImageUrl = PathConstants.PageImagePath
                        newNode.ShowCheckBox = True
                        node.ChildNodes.Add(newNode)
                    Next
                Else
                    For Each row As DataRow In resultDataSet.Tables(ApplicationConstants.IntranetPagesDt).Rows
                        Dim newNode As TreeNode = New TreeNode()
                        'newNode.PopulateOnDemand = True
                        newNode.Text = row(ApplicationConstants.PageNameCol)
                        newNode.Value = row(ApplicationConstants.PageIdCol)
                        newNode.ImageUrl = PathConstants.PageImagePath
                        newNode.ShowCheckBox = True
                        newNode.Checked = False
                        node.ChildNodes.Add(newNode)
                    Next
                End If
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

End Class