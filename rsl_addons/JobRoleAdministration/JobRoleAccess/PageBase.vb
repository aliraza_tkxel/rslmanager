﻿Imports JRA_Utilities
Imports JRA_BusinessLogic
Imports System.Web
Public Class PageBase
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

    Public pathsData As New Dictionary(Of String, String)

    Sub New()

    End Sub


#Region "Is Session Exist"
    ''' <summary>
    ''' Checks if session is free.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub isSessionExist()

        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()
        If (userId = 0) Then
            Response.Redirect(PathConstants.BridgePath)
        ElseIf (Not checkPageAccess()) Then
            Response.Redirect(PathConstants.AccessDeniedPath)
        End If

    End Sub
#End Region

#Region "Check Page Access"
    ''' <summary>
    ''' Check Page Access
    ''' </summary>
    ''' <remarks></remarks>
    Function checkPageAccess()

        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()
        Dim resultDataset As DataSet = New DataSet
        Dim varPageUrl = Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1)

        Dim isAccessAllowed As Boolean = False
        Dim pageName As String = String.Empty

        If varPageUrl = PathConstants.JobRoleHighlight Then
            pageName = ApplicationConstants.JobRolePageName
        ElseIf varPageUrl = PathConstants.JobRoleAuditReportHighlight Then
            pageName = ApplicationConstants.JobRoleReportPageName
        End If

        Dim objRSLModulesBL As New MasterPageBL
        objRSLModulesBL.checkEmployeePageAccess(resultDataset, userId, pageName)

        If (resultDataset.Tables(0).Rows.Count() > 0) Then
            isAccessAllowed = True
        End If

        Return isAccessAllowed

    End Function

#End Region

#Region "Session Time out"
    ''' <summary>
    ''' This event handles the page_init event of the base page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
    End Sub
#End Region

#Region "OnInit"
    ''' <summary>
    ''' This function is used to destroy browser cache. so that every time page load 'll be called
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
    End Sub

#End Region

#Region "High Light Menu Items"
    ''' <summary>
    ''' 'This function 'll highlight the menu items and set the visibilty of panels
    ''' </summary>
    ''' <param name="controlList">contains the list of control name that should be highlighted or visible</param>    
    ''' <remarks></remarks>
    Public Sub highLightMenuItems(ByRef controlList As List(Of Tuple(Of String, String)))

        For i As Integer = 0 To controlList.Count - 1
            If controlList.Item(i).Item1 = ApplicationConstants.LinkButtonType Then
                Dim lnkBtn As LinkButton = CType(Master.FindControl(controlList.Item(i).Item2), LinkButton)
                lnkBtn.Style.Add(ApplicationConstants.ConWebBackgroundProperty, ApplicationConstants.ConWebBackgroundColor)

            ElseIf controlList.Item(i).Item1 = ApplicationConstants.UpdatePanelType Then
                Dim updPanel As UpdatePanel = CType(Master.FindControl(controlList.Item(i).Item2), UpdatePanel)
                updPanel.Visible = True
            End If
        Next
    End Sub
#End Region

End Class
