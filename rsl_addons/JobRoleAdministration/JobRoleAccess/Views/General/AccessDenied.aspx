﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/JobRoleAccess.Master"
    CodeBehind="AccessDenied.aspx.vb" Inherits="JobRoleAccess.AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        img
        {
            position: absolute;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div >
        <asp:Image ID="imgAccessDenied" ImageUrl="~/Images/access_denied.png" runat="server" />
    </div>
</asp:Content>
