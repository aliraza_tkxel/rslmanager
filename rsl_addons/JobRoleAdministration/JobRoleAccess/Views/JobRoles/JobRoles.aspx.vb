﻿Imports JRA_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports JRA_BusinessLogic
Imports JRA_BusinessObject
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Threading
Imports System.Globalization


Public Class JobRoles
    Inherits PageBase


#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobRoleName", 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

                Me.populateSearchTeamsDropdownList()
                Me.populateSaveTeamsDropdownList()
                Me.populateJobRoleList()

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Users Count link button click"
    ''' <summary>
    ''' Users Count link button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnUsersCount_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkBtnUserCount As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnUserCount.NamingContainer, GridViewRow)

            Dim hdnTeamJobRoleId As HiddenField = DirectCast(row.FindControl("hdnTeamJobRoleId"), HiddenField)
            Dim lblJobRoleName As Label = DirectCast(row.FindControl("lblJobRoleName"), Label)
            Dim lblTeamName As Label = DirectCast(row.FindControl("lblTeamName"), Label)

            Dim jobRoleName As String = lblJobRoleName.Text
            Dim jobTeamName As String = lblTeamName.Text
            Dim teamJobRoleId As Integer = Convert.ToInt32(hdnTeamJobRoleId.Value)

            teamUsersPopup(jobRoleName, jobTeamName, teamJobRoleId)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Delete button click"
    ''' <summary>
    ''' Delete button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkbtnDeleteJobRole_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkBtnDelete As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnDelete.NamingContainer, GridViewRow)

            Dim hdnTeamJobRoleId As HiddenField = DirectCast(row.FindControl("hdnTeamJobRoleId"), HiddenField)
            Dim teamJobRoleId As Integer = Convert.ToInt32(hdnTeamJobRoleId.Value)
            hdnJobRoleTeamId.Value = teamJobRoleId
            mdlPopupJobRoleDeletion.Show()



        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Export to XLS button click"
    ''' <summary>
    ''' Export to XLS button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnToXls_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToXls.Click
        Try
            ExportGridToExcel(Me.getFullJobRoleGridView())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Print button click"
    ''' <summary>
    ''' Export button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        Try
            Me.printGridView(Me.getFullJobRoleGridView())
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Add button click event"
    ''' <summary>
    ''' Add button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddNewJobRole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddNewJobRole.Click
        Try
            Me.populateSaveTeamsDropdownList()
            txtSaveJobRole.Text = String.Empty
            Me.hideStatus()
            hdnUpdateSaveCheck.Value = ApplicationConstants.SaveValue
            pnlSaveJobRole.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try

    End Sub
#End Region

#Region "Delete button click event"
    ''' <summary>
    ''' Delete button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDeleteOk_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDeleteOk.Click
        Try
            Dim teamJobRoleId As Integer = Convert.ToInt32(hdnJobRoleTeamId.Value)
            deleteJobRole(teamJobRoleId)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try

    End Sub
#End Region

#Region "Cancel button click event"
    ''' <summary>
    ''' Cancel button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancelNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelNew.Click
        Try

            pnlSaveJobRole.Visible = False

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try

    End Sub
#End Region

#Region "Button Search Click Event"
    ''' <summary>
    ''' Button Search Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Try
            Me.populateJobRoleList()
            pnlSaveJobRole.Visible = False
            pnlTabs.Visible = False

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try
    End Sub

#End Region

#Region "Button Save Click Event"
    ''' <summary>
    ''' Button Save Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveJobrole_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveJobrole.Click
        Try

            If (hdnUpdateSaveCheck.Value.Equals(ApplicationConstants.SaveValue)) Then
                Me.saveJobRole()
            Else
                Me.updateJobRole()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try
    End Sub

#End Region

#Region "Button Save Access Rights Click Event"
    ''' <summary>
    ''' Button Save Access Rights Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveAccessRights_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAccessRights.Click
        Try

            updPanelJobRoles.Update()

            If (hdnCheckModule.Value.Equals(ApplicationConstants.PanelDataChanged)) Then
                ModulesControl.saveModulesAccessRights()
                hdnCheckModule.Value = ApplicationConstants.PanelDataDefault
            End If

            If (hdnCheckAlert.Value.Equals(ApplicationConstants.PanelDataChanged)) Then
                AlertsControl.saveTeamAlertsAccessRights()
                hdnCheckAlert.Value = ApplicationConstants.PanelDataDefault
            End If

            If (hdnCheckIntranet.Value.Equals(ApplicationConstants.PanelDataChanged)) Then
                IntranetControl.saveIntranetAccessRights()
                hdnCheckIntranet.Value = ApplicationConstants.PanelDataDefault
            End If

            Me.showMessagePopup(UserMessageConstants.SuccessStatus, UserMessageConstants.AccessRightsSavedSuccessfully)
            Me.restoreCurrentTab()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try
    End Sub

#End Region

#Region "Button Cancel Access Rights Click Event"
    ''' <summary>
    ''' Button Cancel Access Rights Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Try
            pnlTabs.Visible = False

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try
    End Sub

#End Region

#Region "Gridview sorting"
    ''' <summary>
    ''' Gridview sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdJobRolesList_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdJobRolesList.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobRoleName", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdJobRolesList.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            Me.populateJobRoleList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Grid Job Role List row data bound"
    ''' <summary>
    ''' Grid Job Role List row data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdJobRolesList_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdJobRolesList.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                e.Row.BackColor = Drawing.Color.White
                Dim row As GridViewRow = e.Row

                Dim lblcount As Label = DirectCast(row.FindControl("lblUsersCount"), Label)
                Dim lnkBtnCount As LinkButton = DirectCast(row.FindControl("lnkbtnUsersCount"), LinkButton)
                Dim lnkBtnDel As LinkButton = DirectCast(row.FindControl("lnkBtnDelete"), LinkButton)

                Dim count As Integer = Convert.ToInt32(lblcount.Text)

                If (count > 0) Then
                    lnkBtnCount.Visible = True
                    lblcount.Visible = False
                    lnkBtnDel.Visible = False
                Else
                    lnkBtnCount.Visible = False
                    lblcount.Visible = True
                    lnkBtnDel.Visible = True
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Edit Job Role"
    ''' <summary>
    ''' Edit Job Role
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lnkBtnEditJobRole_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Me.populateSaveTeamsDropdownList()

            Me.resetHiddenFields()
            pnlSaveControls.Visible = False

            Dim lnkBtnEdit As LinkButton = DirectCast(sender, LinkButton)
            Dim row As GridViewRow = DirectCast(lnkBtnEdit.NamingContainer, GridViewRow)

            Dim lblJobRoleName As Label = DirectCast(row.FindControl("lblJobRoleName"), Label)
            Dim lblTeamName As Label = DirectCast(row.FindControl("lblTeamName"), Label)

            Dim hdnJobRoleId As HiddenField = DirectCast(row.FindControl("hdnJobRoleId"), HiddenField)
            Dim hdnTeamId As HiddenField = DirectCast(row.FindControl("hdnTeamId"), HiddenField)
            Dim hdnTeamJobRoleId As HiddenField = DirectCast(row.FindControl("hdnTeamJobRoleId"), HiddenField)
            Dim hdnStatus As HiddenField = DirectCast(row.FindControl("hdnStatus"), HiddenField)

            Dim teamJobRoleId As Integer = Convert.ToInt32(hdnTeamJobRoleId.Value)
            Dim teamId As Integer = Convert.ToInt32(hdnTeamId.Value)
            Dim jobRoleId As Integer = Convert.ToInt32(hdnJobRoleId.Value)
            Dim status As Boolean = Convert.ToBoolean(hdnStatus.Value)

            Dim objTeamJobRoleBo As TeamJobRoleBO = New TeamJobRoleBO()
            objTeamJobRoleBo.TeamJobRoleId = teamJobRoleId
            objTeamJobRoleBo.TeamId = teamId
            objTeamJobRoleBo.JobRoleId = jobRoleId
            objTeamJobRoleBo.IsActive = status
            objTeamJobRoleBo.JobRole = lblJobRoleName.Text

            SessionManager.setTeamJobRoleId(teamJobRoleId)
            SessionManager.setTeamJobRoleBO(objTeamJobRoleBo)

            Me.populateUpdateTeamJobRolePanel()
            Me.refreshTabsPanel()

            pnlTabs.Visible = True

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try

    End Sub
#End Region

#Region "Hidden save button click event"
    ''' <summary>
    ''' Hidden save button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHiddenSavePanel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHiddenSavePanel.Click
        Try
            pnlSaveControls.Visible = True
            restoreCurrentTab()
           
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Restore current tab"
    ''' <summary>
    ''' Restore current tab
    ''' </summary>
    ''' <remarks></remarks>
    Sub restoreCurrentTab()
        Dim tab As Integer = Convert.ToInt32(Request.Params.Get("__EVENTARGUMENT"))
        If (tab = ApplicationConstants.ModuleTab) Then
            ScriptManager.RegisterStartupScript(Me, GetType(String), "AKey", "$('#moduleTab').click();", True)
        ElseIf tab = ApplicationConstants.AlertTab Then
            ScriptManager.RegisterStartupScript(Me, GetType(String), "AKey", "$('#alertTab').click();", True)
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(String), "AKey", "$('#intranetTab').click();", True)
        End If
    End Sub

#End Region

#Region "Reset hidden fields of Modules/Alerts/Intranet Tab"
    ''' <summary>
    ''' Reset hidden fields of Modules/Alerts/Intranet Tab
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetHiddenFields()

        hdnCheckModule.Value = ApplicationConstants.PanelDataDefault
        hdnCheckAlert.Value = ApplicationConstants.PanelDataDefault
        hdnCheckIntranet.Value = ApplicationConstants.PanelDataDefault

    End Sub

#End Region

#Region "Show user message popup"
    ''' <summary>
    ''' Show user message popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showMessagePopup(ByVal status As String, ByVal message As String)

        lblMessageStatus.Text = status
        lblUserMessage.Text = message
        mdlPopupMessage.Show()

    End Sub

#End Region

#Region "Refresh tabs panel"
    ''' <summary>
    ''' Refresh tabs panel
    ''' </summary>
    ''' <remarks></remarks>
    Sub refreshTabsPanel()
        'Refresh Modules Tab
        ModulesControl.refreshModulesTree()

        'Refresh Alerts Tab
        AlertsControl.populateAlertGrid()

        'Refresh Intranet Tab
        IntranetControl.refreshIntranetTree()

    End Sub

#End Region

#Region "Populate Update Team Job Role Panel"
    ''' <summary>
    ''' Populate Update Team Job Role Panel
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateUpdateTeamJobRolePanel()

        Dim objTeamJobRoleBo As TeamJobRoleBO = New TeamJobRoleBO()
        objTeamJobRoleBo = SessionManager.getTeamJobRoleBO()

        ddlSaveTeam.SelectedValue = Convert.ToString(objTeamJobRoleBo.TeamId)
        txtSaveJobRole.Text = objTeamJobRoleBo.JobRole
        hdnUpdateSaveCheck.Value = ApplicationConstants.UpdateValue

        If (objTeamJobRoleBo.IsActive) Then
            rbtnJobRoleActiveYes.Checked = True
            rbtnJobRoleActiveNo.Checked = False
        Else
            rbtnJobRoleActiveYes.Checked = False
            rbtnJobRoleActiveNo.Checked = True
        End If

        pnlSaveJobRole.Visible = True
        Me.showStatus()

    End Sub

#End Region

#Region "Show status"
    ''' <summary>
    ''' Show status
    ''' </summary>
    ''' <remarks></remarks>
    Sub showStatus()
        lblActive.Visible = True
        rbtnJobRoleActiveNo.Visible = True
        rbtnJobRoleActiveYes.Visible = True
    End Sub

#End Region

#Region "Hide status"
    ''' <summary>
    ''' Hide status
    ''' </summary>
    ''' <remarks></remarks>
    Sub hideStatus()
        lblActive.Visible = False
        rbtnJobRoleActiveNo.Visible = False
        rbtnJobRoleActiveYes.Visible = False
    End Sub

#End Region

#Region "Delete JobRole"
    ''' <summary>
    ''' Delete JobRole
    ''' </summary>
    ''' <remarks></remarks>
    Sub deleteJobRole(ByVal teamJobroleId As Integer)

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()

        If (objJobRoleBL.deleteTeamJobRole(teamJobroleId)) Then
            Me.populateJobRoleList()
            showMessagePopup(UserMessageConstants.SuccessStatus, UserMessageConstants.JobroleSuccessfullyDeleted)

            pnlSaveJobRole.Visible = False
            pnlTabs.Visible = False
        Else
            showMessagePopup(UserMessageConstants.FailureStatus, UserMessageConstants.FailedDeleteJobrole)

        End If


    End Sub
#End Region

#Region "Show team users popup"
    ''' <summary>
    ''' Show team users popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub teamUsersPopup(ByVal jobRoleName As String, ByVal teamName As String, ByVal teamJobRoleId As Integer)

        Me.resetPopup()

        lblPopJobrole.Text = jobRoleName
        lblPopTeam.Text = teamName

        Dim resultDataset As DataSet = New DataSet()
        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        objJobRoleBL.getTeamEmployees(resultDataset, teamJobRoleId)

        grdEmployees.DataSource = resultDataset.Tables(0)
        grdEmployees.DataBind()

        mdlPopupJobRole.Show()

    End Sub

#End Region

#Region "Reset popup"
    ''' <summary>
    ''' Reset popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetPopup()

        lblPopJobrole.Text = String.Empty
        lblPopTeam.Text = String.Empty

    End Sub

#End Region

#Region "Populate search teams"
    ''' <summary>
    ''' Populate search teams
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSearchTeamsDropdownList()
        Dim resultDataSet As DataSet = New DataSet()

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        objJobRoleBL.getAllTeams(resultDataSet)

        ddlSearchTeam.DataSource = resultDataSet.Tables(0).DefaultView
        ddlSearchTeam.DataValueField = "TeamId"
        ddlSearchTeam.DataTextField = "TeamName"
        ddlSearchTeam.DataBind()

        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlSearchTeam.Items.Insert(0, item)
    End Sub

#End Region

#Region "Populate save teams"
    ''' <summary>
    ''' Populate save teams
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateSaveTeamsDropdownList()
        Dim resultDataSet As DataSet = New DataSet()

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        objJobRoleBL.getAllTeams(resultDataSet)

        ddlSaveTeam.DataSource = resultDataSet.Tables(0).DefaultView
        ddlSaveTeam.DataValueField = "TeamId"
        ddlSaveTeam.DataTextField = "TeamName"
        ddlSaveTeam.DataBind()

        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlSaveTeam.Items.Insert(0, item)
    End Sub

#End Region

#Region "Add new job role"
    ''' <summary>
    ''' Add new job role
    ''' </summary>
    ''' <remarks></remarks>
    Sub addNewJobRole()
        Me.populateSaveTeamsDropdownList()
        pnlSaveControls.Visible = True
    End Sub

#End Region

#Region "Save job role"
    ''' <summary>
    ''' Save job role
    ''' </summary>
    ''' <remarks></remarks>
    Sub saveJobRole()

        Dim objteamJobRoleBO As TeamJobRoleBO = New TeamJobRoleBO()
        objteamJobRoleBO.TeamId = ddlSaveTeam.SelectedValue
        objteamJobRoleBO.JobRole = txtSaveJobRole.Text
        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()

        Dim results As ValidationResults = Validation.Validate(objteamJobRoleBO)
        If results.IsValid Then

            If (objJobRoleBL.checkTeamJobRoleExist(objteamJobRoleBO)) Then
                showMessagePopup(UserMessageConstants.ErrorStatus, UserMessageConstants.JobroleAlreadyExist)

            Else

                If (objJobRoleBL.saveTeamJobRole(objteamJobRoleBO, userId)) Then
                    Me.populateJobRoleList()
                    pnlTabs.Visible = False
                    pnlSaveJobRole.Visible = False
                    showMessagePopup(UserMessageConstants.SuccessStatus, UserMessageConstants.JobroleSuccessfullyAdded)

                Else
                    showMessagePopup(UserMessageConstants.FailureStatus, UserMessageConstants.FailedSaveJobrole)

                End If

            End If


        Else
            Dim message = String.Empty
            For Each result As ValidationResult In results
                message += result.Message
                Exit For
            Next
            showMessagePopup(UserMessageConstants.ErrorStatus, message)

        End If

    End Sub

#End Region

#Region "Update job role"
    ''' <summary>
    ''' Update job role
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateJobRole()

        Dim objteamJobRoleBO As TeamJobRoleBO = New TeamJobRoleBO()
        objteamJobRoleBO = SessionManager.getTeamJobRoleBO()
        objteamJobRoleBO.TeamId = ddlSaveTeam.SelectedValue
        objteamJobRoleBO.JobRole = txtSaveJobRole.Text
        objteamJobRoleBO.IsActive = IIf(rbtnJobRoleActiveYes.Checked, True, False)
        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        Dim userId As Integer = SessionManager.getJobRoleAccessUserId()

        Dim results As ValidationResults = Validation.Validate(objteamJobRoleBO)
        If results.IsValid Then

            'If (objJobRoleBL.checkTeamJobRoleExist(objteamJobRoleBO)) Then
            '    showMessagePopup(UserMessageConstants.ErrorStatus, UserMessageConstants.JobroleAlreadyExist)

            'Else

            If (objJobRoleBL.updateTeamJobRole(objteamJobRoleBO, userId)) Then
                Me.populateJobRoleList()
                pnlTabs.Visible = False
                showMessagePopup(UserMessageConstants.SuccessStatus, UserMessageConstants.JobroleSuccessfullyUpdate)

            Else
                showMessagePopup(UserMessageConstants.FailureStatus, UserMessageConstants.FailedUpdateJobrole)

            End If

            'End If

        Else
            Dim message = String.Empty
            For Each result As ValidationResult In results
                message += result.Message
                Exit For
            Next
            showMessagePopup(UserMessageConstants.FailureStatus, message)

        End If

    End Sub

#End Region

#Region "Populate Job Role List"
    ''' <summary>
    ''' Populate Job Role List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateJobRoleList()

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        Dim resultDataset As DataSet = New DataSet()
        Dim totalCount As Integer

        Dim teamId As Integer = ddlSearchTeam.SelectedValue
        Dim searchText As String = txtSearchJobrole.Text

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobRoleName", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        totalCount = objJobRoleBL.searchTeamJobRole(resultDataset, objPageSortBo, searchText, teamId)
        grdJobRolesList.DataSource = resultDataset.Tables(0)
        grdJobRolesList.DataBind()


        If (resultDataset.Tables(0).Rows.Count() > 0) Then
            pnlPagination.Visible = True
            btnPrint.Enabled = True
            btnToXls.Enabled = True
        Else
            pnlPagination.Visible = False
            btnPrint.Enabled = False
            btnToXls.Enabled = False
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobRoleName", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            Me.populateJobRoleList()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)

            End If
        End Try

    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobRoleName", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)

                Me.populateJobRoleList()

            Else
                showMessagePopup(UserMessageConstants.ErrorStatus, UserMessageConstants.InvalidPageNumber)

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)

            End If
        End Try
    End Sub

#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "JobRoleName", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Get full JobRole GridView"
    ''' <summary>
    ''' Get full JobRole GridView
    ''' </summary>
    ''' <remarks></remarks>
    Function getFullJobRoleGridView()
        Dim grdFullJobRoleList As New GridView()
        grdFullJobRoleList.AllowPaging = False
        grdFullJobRoleList.AutoGenerateColumns = False

        Dim jobRoleField As BoundField = New BoundField()
        jobRoleField.HeaderText = "Job Role"
        jobRoleField.DataField = "JobRoleName"
        grdFullJobRoleList.Columns.Add(jobRoleField)

        Dim teamField As BoundField = New BoundField()
        teamField.HeaderText = "Team"
        teamField.DataField = "TeamName"
        grdFullJobRoleList.Columns.Add(teamField)

        Dim usersField As BoundField = New BoundField()
        usersField.HeaderText = "Users"
        usersField.DataField = "UsersCount"
        grdFullJobRoleList.Columns.Add(usersField)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        objJobRoleBL.searchFullTeamJobRole(resultDataSet, txtSearchJobrole.Text, ddlSearchTeam.SelectedValue)
        grdFullJobRoleList.DataSource = resultDataSet.Tables(0)
        grdFullJobRoleList.DataBind()

        Return grdFullJobRoleList

    End Function
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "JobRoleAdministration_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.BufferOutput = True
        Response.Flush()
        Response.Close()

    End Sub

#End Region

#Region "Print Gridview"
    ''' <summary>
    ''' Print Gridview
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub printGridView(ByVal grdView As GridView)

        Dim fileName As String = "JobRoleAdministration_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        grdView.RenderControl(hw)
        Dim gridHTML As String = sw.ToString().Replace("""", "'") _
             .Replace(System.Environment.NewLine, "")
        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(gridHTML)
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), fileName, sb.ToString())
    End Sub
#End Region

#End Region

End Class