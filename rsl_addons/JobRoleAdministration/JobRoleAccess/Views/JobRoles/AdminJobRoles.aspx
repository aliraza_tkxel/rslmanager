﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/HR.Master"
    CodeBehind="AdminJobRoles.aspx.vb" Inherits="JobRoleAccess.AdminJobRoles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc1" TagName="Modules" Src="~/Controls/JobRoles/Modules.ascx" %>
<%@ Register TagPrefix="uc2" TagName="Alerts" Src="~/Controls/JobRoles/Alerts.ascx" %>
<%@ Register TagPrefix="uc3" TagName="Intranet" Src="~/Controls/JobRoles/Intranet.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/JobRole.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <style type="text/css">
        .style1
        {
            width: 152px;
        }
        .message
        {
            padding-left: 12px;
        }
        .topMargin
        {
            margin-top: -15px;
        }
    </style>
    <script type="text/javascript" language="javascript">

        function showModules() {

            // TABS CSS CLASS
            document.getElementById("moduleTab").className = "tab-active aleft";
            document.getElementById("alertTab").className = "tab aleft";
            document.getElementById("intranetTab").className = "tab aleft";

            // DIVS CSS CLASS
            document.getElementById("moduleData").className = "showDiv";
            document.getElementById("alertData").className = "hideDiv";
            document.getElementById("intranetData").className = "hideDiv";

        }

        function showAlerts() {

            // TABS CSS CLASS
            document.getElementById("moduleTab").className = "tab aleft";
            document.getElementById("alertTab").className = "tab-active aleft";
            document.getElementById("intranetTab").className = "tab aleft";

            // DIVS CSS CLASS
            document.getElementById("moduleData").className = "hideDiv";
            document.getElementById("alertData").className = "showDiv";
            document.getElementById("intranetData").className = "hideDiv";

        }

        function showIntranet() {

            // TABS CSS CLASS
            document.getElementById("moduleTab").className = "tab aleft";
            document.getElementById("alertTab").className = "tab aleft";
            document.getElementById("intranetTab").className = "tab-active aleft";

            // DIVS CSS CLASS
            document.getElementById("moduleData").className = "hideDiv";
            document.getElementById("alertData").className = "hideDiv";
            document.getElementById("intranetData").className = "showDiv";
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);

            // Place here the first init of the BindCheckBoxes
            BindCheckBoxes();
        });

        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the BindCheckBoxes
            BindCheckBoxes();
        }

        function BindCheckBoxes() {
            $(function () {
                $("#trVwAccess input[type=checkbox]").bind("change", function () {
                    var table = $(this).closest("table");
                    if (table.next().length > 0 && table.next()[0].tagName == "DIV") {
                        //Is Parent CheckBox
                        var childDiv = table.next();
                        var isChecked = $(this).is(":checked");
                        $("input[type=checkbox]", childDiv).each(function () {
                            if (isChecked) {
                                $(this).attr("checked", "checked");
                            } else {
                                $(this).removeAttr("checked");
                            }
                        });
                    }
                });
            });
        }

        function getActiveTab() {

            var moduleClass = document.getElementById("moduleTab").className;
            var alertClass = document.getElementById("alertTab").className;
            var intranetClass = document.getElementById("intranetTab").className;

        if (moduleClass == "tab-active aleft"){
            return 0;
        }
        else if (alertClass == "tab-active aleft"){
            return 1;
        }
        else {
            return 2;
         }
        
        }

        function clickSaveAccessRights() {
        var tabIndex = getActiveTab()

        __doPostBack("<%=btnSaveAccessRights.UniqueID %>", tabIndex);
           
        }

        function postBack(e, tabIndex) {
            var o = e ? e.target : window.event.srcElement;

            if (o.tagName == "INPUT" && o.type == "checkbox") {

                if (tabIndex == 0) {
                    var hdnCheckModule = document.getElementById('<%= hdnCheckModule.ClientID %>');
                    if (hdnCheckModule != null)
                        hdnCheckModule.value = "1";
                }
                else if (tabIndex == 1) {
                    var hdnCheckAlert = document.getElementById('<%= hdnCheckAlert.ClientID %>');
                    if (hdnCheckAlert != null)
                        hdnCheckAlert.value = "1";
                }
                else {
                    var hdnCheckIntranet = document.getElementById('<%= hdnCheckIntranet.ClientID %>');
                    if (hdnCheckIntranet != null)
                        hdnCheckIntranet.value = "1";
                }

                var panel = document.getElementById('<%= pnlSaveControls.ClientID %>');

                if (panel) {
                } else {
                    __doPostBack("<%=btnHiddenSavePanel.UniqueID %>", tabIndex);
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelJobRoles" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="count" />
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" Font-Size="14px" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <!-- wrapper -->
            <div class="jr-wrap">
                <div class="title-bar">
                    Job Roles Access Administration
                </div>
                <div class="data-panels">
                    <div>
                        <!-- left column -->
                        <div class="left-panel">
                            <div class="in-panels">
                                <table width="80%" border="0" align="center" cellpadding="3" cellspacing="0">
                                    <tr>
                                        <td width="21%" align="left">
                                            Job Role:
                                        </td>
                                        <td width="54%">
                                            <asp:TextBox ID="txtSearchJobrole" CssClass="txtfield" runat="server"></asp:TextBox>
                                        </td>
                                        <td width="20%">
                                            <asp:Button ID="btnSearch" CssClass="btn" runat="server" Text="Search" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            Team:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlSearchTeam" CssClass="slct" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:Panel ID="pnlSaveJobRole" runat="server" Visible="false">
                                <div class="in-panels">
                                    <table width="80%" border="0" align="center" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td align="left">
                                                Team:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlSaveTeam" CssClass="slct" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="21%" align="left">
                                                Job Role:
                                            </td>
                                            <td width="54%">
                                                <asp:TextBox ID="txtSaveJobRole" CssClass="txtfield" runat="server"></asp:TextBox>
                                            </td>
                                            <td width="20%">
                                                <asp:Button ID="btnSaveJobrole" CssClass="btn" Width="100px" runat="server" Text="Save Job Role" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="21%" align="left">
                                                <asp:Label ID="lblActive" runat="server" Text="Active:" Visible="false"></asp:Label>
                                                <asp:HiddenField ID="hdnUpdateSaveCheck" runat="server" />
                                            </td>
                                            <td width="54%">
                                                <asp:RadioButton ID="rbtnJobRoleActiveNo" runat="server" GroupName="JobRoleStatus"
                                                    Text="No" Visible="false" />
                                                &nbsp &nbsp
                                                <asp:RadioButton ID="rbtnJobRoleActiveYes" runat="server" GroupName="JobRoleStatus"
                                                    Text="Yes" Visible="false" />
                                            </td>
                                            <td width="20%">
                                                <asp:Button ID="btnCancelNew" CssClass="btn" runat="server" Text="Cancel" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </div>
                        <!-- left column //-->
                        <!-- right column -->
                        <div class="right-panel">
                            <div class="addnew-panel">
                                <asp:Button ID="btnAddNewJobRole" CssClass="btn" runat="server" Text="Add a New Job Role" />
                            </div>
                        </div>
                        <!-- right column //-->
                        <div class="clear">
                        </div>
                    </div>
                    <div>
                        <div class="left-panel">
                            <div class="in-panels">
                                <div style="margin-left: -5px; margin-right: -5px;">
                                    &nbsp
                                    <asp:Label ID="Label3" Font-Bold="true" Font-Size="13px" runat="server" Text="Job Role"></asp:Label>
                                    <asp:Button ID="btnToXls" runat="server" CssClass="btn rightControl" Width="70px"
                                        Text="To XLS" />
                                    <asp:Button ID="btnPrint" runat="server" CssClass="btn rightControl" Width="70px"
                                        Text="Print" />
                                </div>
                                <br />
                                <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                                <cc1:PagingGridView ID="grdJobRolesList" runat="server" AllowPaging="false" AllowSorting="True"
                                    AllowCustomPaging="true" AutoGenerateColumns="False" BackColor="White" ShowHeaderWhenEmpty="true"
                                    CssClass="sort-expression" HeaderStyle-CssClass="gridfaults-header" CellPadding="4"
                                    ForeColor="Black" GridLines="None" OrderBy="" PageSize="30" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Job Role" SortExpression="JobRoleName">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnJobRoleId" runat="server" Value='<%# Bind("JobRoleId") %>' />
                                                <asp:Label ID="lblJobRoleName" runat="server" Text='<%# Bind("JobRoleName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" Font-Bold="false" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Team" SortExpression="TeamName">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnTeamId" runat="server" Value='<%# Bind("TeamId") %>' />
                                                <asp:Label ID="lblTeamName" runat="server" Text='<%# Bind("TeamName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Bold="false" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Users" SortExpression="UsersCount">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnUsersCount" OnClick="lnkbtnUsersCount_Click" runat="server"
                                                    Text='<%# Bind("UsersCount") %>' Visible="false"></asp:LinkButton>
                                                <asp:Label ID="lblUsersCount" runat="server" Text='<%# Bind("UsersCount") %>' Visible="false"></asp:Label>
                                                <asp:HiddenField ID="hdnTeamJobRoleId" runat="server" Value='<%# Bind("TeamJobRoleId") %>' />
                                                <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Bind("IsActive") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Bold="false" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBtnEdit" runat="server" OnClick="lnkBtnEditJobRole_Click"
                                                    CommandArgument='<%#Eval("TeamJobRoleId")%>'><img src="../../Images/edit.png" title ="Amend" alt="Amend" style="border:none;" />  </asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="25px" BorderStyle="None" HorizontalAlign="Left" Font-Bold="false" />
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkBtnDelete" OnClick="lnkbtnDeleteJobRole_Click" runat="server"
                                                    CommandArgument='<%#Eval("TeamJobRoleId")%>'><img src="../../Images/cross2.png" style="border:none;" />  </asp:LinkButton>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            <ItemStyle Width="25px" BorderStyle="None" HorizontalAlign="Left" Font-Bold="false" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <%-- Grid View Styling Start --%>
                                    <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                    <SelectedRowStyle BackColor="#BBBBBB" ForeColor="Black" Font-Bold="True"></SelectedRowStyle>
                                    <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
                                    <HeaderStyle BackColor="#FFFFFF" ForeColor="Black" Font-Bold="True" Font-Underline="false"
                                        HorizontalAlign="Left"></HeaderStyle>
                                    <%-- Grid View Styling End --%>
                                    <PagerSettings Mode="NumericFirstLast">
                                    </PagerSettings>
                                </cc1:PagingGridView>
                                <%--Pager Template Start--%>
                                <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                                    background-color: #ffffff; font-weight: normal; vertical-align: middle; border-top: 0px;">
                                    <hr />
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td style="width: 77%;">
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                                        CommandArgument="First" />
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Previous" runat="server" CommandName="Page"
                                                                        CommandArgument="Prev" />
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    Records:
                                                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                                    to
                                                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                                    of
                                                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                                                    Page:
                                                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                                    of
                                                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                                        CommandArgument="Next" />
                                                                    &nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                                        CommandArgument="Last" />
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    Page:
                                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                                        ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                                        Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </asp:Panel>
                                <%--Pager Template End--%>
                            </div>
                        </div>
                        <asp:Panel ID="pnlTabs" Visible="false" runat="server">
                            <div id="tabsPanel" class="right-panel">
                                <div class="tab-panel">
                                    <div class="tab-bar">
                                        <div>
                                            <a id="moduleTab" href="#" onclick="showModules()" class="tab-active aleft">Modules
                                            </a><a id="alertTab" href="#" onclick="showAlerts()" class="tab aleft">Alerts </a>
                                            <a id="intranetTab" href="#" onclick="showIntranet()" class="tab aleft">Intranet
                                            </a>
                                        </div>
                                    </div>
                                    <div class="tabdata">
                                        <div id="moduleData" class="showDiv">
                                            <uc1:Modules ID="ModulesControl" runat="server" />
                                            <asp:HiddenField ID="hdnCheckModule" runat="server" Value="0" />
                                        </div>
                                        <div id="alertData" class="hideDiv">
                                            <uc2:Alerts ID="AlertsControl" runat="server" />
                                            <asp:HiddenField ID="hdnCheckAlert" runat="server" Value="0" />
                                        </div>
                                        <div id="intranetData" class="hideDiv">
                                            <uc3:Intranet ID="IntranetControl" runat="server" />
                                            <asp:HiddenField ID="hdnCheckIntranet" runat="server" Value="0" />
                                        </div>
                                        <asp:Panel ID="pnlSaveControls" Visible="false" runat="server">
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn leftControl"
                                                Width="100px" />
                                            <asp:Button ID="btnSaveAccessRights" OnClientClick ="clickSaveAccessRights()"  runat="server" Width="150px" Text="Save Access Rights"
                                                CssClass="btn rightControl" />
                                        </asp:Panel>
                                        <asp:Button ID="btnHiddenSavePanel" runat="server" Text="" Style="display: none;" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </div>
            <!-- wrapper ends //-->
            <%-- JOB ROLE POPUP --%>
            <asp:Button ID="Button1" runat="server" Text="" Style="display: none;" />
            <asp:ModalPopupExtender ID="mdlPopupJobRole" runat="server" TargetControlID="btnHidden"
                PopupControlID="pnlJobRole" Enabled="true" DropShadow="true" CancelControlID="imgBtnCloseAddAppointment">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlJobRole" CssClass="left" runat="server" BackColor="White" Width="350px"
                Height="350px" Style="padding: 15px; padding-bottom: 25px; border: 1px solid black;
                font-weight: normal; font-size: 13px;">
                <br />
                <div style="border-width: 1px; border-style: solid;">
                    <table style="width: 100%; margin: 10px;">
                        <tr>
                            <td>
                                <asp:Label ID="Label1" Font-Bold="true" runat="server" Text="Job Role:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPopJobrole" Font-Bold="true" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Team:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblPopTeam" Font-Bold="true" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Employee Name:</b>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <hr />
                    <div style="clear: both; height: 1px;">
                    </div>
                    <asp:ImageButton ID="imgBtnCloseAddAppointment" runat="server" Style="position: absolute;
                        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                    <div style="overflow: auto; height: 230px; padding-top: -10px !important; padding-left: 10px;">
                        <asp:GridView ID="grdEmployees" runat="server" AutoGenerateColumns="False" Width="100%"
                            CssClass="topMargin" AllowSorting="false" AllowPaging="False" GridLines="None"
                            CellSpacing="10" HorizontalAlign="Left" ClientIDMode="AutoID" CellPadding="2"
                            BorderColor="#BBBBBB">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmployee" runat="server" Text='<%#Eval("EmployeeName").Tostring()%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle Wrap="True" />
                            <RowStyle BorderStyle="None" />
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <%-- CONFIRM DELETION POPUP --%>
            <asp:Button ID="btnHidden2" runat="server" Text="" Style="display: none;" />
            <asp:ModalPopupExtender ID="mdlPopupJobRoleDeletion" runat="server" TargetControlID="btnHidden2"
                BackgroundCssClass="modalBackground" PopupControlID="pnlJobRoleDeletion" Enabled="true"
                DropShadow="true" CancelControlID="imgBtnCloseJobRoleDeletion">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlJobRoleDeletion" CssClass="left" runat="server" BackColor="White"
                Width="350px" Style="padding: 15px; padding-bottom: 25px; border: 1px solid black;
                font-weight: normal; font-size: 13px;">
                <br />
                <div>
                    <table style="width: 100%; margin: 10px;">
                        <tr>
                            <td align="center">
                                <b>Job Role will be deleted permanently</b>
                                <asp:HiddenField ID="hdnJobRoleTeamId" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnDeleteOk" Width="100px" runat="server" Text="OK" />
                            </td>
                        </tr>
                    </table>
                    <asp:ImageButton ID="imgBtnCloseJobRoleDeletion" runat="server" Style="position: absolute;
                        top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                </div>
            </asp:Panel>
            <%-- MESSAGE POPUP --%>
            <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
            <asp:ModalPopupExtender ID="mdlPopupMessage" runat="server" TargetControlID="btnHidden3"
                PopupControlID="pnlUserMessage" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
                CancelControlID="btnClose">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlUserMessage" CssClass="left" runat="server" BackColor="White" Width="350px"
                Style="border: 1px solid black; font-weight: normal; font-size: 13px;">
                <br />
                <div>
                    <table style="width: 100%; margin-top: -16px; margin-bottom: 20px;">
                        <tr style="background-color: Gray; height: 40px;">
                            <td align="center">
                                <asp:Label ID="lblMessageStatus" Font-Bold="true" ForeColor="White" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblUserMessage" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnClose" CssClass="btn" Width="100px" runat="server" Text="Close" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnToXls" />
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
