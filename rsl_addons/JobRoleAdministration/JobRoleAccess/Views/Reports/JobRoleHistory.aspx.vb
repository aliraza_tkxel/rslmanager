﻿Imports JRA_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports JRA_BusinessLogic
Imports JRA_BusinessObject
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Threading
Imports System.Globalization


Public Class JobRoleHistory
    Inherits System.Web.UI.Page
#Region "Properties"
    Dim totalCount As Integer = 0
    Private PageSize As Integer = 30
    Public Shared empId As Integer = 0
    Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "CreatedDate", 1, PageSize)
    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
#End Region

#Region "Events"
#Region "Page Load event"
    ''' <summary>
    ''' Page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                getQueryStringValues()
                If empId = 0 Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.JobRoleHistory, True)
                Else
                    populateJobRoleList(empId)
                End If

                'Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "Date", 1, 30)
                'ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btnToXls click event"
    ''' <summary>
    ''' btnToXls click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnToXls_Click(sender As Object, e As EventArgs) Handles btnToXls.Click
        Try
            exportToExcel()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            'If uiMessageHelper.IsExceptionLogged = False Then
            '    ExceptionPolicy.HandleException(ex, "Exception Policy")
            'End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If

        End Try

    End Sub
#End Region
#End Region
#Region "Functions"
#Region "Populate Job Role List"
    ''' <summary>
    ''' Populate Job Role List
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateJobRoleList(ByVal empId As Integer)

        Dim objJobRoleBL As JobRoleBL = New JobRoleBL()
        Dim resultDataset As DataSet = New DataSet()
        objPageSortBo.PageSize = PageSize
        totalCount = objJobRoleBL.searchTeamJobRoleHistory(resultDataset, objPageSortBo, empId)
        grdJobRolesList.DataSource = resultDataset.Tables(0)
        grdJobRolesList.DataBind()


        If (resultDataset.Tables(0).Rows.Count() > 0) Then
            pnlPagination.Visible = True
            btnToXls.Enabled = True
        Else
            pnlPagination.Visible = False
            btnToXls.Enabled = False
        End If

        objPageSortBo.TotalRecords = totalCount
        Dim toRecord As Integer = Convert.ToInt32(resultDataset.Tables(0).Rows.Count)
        'setPageSortBoViewState(objPageSortBo)
        ' GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        Me.PopulatePager(totalCount, objPageSortBo.PageNumber, toRecord)
        'pnlSaveJobRole.Visible = False
        'pnlTabs.Visible = False

    End Sub

#End Region
#Region " Pager Change Event"
    ''' <summary>
    ''' Page index page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Changed(sender As Object, e As EventArgs)
        Dim pageIndex As Integer = Integer.Parse(TryCast(sender, LinkButton).CommandArgument)
        objPageSortBo.PageNumber = pageIndex
        Me.populateJobRoleList(empId)
    End Sub
#End Region
#Region "Populate Pager"
    ''' <summary>
    ''' Populate Pager for paging
    ''' </summary>
    ''' <param name="recordCount"></param>
    ''' <param name="currentPage"></param>
    ''' <param name="toRecords"></param>
    ''' <remarks></remarks>
    Private Sub PopulatePager(recordCount As Integer, currentPage As Integer, toRecords As Integer)
        'PageSize = Convert.ToInt32(txtResults.Text.Trim())
        Dim dblPageCount As Double = CDbl(CDec(recordCount) / Convert.ToDecimal(PageSize))
        Dim pageCount As Integer = CInt(Math.Ceiling(dblPageCount))
        Dim pages As New List(Of ListItem)()
        If pageCount > 0 Then
            pages.Add(New ListItem("<div style=' float:left; width:100%;'><div style=' float:left;'><<", "1", currentPage > 1))
            pages.Add(New ListItem("<", currentPage - 1, currentPage > 1))
            pages.Add(New ListItem("Page <input type='text' value='" + currentPage.ToString() + "' style='width:30px;' id='txtPage' onblur='changePage();'    /> of " + pageCount.ToString() + " :", "", False))

            pages.Add(New ListItem(">", currentPage + 1, currentPage < pageCount))
            pages.Add(New ListItem(">></div>", pageCount.ToString(), currentPage < pageCount))
            pages.Add(New ListItem("<div style=' float:right;'>Displaying " + ((currentPage - 1) * PageSize + 1).ToString() + " - " + (((currentPage - 1) * PageSize) + toRecords).ToString() + " of " + recordCount.ToString() + "</div></div>", "", False))

        End If
        rptPager.DataSource = pages
        rptPager.DataBind()
    End Sub
#End Region
#Region "get Query String Values"
    ''' <summary>
    ''' get Query String Values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub getQueryStringValues()

        If Request.QueryString(PathConstants.empId) IsNot Nothing Then
            empId = Convert.ToInt32(Request.QueryString(PathConstants.empId))
        End If
    End Sub
#End Region
#Region "Export to excel"
    ''' <summary>
    ''' This is the function that export the gridview data in excel sheet.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub exportToExcel()
        Response.Clear()
        Response.Buffer = True
        Dim fileName As String = "JobRoleAdministration_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls"
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim strWrite As New StringWriter()
        Dim htmWrite As New HtmlTextWriter(strWrite)
        Dim htmfrm As New HtmlForm()
        populateJobRoleList(empId)
        grdJobRolesList.Parent.Controls.Add(htmfrm)
        htmfrm.Attributes("runat") = "server"
        htmfrm.Controls.Add(grdJobRolesList)
        htmfrm.RenderControl(htmWrite)
        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Write(strWrite.ToString())
        Response.Flush()
        Response.[End]()
    End Sub
#End Region
#Region "Go to page Number"
    ''' <summary>
    ''' Go to page Number
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnHidden.Click
        Dim argument = Request.Form("__EVENTARGUMENT")
        objPageSortBo.PageNumber = argument
        populateJobRoleList(empId)
    End Sub
#End Region



#End Region


End Class