﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/JobRoleAccess.Master"
    CodeBehind="JobRoleAuditReport.aspx.vb" Inherits="JobRoleAccess.JobRoleAuditReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/JobRole.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">

        function changePage() {
            var page = document.getElementById("txtPage").value;
            __doPostBack('<%= btnHidden.UniqueID %>', page);
        }
                  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelJobRoles" runat="server" UpdateMode="Conditional">
    
        <ContentTemplate>
            <!-- wrapper -->
            <div class="jr-wrap">
                <div class="title-bar">
                    Job Role Audit Report</div>
                <br />
                <div class="in-panels" style="margin-left: 10px;">
                    <table id="tbfilters" cellspacing="4" cellpadding="4" style="margin-bottom: 10px;
                        margin-top: 10px;">
                        <tr>
                            <td style="text-align: left;">
                                Filters:
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                Quick Find:
                            </td>
                            <td>
                                <asp:TextBox ID="txtSearch" CssClass="txtfield" runat="server" Height="21px" Width="100%"
                                    AutoPostBack="false" AutoCompleteType="Search"></asp:TextBox>
                                <ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                    TargetControlID="txtSearch" WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                                </ajaxToolkit:TextBoxWatermarkExtender>
                                <div style="display: none;">
                                    <asp:HiddenField ID="hdnSelectedPropertyId" runat="server" />
                                </div>
                            </td>
                            <td>
                                &nbsp &nbsp &nbsp &nbsp
                            </td>
                            <td>
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDate" CssClass="txtfield" Width="80%" name="txtDate" runat="server"> </asp:TextBox>
                                &nbsp<asp:Image ID="imgCalDate" runat="server" src="../../Images/calendar.png" /><asp:CalendarExtender
                                    ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy" PopupButtonID="imgCalDate"
                                    PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                            </td>
                            <td>
                                &nbsp &nbsp &nbsp &nbsp
                            </td>
                            <td>
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Action:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAction" CssClass="slct" runat="server" Width="101%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp &nbsp &nbsp &nbsp
                            </td>
                            <td>
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                User:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlUser" CssClass="slct" runat="server" Width="101%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp &nbsp &nbsp &nbsp
                            </td>
                            <td>
                                <asp:Button ID="btnUpdateResult" runat="server" CssClass="btn" Text="  Update Results  " />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div style="margin-left: -5px; margin-right: -5px;">
                        &nbsp
                        <asp:Button ID="btnToXls" runat="server" CssClass="btn rightControl" Width="100px"
                            Text="To XLS" />
                        <asp:Button ID="btnPrint" runat="server" CssClass="btn rightControl" Width="100px"
                            Text="Print" />


                    </div>
                    <br />
                    <br />
                    <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                    <div id="reportDiv">
                    <cc1:PagingGridView ID="grdReport" runat="server" AllowPaging="True" AllowSorting="True"
                        AllowCustomPaging="true" AutoGenerateColumns="False" BackColor="White" ShowHeaderWhenEmpty="true"
                        CssClass="sort-expression" HeaderStyle-CssClass="gridfaults-header" CellPadding="4"
                        ForeColor="Black" OrderBy="" PageSize="30" Width="100%" GridLines="None" Style="border: none;">
                        <Columns>
                            <asp:TemplateField HeaderText="Action" SortExpression="ActionName">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnActionId" runat="server" Value='<%# Bind("JobRoleActionId") %>' />
                                    <asp:Label ID="lblActionName" runat="server" Text='<%# Bind("ActionName") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Team" SortExpression="TeamName">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTeamId" runat="server" Value='<%# Bind("JobRoleTeamId") %>' />
                                    <asp:Label ID="lblTeamName" runat="server" Text='<%# Bind("TeamName") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job Role" SortExpression="JobRoleName">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnJobRoleId" runat="server" Value='<%# Bind("JobRoleId") %>' />
                                    <asp:Label ID="lblJobRoleName" runat="server" Text='<%# Bind("JobRoleName") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Details" SortExpression="Detail">
                                <ItemTemplate>
                                    <asp:Label ID="lblDetails" runat="server" Text='<%# Bind("Detail") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User" SortExpression="UserName">
                                <ItemTemplate>
                                    <asp:Label ID="lblUser" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                    <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Bind("CreatedBy") %>' />
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date/Time" SortExpression="CreatedDateSort">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                <ItemStyle Width="20%" HorizontalAlign="Left" />
                            </asp:TemplateField>
                        </Columns>
                        <%-- Grid View Styling Start --%>
                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                        <SelectedRowStyle BackColor="#BBBBBB" ForeColor="Black" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
                        <HeaderStyle BackColor="#FFFFFF" ForeColor="Black" Font-Bold="True" Font-Underline="false"
                            HorizontalAlign="Left"></HeaderStyle>
                        <%-- Grid View Styling End --%>
                        <PagerSettings Mode="NumericFirstLast"></PagerSettings>
                        <RowStyle BackColor="#F5F9E0"></RowStyle>
                        <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
                    </cc1:PagingGridView>

                    </div>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        background-color: #ffffff; font-weight: normal; vertical-align: middle; border-top: 0px;">
                        <hr />
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td style="width: 77%;">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                            CommandArgument="First" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Previous" runat="server" CommandName="Page"
                                                            CommandArgument="Prev" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        Records:
                                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                        to
                                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                        of
                                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                                        &nbsp &nbsp Page:
                                                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                        of
                                                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                            CommandArgument="Next" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                            CommandArgument="Last" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right">
                                        Page:
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                    <%--Pager Template End--%>
                </div>
            </div>
            <div>
            </div>
            <%-- MESSAGE POPUP --%>
            <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
            <asp:ModalPopupExtender ID="mdlPopupMessage" runat="server" TargetControlID="btnHidden3"
                PopupControlID="pnlUserMessage" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
                CancelControlID="btnClose">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlUserMessage" CssClass="left" runat="server" BackColor="White" Width="350px"
                Style="border: 1px solid black; font-weight: normal; font-size: 13px;">
                <br />
                <div>
                    <table style="width: 100%; margin-top: -16px; margin-bottom: 20px;">
                        <tr style="background-color: Gray; height: 40px;">
                            <td align="center">
                                <asp:Label ID="lblMessageStatus" Font-Bold="true" ForeColor="White" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblUserMessage" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnClose" CssClass="btn" Width="100px" runat="server" Text="Close" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnToXls" />
            <asp:PostBackTrigger ControlID="btnPrint" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
