﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JobRoleHistory.aspx.vb"
    Inherits="JobRoleAccess.JobRoleHistory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Job Role history</title>
  <%--  <link href="../../Styles/JobRole.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/RSL.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" charset="utf-8">

        function changePage() {
            var page = document.getElementById("txtPage").value;
            __doPostBack('<%= btnHidden.UniqueID %>', page);
        }            
    </script>
    <script type="text/javascript">
        function CallPrint() {
            var prtContent = document.getElementById('divGridRoleHistory');
            var WinPrint = window.open('', '', 'letf=0,top=0,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="left-panel">
        <div class="in-panels">
            <div style="padding: 5px;">
                &nbsp
                <asp:Label ID="Label3" Font-Bold="true" Font-Size="13px" runat="server" Text="Job Role History"></asp:Label>
                <input type="button" value="Print " id="btnPrint" class="rightControl" onclick="javascript:CallPrint()" />
                <asp:Button ID="btnToXls" runat="server" CssClass="rightControl" Text="To XLS" UseSubmitBehavior="False" />
            </div>
            <br />
            <asp:UpdatePanel ID="updPanelJobRoles" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" Font-Size="14px" runat="server"></asp:Label>
                        <br />
                    </asp:Panel>
                    <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                    <div id="divGridRoleHistory">
                        <cc1:PagingGridView ID="grdJobRolesList" runat="server" AllowSorting="True" AllowCustomPaging="true"
                            AutoGenerateColumns="False" BackColor="White" ShowHeaderWhenEmpty="True" CssClass="sort-expression"
                            CellPadding="4" ForeColor="Black" GridLines="None" OrderBy="" PageSize="30" Width="100%">
                            <Columns>
                              
                                 <asp:TemplateField HeaderText="Date" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20%" Font-Bold="false" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Job Role" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblJobRoleName" runat="server" Text='<%# Bind("JobRoleName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="30%" Font-Bold="false" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Team">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTeamName" runat="server" Text='<%# Bind("TeamName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Width="20%" Font-Bold="false" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="CreatedBy" HeaderText="By" />
                            </Columns>
                            <%-- Grid View Styling Start --%>
                            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                            <SelectedRowStyle BackColor="#BBBBBB" ForeColor="Black" Font-Bold="True"></SelectedRowStyle>
                            <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
                            <HeaderStyle BackColor="#FFFFFF" ForeColor="Black" Font-Bold="True" Font-Underline="false"
                                HorizontalAlign="Left"></HeaderStyle>
                            <%-- Grid View Styling End --%>
                        </cc1:PagingGridView>
                   </div>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        background-color: #ffffff; font-weight: normal; vertical-align: middle; border-top: 0px;">
                        <hr />
                        <div style='width: 90%; float: left; text-align: left;'>
                            &nbsp;
                            <asp:Repeater ID="rptPager" runat="server">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                        Enabled='<%# Eval("Enabled") %>' OnClick="Page_Changed" Style='text-decoration: none;
                                        color: #000000;'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                    <%--Pager Template End--%> 
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    </form>
</body>
</html>
