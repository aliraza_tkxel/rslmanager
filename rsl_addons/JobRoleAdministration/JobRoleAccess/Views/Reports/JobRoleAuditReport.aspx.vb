﻿Imports JRA_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports JRA_BusinessLogic
Imports JRA_BusinessObject
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Threading
Imports System.Globalization

Public Class JobRoleAuditReport
    Inherits PageBase

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then

                Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
                ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)

                populateActionsDropdown()
                populateUsersDropdown()
                populateReport()

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub

#End Region

#Region "Update Result button click event"
    ''' <summary>
    ''' Update Result button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdateResult_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdateResult.Click
        Try
            populateReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            Dim pageNumber As Integer = 1
            Integer.TryParse(txtPageNumber.Text, pageNumber)
            txtPageNumber.Text = String.Empty

            objPageSortBo = getPageSortBoViewState()

            If pageNumber >= 1 AndAlso pageNumber <= objPageSortBo.TotalPages Then
                objPageSortBo = getPageSortBoViewState()
                objPageSortBo.PageNumber = pageNumber
                setPageSortBoViewState(objPageSortBo)
                Dim a As EventArgs
                Dim b As Object
                Me.btnUpdateResult_Click(b, a)

            Else
                showMessagePopup(UserMessageConstants.ErrorStatus, UserMessageConstants.InvalidPageNumber)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            Dim a As EventArgs
            Dim b As Object
            Me.btnUpdateResult_Click(b, a)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Gridview sorting"
    ''' <summary>
    ''' Gridview sorting
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdReport_Sorting(sender As Object, e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles grdReport.Sorting
        Try
            Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
            objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

            objPageSortBo.SortExpression = e.SortExpression
            objPageSortBo.PageNumber = 1
            grdReport.PageIndex = 0
            objPageSortBo.setSortDirection()

            ViewState.Item(ViewStateConstants.PageSortBo) = objPageSortBo
            populateReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Export to XLS button click"
    ''' <summary>
    ''' Export to XLS button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnToXls_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnToXls.Click
        Try
            ExportGridToExcel(getFullJobRoleReportGridView())

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Print button click"
    ''' <summary>
    ''' Export button click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        Try
            Me.printGridView(getFullJobRoleReportGridView())
            populateReport()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate Report"
    ''' <summary>
    ''' Populate Report
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateReport()

        Dim reportDataSet As DataSet = New DataSet()
        Dim objReportBL As ReportBL = New ReportBL()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
        Dim totalCount As Integer

        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)

        Dim userID As Int32 = Convert.ToInt32(ddlUser.SelectedValue)
        Dim actionID As Int32 = Convert.ToInt32(ddlAction.SelectedValue)
        Dim selectedDate As String

        If Not String.IsNullOrEmpty(txtDate.Text) Then
            selectedDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text)
        Else
            selectedDate = String.Empty
        End If

        Dim searchText As String = txtSearch.Text
        totalCount = objReportBL.getReportData(userID, actionID, selectedDate, reportDataSet, objPageSortBo, searchText, ApplicationConstants.AllowPaging)
        grdReport.DataSource = reportDataSet
        grdReport.DataBind()

        If (reportDataSet.Tables(0).Rows.Count() > 0) Then
            btnToXls.Enabled = True
            btnPrint.Enabled = True
            pnlPagination.Visible = True
        Else

            btnToXls.Enabled = False
            btnPrint.Enabled = False
            pnlPagination.Visible = False
            showMessagePopup(UserMessageConstants.ErrorStatus, UserMessageConstants.NoRecordFound)
        End If

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
        setPageSortBoViewState(objPageSortBo)
        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)


    End Sub

#End Region

#Region "Show user message popup"
    ''' <summary>
    ''' Show user message popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showMessagePopup(ByVal status As String, ByVal message As String)

        lblMessageStatus.Text = status
        lblUserMessage.Text = message
        mdlPopupMessage.Show()

    End Sub

#End Region

#Region "Populate actions dropdown"
    ''' <summary>
    ''' Populate actions dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateActionsDropdown()

        Dim objReportBL As ReportBL = New ReportBL()
        Dim actionDataSet As DataSet = New DataSet()

        objReportBL.getjobRoles(actionDataSet)
        ddlAction.DataSource = actionDataSet.Tables(0).DefaultView
        ddlAction.DataValueField = "JobRoleActionId"
        ddlAction.DataTextField = "ActionTitle"
        ddlAction.DataBind()

        Dim item As ListItem = New ListItem("Please select", "-1")
        ddlAction.Items.Insert(0, item)

    End Sub

#End Region

#Region "Populate users dropdown"
    ''' <summary>
    ''' Populate users dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateUsersDropdown()

        Dim userDataSet As DataSet = New DataSet()
        Dim objReportBL As ReportBL = New ReportBL()
        Dim item As ListItem = New ListItem("Please select", "-1")

        objReportBL.getUsers(userDataSet)
        ddlUser.DataSource = userDataSet.Tables(0).DefaultView
        ddlUser.DataValueField = "UserId"
        ddlUser.DataTextField = "UserName"
        ddlUser.DataBind()
        ddlUser.Items.Insert(0, item)

    End Sub
#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Get full JobRole Report GridView"
    ''' <summary>
    ''' Get full JobRole Report GridView
    ''' </summary>
    ''' <remarks></remarks>
    Function getFullJobRoleReportGridView()

        Dim grdFullJobRoleReport As New GridView()
        grdFullJobRoleReport.AllowPaging = False
        grdFullJobRoleReport.AutoGenerateColumns = False
        grdFullJobRoleReport.AlternatingRowStyle.BackColor = Color.White
        grdFullJobRoleReport.RowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#F5F9E0")

        Dim actionField As BoundField = New BoundField()
        actionField.HeaderText = "Action"
        actionField.DataField = "ActionName"
        grdFullJobRoleReport.Columns.Add(actionField)

        Dim teamField As BoundField = New BoundField()
        teamField.HeaderText = "Team"
        teamField.DataField = "TeamName"
        grdFullJobRoleReport.Columns.Add(teamField)

        Dim jobRoleField As BoundField = New BoundField()
        jobRoleField.HeaderText = "Job Role"
        jobRoleField.DataField = "JobRoleName"
        grdFullJobRoleReport.Columns.Add(jobRoleField)

        Dim detailsField As BoundField = New BoundField()
        detailsField.HeaderText = "Details"
        detailsField.DataField = "Detail"
        grdFullJobRoleReport.Columns.Add(detailsField)

        Dim userField As BoundField = New BoundField()
        userField.HeaderText = "User"
        userField.DataField = "UserName"
        grdFullJobRoleReport.Columns.Add(userField)

        Dim usersField As BoundField = New BoundField()
        usersField.HeaderText = "Date/Time"
        usersField.DataField = "CreatedDate"
        grdFullJobRoleReport.Columns.Add(usersField)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objReportBL As ReportBL = New ReportBL()
        Dim objPageSortBo As PageSortBO = New PageSortBO("DESC", "ActionName", 1, 30)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim userID As Int32 = Convert.ToInt32(ddlUser.SelectedValue)
        Dim actionID As Int32 = Convert.ToInt32(ddlAction.SelectedValue)
        Dim selectedDate As String

        If Not String.IsNullOrEmpty(txtDate.Text) Then
            selectedDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text)
        Else
            selectedDate = String.Empty
        End If

        Dim searchText As String = txtSearch.Text
        objReportBL.getReportData(userID, actionID, selectedDate, resultDataSet, objPageSortBo, searchText, Not ApplicationConstants.AllowPaging)
        grdFullJobRoleReport.DataSource = resultDataSet.Tables(0)
        grdFullJobRoleReport.DataBind()

        Return grdFullJobRoleReport

    End Function
#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel(ByVal grdViewObject As GridView)

        Dim fileName As String = "JobRoleAdministrationAuditReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdViewObject.RenderControl(hw)

        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.BufferOutput = True
        Response.Flush()
        Response.Close()

    End Sub

#End Region

#Region "Print Gridview"
    ''' <summary>
    ''' Print Gridview
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub printGridView(ByVal grdView As GridView)

        Dim fileName As String = "JobRoleAuditReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)

        grdView.RenderControl(hw)

        Dim gridHTML As String = sw.ToString().Replace("""", "'").Replace(vbCr, " ").Replace(vbLf, " ").Replace(vbTab, "")

        Dim sb As New StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload = new function(){")
        sb.Append("var printWin = window.open('', '', 'left=0")
        sb.Append(",top=0,width=1000,height=1000,status=0');")
        sb.Append("printWin.document.write(""")
        sb.Append(gridHTML)
        sb.Append(""");")
        sb.Append("printWin.document.close();")
        sb.Append("printWin.focus();")
        sb.Append("printWin.print();")
        sb.Append("printWin.close();};")
        sb.Append("</script>")
        ClientScript.RegisterStartupScript(Me.[GetType](), fileName, sb.ToString())

    End Sub
#End Region

#End Region

End Class