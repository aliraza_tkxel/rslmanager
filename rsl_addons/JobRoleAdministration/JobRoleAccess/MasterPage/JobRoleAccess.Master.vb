﻿Imports System
Imports JRA_BusinessLogic
Imports JRA_Utilities


Public Class JobRoleAccess
    Inherits System.Web.UI.MasterPage

    Public messageClass As String = "topmessagesuccess"
    Private varBackgroundGrayColor As String = "#e6e6e6"
    Private varBackgroundWhiteColor As String = "#FFFFFF"
    Private varBackgroundProperty As String = "background-color"
    Private varPageUrl As String

#Region "Events"

#Region "Page Load"

    ''' <summary>
    ''' Page Load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'TODO Remove employee id.
            'SessionManager.setJobRoleAccessUserId(615)
            'SessionManager.setUserEmployeeId(615)

            populateMenus()
        End If

    End Sub

#End Region

#Region "Menu Item Click"
    ''' <summary>
    ''' Menu Item Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub menuHeader_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs) Handles menuHeader.MenuItemClick

        populatePageMenu()

    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Populate Menus"
    ''' <summary>
    ''' Populate Menus
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub populateMenus()

        populateModules()
        populateHeaderMenus()
        populatePageMenu()

    End Sub

#End Region

#Region "Populate modules menu"
    ''' <summary>
    ''' Populate modules menu
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateModules()
        Dim objRSLModulesBL As New MasterPageBL
        Dim modulesDataset As DataSet = New DataSet
        Dim employeeId As Integer = SessionManager.getUserEmployeeId

        'populate Business Menu
        objRSLModulesBL.getRSLModules(modulesDataset, employeeId)
        rptRSLMenu.DataSource = modulesDataset
        rptRSLMenu.DataBind()

    End Sub

#End Region

#Region "Populate header menu"
    ''' <summary>
    ''' Populate header menu
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateHeaderMenus()

        Dim objRSLModulesBL As New MasterPageBL
        Dim resultDataset As DataSet = New DataSet
        Dim employeeId As Integer = SessionManager.getUserEmployeeId

        'Populate Header Menu
        objRSLModulesBL.getRSLMenus(resultDataset, employeeId) ' Need to change UserId  
        For Each parentItem As DataRow In resultDataset.Tables(0).Rows

            Dim categoryItem As MenuItem = New MenuItem("  " + parentItem("Menu").ToString() + "  ", " ", "../Images/menu/sep.jpg")

            Dim varPageUrl = Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1)
            If varPageUrl = PathConstants.JobRoleHighlight And parentItem("Menu").ToString = ApplicationConstants.AccessControlMenuName Then
                categoryItem.Selected = True
            ElseIf (varPageUrl = PathConstants.JobRoleAuditReportHighlight And parentItem("Menu").ToString = ApplicationConstants.ReportsMenuName) Then
                categoryItem.Selected = True
            End If

            categoryItem.Value = parentItem("MenuId").ToString()
            menuHeader.Items.Add(categoryItem)
        Next
        menuHeader.DataBind()

    End Sub

#End Region

#Region "Populate page menu"
    ''' <summary>
    ''' Populate page menu
    ''' </summary>
    ''' <remarks></remarks>
    Sub populatePageMenu()

        Dim objRSLModulesBL As New MasterPageBL
        Dim pageMenuDatset As DataSet = New DataSet
        Dim employeeId As Integer = SessionManager.getUserEmployeeId

        menuPage.Items.Clear()
        Dim selectedMenuId As Int32
        If menuHeader.Items.Count > 0 AndAlso Not Request.Url.PathAndQuery.Contains("AccessDenied") Then
            selectedMenuId = Convert.ToInt32(menuHeader.SelectedItem.Value)
        End If

        'Populate Page Menu

        Dim varPageUrl = Request.Url.AbsolutePath.Substring(Request.Url.AbsolutePath.LastIndexOf("/") + 1)
        objRSLModulesBL.getRSLPage(pageMenuDatset, employeeId, selectedMenuId)
        For Each parentItem As DataRow In pageMenuDatset.Tables(0).Rows
            Dim categoryItem As MenuItem = New MenuItem("  " + parentItem("PageName").ToString() + "  ")
            'categoryItem.NavigateUrl = parentItem("PageNamePath").ToString()

            ' Select current page
            If varPageUrl = PathConstants.JobRoleHighlight And parentItem("PageName").ToString = ApplicationConstants.JobRolePageName Then
                categoryItem.Selected = True
            ElseIf (varPageUrl = PathConstants.JobRoleAuditReportHighlight And parentItem("PageName").ToString = ApplicationConstants.JobRoleReportPageName) Then
                categoryItem.Selected = True
            End If

            'Set navigation path
            If (parentItem("PageName").ToString = ApplicationConstants.JobRolePageName) Then
                categoryItem.NavigateUrl = "../" + parentItem("PageNamePath").ToString()
            ElseIf (parentItem("PageName").ToString = ApplicationConstants.JobRoleReportPageName) Then
                categoryItem.NavigateUrl = "../" + parentItem("PageNamePath").ToString()
            Else
                categoryItem.NavigateUrl = "../../" + parentItem("PageNamePath").ToString()
            End If

            menuPage.Items.Add(categoryItem)
        Next
        menuPage.DataBind()

        If (menuPage.Items.Count > 0) Then
            pnlPageMenu.Visible = True
        Else
            pnlPageMenu.Visible = False
        End If

        updPanelDashboard.Update()

    End Sub

#End Region

#Region "Remove Session Values"
    ''' <summary>
    ''' Remove Session Values
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub removeSessionValues()
        Session.RemoveAll()
    End Sub
#End Region

#End Region


End Class