USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[E_JOBROLE]    Script Date: 12/17/2013 14:42:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[E_JOBROLE](
	[JobRoleId] [int] IDENTITY(1,1) NOT NULL,
	[JobeRoleDescription] [varchar](50) NULL,
 CONSTRAINT [PK_E_JOBROLE] PRIMARY KEY CLUSTERED 
(
	[JobRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


