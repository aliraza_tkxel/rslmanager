USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

--DECLARE	@return_value int,
--		@isDeleted bit
--EXEC	@return_value = [dbo].[JRA_DeleteTeamJobRole]
--		@teamJobRoleId = 23,
--		@isDeleted = @isDeleted OUTPUT
--SELECT	@isDeleted as N'@isDeleted'

-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Delete team job role>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_DeleteTeamJobRole]
(
	@teamJobRoleId int,
	@isDeleted bit out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    BEGIN TRANSACTION;
	BEGIN TRY
    
     
    UPDATE E_JOBROLETEAM
	SET E_JOBROLETEAM.isDeleted = 1
    WHERE E_JOBROLETEAM.JobRoleTeamId = @teamJobRoleId
    
    
    END TRY
	BEGIN CATCH 
    IF @@TRANCOUNT > 0
	BEGIN     
   ROLLBACK TRANSACTION;   
	SET @isDeleted = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isDeleted = 1
 END
	

	
END
