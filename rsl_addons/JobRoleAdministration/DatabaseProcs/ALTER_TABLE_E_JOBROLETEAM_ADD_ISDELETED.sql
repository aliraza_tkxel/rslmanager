/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	10th January 2014
--  Description:	isDeleted is added to E_JOBROLETEAM to check whether team job role is deleted or not. 
--					
 '==============================================================================*/
 
USE [RSLBHALive]
GO

ALTER TABLE E_JOBROLETEAM
ADD isDeleted bit NOT NULL DEFAULT(0)