USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetTeamEmployees 
-- Author:<Ahmed Mehmood>
-- Create date: <24/12/2013>
-- Description:	<Get employees associated with team>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetTeamEmployees]
( 
	@teamJobRoleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT	E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME as EmployeeName
    FROM	E__EMPLOYEE
			INNER JOIN E_JOBDETAILS ON  E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID 
    WHERE	E__EMPLOYEE.JobRoleTeamId = @teamJobRoleId
			AND E_JOBDETAILS.ACTIVE = 1
	ORDER BY E__EMPLOYEE.LASTNAME ASC
    
	
END
