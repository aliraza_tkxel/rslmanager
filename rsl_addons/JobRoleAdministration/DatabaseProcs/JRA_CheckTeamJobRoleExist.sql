USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_CheckTeamJobRoleExist 
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Check Jobrole exist>
-- Web Page: JobRoles.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_CheckTeamJobRoleExist]
(
@teamId int
,@jobRole varchar(50)
,@isExist bit out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    	IF EXISTS (	SELECT	1
					FROM	E_JOBROLETEAM
							INNER JOIN (SELECT	E_JOBROLE.jobroleid as JobRoleId
										FROM	E_JOBROLE
										WHERE	E_JOBROLE.JobeRoleDescription= @jobRole ) AS TEAMJOBROLE ON E_JOBROLETEAM.JobRoleId = TEAMJOBROLE.JobRoleId 
					WHERE E_JOBROLETEAM.TeamId = @teamId
				)
		BEGIN	
				SET @isExist= 1
		END
		ELSE
		BEGIN
				SET @isExist= 0
		END
    
	
    
	
END

