USE [RSLBHALive]
GO 
--Created By : Ahmed Mehmood
--Description:This is table valued parameter is used for insertion of alertId of Team Job Role
--Web Page: JobRoles.aspx

CREATE TYPE [dbo].[TEAMJOBROLE_ALERTS_ACCESSRIGHTS] AS TABLE(
	[JobRoleTeamId] [int] NULL,
	[AlertId] [int] NULL
)
GO