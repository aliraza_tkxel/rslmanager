USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_ResetTeamAccessRights
-- Author:<Ahmed Mehmood>
-- Create date: <Create Date,,30/12/2012>
-- Description:	<Reset Team Access Rights>
-- Web Page: JobRoles.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_ResetTeamAccessRights](
@jobRoleTeamId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM AC_MODULES_ACCESS
	WHERE AC_MODULES_ACCESS.JobRoleTeamId = @jobRoleTeamId
	
	DELETE FROM AC_MENUS_ACCESS
	WHERE AC_MENUS_ACCESS.JobRoleTeamId = @jobRoleTeamId
	
	DELETE FROM AC_PAGES_ACCESS
	WHERE AC_PAGES_ACCESS.JobRoleTeamId = @jobRoleTeamId 
	
           
END
