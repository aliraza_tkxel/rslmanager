/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	11 June 2014
--  Description:	Add AccessLevel and ParentPage column to determine the levels of the page.
--					
 '==============================================================================*/
-- ADD SOrder Column
ALTER	TABLE AC_PAGES
ADD		AccessLevel INT NULL CONSTRAINT AccessLevel_Default Default 1
		,ParentPage INT	NULL
		,BridgeActualPage VARCHAR(300) NULL

UPDATE	AC_PAGES
SET		AccessLevel =1
