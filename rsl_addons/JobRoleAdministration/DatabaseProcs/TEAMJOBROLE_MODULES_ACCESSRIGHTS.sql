USE [RSLBHALive]
GO 
--Created By : Ahmed Mehmood
--Description:This is table valued parameter is used for insertion of moduleId of Team Job Role
--Web Page: JobRoles.aspx

CREATE TYPE [dbo].[TEAMJOBROLE_MODULES_ACCESSRIGHTS] AS TABLE(
	[JobRoleTeamId] [int] NULL,
	[ModuleId] [int] NULL
)
GO

CREATE TYPE [dbo].[TEAMJOBROLE_MENU_ACCESSRIGHTS] AS TABLE(
	[JobRoleTeamId] [int] NULL,
	[MenuId] [int] NULL
)
GO

CREATE TYPE [dbo].[TEAMJOBROLE_PAGE_ACCESSRIGHTS] AS TABLE(
	[JobRoleTeamId] [int] NULL,
	[PageId] [int] NULL
)
GO