USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
--EXEC	[dbo].[JRA_SearchJobRoleList]
--		@searchedText = N'george',
--		@teamId = 1

--		@pageSize = 2,
--		@pageNumber = 1,
--		@sortColumn = N'Recorded',
--		@sortOrder = N'DESC',
--		@totalCount = @totalCount OUTPUT	
	
-- Author:		<Ahmed Mehmood>
-- Create date: <20/12/2013>
-- Description:	<Search Job Role List>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_SearchJobRoleList] 
( 
	-- Add the parameters for the stored procedure here
		@searchedText varchar(8000)
		,@teamId int
		--Parameters which would help in sorting and paging
		,@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(50) = 'JobRoleName',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
)
AS
BEGIN
	DECLARE 
		@SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),        
        @rowNumberQuery varchar(6000),
        @finalQuery varchar(6500),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 AND E_JOBROLETEAM.isDeleted = 0 '
		
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND E_JOBROLE.JobeRoleDescription LIKE ''%' + @searchedText + '%'''
		END	
		
		IF @teamId != -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND E_TEAM.TEAMID = '+CONVERT(nvarchar(10), @teamId) 	 	 	
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+') 
		E_JOBROLETEAM.JobRoleTeamId AS TeamJobRoleId
		,E_JOBROLE.JobRoleId AS JobRoleId
		,E_JOBROLE.JobeRoleDescription  AS JobRoleName
		,E_TEAM.TEAMID AS TeamId
		,E_TEAM.TEAMNAME AS TeamName
		,ISNULL(TEAMCOUNT.TeamCount,0) AS UsersCount
		,E_JOBROLETEAM.IsActive '
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +' FROM	E_JOBROLETEAM
		INNER JOIN E_JOBROLE ON  E_JOBROLETEAM.JobRoleId = E_JOBROLE.JobRoleId 
		INNER JOIN E_TEAM ON E_JOBROLETEAM.TeamId = E_TEAM.TEAMID 
		LEFT JOIN (SELECT	E_JOBROLETEAM.JobRoleTeamId AS  JobRoleTeamId
							,COUNT(E__EMPLOYEE.JobRoleTeamId) AS TeamCount
					FROM	E_JOBROLETEAM
							LEFT JOIN E__EMPLOYEE ON E_JOBROLETEAM.JobRoleTeamId = E__EMPLOYEE.JobRoleTeamId
							INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
					WHERE	E_JOBDETAILS.ACTIVE =1
					GROUP BY E_JOBROLETEAM.JobRoleTeamId 
						) AS TEAMCOUNT ON  E_JOBROLETEAM.JobRoleTeamId = TEAMCOUNT.JobRoleTeamId '
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================									
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(2000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================							
END



GO


