USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetMenus @moduleId=12,@teamJobRoleId = 616
-- Author:<Ahmed Mehmood>
-- Create date: <12/18/2013>
-- Description:	<Get all menus related to module>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetMenus](
@teamJobRoleId int
,@moduleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT	AC_MENUS.MENUID MenuId, AC_MENUS.DESCRIPTION MenuName
    FROM	AC_MENUS
    WHERE	AC_MENUS.MODULEID = @moduleId and AC_MENUS.ACTIVE = 1
    
    SELECT	AC_MENUS_ACCESS.MenuId as MenuId
    FROM	AC_MENUS_ACCESS
    WHERE	AC_MENUS_ACCESS.JobRoleTeamId = @teamJobRoleId
    		
END
