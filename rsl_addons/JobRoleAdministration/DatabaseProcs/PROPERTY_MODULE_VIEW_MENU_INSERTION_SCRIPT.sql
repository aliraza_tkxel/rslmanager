/* ===========================================================================
--  Author:			Abdullah Saeed
--  DATE CREATED:	July 8, 2014
--  Description:	Insertion scripts for sub-tabs under Search Menu
--					
 '==============================================================================*/


BEGIN TRANSACTION
BEGIN TRY

DECLARE @MENUID int, @PARENTPAGE int

--=================================================================================================================

SELECT @MENUID=AC_MENUS.MENUID
FROM AC_MENUS
INNER JOIN AC_MODULES
ON AC_MENUS.MODULEID = AC_MODULES.MODULEID
WHERE AC_MENUS.DESCRIPTION = 'Search' AND AC_MODULES.DESCRIPTION = 'Property' 

SELECT @PARENTPAGE = AC_PAGES.PAGEID FROM AC_PAGES
INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
INNER JOIN AC_MODULES ON AC_MODULES.MODULEID = AC_MENUS.MODULEID
WHERE AC_PAGES.DESCRIPTION = 'Search' AND AC_MODULES.DESCRIPTION='Property' AND AC_PAGES.LINK = 1 

--==================================================================================================================

INSERT INTO [RSLBHALive].[dbo].[AC_PAGES]
           ([DESCRIPTION], [MENUID], [ACTIVE], [LINK], [PAGE], [ORDERTEXT], [AccessLevel], [ParentPage], [BridgeActualPage])
     VALUES
           ('Summary', @MENUID, 1, 1, '~/Controls/Property/SummaryControl.ascx', '1', 2, @PARENTPAGE, NULL)

----------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO [RSLBHALive].[dbo].[AC_PAGES]
           ([DESCRIPTION], [MENUID], [ACTIVE], [LINK], [PAGE], [ORDERTEXT], [AccessLevel], [ParentPage], [BridgeActualPage])
     VALUES
           ('Activities', @MENUID, 1, 1, '~/Controls/Property/Activities.ascx', '2', 2, @PARENTPAGE, NULL)
           
----------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO [RSLBHALive].[dbo].[AC_PAGES]
           ([DESCRIPTION], [MENUID], [ACTIVE], [LINK], [PAGE], [ORDERTEXT], [AccessLevel], [ParentPage], [BridgeActualPage])
     VALUES
           ('Attributes', @MENUID, 1, 1, '~/Controls/Property/Attributes.ascx', '3', 2, @PARENTPAGE, NULL)

-----------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO [RSLBHALive].[dbo].[AC_PAGES]
           ([DESCRIPTION], [MENUID], [ACTIVE], [LINK], [PAGE], [ORDERTEXT], [AccessLevel], [ParentPage], [BridgeActualPage])
     VALUES
           ('Documents', @MENUID, 1, 1, '~/Controls/Property/DocumentTab.ascx', '4', 2, @PARENTPAGE, NULL)   
           
---------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO [RSLBHALive].[dbo].[AC_PAGES]
           ([DESCRIPTION], [MENUID], [ACTIVE], [LINK], [PAGE], [ORDERTEXT], [AccessLevel], [ParentPage], [BridgeActualPage])
     VALUES
           ('Financial', @MENUID, 1, 1, '~/Controls/Property/FinancialTab.ascx', '5', 2, @PARENTPAGE, NULL)  
           
---------------------------------------------------------------------------------------------------------------------------------------


INSERT INTO [RSLBHALive].[dbo].[AC_PAGES]
           ([DESCRIPTION], [MENUID], [ACTIVE], [LINK], [PAGE], [ORDERTEXT], [AccessLevel], [ParentPage], [BridgeActualPage])
     VALUES
           ('Health & Safety', @MENUID, 1, 1, '~/Controls/Property/HealthSafetyTab.ascx', '6', 2, @PARENTPAGE, NULL)     
           
---------------------------------------------------------------------------------------------------------------------------------------- 


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END                              