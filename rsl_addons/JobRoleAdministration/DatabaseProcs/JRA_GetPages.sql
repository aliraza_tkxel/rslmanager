
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetPages @menuId=12,@teamJobRoleId = 616
-- Author:<Ahmed Mehmood>
-- Create date: <12/18/2013>
-- Description:	<Get all pages related to module>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetPages](
@teamJobRoleId int
,@menuId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT	PAGEID as PageId, DESCRIPTION as PageName, AccessLevel , ISNULL(ParentPage,-1) AS ParentPage, ORDERTEXT as SortOrder
    FROM	AC_PAGES 
    WHERE	AC_PAGES.MENUID = @menuId 
			AND AC_PAGES.ACTIVE = 1
			AND LINK = 1
    
    SELECT	AC_PAGES_ACCESS.PageId as PageId
    FROM	AC_PAGES_ACCESS
    WHERE	AC_PAGES_ACCESS.JobRoleTeamId = @teamJobRoleId
    	
	
END
