USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int

--DECLARE @alertIdList AS TEAMJOBROLE_ALERTS_ACCESSRIGHTS;
--INSERT INTO @alertIdList(JobRoleTeamId,AlertId)
--VALUES (1,1)

--EXEC	@return_value = [dbo].[JRA_SaveAlertsAccessRightsForTeam]
--		@alertIdList = @alertIdList,
--		@teamJobRoleId = 21

--SELECT	'Return Value' = @return_value
-- Author:<Ahmed Mehmood>
-- Create date: <Create Date,,26/12/2012>
-- Description:	<Save alerts access rights for team>
-- Web Page: Alerts.ascx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_SaveAlertsAccessRightsForTeam](
@teamJobRoleId int
,@userId int
,@alertIdList as TEAMJOBROLE_ALERTS_ACCESSRIGHTS readonly
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--====================================================
--			E_TEAMALERTS INSERTION
--====================================================

DELETE FROM E_TEAMALERTS
WHERE E_TEAMALERTS.JobRoleTeamId = @teamJobRoleId  

INSERT INTO E_TEAMALERTS( AlertId ,JobRoleTeamId )
SELECT AlertId,JobRoleTeamId 
FROM @alertIdList;

    ------------------------------------------------------
	-----Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
    Declare @jobRoleActionId int  

    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Access Rights Amended - Alerts'
                   
    INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           ,'Amended'
           ,@userId
           ,GETDATE())
---------------------------------------------------------------   
           
END
