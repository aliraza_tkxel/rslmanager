/* ===========================================================================
--  Author:			Ali raza
--  DATE CREATED:	08 January 2013
--  Description:	JobRoleId is added to identify the team and role of employee
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE E_JOBDETAILS
ADD JobRoleId int NULL 
