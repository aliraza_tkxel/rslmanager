USE [RSLBHALive]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[I_PAGE_TEAMJOBROLE](
	[PageTeamId] [int] IDENTITY(1,1) NOT NULL,
	[PageId] [int] NOT NULL,
	[TeamJobRoleId] [int] NOT NULL,
 CONSTRAINT [PK_I_PAGE_TEAMJOBROLE] PRIMARY KEY CLUSTERED 
(
	[PageTeamId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[I_PAGE_TEAMJOBROLE]  WITH CHECK ADD  CONSTRAINT [FK_E_TEAMJOBROLE_I_PAGE_TEAMJOBROLE] FOREIGN KEY([TeamJobRoleId])
REFERENCES [dbo].[E_JOBROLETEAM] ([JobRoleTeamId])
GO

ALTER TABLE [dbo].[I_PAGE_TEAMJOBROLE] CHECK CONSTRAINT [FK_E_TEAMJOBROLE_I_PAGE_TEAMJOBROLE]
GO

ALTER TABLE [dbo].[I_PAGE_TEAMJOBROLE]  WITH CHECK ADD  CONSTRAINT [FK_I_PAGE_I_PAGE_TEAMJOBROLE] FOREIGN KEY([PageId])
REFERENCES [dbo].[I_PAGE] ([PageID])
GO

ALTER TABLE [dbo].[I_PAGE_TEAMJOBROLE] CHECK CONSTRAINT [FK_I_PAGE_I_PAGE_TEAMJOBROLE]
GO


