USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetModules @teamJobRoleId = 616
-- Author:<Ahmed Mehmood>
-- Create date: <12/18/2013>
-- Description:	<Get all modules also team specific modules data>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetModules](
@teamJobRoleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT	AC_MODULES.MODULEID as ModuleId, AC_MODULES.DESCRIPTION as ModuleName
    FROM	AC_MODULES
    WHERE	AC_MODULES.ACTIVE = 1
			AND DISPLAY = 1 
    
    SELECT	AC_MODULES_ACCESS.ModuleId as ModuleId
    FROM	AC_MODULES_ACCESS
    WHERE	AC_MODULES_ACCESS.JobRoleTeamId = @teamJobRoleId

	
END
