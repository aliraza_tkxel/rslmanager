/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	11 June 2014
--  Description:	Insertion scripts for property related Menus, pages and subpages
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY     

DECLARE @PROPERTYID INT
DECLARE @SEARCH_MENUID INT,@SERVICING_MENUID INT,@REPAIRS_MENUID INT
		, @ASBESTOS_MENUID INT, @VOIDS_MENUID INT,@PLANNED_MENUID INT
		,@RESOURCES_MENUID INT
DECLARE @RESOURCES_Servicing_PAGEID INT,@REPORTS_Servicing_PAGEID INT
		,@REPAIRS_REPORTS_PAGEID INT,@PLANNED_LIFECYCLE_PAGEID INT
		,@PLANNED_SCHEDULING_PAGEID INT, @PLANNED_RESOURCES_PAGEID INT
		,@PLANNED_REPORTS_PAGEID INT,@REPORTS_FAULT_ADMIN INT
		,@SERVICING_CALENDAR_PAGEID INT ,@SERVICING_RESOURCES_LETTER_PAGEID INT
		,@PLANNED_RESOURCES_LETTER_PAGEID INT,@PLANNED_SCHEDULING_MISCWORK_PAGEID INT
		,@PROPERTY_SEARCH_PAGEID INT, @PLANNED_RESOURCES_STATUSANDACTION_PAGEID INT
		,@SERVICING_RESOURCES_STATUSANDACTION_PAGEID INT
		
		
--======================================================================================================
--												MODULES
--======================================================================================================


INSERT	INTO [AC_MODULES]
           ([DESCRIPTION],[ACTIVE],[DIRECTORY],[PAGE],[ALLACCESS],[DISPLAY],[IMAGE],[IMAGEOPEN],[IMAGEMOUSEOVER],[IMAGETAG],[IMAGETITLE],[ORDERTEXT])
VALUES
           ('Property',1,'PropertyModule','Bridge.aspx',0,1,NULL,NULL,NULL,NULL,NULL,NULL)
           
SET @PROPERTYID = SCOPE_IDENTITY()


--======================================================================================================
--												MENUS
--======================================================================================================

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Search',@PROPERTYID,1,'~/../PropertyModule/Bridge.aspx',100,170,'1')
SET		@SEARCH_MENUID = SCOPE_IDENTITY()

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Servicing',@PROPERTYID,1,'~/../RSLApplianceServicing/Bridge.aspx?cmd=rsl',100,170,'2')
SET		@SERVICING_MENUID = SCOPE_IDENTITY()

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Repairs',@PROPERTYID,1,'~/../RepairsDashboard/Bridge.aspx',100,170,'3')
SET		@REPAIRS_MENUID = SCOPE_IDENTITY()

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Asbestos',@PROPERTYID,1,'',100,170,'4')
SET		@ASBESTOS_MENUID = SCOPE_IDENTITY()

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Voids',@PROPERTYID,1,'',100,170,'5')
SET		@VOIDS_MENUID = SCOPE_IDENTITY()

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Planned',@PROPERTYID,1,'~/../PlannedMaintenance/Bridge.aspx',100,170,'6')
SET		@PLANNED_MENUID = SCOPE_IDENTITY()

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Resources',@PROPERTYID,1,'~/../RSLApplianceServicing/Bridge.aspx?resources=yes',100,170,'7')
SET		@RESOURCES_MENUID = SCOPE_IDENTITY()


--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================

--============
-- Search
--============
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Search',@SEARCH_MENUID,1,1,'~/../PropertyModule/Views/Search/SearchProperty.aspx','','1',1,NULL)
SET		@PROPERTY_SEARCH_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Access Denied',@SEARCH_MENUID,1,0,'~/../PropertyModule/Views/General/AccessDenied.aspx','','2',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Upload Document',@SEARCH_MENUID,1,0,'~/../RSLApplianceServicing/Views/Common/UploadDocument.aspx','','3',1,NULL)

--============
-- Servicing
--============
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Dashboard',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Dashboard/Dashboard.aspx','','1',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Resources',@SERVICING_MENUID,1,1,'','','2',1,NULL)
SET		@RESOURCES_Servicing_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Admin Calendar',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Scheduling/AdminScheduling.aspx','','3',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Scheduling',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx','','4',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Calendar',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Calendar/WeeklyCalendar.aspx','','5',1,NULL)
SET		@SERVICING_CALENDAR_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Reports',@SERVICING_MENUID,1,1,'','','6',1,NULL)
SET		@REPORTS_Servicing_PAGEID = SCOPE_IDENTITY()


INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Property Case',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Reports/PrintPropertyCase.aspx','','10',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Access Denied',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/AccessDenied.aspx','','12',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Download',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Common/Download.aspx','','13',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Not In Release',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Common/NotInRelease.aspx','','14',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print CP12 Document',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Common/PrintCP12Document.aspx','','15',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Letter',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Common/PrintLetter.aspx','','16',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Upload Document',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Common/UploadDocument.aspx','','17',1,NULL)

--============
-- Repairs
--============
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Dashboard',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx','~/../RepairsDashboard/Views/Dashboard/Dashboard.aspx','1',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Calendar',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?cal=yes','~/../BRSFaultLocator/Views/Faults/SchedulersCalanderSearch.aspx','2',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Reports',@REPAIRS_MENUID,1,1,'',NULL,'3',1,NULL)
SET		@REPAIRS_REPORTS_PAGEID = SCOPE_IDENTITY()


INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Completed Jobsheet',@REPAIRS_MENUID,1,0,'~/../RepairsDashboard/Views/JobSheets/CompletedJobsheet.aspx','','4',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Scheduling Calendar',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/FaultCalendar/SchedulingCalendar.aspx','','5',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Appointment Summary',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/AppointmentSummary.aspx','','6',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Available Appointments',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/AvailableAppointments.aspx','','7',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Cancellation Appointment Summary',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/CancellationAppointmentSummary.aspx','','8',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Fault Basket',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/FaultBasket.aspx','','9',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Job Sheet Detail',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/JobSheetDetail.aspx','','10',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Job Sheet Summary Subcontractor',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/JobSheetSummarySubcontractor.aspx','','11',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('More Details',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/MoreDetails.aspx','','12',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Other Appointments',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/OtherAppointments.aspx','','13',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Job Sheet Detail',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/PrintJobSheetDetail.aspx','','14',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Subcontractor Job Sheet Summary',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/PrintSubcontractorJobSheetSummary.aspx','','15',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print SubContractor Job Sheet Summary Update',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/PrintSubContractorJobSheetSummaryUpdate.aspx','','16',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('ReArranging Current Fault',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/ReArrangingCurrentFault.aspx','','17',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Recall Details',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/RecallDetails.aspx','','18',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Schedular Search',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Faults/SchedularSearch.aspx','','19',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Search Fault',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Views/Search/SearchFault.aspx','','20',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Access Denied',@REPAIRS_MENUID,1,0,'~/../BRSFaultLocator/Bridge.aspx?accessDenied=yes','~/../BRSFaultLocator/AccessDenied.aspx','21',1,NULL)

--============
-- Planned
--============
		
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Dashboard',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Dashboard/Dashboard.aspx','','1',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Lifecycles',@PLANNED_MENUID,1,1,'','','2',1,NULL)
SET @PLANNED_LIFECYCLE_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Scheduling',@PLANNED_MENUID,1,1,'','','3',1,NULL)
SET @PLANNED_SCHEDULING_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Resources',@PLANNED_MENUID,1,1,'','','4',1,NULL)
SET @PLANNED_RESOURCES_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Reports',@PLANNED_MENUID,1,1,'','','5',1,NULL)
SET @PLANNED_REPORTS_PAGEID = SCOPE_IDENTITY()


INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Letter',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Common/PrintLetter.aspx','','6',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Upload Document',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Common/UploadDocument.aspx','','7',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('No Entry Report',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Reports/NoEntryReport.aspx','','8',1,NULL)

--============
-- Resources
--============

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Users',@RESOURCES_MENUID,1,1,'~/../RSLApplianceServicing/Views/Resources/Resources.aspx','','1',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Access',@RESOURCES_MENUID,1,1,'~/../RSLApplianceServicing/Views/Resources/Access.aspx','','2',1,NULL)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Upload Document',@RESOURCES_MENUID,1,0,'~/../PlannedMaintenance/Views/Common/UploadDocument.aspx','','3',1,NULL)

--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================

--======================
-- Search > Search
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print JobSheet Detail',@SEARCH_MENUID,1,0,'~/../RSLApplianceServicing/Views/Property/PrintJobSheetDetail.aspx','','1',2,@PROPERTY_SEARCH_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Property Record',@SEARCH_MENUID,1,0,'~/../RSLApplianceServicing/Views/Property/PropertyRecord.aspx','','2',2,@PROPERTY_SEARCH_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Activities',@SEARCH_MENUID,1,0,'~/../RSLApplianceServicing/Views/Property/PrintActivities.aspx','','3',2,@PROPERTY_SEARCH_PAGEID)


--======================
-- Servicing > Calendar
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Calendar',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Calendar/Calendar.aspx','','1',2,@SERVICING_CALENDAR_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Calendar Search',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Calendar/CalendarSearch.aspx','','2',2,@SERVICING_CALENDAR_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Monthly Calendar',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Calendar/MonthlyCalendar.aspx','','3',2,@SERVICING_CALENDAR_PAGEID)

--======================
-- Servicing > Resources
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Status and Actions',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Resources/StatusActionMain.aspx','','1',2,@RESOURCES_Servicing_PAGEID)
SET @SERVICING_RESOURCES_STATUSANDACTION_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Letters',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Resources/ViewLetters.aspx','','2',2,@RESOURCES_Servicing_PAGEID)
SET @SERVICING_RESOURCES_LETTER_PAGEID = SCOPE_IDENTITY()

--======================
-- Servicing > Reports
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Certificate Expiry',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Reports/CertificateExpiry/CertificateExpiry.aspx','','1',2,@REPORTS_Servicing_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Issued Certificates',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Reports/IssuedCertificates/IssuedCertificates.aspx','','2',2,@REPORTS_Servicing_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Print Certificates',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Views/Reports/PrintCertificates/PrintCertificates.aspx','','3',2,@REPORTS_Servicing_PAGEID)

--======================
-- Repairs > Reports
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Reported Faults',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx?rpt=rf','~/../RepairsDashboard/Views/Reports/ReportsArea.aspx?rpt=rf','1',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('No Entry',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=ne','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=ne','2',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Follow On Works',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=fow','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=fow','3',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('In Progress Faults',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx?rpt=ip','~/../RepairsDashboard/Views/Reports/ReportsArea.aspx?rpt=ip','4',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Overdue Faults',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx?rpt=od','~/../RepairsDashboard/Views/Reports/ReportsArea.aspx?rpt=od','5',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Emergency Faults',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx?rpt=em','~/../RepairsDashboard/Views/Reports/ReportsArea.aspx?rpt=em','6',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Completed Faults',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx?rpt=cmp','~/../RepairsDashboard/Views/Reports/ReportsArea.aspx?rpt=cmp','7',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Assigned To Contractor',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=atc','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=atc','8',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Recalls',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=rc','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=rc','9',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Paused Fault',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=pf','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=pf','10',2,@REPAIRS_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Fault Locator Admin',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=fm','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=fm','11',2,@REPAIRS_REPORTS_PAGEID)
SET @REPORTS_FAULT_ADMIN = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Priority & Completion',@REPAIRS_MENUID,1,1,'~/../RepairsDashboard/Bridge.aspx?rpt=pc','~/../RepairsDashboard/Views/Reports/ReportsArea.aspx?rpt=pc','12',2,@REPAIRS_REPORTS_PAGEID)

--======================
-- Planned > Lifecycle
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Components',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Lifecycle/Components.aspx','','1',2,@PLANNED_LIFECYCLE_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Replacement List',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Lifecycle/ReplacementList.aspx','','2',2,@PLANNED_LIFECYCLE_PAGEID)
		
--======================
-- Planned > Scheduling
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Schedule Works',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Scheduling/ScheduleWorks.aspx','','1',2,@PLANNED_SCHEDULING_PAGEID)
	
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Misc Works',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Scheduling/MiscWorks.aspx','','2',2,@PLANNED_SCHEDULING_PAGEID)
SET @PLANNED_SCHEDULING_MISCWORK_PAGEID = SCOPE_IDENTITY()	
	
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Appointment Summary',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/AppointmentSummary.aspx','','3',2,@PLANNED_SCHEDULING_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Arranged Available Appointments',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ArrangedAvailableAppointments.aspx','','4',2,@PLANNED_SCHEDULING_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('To Be Arranged Appointment Summary',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ToBeArrangedAppointmentSummary.aspx','','5',2,@PLANNED_SCHEDULING_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('To Be Arranged Available Appointments',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ToBeArrangedAvailableAppointments.aspx','','6',2,@PLANNED_SCHEDULING_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('View Arranged Appointments',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/ViewArrangedAppointments.aspx','','7',2,@PLANNED_SCHEDULING_PAGEID)

--======================
-- Planned > Resources
--======================
	
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Status & Actions',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Resources/StatusActionMain.aspx','','1',2,@PLANNED_RESOURCES_PAGEID)
SET @PLANNED_RESOURCES_STATUSANDACTION_PAGEID = SCOPE_IDENTITY()
		
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Letters',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Resources/ViewLetters.aspx','','2',2,@PLANNED_RESOURCES_PAGEID)
SET		@PLANNED_RESOURCES_LETTER_PAGEID = SCOPE_IDENTITY()
 	
--======================
-- Planned > Reports
--======================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Component Cost Forecast',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/AnnualSpendPlanner.aspx','','1',2,@PLANNED_REPORTS_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Spend Report',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/SpendReport.aspx','','2',2,@PLANNED_REPORTS_PAGEID)
	
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Component Acc�nting',@PLANNED_MENUID,1,1,'~/../PlannedMaintenance/Views/Reports/ComponentAccounting.aspx','','3',2,@PLANNED_REPORTS_PAGEID)


--======================================================================================================
--												PAGES	(LEVEL 3)
--======================================================================================================

--============================================
-- Repairs > Reports > Fault Locator Admin
--============================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Fault Management',@REPAIRS_MENUID,1,1,'~/../BRSFaultLocator/Bridge.aspx?report=yes&rd=fm','~/../BRSFaultLocator/Views/Reports/ReportsArea.aspx?rd=fm','1',3,@REPORTS_FAULT_ADMIN)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Price Control',@REPAIRS_MENUID,1,1,'/faultlocator/faultmanagement/fault_manager.aspx?option=pricing','','2',3,@REPORTS_FAULT_ADMIN)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Fault Report Management',@REPAIRS_MENUID,1,1,'/faultlocator/secure/faultlocator/repair_list_management.aspx','','3',3,@REPORTS_FAULT_ADMIN)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Priority Settings',@REPAIRS_MENUID,1,1,'/faultlocator/faultmanagement/fault_manager.aspx?option=priority','','4',3,@REPORTS_FAULT_ADMIN)

--============================================
-- Servicing > Resources > Letters
--============================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Create Letter',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/CreateLetter.aspx','','1',3,@SERVICING_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Edit Letter',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/EditLetter.aspx','','2',3,@SERVICING_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Letters',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/Letters.aspx','','3',3,@SERVICING_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Preview Letter',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/PreviewLetter.aspx','','4',3,@SERVICING_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Save Letter As Pdf',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/SaveLetterAsPdf.aspx','','5',3,@SERVICING_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('View Letter',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/ViewLetter.aspx','','6',3,@SERVICING_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('View Letters',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/ViewLetters.aspx','','7',3,@SERVICING_RESOURCES_LETTER_PAGEID)

--============================================
-- Planned > Resources > Letters
--============================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Create Letter',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Resources/CreateLetter.aspx','','1',3,@PLANNED_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Edit Letter',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Resources/EditLetter.aspx','','2',3,@PLANNED_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Preview Letter',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Resources/PreviewLetter.aspx','','3',3,@PLANNED_RESOURCES_LETTER_PAGEID)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('View Letters',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Resources/ViewLetters.aspx','','4',3,@PLANNED_RESOURCES_LETTER_PAGEID)

--============================================
-- Planned > Scheduling > Misc Works
--============================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Misc Job Sheet',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Scheduling/MiscJobSheet.aspx','','1',3,@PLANNED_SCHEDULING_MISCWORK_PAGEID)

--============================================
-- Planned > Resources > Status & Actions
--============================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Status',@PLANNED_MENUID,1,0,'~/../PlannedMaintenance/Views/Resources/Status.aspx','','1',3,@PLANNED_RESOURCES_STATUSANDACTION_PAGEID)

--============================================
-- Servicing > Resources > Status & Actions
--============================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[BridgeActualPage],[ORDERTEXT],[AccessLevel],[ParentPage])
VALUES	('Status',@SERVICING_MENUID,1,0,'~/../RSLApplianceServicing/Views/Resources/Status.aspx','','1',3,@SERVICING_RESOURCES_STATUSANDACTION_PAGEID)


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 