-- =============================================    
--EXEC [dbo].[JRA_SearchJobRoleHistoryList]    
--  @empId = 1
--  @pageSize = 2,    
--  @pageNumber = 1,    
--  @sortColumn = N'Recorded',    
--  @sortOrder = N'DESC',    
--  @totalCount = @totalCount OUTPUT     
     
-- Author:  <Ali Raza>    
-- Create date: <06/01/2014>    
-- Description: <Search Job Role History List>    
-- Web Page: JobRoleHistory.aspx    
-- =============================================    
CREATE PROCEDURE [dbo].[JRA_SearchJobRoleHistoryList]     
(     
 -- Add the parameters for the stored procedure here  
   
  @empId int    
  --Parameters which would help in sorting and paging    
  ,@pageSize int = 30,    
  @pageNumber int = 1,    
  @sortColumn varchar(50) = 'JobRoleName',    
  @sortOrder varchar (5) = 'DESC',    
  @totalCount int = 0 output    
)    
AS    
BEGIN    
 DECLARE     
  @SelectClause varchar(2000),    
        @fromClause   varchar(1500),    
        @whereClause  varchar(1500),             
        @orderClause  varchar(100),     
        @mainSelectQuery varchar(5500),            
        @rowNumberQuery varchar(6000),    
        @finalQuery varchar(6500),    
        -- used to add in conditions in WhereClause based on search criteria provided    
        @searchCriteria varchar(1500),    
            
        --variables for paging    
        @offset int,    
  @limit int    
      
  --Paging Formula    
  SET @offset = 1+(@pageNumber-1) * @pageSize    
  SET @limit = (@offset + @pageSize)-1    
      
  --========================================================================================    
  -- Begin building SearchCriteria clause    
  -- These conditions will be added into where clause based on search criteria provided    
      
  SET @searchCriteria = ' 1=1 '    
      
     
      
  IF @empId != -1    
   SET @searchCriteria = @searchCriteria + CHAR(10) +' AND E_EMPLOYEEJOBROLEHISTORY.EMPLOYEEID = '+CONVERT(nvarchar(10), @empId)          
      
  -- End building SearchCriteria clause       
  --========================================================================================    
      
  SET NOCOUNT ON;    
  --========================================================================================             
  -- Begin building SELECT clause    
  -- Insert statements for procedure here    
      
  SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')     
TEAMNAME as TeamName ,JobeRoleDescription as JobRoleName
,(SUBSTRING(cre.FirstName, 1, 1) + cre.LASTNAME) as CreatedBy  
 ,(CONVERT(VARCHAR(50), CreatedDate, 103))as CreatedDate '    
  -- End building SELECT clause    
  --========================================================================================            
      
      
  --========================================================================================        
  -- Begin building FROM clause    
  SET @fromClause =   CHAR(10) +' FROM E_EMPLOYEEJOBROLEHISTORY  
--INNER JOIN E__EMPLOYEE emp on emp.JobRoleTeamId = E_EMPLOYEEJOBROLEHISTORY.JobRoleTeamId
INNER JOIN E_JOBROLETEAM on E_JOBROLETEAM.JobRoleTeamId  = E_EMPLOYEEJOBROLEHISTORY.JobRoleTeamId  
INNER JOIN E_JOBROLE on E_JOBROLE.JobRoleId = E_JOBROLETEAM.JobRoleId      
INNER Join E_TEAM on E_TEAM.TEAMID = E_JOBROLETEAM.TeamId   
INNER JOIN E__EMPLOYEE cre on cre.EMPLOYEEID  = E_EMPLOYEEJOBROLEHISTORY.CreatedBy '    
  -- End building From clause    
  --========================================================================================                     
      
      
      
  --========================================================================================        
  -- Begin building OrderBy clause      
      
  -- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias    
      
  SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder    
      
  -- End building OrderBy clause    
  --========================================================================================            
      
  --========================================================================================    
  -- Begin building WHERE clause    
         
  -- This Where clause contains subquery to exclude already displayed records         
      
  SET @whereClause = CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria     
      
  -- End building WHERE clause    
  --========================================================================================    
      
  --========================================================================================    
  -- Begin building the main select Query    
      
  Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause     
      
  -- End building the main select Query    
  --========================================================================================                                       
    
  --========================================================================================    
  -- Begin building the row number query    
      
  Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row     
        FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'    
      
  -- End building the row number query    
  --========================================================================================    
      
  --========================================================================================    
  -- Begin building the final query     
      
  Set @finalQuery  =' SELECT *    
       FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result     
       WHERE    
       Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)        
      
  -- End building the final query    
  --========================================================================================             
     
  --========================================================================================    
  -- Begin - Execute the Query     
  print(@finalQuery)    
  EXEC (@finalQuery)                             
  -- End - Execute the Query     
  --========================================================================================             
      
  --========================================================================================    
  -- Begin building Count Query     
      
  Declare @selectCount nvarchar(2000),     
  @parameterDef NVARCHAR(500)    
      
  SET @parameterDef = '@totalCount int OUTPUT';    
  SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause    
      
  --print @selectCount    
  EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;    
        
  -- End building the Count Query    
  --========================================================================================           
END    
    
    