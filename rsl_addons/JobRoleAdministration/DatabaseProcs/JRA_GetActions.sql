USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[JRA_GetActions]    Script Date: 01/08/2014 19:14:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetActions
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,12/24/2013,>
-- Description:	<Description,,for the dropdown list on the Job ROle Audit Report>
-- WebPage: JobRoleAuditReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetActions]
AS
BEGIN
	SELECT * from E_JOBROLEACTIONS
END
