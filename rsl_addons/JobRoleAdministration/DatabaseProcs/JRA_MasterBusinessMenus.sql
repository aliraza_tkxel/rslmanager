-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE JRA_MasterBusinessMenus(
@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT AC_MODULES.DESCRIPTION,AC_MODULES.MODULEID,'/' + DIRECTORY + '/' + PAGE AS THEPATH FROM AC_MODULES
INNER JOIN AC_MODULES_ACCESS on AC_MODULES_ACCESS.ModuleId = AC_MODULES.MODULEID
INNER JOIN E__EMPLOYEE on E__EMPLOYEE.JobRoleTeamId = AC_MODULES_ACCESS.JobRoleTeamId
	Where E__EMPLOYEE.EMPLOYEEID = @employeeId And AC_MODULES.MODULEID != 1 AND AC_MODULES.ACTIVE = 1

	ORDER BY ORDERTEXT ASC

END
GO
