USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_SavePageAccessRights @teamId=1,@jobRole = 'Administrator'
-- Author:		Ahmed Mehmood
-- Create date: <19/12/2013>
-- Last Modified: <19/12/2013>
-- Description:	<Save Team Job Role. >
-- Web Page: JobRoles.aspx

-- =============================================
CREATE PROCEDURE [dbo].[JRA_SavePageAccessRights]
@teamJobRoleId int
,@pageId int
,@isSaved bit out
AS
BEGIN

DECLARE @menuId int,@moduleId int


BEGIN TRANSACTION;
BEGIN TRY

--====================================================
--			AC_PAGES_ACCESS INSERTION
--====================================================

	INSERT INTO AC_PAGES_ACCESS
           ([PageId]
           ,[JobRoleTeamId])
    VALUES
           (@pageId
           ,@teamJobRoleId)

--====================================================
--			AC_MENUS_ACCESS INSERTION
--====================================================


	SELECT	@menuId = AC_PAGES.MENUID 
	FROM	AC_PAGES 
	WHERE	AC_PAGES.PAGEID = @pageId

	
	IF NOT EXISTS (	SELECT	1
	FROM	AC_MENUS_ACCESS
	WHERE	AC_MENUS_ACCESS.MenuId = @menuId AND AC_MENUS_ACCESS.JobRoleTeamId = @teamJobRoleId)
	BEGIN
		
		INSERT INTO AC_MENUS_ACCESS
           ([MenuId]
           ,[JobRoleTeamId])
		VALUES
           (@menuId
           ,@teamJobRoleId)
           
	END


--====================================================
--			AC_MODULES_ACCESS INSERTION
--====================================================

	SELECT	@moduleId = AC_MENUS.MODULEID  
	FROM	AC_MENUS 
	WHERE	AC_MENUS.MENUID = @menuId

	
	IF NOT EXISTS (	SELECT	1
	FROM	AC_MODULES_ACCESS
	WHERE	AC_MODULES_ACCESS.ModuleId = @moduleId AND AC_MODULES_ACCESS.JobRoleTeamId = @teamJobRoleId)
	BEGIN
		
		INSERT INTO AC_MODULES_ACCESS
           ([ModuleId]
           ,[JobRoleTeamId])
		VALUES
           (@moduleId
           ,@teamJobRoleId)
           
	END
      

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END