USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetIntranetMainMenus @teamJobRoleId = 616
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Get intranet main menus>
-- Web Page: JobRoles.aspx
-- =============================================
CREATE PROCEDURE [dbo].[JRA_GetIntranetMainMenus](
@teamJobRoleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    --============================================================
    --					MAIN MENUS
    --============================================================
    
    SELECT	I_MENU.MenuID AS MenuId, I_MENU.MenuTitle AS MenuName
    FROM	I_MENU 
    WHERE	I_MENU.ACTIVE = 1 and ParentID IS NULL
       
	
END
