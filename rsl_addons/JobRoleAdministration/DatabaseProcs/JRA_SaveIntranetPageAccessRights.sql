USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit
		
--DECLARE @pageIdList AS TEAMJOBROLE_INTRANET_PAGE_ACCESSRIGHTS;
--INSERT INTO @pageIdList(JobRoleTeamId,PageId)
--VALUES (1,1)

--EXEC	@return_value = [dbo].[JRA_SaveIntranetPageAccessRights]
--		@teamJobRoleId = 21,
--		@userId =615,
--		@pageIdList = @pageIdList,
--		@isSaved = @isSaved OUTPUT

--SELECT	@isSaved as N'@isSaved'

--SELECT	'Return Value' = @return_value
-- Author:		Ahmed Mehmood
-- Create date: <3/1/2014>
-- Last Modified: <3/1/2014>
-- Description:	<Save Intranet Page Access Rights. >
-- Web Page: JobRoles.aspx

-- =============================================
ALTER PROCEDURE [dbo].[JRA_SaveIntranetPageAccessRights]
@teamJobRoleId int
,@userId int
,@pageIdList as TEAMJOBROLE_INTRANET_PAGE_ACCESSRIGHTS readonly
,@isSaved bit out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

--====================================================
--			AC_PAGES_ACCESS INSERTION
--====================================================

    DELETE FROM I_PAGE_TEAMJOBROLE
	WHERE I_PAGE_TEAMJOBROLE.TeamJobRoleId = @teamJobRoleId     
           
           
    INSERT INTO I_PAGE_TEAMJOBROLE(PageId,TeamJobRoleId)
	SELECT PageId,JobRoleTeamId 
	FROM @pageIdList;

    ------------------------------------------------------
	-- Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
    Declare @jobRoleActionId int  

    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Access Rights Amended - Intranet'
                  
                         
    INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           ,'Amended'
           ,@userId
           ,GETDATE())
           
---------------------------------------------------------------

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END