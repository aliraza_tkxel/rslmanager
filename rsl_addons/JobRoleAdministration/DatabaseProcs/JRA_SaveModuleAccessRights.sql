USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--DECLARE	@return_value int,
--		@isSaved bit

--DECLARE @moduleIdList AS TEAMJOBROLE_MODULES_ACCESSRIGHTS;
--INSERT INTO @moduleIdList(JobRoleTeamId,ModuleId)
--VALUES (1,1)

--DECLARE @menuIdList AS TEAMJOBROLE_MENU_ACCESSRIGHTS;
--INSERT INTO @menuIdList(JobRoleTeamId,MenuId)
--VALUES (1,1)

--DECLARE @pageIdList AS TEAMJOBROLE_PAGE_ACCESSRIGHTS;
--INSERT INTO @pageIdList(JobRoleTeamId,PageId)
--VALUES (1,1)


--EXEC	@return_value = [dbo].[JRA_SaveModuleAccessRights]
--		@teamJobRoleId = 21,
--		@userId = 615,
--		@moduleIdList = @moduleIdList,
--		@menuIdList=@menuIdList,
--		@pageIdList=@pageIdList,
--		@isSaved = @isSaved OUTPUT

--SELECT	@isSaved as N'@isSaved'
--SELECT	'Return Value' = @return_value

-- Author:		Ahmed Mehmood
-- Create date: <2/1/2014>
-- Last Modified: <2/1/2014>
-- Description:	<Save Team Job Role. >
-- Web Page: JobRoles.aspx

-- =============================================
ALTER PROCEDURE [dbo].[JRA_SaveModuleAccessRights]
@teamJobRoleId int
,@userId int
,@moduleIdList as TEAMJOBROLE_MODULES_ACCESSRIGHTS readonly
,@menuIdList as TEAMJOBROLE_MENU_ACCESSRIGHTS readonly
,@pageIdList as TEAMJOBROLE_PAGE_ACCESSRIGHTS readonly
,@isSaved bit out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY


--====================================================
--			AC_MODULES_ACCESS INSERTION
--====================================================

DELETE FROM AC_MODULES_ACCESS
WHERE AC_MODULES_ACCESS.JobRoleTeamId = @teamJobRoleId  

INSERT INTO AC_MODULES_ACCESS(ModuleId ,JobRoleTeamId )
SELECT ModuleId,JobRoleTeamId 
FROM @moduleIdList;

--====================================================
--			AC_MENUS_ACCESS INSERTION
--====================================================

DELETE FROM AC_MENUS_ACCESS
WHERE AC_MENUS_ACCESS.JobRoleTeamId = @teamJobRoleId  

INSERT INTO AC_MENUS_ACCESS(MenuId ,JobRoleTeamId )
SELECT MenuId,JobRoleTeamId 
FROM @menuIdList;

--====================================================
--			AC_PAGES_ACCESS INSERTION
--====================================================

DELETE FROM AC_PAGES_ACCESS
WHERE AC_PAGES_ACCESS.JobRoleTeamId = @teamJobRoleId  

INSERT INTO AC_PAGES_ACCESS(PageId,JobRoleTeamId )
SELECT PageId,JobRoleTeamId 
FROM @pageIdList;
           
    ------------------------------------------------------
	-----Insertion in JobRoleAuditHistory Table
	------------------------------------------------------
    Declare @jobRoleActionId int  

    SELECT	@jobRoleActionId = E_JOBROLEACTIONS.JobRoleActionId 
    FROM	E_JOBROLEACTIONS
    WHERE	E_JOBROLEACTIONS.ActionTitle = 'Access Rights Amended - Modules'
          
                 
    INSERT INTO E_JOBROLEAUDITHISTORY
           ([JobRoleActionId]
           ,[JobRoleTeamId]
           ,[Detail]
           ,[CreatedBy]
           ,[CreatedDate])
     VALUES
           (@jobRoleActionId
           ,@teamJobRoleId
           ,'Amended'
           ,@userId
           ,GETDATE())
---------------------------------------------------------------
      

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	

END