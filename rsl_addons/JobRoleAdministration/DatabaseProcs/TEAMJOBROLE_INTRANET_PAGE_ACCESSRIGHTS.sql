USE [RSLBHALive]
GO 
--Created By : Ahmed Mehmood
--Description:This is table valued parameter is used for insertion of pageId of Team Job Role
--Web Page: JobRoles.aspx

CREATE TYPE [dbo].[TEAMJOBROLE_INTRANET_PAGE_ACCESSRIGHTS] AS TABLE(
	[JobRoleTeamId] [int] NULL,
	[PageId] [int] NULL
)
GO