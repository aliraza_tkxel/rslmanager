/* ===========================================================================
--  Author:			Noor Muhammad
--  DATE CREATED:	17 December 2013
--  Description:	JobRoleTeamId is added to identify the team and role of employee
--					
 '==============================================================================*/

USE [RSLBHALive]
GO


ALTER TABLE E__Employee
ADD JobRoleTeamId int NULL 
