USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[JRA_GetAlertsList]    Script Date: 12/18/2013 18:28:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetAlertsList @teamJobRoleId = 616
-- Author:<Ahmed Mehmood>
-- Create date: <12/18/2013>
-- Description:	<Get all alerts list>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetAlertsList](
@teamJobRoleId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    SELECT	E_ALERTS.AlertID AS AlertId
			,E_ALERTS.AlertName AS AlertName
			,ISNULL(TEAMALERTS.TeamAlertId ,-1) AS IsChecked
    FROM	E_ALERTS 
			LEFT JOIN (	SELECT	E_TEAMALERTS.TeamAlertId as TeamAlertId
								,E_TEAMALERTS.AlertId as AlertId
						FROM	E_TEAMALERTS
						WHERE	E_TEAMALERTS.JobRoleTeamId = @teamJobRoleId )AS TEAMALERTS ON  E_ALERTS.AlertID = TEAMALERTS.AlertId
	ORDER BY AlertName
    	
	
END


