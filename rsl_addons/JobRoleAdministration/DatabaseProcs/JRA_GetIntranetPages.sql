USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_GetIntranetPages @teamJobRoleId = 616, @submenuId = 10
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Get intranet pages>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_GetIntranetPages](
@teamJobRoleId int
,@submenuId int
) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
     --============================================================
    --					MENU PAGES
    --============================================================
    
    SELECT	I_PAGE.PageID AS PageId,I_PAGE.Title as PageName
    FROM	I_PAGE 
    WHERE	I_PAGE.Active =1 AND I_PAGE.MenuID = @submenuId
    
    --============================================================
    --					TEAM INTRANET PAGES
    --============================================================
    
    SELECT	I_PAGE_TEAMJOBROLE.PageId AS PageId
    FROM	I_PAGE_TEAMJOBROLE			
    WHERE	I_PAGE_TEAMJOBROLE.TeamJobRoleId = @teamJobRoleId
			
    
	
END
