USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC JRA_SearchFullJobRoleList @searchedText = '' , @teamId =12
-- Author:<Ahmed Mehmood>
-- Create date: <19/12/2013>
-- Description:	<Get full job role list.>
-- Web Page: JobRoles.aspx
-- =============================================
ALTER PROCEDURE [dbo].[JRA_SearchFullJobRoleList]
( 
		@searchedText varchar(200)
		,@teamId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    DECLARE 
		@SelectClause varchar(2000),
        @fromClause   varchar(1500),
        @whereClause  varchar(1500),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(5500),                
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1500)
        
		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 AND E_JOBROLETEAM.isDeleted = 0 '
		
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND E_JOBROLE.JobeRoleDescription LIKE ''%' + @searchedText + '%'''
		END	
		
		IF @teamId != -1
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND E_TEAM.TEAMID = '+CONVERT(nvarchar(10), @teamId) 	 	 	
		
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @selectClause = 'SELECT  
		E_JOBROLETEAM.JobRoleTeamId AS TeamJobRoleId
		,E_JOBROLE.JobRoleId AS JobRoleId
		,E_JOBROLE.JobeRoleDescription  AS JobRoleName
		,E_TEAM.TEAMID AS TeamId
		,E_TEAM.TEAMNAME AS TeamName
		,ISNULL(TEAMCOUNT.TeamCount,0) AS UsersCount'
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		SET @fromClause =	  CHAR(10) +' FROM	E_JOBROLETEAM
		INNER JOIN E_JOBROLE ON  E_JOBROLETEAM.JobRoleId = E_JOBROLE.JobRoleId 
		INNER JOIN E_TEAM ON E_JOBROLETEAM.TeamId = E_TEAM.TEAMID 
		LEFT JOIN (SELECT	E_JOBROLETEAM.JobRoleTeamId AS  JobRoleTeamId
							,COUNT(E__EMPLOYEE.JobRoleTeamId) AS TeamCount
					FROM	E_JOBROLETEAM
							LEFT JOIN E__EMPLOYEE ON E_JOBROLETEAM.JobRoleTeamId = E__EMPLOYEE.JobRoleTeamId
							INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
					WHERE	E_JOBDETAILS.ACTIVE =1
					GROUP BY E_JOBROLETEAM.JobRoleTeamId 
						) AS TEAMCOUNT ON  E_JOBROLETEAM.JobRoleTeamId = TEAMCOUNT.JobRoleTeamId '
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause		
		
		-- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias
		
	
		
		-- End building OrderBy clause
		--========================================================================================								
		
		--========================================================================================
		-- Begin building WHERE clause
	    
		-- This Where clause contains subquery to exclude already displayed records			  
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause 
		print(@mainSelectQuery)
		EXEC (@mainSelectQuery)	
	
END
