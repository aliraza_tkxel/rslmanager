-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date, 4/1/2014>
-- Description:	<Description, Return Alerts of Employee>
-- =============================================
ALTER PROCEDURE [dbo].[I_ALERTS_SELECT_BY_EMP]	
(
	@EmpId	int
)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
 --SELECT     E_EMPLOYEEALERTS.ALERTID, E_ALERTS.AlertName, E_ALERTS.AlertUrl, E_ALERTS.SOrder  
 --FROM       E_EMPLOYEEALERTS  
 --INNER JOIN E_ALERTS ON E_EMPLOYEEALERTS.ALERTID = E_ALERTS.AlertID  
   
 --WHERE      (E_EMPLOYEEALERTS.EMPLOYEEID = @EmpId)  
 --ORDER BY   E_ALERTS.SOrder 
 
 
 SELECT     E_TEAMALERTS.ALERTID, E_ALERTS.AlertName, E_ALERTS.AlertUrl, E_ALERTS.SOrder  
 FROM	E__EMPLOYEE 
		INNER JOIN E_TEAMALERTS ON E__EMPLOYEE.JobRoleTeamId = E_TEAMALERTS.JobRoleTeamId 
		INNER JOIN E_ALERTS ON E_TEAMALERTS.AlertId = E_ALERTS.AlertID 
 WHERE      (E__EMPLOYEE.EMPLOYEEID = @EmpId)  
 ORDER BY   E_ALERTS.SOrder 
		
 
 
END

