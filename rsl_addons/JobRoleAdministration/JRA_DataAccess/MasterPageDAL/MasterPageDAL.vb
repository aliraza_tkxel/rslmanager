﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports JRA_Utilities
Imports JRA_BusinessObject

Namespace JRA_DataAccess
    Public Class DashboardDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Business Menus"
        ''' <summary>
        ''' Get Business Menus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getBusinessMenus(ByRef businessMenuDataset As DataSet, ByVal employeeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(businessMenuDataset, parametersList, SpNameConstants.GetBusinessMenus)


        End Sub
#End Region

#Region "Get Pages "
        ''' <summary>
        ''' Get Pages for left Menu
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getRSLPage(ByRef pageMenuDatset As DataSet, ByVal employeeId As Integer, ByVal selectedMenuId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim dtPageMenus As DataTable = New DataTable()
            dtPageMenus.TableName = ApplicationConstants.PageMenusDt
            pageMenuDatset.Tables.Add(dtPageMenus)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim menuIdParam As ParameterBO = New ParameterBO("menuId", selectedMenuId, DbType.Int32)
            parametersList.Add(menuIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetMenuPages)
            pageMenuDatset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtPageMenus)
        End Sub
#End Region

#Region "RSL Menus"

        Sub getRSLMenus(ByRef resultDataset As DataSet, ByVal employeeId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            MyBase.LoadDataSet(resultDataset, parametersList, SpNameConstants.GetModuleMenus)

        End Sub

#End Region

#Region "Check employee page access"
        ''' <summary>
        ''' Check employee page access
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Sub checkEmployeePageAccess(ByRef resultDataset As DataSet, ByVal employeeId As Integer, ByVal pageName As String)

            Dim parametersList As ParameterList = New ParameterList()

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim pageNameParam As ParameterBO = New ParameterBO("pageName", pageName, DbType.String)
            parametersList.Add(pageNameParam)

            MyBase.LoadDataSet(resultDataset, parametersList, SpNameConstants.CheckEmployeePageAccess)

        End Sub

#End Region

#End Region


    End Class

End Namespace