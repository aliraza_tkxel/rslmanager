﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports JRA_Utilities
Imports JRA_BusinessObject


Namespace JRA_DataAccess
    Public Class ReportDAL : Inherits BaseDAL

#Region "Get job Actions"
        Sub getjobRoles(ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim ActionsDt As DataTable = New DataTable()
            ActionsDt.TableName = ApplicationConstants.ActionsDt
            resultDataSet.Tables.Add(ActionsDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJRAActions)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, ActionsDt)
        End Sub
#End Region

#Region "Get Users"
        Sub getUsers(ByRef resultDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim UsersDt As DataTable = New DataTable()
            UsersDt.TableName = ApplicationConstants.UsersDt
            resultDataSet.Tables.Add(UsersDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJRAUsers)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, UsersDt)
        End Sub
#End Region

#Region "Get Report Data"
        Public Function getReportData(ByVal userID As Integer, ByVal actionID As Integer, ByVal selectedDate As String, ByRef reportDataSet As DataSet, ByVal objPageSortBo As PageSortBO, ByVal searchText As String, ByVal allowPaging As Boolean)
            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            Dim userIDParam As ParameterBO = New ParameterBO("userId", userID, DbType.Int32)
            parametersList.Add(userIDParam)

            Dim actionIDParam As ParameterBO = New ParameterBO("actionId", actionID, DbType.Int32)
            parametersList.Add(actionIDParam)

            Dim selectedDateParam As ParameterBO = New ParameterBO("selectedDate", selectedDate, DbType.String)
            parametersList.Add(selectedDateParam)

            Dim pageSizeParam As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSizeParam)

            Dim pageNumberParam As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumberParam)

            Dim sortColumnParam As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumnParam)

            Dim sortOrderParam As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrderParam)

            Dim allowPagingParam As ParameterBO = New ParameterBO("allowPaging", allowPaging, DbType.Boolean)
            parametersList.Add(allowPagingParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParamList.Add(totalCountParam)

            'Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.JRAGetReportData)
            'reportDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, ReportDt)

            MyBase.LoadDataSet(reportDataSet, parametersList, outParamList, SpNameConstants.JRAGetReportData)

            Return outParamList.Item(0).Value
        End Function
#End Region

    End Class
End Namespace
