﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports JRA_Utilities
Imports JRA_BusinessObject

Namespace JRA_DataAccess
    Public Class JobRoleDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get team jobrole modules"
        ''' <summary>
        ''' Get team jobrole modules
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>

        Public Sub getTeamModules(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim allModulesDt As DataTable = New DataTable()
            allModulesDt.TableName = ApplicationConstants.AllModulesDt

            Dim teamModulesDt As DataTable = New DataTable()
            teamModulesDt.TableName = ApplicationConstants.TeamModulesDt

            resultDataSet.Tables.Add(allModulesDt)
            resultDataSet.Tables.Add(teamModulesDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetModules)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, allModulesDt, teamModulesDt)


        End Sub

#End Region

#Region "Get team jobrole menus"
        ''' <summary>
        ''' Get team jobrole menus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>

        Public Sub getTeamMenus(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal moduleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim allMenusDt As DataTable = New DataTable()
            allMenusDt.TableName = ApplicationConstants.AllMenusDt

            Dim teamMenusDt As DataTable = New DataTable()
            teamMenusDt.TableName = ApplicationConstants.TeamMenusDt

            resultDataSet.Tables.Add(allMenusDt)
            resultDataSet.Tables.Add(teamMenusDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim moduleIdParam As ParameterBO = New ParameterBO("moduleId", moduleId, DbType.Int32)
            parametersList.Add(moduleIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetMenus)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, allMenusDt, teamMenusDt)


        End Sub

#End Region

#Region "Get team jobrole pages"
        ''' <summary>
        ''' Get team jobrole pages
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>

        Public Sub getTeamPages(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal menuId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim allPagesDt As DataTable = New DataTable()
            allPagesDt.TableName = ApplicationConstants.AllPagesDt

            Dim teamPagesDt As DataTable = New DataTable()
            teamPagesDt.TableName = ApplicationConstants.TeamPagesDt

            resultDataSet.Tables.Add(allPagesDt)
            resultDataSet.Tables.Add(teamPagesDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim menuIdParam As ParameterBO = New ParameterBO("menuId", menuId, DbType.Int32)
            parametersList.Add(menuIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPages)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, allPagesDt, teamPagesDt)


        End Sub

#End Region

#Region "Get Alerts List"
        ''' <summary>
        ''' Get Alerts List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getAlertsList(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim alertsDt As DataTable = New DataTable()
            alertsDt.TableName = ApplicationConstants.AlertsDt
            resultDataSet.Tables.Add(alertsDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAlertsList)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, alertsDt)


        End Sub
#End Region

#Region "Get All Teams"
        ''' <summary>
        ''' Get All Teams
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub getAllTeams(ByRef resultDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()

            Dim teamsDt As DataTable = New DataTable()
            teamsDt.TableName = ApplicationConstants.TeamsDt
            resultDataSet.Tables.Add(teamsDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetAllTeams)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, teamsDt)


        End Sub
#End Region

#Region "Get Intranet Main Menus"
        ''' <summary>
        ''' Get Intranet Main Menus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getIntranetMainMenus(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim intranetMenusDt As DataTable = New DataTable()
            intranetMenusDt.TableName = ApplicationConstants.IntranetMenusDt
            resultDataSet.Tables.Add(intranetMenusDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetIntranetMainMenus)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, intranetMenusDt)
        End Sub
#End Region

#Region "Get Intranet SubMenus"
        ''' <summary>
        ''' Get Intranet SubMenus
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getIntranetSubMenus(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal mainmenuId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim intranetSubmenusDt As DataTable = New DataTable()
            intranetSubmenusDt.TableName = ApplicationConstants.IntranetSubMenusDt
            resultDataSet.Tables.Add(intranetSubmenusDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim mainmenuIdParam As ParameterBO = New ParameterBO("mainmenuId", mainmenuId, DbType.Int32)
            parametersList.Add(mainmenuIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetIntranetSubMenus)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, intranetSubmenusDt)


        End Sub
#End Region

#Region "Get Intranet Pages"
        ''' <summary>
        ''' Get Intranet Pages
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getIntranetPages(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer, ByVal submenuId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim intranetPagesDt As DataTable = New DataTable()
            intranetPagesDt.TableName = ApplicationConstants.IntranetPagesDt
            resultDataSet.Tables.Add(intranetPagesDt)

            Dim intranetTeamPagesDt As DataTable = New DataTable()
            intranetTeamPagesDt.TableName = ApplicationConstants.IntranetTeamPagesDt
            resultDataSet.Tables.Add(intranetTeamPagesDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim submenuIdParam As ParameterBO = New ParameterBO("submenuId", submenuId, DbType.Int32)
            parametersList.Add(submenuIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetIntranetPages)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, intranetPagesDt, intranetTeamPagesDt)


        End Sub
#End Region

#Region "Get team employees"
        ''' <summary>
        ''' Get team employees
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="teamJobRoleId"></param>
        ''' <remarks></remarks>
        Sub getTeamEmployees(ByRef resultDataSet As DataSet, ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim emplyeeDt As DataTable = New DataTable()
            emplyeeDt.TableName = ApplicationConstants.EmployeesDt
            resultDataSet.Tables.Add(emplyeeDt)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTeamEmployees)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, emplyeeDt)


        End Sub
#End Region

#Region "Check Team JoRole Exist"
        ''' <summary>
        ''' Check Team JoRole Exist
        ''' </summary>
        ''' <param name="objTeamJobRoleBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function checkTeamJobRoleExist(ByVal objTeamJobRoleBO As TeamJobRoleBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamIdParam As ParameterBO = New ParameterBO("teamId", objTeamJobRoleBO.TeamId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim jobRoleParam As ParameterBO = New ParameterBO("jobRole", objTeamJobRoleBO.JobRole, DbType.String)
            parametersList.Add(jobRoleParam)

            Dim isExistParam As ParameterBO = New ParameterBO("isExist", 0, DbType.Boolean)
            outParametersList.Add(isExistParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.CheckTeamJobRoleExist)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Save Team JoRole"
        ''' <summary>
        ''' Save Team JoRole
        ''' </summary>
        ''' <param name="objTeamJobRoleBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveTeamJobRole(ByVal objTeamJobRoleBO As TeamJobRoleBO, ByVal userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamIdParam As ParameterBO = New ParameterBO("teamId", objTeamJobRoleBO.TeamId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim jobRoleParam As ParameterBO = New ParameterBO("jobRole", objTeamJobRoleBO.JobRole, DbType.String)
            parametersList.Add(jobRoleParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveTeamJobRole)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Update Team JoRole"
        ''' <summary>
        ''' Update Team JoRole
        ''' </summary>
        ''' <param name="objTeamJobRoleBO"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function updateTeamJobRole(ByVal objTeamJobRoleBO As TeamJobRoleBO, ByVal userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamIdParam As ParameterBO = New ParameterBO("teamId", objTeamJobRoleBO.TeamId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim jobRoleParam As ParameterBO = New ParameterBO("jobRole", objTeamJobRoleBO.JobRole, DbType.String)
            parametersList.Add(jobRoleParam)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", objTeamJobRoleBO.TeamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim isActiveParam As ParameterBO = New ParameterBO("IsActive", objTeamJobRoleBO.IsActive, DbType.Boolean)
            parametersList.Add(isActiveParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.UpdateTeamJobRole)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Search Team JoRole"
        ''' <summary>
        ''' Search Team JoRole
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="searchText"></param>
        ''' <param name="teamId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function searchTeamJobRole(ByRef resultDataset As DataSet, ByRef objPageSortBo As PageSortBO, ByVal searchText As String, ByVal teamId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchedText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            Dim teamIdParam As ParameterBO = New ParameterBO("teamId", teamId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataset, parametersList, outParametersList, SpNameConstants.SearchJobRoleList)
            Return outParametersList.Item(0).Value

        End Function

#End Region

#Region "Delete Team JoRole"
        ''' <summary>
        ''' Delete Team JoRole
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function deleteTeamJobRole(ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim isDeletedParam As ParameterBO = New ParameterBO("isDeleted", 0, DbType.Boolean)
            outParametersList.Add(isDeletedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.DeleteTeamJobRole)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Remove Team JobRole Alerts Access Rights"
        ''' <summary>
        ''' Remove Team JobRole Alerts Access Rights
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub removeTeamJobRoleAlertsAccessRights(ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)
            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.RemoveAlertsAccessRightsForTeam)

        End Sub

#End Region

#Region "Save Team JobRole Alerts Access Right"
        ''' <summary>
        ''' Save Team JobRole Alerts Access Right
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub saveTeamJobRoleAlertsAccessRights(ByVal alertDt As DataTable, ByVal teamJobRoleId As Integer, ByVal userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim alertIdListParam As ParameterBO = New ParameterBO("@alertIdList", alertDt, SqlDbType.Structured)
            parametersList.Add(alertIdListParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveAlertsAccessRightsForTeam)

        End Sub

#End Region

#Region "Search Full Team JoRole"
        ''' <summary>
        ''' Search Full Team JoRole
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="teamId"></param>        
        ''' <remarks></remarks>
        Public Sub searchFullTeamJobRole(ByRef resultDataset As DataSet, ByVal searchText As String, ByVal teamId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamIdParam As ParameterBO = New ParameterBO("teamId", teamId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim searchTextParam As ParameterBO = New ParameterBO("searchedText", searchText, DbType.String)
            parametersList.Add(searchTextParam)

            MyBase.LoadDataSet(resultDataset, parametersList, outParametersList, SpNameConstants.SearchFullJobRoleList)

        End Sub

#End Region

#Region "Reset Team JobRole Modules Access Rights"
        ''' <summary>
        ''' Reset Team JobRole Modules Access Rights
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub resetTeamAccessRights(ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("jobRoleTeamId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.ResetTeamAccessRights)

        End Sub

#End Region

#Region "Reset Team JobRole Intranet Access Rights"
        ''' <summary>
        ''' Reset Team JobRole Intranet Access Rights
        ''' </summary>        
        ''' <remarks></remarks>
        Public Sub resetIntranetTeamAccessRights(ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("jobRoleTeamId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.ResetIntranetTeamAccessRights)

        End Sub

#End Region

#Region "Save Modules Access Rights"
        ''' <summary>
        ''' Save Modules Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveModulesAccessRights(ByVal moduleDt As DataTable, ByVal menuDt As DataTable, ByVal pageDt As DataTable, ByVal teamJobRoleId As Integer, ByVal userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim moduleListParam As ParameterBO = New ParameterBO("@moduleIdList", moduleDt, SqlDbType.Structured)
            parametersList.Add(moduleListParam)

            Dim menuListParam As ParameterBO = New ParameterBO("@menuIdList", menuDt, SqlDbType.Structured)
            parametersList.Add(menuListParam)

            Dim pageListParam As ParameterBO = New ParameterBO("@pageIdList", pageDt, SqlDbType.Structured)
            parametersList.Add(pageListParam)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveModuleAccessRights)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Save Menu Access Rights"
        ''' <summary>
        ''' Save Menu Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveMenuAccessRights(ByVal menuId As Integer, ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim menuIdParam As ParameterBO = New ParameterBO("menuId", menuId, DbType.Int32)
            parametersList.Add(menuIdParam)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveMenuAccessRights)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Save Module Page Access Rights"
        ''' <summary>
        ''' Save Module Page Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function savePageAccessRights(ByVal pageId As Integer, ByVal teamJobRoleId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pageIdParam As ParameterBO = New ParameterBO("pageId", pageId, DbType.Int32)
            parametersList.Add(pageIdParam)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SavePageAccessRights)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Save Intranet Page Access Rights"
        ''' <summary>
        ''' Save Intranet Page Access Rights
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function saveIntranetPageAccessRights(ByVal pageDt As DataTable, ByVal teamJobRoleId As Integer, ByVal userId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pageIdParam As ParameterBO = New ParameterBO("@pageIdList", pageDt, SqlDbType.Structured)
            parametersList.Add(pageIdParam)

            Dim teamJobRoleIdParam As ParameterBO = New ParameterBO("teamJobRoleId", teamJobRoleId, DbType.Int32)
            parametersList.Add(teamJobRoleIdParam)

            Dim userIdParam As ParameterBO = New ParameterBO("userId", userId, DbType.Int32)
            parametersList.Add(userIdParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveIntranetPageAccessRights)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function

#End Region

#Region "Search Team Job Role History"
        ''' <summary>
        ''' Search Team Job Role History
        ''' </summary>
        ''' <param name="resultDataset"></param>
        ''' <param name="empId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function searchTeamJobRoleHistory(ByRef resultDataset As DataSet, ByRef objPageSortBo As PageSortBO, ByVal empId As Integer)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parametersList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parametersList.Add(pageNumber)

            Dim sortColumn As ParameterBO = New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            parametersList.Add(sortColumn)

            Dim sortOrder As ParameterBO = New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            parametersList.Add(sortOrder)



            Dim teamIdParam As ParameterBO = New ParameterBO("empId", empId, DbType.Int32)
            parametersList.Add(teamIdParam)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDataset, parametersList, outParametersList, SpNameConstants.SearchJobRoleHistoryList)
            Return outParametersList.Item(0).Value

        End Function

#End Region

#End Region

    End Class
End Namespace




