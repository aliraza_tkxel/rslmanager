﻿using Dashboard_UI.Models.WebAPILayer.ServiceManagers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO; 
using System.Web.Configuration;
using System.Net;
using Utilities;


namespace Dashboard_UI
{
    public partial class Bridge : MSDN.SessionPage
    {
        public bool isActive = false;
        public int requestID = 0;


        protected override void OnPreInit(System.EventArgs e)
        {
            base.OnPreInit(e);
 
            requestID = GetUserIdFromSession();
   
            if (requestID == 0)
            {
                redirectToLoginPage();
            }
            else
            {
                Session["UserId"] = requestID;
                redirectToDashboardPage();
            }

        }

        public void redirectToLoginPage()
        {
            System.Diagnostics.Debug.WriteLine("redirect in basepage");
            Response.Redirect(ApplicationConstants.LoginPath, true);
        }

        public void redirectToDashboardPage()
        {
            System.Diagnostics.Debug.WriteLine("redirect in basepage");
            Response.Redirect("Dashboard.aspx", true);
        }
    
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Bridge
        int classicUserId = 0;
        public bool LoadUser(int userId)
        {
            bool isActive = false;
            classicUserId = userId;

            //checkClassicAspSession(userId);
            SessionService objSessionService = new SessionService();
            SessionResponse objSessionResponse = objSessionService.CreateSession(classicUserId);

            if (objSessionResponse.IsActive == 0)
            {
                isActive = false;//error msg
            }
            else
            {
                isActive = true;
            }
            return isActive;

        }

        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    string endpoint =
        //        "https://testcrm.broadlandhousinggroup.org/RSLASBModuleapi/api/Session/CreateSession?userId=443";// + userId;

        //    requestID = GET(endpoint);

        //            // string sessionUserId = Session["UserId"] as string;
        //            SessionService objSessionService = new SessionService();
        //    SessionResponse objSessionResponse = objSessionService.CreateSession(443);
        //    if (objSessionResponse.IsActive == 0)
        //    {
        //        isActive = false;//error msg
        //    }
        //    else
        //    {
        //        isActive = true;
        //    }

        //    string SessionVariableName;
        //    string SessionVariableValue;

        //    foreach (string SessionVariable in Session.Keys)
        //    {
        //        // Find Session variable name
        //        SessionVariableName = SessionVariable;
        //        // Find Session variable value
        //        SessionVariableValue = Session[SessionVariableName].ToString();

        //        // Do something with this data
        //        requestID = SessionVariableValue + " " + requestID;


        //    }
        //    Session["UserId"] = 113;

        //    // isActive = LoadUser(GetUserIdFromSession());
        //}


        protected int GetUserIdFromSession()
        {
            //TODO:
            int userId = 0;

            //string tmpUserId = WebConfigurationManager.AppSettings["TempUserId"];
            //System.Diagnostics.Debug.WriteLine("base page 1");
            //try
            //{
                //System.Diagnostics.Debug.WriteLine("userId before set in basepage" + userId);
                userId = Convert.ToInt32(Request.QueryString["uID"]);
            //System.Diagnostics.Debug.WriteLine("userId on set in basepage" + userId);
            //}
            //catch
            //{
            //    userId = 0;
            //    System.Diagnostics.Debug.WriteLine("userId catch in basepage" + userId);
            //}
            //System.Diagnostics.Debug.WriteLine("basepage 2");
            //if (userId == 0 && tmpUserId.Length > 0)
            //{
            //    userId = Int32.Parse(tmpUserId);
            //}
            return userId;
        }


        string GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, System.Text.Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }
    }

      
    }