﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Home</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <style>

        .NewEmployeeName {
            font-weight: bold;
        }

        .AlertListItemsInitial {
            display: block;
        }

        .AlertListItems {
            display: none;
        }

        .alignleft {
            float: left;
        }
        .alignright {
            float: right;
            color:red
        }

        .alertGreaterThanZero { 
            color:red
        }

        .my-4 {
            margin-bottom: 0.35rem !important;
            font-size: 1.3rem;
            color:gray;

        }

        .mb-4 {
            margin-bottom: .35rem !important;  
                  
        }
              
        .QuickLink {
            background-color: #95B8D8;
            color: white;
            border: none !important;
            border-radius: 0 !important;
        }

        .QuickLink h4 a { 
            color: white;
            font-size: 20px;
            font-weight: bold;
        }


       
          
          
        .cardOverride {
            padding: 0.35rem !important;
        }

        .navOverride {
            background-color: red !Important;
        }

        .non-nav-link {
            display: block;
            padding: 0.5rem 1rem;
            color: white;
        }

        .card-title-Override {
            margin-bottom: 0.35rem;
        }

        .list-group-item {
            border-radius: 0 !important;
            height: 40px;
            padding: .4rem 1.25rem !important;
        }

        .list-group-item.a {
            color: #383d41;
        }

          
        @media (min-width: 501px) {
            .NewStarter {
                display: block;
            }

            .coloverride {
                padding-right: 0px !important;

            }
        }

        @media (max-width: 500px) {
            .NewStarter {
                display: none;
            }
        }

        @media (min-width: 768px) {
            .NavBarMobileOnly {
                display: none;
            }
            .NavBarWebOnly {
                display: block;
            }
            body {
                 padding-top: 0px !important; 
            }
        }

        @media (max-width: 769px) {
            .NavBarMobileOnly {
                display: block;
            }
            .NavBarWebOnly {
                display: none;
            }
            body {
                padding-top: 54px !important; 
            }
        }
       
        
        #wrapper {
            width: 500px; 
            overflow: hidden; /* will contain if #first is longer than #second */
        }
        #first {
            width: 60px;
            float:left; /* add this */ 
        }
        #second {
            /*text-align: center;*/
            padding-top: 08px !important;
            color: white;
            font-weight: bold;
            overflow: hidden; /* if you don't want #second to wrap below #first */
        }

    </style>
   <%-- Menu--%>
<style>
    .dropbtn {
        background-color: #000000;
        color: white;
        padding-top: 2px;
        padding-bottom: 2px;
        padding-right: 20px;
        padding-left: 5px;
        font-size: 14px;
        border: none;
        cursor: pointer;  
        border-radius: 5px;
    }

    .dropbtn:hover, .dropbtn:focus {
        background-color: #000000;
        border: #000000;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        overflow: auto;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown a:hover {background-color: #ddd;}

    .show {display: block;}
</style>
    <script type="text/javascript">

        var AlertShowState = false;
        function ShowAllAlerts() {

            var AlertListExpandHideDisplay = "none";
            var AlertListExpandDisplay = "block";
            document.getElementById("AlertExplander").innerText = "Show Less";
            if (AlertShowState) {
                AlertListExpandHideDisplay = "block";
                AlertListExpandDisplay = "none";
                document.getElementById("AlertExplander").innerText = "More";
            }
            var AlertListExpandHide = document.getElementsByClassName('AlertListItemsInitial');
            for (i = 0; i < AlertListExpandHide.length; i++) {
                AlertListExpandHide[i].style.display = AlertListExpandHideDisplay;
            }

            var AlertListExpand = document.getElementsByClassName('AlertListItems');
            for (i = 0; i < AlertListExpand.length; i++) {
                AlertListExpand[i].style.display = AlertListExpandDisplay;
            }
            AlertShowState = !AlertShowState;
        }        

    </script>
</head>

<body>

<!-- Navigation -->
<nav class=" NavBarMobileOnly navOverride navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="NavBarMobileOnly container">
      
        <div id="wrapper">
            <div id="first">
                 <button class="NavBarMobileOnly navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="NavBarMobileOnly navbar-toggler-icon"></span>
                 </button>
            </div>
            <div id="second">BROADLAND HOUSING GROUP</div>
        </div>
       

        <div class="NavBarMobileOnly collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
             
               <%=MenuString %> 
                
            </ul>
        </div>
    </div>
</nav>
<div class="NavBarWebOnly" style="width: 100%; background-color: #ED1B24">   
    <div class="container" style="vert-align: middle; vertical-align: middle">

    <div class="row">
        <div class="col-lg-4">
            <br /><br />
            <div class="dropdown">

                <button onclick="myFunction()" class="dropbtn" style="font-weight: bold;">
                    <img src="images/menu1.PNG" class="dropbtn" style="padding-right: 5px"/> Modules</button>
                <div id="myDropdown" class="dropdown-content">
                    <%=MenuButtonString %>
                </div>
            </div>
           
       </div>
        <div class="col-lg-4">
            <p style="text-align: center; color:#ecdede; font-weight: bold; "><img src="images/logo1.PNG" style="width:30%" /></p>
        </div>
        <div class="col-lg-4" style="text-align:right; color:white; margin-top: 60px;">
             User: <%=LoggedInUserFullName %> 
        </div>
    </div>
    
    </div>
</div> 
    
<script>
    /* When the user clicks on the button, 
    toggle between hiding and showing the dropdown content */
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>
<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-4">

            <h3 class="my-4">Alerts</h3>
       <%--     <ul class="list-group">
                <li class="list-group-item list-group-item-dark" style="background-color: gray; color: white">Purchasing</li> 
            </ul>--%>
            
            <ul class="list-group">
                <%=AlertsString %>
            </ul>
            
            <ul class="list-group" >
                <li class="list-group-item list-group-item-secondary" style="background-color: #E9ECEF; height: 22px; cursor: pointer; cursor: hand;  " onclick="ShowAllAlerts()">
                   <p id="AlertExplander" style="margin-top: -7px; text-align: center; color: #8A8C8E; font-weight: bold; font-size: 12px"> More </p>
                  </li> 
            </ul>
            
            <div class="NewStarter">
                <h3 class="my-4">New Starter</h3>
                <div style="background-color: #F5F5F5; padding: 18px; border: 1px solid rgba(0, 0, 0, 0.2); ">
                    <div id="empbody">
                        <p class="alignleft" id="empimage"><img id="imgEmp" src="<%=NewEmployeeImagePath %>" style="border-width:0px; padding: 5px; height: 110px; width:110px;"></p>
                        <p class="alignright" >
                            <div id="empcontent" >
                                <div class="NewEmployeeName"><%=NewEmployeeName %></div>
                                <div><%=NewEmployeeRole %></div> 
                                <div>Dept: <%=NewEmployeeDept %></div>
                                <div>Start: <%=NewEmployeeStart %></div> 
                            </div> 
                        </p> 
                    </div>  
                </div>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-8">

            <h3 class="my-4">Quick Links</h3>
            <div class=" row">
                <%=DashBox%>
                
            </div>
            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
