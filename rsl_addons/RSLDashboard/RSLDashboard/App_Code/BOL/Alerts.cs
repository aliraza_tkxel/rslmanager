﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NominalCodesCore
/// </summary>
public class Alerts : BasePage
{
     static readonly string ConnectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
    public static int EmployeeId;
    public static string TeamCode = "NothingYet"; //ASPSession("TeamCode")
    public static void GetAlertList(int employeeId, DataTable ds)
    {
        EmployeeId = employeeId;
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec dbo.I_ALERTS_SELECT_BY_EMP " + employeeId; // THIS WILL BE LINKED COMPANY TO NL ACCOUNT
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);
    }

    public static void GetMenuList(int employeeId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = " I_RSLMODULES_GETMODULELIST @USERID=" + employeeId + ",@OrderASC=1 "; // THIS WILL BE LINKED COMPANY TO NL ACCOUNT
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);
    }

    public static void GetAlertList(int employeeId, int alertId, int returnval)
    {
        EmployeeId = employeeId;
        returnval = AlertCountQueryGet(alertId);
    }


    /// <summary>
    ///     ''' Enum to select count function
    ///     ''' SEE MyWeb/include/alerts.asp for original code for alerts rules... **
    ///     ''' keep this list uptodate with E_ALERTS table
    ///     ''' </summary>
    ///     ''' <remarks></remarks>
    private enum EAlertIDs 
    {
        ReceivedInvoices = 1,
        LeaveRequest = 2,
        Absentees = 3,
        QueuedPurchases = 4,
        GeneralEnquiries = 5,
        RentEnquiries = 6,
        TransferExchange = 7,
        AdaptationEnquiries = 8,
        ServiceComplaints = 9,
        RepairsAwaitingApproval = 10,
        TenantsOnlineEnquiries = 11,
        VulnerabilityReviews = 12,
        TenantsOnlineResponses = 13,
        PreInspection = 14,
        PostInspection = 15,
        RiskReviews = 16,
        AsbComplaints = 17,
        TenancySupportReferral = 18,
        ConditionWorksApprovalRequired = 19,
        ApprovedConditionWorks = 20,
        GoodsReceivedAwaitingApproval = 21,
        GoodsReceived = 22,
        QueuedRepairs = 23,
        AvailableProperties = 24,
        VoidInspections = 25,
        BritishGasNotification = 26,
        FaultRejected = 27,
        AsbOpenCases = 28,
        DefectRequiringApproval = 29,
        DeclarationOfInterest = 30,
        TrainingApprovalMyStaff = 31,
        TrainingApprovalHR = 32,
        DeclarationofInterestPendingReview = 33,
        PayPointReview = 34,
        Gifts = 35,
        TrainingApprovalExec = 36,
        AwaitingInvoice = 37
    }
     

    public static int AlertCountQueryGet(int iAlert)
    {
        
        switch ((int)iAlert)
        {
            case (int)EAlertIDs.AvailableProperties:
            {
                return AvailableProperties();

            }

            case (int)EAlertIDs.VoidInspections:
            {
                return VoidInspections();

            }

            case (int)EAlertIDs.BritishGasNotification:
            {
                return BritishGasNotification();
            }

            case (int)EAlertIDs.Absentees:
            {
                return ExecAbsentees();

            }

            case (int)EAlertIDs.AdaptationEnquiries:
            {
                return ExecAdaptationEnquiries();

            }

            case (int)EAlertIDs.GeneralEnquiries:
            {
                return ExecGeneralEnquiries();
            }

            case (int)EAlertIDs.LeaveRequest:
            {
                return ExecAbsenceLeaveRequest();
                
                }

            case (int)EAlertIDs.QueuedPurchases:
            {
                return ExecQueuedPurchases();

            }

            case (int)EAlertIDs.ReceivedInvoices:
                {
 
                    return ExecPOReceivedInvoices();
                }

            case (int)EAlertIDs.RentEnquiries:
            {
                return ExecRentEnquiries();
            }

            case (int) EAlertIDs.ServiceComplaints:
            {
                return ExecServiceComplaints(true);
            }

            case (int)EAlertIDs.AsbComplaints:
                {
                    return Asb(false); 
                }

            case (int)EAlertIDs.TransferExchange:
            {
                return ExecTransferExchange();
            }

            case (int)EAlertIDs.RepairsAwaitingApproval:
            {
                return ExecRepairsAwaitingApproval();
            }

            case (int)EAlertIDs.TenantsOnlineEnquiries:
            {
                return TenantsOnlineEnquiries();
            }

            case (int)EAlertIDs.VulnerabilityReviews:
            {
                return VulnerabilityReviews(false); 
            }

            case (int)EAlertIDs.TenantsOnlineResponses:
            {
                return TenantsOnlineResponses();
            }

            case (int)EAlertIDs.PreInspection:
                {
                   return PreInspectionRecords(false);

                    
                }

            case (int)EAlertIDs.PostInspection:
                {
                    return PostInspectionRecords();
                }

            case (int)EAlertIDs.RiskReviews:
                {
                return RiskReviews(false);
                }

                
            case (int)EAlertIDs.TenancySupportReferral:
            {
                return TenantsSupportReferralCount();
            }

            case (int)EAlertIDs.GoodsReceivedAwaitingApproval:
            {
                return ExecGoodsReceivedAwaitingApproval();
            }

            case (int)EAlertIDs.GoodsReceived:
            {
                return ExecGoodsReceived();
            }

            case (int)EAlertIDs.ConditionWorksApprovalRequired:
                {
                    return ConditionApprovalRequired();
                }

            case (int)EAlertIDs.ApprovedConditionWorks:
                {
                    return ConditionApproved();
                }

            case (int)EAlertIDs.QueuedRepairs:
                {
                    return QueuedRepairsCount();
                }

            case (int)EAlertIDs.FaultRejected:
            {
                return ExecRejectedFaults();
            }

            case (int)EAlertIDs.AsbOpenCases:
                {
                    return ExecAsbOpenCases(false);
                }

            case (int)EAlertIDs.DefectRequiringApproval:
                {
                   return ExecDefectApprovalListCount();
                    
                }

            case (int)EAlertIDs.DeclarationOfInterest:
            {
                return ExecDeclarationOfInterest();
            }
            case (int) EAlertIDs.TrainingApprovalMyStaff:
            {
                return ExecTrainingApprovalMyStaff();
            }
            case (int) EAlertIDs.TrainingApprovalHR:
            {
                return ExecTrainingApprovalHR();
            }
            case (int) EAlertIDs.DeclarationofInterestPendingReview:
            {
                return ExecDeclarationofInterestPendingReview();
            }
            case (int) EAlertIDs.PayPointReview:
            {
                return ExecPayPointReview();
            }
            case (int) EAlertIDs.Gifts:
            {
                return ExecGifts();
            }

            case (int)EAlertIDs.TrainingApprovalExec:
            {
                return ExecTrainingApprovalExec();
            }

            case (int)EAlertIDs.AwaitingInvoice:
            {
                return ExecAwaitingInvoice();
            }

        }
        return 0;
    }

    //protected string AlertUrl(string sInputStr)
    //{
    //    if (sInputStr.Contains("UserId"))
    //        AlertUrl = sInputStr.Replace("{UserId}", EmployeeId);
    //    else
    //        AlertUrl = sInputStr.Replace("{LastName}", ASPSession("LastName"));
    //}


    private static int AvailableProperties()
    {
        int iResult = 0;
 

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[7];
            SqlConnection myConnection = new SqlConnection(ConnectString);
            SqlCommand cmdCommand = new SqlCommand("V_GetAvailableProperties", myConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@searchText", System.Data.SqlDbType.VarChar, 200);
            prms[0].Value = "";
            prms[1] = new SqlParameter("@pageSize", System.Data.SqlDbType.Int);
            prms[1].Value = 30;
            prms[2] = new SqlParameter("@pageNumber", System.Data.SqlDbType.Int);
            prms[2].Value = 1;
            prms[3] = new SqlParameter("@sortColumn", System.Data.SqlDbType.VarChar, 500);
            prms[3].Value = "Address";
            prms[4] = new SqlParameter("@sortOrder", System.Data.SqlDbType.VarChar, 5);
            prms[4].Value = "asc";
            prms[5] = new SqlParameter("@getOnlyCount", System.Data.SqlDbType.Bit);
            prms[5].Value = true;



            prms[6] = new SqlParameter("@totalCount", System.Data.SqlDbType.Int);
            prms[6].Direction = System.Data.ParameterDirection.Output;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            cmdCommand.ExecuteReader();
            myConnection.Close();
            int.TryParse(cmdCommand.Parameters["@totalCount"].Value.ToString(), out iResult);
        }
        catch
        {
        }

        

        return iResult;
    }

    private static int VoidInspections()
    {
        int iResult = 0;

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[7];
            SqlConnection myConnection = new SqlConnection(ConnectString);
            SqlCommand cmdCommand = new SqlCommand("V_GetVoidInspectionsToBeArranged", myConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@searchText", System.Data.SqlDbType.VarChar, 200);
            prms[0].Value = "";
            prms[1] = new SqlParameter("@pageSize", System.Data.SqlDbType.Int);
            prms[1].Value = 30;
            prms[2] = new SqlParameter("@pageNumber", System.Data.SqlDbType.Int);
            prms[2].Value = 1;
            prms[3] = new SqlParameter("@sortColumn", System.Data.SqlDbType.VarChar, 500);
            prms[3].Value = "Address";
            prms[4] = new SqlParameter("@sortOrder", System.Data.SqlDbType.VarChar, 5);
            prms[4].Value = "asc";
            prms[5] = new SqlParameter("@getOnlyCount", System.Data.SqlDbType.Bit);
            prms[5].Value = true;

            prms[6] = new SqlParameter("@totalCount", System.Data.SqlDbType.Int);
            prms[6].Direction = System.Data.ParameterDirection.Output;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            cmdCommand.ExecuteReader();
            myConnection.Close();
            int.TryParse(cmdCommand.Parameters["@totalCount"].Value.ToString(), out iResult);
        }
        catch
        {
        }

        

        return iResult;
    }


    private static int ExecDefectApprovalListCount()
    {
        int iResult = 0;

        try
        {
            SqlConnection conConnection = new SqlConnection(ConnectString);
            // set-up initialisation
            SqlConnection myConnection = new SqlConnection(ConnectString);
            SqlCommand cmdCommand = new SqlCommand("DF_GetDefectsToBeApprovedListCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        //finally
        //{
        //    conConnection.Close();
        //}
        return iResult;
    }

    private static int ExecGeneralEnquiries()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
             SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " + "INNER JOIN C_GENERAL G ON J.JOURNALID = G.JOURNALID " + "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " + "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " + "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " + "WHERE J.ITEMID = 3 AND J.CURRENTITEMSTATUSID = 13 AND " + "	G.GENERALHISTORYID = (SELECT MAX(GENERALHISTORYID) FROM C_GENERAL GG WHERE GG.JOURNALID = J.JOURNALID) " + "	AND ASSIGNTO = " + EmployeeId;


            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        //    finally
        //    {
        //        conConnection.Close();
        //    }

        return iResult;
    }

    private static int ExecReceivedInvoices()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT count(*) as ReceivedInvioceCount " + " FROM C_JOURNAL J " + "   INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID   " + "  LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID   " + "  INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID  " + "  INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID  " + " INNER JOIN F_PURCHASEORDER PORDER ON PORDER.ORDERID = P.ORDERID  " + " INNER JOIN F_POSTATUS POSTATUS ON POSTATUS.POSTATUSID = PORDER.POSTATUS  " + " WHERE J.ITEMID = 1  " + "	AND PORDER.POSTATUS = 7 " + "	AND J.ITEMNATUREID IN (2,22,20,21)  " + "	AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) " + "	AND R.CONTRACTORID = 1	";

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        //finally
        //{
        //    conConnection.Close();
        //}

        return iResult;
    }

    private static int ExecPOReceivedInvoices()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
             SqlCommand cmdCommand = new SqlCommand("F_POInvoiceReceivedCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@USERID", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecGoodsReceivedAwaitingApproval()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(*) AS GoodsReceivedAwaitingApproval " + "FROM F_PURCHASEORDER PO  " + "WHERE PO.POSTATUS = 1 AND PO.ACTIVE = 1 AND PO.POTYPE = 1 ";

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

       return iResult;
    }

    private static int ExecAbsentees()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter prms;
            //
     
            SqlCommand cmdCommand = new SqlCommand("E_AbsentsCounts", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        //finally
        //{
        //    conConnection.Close();
        //}
       return iResult;
    }

    private static int ExecAbsenceLeaveRequest()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter prms;
           
            SqlCommand cmdCommand = new SqlCommand("E_LeaveRequestCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecQueuedPurchases()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SEEALLQUEUEDPURCHASEORDERS' AND DEFAULTVALUE = " + EmployeeId;

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());

            int CANSEE = 0;
            if (iResult == 1)
                CANSEE = 1;

            sCommandText = " DECLARE @Date AS DATE = GETDATE() " + " SELECT  COUNT(DISTINCT PO.ORDERID) As QueuedPurchasesCount " + " FROM  F_PURCHASEORDER PO " + " CROSS APPLY (SELECT SUM(GROSSCOST)AS TOTALCOST FROM F_PURCHASEITEM " + " WHERE ACTIVE = 1 AND ORDERID = PO.ORDERID) SU " + " INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " + " LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID " + " INNER JOIN F_PURCHASEITEM PI on PI.ORDERID = PO.ORDERID ";
            if ((CANSEE == 0))
                sCommandText = sCommandText + " JOIN F_EMPLOYEELIMITS EL ON EL.EXPENDITUREID = PI.EXPENDITUREID AND EL.EMPLOYEEID = " + EmployeeId + " AND (  " + "   ( " + "	 El.LIMIT >= PI.GROSSCOST And " + "	  (select isnull(Max(LIMIT), 0) from F_EMPLOYEELIMITS where  " + "			EXPENDITUREID =  PI.EXPENDITUREID and LIMIT <  " + "				(select LIMIT from F_EMPLOYEELIMITS where  " + "					EXPENDITUREID =  PI.EXPENDITUREID and EMPLOYEEID = " + EmployeeId + ")) < Pi.GROSSCOST " + "   )  " + "   OR " + "   ( El.LIMIT >= PI.GROSSCOST AND  Pi.GROSSCOST > " + "	    (select Isnull(( " + "		    select LIMIT  from " + "		        F_EMPLOYEELIMITS where  " + "			        EXPENDITUREID =   PI.EXPENDITUREID and LIMIT <=  " + "				        (select LIMIT from F_EMPLOYEELIMITS where  " + "					        EXPENDITUREID =   PI.EXPENDITUREID and EMPLOYEEID = " + EmployeeId.ToString() + ") " + "		        group by LIMIT  " + "		        order by Limit desc  " + "		        OFFSET (datediff(DD, PI.PIDATE, getDate())/3 +1) ROWS FETCH NEXT (1) ROWS ONLY), 0) as LIMIT) " + "   ) " + " ) ";

            sCommandText = sCommandText + " WHERE(PO.POTYPE = 1 Or PO.POTYPE = 2 Or PO.POTYPE = 7 Or PO.POTYPE = 8) " + " AND ( PO.ACTIVE = 1 ) " + " AND PO.ACTIVE = 1 AND PO.POSTATUS = 0 AND PI.ACTIVE = 1 And PI.PISTATUS = 0 ";
            // *****************************************************************************************************************
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecRentEnquiries()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " + "INNER JOIN C_GENERAL G ON J.JOURNALID = G.JOURNALID " + "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " + "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " + "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " + "WHERE J.ITEMID = 2 AND J.ITEMNATUREID NOT IN (5,6,7) AND J.CURRENTITEMSTATUSID = 13 AND " + "	G.GENERALHISTORYID = (SELECT MAX(GENERALHISTORYID) FROM C_GENERAL GG WHERE GG.JOURNALID = J.JOURNALID) " + "	AND ASSIGNTO = " + EmployeeId;

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecServiceComplaints(bool bUseUserId)
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[6];

            SqlCommand cmdCommand = new SqlCommand("C_SERVICECOMPLAINTS_REPORT", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@QUERYTYPE", System.Data.SqlDbType.Int);
            prms[0].Value = 2;
            prms[1] = new SqlParameter("@ORDERBY", System.Data.SqlDbType.VarChar, 100);
            prms[1].Value = "";
            prms[2] = new SqlParameter("@TENANT", System.Data.SqlDbType.VarChar, 100);
            prms[2].Value = "";
            prms[3] = new SqlParameter("@ASSIGNEDTO", System.Data.SqlDbType.VarChar, 100);
            prms[3].Value = "";
            prms[4] = new SqlParameter("@USERID", System.Data.SqlDbType.Int);
            if (bUseUserId)
                prms[4].Value = EmployeeId;
            else
                prms[4].Value = System.DBNull.Value;

            prms[5] = new SqlParameter("@return_value", System.Data.SqlDbType.Int);
            prms[5].Direction = System.Data.ParameterDirection.ReturnValue;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ConditionApproved()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[6];

            SqlCommand cmdCommand = new SqlCommand("PLANNED_GetConditionalToBeArrangedList", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@searchText", System.Data.SqlDbType.VarChar, 200);
            prms[0].Value = "";
            prms[1] = new SqlParameter("@pageSize", System.Data.SqlDbType.Int);
            prms[1].Value = 30;
            prms[2] = new SqlParameter("@pageNumber", System.Data.SqlDbType.Int);
            prms[2].Value = 1;
            prms[3] = new SqlParameter("@sortColumn", System.Data.SqlDbType.VarChar, 500);
            prms[3].Value = "Address";
            prms[4] = new SqlParameter("@sortOrder", System.Data.SqlDbType.VarChar, 5);
            prms[4].Value = "asc";
            prms[5] = new SqlParameter("@totalCount", System.Data.SqlDbType.Int);
            prms[5].Direction = System.Data.ParameterDirection.Output;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            cmdCommand.ExecuteReader();
            conConnection.Close();
            int.TryParse(cmdCommand.Parameters["@totalCount"].Value.ToString(), out iResult);
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ConditionApprovalRequired()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[7];

            SqlCommand cmdCommand = new SqlCommand("PLANNED_GetConditionRatingList", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@searchText", System.Data.SqlDbType.VarChar, 200);
            prms[0].Value = "";
            prms[1] = new SqlParameter("@isFullList", System.Data.SqlDbType.Bit);
            prms[1].Value = false;
            prms[2] = new SqlParameter("@pageSize", System.Data.SqlDbType.Int);
            prms[2].Value = 0;
            prms[3] = new SqlParameter("@pageNumber", System.Data.SqlDbType.Int);
            prms[3].Value = 1;
            prms[4] = new SqlParameter("@sortColumn", System.Data.SqlDbType.VarChar, 500);
            prms[4].Value = "Address";
            prms[5] = new SqlParameter("@sortOrder", System.Data.SqlDbType.VarChar, 5);
            prms[5].Value = "asc";
            prms[6] = new SqlParameter("@totalCount", System.Data.SqlDbType.Int);
            prms[6].Direction = System.Data.ParameterDirection.Output;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            cmdCommand.ExecuteReader();
            conConnection.Close();
            int.TryParse(cmdCommand.Parameters["@totalCount"].Value.ToString(), out iResult);
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int Asb(bool bUseUserId)
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(J.JOURNALID)" + " FROM C_JOURNAL J " + " INNER JOIN C_ASB G ON J.JOURNALID = G.JOURNALID " + " LEFT JOIN dbo.C_ASB_CATEGORY_BY_GRADE cat ON cat.CATEGORYID=g.CATEGORY " + " LEFT JOIN C_ASB_CATEGORY AC ON AC.CATEGORYID = G.CATEGORY " + " INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " + " LEFT JOIN E__EMPLOYEE E ON E.EMPLOYEEID = G.ASSIGNTO " + " LEFT JOIN G_TITLE TIT ON TIT.TITLEID = C.TITLE " + " INNER JOIN ( SELECT MAX(TENANCYID) AS MAXTEN, CUSTOMERID FROM C_CUSTOMERTENANCY GROUP BY CUSTOMERID ) MT ON MT.CUSTOMERID = C.CUSTOMERID " + " INNER JOIN ( SELECT MAX(ASBHISTORYID) AS MAXHIST, JOURNALID FROM C_ASB GROUP BY JOURNALID ) MH ON MH.JOURNALID = J.JOURNALID " + " INNER JOIN C_TENANCY T ON T.TENANCYID = MT.MAXTEN " + " INNER JOIN P__PROPERTY P ON P.PROPERTYID = T.PROPERTYID " + " LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID " + " LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID " + " INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " + " INNER JOIN G_TITLE TIT2 ON TIT2.TITLEID = E.TITLE " + " WHERE J.ITEMNATUREID IN (8,9,24,25) " + " AND G.ASBHISTORYID=(SELECT MAX(ASBHISTORYID) FROM C_ASB WHERE JOURNALID=G.JOURNALID) AND J.CURRENTITEMSTATUSID = 13 ";

            if (bUseUserId)
            {
                string sAdditionalText;
                sAdditionalText = " AND G.ASSIGNTO = " + EmployeeId;
                sCommandText = sCommandText + sAdditionalText;
            }

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch (Exception ex)
        {
            //ErrorHelper.LogError(ex, false);
        }
        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecAdaptationEnquiries()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " + "INNER JOIN C_ADAPTATION G ON J.JOURNALID = G.JOURNALID " + "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " + "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " + "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " + "WHERE J.ITEMID = 2 AND J.ITEMNATUREID IN (5) AND J.CURRENTITEMSTATUSID = 13 AND " + "	G.ADAPTATIONHISTORYID = (SELECT MAX(ADAPTATIONHISTORYID) FROM C_ADAPTATION GG WHERE GG.JOURNALID = J.JOURNALID) " + "	AND ASSIGNTO = " + EmployeeId;

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecTransferExchange()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(J.JOURNALID) AS THECOUNT FROM C_JOURNAL J " + "INNER JOIN C_TRANSFEREXCHANGE G ON J.JOURNALID = G.JOURNALID " + "INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID " + "INNER JOIN C_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID " + "INNER JOIN C_STATUS S ON S.ITEMSTATUSID = J.CURRENTITEMSTATUSID " + "WHERE J.ITEMID = 2 AND J.ITEMNATUREID IN (6,7) AND J.CURRENTITEMSTATUSID = 13 AND " + "	G.TRANSFEREXCHANGEHISTORYID = (SELECT MAX(TRANSFEREXCHANGEHISTORYID) FROM C_TRANSFEREXCHANGE GG WHERE GG.JOURNALID = J.JOURNALID) " + "	AND ASSIGNTO = " + EmployeeId;

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int TenantsOnlineEnquiries()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = " SELECT COUNT(TO_ENQUIRY_LOG.EnquiryLogID) as NEW_ENQUIRY_COUNT " + " FROM TO_ENQUIRY_LOG " + " INNER JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID " + " INNER JOIN C_NATURE ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID " + " INNER JOIN C_ITEM ON C_NATURE.ITEMID = C_ITEM.ITEMID " + " WHERE(TO_ENQUIRY_LOG.ITEMSTATUSID = 26) ";

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int TenantsOnlineResponses()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = "SELECT COUNT(TO_ENQUIRY_LOG.EnquiryLogID) As numOfRows " + " FROM TO_ENQUIRY_LOG" + " INNER JOIN C__CUSTOMER ON TO_ENQUIRY_LOG.CustomerID = C__CUSTOMER.CUSTOMERID " + " INNER JOIN TO_CUSTOMER_RESPONSE CR 	ON TO_ENQUIRY_LOG.EnquiryLogId = CR.EnquiryLogId " + " INNER JOIN C_NATURE ON TO_ENQUIRY_LOG.ItemNatureID = C_NATURE.ITEMNATUREID " + " INNER JOIN C_ITEM ON C_NATURE.ITEMID = C_ITEM.ITEMID " + " WHERE(CR.Archived = 0 And TO_ENQUIRY_LOG.ItemStatusID = 29 And 1 = 1)";
            // sCommandText = "EXEC TO_ENQUIRY_LOG_RESPONSECOUNTSEARCHRESULTS"

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }


        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int VulnerabilityReviews(bool bUseUserId)
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;
            string sCommandText;
            sCommandText = " SELECT COUNT(CV.VULNERABILITYHISTORYID) " + " FROM C_JOURNAL J  " + " INNER JOIN C_VULNERABILITY CV ON CV.JOURNALID = J.JOURNALID  " + " INNER JOIN C_ADDRESS CUSTADD ON CUSTADD.CUSTOMERID = J.CUSTOMERID AND CUSTADD.ISDEFAULT=1  " + " INNER JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID=CUSTADD.CUSTOMERID  " + " INNER JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = CV.ASSIGNTO  " + " LEFT JOIN C_CUSTOMERTENANCY T  WITH (NOLOCK) ON T.CUSTOMERID= CUST.CUSTOMERID AND T.ENDDATE IS NULL  " + " LEFT JOIN C_TENANCY CT  WITH (NOLOCK) ON CT.TENANCYID= T.TENANCYID AND CT.ENDDATE IS NULL  " + " LEFT JOIN P__PROPERTY P  WITH (NOLOCK) ON P.PROPERTYID = CT.PROPERTYID  " + " LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   " + " LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 	 " + " WHERE(J.ITEMNATUREID = 61)  " + " AND VULNERABILITYHISTORYID=(SELECT MAX(VULNERABILITYHISTORYID) FROM C_VULNERABILITY WHERE JOURNALID=J.JOURNALID) " + " AND CV.ITEMSTATUSID=13 AND CV.ASSIGNTO = " + EmployeeId;
            if (bUseUserId)
            {
                string sAdditionalText;
                sAdditionalText = " AND CV.REVIEWDATE < (SELECT DATEADD(WK,6,GETDATE())) AND CV.ASSIGNTO = " + EmployeeId;
                sCommandText = sCommandText + sAdditionalText;
            }

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch (Exception ex)
        {
           // ErrorHelper.LogError(ex, false);
        }
        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static  int RiskReviews(bool bUseUserId)
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = " SELECT COUNT(CR.RISKHISTORYID) " + " FROM C_JOURNAL J  " + " INNER JOIN C_RISK CR ON CR.JOURNALID = J.JOURNALID  " + " INNER JOIN C_ADDRESS CUSTADD ON CUSTADD.CUSTOMERID = J.CUSTOMERID AND CUSTADD.ISDEFAULT=1  " + " INNER JOIN C__CUSTOMER CUST ON CUST.CUSTOMERID=CUSTADD.CUSTOMERID  " + " INNER JOIN E__EMPLOYEE EMP ON EMP.EMPLOYEEID = CR.ASSIGNTO  " + " LEFT JOIN C_CUSTOMERTENANCY T  WITH (NOLOCK) ON T.CUSTOMERID= CUST.CUSTOMERID AND T.ENDDATE IS NULL  " + " LEFT JOIN C_TENANCY CT  WITH (NOLOCK) ON CT.TENANCYID= T.TENANCYID AND CT.ENDDATE IS NULL " + " LEFT JOIN P__PROPERTY P  WITH (NOLOCK) ON P.PROPERTYID = CT.PROPERTYID  " + " LEFT JOIN PDR_DEVELOPMENT DEV ON DEV.DEVELOPMENTID = P.DEVELOPMENTID   " + " LEFT JOIN E_PATCH EP ON EP.PATCHID = DEV.PATCHID 	 " + " WHERE(J.ITEMNATUREID = 63)  " + " AND RISKHISTORYID=(SELECT MAX(RISKHISTORYID) FROM C_RISK WHERE JOURNALID=J.JOURNALID) " + " AND CR.ITEMSTATUSID=13 AND CR.ASSIGNTO = " + EmployeeId;
            if (bUseUserId)
            {
                string sAdditionalText;
                sAdditionalText = " AND CR.REVIEWDATE < (SELECT DATEADD(WK,6,GETDATE())) AND CR.ASSIGNTO = " + EmployeeId;
                sCommandText = sCommandText + sAdditionalText;
            }

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch (Exception ex)
        {
            //ErrorHelper.LogError(ex, false);
        }
        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecRepairsAwaitingApproval()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            string sCommandText;
            sCommandText = " SELECT COUNT (*) AS CompletedItemCount" + " FROM C_JOURNAL J  " + "     INNER JOIN C_REPAIR R ON R.JOURNALID = J.JOURNALID  " + "     LEFT JOIN S_ORGANISATION O ON O.ORGID = R.CONTRACTORID  " + "     INNER JOIN P_WOTOREPAIR W ON W.JOURNALID = J.JOURNALID " + "     INNER JOIN F_PURCHASEITEM P ON P.ORDERITEMID = W.ORDERITEMID " + " WHERE J.ITEMID = 1 " + "    AND J.CURRENTITEMSTATUSID =  7  " + "    AND J.ITEMNATUREID IN (2,22,20,21) " + "    AND R.REPAIRHISTORYID = (SELECT MAX (REPAIRHISTORYID) FROM C_REPAIR WHERE JOURNALID = J.JOURNALID) ";

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int PreInspectionRecords(bool bUseUserId)
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[1];

            SqlCommand cmdCommand = new SqlCommand("FL_PREINSPECTION_REPORT_ALERT", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@USERID", System.Data.SqlDbType.Int);
            if (bUseUserId)
                prms[0].Value = EmployeeId;
            else
                prms[0].Value = System.DBNull.Value;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int PostInspectionRecords()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[1];

            SqlCommand cmdCommand = new SqlCommand("FL_CO_POSTINSPECTION_ROWCOUNT", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;


            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int TenantsSupportReferralCount()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand("TSR_WhiteBoardAlerts", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }
        return iResult;
    }

    private static int ExecGoodsReceived()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {

            // set-up initialisation
            SqlParameter prms;

            SqlCommand cmdCommand = new SqlCommand("F_POGoodsReceivedCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@USERID", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }

        // set-up initialisation
        // conConnection.ConnectionString = ConfigurationManager.ConnectionStrings("RSL Manager Broadlands DevConnectionString").ConnectionString
        // Dim cmdCommand As New SqlCommand()
        // cmdCommand.Connection = conConnection
        // cmdCommand.CommandType = Data.CommandType.Text

        // Dim sCommandText As String
        // sCommandText = " DECLARE @POSTATUSID INT  " & _
        // " DECLARE @WOSTATUSID INT  " & _
        // " Select @POSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'  " & _
        // " Select @WOSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'   " & _
        // " SELECT COUNT(DISTINCT PO.ORDERID) AS GoodsReceived " & _
        // " FROM F_PURCHASEORDER PO  " & _
        // " OUTER APPLY (SELECT 1 AS Approved FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND ( POL.POSTATUS = @POSTATUSID or POL.POSTATUS = @WOSTATUSID )) GA " & _
        // " INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID " & _
        // " WHERE PO.POSTATUS IN (1,3,  7, 17) AND PO.ACTIVE = 1 AND GA.Approved IS NULL " & _
        // " AND " & Session("UserID") & " IN (PO.USERID, PIApprovedFilter.APPROVED_BY) "

        // sCommandText = " DECLARE @POSTATUSID INT  " & _
        // " DECLARE @WOSTATUSID INT  " & _
        // " Select @POSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'goods approved'  " & _
        // " Select @WOSTATUSID = PS.POSTATUSID From F_POSTATUS PS Where Lower( PS.POSTATUSNAME ) = 'work completed'   " & _
        // " SELECT COUNT(DISTINCT PO.ORDERID) AS GoodsReceived " & _
        // " FROM F_PURCHASEORDER PO  " & _
        // " OUTER APPLY (SELECT 1 AS Approved FROM F_PURCHASEORDER_LOG POL WHERE POL.ORDERID = PO.ORDERID AND ( POL.POSTATUS = @POSTATUSID or POL.POSTATUS = @WOSTATUSID )) GA " & _
        // " INNER JOIN F_PURCHASEITEM PIApprovedFilter ON PO.ORDERID = PIApprovedFilter.ORDERID " & _
        // " WHERE PO.POSTATUS IN (1,3,  7, 17) AND PO.ACTIVE = 1 AND GA.Approved IS NULL " & _
        // " AND " & Session("UserID") & " IN (PO.USERID, PIApprovedFilter.APPROVED_BY) "

        // cmdCommand.Connection.Open()
        // cmdCommand.CommandText = sCommandText
        // iResult = Convert.ToInt32(cmdCommand.ExecuteScalar)
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int QueuedRepairsCount()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation

            SqlCommand cmdCommand = new SqlCommand();
            cmdCommand.Connection = conConnection;
            cmdCommand.CommandType = System.Data.CommandType.Text;

            int LoggedInUserId = EmployeeId;

            int CANSEE = 0;

            string CanSeeQuery = "SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'SEEALLQUEUEDPURCHASEORDERS' AND DEFAULTVALUE = " + LoggedInUserId;
            cmdCommand.CommandText = CanSeeQuery;
            cmdCommand.Connection.Open();
            SqlDataReader reader = cmdCommand.ExecuteReader();
            if ((reader.HasRows && reader.Read()))
                CANSEE = 1;
            reader.Close();
            cmdCommand.Connection.Close();

            string SQL_show_all_pos = "";

            // DETERMINE WETHER OR NOT TO SHOWL ALL PURCHASE ORDERS ONLY IF TEAM IS IT
            if (!(TeamCode == "ITI" | System.Convert.ToInt32(CANSEE) == 1))
                SQL_show_all_pos = " AND (SELECT COUNT(IT.ORDERITEMID) FROM F_PURCHASEITEM IT INNER JOIN F_EMPLOYEELIMITS LIM ON IT.EXPENDITUREID = LIM.EXPENDITUREID " + "WHERE ORDERID = PO.ORDERID AND LIM.EMPLOYEEID = " + LoggedInUserId + " AND LIM.LIMIT >= IT.GROSSCOST) > 0 ";

            string sCommandText = "SELECT COUNT(WO.WOID) " + "FROM F_PURCHASEORDER PO " + "inner join P_WORKORDER WO ON WO.ORDERID = PO.ORDERID " + "left JOIN (SELECT SUM(GROSSCOST) AS TOTALCOST, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 GROUP BY ORDERID) SU ON SU.ORDERID = PO.ORDERID " + "left JOIN (SELECT count(orderitemid) AS TOTALqueued, ORDERID FROM F_PURCHASEITEM WHERE ACTIVE = 1 and pistatus = 0 GROUP BY ORDERID) tq ON tq.ORDERID = PO.ORDERID " + "LEFT JOIN E__EMPLOYEE E ON PO.USERID = E.EMPLOYEEID " + "INNER JOIN F_POSTATUS PS ON PS.POSTATUSID = PO.POSTATUS " + "WHERE PO.POTYPE = 2 AND PO.ACTIVE = 1 and totalqueued > 0 " + SQL_show_all_pos;

            cmdCommand.Connection.Open();
            cmdCommand.CommandText = sCommandText;
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }





    private static int BritishGasNotification()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[8];

            SqlCommand cmdCommand = new SqlCommand("V_GetBritishGasNotificationList", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@searchText", System.Data.SqlDbType.VarChar, 200);
            prms[0].Value = "";

            prms[1] = new SqlParameter("@notificationStatus", System.Data.SqlDbType.VarChar, 200);
            prms[1].Value = "InProgress";
            prms[2] = new SqlParameter("@pageSize", System.Data.SqlDbType.Int);
            prms[2].Value = 30;
            prms[3] = new SqlParameter("@pageNumber", System.Data.SqlDbType.Int);
            prms[3].Value = 1;
            prms[4] = new SqlParameter("@sortColumn", System.Data.SqlDbType.VarChar, 500);
            prms[4].Value = "Address";
            prms[5] = new SqlParameter("@sortOrder", System.Data.SqlDbType.VarChar, 5);
            prms[5].Value = "asc";
            prms[6] = new SqlParameter("@getOnlyCount", System.Data.SqlDbType.Bit);
            prms[6].Value = true;

            prms[7] = new SqlParameter("@totalCount", System.Data.SqlDbType.Int);
            prms[7].Direction = System.Data.ParameterDirection.Output;

            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            cmdCommand.ExecuteReader();
            conConnection.Close();
            int.TryParse(cmdCommand.Parameters["@totalCount"].Value.ToString(), out iResult);
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecRejectedFaults()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter prms;

            SqlCommand cmdCommand = new SqlCommand("FL_CountRejectedFaults", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@userId", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }
        return iResult;
    }

    private static int ExecAsbOpenCases(bool bUseUserId)
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter[] prms = new SqlParameter[2];

            SqlCommand cmdCommand = new SqlCommand("ASB_GetOpenCases", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;

            // add parameters
            prms[0] = new SqlParameter("@USERID", System.Data.SqlDbType.Int);
            if (bUseUserId)
                prms[0].Value = EmployeeId;
            else
                prms[0].Value = System.DBNull.Value;
            prms[1] = new SqlParameter("@return_value", System.Data.SqlDbType.Int);
            prms[1].Direction = System.Data.ParameterDirection.ReturnValue;
            foreach (SqlParameter prm in prms)
                cmdCommand.Parameters.Add(prm);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecDeclarationOfInterest()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);

        try
        {
            // set-up initialisation
            SqlParameter prms;

            SqlCommand cmdCommand = new SqlCommand("E_DeclarationOfInterestCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);

            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecGifts()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("E_GiftsCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@USERID", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecTrainingApprovalMyStaff()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("E_TrainingApprovalMyStaff", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }




    private static int ExecTrainingApprovalHR()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("E_TrainingApprovalHR", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }



    private static int ExecDeclarationofInterestPendingReview()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("E_PendingReviewDeclarationOfInterestCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecPayPointReview()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("E_PayPointReviewCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecTrainingApprovalExec()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("E_TrainingApprovalDirector", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@LineMgr", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

    private static int ExecAwaitingInvoice()
    {
        int iResult = 0;
        SqlConnection conConnection = new SqlConnection(ConnectString);
        try
        {
            // set-up initialisation
            SqlParameter prms;
            SqlCommand cmdCommand = new SqlCommand("F_GetAwaitingInvoiceCount", conConnection);
            cmdCommand.CommandType = System.Data.CommandType.StoredProcedure;
            // add parameters
            prms = new SqlParameter("@UserId", System.Data.SqlDbType.Int);
            prms.Value = EmployeeId;
            cmdCommand.Parameters.Add(prms);
            cmdCommand.Connection.Open();
            iResult = Convert.ToInt32(cmdCommand.ExecuteScalar());
        }
        catch
        {
        }

        finally
        {
            conConnection.Close();
        }

        return iResult;
    }

}
