﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NominalCodesCore
/// </summary>
public class DashboardCore : BasePage
{
     static readonly string ConnectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;

    public static void GetDashboardLinksList(int employeeId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec dbo.[I_DASHBOARD_SELECT_BY_EMP]  " + employeeId; // THIS WILL BE LINKED COMPANY TO NL ACCOUNT
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);
    }

    public static void GetNewEmployee( DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec dbo.[E__NewEmployee] "; 
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);
    }

    public static void GetEmployeeFullName(int employeeId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec P_GetEmployeeById " + employeeId;
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);
    }

    


}