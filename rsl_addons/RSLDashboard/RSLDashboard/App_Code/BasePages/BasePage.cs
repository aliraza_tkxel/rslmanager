﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Utilities;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : System.Web.UI.Page
{

    public int GlobalUserId = 0;
    protected override void OnPreInit(System.EventArgs e)
    {
        base.OnPreInit(e);

        var userId = GetUserIdFromSession();
        //if (objBridge.LoadUser(userId) == false)
        //{
        //    throw new Exception();
        //}
        if (userId == 0)
        {
            redirectToLoginPage();
        }
          
    }

    /// <summary>
    /// Get user id from session
    /// </summary>
    /// <returns>UserId</returns>
    /// <exception cref="Exception">If Session variable is not set then set UserId = 0</exception>
    protected int GetUserIdFromSession()
    {
        //TODO:
        int userId = 0;

        string tmpUserId = WebConfigurationManager.AppSettings["TempUserId"];
        System.Diagnostics.Debug.WriteLine("base page 1");
        try
        {
             System.Diagnostics.Debug.WriteLine("userId before set in basepage" + userId);
            userId = int.Parse(Session["UserId"].ToString());
            System.Diagnostics.Debug.WriteLine("userId on set in basepage" + userId);
        }
        catch
        {
            userId = 0;
            System.Diagnostics.Debug.WriteLine("userId catch in basepage" + userId);
        }
        System.Diagnostics.Debug.WriteLine("basepage 2" );
        if (userId == 0 && tmpUserId.Length > 0)
        {
            userId = Int32.Parse(tmpUserId);
        }
        return userId;
    }

   
    public void redirectToLoginPage()
    {
        System.Diagnostics.Debug.WriteLine("redirect in basepage" );
        Response.Redirect(ApplicationConstants.LoginPath, true);
    }

  
}