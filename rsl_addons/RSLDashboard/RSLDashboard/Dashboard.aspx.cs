﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard : System.Web.UI.Page
{
    public int UserId = 0;
    public string ConnectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
    
    public string NominalCodeList;

    public bool Box1;
    public bool Box2;
    public string LoggedInUserFullName;
    public string AccountId;
    public string AlertsString = "";
    public string MenuString = "";
    public string MenuButtonString = "";
    
    public string DashBox = "";
    public string NewEmployeeName = "";
    public string NewEmployeeRole = "";
    public string NewEmployeeDept = "";
    public string NewEmployeeStart = "";
    public string NewEmployeeImagePath = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            UserId = Convert.ToInt32(Session["UserId"]);
            AccountId = Request.QueryString["AccountNumber"];

            //        //}
            GetMenuList();
            GetAlerts(); 
            GetNewEmployee(); 
            GetModulePanels();
            GetEmployeeFullName();
        }

    }

    private  void GetNewEmployee()
    {
        
        DataTable dtcheck2 = new DataTable();
        DashboardCore.GetNewEmployee(dtcheck2);

        for (int p = 0; p < dtcheck2.Rows.Count; p++)
        {
            NewEmployeeName = dtcheck2.Rows[p]["FullName"].ToString();
            NewEmployeeRole = dtcheck2.Rows[p]["JobTitle"].ToString();
            NewEmployeeDept = dtcheck2.Rows[p]["TeamName"].ToString();
            NewEmployeeStart = DateTime.Parse(dtcheck2.Rows[p]["STARTDATE"].ToString()).ToString("dd MMMM yyyy");
            NewEmployeeImagePath = getLogicalEmployeeProfileImagePath(dtcheck2.Rows[p]["IMAGEPATH"].ToString());
        }

    }

    private void GetEmployeeFullName()
    {

        DataTable dtcheck2 = new DataTable();
        DashboardCore.GetEmployeeFullName(UserId, dtcheck2);

        for (int p = 0; p < dtcheck2.Rows.Count; p++)
        {
            LoggedInUserFullName = dtcheck2.Rows[p]["FullName"].ToString(); 
        }

    }

    private void GetModulePanels()
    {
        
        string dashBoxString = "<div id ='Box1' class='coloverride col-lg-4 col-md-6 mb-4' style='padding-left:5px; cursor: pointer;'  onclick='window.location=\"{2}\"'><div class=' QuickLink card h-100'>" +
                        "<div class='cardOverride card-body'>" +
                            "<img src='images/{0}.PNG' /> " +
                           "<h4 class='card-title-Override card-title''><a href='{2}'>{1}</a></h4>" +
                        "</div></div></div>";


        DataTable dtcheck2 = new DataTable();
        DashboardCore.GetDashboardLinksList(UserId, dtcheck2);

        for (int p = 0; p < dtcheck2.Rows.Count; p++)
        {
            var moduleid = dtcheck2.Rows[p]["moduleid"].ToString();
            var modulename = dtcheck2.Rows[p]["modulename"].ToString();
            var moduleurl = dtcheck2.Rows[p]["modulelink"].ToString();

            DashBox = DashBox + string.Format(dashBoxString, moduleid, modulename, moduleurl);
        }
 
    }

    private void GetAlerts()
    {
        string StartLi_1 =
            "<li class='list-group-item list-group-item-secondary AlertListItemsInitial' style='background-color: #F5F5F5'>";
        string StartLi_2 =
            "<li class='list-group-item AlertListItemsInitial'  >";

        string StartLi_1_hidden =
            "<li class='list-group-item list-group-item-secondary AlertListItems' style='background-color: #F5F5F5'>";
        string StartLi_2_hidden =
            "<li class='list-group-item AlertListItems'  >";

        string EndLi =
            "</li>";

        // TRYING CHECKBOX LIST SELECT
        DataTable dtcheck = new DataTable();
        Alerts.GetAlertList(UserId, dtcheck);


        //        if (dtcheck != null && dtcheck.Rows.Count > 0)
        //        {
        Boolean x = true;
        Boolean z = true;
        string currentAlertCategory = "";
        string currentAlertCategoryTest = "";
        string CategoryInitial = "";
        string Category = "";
        bool initialReset = false;
        for (int p = 0; p < dtcheck.Rows.Count; p++)
        {
            int AlertCount = Alerts.AlertCountQueryGet((int) dtcheck.Rows[p]["AlertId"]);
            string AlertCategory = dtcheck.Rows[p]["AlertCategory"].ToString();


            if (currentAlertCategory != AlertCategory)
            {
                Category =
                    "<li  class='list-group-item list-group-item-secondary AlertListItems' style='color:white;background-color: gray'><p class='alignleft '>" +
                    AlertCategory +
                    "</p></li>";
            }
            else
            { 
                Category = "";
            }

           

            if (currentAlertCategory == "" || currentAlertCategory != AlertCategory)
            {
                currentAlertCategory = AlertCategory;
                initialReset = false;
            }
             

            if (AlertCount > 0)
            {
                if (!initialReset)
                {
                    CategoryInitial =
                        "<li   class='list-group-item list-group-item-secondary AlertListItemsInitial' style='color:white;background-color: gray'><p class='alignleft '>" +
                        AlertCategory  +
                        "</p></li>";

                    initialReset = true;
                } 

                

                AlertsString = AlertsString + CategoryInitial + (z ? StartLi_1 : StartLi_2) + "<p class='alignleft '><a href='" +
                               dtcheck.Rows[p]["AlertUrl"] + "' style='color:#383d41'>" +
                               dtcheck.Rows[p]["AlertName"] +
                               "</a></p><p class=' alignright alertGreaterThanZero'><a href='" +
                               dtcheck.Rows[p]["AlertUrl"] + "' style='color:red'>(" + AlertCount +
                               ")</a></p>";
                CategoryInitial = "";
                z = !z;
            }

 
        
            AlertsString = AlertsString + Category + (x ? StartLi_1_hidden : StartLi_2_hidden) + "<p class='alignleft '><a href='" +
                           dtcheck.Rows[p]["AlertUrl"] + "' style='color:#383d41'>" +
                           dtcheck.Rows[p]["AlertName"] +
                           "</a></p><p class=' alignright alertGreaterThanZero'><a href='" +
                           dtcheck.Rows[p]["AlertUrl"] + "' style='color:red'>(" + AlertCount +
                           ")</a></p>";



            x = !x;
        }
    }

    private void GetMenuList()
    {
        
        // TRYING CHECKBOX LIST SELECT
        DataTable dtcheck = new DataTable();
        Alerts.GetMenuList(UserId, dtcheck);
 
        for (int p = 0; p < dtcheck.Rows.Count; p++)
        {
            String menuDescription = dtcheck.Rows[p]["Description"].ToString();
            String menuPath = dtcheck.Rows[p]["thepath"].ToString();
             
            MenuString = MenuString + "<li class='NavBarMobileOnly nav-item'><p class='NavBarMobileOnly alignleft '>" +
                           "<a class='NavBarMobileOnly non-nav-link' href='" + menuPath + "'>" + menuDescription + "</a></p></li>";

            MenuButtonString = MenuButtonString + "<a href='" + menuPath + "'>" + menuDescription + "</a>";
        }
    }

    private static string GetValue(DataSet ds, string Column)
    {
        return ds.Tables[0].Rows[0][Column].ToString();
    }

    private static string getLogicalEmployeeProfileImagePath(string _imagePath)
    {
        string imagePath = "/RSLDashboard/images/emp.gif";
        if (!string.IsNullOrEmpty(_imagePath))
        {
            var requestURL = HttpContext.Current.Request.Url;
            imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/EmployeeImage/" + _imagePath;
        }
        

        return imagePath;
    }



}