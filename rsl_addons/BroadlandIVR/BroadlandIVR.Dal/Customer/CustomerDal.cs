﻿using BroadlandIVR.Dal.Base;
using System;
using System.Linq;
using System.Collections.Generic;
using BroadlandIVR.Contracts;
using BroadlandIVR.Entities;
using BroadlandIVR.Utilities;
using System.Data;
using System.Reflection;
using System.Transactions;

namespace BroadlandIVR.Dal
{
    public class CustomerDal : BaseDal
    {
        #region "Get Customer Date Of Birth"
        public int? getCustomerByDateOfBirth(int tenancyId, string dateOfBirth)
        {
            int? customerId = null;
            string customerDateOfBirth = String.Empty;
            var tenants =
                                (from cct in context.C_CUSTOMERTENANCY
                                 join cc in context.C__CUSTOMER on cct.CUSTOMERID equals cc.CUSTOMERID
                                 where cct.TENANCYID == tenancyId
                                 select new { dob = cc.DOB, cId = cc.CUSTOMERID }
                                ).Concat(
                                    from cct in context.C_OCCUPANT
                                    join cc in context.C__CUSTOMER on cct.CUSTOMERID equals cc.CUSTOMERID
                                    where cct.TENANCYID == tenancyId
                                    select new { dob = cc.DOB, cId = cc.CUSTOMERID }
                                ).ToList();

            foreach (var tenant in tenants)
            {
                customerDateOfBirth = String.Format("{0:dd/MM/yyyy}", tenant.dob);
                if ((!String.IsNullOrEmpty(customerDateOfBirth))
                && customerDateOfBirth.Equals(dateOfBirth))
                {
                    customerId = tenant.cId;
                    break;
                }
            }

            return customerId;
        }
        #endregion

        #region "Get Customer Rent Balance"
        public double getCustomerRentBalance(int tenancyId)
        {
            List<string> statusList = new List<string>() { "Estimated", "Cancelled" };
            List<string> paymentTypeList = new List<string>() { "DD Unpaid", "Cheque Unpaid" };
            double rentBalance = 0.0;

            var rentBalanceInfo = (from frj in context.F_RENTJOURNAL
                                   join fts in context.F_TRANSACTIONSTATUS on frj.STATUSID equals fts.TRANSACTIONSTATUSID into transStatus
                                   from transactionStatus in transStatus.DefaultIfEmpty()
                                   join fps in context.F_PAYMENTTYPE on frj.PAYMENTTYPE equals fps.PAYMENTTYPEID into pType
                                   from paymentType in pType.DefaultIfEmpty()
                                   where frj.TENANCYID == tenancyId & (!(statusList.Contains(transactionStatus.DESCRIPTION))
                                                                    || paymentTypeList.Contains(paymentType.DESCRIPTION))
                                   select new { Amount = frj.AMOUNT }
                                   );
            if (rentBalanceInfo.Count() > 0)
            {
                decimal? rBalance = rentBalanceInfo.Sum(x => x.Amount);
                rentBalance = rBalance == null ? 0.0 : (double)rBalance;
            }
            return rentBalance;

        }
        #endregion

        #region "Get Customer Email"
        public string getCustomerEmail(int? customerId)
        {
            string email = string.Empty;

            var customerEmail = (
                                from ca in context.C_ADDRESS
                                where ca.CUSTOMERID == customerId && ca.ISDEFAULT == 1
                                select new { Email = ca.EMAIL }
                                ).FirstOrDefault();
            if (customerEmail != null)
            {
                email = customerEmail.Email == null ? string.Empty : customerEmail.Email;
            }

            return email;
        }
        #endregion

        #region "Get Tenant Repairs List"
        public List<RepairsListResult> getTenantRepairsList(int TenantID)
        {
            List<RepairsListResult> TenantRepairResponse = new List<RepairsListResult>();

            var tenantRepairList = (from flt in context.FL_FAULT
                                    join fl in context.FL_FAULT_LOG on flt.FaultID equals fl.FaultID
                                    join s in context.FL_FAULT_STATUS on fl.StatusID equals s.FaultStatusID
                                    join app in context.FL_FAULT_APPOINTMENT on fl.FaultLogID equals app.FaultLogId into appt
                                    from app in appt.DefaultIfEmpty()
                                    join coApp in context.FL_CO_APPOINTMENT on app.AppointmentId equals coApp.AppointmentID into coAppt
                                    from coApp in coAppt.DefaultIfEmpty()
                                    join c in context.C__CUSTOMER on fl.CustomerId equals c.CUSTOMERID
                                    join ct in context.C_CUSTOMERTENANCY on c.CUSTOMERID equals ct.CUSTOMERID
                                    where ct.TENANCYID == TenantID &&
                                    (fl.StatusID != 13 && fl.StatusID != 17)
                                    && s.ACTIVE == true

                                    select new RepairsListResult()
                                    {
                                        appointmentDate = coApp.AppointmentDate,
                                        RepairReference = fl.JobSheetNumber,
                                        RepairType = flt.Description,
                                        BRSRepair = false,
                                        dueDate = fl.DueDate,
                                        statusId = fl.StatusID
                                    }
                                );
            if (tenantRepairList.Count() > 0)
            {
                TenantRepairResponse = tenantRepairList.ToList();
                foreach (RepairsListResult item in TenantRepairResponse)
                {
                    if (item.statusId == 2 || item.statusId == 18 || item.statusId == 19 || item.statusId == 20)
                    {
                        item.RepairDate = item.dueDate;

                    }
                    else
                    {
                        item.BRSRepair = true;
                        item.RepairDate = DateTime.Parse(((DateTime)item.appointmentDate).ToString("dd/MM/yyyy") + " " + item.appointmentStartTimeString);
                    }
                }
            }
            return TenantRepairResponse;
        }
        #endregion

        #region "Tenant Repairs Cancellation" 

        public RepairCancellation TenantRepairCancellation(string JSNumber)
        {
            RepairCancellation TenantRepairResponse = new RepairCancellation();
            using (TransactionScope trans = new TransactionScope())
            {
                int? AppointmentId = 0;
                int? faultLogId = 0;
                var data = (from f in context.FL_FAULT_LOG
                            join app in context.FL_FAULT_APPOINTMENT on f.FaultLogID equals app.FaultLogId into appt
                            from appointment in appt.DefaultIfEmpty()
                                //join coApp in context.FL_CO_APPOINTMENT on appointment.AppointmentId equals coApp.AppointmentID into coAppt
                                //from coAppointment in coAppt.DefaultIfEmpty()
                            where f.JobSheetNumber.Equals(JSNumber)
                            select new AppointmentDetail
                            {
                                appointmentId = appointment.AppointmentId,
                                faultLogId = f.FaultLogID
                            }
                             );
                if (data != null)
                {
                    AppointmentId = data.FirstOrDefault().appointmentId;
                    faultLogId = data.FirstOrDefault().faultLogId;

                    if (AppointmentId != null && AppointmentId > 0)
                    {
                        //Update FL_CO_APPOINTMENT Appointment Status
                        UpdateAppointment((int)AppointmentId);
                    }
                    if (faultLogId > 0)
                    {
                        var faultLog = (from f in context.FL_FAULT_LOG
                                        where f.FaultLogID == faultLogId
                                        select f).FirstOrDefault();

                        var cancelledStatus = (from fs in context.FL_FAULT_STATUS
                                               where fs.Description == "Cancelled"
                                               select fs.FaultStatusID).FirstOrDefault();


                        faultLog.StatusID = cancelledStatus;
                        context.SaveChanges();

                        //Insert entry for cancellation
                        FL_FAULT_CANCELLED Cancelled = new FL_FAULT_CANCELLED();
                        Cancelled.FaultLogId = faultLogId;
                        Cancelled.RecordedOn = DateTime.Now;
                        Cancelled.Notes = "Tenant Cancelled - IVR";
                        context.FL_FAULT_CANCELLED.Add(Cancelled);
                        context.SaveChanges();

                        InsertFL_FAULT_JOURNALHISTORY(faultLog);
                        TenantRepairResponse.Responsecode = MessageConstants.SuccessfulMsg;
                        TenantRepairResponse.Cancellation = true;
                        trans.Complete();
                    }
                    else
                    {
                        TenantRepairResponse.Responsecode = MessageConstants.FailedMsg;
                        TenantRepairResponse.Cancellation = false;
                        trans.Dispose();
                    }
                }
                else
                {
                    trans.Dispose();
                }
            }
            return TenantRepairResponse;
        }
        #region "UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Cancelled’"
        public void UpdateFaultJOURNAL(int FaultLogIds)
        {

            var FaultJOURNAL = (from fl in context.FL_FAULT_JOURNAL
                                where fl.FaultLogID == FaultLogIds
                                select fl).FirstOrDefault();

            FaultJOURNAL.FaultStatusID = 13;
            context.SaveChanges();
        }
        #region "INSERT INTO FL_FAULT_LOG_HISTORY"
        public void InsertFL_FAULT_JOURNALHISTORY(FL_FAULT_LOG faultLog)
        {

            Nullable<int> faultLogHistoryId = null;
            FL_FAULT_LOG_HISTORY faultLogHistoryData = new FL_FAULT_LOG_HISTORY();

            var faultJournalList = context.FL_FAULT_JOURNAL.Where(jor => jor.FaultLogID == faultLog.FaultLogID);
            if (faultJournalList.Count() > 0)
            {
                FL_FAULT_JOURNAL faultJournal = faultJournalList.First();
                faultLogHistoryData.JournalID = faultJournal.JournalID;
                faultJournal.FaultStatusID = faultLog.StatusID;
            }
            faultLogHistoryData.FaultStatusID = faultLog.StatusID;
            // faultLogHistoryData.itemActionID;
            faultLogHistoryData.LastActionDate = DateTime.Now;
            faultLogHistoryData.LastActionUserID = 423;
            faultLogHistoryData.FaultLogID = faultLog.FaultLogID;
            faultLogHistoryData.ORGID = faultLog.ORGID;
            //faultLogHistoryData.scopeID;
            //faultLogHistoryData.title;
            faultLogHistoryData.Notes = faultLog.Notes;
            faultLogHistoryData.PROPERTYID = faultLog.PROPERTYID;
            faultLogHistoryData.ContractorID = faultLog.ContractorID;
            faultLogHistoryData.SchemeID = faultLog.SchemeId;
            faultLogHistoryData.BlockID = faultLog.BlockId;

            context.FL_FAULT_LOG_HISTORY.Add(faultLogHistoryData);
            context.SaveChanges();
            faultLogHistoryId = faultLogHistoryData.FaultLogHistoryID;


        }
        #endregion

        #endregion

        #region "Update Appointment"
        public void UpdateAppointment(int AppointmentId)
        {
            var cancelledStatus = (from fs in context.FL_FAULT_STATUS
                                   where fs.Description == "Cancelled"
                                   select fs.Description).FirstOrDefault();

            var CoAppointment = (from fc in context.FL_CO_APPOINTMENT
                                 where fc.AppointmentID == AppointmentId
                                 select fc).FirstOrDefault();
            CoAppointment.AppointmentStatus = cancelledStatus;
            context.SaveChanges();
            InsertFLCOAPPOINTMENTHISTORY(AppointmentId);
        }
        #endregion

        #endregion

        #region "Get Available Scheduling Dates"

        public RepairDateList GetAvailableDates(string RepairReference, string StartDate)
        {
            RepairDateList RepairDateListOBJ = new RepairDateList();
            if (RepairReference == null || RepairReference.Equals(""))
            {
                RepairDateListOBJ.Responsecode = MessageConstants.NullRepairReference;
                return RepairDateListOBJ;
            }
            else if (StartDate == null || StartDate.Equals(""))
            {
                RepairDateListOBJ.Responsecode = MessageConstants.NullDate;
                return RepairDateListOBJ;
            }

            DateTime StartDateTime = new DateTime();
            if (StartDate.Contains("AM") || StartDate.Contains("PM"))
            {
                var dateSeparator = StartDate.Split(' ');
                var isValidDate = DateTime.TryParse(dateSeparator[0], out StartDateTime);
                if (isValidDate == false)
                {
                    RepairDateListOBJ.Responsecode = MessageConstants.InvalidDate;
                    return RepairDateListOBJ;
                }
                string timePeriod = dateSeparator[1];

                if (timePeriod.Equals("AM"))
                {
                    StartDateTime = StartDateTime.AddHours(13);
                }
                else if (timePeriod.Equals("PM"))
                {
                    StartDateTime = StartDateTime.AddDays(1);
                    StartDateTime = StartDateTime.AddHours(8);
                }
            }
            else
            {
                var isValidDate = DateTime.TryParse(StartDate, out StartDateTime);
                if (isValidDate == false)
                {
                    RepairDateListOBJ.Responsecode = MessageConstants.InvalidDate;
                    return RepairDateListOBJ;
                }
                StartDateTime = StartDateTime.AddHours(8);
            }

            skipWeekend(ref StartDateTime);

            DateTime aptStartTime = StartDateTime;
            DateTime aptEndTime = StartDateTime.AddHours(4);
            int? OperativeID = 0;
            var OperId = (from f in context.FL_FAULT_LOG
                          join fa in context.FL_FAULT_APPOINTMENT on f.FaultLogID equals fa.FaultLogId
                          join fc in context.FL_CO_APPOINTMENT on fa.AppointmentId equals fc.AppointmentID
                          where f.JobSheetNumber.Equals(RepairReference)
                          select new
                          {
                              OPId = fc.OperativeID
                          }
                         ).FirstOrDefault();
            OperativeID = OperId.OPId;
            if (OperativeID == null)
            {
                RepairDateListOBJ.Responsecode = MessageConstants.FailedMsg;
            }
            else
            {
                int optID = int.Parse(OperativeID.ToString());
                RepairDateListOBJ.Responsecode = MessageConstants.SuccessfulMsg;
                bool IsSickLeave = CheckIfOnSickLeave(int.Parse(OperativeID.ToString()), StartDateTime);
                if (IsSickLeave == true)
                {
                    RepairDateListOBJ.ResponseMessage = MessageConstants.OnSickLeave;
                    return RepairDateListOBJ;
                }
                while (RepairDateListOBJ.DateList.Count < 6)
                {
                    skipWeekend(ref StartDateTime);
                    aptStartTime = StartDateTime;
                    aptEndTime = StartDateTime.AddHours(4);

                    List<IVR_GetAvailableOperativesLeaves_Result> leaves = new List<IVR_GetAvailableOperativesLeaves_Result>();
                    List<IVR_GetAvailableOperatives_Result> appointments = new List<IVR_GetAvailableOperatives_Result>();
                    bool isLeaveExistFirst = true, isLeaveExistSecond = true, isAppointmentExistFirst = true, isAppointmentExistSecond = true;
                    leaves = (from leavesIBR in context.IVR_GetAvailableOperativesLeaves(StartDateTime, OperativeID) select leavesIBR).ToList();
                    appointments = (from AppIBR in context.IVR_GetAvailableOperatives(StartDateTime, OperativeID) select AppIBR).ToList();

                    isLeaveExistFirst = isLeaveExist(leaves, optID, StartDateTime, aptStartTime, aptEndTime);
                    isAppointmentExistFirst = isAppointmentExist(appointments, optID, StartDateTime, aptStartTime, aptEndTime, StartDateTime);
                    if (isLeaveExistFirst == false && isAppointmentExistFirst == false)
                    {
                        string FirstDate = aptStartTime.ToString("dd/MM/yyyy");
                        if (aptStartTime.Hour == 8)
                        {
                            FirstDate = FirstDate + " AM";
                        }
                        else
                        {
                            FirstDate = FirstDate + " PM";
                        }
                        RepairDateListOBJ.DateList.Add(FirstDate);
                        if (RepairDateListOBJ.DateList.Count >= 5)
                            break;
                    }

                    if (StartDateTime.Hour == 8)
                    {
                        aptStartTime = aptStartTime.AddHours(5);
                        aptEndTime = aptEndTime.AddHours(5);
                    }
                    else
                    {
                        aptStartTime = aptStartTime.AddDays(1);
                        aptEndTime = aptEndTime.AddDays(1);
                        skipWeekend(ref aptStartTime);
                        skipWeekend(ref aptEndTime);
                        aptStartTime = aptStartTime.AddHours(-5);
                        aptEndTime = aptEndTime.AddHours(-5);
                    }

                    isLeaveExistSecond = isLeaveExist(leaves, optID, StartDateTime, aptStartTime, aptEndTime);
                    isAppointmentExistSecond = isAppointmentExist(appointments, optID, StartDateTime, aptStartTime, aptEndTime, StartDateTime);

                    if (isLeaveExistSecond == false && isAppointmentExistSecond == false)
                    {
                        string SecondDate = aptStartTime.ToString("dd/MM/yyyy");
                        if (aptStartTime.Hour == 8)
                        {
                            SecondDate = SecondDate + " AM";
                        }
                        else
                        {
                            SecondDate = SecondDate + " PM";
                        }
                        RepairDateListOBJ.DateList.Add(SecondDate);
                        if (RepairDateListOBJ.DateList.Count >= 5)
                            break;
                    }

                    StartDateTime = StartDateTime.AddDays(1);//Resetting Start Date
                    aptStartTime = StartDateTime;//Resetting Start Time
                    aptEndTime = StartDateTime.AddHours(4);//Resetting End Time
                }
            }
            return RepairDateListOBJ;
        }

        #region "Check If On Sick Leave"
        /// <summary>
        /// This function checks if operative is on unreturned sickness leave or not.
        /// </summary>
        /// <param name="operativeId"></param>
        /// <param name="StartDate"></param>
        /// <returns>true or false</returns>
        /// <remarks></remarks>
        private bool CheckIfOnSickLeave(int operativeId, DateTime StartDate)
        {
            bool IsOnSickLeave = false;
            var MaxAbsenceHistoryIdOBJ = (from eab in context.E_ABSENCE
                                          join ej in context.E_JOURNAL on eab.JOURNALID equals ej.JOURNALID
                                          where ej.EMPLOYEEID == operativeId
                                          group eab by eab.JOURNALID into g
                                          select new
                                          {
                                              JournalId = g.Key,
                                              MaxABSENCEHISTORYID = g.Max(s => s.ABSENCEHISTORYID)
                                          }).ToList();

            List<int> MaxAbsenceHistoryIds = new List<int>();
            MaxAbsenceHistoryIds = MaxAbsenceHistoryIdOBJ.Select(x => x.MaxABSENCEHISTORYID).ToList();

            var check = (from ej in context.E_JOURNAL
                         join ea in context.E_ABSENCE on ej.JOURNALID equals ea.JOURNALID
                         where ej.EMPLOYEEID == operativeId
                         && ej.ITEMNATUREID == 1
                         && ea.RETURNDATE == null
                         && ea.STARTDATE <= StartDate
                         && ea.ITEMSTATUSID == 1
                         && MaxAbsenceHistoryIds.Contains(ea.ABSENCEHISTORYID)
                         select ea).ToList();
            if (check.Count != 0)
            {
                IsOnSickLeave = true;
            }
            return IsOnSickLeave;
        }
        #endregion

        #region "is Leave Exist"
        /// <summary>
        /// This function checks either operative has leave in the given start time and end time
        /// if leave exists then go to next time slot that 'll start from end time of leave
        /// </summary>
        /// <param name="operativeId"></param>
        /// <param name="aptStartTime"></param>
        /// <param name="aptEndTime"></param>
        /// <returns>true or false</returns>
        /// <remarks></remarks>
        private bool isLeaveExist(List<IVR_GetAvailableOperativesLeaves_Result> leavesList, int operativeId, DateTime runningDate, DateTime aptStartTime, DateTime aptEndTime)
        {
            DataTable leavesDT = new DataTable();
            leavesDT = ToDataTable(leavesList);
            aptStartTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            aptEndTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString());

            //Convert time into seconds for comparison
            double lStartTimeInMin = 0;
            double lEndTimeInMin = 0;

            lStartTimeInMin = convertDateInMin(aptStartTime);
            lEndTimeInMin = convertDateInMin(aptEndTime);

            var leaves = leavesDT.AsEnumerable();
            var leaveResult = (from app in leaves
                               orderby Convert.ToDouble(app["EndTimeInMin"]) ascending
                               where Convert.ToDouble(app["OperativeId"]) == operativeId &&
                               ((lStartTimeInMin <= Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin > Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin <= Convert.ToDouble(app["EndTimeInMin"])) |
                               (lStartTimeInMin >= Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin <= Convert.ToDouble(app["EndTimeInMin"])) |
                               (lStartTimeInMin >= Convert.ToDouble(app["StartTimeInMin"]) & lStartTimeInMin < Convert.ToDouble(app["EndTimeInMin"]) & lEndTimeInMin >= Convert.ToDouble(app["EndTimeInMin"])) |
                               (lStartTimeInMin < Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin > Convert.ToDouble(app["EndTimeInMin"])))
                               select app);
            bool LeaveExist = false;
            if (leaveResult != null)
            {
                var leaveList = leaveResult.ToList();
                if (leaveList.Count > 0)
                {
                    LeaveExist = true;
                }
                else
                {
                    LeaveExist = false;
                }
            }
            else
            {
                LeaveExist = false;
            }
            return LeaveExist;
        }
        #endregion

        #region "is Appointment Exist"
        /// <summary>
        /// This function checks either operative has appointment in the given start time and end time
        /// </summary>
        /// <param name="operativeId"></param>
        /// <param name="aptStartTime "></param>
        /// <param name="aptEndTime "></param>
        /// <returns>true or false</returns>
        /// <remarks></remarks>
        private bool isAppointmentExist(List<IVR_GetAvailableOperatives_Result> appointmentsList, int operativeId, DateTime runningDate, DateTime aptStartTime, DateTime aptEndTime, DateTime aptStartDate = default(DateTime))
        {
            DataTable appointmentsDt = new DataTable();
            appointmentsDt = ToDataTable(appointmentsList);

            if (aptStartDate == default(DateTime))
            {
                aptStartTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            }
            else
            {
                aptStartTime = Convert.ToDateTime(aptStartDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            }

            aptEndTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString());

            //convert time into seconds for comparison
            double startTimeInSec = 0;
            double endTimeInSec = 0;

            startTimeInSec = convertDateInSec(aptStartTime);
            endTimeInSec = convertDateInSec(aptEndTime);

            var appointments = appointmentsDt.AsEnumerable();
            var appResult = (from app in appointments
                             orderby Convert.ToDouble(app["EndTimeInSec"]) ascending
                             where Convert.ToDouble(app["OperativeId"]) == operativeId &&
                             ((startTimeInSec == Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec < Convert.ToDouble(app["EndTimeInSec"]) |
                             (startTimeInSec < Convert.ToDouble(app["EndTimeInSec"]) & endTimeInSec == Convert.ToDouble(app["EndTimeInSec"])) |
                             (startTimeInSec > Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec < Convert.ToDouble(app["EndTimeInSec"])) |
                             (startTimeInSec < Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec > Convert.ToDouble(app["EndTimeInSec"])) |
                             (startTimeInSec < Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec > Convert.ToDouble(app["StartTimeInSec"])) |
                             (startTimeInSec < Convert.ToDouble(app["EndTimeInSec"]) & endTimeInSec > Convert.ToDouble(app["EndTimeInSec"]))))
                             select app);
            bool appointmentExist = false;
            if (appResult != null)
            {
                var appList = appResult.ToList();
                if (appList.Count > 0)
                {
                    appointmentExist = true;
                }
                else
                {
                    appointmentExist = false;
                }
            }
            else
            {
                appointmentExist = false;
            }
            return appointmentExist;
        }
        #endregion

        #region "Convert Date In Seconds"
        public static double convertDateInSec(DateTime timeToConvert)
        {

            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalSeconds;
        }
        #endregion

        #region "Convert Date In Minute"
        public static double convertDateInMin(System.DateTime timeToConvert)
        {
            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalMinutes;
        }
        #endregion

        #region "Convert List To DataTable"
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        #endregion

        #region "Skip Weekend"
        /// <summary>
        /// This function 'll skip the weekend (saturday or sunday)
        /// </summary>
        /// <param name="aptStartDate"></param>
        /// <param name="aptStartHour"></param>
        /// <remarks></remarks>
        private void skipWeekend(ref DateTime aptStartDate)
        {
            if (aptStartDate.DayOfWeek == DayOfWeek.Saturday)
            {
                aptStartDate = aptStartDate.AddDays(2);
            }
            else if (aptStartDate.DayOfWeek == DayOfWeek.Sunday)
            {
                aptStartDate = aptStartDate.AddDays(1);
            }
        }
        #endregion

        #endregion

        #region "Schedule Appointment"

        public RescheduleRepairResponse RescheduleRepair(string RepairReference, string SelectedDate)
        {
            RescheduleRepairResponse RescheduleRepairResponseOBJ = new RescheduleRepairResponse();
            if (RepairReference == null || RepairReference.Equals(""))
            {
                RescheduleRepairResponseOBJ.Rescheduled = false;
                RescheduleRepairResponseOBJ.Responsecode = MessageConstants.NullRepairReference;
                return RescheduleRepairResponseOBJ;
            }
            else if (SelectedDate == null || SelectedDate.Equals(""))
            {
                RescheduleRepairResponseOBJ.Rescheduled = false;
                RescheduleRepairResponseOBJ.Responsecode = MessageConstants.NullDate;
                return RescheduleRepairResponseOBJ;
            }

            if (!SelectedDate.Contains("PM") && !SelectedDate.Contains("AM"))
            {
                RescheduleRepairResponseOBJ.Rescheduled = false;
                RescheduleRepairResponseOBJ.Responsecode = MessageConstants.WrongDateFormat;
                return RescheduleRepairResponseOBJ;
            }

            DateTime AppointmentDate = new DateTime();
            DateTime AppointmentStartTime = new DateTime();
            DateTime AppointmentEndTime = new DateTime();

            var dateSeparator = SelectedDate.Split(' ');
            var isValidDate = DateTime.TryParse(dateSeparator[0], out AppointmentDate);
            if (isValidDate == false)
            {
                RescheduleRepairResponseOBJ.Rescheduled = false;
                RescheduleRepairResponseOBJ.Responsecode = MessageConstants.InvalidDate;
                return RescheduleRepairResponseOBJ;
            }

            string timePeriod = dateSeparator[1];
            AppointmentStartTime = AppointmentDate;
            AppointmentEndTime = AppointmentDate;


            var FaultLogIds = (from f in context.FL_FAULT_LOG
                               join fa in context.FL_FAULT_APPOINTMENT on f.FaultLogID equals fa.FaultLogId
                               join fc in context.FL_CO_APPOINTMENT on fa.AppointmentId equals fc.AppointmentID
                               where f.JobSheetNumber.Equals(RepairReference)
                               select fa.FaultLogId
                         ).ToList();
            int? OperativeID = (from f in context.FL_FAULT_LOG
                                join fa in context.FL_FAULT_APPOINTMENT on f.FaultLogID equals fa.FaultLogId
                                join fc in context.FL_CO_APPOINTMENT on fa.AppointmentId equals fc.AppointmentID
                                where f.JobSheetNumber.Equals(RepairReference)
                                select fc.OperativeID
                         ).FirstOrDefault();

            int AppointmentID = (from f in context.FL_FAULT_LOG
                                 join fa in context.FL_FAULT_APPOINTMENT on f.FaultLogID equals fa.FaultLogId
                                 join fc in context.FL_CO_APPOINTMENT on fa.AppointmentId equals fc.AppointmentID
                                 where f.JobSheetNumber.Equals(RepairReference)
                                 select fc.AppointmentID
             ).FirstOrDefault();

            if (timePeriod.Equals("PM"))
            {
                AppointmentStartTime = AppointmentStartTime.AddHours(13);
                AppointmentEndTime = AppointmentEndTime.AddHours(17);
            }
            if (timePeriod.Equals("AM"))
            {
                AppointmentStartTime = AppointmentStartTime.AddHours(8);
                AppointmentEndTime = AppointmentEndTime.AddHours(12);
            }


            UpdateFaultLog(FaultLogIds);
            InsertFLFAULTLOGHISTORY(FaultLogIds);
            //UpdateFaultJOURNAL(FaultLogIds);
            UpdateFLCOAPPOINTMENT(int.Parse(OperativeID.ToString()), AppointmentID, AppointmentDate, AppointmentStartTime, AppointmentEndTime);
            InsertFLCOAPPOINTMENTHISTORY(AppointmentID);

            RescheduleRepairResponseOBJ.Rescheduled = true;
            RescheduleRepairResponseOBJ.Responsecode = MessageConstants.SuccessfulMsg;
            return RescheduleRepairResponseOBJ;
        }

        #region "UPDATE FL_FAULT_LOG SET FaultStatusId=’Appointment Arranged’"
        public void UpdateFaultLog(List<int> FaultLogIds)
        {
            var FaultLogs = (from fl in context.FL_FAULT_LOG
                             where FaultLogIds.Contains(fl.FaultLogID)
                             select fl).ToList();
            foreach (var fault in FaultLogs)
            {
                fault.StatusID = 4;
                fault.SubmitDate = DateTime.Now;
            }
            context.SaveChanges();
        }
        #endregion

        #region "INSERT INTO FL_FAULT_LOG_HISTORY"
        public void InsertFLFAULTLOGHISTORY(List<int> FaultLogIds)
        {
            var FaultLogs = (from fl in context.FL_FAULT_LOG
                             join fj in context.FL_FAULT_JOURNAL on fl.FaultLogID equals fj.FaultLogID
                             where FaultLogIds.Contains(fl.FaultLogID)
                             select new
                             {
                                 fj.JournalID,
                                 fl.StatusID,
                                 fl.FaultLogID,
                                 fl.ORGID,
                                 fl.Notes,
                                 fl.PROPERTYID,
                                 fl.ContractorID,
                                 fl.Duration,
                                 fl.SchemeId,
                                 fl.BlockId
                             }).ToList();
            foreach (var fault in FaultLogs)
            {
                FL_FAULT_LOG_HISTORY FaultLogHistory = new FL_FAULT_LOG_HISTORY();
                FaultLogHistory.JournalID = fault.JournalID;
                FaultLogHistory.FaultStatusID = fault.StatusID;
                FaultLogHistory.LastActionDate = DateTime.Now;
                FaultLogHistory.FaultLogID = fault.FaultLogID;
                FaultLogHistory.ORGID = fault.ORGID;
                FaultLogHistory.Notes = fault.Notes;
                FaultLogHistory.PROPERTYID = fault.PROPERTYID;
                FaultLogHistory.ContractorID = fault.ContractorID;
                FaultLogHistory.Duration = fault.Duration;
                FaultLogHistory.SchemeID = fault.SchemeId;
                FaultLogHistory.BlockID = fault.BlockId;
                context.FL_FAULT_LOG_HISTORY.Add(FaultLogHistory);
            }
            context.SaveChanges();
        }
        #endregion



        #region "UPDATE FL_CO_APPOINTMENT"
        public void UpdateFLCOAPPOINTMENT(int OperativeId, int AppointmentID, DateTime AppointmentDate, DateTime StartTime, DateTime EndTime)
        {
            var FLCOAPPOINTMENT = (from flc in context.FL_CO_APPOINTMENT
                                   where flc.AppointmentID == AppointmentID
                                   select flc).FirstOrDefault();

            FLCOAPPOINTMENT.AppointmentDate = AppointmentDate;
            FLCOAPPOINTMENT.OperativeID = OperativeId;
            FLCOAPPOINTMENT.LastActionDate = DateTime.Now;
            FLCOAPPOINTMENT.Time = StartTime.ToShortTimeString();
            FLCOAPPOINTMENT.EndTime = EndTime.ToShortTimeString();
            FLCOAPPOINTMENT.AppointmentStatus = "Appointment Arranged";
            FLCOAPPOINTMENT.AppointmentEndDate = AppointmentDate;

            context.SaveChanges();
        }
        #endregion

        #region "INSERT INTO FL_CO_APPOINTMENT_HISTORY"
        public void InsertFLCOAPPOINTMENTHISTORY(int AppointmentID)
        {
            var FaultAPPOINTMENT = (from fca in context.FL_CO_APPOINTMENT
                                    where fca.AppointmentID == AppointmentID
                                    select fca).FirstOrDefault();

            FL_CO_APPOINTMENT_HISTORY FaultCoAppHistory = new FL_CO_APPOINTMENT_HISTORY();
            FaultCoAppHistory.AppointmentId = FaultAPPOINTMENT.AppointmentID;
            FaultCoAppHistory.JobSheetNumber = FaultAPPOINTMENT.JobSheetNumber;
            FaultCoAppHistory.AppointmentDate = FaultAPPOINTMENT.AppointmentDate;
            FaultCoAppHistory.OperativeID = FaultAPPOINTMENT.OperativeID;
            FaultCoAppHistory.LastActionDate = FaultAPPOINTMENT.LastActionDate;
            FaultCoAppHistory.Notes = FaultAPPOINTMENT.Notes;
            FaultCoAppHistory.Time = FaultAPPOINTMENT.Time;
            FaultCoAppHistory.EndTime = FaultAPPOINTMENT.EndTime;
            FaultCoAppHistory.isCalendarAppointment = FaultAPPOINTMENT.isCalendarAppointment;
            FaultCoAppHistory.AppointmentEndDate = FaultAPPOINTMENT.AppointmentEndDate;
            context.FL_CO_APPOINTMENT_HISTORY.Add(FaultCoAppHistory);
            context.SaveChanges();
        }
        #endregion

        #endregion
    }
}
