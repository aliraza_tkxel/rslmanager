﻿using BroadlandIVR.Contracts;
using BroadlandIVR.Dal.Base;
using BroadlandIVR.Entities;
using BroadlandIVR.Utilities.Constants;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BroadlandIVR.Dal.Payment
{
    public class PaymentDal: BaseDal
    {

        #region Save rent payment info
        /// <summary>
        /// This function saves rent payment info
        /// </summary>
        /// <returns> status whether rent payment has been saved or not</returns>
        public bool savePaymentInfo(PaymentInfo payment)
        {

            F_IVR ivrInfo = new F_IVR();
            bool isSaved = false;
            using (TransactionScope trans = new TransactionScope())
            {
                // Get Mapped ItemName
                payment.ItemsName = getMappedItemName(payment.ItemsName);

                // Insert in F_RENTJOURNAL
                int? rentJournalId = saveRentJournalInfo(payment);

                // Save in Cashposting
                saveCashPostingInfo(payment, rentJournalId);
                
                // Calculate balance after payment
                CustomerDal objCustDal = new CustomerDal();
                decimal? balance = (decimal?) objCustDal.getCustomerRentBalance(payment.CustomerTenancyRef);

                // Insert in F_IVR
                ivrInfo.CustomerTenancyRef = payment.CustomerTenancyRef;
                ivrInfo.CardLastFour = payment.CardLastFour;
                ivrInfo.CardExpiryMonth = payment.CardExpiryMonth;
                ivrInfo.CardExpiryYear = payment.CardExpiryYear;
                ivrInfo.ItemsName = payment.ItemsName;
                ivrInfo.ItemsPrice = payment.ItemsPrice;
                ivrInfo.ItemsQuantity = payment.ItemsQuantity;
                ivrInfo.ItemsReference = payment.ItemsReference;
                ivrInfo.TransactionKey = payment.TransactionKey;
                ivrInfo.TransactionTime = payment.TransactionTime;
                ivrInfo.TransactionStatus = payment.TransactionStatus;
                ivrInfo.TransactionValue = payment.TransactionValue;
                ivrInfo.TransactionType = payment.TransactionType;
                ivrInfo.TransactionCurrency = payment.TransactionCurrency;
                ivrInfo.TransactionAuthCode = payment.TransactionAuthCode;
                ivrInfo.TransactionToken = payment.TransactionToken;
                ivrInfo.RentJournalId = rentJournalId;
                ivrInfo.LoggedDate = DateTime.Now;
                ivrInfo.Balance = balance;
 
                context.F_IVR.Add(ivrInfo);
                context.SaveChanges();
                trans.Complete();
                isSaved = true;
            }

            return isSaved;

        }
        #endregion

        #region Save rent journal info
        /// <summary>
        /// This function saves rent journal info
        /// </summary>
        /// <returns>new rent journal id in case of success</returns>
        public int saveRentJournalInfo(PaymentInfo payment)
        {
            F_RENTJOURNAL rentJournal = new F_RENTJOURNAL();

            // Using 'Payment Card' & 'Paid' default options request by Peter 
            string transactionStatus = "Paid";
            int isDebit = 1;

            //Change in PaymentType as discussed with Kate
            int payType = 93;

            rentJournal.TENANCYID = payment.CustomerTenancyRef;
            rentJournal.TRANSACTIONDATE = payment.TransactionTime;
            rentJournal.ITEMTYPE = getItemTypeId(payment.ItemsName);
            rentJournal.PAYMENTTYPE = payType;
            rentJournal.PAYMENTSTARTDATE = payment.TransactionTime;
            rentJournal.PAYMENTENDDATE = payment.TransactionTime;
            rentJournal.AMOUNT = payment.TransactionValue;
            rentJournal.ISDEBIT = isDebit;
            rentJournal.STATUSID = getTransactionStatusId(transactionStatus);
                        
            context.F_RENTJOURNAL.Add(rentJournal);
            context.SaveChanges();
            return rentJournal.JOURNALID;

        }
        #endregion

        #region Save Cash Posting info
        /// <summary>
        /// This function Save Cash Posting info
        /// </summary>
        /// <returns>rent journal id in case of success</returns>
        public int saveCashPostingInfo(PaymentInfo payment, int? rentJournalId)
        {
            F_CASHPOSTING cashPosting = new F_CASHPOSTING();

            // Using 'Payment Card' default options request by Peter 
            string paymentType = "Payment Card";
            int cashPostingOrigin = 1;
            int createdBy = 0;
            string IVRLocalOffice = "IVR";
            string receivedFrom = "";
            string chqNumber = null;
            string slipNumber = null;

            //Change in PaymentType as discussed with Kate
            int payType = 93;

            var itemtypeId = getItemTypeId(payment.ItemsName);
            cashPosting.JOURNALID = rentJournalId;
            cashPosting.TENANCYID = payment.CustomerTenancyRef;
            cashPosting.CUSTOMERID = getTenancyCustomerId(payment.CustomerTenancyRef);
            cashPosting.CREATIONDATE = ((DateTime)payment.TransactionTime).Date;
            cashPosting.CREATEDBY = createdBy;
            cashPosting.OFFICE = (int)getLocalOfficeId(IVRLocalOffice);
            cashPosting.AMOUNT = Math.Abs((double)payment.TransactionValue);
            cashPosting.RECEIVEDFROM = receivedFrom;
            cashPosting.ITEMTYPE = itemtypeId == null ? (int)getItemTypeId(ServiceDescConstants.ItemTypeRent) : (int)itemtypeId;
            cashPosting.PAYMENTTYPE = payType;
            cashPosting.CHQNUMBER = chqNumber;
            cashPosting.PAYMENTSLIPNUMBER = slipNumber;
            cashPosting.CASHPOSTINGORIGIN = cashPostingOrigin;

            context.F_CASHPOSTING.Add(cashPosting);
            context.SaveChanges();
            return (int)cashPosting.JOURNALID;

        }

        private int? getTenancyCustomerId(int customerTenancyRef)
        {
            var customer = context.C_CUSTOMERTENANCY.Where(x => x.TENANCYID == customerTenancyRef).FirstOrDefault();
            return customer.CUSTOMERID;
        }
        #endregion

        #region Get Mapped Item Name
        /// <summary>
        /// Get Mapped Item Name
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public string getMappedItemName(string itemName)
        {
            string mappedName = string.Empty;

            string result;
            if (ApplicationConstant.RentTypeMap.TryGetValue(itemName, out result))
            {
                mappedName = result;
            }
            else
            {
                mappedName = itemName;
            }

            return mappedName;
        }
        #endregion

        #region Get Item Type Id
        /// <summary>
        /// Get Item Type Id
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public int? getItemTypeId(string itemName)
        {
            var item = context.F_ITEMTYPE
                              .Where(t => t.DESCRIPTION == itemName)
                              .FirstOrDefault();
            if (item != null)
                return item.ITEMTYPEID;
            return null;    
        }
        #endregion

        #region Get Payment Type Id
        /// <summary>
        /// Get Payment Type Id
        /// </summary>
        /// <param name="paymentName"></param>
        /// <returns></returns>
        public int? getPaymentTypeId(string paymentName)
        {
            var item = context.F_PAYMENTTYPE.Where(t => t.DESCRIPTION == paymentName).FirstOrDefault();
            if (item != null)
                return item.PAYMENTTYPEID;
            return null;
        }
        #endregion

        #region Get Transaction Status Id
        /// <summary>
        /// Get Trnsaction Status Id
        /// </summary>
        /// <param name="transactionStatusName"></param>
        /// <returns></returns>
        public int? getTransactionStatusId(string statusName)
        {
            var status = context.F_TRANSACTIONSTATUS.Where(t => t.DESCRIPTION == statusName).FirstOrDefault();
            if (status != null)
                return status.TRANSACTIONSTATUSID;
            return null;
        }
        #endregion

        #region Get Local Office Id
        /// <summary>
        /// Get Local Office Id
        /// </summary>
        /// <param name="Local Office Name"></param>
        /// <returns></returns>
        public int? getLocalOfficeId(string localOfficeName)
        {
            var localOffice = context.G_OFFICE.Where(t => t.DESCRIPTION == localOfficeName).FirstOrDefault();
            if (localOffice != null)
                return localOffice.OFFICEID;
            return null;
        }
        #endregion

    }
}
