﻿using BroadlandIVR.Entities;

namespace BroadlandIVR.Dal.Base
{
    public class BaseDal
    {
        protected BroadlandIVREntities context;

        public BaseDal()
        {
            context = new BroadlandIVREntities();
        }
    }
}
