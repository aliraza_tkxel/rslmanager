﻿using BroadlandIVR.Contracts;
using BroadlandIVR.Dal.Payment;
using BroadlandIVR.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BroadlandIVR.BusinessLayer.Payment
{
    public class PaymentBl
    {
        #region "Save Payment Info" 

        /// <summary>
        /// Save Payment Info 
        /// 
        /// </summary>
        /// <param name="objPayment"></param>
        /// <returns></returns>
        public PaymentResponse savePaymentInfo(PaymentInfo objPayment)
        {
            PaymentResponse objPaymentResponse = new PaymentResponse();
            PaymentDal objPaymentDal = new PaymentDal();
            bool isSaved = false;
            isSaved = objPaymentDal.savePaymentInfo(objPayment);

            if (isSaved){
                objPaymentResponse.Responsecode = MessageConstants.SuccessfulMsg;
            }
            else {
                objPaymentResponse.Responsecode = MessageConstants.FailedMsg;
            }

            return objPaymentResponse;
        }
        #endregion

    }
}
