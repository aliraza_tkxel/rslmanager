﻿using BroadlandIVR.Contracts;
using BroadlandIVR.Dal;
using BroadlandIVR.Utilities;
using System;
using System.Collections.Generic;

namespace BroadlandIVR.BusinessLayer
{
    public class CustomerBl
    {
        #region "Validate Customer"       
        public ValidationResponseClass validateCustomer(CustomerValidationClass objCustomer)
        {
            ValidationResponseClass objValidationResponse = new ValidationResponseClass();
            CustomerDal objCustDal = new CustomerDal();
            int? customerId = isCustomerInfoValid(objCustomer);
            if (customerId != null)
            {
                objValidationResponse.Responsecode = MessageConstants.SuccessfulAuthenticationMsg;
                objValidationResponse.Validation = true;
            }
            else
            {
                objValidationResponse.Responsecode = MessageConstants.FailedAuthenticationMsg;
                objValidationResponse.Validation = false;
            }

            return objValidationResponse;
        }
        #endregion

        #region "Tenant Repairs List"       
        public RepairList GetTenantsResult(int TenantID)
        {
            RepairList TenantRepairResponse = new RepairList();
            CustomerDal objCustDal = new CustomerDal();
            bool TenantIdCheck = isTenancyIdValid(TenantID);
            if (TenantIdCheck==true)
            {
                TenantRepairResponse.Responsecode = MessageConstants.SuccessfulMsg;
                TenantRepairResponse.RepairsList = objCustDal.getTenantRepairsList(TenantID);
            }
            else
            {
                TenantRepairResponse.Responsecode = MessageConstants.FailedMsg;
            }

            return TenantRepairResponse;
        }
        #endregion


        #region "Tenant Repairs Cancellation"       
        public RepairCancellation TenantRepairCancellation(string JSNumber)
        {
            CustomerDal objCustDal = new CustomerDal();
            return objCustDal.TenantRepairCancellation(JSNumber);
        }
        #endregion

        #region "Customer Info Validation"
        public int? isCustomerInfoValid(CustomerValidationClass objCustomer)
        {
            int? customerId = null;
            if (isTenancyIdValid(objCustomer.TenantID))
            {
                customerId = isCustomerDateOfBirthValid(objCustomer.TenantID, objCustomer.DateOfBirth);
            }
            return customerId;
        }
        #endregion

        #region "TenancyId Validation"
        public bool isTenancyIdValid(int tenancyId)
        {
            bool isValid = true;
            if (tenancyId <= 0)
            {
                isValid = false;
            }

            return isValid;
        }
        #endregion

        #region "Customer Date Of Birth Validation"
        public int? isCustomerDateOfBirthValid(int tenancyId, string dateOfBirth)
        {
            int? customerId = null;
            int DATEOFBIRTH_CHAR_LENGTH = 8;
            int dob;

            if (!(string.IsNullOrEmpty(dateOfBirth)
                || dateOfBirth.Length != DATEOFBIRTH_CHAR_LENGTH
                || !Int32.TryParse(dateOfBirth, out dob)))
            {
                CustomerDal objCustDal = new CustomerDal();
                string strDateOfBirth = string.Format("{0}/{1}/{2}", dateOfBirth.Substring(6, 2), dateOfBirth.Substring(4, 2), dateOfBirth.Substring(0, 4));
                customerId = objCustDal.getCustomerByDateOfBirth(tenancyId, strDateOfBirth);
            }
            return customerId;
        }
        #endregion

        #region "Get Available Scheduling Dates"       
        public RepairDateList GetAvailableDates(string RepairReference, string StartDate)
        {
            CustomerDal objCustDal = new CustomerDal();
            return objCustDal.GetAvailableDates(RepairReference, StartDate);
        }
        #endregion

        #region "Schedule Appointment"       
        public RescheduleRepairResponse RescheduleRepair(string RepairReference, string SelectedDate)
        {
            CustomerDal objCustDal = new CustomerDal();
            return objCustDal.RescheduleRepair(RepairReference, SelectedDate);
        }
        #endregion
    }
}
