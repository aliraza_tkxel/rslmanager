﻿using BroadlandIVR.BusinessLayer;
using BroadlandIVR.Contracts;
using BroadlandIVR.Utilities;
using System;
using System.Collections.Generic;
using System.Web;

namespace BroadlandIVR.Service
{

    public class Customer : BaseService, ICustomer
    {

        #region "Validate Customer"     
        /// <summary>
        ///  Validates the customer info then return the Current Rent Balance and a customer email address (if present)
        /// </summary>
        /// <param name="objCustomerValidation"></param>
        /// <returns></returns>
        public ValidationResponseClass CustomerValidation(CustomerValidationClass objCustomerValidation)
        {
            CustomerBl objCustomerBl = new CustomerBl();
            ValidationResponseClass objValidationResponse = new ValidationResponseClass();
            try
            {
                base.CheckLogger();
                objValidationResponse = objCustomerBl.validateCustomer(objCustomerValidation);

            }
            catch (Exception ex)
            {
                log.Error("******************  Validate Customer ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                objValidationResponse.Responsecode = MessageConstants.FailedAuthenticationMsg;
                
            }
            return objValidationResponse;
        }
        #endregion

        #region "Tenant Repairs List"     
        /// <summary>
        /// Return the Tenant's Repair List
        /// </summary>
        /// <param name="TenantId"></param>
        /// <returns></returns>
        public RepairList TenantRepairsList(int TenantID)
        {
            CustomerBl objCustomerBl = new CustomerBl();
            RepairList TenantRepairResponse = new RepairList();
            try
            {
                base.CheckLogger();
                TenantRepairResponse = objCustomerBl.GetTenantsResult(TenantID);

            }
            catch (Exception ex)
            {
                log.Error("****************** Tenant Repairs List ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                TenantRepairResponse.Responsecode = MessageConstants.FailedMsg;

            }
            return TenantRepairResponse;
        }
        #endregion

        #region "Tenant Customer"     
        /// <summary>
        ///  Validates the customer info then return the Current Rent Balance and a customer email address (if present)
        /// </summary>
        /// <param name="objCustomerValidation"></param>
        /// <returns></returns>
        public ValidationResponseClass TenantValidation(CustomerValidationClass objCustomerValidation)
        {
            CustomerBl objCustomerBl = new CustomerBl();
            ValidationResponseClass objValidationResponse = new ValidationResponseClass();
            try
            {
                base.CheckLogger();
                objValidationResponse = objCustomerBl.validateCustomer(objCustomerValidation);

            }
            catch (Exception ex)
            {
                log.Error("******************  Validate Tenant ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                objValidationResponse.Responsecode = MessageConstants.FailedAuthenticationMsg;

            }
            return objValidationResponse;
        }
        #endregion

        #region "Tenant Repair Cancellation"     
        /// <summary>
        /// Tenant's Repair List Cancellation
        /// </summary>
        /// <param name="JSNumber"></param>
        /// <returns></returns>
        public RepairCancellation TenantRepairCancellation(string JSNumber)
        {
            CustomerBl objCustomerBl = new CustomerBl();
            RepairCancellation RepairCancellationResponse = new RepairCancellation();
            try
            {
                base.CheckLogger();
                RepairCancellationResponse = objCustomerBl.TenantRepairCancellation(JSNumber);
            }
            catch (Exception ex)
            {
                log.Error("****************** Tenant Cancellation ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                RepairCancellationResponse.Responsecode = MessageConstants.FailedMsg;
                RepairCancellationResponse.Cancellation = false;
            }
            return RepairCancellationResponse;
        }
        #endregion

        #region "Get Available Scheduling Dates"     
        /// <summary>
        /// Get Available Scheduling Dates
        /// </summary>
        /// <param name="RepairReference"></param>
        /// <param name="StartDate"></param>
        /// <returns></returns>
        public RepairDateList GetAvailableDates(string RepairReference, string StartDate)
        {
            CustomerBl objCustomerBl = new CustomerBl();
            RepairDateList RepairDateListOBJ = new RepairDateList();
            try
            {
                base.CheckLogger();
                RepairDateListOBJ = objCustomerBl.GetAvailableDates(RepairReference, StartDate);
            }
            catch (Exception ex)
            {
                log.Error("****************** Available Dates ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                RepairDateListOBJ.Responsecode = MessageConstants.FailedMsg;
            }
            return RepairDateListOBJ;
        }
        #endregion

        #region "Schedule Appointment"     
        /// <summary>
        /// Schedule Appointment
        /// </summary>
        /// <param name="RepairReference"></param>
        /// <param name="SelectedDate"></param>
        /// <returns></returns>
        public RescheduleRepairResponse RescheduleRepair(string RepairReference, string SelectedDate)
        {
            CustomerBl objCustomerBl = new CustomerBl();
            RescheduleRepairResponse RescheduleRepairResponseOBJ = new RescheduleRepairResponse();
            try
            {
                base.CheckLogger();
                RescheduleRepairResponseOBJ = objCustomerBl.RescheduleRepair(RepairReference, SelectedDate);
            }
            catch (Exception ex)
            {
                log.Error("****************** Schedule Appointment ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                RescheduleRepairResponseOBJ.Responsecode = MessageConstants.FailedMsg;
                RescheduleRepairResponseOBJ.Rescheduled = false;
            }
            return RescheduleRepairResponseOBJ;
        }
        #endregion
    }
}
