﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BroadlandIVR.Service
{
    public class BaseService
    {

        #region "Setup log4net"        
        protected static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Setup logging
        /// </summary>
        protected void CheckLogger()
        {
            try
            {

                string logFile = "";
                string strconfig = "\\log";
                if (strconfig != string.Empty)
                {
                    logFile = HttpContext.Current.Server.MapPath("../Logger.Config");
                    strconfig = string.Empty;
                }
                log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(logFile));
            }
            catch (Exception ex)
            {
                log.Error("****************** Exception Block While Create log file ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
            }

        }
        #endregion
    }
}