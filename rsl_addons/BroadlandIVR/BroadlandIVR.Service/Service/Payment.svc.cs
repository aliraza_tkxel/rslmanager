﻿using BroadlandIVR.BusinessLayer.Payment;
using BroadlandIVR.Contracts;
using BroadlandIVR.Utilities;
using System;

namespace BroadlandIVR.Service.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Payment" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Payment.svc or Payment.svc.cs at the Solution Explorer and start debugging.
    public class Payment :BaseService, IPayment
    {
        /// <summary>
        /// Saves payment info returns the status of transaction
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        public PaymentResponse SavePaymentInfo(PaymentInfo payment)
        {
            PaymentBl objPaymentBl = new PaymentBl();
            PaymentResponse objResponse = new PaymentResponse();
            try
            {
                base.CheckLogger();
                objResponse = objPaymentBl.savePaymentInfo(payment);
            }
            catch (Exception ex)
            {
                log.Error("******************  Save Payment Info ******************");
                log.Error("Exception Message: " + ex.Message);
                log.Error("Inner Exception: " + ex.InnerException);
                log.Error("Exception Object: " + ex);
                objResponse.Responsecode = MessageConstants.FailedMsg;
            }
            return objResponse;
        }
    }
}
