﻿using BroadlandIVR.Contracts;
using BroadlandIVR.Utilities.Constants;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace BroadlandIVR.Service.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPayment" in both code and config file together.
    [ServiceContract]
    public interface IPayment
    {
        #region Save Payment Info
        /// <summary>
        /// This function saves Rent Payment Info
        /// </summary>
        /// <returns>Status</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SavePaymentInfo)]
        [WebInvoke (    UriTemplate = UriTemplateConstants.SavePaymentInfo,
                        Method = "POST",                         
                        RequestFormat = WebMessageFormat.Json, 
                        ResponseFormat = WebMessageFormat.Json)]
        PaymentResponse SavePaymentInfo(PaymentInfo payment);
        #endregion
    }
}
