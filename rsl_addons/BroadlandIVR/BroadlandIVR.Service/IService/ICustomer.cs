﻿using BroadlandIVR.Contracts;
using System;
using System.ServiceModel;

namespace BroadlandIVR.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICustomer" in both code and config file together.
    [ServiceContract]
    public interface ICustomer
    {
        [OperationContract]
        ValidationResponseClass CustomerValidation(CustomerValidationClass objCustomerValidation);

        [OperationContract]
        RepairList TenantRepairsList(int TenantID);

        [OperationContract]
        ValidationResponseClass TenantValidation(CustomerValidationClass objCustomerValidation);

        [OperationContract]
        RepairCancellation TenantRepairCancellation(string JSNumber);

        [OperationContract]
        RepairDateList GetAvailableDates(string RepairReference, string StartDate);

        [OperationContract]
        RescheduleRepairResponse RescheduleRepair(string RepairReference, string StartDate);

    }
}
