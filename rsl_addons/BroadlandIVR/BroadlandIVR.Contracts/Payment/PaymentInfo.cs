﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [DataContract]
    public class PaymentInfo
    {
        [DataMember]
        public int CustomerTenancyRef { get; set; }

        [DataMember]
        public string CardLastFour { get; set; }

        [DataMember]
        public string CardExpiryMonth { get; set; }

        [DataMember]
        public string CardExpiryYear { get; set; }

        [DataMember]
        public string ItemsName { get; set; }

        [DataMember]
        public decimal? ItemsPrice { get; set; }

        [DataMember]
        public int? ItemsQuantity { get; set; }

        [DataMember]
        public string ItemsReference { get; set; }

        [DataMember]
        public string TransactionKey { get; set; }

        [DataMember]
        public DateTime? TransactionTime { get; set; }

        [DataMember]
        public string TransactionStatus { get; set; }

        [DataMember]
        public decimal? TransactionValue { get; set; }

        [DataMember]
        public string TransactionType { get; set; }

        [DataMember]
        public string TransactionCurrency { get; set; }

        [DataMember]
        public string TransactionAuthCode { get; set; }

        [DataMember]
        public string TransactionToken { get; set; }

    }
}
