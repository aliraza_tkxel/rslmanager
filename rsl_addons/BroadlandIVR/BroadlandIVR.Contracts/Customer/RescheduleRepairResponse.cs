﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [DataContract]
    public class RescheduleRepairResponse
    {
        [DataMember]
        public string Responsecode { get; set; }
        [DataMember]
        public bool Rescheduled { get; set; }
    }
}