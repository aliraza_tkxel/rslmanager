﻿using System;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [MessageContract]
    public class CustomerValidation
    {
        [MessageBodyMember]
        public string PaymentType { get; set; }

        [MessageBodyMember]
        public int TenantID { get; set; }

        [MessageBodyMember]
        public string DateOfBirth { get; set; }
    }
}