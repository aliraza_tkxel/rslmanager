﻿using System;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [MessageContract]
    public class ValidationResponseClass
    {
        [MessageBodyMember]
        public string Responsecode { get; set; }
        [MessageBodyMember]
        public bool Validation { get; set; }

    }
}