﻿using System;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [MessageContract]
    public class CustomerValidationClass
    {
        [MessageBodyMember]
        public int TenantID { get; set; }

        [MessageBodyMember]
        public string DateOfBirth { get; set; }
    }
}