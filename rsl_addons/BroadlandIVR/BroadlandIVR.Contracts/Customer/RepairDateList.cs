﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [DataContract]
    public class RepairDateList
    {
        [DataMember]
        public string Responsecode { get; set; }
        [DataMember]
        public string ResponseMessage { get; set; }
        [DataMember]
        public List<string> DateList { get; set; } = new List<string>();
    }
}