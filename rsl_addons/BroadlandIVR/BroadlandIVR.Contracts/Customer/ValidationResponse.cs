﻿using System;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [MessageContract]
    public class ValidationResponse
    {
        [MessageBodyMember]
        public string Responsecode { get; set; }
        [MessageBodyMember]
        public double OutstandingBalance { get; set; }
        [MessageBodyMember]
        public string EmailAddress { get; set; }

    }
}