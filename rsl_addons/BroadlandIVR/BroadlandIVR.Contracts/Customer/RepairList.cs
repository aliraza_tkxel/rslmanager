﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace BroadlandIVR.Contracts
{
    [DataContract]
    public class RepairList
    {
        [DataMember]
        public string Responsecode { get; set; }
        [DataMember]
        public List<RepairsListResult> RepairsList {get;set;}
    }
    [DataContract]
    public class RepairsListResult
    {
        [DataMember]
        public DateTime? RepairDate { get; set; }
        [DataMember]
        public string RepairReference { get; set; }
        [DataMember]
        public string RepairType { get; set; }
        [DataMember]
        public Boolean BRSRepair { get; set; }
        [IgnoreDataMember]
        public String appointmentStartTimeString { get; set; }
        [IgnoreDataMember]
        public DateTime? appointmentDate { get; set; }
        [IgnoreDataMember]
        public int? statusId { get; set; }
        [IgnoreDataMember]
        public DateTime? dueDate { get; set; }
    }
    public class AppointmentDetail
    {
        public int? faultLogId { get; set; }
        public int? appointmentId { get; set; }
    }

    }
