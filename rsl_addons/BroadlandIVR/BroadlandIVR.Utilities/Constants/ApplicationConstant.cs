﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BroadlandIVR.Utilities.Constants
{
    public static class ApplicationConstant
    {
        public static Dictionary<string, string> RentTypeMap = new Dictionary<string, string>
        {
            {"LegalRecharge", "Legal Recharges"},
            {"Recharge", "Recharges"},
            {"Rent", "Rent"}
        };

    }
}
