﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BroadlandIVR.Utilities
{
    public static class MessageConstants
    {
        public const string FailedAuthenticationMsg = "Failed Authentication";
        public const string SuccessfulAuthenticationMsg = "Successful Authentication";
        public const string FailedMsg = "Failed";
        public const string SuccessfulMsg = "Successful";
        public const string OnSickLeave = "Operative on Sick Leave";
        public const string NullDate = "No Date received";
        public const string NullRepairReference = "No Repair Reference Number received";
        public const string WrongDateFormat = "Wrong Date Format";
        public const string InvalidDate = "Invalid Date";
    }
}
