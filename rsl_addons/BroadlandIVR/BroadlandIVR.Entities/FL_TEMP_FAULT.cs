//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BroadlandIVR.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FL_TEMP_FAULT
    {
        public int TempFaultID { get; set; }
        public Nullable<int> FaultID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> ProblemDays { get; set; }
        public Nullable<bool> RecuringProblem { get; set; }
        public Nullable<bool> CommunalProblem { get; set; }
        public string Notes { get; set; }
        public Nullable<int> ORGID { get; set; }
        public Nullable<int> ItemStatusId { get; set; }
        public Nullable<int> ItemActionId { get; set; }
        public Nullable<bool> Recharge { get; set; }
        public string PROPERTYID { get; set; }
        public Nullable<int> FaultLogId { get; set; }
        public Nullable<bool> IsAppointmentConfirmed { get; set; }
        public Nullable<bool> ISRECALL { get; set; }
        public Nullable<int> FaultTradeId { get; set; }
        public Nullable<bool> ISREARRANGE { get; set; }
        public Nullable<int> OriginalFaultLogId { get; set; }
        public bool isFollowon { get; set; }
        public Nullable<int> FollowOnFaultLogID { get; set; }
        public Nullable<decimal> Duration { get; set; }
        public Nullable<int> SchemeId { get; set; }
        public Nullable<int> BlockId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<System.DateTime> SubmitDate { get; set; }
        public Nullable<int> areaId { get; set; }
    
        public virtual FL_FAULT FL_FAULT { get; set; }
    }
}
