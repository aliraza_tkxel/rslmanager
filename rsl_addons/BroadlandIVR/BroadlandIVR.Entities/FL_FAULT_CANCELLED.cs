//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BroadlandIVR.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class FL_FAULT_CANCELLED
    {
        public int CancelID { get; set; }
        public Nullable<int> FaultLogId { get; set; }
        public Nullable<System.DateTime> RecordedOn { get; set; }
        public string Notes { get; set; }
    
        public virtual FL_FAULT_LOG FL_FAULT_LOG { get; set; }
    }
}
