﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NominalTreeJson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string State = Request.QueryString["state"];
        string Company = Request.QueryString["company"];
        string OpenedAccountId = Request.QueryString["openedAccountId"];

        string outputText;
       
        DataSet ds = new DataSet();
        NominalCodesCore.GetNominalCodeJson(ds, State, Company, OpenedAccountId);

        outputText = ds.Tables[0].Rows[0]["theJson"].ToString();
        
        Response.ContentType = "application/json";
        Response.Write(outputText);
    }
}