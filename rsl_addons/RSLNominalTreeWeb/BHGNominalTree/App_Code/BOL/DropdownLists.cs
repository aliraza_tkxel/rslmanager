﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for DropdownLists
/// </summary>
public class DropdownLists
{
    public static void getCompany(DropDownList obj)
    {
        BuildSelect.DropDownLocaleDataRetriever(obj, "", "g_company", "companyid", "description","companyid", "");
    }

    public static void getCompany(CheckBoxList obj)
    {
        BuildSelect.CheckBoxListLocaleDataRetriever(obj, "", "g_company", "companyid", "description", "companyid", "");
    }

    public static void getAccountType(DropDownList obj)
    {
        BuildSelect.DropDownLocaleDataRetriever(obj, "", "NL_ACCOUNTTYPE", "accounttypeid", "description", "accounttypeid", "");
    }


    public static void getVAT(DropDownList obj)
    {//"CAST(VATRATE AS NVARCHAR(20)) + '%'"
        BuildSelect.DropDownLocaleDataRetriever(obj, "", "f_vat", "VATID", "VATRATE", "VATID", "");
    }
   

}