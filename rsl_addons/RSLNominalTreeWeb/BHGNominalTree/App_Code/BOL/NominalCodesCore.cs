﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NominalCodesCore
/// </summary>
public class NominalCodesCore : BasePage
{
     static readonly string ConnectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;

    public static void GetNominalCodeParentInformation(string ParentAccount, DataSet ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        string QueryString = @"select ISNULL(PA.AccountNumber,'') as ParentAccountNumber,  ISNULL(PA.Name,'') as ParentName,
                                                  ISNULL(PB.AccountNumber,'') as GrandParentAccountNumber,  ISNULL(PB.Name,'') AS GrandParentName
                                           FROM  NL_ACCOUNT PA 
                                                LEFT JOIN NL_ACCOUNT PB ON PB.AccountId = PA.PARENTREFLISTID 
                                            WHERE pa.accountid = " + ParentAccount + "";


        SqlDataAdapter myCommand = new SqlDataAdapter(QueryString, myConnection);

        myCommand.Fill(ds, "Authors");
    }

    public static void GetNominalCodeInformation(string AccountId, DataSet ds)
    {
        
        SqlConnection myConnection = new SqlConnection(ConnectString);
        string QueryString = @" exec NL_GetNominalCodeDetails " + AccountId + "";


        SqlDataAdapter myCommand = new SqlDataAdapter(QueryString, myConnection);

        myCommand.Fill(ds, "NL_GetNominalCodeDetails");
    }

    public static void GetNominalCodeJson( DataSet ds , string nlstate, string company, string OpenedAccountId)
    {

        SqlConnection myConnection = new SqlConnection(ConnectString);
        string queryString = @"exec NL_TreeJson @thestate = '" + nlstate + "', @company= '" + company + "', @OpenedAccountId= '" + OpenedAccountId + "'";
        SqlDataAdapter myCommand = new SqlDataAdapter(queryString, myConnection);

        myCommand.Fill(ds, "GetNominalCodeJson");
    }

    

    public static void GetNominalCodeList(string AccountId, DataSet ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String QueryStringNCeXTRA = "";
        if (AccountId != "0")
        {
            QueryStringNCeXTRA = QueryStringNCeXTRA + " WHERE AccountId not in (" + AccountId + ")";
        }
        string QueryStringNC = @" SELECT STUFF((SELECT ',''' + ACCOUNTNUMBER + ''''
                                        FROM dbo.NL_ACCOUNT " + QueryStringNCeXTRA + @"
                                        FOR XML PATH('')) ,1,1,'') AS Txt";



        SqlDataAdapter myCommandNC = new SqlDataAdapter(QueryStringNC, myConnection);
        myCommandNC.Fill(ds, "NominalCodeList");
    }

    public static void GetSelectedCompanies(string AccountId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "Select companyid from nl_account_to_company where accountid = " + AccountId; // THIS WILL BE LINKED COMPANY TO NL ACCOUNT
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);

    }

    public static void GetSelectedCompanyDefaultPurchaseControlAccount(string AccountId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "Select companyid from NL_ACCOUNT_TO_COMPANY_PURCHASECONTROL_DEFAULT where accountid = " + AccountId; // THIS WILL BE LINKED COMPANY TO NL ACCOUNT
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);

    }

    public static void GetSelectedCompanyDefaultBankAccount(string AccountId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "Select companyid from NL_ACCOUNT_TO_COMPANY_BANK_DEFAULT where accountid = " + AccountId; // THIS WILL BE LINKED COMPANY TO NL ACCOUNT
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);

    }


    public static void GetMainMenu(string UserId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec P_GetPropertyMenuPages " + UserId + ", '' "; 
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);

    }


    public static void GetMainAccountMenu(string UserId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec AC_MENULIST " + UserId + ", 14 "; 
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);

    }

    public static void GetMainAccountMenuPages(string UserId, DataTable ds)
    {
        SqlConnection myConnection = new SqlConnection(ConnectString);
        String TargetQuery = "exec AC_PAGELIST " + UserId + ", 14 "; 
        SqlDataAdapter adapt = new SqlDataAdapter(TargetQuery, myConnection);
        adapt.Fill(ds);

    }



}