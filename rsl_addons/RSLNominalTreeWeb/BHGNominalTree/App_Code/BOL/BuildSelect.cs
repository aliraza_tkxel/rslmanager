﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for BuildSelect
/// </summary>
public class BuildSelect
{

    static readonly string connectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;


    public static void DropDownLocaleDataRetriever(DropDownList ObjectName, string topLine,  string ListTableName,  string IDColumn, string DescColumn,  string JoinColName,  string AdditionalSQL)
    {
         
        SqlConnection myConnection = new SqlConnection(connectString);
        
        // Account TYPE DD TO SORT
        string QueryString2 = "select " + IDColumn + ", " + DescColumn + " from " + ListTableName;

        SqlDataAdapter myCommand2 = new SqlDataAdapter(QueryString2, myConnection);
        DataSet ds2 = new DataSet();
        myCommand2.Fill(ds2, "DropdownList");

        ObjectName.DataSource = ds2;
        ObjectName.DataTextField = DescColumn;
        ObjectName.DataValueField = IDColumn;
        ObjectName.DataBind();


    }

    public static void CheckBoxListLocaleDataRetriever(CheckBoxList ObjectName, string topLine, string ListTableName, string IDColumn, string DescColumn, string JoinColName, string AdditionalSQL)
    {
      

        SqlConnection myConnection = new SqlConnection(connectString);

        // Account TYPE DD TO SORT
        string QueryString2 = "select " + IDColumn + ", " + DescColumn + " from " + ListTableName;

        SqlDataAdapter myCommand2 = new SqlDataAdapter(QueryString2, myConnection);
        DataSet ds2 = new DataSet();
        myCommand2.Fill(ds2, "DropdownList");

        ObjectName.DataSource = ds2;
        ObjectName.DataTextField = DescColumn;
        ObjectName.DataValueField = IDColumn;
        ObjectName.DataBind();


    }


}