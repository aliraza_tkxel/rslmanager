﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NominalTree.aspx.cs" Inherits="NominalTree" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Nominal Tree</title>

	<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!--
	<meta name="viewport" content="width=device-width, initial-scale=1">
-->
	<!--
	NOTE: "Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3"
	-->
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/Site.css" rel="stylesheet" >
	<link href="src/skin-bootstrap/ui.fancytree.css" rel="stylesheet" class="skinswitcher">

	<script src="src/jquery.fancytree.js"></script> 
	<script src="src/jquery.fancytree.edit.js"></script>
	<script src="src/jquery.fancytree.glyph.js"></script>
	<script src="src/jquery.fancytree.table.js"></script>
 
    <script>
        var uAccountId = '<%=uAccountid%>'
    </script>
<!-- Add code to initialize the tree when the document is loaded: -->
    <script src="src/NominalTree.fancytree.js?dt=<%=DateTime.UtcNow %>"></script>
    <script src="src/NominalTree.js?dt=<%=DateTime.UtcNow %>"></script>

</head>
<body>
   	<!--<p>
		Font size: <span id="curSize"></span>
		<input id="fontSize" type="number" min="4" max="48" value="10"> pt
	</p>-->
<div class="TopNavigation">
    <ul>
        <li>
            <div class="asb-dropdown">
                <a href="javascript:void(0)" class="module">Accounts</a>
                <div class="asb-dropdown-content" style="min-width:150px !important; margin-left:8px !important;">
                  
                    <%=menu %>
                   
                </div>
            </div>
        </li>
     
        <%=MenuList %>
                     
        
    
       
    </ul>
</div>
<div style="padding:10px"></div>
<div class="BlackBarHeading">
    Create/Amend Nominal Codes
</div>


<div style="padding:5px; width:90%;"></div>
    <div>
    <div class="ToggleButtonActive" id="ToggleButtonActive">
        <span class="LoadActiveInactive" clickstate="active" >Active</span> 

    </div>
    <div class="ToggleButtonInactive" id="ToggleButtonInactive">
        <span class="LoadActiveInactive" clickstate="inactive" >Inactive</span> 
    </div>

 </div><br />
<div style="color:white;">.</div>
<div style="padding: 5px;">
    <form runat="server">
        Company: <asp:DropDownList runat="server" id="ddCompany" AppendDataBoundItems="true">
             <asp:ListItem Value="" Text="-- All Codes --"></asp:ListItem>
        </asp:DropDownList>
    </form>  
</div>
	<div style="width: 90%;">
		<div style="float: left; width:  60%; height:800px; overflow-y:scroll; overflow-x:hidden;"> 

			<table id="treetable" class="table table-condensed table-hover table-striped fancytree-fade-expander">
				<colgroup>
					<col width="5px"></col>
					<col width="20px"></col>
					<col width="*"></col>
					<!--<col width="50px"></col>-->
					<!--<col width="50px"></col>-->
					<col width="150px"></col>
				</colgroup>				
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<!--<td></td>-->
						<!--<td></td>-->
						<td></td>
					</tr>
				</tbody>
			</table>

		</div>
		<div style="float: left; width: 5%; min-height:1px;"> </div>
		<div style="float: left; width: 35%;  border:1px solid black; padding: 10px;"  id="products">Select a node in the tree to view details</div>
		<br style="clear: left;" />
	</div>
</body>
</html>
