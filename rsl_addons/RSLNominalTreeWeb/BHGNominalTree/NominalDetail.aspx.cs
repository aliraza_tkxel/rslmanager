﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NominalDetail : System.Web.UI.Page
{
    public string ConnectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
    
    public string NominalCodeList;
   
    public string AccountId;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            btnSave.Attributes.Add("onclick", " return isValidForm()");
            AccountId = Request.QueryString["AccountNumber"];
            hdnId.Value = AccountId;

            if (!string.IsNullOrEmpty(AccountId))
            {
                
                SqlConnection myConnection = new SqlConnection(ConnectString);

                string ParentAccount = Request.QueryString["ParentAccountNumber"];

                hdnParent.Value = ParentAccount;
                
                PopulateDropdowns();

                

                DataSet dsNC = new DataSet();
                NominalCodesCore.GetNominalCodeList(AccountId, dsNC);
                NominalCodeList = dsNC.Tables[0].Rows[0]["Txt"].ToString();

                if (AccountId != "0")
                {
                     
                    DataSet ds = new DataSet();
                    NominalCodesCore.GetNominalCodeInformation(AccountId, ds);
                     
                     
                    divHasParent.Visible = false;
                    lblParent.Text =  GetValue(ds, "ParentAccountNumber") + ' ' + GetValue(ds, "ParentName") ;
                    if (!string.IsNullOrEmpty( GetValue(ds, "GrandParentAccountNumber") )) {
                        lblGrandParent.Text = GetValue(ds, "GrandParentAccountNumber") + ' ' + GetValue(ds, "GrandParentName");
                        divHasParent.Visible = true;
                    }
                
                    txtAccountNumber.Text = GetValue(ds,"AccountNumber");
                    txtName.Text =  GetValue(ds, "Name");
                    ddAccountType.SelectedValue = GetValue(ds, "AccountType");  //ds.Tables[0].Rows[0]["AccountType"].ToString();
                    ddVAT.SelectedValue = GetValue(ds, "TaxCode");  //ds.Tables[0].Rows[0]["TaxCode"].ToString();
                    rdoAllocateExpenditure.SelectedValue = GetValue(ds, "AllocateExpenditures");  //ds.Tables[0].Rows[0]["AllocateExpenditures"].ToString();
                    rdoIsBankAccount.SelectedValue = GetValue(ds, "isBankable");  //ds.Tables[0].Rows[0]["isBankable"].ToString();
                    rdoActive.SelectedValue = GetValue(ds, "isActive");  //ds.Tables[0].Rows[0]["isActive"].ToString();
                    
                    if (Int32.Parse(GetValue(ds, "CountOfLinkedRecords")) > 0)
                    {
                        btnDelete.Visible = false;
                    }

                    //if (txtAccountNumber.Text == "2400" || GetValue(ds, "isBankable") == "1")
                    //{
                    //    btnSave.Visible = false;

                    //}

                    // TRYING CHECKBOX LIST SELECT
                    DataTable dtcheck = new DataTable();
                    NominalCodesCore.GetSelectedCompanies(AccountId, dtcheck);
                    
                    if (dtcheck != null && dtcheck.Rows.Count > 0)
                    {
                        for (int i = 0; i < chkLstCompany.Items.Count; i++)
                        {
                            for (int p = 0; p < dtcheck.Rows.Count; p++)
                            {
                                chkLstCompany.Items.FindByValue(dtcheck.Rows[p]["companyid"].ToString()).Selected = true;
                            } 
                        }
                    }

                    // TRYING CHECKBOX LIST SELECT
                    DataTable dtcheck2 = new DataTable();
                    NominalCodesCore.GetSelectedCompanyDefaultBankAccount(AccountId, dtcheck2);

                    if (dtcheck2 != null && dtcheck2.Rows.Count > 0)
                    {
                        for (int i = 0; i < chkLstCompanyDefaultBankAccount.Items.Count; i++)
                        {
                            for (int p = 0; p < dtcheck2.Rows.Count; p++)
                            {
                                chkLstCompanyDefaultBankAccount.Items.FindByValue(dtcheck2.Rows[p]["companyid"].ToString()).Selected = true;
                            }
                        }
                    }

                    // TRYING CHECKBOX LIST SELECT
                    DataTable dtcheck3 = new DataTable();
                    NominalCodesCore.GetSelectedCompanyDefaultPurchaseControlAccount(AccountId, dtcheck3);

                    if (dtcheck3 != null && dtcheck3.Rows.Count > 0)
                    {
                        for (int i = 0; i < chkLstCompanyDefaultPurchaseControlAccount.Items.Count; i++)
                        {
                            for (int p = 0; p < dtcheck3.Rows.Count; p++)
                            {
                                chkLstCompanyDefaultPurchaseControlAccount.Items.FindByValue(dtcheck3.Rows[p]["companyid"].ToString()).Selected = true;
                            }
                        }
                    }


                }
                else
                {
                    divHasParent.Visible = false;

                    if (ParentAccount != "0")
                    {
                         
                        DataSet ds = new DataSet();
                        NominalCodesCore.GetNominalCodeParentInformation(ParentAccount,ds);


                        if (!string.IsNullOrEmpty(GetValue(ds, "ParentAccountNumber")))
                    {
                        lblParent.Text = GetValue(ds, "ParentAccountNumber") + ' ' + GetValue(ds, "ParentName");
                        }
                    if (!string.IsNullOrEmpty(GetValue(ds, "GrandParentAccountNumber")))
                    {
                        lblGrandParent.Text = GetValue(ds, "GrandParentAccountNumber") + ' ' + GetValue(ds, "GrandParentName");
                        divHasParent.Visible = true;
                    }

                    }
                    btnSave.Text = "Create";
                    btnDelete.Visible = false;
                    divActive.Visible = false;
                } 
               
            }

           
        }

    }

    private static string GetValue(DataSet ds, string Column)
    {
        return ds.Tables[0].Rows[0][Column].ToString();
    }


    private void PopulateDropdowns()
    {
        DropdownLists.getAccountType(ddAccountType);
        DropdownLists.getVAT(ddVAT);
        DropdownLists.getCompany(chkLstCompany);
        DropdownLists.getCompany(chkLstCompanyDefaultBankAccount);
        DropdownLists.getCompany(chkLstCompanyDefaultPurchaseControlAccount);
        



        rdoAllocateExpenditure.Items.Add(new ListItem("Yes", "1"));
        rdoAllocateExpenditure.Items.Add(new ListItem("No", "0"));

        rdoActive.Items.Add(new ListItem("Yes", "1"));
        rdoActive.Items.Add(new ListItem("No", "0"));

        rdoIsBankAccount.Items.Add(new ListItem("Yes", "1"));
        rdoIsBankAccount.Items.Add(new ListItem("No", "0"));
    }

    protected void btnSave_OnClick(object sender, EventArgs e)
    {
         
        if (hdnId.Value == "0")
        {
            AddNewNominalCode();
        }
        else
        {
            UpdateNominalCode();
        }
        

        Server.TransferRequest("NominalTree.aspx?uAccountId=" + AccountId);
    }

    private void AddNewNominalCode()
    {
        using (SqlConnection myConnection = new SqlConnection(ConnectString))
        {
            myConnection.Open();

            // 1.  create a command object identifying the stored procedure
            SqlCommand cmd = new SqlCommand("nl_CreateCode", myConnection);

            // 2. set the command object so it knows to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            string accountname = txtName.Text;
            string accountnumber = txtAccountNumber.Text;
            string accounttype = ddAccountType.SelectedValue;
            string parentid = hdnParent.Value;
            string taxcode = ddVAT.SelectedValue;
            string desc = txtName.Text;
            string isBankable = rdoIsBankAccount.SelectedValue;
            string allocateExpenditures = rdoAllocateExpenditure.SelectedValue;

            string companyList = String.Join(",",
                chkLstCompany.Items.OfType<ListItem>().Where(r => r.Selected)
                    .Select(r => r.Value));

            string companyDefaultBankAccountList = String.Join(",",
                chkLstCompanyDefaultBankAccount.Items.OfType<ListItem>().Where(r => r.Selected)
                    .Select(r => r.Value));


            string companyDefaultPurchaseControlAccountList = String.Join(",",
                chkLstCompanyDefaultPurchaseControlAccount.Items.OfType<ListItem>().Where(r => r.Selected)
                    .Select(r => r.Value));
            


            // 3. add parameter to command, which will be passed to the stored procedure
            cmd.Parameters.Add(new SqlParameter("@Name", accountname));
            cmd.Parameters.Add(new SqlParameter("@AccountNumber", accountnumber));
            cmd.Parameters.Add(new SqlParameter("@accounttype", accounttype));
            cmd.Parameters.Add(new SqlParameter("@parentid", parentid));
            cmd.Parameters.Add(new SqlParameter("@taxcode", taxcode));
            cmd.Parameters.Add(new SqlParameter("@desc", desc));
            cmd.Parameters.Add(new SqlParameter("@isBankable", isBankable));
            cmd.Parameters.Add(new SqlParameter("@AllocateExpenditures", allocateExpenditures)); 
            cmd.Parameters.Add(new SqlParameter("@Company", companyList));
            cmd.Parameters.Add(new SqlParameter("@CompanyBankDefault", companyDefaultBankAccountList));
            cmd.Parameters.Add(new SqlParameter("@CompanyPurchaseControlDefault", companyDefaultPurchaseControlAccountList));

            
            var returnParameter = cmd.Parameters.Add("@AccountId", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            AccountId = returnParameter.Value.ToString();
        }
    }

    private void UpdateNominalCode()
    {
        using (SqlConnection myConnection = new SqlConnection(ConnectString))
        {
            myConnection.Open();

            // 1.  create a command object identifying the stored procedure
            SqlCommand cmd = new SqlCommand("nl_UpdateCode", myConnection);

            // 2. set the command object so it knows to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            string accountname = txtName.Text;
            string accountnumber = txtAccountNumber.Text;
            string accounttype = ddAccountType.SelectedValue;
            string parentid = hdnParent.Value;
            string taxcode = ddVAT.SelectedValue;
            string desc = txtName.Text;
            string isBankable = rdoIsBankAccount.SelectedValue;
            string allocateExpenditures = rdoAllocateExpenditure.SelectedValue;
            AccountId = hdnId.Value;
            int isActive = Int32.Parse(rdoActive.SelectedValue);

            string companyList = String.Join(",",
                chkLstCompany.Items.OfType<ListItem>().Where(r => r.Selected)
                    .Select(r => r.Value));

            string companyDefaultBankAccountList = String.Join(",",
                chkLstCompanyDefaultBankAccount.Items.OfType<ListItem>().Where(r => r.Selected)
                    .Select(r => r.Value));

            string companyDefaultPurchaseControlAccountList = String.Join(",",
                chkLstCompanyDefaultPurchaseControlAccount.Items.OfType<ListItem>().Where(r => r.Selected)
                    .Select(r => r.Value));
            

            // 3. add parameter to command, which will be passed to the stored procedure
            cmd.Parameters.Add(new SqlParameter("@AccountId", AccountId));
            cmd.Parameters.Add(new SqlParameter("@Name", accountname));
            cmd.Parameters.Add(new SqlParameter("@AccountNumber", accountnumber));
            cmd.Parameters.Add(new SqlParameter("@accounttype", accounttype));
            cmd.Parameters.Add(new SqlParameter("@taxcode", taxcode));
            cmd.Parameters.Add(new SqlParameter("@desc", desc));
            cmd.Parameters.Add(new SqlParameter("@isBankable", isBankable));
            cmd.Parameters.Add(new SqlParameter("@AllocateExpenditures", allocateExpenditures));
            cmd.Parameters.Add(new SqlParameter("@Company", companyList));
            cmd.Parameters.Add(new SqlParameter("@CompanyBankDefault", companyDefaultBankAccountList));
            cmd.Parameters.Add(new SqlParameter("@CompanyPurchaseControlDefault", companyDefaultPurchaseControlAccountList)); 
            cmd.Parameters.Add(new SqlParameter("@isActive", isActive));
            int rowsAffected = cmd.ExecuteNonQuery();
        }
    }

}