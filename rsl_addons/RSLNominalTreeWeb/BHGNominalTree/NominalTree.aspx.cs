﻿using System; 
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
 

public partial class NominalTree : BasePage
{

    public string ConnectString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
    public string uAccountid;
    public string menu;
    public string MenuList;

    protected void Page_Load(object sender, EventArgs e)
    {
        uAccountid = "113";
        if (Session["UserId"] == null)
        {
            //Server.TransferRequest("~/../BHAIntranet/Login.aspx");
        }

        // GET MAIN MENU
        DataTable dtcheck = new DataTable();
        NominalCodesCore.GetMainMenu(uAccountid, dtcheck);
        menu = "";

        if (dtcheck.Rows.Count > 0)
        {
            
            for (int p = 0; p < dtcheck.Rows.Count; p++)
                { 
                    string Path =  dtcheck.Rows[p]["thepath"].ToString();
                    menu = menu + "<a href='" + Path + "'>" + dtcheck.Rows[p]["Description"].ToString() + "</a>";
                }
             
        }

        // GET MAIN MENU
        DataTable dtcheck2 = new DataTable();
        NominalCodesCore.GetMainAccountMenu(uAccountid, dtcheck2);

        if (dtcheck2.Rows.Count > 0)
        {

            for (int p2 = 0; p2 < dtcheck2.Rows.Count; p2++)
            {
                DataTable dtcheck3 = new DataTable();
                NominalCodesCore.GetMainAccountMenuPages(uAccountid, dtcheck3);
                string subMenu = "";

                if (dtcheck3.Rows.Count > 0)
                {
                    subMenu = subMenu +
                              "<div class='asb-dropdown-content' style='min-width:150px !important; margin-left:8px !important;'>";
                    for (int p3 = 0; p3 < dtcheck3.Rows.Count; p3++)
                    {
                       
                        if (dtcheck3.Rows[p3]["MENUID"].ToString() == dtcheck2.Rows[p2]["MENUID"].ToString())
                        {
                            subMenu = subMenu +  "<a href='" + dtcheck3.Rows[p3]["thepath"].ToString() + "'>" + dtcheck3.Rows[p3]["Description"].ToString() + "</a>";
                        }
                        
                    }
                    subMenu = subMenu + "</div>";
                }

                string DefaultPath = "/Accounts/Accounts.asp";
                string Path = (string.IsNullOrEmpty(dtcheck2.Rows[p2]["thepath"].ToString())) ? DefaultPath : dtcheck2.Rows[p2]["thepath"].ToString();

                MenuList = MenuList + "<li><div class='asb-dropdown'><a href='" + Path + "'>" +
                           dtcheck2.Rows[p2]["Description"] + "</a>" + "" + subMenu + "<div></li>";
            }

        } 


        DropdownLists.getCompany(ddCompany);

        DeleteNominalCode();
    }

    private void DeleteNominalCode()
    {
        string AccountId = Request.QueryString["aid"];
        string at = Request.QueryString["at"];

        if (!string.IsNullOrEmpty(at) && !string.IsNullOrEmpty(AccountId))
        {
            using (SqlConnection myConnection = new SqlConnection(ConnectString))
            {
                myConnection.Open();

                // 1.  create a command object identifying the stored procedure
                SqlCommand cmd = new SqlCommand("NL_DeleteCode", myConnection);

                // 2. set the command object so it knows to execute a stored procedure
                cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                cmd.Parameters.Add(new SqlParameter("@AccountId", Int32.Parse(AccountId)));

                cmd.ExecuteNonQuery();
            }
        }
    }
}