﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NominalDetail.aspx.cs" Inherits="NominalDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Nominal Tree</title>

<style type="text/css">

    #modalContainer {
        background-color:rgba(0, 0, 0, 0.3);
        position:absolute;
        width:100%;
        height:100%;
        top:0px;
        left:0px;
        z-index:10000;
        background-image:url(tp.png); /* required by MSIE to prevent actions on lower z-index elements */
    }

    #alertBox {
        position:relative;
        width:400px;
        min-height:200px;
        margin-top:50px;
        border:1px solid #666;
        background-color:#fff;
        background-repeat:no-repeat;
        background-position:20px 30px;
    }

    #modalContainer > #alertBox {
        position:fixed;
    }

    #alertBox h1 {
        margin:0;
        font:bold 0.9em verdana,arial;
        background-color:#000000;
        color:#FFF;
        border-bottom:1px solid #000;
        padding:2px 0 2px 5px;
    }

    #alertBox p {
        font:12px verdana,arial;
        height:10px;
        padding-left:5px;
        padding-top:5px;
        margin-left:55px;
    }

    #alertBox #closeBtn {
        display:block;
        position:relative;
        margin:5px auto;
        padding:7px;
        border:0 none;
        width:70px;
        font:0.9em verdana,arial;
        text-transform:uppercase;
        text-align:center;
        color:#FFF;
        background-color:#000000;
        border-radius: 3px;
        text-decoration:none;
    }

    #alertBox #YesBtn {
        display:block;
        position:relative;
        margin:5px auto;
        padding:7px;
        border:0 none;
        width:70px;
        font:0.9em verdana,arial;
        text-transform:uppercase;
        text-align:center;
        color:#FFF;
        background-color:#000000;
        border-radius: 3px;
        text-decoration:none;
    }

    /* unrelated styles */

    #mContainer {
        position:relative;
        width:600px;
        margin:auto;
        padding:5px;
        border-top:2px solid #000;
        border-bottom:2px solid #000;
        font:0.7em verdana,arial;
    }

    h1,h2 {
        margin:0;
        padding:4px;
        font:bold 1.5em verdana;
        border-bottom:1px solid #000;
    }

    code {
        font-size:1.2em;
        color:#069;
    }

    #credits {
        position:relative;
        margin:25px auto 0px auto;
        width:350px; 
        font:0.7em verdana;
        border-top:1px solid #000;
        border-bottom:1px solid #000;
        height:90px;
        padding-top:4px;
    }

    #credits img {
        float:left;
        margin:5px 10px 5px 0px;
        border:1px solid #000000;
        width:80px;
        height:79px;
    }

    .important {
        background-color:#F5FCC8;
        padding:2px;
    }

    code span {
        color:green;
    }

</style>

<script type="text/javascript">

    function DeleteAlert() {

        var AccountCode = document.getElementById('<%=txtAccountNumber.ClientID%>').value;
        var AccountName = document.getElementById('<%=txtName.ClientID%>').value;
        confirm("" + AccountCode + ": " + AccountName + " \n\n Are you sure you want to delete this code?");
        return false;
    }


    var ALERT_TITLE = "Delete Nominal Code?";
    var ALERT_BUTTON_TEXT_NO = "No";
    var ALERT_BUTTON_TEXT_YES = "Yes";

    if (document.getElementById) {
        window.alert = function (txt) {
            createCustomAlert(txt);
        }
    }

    function createCustomAlert(txt) {

        var AccountCode = document.getElementById('<%=txtAccountNumber.ClientID%>').value;
        var AccountName = document.getElementById('<%=txtName.ClientID%>').value;
        txt = "" + AccountCode + ": " + AccountName + "";
        var txt2 = "Are you sure you want to delete this code?";
        d = document;

        if (d.getElementById("modalContainer")) return;

        mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
        mObj.id = "modalContainer";
        mObj.style.height = d.documentElement.scrollHeight + "px";

        alertObj = mObj.appendChild(d.createElement("div"));
        alertObj.id = "alertBox";
        if (d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
        alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
        alertObj.style.visiblity = "visible";

        h1 = alertObj.appendChild(d.createElement("h1"));
        h1.appendChild(d.createTextNode(ALERT_TITLE));

        topGap = alertObj.appendChild(d.createElement("p"));

        msg = alertObj.appendChild(d.createElement("p"));
        //msg.appendChild(d.createTextNode(txt));
        msg.innerHTML = txt;


        msg2 = alertObj.appendChild(d.createElement("p"));

        msg3 = alertObj.appendChild(d.createElement("p"));
        //msg.appendChild(d.createTextNode(txt));
        msg3.innerHTML = txt2;

        msg4 = alertObj.appendChild(d.createElement("p"));

        buttonp = alertObj.appendChild(d.createElement("p"));

        btn = buttonp.appendChild(d.createElement("a"));
        btn.id = "closeBtn";
        btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT_NO));
        btn.href = "#";
        btn.style.float = "left";
        btn.style.paddingLeft = "10px";
        btn.focus();
        btn.onclick = function () { removeCustomAlert(); return false; }

        btn2 = buttonp.appendChild(d.createElement("a"));
        btn2.id = "YesBtn";
        btn2.appendChild(d.createTextNode(ALERT_BUTTON_TEXT_YES));
        btn2.href = "NominalTree.aspx?at=d&aid=<%=AccountId%>";
        console.write("asasas");
        btn2.style.float = "left";
        btn2.focus();
        btn2.onclick = function () { removeCustomAlert(); return false; }


        alertObj.style.display = "block";

    }

    function removeCustomAlert() {
        document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    }

    function ful() {
        alert();
    }

    function isValidForm() {

        if ([<%=NominalCodeList%>].indexOf(document.getElementById('<%=txtAccountNumber.ClientID%>').value) > -1) {
            document.getElementById('UniqueCodeValidation').style.display = 'block';
            return false;
        }
    }
 
</script>

    </head>
        <body>
         
        <form id="form1" runat="server" >
             
    <div>

        <div style="font-weight: bold">Nominal Code Details</div>
        <br/>
        <div style="background-color: cornsilk; overflow: auto; padding: 10px;">
            <div style="font-weight: bold">Parent Code(s)</div>
            <div>
                <asp:Label ID="lblGrandParent" runat="server"/>
            </div>
            <div id="divHasParent" runat="server" style="float: left; width: 20px;">&#8618;</div> <asp:Label ID="lblParent" runat="server" style="float: left; width: 250px;"></asp:Label>
        </div>
        <br/>
        <div>
        <label class="rslLabel" style="float: left; width: 80px;">Number:</label> <asp:TextBox runat="server" ID="txtAccountNumber" style="float: left; width: 250px;"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtAccountNumber" ForeColor="red">* Required</asp:RequiredFieldValidator>
        </div> <br/>
        <div>
            <label class="rslLabel" style="float: left; width: 80px;">Name:</label>
            <asp:TextBox runat="server" ID="txtName" style="float: left; width: 250px;"></asp:TextBox> 
            <asp:RequiredFieldValidator runat="server" ID="rqName" ControlToValidate="txtName" ForeColor="red">* Required</asp:RequiredFieldValidator>
            
        </div>
        <div id="UniqueCodeValidation" style="display: none; color: #ff0000;"><BR />This nominal code is already in use!</div>
        <br/>
        <div>
            <label class="rslLabel" style="float: left; width: 80px;">Type:</label> 
            <asp:DropDownList runat="server" ID="ddAccountType" style="float: left; width: 250px;"></asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddAccountType" ForeColor="red">* Required</asp:RequiredFieldValidator>
        </div>
        <br/>
        <div>
            <label class="rslLabel" style="float: left; width: 80px;">Vat Rate:</label>
            <asp:DropDownList runat="server" ID="ddVAT" style="float: left; width: 250px;"></asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="ddAccountType" ForeColor="red">* Required</asp:RequiredFieldValidator>
        </div>
        <br/>
        <label class="rslLabel" style="float: left; padding-right: 5px;">Allocate Expenditures?:</label> <asp:Label runat="server" ID="Label3"></asp:Label>
        <div style="padding-left: 5px">
            <asp:RadioButtonList ID="rdoAllocateExpenditure" runat="server" RepeatDirection="Horizontal"/>
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="rdoAllocateExpenditure" ForeColor="red">* Required</asp:RequiredFieldValidator>
        </div>
        <label class="rslLabel" style="float: left; width: 80px;">Company:</label> <asp:CheckBoxList runat="server" ID="chkLstCompany" style="font-weight: normal"></asp:CheckBoxList>
        <br/>
        <label class="rslLabel" style="float: left; padding-right: 5px;">Bank Account?:</label> 
        <div style="padding-left: 5px">
            <asp:RadioButtonList ID="rdoIsBankAccount" runat="server" RepeatDirection="Horizontal"/>
        </div>
       
        <br/>
        <label class="rslLabel" style="float: left; width: 120px;">Default Bank:
          <br/><font style="font-style: italic; font-size: 9px;">(Only one bank account per company can be selected)</font></label>
        <div style="padding-left: 5px">
        <asp:CheckBoxList runat="server" ID="chkLstCompanyDefaultBankAccount" style="font-weight: normal"></asp:CheckBoxList>
        </div>
        <br/>
        
        <label class="rslLabel" style="float: left; width: 120px;">Default Purchase Control:
            <br/><font style="font-style: italic; font-size: 9px;">(Only one purchase control account per company can be selected)</font></label> 
        <div style="padding-left: 5px">
        <asp:CheckBoxList runat="server" ID="chkLstCompanyDefaultPurchaseControlAccount" style="font-weight: normal"></asp:CheckBoxList>
       </div> 
        <br/>

        <div id="divActive" runat="server">
            <label class="rslLabel" style="float: left; padding-right: 5px;">Active?:</label> 
            <div style="padding-left: 5px">
                <asp:RadioButtonList ID="rdoActive" runat="server" RepeatDirection="Horizontal"/>
            </div>
        </div>
        <br/>
        <div width="100%">
            <div style=" width: 100px;float: left;"><asp:Button ID="btnSave" runat="server" Text="Save Changes" style="float: left;"  OnClick="btnSave_OnClick"/> </div>
            <div style="padding-left: 10px; width: 10px;float: left;"></div>
            <div style=" width: 100px;float: left;"><input type="button" ID="btnDelete" runat="server" value="Delete" style="float: left;" onclick="alert('Alert this pages'); false;"/></div>
            </div>
        <asp:HiddenField ID="hdnParent" runat="server"/>
        <asp:HiddenField ID="hdnId" runat="server"/>
        </div>
   
</form>

</body>
</html>