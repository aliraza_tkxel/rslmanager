
glyph_opts = {
    map: {
        doc: "glyphicon glyphicon-tag",
        docOpen: "glyphicon glyphicon-tag",
        checkbox: "glyphicon glyphicon-unchecked",
        checkboxSelected: "glyphicon glyphicon-check",
        checkboxUnknown: "glyphicon glyphicon-share",
        dragHelper: "glyphicon glyphicon-play",
        dropMarker: "glyphicon glyphicon-arrow-right",
        error: "glyphicon glyphicon-warning-sign",
        expanderClosed: "glyphicon glyphicon-menu-right",
        expanderLazy: "glyphicon glyphicon-menu-right",  // glyphicon-plus-sign
        expanderOpen: "glyphicon glyphicon-menu-down",  // glyphicon-collapse-down
        folder: "glyphicon glyphicon-tag",
        folderOpen: "glyphicon glyphicon-tag",
        loading: "glyphicon glyphicon-refresh glyphicon-spin"
    }
};
$(function () {
    // Initialize Fancytree
    
    // Add code to initialize the tree when the document is loaded: 
    $("#treetable").fancytree({
        //"dnd",
        extensions: ["edit", "glyph", "table"],
        checkbox: false,
        //dnd: {
        //	focusOnClick: true,
        //	dragStart: function(node, data) { return true; },
        //	dragEnter: function(node, data) { return true; },
        //	dragDrop: function(node, data) { data.otherNode.copyTo(node, data.hitMode); }
        //},
        glyph: glyph_opts,
        source: { url: "NominalTreeJson.aspx?openedAccountId=" + uAccountId, debugDelay: 1000 },
        table: {
            checkboxColumnIdx: 1,
            nodeColumnIdx: 2
        },

        activate: function (event, data) {
        },
        lazyLoad: function (event, data) {
            data.result = { url: "ajax-sub2.json", debugDelay: 1000 };
        },
        renderColumns: function (event, data) {
            var node = data.node,
                $tdList = $(node.tr).find(">td");
            //$tdList.eq(0).text(node.getIndexHier());
            $tdList.eq(3).text(node.data["codetype"]);
        }
    });
});

$(function () {
    $("#btnExpandAll").click(function () {
        $("#tree").fancytree("getTree").visit(function (node) {
            node.setExpanded(true);
        });
    });
    $("#btnCollapseAll").click(function () {
        $("#tree").fancytree("getTree").visit(function (node) {
            node.setExpanded(false);
        });
    });

    $("#fontSize").change(function () {
        $("#tree .fancytree-container").css("font-size", $(this).prop("value") + "pt");
    });//.prop("value", 12);

    $("#plainTreeStyles").on("change", "input", function (e) {
        $("#tree ul").toggleClass($(this).data("class"), $(this).is(":checked"));
    });
    $("#bootstrapTableStyles").on("change", "input", function (e) {
        $("#treetable").toggleClass($(this).data("class"), $(this).is(":checked"));
    });
});