﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class ApplicationConstants
    {
        public const string LoginPath = "~/../BHAIntranet/Login.aspx";
        public const string UsersAccountDeactivated = "Your access to the Nominal Tree Admin has been de-activiated. Please contact administrator.";
        public const string HRMenu = "Accounts";
        /// <summary>
        /// DateFormat key. Saved in Web.config
        /// </summary>
        public const string KeyDateFormat = "DateFormat";
        /// <summary>
        /// DateFormat key for Model. Saved in Web.config
        /// </summary>
        public const string KeyDateFormatModel = "DateFormatForModel";
        /// <summary>
        /// Virtual Directory Key. This key is used to get virtual directory name from Web.config 
        /// </summary>
        public const string VirtualDirectory = "VirtualDirectory";

        public const string ContentType = "application/json";
        //TODO:
        //public const string BaseURL = GeneralHelper. + "/BHGNominalTree/";

       
    }
}
